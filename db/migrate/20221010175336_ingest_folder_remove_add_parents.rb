class IngestFolderRemoveAddParents < ActiveRecord::Migration[5.2]
  def change
    change_table :ingest_folders do |t|
      t.remove :add_parents
    end
  end
end
