class AddPidToEvents < ActiveRecord::Migration[5.2]
  def change
    change_table :events do |t|
      t.string "pid"
    end
    add_index :events, :pid    
  end
end
