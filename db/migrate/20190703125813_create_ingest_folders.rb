class CreateIngestFolders < ActiveRecord::Migration[5.2]
  def change
    create_table :ingest_folders do |t|
      t.references :user
      t.string :base_path
      t.string :sub_path
      t.string :admin_policy_id
      t.string :collection_id
      t.string :model
      t.string :checksum_file
      t.string :checksum_type
      t.boolean :add_parents
      t.integer :parent_id_length

      t.timestamps
    end
  end
end
