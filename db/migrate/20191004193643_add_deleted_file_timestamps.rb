class AddDeletedFileTimestamps < ActiveRecord::Migration[5.2]
  def change
    change_table :deleted_files do |t|
      t.timestamps
    end
  end
end
