# frozen_string_literal: true
# This migration comes from ddr_core_engine (originally 20200207194453)
class AddDefaultToLockVersion < ActiveRecord::Migration[5.2]
  def change
    change_column_default :orm_resources, :lock_version, from: nil, to: 0
  end
end
