class AddDuidToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :duid, :string
    add_index :users, :duid
  end
end
