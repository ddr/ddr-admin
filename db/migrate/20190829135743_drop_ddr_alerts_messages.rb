class DropDdrAlertsMessages < ActiveRecord::Migration[5.2]
  def up
    drop_table :ddr_alerts_messages
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
