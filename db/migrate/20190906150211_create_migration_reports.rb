class CreateMigrationReports < ActiveRecord::Migration[5.2]
  def change

    create_table :migration_reports do |t|
      t.string :fcrepo3_pid
      t.string :resource_id
      t.string :model
      t.string :object_status, default: 'NEEDED'
      t.string :relationship_status, default: 'NEEDED'
      t.string :completion_status, default: 'NEEDED'

      t.timestamps
    end

    add_index :migration_reports, :fcrepo3_pid, unique: true
    add_index :migration_reports, :resource_id, unique: true
    add_index :migration_reports, :model
    add_index :migration_reports, :object_status
    add_index :migration_reports, :relationship_status
    add_index :migration_reports, :completion_status

    create_table :migration_timers do |t|
      t.belongs_to :migration_report, index: true
      t.string :event
      t.float :duration

      t.timestamps
    end

  end
end
