class RenamePidColumnToResourceId < ActiveRecord::Migration[5.2]
  def change
    rename_column :batch_objects, :pid, :resource_id
    remove_index :events, column: :pid
    rename_column :events, :pid, :resource_id
    add_index :events, :resource_id
  end
end
