class AddMetadataFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :metadata_files do |t|
      t.references :user
      t.string :profile
      t.string :collection_id
      t.timestamps
      t.attachment :metadata
    end
  end
end
