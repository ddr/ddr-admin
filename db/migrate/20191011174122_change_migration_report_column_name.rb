class ChangeMigrationReportColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :migration_reports, :completion_status, :index_status
  end
end
