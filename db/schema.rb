# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2024_04_23_194036) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "batch_object_attributes", id: :serial, force: :cascade do |t|
    t.integer "batch_object_id"
    t.string "datastream"
    t.string "name"
    t.string "operation"
    t.text "value"
    t.string "value_type"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "batch_object_datastreams", id: :serial, force: :cascade do |t|
    t.integer "batch_object_id"
    t.string "operation"
    t.string "name"
    t.text "payload"
    t.string "payload_type"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "checksum"
    t.string "checksum_type"
  end

  create_table "batch_object_messages", id: :serial, force: :cascade do |t|
    t.integer "batch_object_id"
    t.integer "level", default: 0
    t.text "message"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "batch_object_relationships", id: :serial, force: :cascade do |t|
    t.integer "batch_object_id"
    t.string "name"
    t.string "operation"
    t.string "object"
    t.string "object_type"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "batch_object_roles", id: :serial, force: :cascade do |t|
    t.integer "batch_object_id"
    t.string "operation"
    t.string "agent"
    t.string "role_type"
    t.string "role_scope"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "batch_objects", id: :serial, force: :cascade do |t|
    t.integer "batch_id"
    t.string "identifier"
    t.string "model"
    t.string "label"
    t.string "resource_id"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "type"
    t.boolean "verified", default: false
    t.boolean "handled", default: false
    t.boolean "processed", default: false
    t.boolean "validated", default: false
    t.index ["verified"], name: "index_batch_objects_on_verified"
  end

  create_table "batches", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "user_id"
    t.string "status"
    t.datetime "start", precision: nil
    t.datetime "stop", precision: nil
    t.string "outcome"
    t.string "version"
    t.string "logfile_file_name"
    t.string "logfile_content_type"
    t.integer "logfile_file_size"
    t.datetime "logfile_updated_at", precision: nil
    t.datetime "processing_step_start", precision: nil
    t.string "collection_id"
    t.string "collection_title"
  end

  create_table "bookmarks", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "user_type"
    t.string "document_id"
    t.string "document_type"
    t.binary "title"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["document_id"], name: "index_bookmarks_on_document_id"
    t.index ["user_id"], name: "index_bookmarks_on_user_id"
  end

  create_table "deleted_files", force: :cascade do |t|
    t.string "repo_id"
    t.string "file_id"
    t.string "version_id"
    t.string "source"
    t.string "path"
    t.datetime "last_modified", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["last_modified"], name: "index_deleted_files_on_last_modified"
    t.index ["repo_id", "file_id", "version_id"], name: "index_deleted_files_on_repo_id_and_file_id_and_version_id"
    t.index ["source"], name: "index_deleted_files_on_source"
  end

  create_table "events", force: :cascade do |t|
    t.datetime "event_date_time", precision: nil
    t.integer "user_id"
    t.string "type"
    t.string "resource_id"
    t.string "software"
    t.text "comment"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "summary"
    t.string "outcome"
    t.text "detail"
    t.text "exception"
    t.string "user_key"
    t.string "permanent_id"
    t.string "pid"
    t.index ["event_date_time"], name: "index_events_on_event_date_time"
    t.index ["outcome"], name: "index_events_on_outcome"
    t.index ["permanent_id"], name: "index_events_on_permanent_id"
    t.index ["pid"], name: "index_events_on_pid"
    t.index ["resource_id"], name: "index_events_on_resource_id"
    t.index ["type"], name: "index_events_on_type"
  end

  create_table "ingest_folders", force: :cascade do |t|
    t.bigint "user_id"
    t.string "base_path"
    t.string "sub_path"
    t.string "admin_policy_id"
    t.string "collection_id"
    t.string "model"
    t.string "checksum_file"
    t.string "checksum_type"
    t.integer "parent_id_length"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["user_id"], name: "index_ingest_folders_on_user_id"
  end

  create_table "metadata_files", force: :cascade do |t|
    t.bigint "user_id"
    t.string "profile"
    t.string "collection_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "metadata_file_name"
    t.string "metadata_content_type"
    t.bigint "metadata_file_size"
    t.datetime "metadata_updated_at", precision: nil
    t.index ["user_id"], name: "index_metadata_files_on_user_id"
  end

  create_table "migration_reports", force: :cascade do |t|
    t.string "fcrepo3_pid"
    t.string "resource_id"
    t.string "model"
    t.string "object_status", default: "NEEDED"
    t.string "relationship_status", default: "NEEDED"
    t.string "index_status", default: "NEEDED"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["fcrepo3_pid"], name: "index_migration_reports_on_fcrepo3_pid", unique: true
    t.index ["index_status"], name: "index_migration_reports_on_index_status"
    t.index ["model"], name: "index_migration_reports_on_model"
    t.index ["object_status"], name: "index_migration_reports_on_object_status"
    t.index ["relationship_status"], name: "index_migration_reports_on_relationship_status"
    t.index ["resource_id"], name: "index_migration_reports_on_resource_id", unique: true
  end

  create_table "migration_timers", force: :cascade do |t|
    t.bigint "migration_report_id"
    t.string "event"
    t.float "duration"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["migration_report_id"], name: "index_migration_timers_on_migration_report_id"
  end

  create_table "orm_resources", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.jsonb "metadata", default: {}, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "internal_resource"
    t.integer "lock_version", default: 0
    t.index ["internal_resource"], name: "index_orm_resources_on_internal_resource"
    t.index ["metadata"], name: "index_orm_resources_on_metadata", using: :gin
    t.index ["metadata"], name: "index_orm_resources_on_metadata_jsonb_path_ops", opclass: :jsonb_path_ops, using: :gin
    t.index ["updated_at"], name: "index_orm_resources_on_updated_at"
  end

  create_table "searches", id: :serial, force: :cascade do |t|
    t.binary "query_params"
    t.integer "user_id"
    t.string "user_type"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["user_id"], name: "index_searches_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "username", default: "", null: false
    t.string "first_name"
    t.string "middle_name"
    t.string "nickname"
    t.string "last_name"
    t.string "display_name"
    t.string "duid"
    t.index ["duid"], name: "index_users_on_duid"
    t.index ["email"], name: "index_users_on_email"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end
end
