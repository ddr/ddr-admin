# DDR API

## /apidoc - Swagger UI

This is a generated Swagger (a.k.a. OpenAPI) 2.0 UI that presents information about all the
API methods and parameters in functional forms, with the exception of authentication, described below.

## Authentication

### Creating a Long-Lived Token

It is now possible to create an access token that will remain valid for one (1) year by using the
[Advanced IT Options](https://idms-web-selfservice.oit.duke.edu/advanced) available in the
[Duke OIT Account Self-Service](https://idms-web-selfservice.oit.duke.edu) app.

In the `Manage Your OAuth Secrets` section, create a new OAuth token:

    Duke UniqueID:              [Select your unique ID (NetID)]
	Client ID Restriction:      [blank, or enter "lib-ddr-oauth" to limit token use to DDR]
	Endpoint Restriction Regex: [blank]

You will get both an access token and a refresh token.

### Authenticating to the API with the Access Token

In Swagger UI, click the _Authorize_ button in the upper right.
In the "oauth (apiKey)" section at the bottom, type in `Bearer`, one space, paste in the access token, and
click _Authorize_.[1]

```
Bearer <ACCESS_TOKEN>
```

If using a command-line tool like `curl`, API requests must include a properly formed header with the
authorization information:

```
$ curl -H 'Authorization: Bearer <ACCESS_TOKEN>' https://ddr-admin.lib.duke.duke.edu/api/resources
```

## Authorization

The API currently requires superuser privileges because it does not yet perform resource-level
authorizations.  This may change in the future.

## Notes

[1] Swagger 3.0 supports the "Bearer" scheme natively; Swagger 2.0 does not; hence, the manually insertion of "Bearer"
in front of the token.
