module Ddr
  RSpec.describe BatchFileCharacterization do
    let(:component) { create_for_repository(:component, :with_content_file) }

    it 'enqueues Components needing file characterization' do
      component
      allow(Ddr::V3::FileUpdateService[:fits_file]).to receive(:call)
      described_class.call
      expect(Ddr::V3::FileUpdateService[:fits_file]).to have_received(:call)
    end
  end
end
