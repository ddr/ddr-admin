module Ddr
  RSpec.describe FileCharacterizationService, type: :service do
    subject { described_class.new(resource) }

    let(:resource) { Component.new(id:) }
    let(:id) { Valkyrie::ID.new(SecureRandom.uuid) }
    let(:file) { fixture_file_upload('imageA.tif', 'image/tiff') }
    let(:fits_xml) { ::File.read(Rails.root.join('spec/fixtures/files/fits.xml')) }

    describe '#call' do
      before do
        change_set = ResourceChangeSet.change_set_for(resource)
        change_set.add_file(file, :content)
        change_set.sync
      end

      describe 'when there is an error running FITS' do
        before do
          allow(subject).to receive(:run_fits).and_raise(FileCharacterizationService::FITSError)
        end

        it 'does not add a `fits` file' do
          subject.call
        rescue FileCharacterizationService::FITSError
        ensure
          expect(resource.fits_file).to be_nil
        end
      end

      describe 'when FITS runs successfully' do
        before do
          allow(subject).to receive(:run_fits).and_return(fits_xml)
        end

        it 'adds a fits file to the resource' do
          subject.call
          res = Ddr.query_service.find_by(id:)
          expect(res.fits_file.content).to eq(fits_xml)
        end
      end
    end

    describe 'structure updating' do
      let(:resource) { create_for_repository(:component, :with_content_file) }

      before do
        allow(subject).to receive(:run_fits).and_return(fits_xml)
      end

      it 'does not trigger a structural metadata update when persisted' do
        expect_any_instance_of(Ddr::ResourceChangeSetPersister)
          .to receive(:save)
          .with(change_set: have_attributes(skip_structure_updates: true))
        subject.call
      end
    end

    describe 'content file' do
      describe 'present' do
        let(:resource) { create_for_repository(:component, :with_content_file) }

        it 'calls FITS' do
          expect(subject).to receive(:run_fits) { fits_xml }
          subject.call
        end
      end

      describe 'not present' do
        let(:resource) { create_for_repository(:component) }

        it 'logs a warning and does not call FITS' do
          expect(Rails.logger).to receive(:warn).with(I18n.t('ddr.resource.warnings.no_content_file',
                                                             model: resource.class.name,
                                                             id: resource.id.id))
          expect(subject).not_to receive(:run_fits)
          subject.call
        end
      end
    end
  end
end
