module Ddr
  RSpec.describe MonitorIngestFolder, type: :service do
    let(:user_key) { 'joe@test.edu' }
    let(:user_email) { 'joe.test@test.edu' }
    let(:user) { double('User', email: user_email, user_key:) }
    let(:collection_id) { SecureRandom.uuid }
    let(:collection_title) { 'Test Collection' }
    let(:collection) { double('Ddr::Collection', id: collection_id, title: [collection_title]) }
    let(:ingest_folder_id) { 1 }
    let(:folder_path) { '/abc/def/' }
    let(:ingest_folder) do
      double('Ddr::IngestFolder', id: ingest_folder_id, abbreviated_path: folder_path, collection_id:,
                                  user:)
    end
    let(:batch_id) { 7 }
    let(:batch) { double('Ddr::Batch::Batch', id: batch_id) }
    let(:batch_url) do
      Rails.application.routes.url_helpers.ddr_batch_url(batch, host: Ddr::Admin.application_hostname,
                                                                protocol: 'https')
    end
    let(:notification) { [IngestFolder::FINISHED, Time.now, Time.now, 'abcdef', payload] }
    let(:payload) { { ingest_folder_id:, batch_id: } }
    let(:expected_msg) do
      <<~EOS
        DPC Folder Ingest has created batch ##{batch.id}
        For collection: #{collection_title}
        From folder: #{folder_path}

        To review and process the batch, go to #{batch_url}
      EOS
    end

    before do
      allow(Ddr.query_service).to receive(:find_by).with(id: collection_id).and_return(collection)
      allow(IngestFolder).to receive(:find).with(ingest_folder_id).and_return(ingest_folder)
      allow(Batch::Batch).to receive(:find).with(batch_id).and_return(batch)
    end

    describe '#call' do
      it 'generates an appropriate email' do
        expect(JobMailer).to receive(:basic).with(to: user_email,
                                                  subject: "BATCH CREATED - DPC Folder Ingest - #{collection_title}",
                                                  message: expected_msg).and_call_original
        described_class.call(*notification)
      end
    end
  end
end
