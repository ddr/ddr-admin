module Ddr
  RSpec.describe AssignPermanentIdService do
    describe '.call' do
      context 'when resource does not have a permanent id' do
        let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
        let(:resource) { FactoryBot.create(:item, id: valkyrie_id) }

        it 'attempts to assign a permanent id to the resource' do
          expect(PermanentId).to receive(:assign!).with(resource.id, nil, { skip_structure_updates: true })
          described_class.call([resource.id])
        end
      end

      context 'when resource has a permanent id' do
        let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
        let(:resource) { Ddr.persister.save(resource: Ddr::Item.new(id: valkyrie_id, permanent_id: 'foo')) }

        it 'does not attempt to assign a permanent id to the resource' do
          expect(PermanentId).not_to receive(:assign!)
          described_class.call([resource.id])
        end
      end
    end
  end
end
