RSpec.shared_examples 'a successfully built manifest based ingest batch' do
  context 'manifest based ingest' do
    let(:batch) { batches.first }
    let(:collection_title) { 'My Collection' }
    let(:expected_item_titles) { ['Item A Title'] }
    let(:batch_objects) { batch.batch_objects }
    let(:collections) { batch_objects.where(model: 'Ddr::Collection') }
    let(:items) { batch_objects.where(model: 'Ddr::Item') }
    let(:components) { batch_objects.where(model: 'Ddr::Component') }
    let(:targets) { batch_objects.where(model: 'Ddr::Target') }

    it 'builds an appropriate batch' do
      # Batch expectations
      expect(batch.id).to be_present
      expect(batch.name).to eq(batch_name)
      expect(batch.collection_id).to eq(coll_id)
      expect(batch.collection_title).to eq(collection_title)
      expect(batch.status).to eq(Ddr::Batch::Batch::STATUS_READY)

      # All batch object expectations
      batch_objects.each do |obj|
        expect(obj.type).to eq('Ddr::Batch::IngestBatchObject')
      end

      # Collection expectations
      expect(collections.count).to eq(coll_count)
      if coll_count == 1
        expect(collections.first.id).to be_present
        expect(collections.first.batch_object_attributes.where(name: 'title').first.value).to eq(collection_title)
        expect(collections.first.batch_object_attributes.where(name: 'admin_set').first.value).to eq(admin_set)
        expect(collections.first.batch_object_roles.size).to eq(1)
        expect(collections.first.batch_object_roles[0].agent).to eq(user.user_key)
        expect(collections.first.batch_object_roles[0].role_type).to eq(Ddr::Auth::Roles::RoleTypes::CURATOR.title)
        expect(collections.first.batch_object_roles[0].role_scope).to eq(Ddr::Auth::Roles::POLICY_SCOPE)
      end

      # Item expectations
      expect(items.count).to eq(1)
      item_ids = []
      item_titles = []
      items.each do |obj|
        expect(obj.id).to be_present
        item_ids << obj.resource_id
        if a = obj.batch_object_attributes.where(name: 'title').first
          item_titles << a.value
        end
        admin_policy_relationships = obj.batch_object_relationships.where(
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY
        )
        expect(admin_policy_relationships.size).to eq(1)
        expect(admin_policy_relationships.first.object).to eq(coll_id)
        parent_relationships = obj.batch_object_relationships.where(
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT
        )
        expect(parent_relationships.size).to eq(1)
        expect(parent_relationships.first.object).to eq(coll_id)
      end
      expect(item_titles).to match_array(expected_item_titles)

      # Component expectations
      expect(components.count).to eq(2)
      component_filepaths = []
      component_checksums = []
      components.each do |obj|
        admin_policy_relationships = obj.batch_object_relationships.where(
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY
        )
        expect(admin_policy_relationships.size).to eq(1)
        expect(admin_policy_relationships.first.object).to eq(coll_id)
        # Parent relationship
        parent_relationships = obj.batch_object_relationships.where(
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT
        )
        expect(parent_relationships.size).to eq(1)
        expect(item_ids.map(&:to_s)).to include(parent_relationships.first.object)
        # Content datastream
        content_datastreams = obj.batch_object_datastreams.where(name: 'content')
        expect(content_datastreams.size).to eq(1)
        expect(content_datastreams.first.checksum_type).to eq(Ddr::Files::CHECKSUM_TYPE_SHA1)
        component_filepaths << content_datastreams.first.payload
        component_checksums << content_datastreams.first.checksum
      end
      expect(component_filepaths).to include('/opt/app-root/spec/fixtures/batch_ingest/standard_ingest/example/data/itemA/image1.tiff')
      expect(component_filepaths).to include('/opt/app-root/spec/fixtures/abc001001.tif')
      expect(component_checksums).to include('548bd2678027f3acb4d4c5ccedf6f92ca07f74bd')
      expect(component_checksums).to include('2cf23f0035c12b6242093e93d0f7eeba0b1e08e8')

      # Target expectations
      expect(targets.count).to eq(1)
      target_filepaths = []
      target_checksums = []
      targets.each do |obj|
        admin_policy_relationships = obj.batch_object_relationships.where(
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY
        )
        expect(admin_policy_relationships.size).to eq(1)
        expect(admin_policy_relationships.first.object).to eq(coll_id)
        # Targets don't have parents
        expect(
          obj.batch_object_relationships.where(
            name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT
          ).size
        ).to eq 0
        # Content datastream
        content_datastreams = obj.batch_object_datastreams.where(name: 'content')
        expect(content_datastreams.size).to eq(1)
        expect(content_datastreams.first.checksum_type).to eq(Ddr::Files::CHECKSUM_TYPE_SHA1)
        target_filepaths << content_datastreams.first.payload
        target_checksums << content_datastreams.first.checksum
      end
      expect(target_filepaths).to include('/opt/app-root/spec/fixtures/batch_ingest/standard_ingest/example/data/dpc_targets/target.png')
      expect(target_checksums).to include('7cc5abd7ed8c1c907d86bba5e6e18ed6c6ec995c')
    end
  end
end

RSpec.shared_examples 'a successfully built manifest based update batch' do
  context 'manifest based update' do
    let(:batch) { batches.first }
    let(:collection_title) { 'My Collection' }
    let(:expected_item_titles) { ['Item A Title', 'New Title'] }
    let(:expected_item_descriptions) { %w[Bar Baz Fi Fo Fum] }
    let(:batch_objects) { batch.batch_objects }
    let(:collections) { batch_objects.where(model: 'Ddr::Collection') }
    let(:items) { batch_objects.where(model: 'Ddr::Item') }
    let(:components) { batch_objects.where(model: 'Ddr::Component') }

    it 'builds an appropriate batch' do
      # Batch expectations
      expect(batch.id).to be_present
      expect(batch.name).to eq(batch_name)
      expect(batch.collection_id).to eq(coll_id)
      expect(batch.collection_title).to eq(collection_title)
      expect(batch.status).to eq(Ddr::Batch::Batch::STATUS_READY)

      # All batch object expectations
      batch_objects.each do |obj|
        expect(obj.type).to eq('Ddr::Batch::UpdateBatchObject')
      end

      # Collection expectations
      expect(collections.count).to eq(coll_count)

      # Item expectations
      expect(items.count).to eq(2)
      item_titles = []
      item_descriptions = []
      items.each do |obj|
        expect(obj.id).to be_present
        if a = obj.batch_object_attributes.where(name: 'title', operation: 'ADD').first
          item_titles << a.value
        end
        obj.batch_object_attributes.where(name: 'description', operation: 'ADD').each do |a|
          item_descriptions << a.value
        end
      end
      expect(item_titles).to match_array(expected_item_titles)
      expect(item_descriptions).to match_array(expected_item_descriptions)

      # Component expectations
      expect(components.count).to eq(1)
      component_filepaths = []
      component_checksums = []
      components.each do |obj|
        # Content datastream
        content_datastreams = obj.batch_object_datastreams.where(name: 'content')
        expect(content_datastreams.size).to eq(1)
        expect(content_datastreams.first.checksum_type).to eq(Ddr::Files::CHECKSUM_TYPE_SHA1)
        component_filepaths << content_datastreams.first.payload
        component_checksums << content_datastreams.first.checksum
      end
      expect(component_filepaths).to include('/opt/app-root/spec/fixtures/abc001001.tif')
      expect(component_checksums).to include('2cf23f0035c12b6242093e93d0f7eeba0b1e08e8')
    end
  end
end

module Ddr
  RSpec.describe BuildBatchesFromManifestBasedIngest, :batch, :ingest, type: :service do
    let(:volumes) { { '/path/to/spec/fixtures' => Rails.root.join('spec/fixtures').to_s } }
    let(:user) { FactoryBot.create(:user) }

    context 'manifest based ingest' do
      let(:filepath) do
        Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/manifest.txt').to_s
      end
      let(:manifest) { Ddr::Manifest.new(filepath, volumes) }
      let(:admin_set) { 'foo' }
      let(:collection_title) { 'My Collection' }
      let(:batch_name) { 'Manifest-Based Ingest' }

      context 'collection id not provided' do
        let(:batch_builder) do
          described_class.new(user:, manifest:, admin_set:,
                              collection_title:, batch_name:)
        end

        it_behaves_like 'a successfully built manifest based ingest batch' do
          let(:batches) { batch_builder.call }
          let(:coll_count) { 1 }
          let(:coll_id) { batches.first.collection_id }
        end
      end

      context 'collection id provided' do
        let(:collection_id) { Valkyrie::ID.new(SecureRandom.uuid).id }
        let(:collection) { Collection.new(id: collection_id, title: collection_title) }

        let(:batch_builder) do
          described_class.new(user:, manifest:, admin_set:,
                              collection_repo_id: collection_id, collection_title:,
                              batch_name:)
        end

        before do
          allow_any_instance_of(Ddr::Batch::Batch).to receive(:found_resource_ids) { { collection_id => 'Collection' } }
          allow(Ddr.query_service).to receive(:find_by).with(id: collection_id) { collection }
        end

        it_behaves_like 'a successfully built manifest based ingest batch' do
          let(:batches) { batch_builder.call }
          let(:coll_count) { 0 }
          let(:coll_id) { collection_id }
        end
      end
    end

    context 'manifest based update' do
      let(:filepath) do
        Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/update_manifest.txt').to_s
      end
      let(:manifest) { Ddr::Manifest.new(filepath, volumes) }
      let(:admin_set) { 'foo' }
      let(:collection_title) { 'My Collection' }
      let(:batch_name) { 'Manifest-Based Update' }
      let(:batch_builder) do
        described_class.new(user:, manifest:, admin_set:,
                            collection_title:, batch_name:)
      end

      it_behaves_like 'a successfully built manifest based update batch' do
        let(:batches) { batch_builder.call }
        let(:coll_count) { 0 }
        let(:coll_id) { batches.first.collection_id }
      end
    end
  end
end
