module Ddr
  RSpec.describe SetDefaultStructure, type: :service do
    subject { described_class.new(repo_id) }

    let(:repo_id) { Valkyrie::ID.new(SecureRandom.uuid) }

    before do
      allow(Ddr.query_service).to receive(:find_by) { resource }
    end

    describe '.call' do
      let(:notification_args) { ['test', DateTime.now - 2.seconds, DateTime.now - 1.second, 'testid', payload] }
      let(:payload) { { resource_id: repo_id, skip_structure_updates: skip_updates } }

      around do |example|
        prev_auto_update_structures = Ddr::Admin.auto_update_structures
        Ddr::Admin.auto_update_structures = true
        example.run
        Ddr::Admin.auto_update_structures = prev_auto_update_structures
      end

      describe 'skip structure updates' do
        let(:skip_updates) { true }

        it 'does not instantiate the service' do
          expect(described_class).not_to receive(:new)
          SetDefaultStructure.call(*notification_args)
        end
      end

      describe 'do not skip structure updates' do
        let(:skip_updates) { false }

        it 'does not instantiate the service' do
          expect(described_class).to receive(:new) { double('SetDefaultStructure', enqueue_default_structure_job: nil) }
          SetDefaultStructure.call(*notification_args)
        end
      end
    end

    describe '#default_structure_needed?' do
      describe 'can have structural metadata' do
        describe 'no existing structural metadata' do
          let(:resource) { double(can_have_struct_metadata?: true, has_struct_metadata?: false) }

          its(:default_structure_needed?) { is_expected.to be true }
        end

        describe 'existing structural metadata' do
          let(:resource) { double(can_have_struct_metadata?: true, has_struct_metadata?: true) }

          describe 'repository maintained' do
            before { allow(resource).to receive_message_chain(:structure, :repository_maintained?) { true } }

            its(:default_structure_needed?) { is_expected.to be true }
          end

          describe 'not repository maintained' do
            before { allow(resource).to receive_message_chain(:structure, :repository_maintained?) { false } }

            its(:default_structure_needed?) { is_expected.to be false }
          end
        end
      end

      describe 'cannot have structural metadata' do
        let(:resource) { double(can_have_struct_metadata?: false) }

        its(:default_structure_needed?) { is_expected.to be false }
      end

      describe 'generate structure job' do
        let(:resource) { double(Ddr::Resource, id: repo_id) }

        before { allow(subject).to receive(:default_structure_needed?).and_return(true) }

        it 'enqueues a job' do
          expect(GenerateDefaultStructureJob).to receive(:perform_later).with(repo_id.id)
          subject.enqueue_default_structure_job
        end
      end
    end
  end
end
