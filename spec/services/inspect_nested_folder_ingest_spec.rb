require 'rails_helper'
require 'support/ingest_folder_helper'

module Ddr
  RSpec.describe InspectNestedFolderIngest, :batch, :nested_folder_ingest, type: :service do
    let(:filepath) { '/foo/bar' }
    let(:inspect_nested_folder_ingest) do
      InspectNestedFolderIngest.new(filepath, nested_folder_ingest_configuration[:scanner])
    end
    let(:filesystem) { Filesystem.new }
    let(:scan_results) { ScanFilesystem::Results.new(filesystem, []) }

    before do
      allow(Dir).to receive(:exist?).and_return(true)
      allow(::File).to receive(:readable?).with(filepath).and_return(true)
      allow_any_instance_of(ScanFilesystem).to receive(:call) { scan_results }
      filesystem.tree = sample_filesystem_without_dot_files
    end

    describe 'nested folder ingest folder' do
      describe 'filepath' do
        context 'valid filepath' do
          it 'does not raise an error' do
            expect { inspect_nested_folder_ingest.call }.not_to raise_error
          end
        end

        context 'filepath does not point to an existing directory' do
          before { allow(Dir).to receive(:exist?).and_return(false) }

          it 'raises a not found or not directory error' do
            expect do
              inspect_nested_folder_ingest.call
            end.to raise_error(Ddr::Batch::Error, /not found or is not a directory/)
          end
        end

        context 'filepath is not readable' do
          before do
            allow(::File).to receive(:readable?).with(filepath).and_return(false)
          end

          it 'raises a not readable error' do
            expect { inspect_nested_folder_ingest.call }.to raise_error(Ddr::Batch::Error, /not readable/)
          end
        end
      end
    end

    describe 'filesystem' do
      context 'valid for nested folder ingest' do
        it 'reports the number of files' do
          expect(inspect_nested_folder_ingest.call.file_count).to eq(6)
        end

        it 'reports the excluded files/folders' do
          expect(inspect_nested_folder_ingest.call.exclusions).to eq([])
        end

        it 'reports the content model stats' do
          stats = inspect_nested_folder_ingest.call.content_model_stats
          expect(stats).to include(collections: 1)
          expect(stats).to include(items: 6)
          expect(stats).to include(components: 6)
        end

        it 'reports the filesystem object' do
          expect(inspect_nested_folder_ingest.call.filesystem).to be_a(Filesystem)
        end
      end
    end
  end
end
