require 'rails_helper'
require 'fileutils'

module Ddr
  module ExportFiles
    RSpec.describe Package do
      let(:basename) { SecureRandom.hex(4) }

      after do
        Dir["#{Ddr::Admin.export_files_store}/*"].each { |ent| FileUtils.remove_entry_secure(ent, true) }
      end

      it 'works' do
        role = FactoryBot.build(:role, :public, :downloader, :policy)
        coll = create_for_repository(:collection, access_role: role)
        item = create_for_repository(:item, parent_id: coll.id, nested_path: 'foobar/baz.pdf')
        comp = create_for_repository(:component, :with_content_file, :with_thumbnail_file, parent_id: item.id)

        described_class.call(identifiers: [comp.id.to_s], basename:, files: %w[content thumbnail])

        package_dir = ::File.join(Ddr::Admin.export_files_store, basename)
        data_dir = ::File.join(package_dir, 'data')
        metadata_file = ::File.join(data_dir, 'METADATA.csv')
        checksum_file = ::File.join(package_dir, 'manifest-sha1.txt')
        objects_dir = ::File.join(data_dir, 'objects')
        parent_dir = ::File.join(objects_dir, 'foobar')
        content_file = ::File.join(parent_dir, comp.content.original_filename)
        thumbnail_file = ::File.join(parent_dir, "#{comp.id}_thumbnail.png")

        expect(Dir.exist?(package_dir)).to be true
        expect(Dir.exist?(data_dir)).to be true
        expect(Dir.exist?(objects_dir)).to be true
        expect(Dir.exist?(parent_dir)).to be true

        expect(::File.exist?(checksum_file)).to be true
        expect(::File.exist?(metadata_file)).to be true
        expect(::File.exist?(content_file)).to be true
        expect(::File.exist?(thumbnail_file)).to be true
      end
    end
  end
end
