module Ddr
  RSpec.describe MonitorMetadataFile, type: :service do
    let(:user_key) { 'joe@test.edu' }
    let(:user_email) { 'joe.test@test.edu' }
    let(:user) { double('User', email: user_email, user_key:) }
    let(:metadata_file_id) { 1 }
    let(:metadata_file_name) { 'metadata.csv' }
    let(:metadata_file) do
      double('Ddr::MetadataFile', id: metadata_file_id, metadata_file_name:, user:)
    end
    let(:batch_id) { 7 }
    let(:batch) { double('Ddr::Batch::Batch', id: batch_id) }
    let(:batch_url) do
      Rails.application.routes.url_helpers.ddr_batch_url(batch, host: Ddr::Admin.application_hostname,
                                                                protocol: 'https')
    end
    let(:notification) { [MetadataFile::FINISHED, Time.now, Time.now, 'abcdef', payload] }
    let(:payload) { { metadata_file_id:, batch_id: } }
    let(:expected_msg) do
      <<~EOS
        Metadata File Upload has created batch ##{batch.id}
        From file: #{metadata_file_name}

        To review and process the batch, go to #{batch_url}
      EOS
    end

    before do
      allow(MetadataFile).to receive(:find).with(metadata_file_id).and_return(metadata_file)
      allow(Batch::Batch).to receive(:find).with(batch_id).and_return(batch)
    end

    describe '#call' do
      it 'generates an appropriate email' do
        expect(JobMailer).to receive(:basic).with(to: user_email,
                                                  subject: "BATCH CREATED - Metadata File Upload - #{metadata_file_name}",
                                                  message: expected_msg).and_call_original
        described_class.call(*notification)
      end
    end
  end
end
