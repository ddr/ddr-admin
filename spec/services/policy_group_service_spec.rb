module Ddr
  RSpec.describe PolicyGroupService do
    let(:group_name) { 'duke:policies:duke-digital-repository:repo_group' }
    let(:group_ext) { 'Repository Group' }
    let(:group) { Ddr::Auth::Group.new(group_name, label: group_ext) }

    describe '.repository_groups' do
      subject { described_class.repository_groups }

      before do
        allow(described_class).to receive(:request) do
          { 'WsFindGroupsResults' => { 'groupResults' => [{ 'name' => group_name, 'displayExtension' => group_ext,
                                                            'typeOfGroup' => 'group' }] } }
        end
      end

      it { is_expected.to include(group) }
    end

    describe '.user_groups' do
      subject { described_class.user_groups(user) }

      let(:user) { ::User.new(duid: '1234') }

      before do
        allow(described_class).to receive(:request) do
          { 'WsGetGroupsResults' => { 'results' => [{ 'wsGroups' => [{ 'name' => group_name, 'displayExtension' => group_ext,
                                                                       'typeOfGroup' => 'group' }] }] } }
        end
      end

      it { is_expected.to include(group) }
    end

    describe '.group_members' do
      subject { described_class.group_members(group_name) }

      before do
        allow(described_class).to receive(:request).and_return({ 'WsGetMembersResults' => { 'results' => [{ 'wsSubjects' => [{ 'attributeValues' => ['Foo Bar'], 'id' => '1234',
                                                                                                                               'sourceId' => 'jndiperson' }] }] } })
      end

      it { is_expected.to include({ 'id' => '1234', 'name' => 'Foo Bar', 'type' => 'person' }) }
    end
  end
end
