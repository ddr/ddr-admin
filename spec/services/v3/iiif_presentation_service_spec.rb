module Ddr::V3
  RSpec.describe IiifPresentationService do
    let(:collection) do
      create_for_repository(:collection,
                            permanent_id: 'ark:/99999/1234',
                            permanent_url: 'http://example.com/ark:/99999/1234',
                            title: 'Collection',
                            workflow_state: 'published')
    end
    let(:item) do
      create_for_repository(:item,
                            parent_id: collection.id,
                            permanent_id: 'ark:/99999/12345',
                            permanent_url: 'http://example.com/ark:/99999/12345',
                            title: 'Item',
                            workflow_state: 'published')
    end
    let(:component) do
      create_for_repository(:component, :with_content_file,
                            parent_id: item.id,
                            permanent_id: 'ark:/99999/123456',
                            permanent_url: 'http://example.com/ark:/99999/123456',
                            title: 'Component 1',
                            workflow_state: 'published')
    end

    subject { described_class.call(resource:) }

    before { component }

    describe 'for collection' do
      let(:resource) { collection }

      its(['type']) { is_expected.to eq 'Collection' }
    end

    describe 'for item' do
      let(:resource) { item }

      its(['type']) { is_expected.to eq 'Manifest' }
    end
  end
end
