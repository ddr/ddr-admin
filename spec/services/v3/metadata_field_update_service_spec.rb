module Ddr::V3
  RSpec.describe MetadataFieldUpdateService do
    let(:collection) { create_for_repository(:collection, admin_set: 'dc', permanent_id: 'ark:/99999/fk41234') }
    let(:item) { create_for_repository(:item, parent_id: collection.id, permanent_id: 'ark:/99999/fk412345') }
    let(:component) { create_for_repository(:component, parent_id: item.id, permanent_id: 'ark:/99999/fk4123456') }

    let!(:resource_ids) { [collection, item, component] }

    it 'sets the property' do
      expect {
        described_class.bulk(resource_ids:, field_name: :workflow_state, value: 'published')
      }.to change { Ddr.find_resource(collection).workflow_state }.to('published')
             .and change { Ddr.find_resource(item).workflow_state }.to('published')
             .and change { Ddr.find_resource(component).workflow_state }.to('published')
    end

    it 'retains other properties' do
      expect {
        described_class.bulk(resource_ids:, field_name: :workflow_state, value: 'published')
      }.not_to change { Ddr.find_resource(collection).permanent_id }.from('ark:/99999/fk41234')
    end
  end
end
