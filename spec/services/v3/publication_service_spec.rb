module Ddr::V3
  RSpec.describe PublicationService, :service, :v3 do
    let(:collection) do
      create_for_repository(:collection, permanent_id: ark1, permanent_url: purl1, admin_set: 'dc', workflow_state:)
    end

    let(:item) do
      create_for_repository(:item, parent_id: collection.id, permanent_id: ark2, permanent_url: purl2,
                                   admin_policy_id: collection.id, workflow_state:)
    end

    let(:component) do
      create_for_repository(:component, :with_content_file, parent_id: item.id,
                            permanent_id: ark3, permanent_url: purl3, admin_policy_id: collection.id, workflow_state:)
    end

    let(:workflow_state) { 'unpublished' }

    let(:iiif_file) { FactoryBot.build(:ddr_file, :iiif) }
    let(:ark1) { 'ark:/fk499999/12345a' }
    let(:purl1) { "https://idn.duke.edu/#{ark1}" }
    let(:ark2) { 'ark:/fk499999/12345b' }
    let(:purl2) { "https://idn.duke.edu/#{ark2}" }
    let(:ark3) { 'ark:/fk499999/12345c' }
    let(:purl3) { "https://idn.duke.edu/#{ark3}" }

    before do
      allow(ArkUpdateService).to receive(:call)
      allow(ArkAssignmentService).to receive(:call)
    end

    describe '.call' do
      let(:resource) { collection }

      describe 'with a valid event' do
        it 'works' do
          expect { described_class.call(event: :publish, resource:) }
            .to change { Ddr.query_service.find_by(id: resource.id).workflow_state }.to('published')
        end
      end

      describe 'with an invalid event' do
        it 'raises an exception' do
          expect { described_class.call(event: :foobar, resource:) }.to raise_error(ArgumentError)
        end
      end

      describe 'async' do
        # TODO
      end
    end

    describe '.transition_to' do
      let(:resource) { collection }

      describe 'with a valid transition' do
        let(:workflow_state) { 'unpublished' }

        it 'works' do
          expect { described_class.transition_to(state: 'published', resource:) }
            .to change { Ddr.query_service.find_by(id: resource.id).workflow_state }.to('published')
        end
      end

      describe 'with an invalid transition' do
        let(:workflow_state) { 'published' }

        it 'skips the publication event' do
          expect do
            described_class.transition_to(state: 'previewable', resource:)
          end.not_to(change { Ddr.find_resource(resource).workflow_state })
        end
      end

      describe 'with a no-op transition' do
        let(:workflow_state) { 'unpublished' }

        it 'is a no-op' do
          expect { described_class.transition_to(state: 'unpublished', resource:) }
            .not_to(change { Ddr.query_service.find_by(id: resource.id).workflow_state })
        end
      end

      describe 'with an invalid workflow state' do
        let(:workflow_state) { 'published' }

        it 'raises an exception' do
          expect { described_class.transition_to(state: 'foobar', resource:) }.to raise_error(ArgumentError)
        end
      end

      describe 'async' do
        let(:workflow_state) { 'unpublished' }

        # TODO
      end
    end

    describe '.publish' do
      before { component }

      describe 'with an unpublished collection' do
        it 'cannot publish an item first' do
          expect { described_class.publish(item) }.not_to(change { Ddr.find_resource(item).workflow_state })
        end

        it 'publishes the collection and its members' do
          expect do
            described_class.publish(collection)
          end.to change { Ddr.query_service.find_by(id: collection.id).workflow_state }.to('published')
                                                                                       .and change {
                                                                                              Ddr.query_service.find_by(id: item.id).workflow_state
                                                                                            }.to('published')
                                                                                             .and change {
                                                                                                    Ddr.query_service.find_by(id: component.id).workflow_state
                                                                                                  }.to('published')
        end

        it 'updates IIIF for the collection and its items' do
          expect do
            described_class.publish(collection)
          end.to change { Ddr.find_resource(collection).iiif_file }
            .and(change { Ddr.find_resource(item).iiif_file })
        end

        it 'updates the ARKs for the collection and its members' do
          described_class.publish(collection)
          expect(ArkUpdateService).to have_received(:call).thrice
        end

        describe 'with a non-publishable item' do
          let(:item) do
            create_for_repository(:item, parent_id: collection.id, workflow_state: 'nonpublishable',
                                         permanent_id: ark2, permanent_url: purl2, admin_policy_id: collection.id)
          end

          it 'publishes the collection' do
            expect do
              described_class.publish(collection)
            end.to change { Ddr.query_service.find_by(id: collection.id).workflow_state }.to('published')
          end

          it 'does not publish the non-publishable item' do
            expect do
              described_class.publish(collection)
            end.not_to(change { Ddr.query_service.find_by(id: item.id).workflow_state })
            expect do
              described_class.publish(collection)
            end.not_to(change { Ddr.query_service.find_by(id: component.id).workflow_state })
          end
        end
      end

      describe 'with a published collection' do
        let(:collection) do
          create_for_repository(:collection, workflow_state: 'published', iiif_file:,
                                             permanent_id: ark1, permanent_url: purl1)
        end

        it 'does not change the published status of the collection' do
          expect do
            described_class.publish(collection)
          end.not_to(change { Ddr.query_service.find_by(id: collection.id).workflow_state })
        end

        it 'publishes the collection members' do
          expect do
            described_class.publish(collection)
          end.to change { Ddr.query_service.find_by(id: item.id).workflow_state }.to('published')
                                                                                 .and change {
                                                                                        Ddr.query_service.find_by(id: component.id).workflow_state
                                                                                      }.to('published')
        end

        it 'updates IIIF for the collection and items' do
          expect do
            described_class.publish(collection)
          end.to change { Ddr.find_resource(collection).iiif_file }
            .and(change { Ddr.find_resource(item).iiif_file })
        end

        describe 'and an unpublished item' do
          it 'updates IIIF for the collection when the item is published' do
            expect { described_class.publish(item) }.to(change { Ddr.find_resource(collection).iiif_file })
          end
        end
      end

      describe 'with a non-publishable collection' do
        let(:collection) do
          create_for_repository(:collection, workflow_state: 'nonpublishable',
                                             permanent_id: ark1, permanent_url: purl1)
        end

        it 'raises an exception' do
          expect { described_class.publish(collection) }.not_to(change { Ddr.find_resource(collection).workflow_state })
        end
      end
    end

    describe '.preview' do
      describe 'with an unpublished collection' do
        it 'cannot preview an item first' do
          expect { described_class.preview(item) }.not_to(change { Ddr.find_resource(item).workflow_state })
        end

        it 'makes the collection and its members previewable' do
          expect do
            described_class.preview(collection)
          end.to change { Ddr.query_service.find_by(id: collection.id).workflow_state }.to('previewable')
                   .and change {
            Ddr.query_service.find_by(id: item.id).workflow_state
          }.to('previewable')
                          .and change {
            Ddr.query_service.find_by(id: component.id).workflow_state
          }.to('previewable')
        end
      end
    end

    describe '.unpublish' do
      describe 'with a published collection' do
        let(:collection) do
          create_for_repository(:collection, workflow_state: 'published', iiif_file:,
                                             permanent_id: ark1, permanent_url: purl1)
        end
        let(:item) do
          create_for_repository(:item, parent_id: collection.id, workflow_state: 'published', iiif_file:,
                                       permanent_id: ark2, permanent_url: purl2)
        end
        let!(:component) do
          create_for_repository(:component, :with_content_file, parent_id: item.id, workflow_state: 'published',
                                                                permanent_id: ark3, permanent_url: purl3)
        end

        it 'unpublishes the collection and its members' do
          expect do
            described_class.unpublish(collection)
          end.to change { Ddr.query_service.find_by(id: collection.id).workflow_state }.to('unpublished')
                                                                                       .and change {
                                                                                              Ddr.query_service.find_by(id: item.id).workflow_state
                                                                                            }.to('unpublished')
                                                                                             .and change {
                                                                                                    Ddr.query_service.find_by(id: component.id).workflow_state
                                                                                                  }.to('unpublished')
        end

        it 'deletes IIIF for the collection and its items' do
          allow(FileDeletionService[:iiif_file]).to receive(:call).twice
          described_class.unpublish(collection)
          expect(FileDeletionService[:iiif_file]).to have_received(:call).twice
        end

        it 'updates the permanent IDs for the collection and its members' do
          described_class.unpublish(collection)
          expect(ArkUpdateService).to have_received(:call).thrice
        end
      end
    end

    describe '.make_nonpublishable' do
      describe 'given the top-level resource is unpublished' do
        let(:collection) { create_for_repository(:collection, workflow_state: 'unpublished', admin_set: 'dc') }
        let(:item) { create_for_repository(:item, parent_id: collection.id, workflow_state: 'unpublished') }
        let(:component) do
          create_for_repository(:component, :with_content_file, parent_id: item.id, workflow_state: 'unpublished')
        end

        it 'marks nonpublishable an unpublished resource and its descendants' do
          expect do
            described_class.make_nonpublishable(collection)
          end.to change { Ddr.query_service.find_by(id: collection.id).workflow_state }.to('nonpublishable')
                                                                                       .and change {
                                                                                              Ddr.query_service.find_by(id: item.id).workflow_state
                                                                                            }.to('nonpublishable')
                                                                                             .and change {
                                                                                                    Ddr.query_service.find_by(id: component.id).workflow_state
                                                                                                  }.to('nonpublishable')
        end
      end

      describe 'given the top-level resource is published' do
        let(:collection) do
          create_for_repository(:collection, workflow_state: 'published', iiif_file:,
                                             permanent_id: ark1, permanent_url: purl1)
        end

        it 'raises an exception' do
          expect { described_class.make_nonpublishable(collection) }.not_to(change do
                                                                              Ddr.find_resource(collection).workflow_state
                                                                            end)
        end
      end
    end
  end
end
