module Ddr::V3
  module FileUpdateService
    RSpec.describe FitsFile, type: :service do
      let(:resource) { create_for_repository(:component, :with_content_file) }
      let(:fits_xml) { ::File.read(Rails.root.join('spec/fixtures/files/fits.xml')) }

      before do
        allow(Ddr::V3::FileCharacterizationService).to receive(:call).and_return(fits_xml)
      end

      it 'adds a fits file to the resource' do
        expect {
          described_class.call(resource:)
        }.to change { Ddr.query_service.find_by(id: resource.id).fits_file }
      end
    end
  end
end
