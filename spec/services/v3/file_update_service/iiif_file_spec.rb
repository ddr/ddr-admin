module Ddr::V3
  module FileUpdateService
    RSpec.describe IiifFile, type: :service, iiif: true do
      let(:collection) do
        create_for_repository(:collection,
                              permanent_id: 'ark:/99999/1234',
                              permanent_url: 'http://example.com/ark:/99999/1234',
                              title: 'Collection',
                              workflow_state: 'published')
      end
      let(:item) do
        create_for_repository(:item,
                              parent_id: collection.id,
                              permanent_id: 'ark:/99999/12345',
                              permanent_url: 'http://example.com/ark:/99999/12345',
                              title: 'Item',
                              workflow_state: 'published')
      end
      let(:component) do
        create_for_repository(:component, :with_content_file,
                              parent_id: item.id,
                              permanent_id: 'ark:/99999/123456',
                              permanent_url: 'http://example.com/ark:/99999/123456',
                              title: 'Component 1',
                              workflow_state: 'published')
      end

      before do
        allow(ArkUpdateService).to receive(:call)
        allow(ArkAssignmentService).to receive(:call)
      end

      describe 'when a collection has no children' do
        it 'does updates IIIF' do
          expect { described_class.call(resource: collection) }.not_to(change { Ddr.find_resource(collection).iiif_file })
        end
      end

      describe 'when an item has no children' do
        it 'does not update IIIF' do
          expect { described_class.call(resource: item) }.not_to(change { Ddr.find_resource(item).iiif_file })
        end
      end

      # @deprecated
      describe '.handle_notification_event' do
        before { component }

        describe 'for a collection' do
          let(:payload) { { resource_id: collection.id, model: 'Ddr::Collection', attributes_changed: ['title'] } }

          it 'updates the IIIF' do
            expect do
              described_class.handle_notification_event('update.ddr_resource', Time.now, Time.now, 1, payload)
            end.to(change { Ddr.query_service.find_by(id: collection.id).iiif_file })
          end
        end

        describe 'for an item' do
          describe 'when the change should not cause an update to the parent' do
            let(:payload) { { resource_id: item.id, model: 'Ddr::Item', attributes_changed: ['title'] } }

            it 'does not update the IIIF of the collection' do
              expect do
                described_class.handle_notification_event('update.ddr_resource', Time.now, Time.now, 1, payload)
              end.not_to(change { Ddr.query_service.find_by(id: collection.id).iiif_file })
            end
          end

          describe 'when the change should cause an update' do
            let(:payload) { { resource_id: item.id, model: 'Ddr::Item', attributes_changed: ['local_id'] } }

            it 'updates the IIIF of the item and collection' do
              expect do
                described_class.handle_notification_event('update.ddr_resource', Time.now, Time.now, 1, payload)
              end.to change { Ddr.query_service.find_by(id: collection.id).iiif_file }
                       .and(change { Ddr.query_service.find_by(id: item.id).iiif_file })
            end
          end
        end

        describe 'for a component' do
          describe 'when the change should not cause an update' do
            let(:payload) do
              { resource_id: component.id, model: 'Ddr::Component', parent: item.id.id,
                attributes_changed: ['description'] }
            end

            it 'does not update the IIIF' do
              expect do
                described_class.handle_notification_event('update.ddr_resource', Time.now, Time.now, 1, payload)
              end.not_to(change { Ddr.query_service.find_by(id: item.id).iiif_file })
            end
          end

          describe 'when the change should cause an update' do
            let(:payload) do
              { resource_id: component.id, model: 'Ddr::Component', parent: item.id.id, attributes_changed: ['title'] }
            end

            it 'updates the IIIF' do
              expect do
                described_class.handle_notification_event('update.ddr_resource', Time.now, Time.now, 1, payload)
              end.to(change { Ddr.query_service.find_by(id: item.id).iiif_file })
            end
          end
        end
      end
    end
  end
end
