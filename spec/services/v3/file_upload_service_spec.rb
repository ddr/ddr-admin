module Ddr::V3
  RSpec.describe FileUploadService do
    let(:file) { fixture_file_upload('imageA.tif', 'image/tiff') }
    let(:resource) { create_for_repository(:component) }

    before do
      allow(ArkUpdateService).to receive(:call)
      allow(ArkAssignmentService).to receive(:call)
    end

    describe 'with a stale resource' do
      xit 'retries on a stale object error' do
        service = described_class[:content].new(resource:, file:)
        allow(service.change_set).to receive(:save).and_call_original
        resource.title = ['Updated']
        Ddr.persister.save(resource:)
        service.run!
        expect(service.change_set).to have_received(:save).twice
      end
    end

    describe ':content' do
      it 'raises an error if the file digest is not valid' do
        expect do
          described_class[:content].call(resource:, file:,
                                         digest: Ddr::Digest.new(value: '12f6fa870dc7a4567a024dc545909fffeee075b3'))
        end.to raise_error(Ddr::ChecksumInvalid)
      end

      it 'triggers file characterization' do
        expect do
          described_class[:content].call(resource:, file:)
        end.to(change { Ddr.find_resource(resource).fits_file })
      end

      it 'triggers image derivative generation' do
        allow(ImageDerivativesUpdateService).to receive(:call)
        described_class[:content].call(resource:, file:)
        expect(ImageDerivativesUpdateService).to have_received(:call)
      end

      describe 'identical content' do
        let!(:resource) { create_for_repository(:component, :with_content_file) }

        xit 'does not update when an identical file is uploaded' do
          expect {
            described_class[:content].call(resource:, file:)
          }.not_to change { Ddr.find_resource(resource).content.file_identifier }
        end
      end

      describe 'malware scanning' do
        before do
          allow(Ddr::Antivirus).to receive(:scan).and_call_original
        end

        it 'scans the file for malware' do
          allow(Ddr::Antivirus).to receive(:scan).with(file.path)
          described_class[:content].call(resource:, file:)
          expect(Ddr::Antivirus).to have_received(:scan).with(file.path)
        end

        it 'is OK if the file is too big' do
          allow(Ddr::Antivirus).to receive(:scan).with(file.path).and_raise(Ddr::Antivirus::MaxFileSizeExceeded)
          expect { described_class[:content].call(resource:, file:) }.not_to raise_error
        end

        it 'quarantines the file if a virus is detected and raises the error' do
          allow(Ddr::Antivirus).to receive(:scan).with(file.path).and_raise(Ddr::Antivirus::VirusFoundError.new('oof'))
          allow(FileUtils).to receive(:rm_f).with(file.path).and_call_original
          expect { described_class[:content].call(resource:, file:) }.to raise_error(Ddr::Antivirus::VirusFoundError)
          expect(FileUtils).to have_received(:rm_f).with(file.path)
        end

        it 'moves a quarantined file to a directory if configured' do
          allow(Ddr::Antivirus).to receive(:scan).with(file.path).and_raise(Ddr::Antivirus::VirusFoundError.new('oof'))

          Dir.mktmpdir do |quarantine|
            allow(ENV).to receive(:[]).and_call_original
            allow(ENV).to receive(:[]).with('QUARANTINE_PATH').and_return(quarantine)
            allow(FileUtils).to receive(:mv).with(file.path, quarantine).and_call_original
            described_class[:content].call(resource:, file:)
            expect(FileUtils).to have_received(:mv).with(file.path, quarantine)
          end
        rescue Ddr::Antivirus::VirusFoundError => _e
        end
      end
    end
  end
end
