module Ddr::V3
  RSpec.describe ImageDerivativesUpdateService, type: :service do
    before do
      allow(ArkUpdateService).to receive(:call)
      allow(ArkAssignmentService).to receive(:call)
    end

    describe 'with a Target resource' do
      let(:resource) { create_for_repository(:target) }

      it 'creates a thumbnail' do
        expect { described_class.call(resource:) }
          .to(change { Ddr.query_service.find_by(id: resource.id).thumbnail })
      end
    end

    describe 'with a Component resource' do
      context 'content is not an image' do
        let(:resource) { create_for_repository(:component, :with_pdf_content_file) }

        it 'does not create a thumbnail' do
          expect do
            described_class.call(resource:)
          end.not_to(change { Ddr.query_service.find_by(id: resource.id).thumbnail })
        end

        it 'does not create a multires image' do
          expect do
            described_class.call(resource:)
          end.not_to(change { Ddr.query_service.find_by(id: resource.id).multires_image })
        end

        it 'does not create a derived image' do
          expect do
            described_class.call(resource:)
          end.not_to(change { Ddr.query_service.find_by(id: resource.id).derived_image })
        end
      end

      context 'content is an image' do
        context 'content is tiff image' do
          let(:resource) { create_for_repository(:component, :with_content_file) }

          it 'creates derivatives' do
            expect do
              described_class.call(resource:)
            end.to change { Ddr.query_service.find_by(id: resource.id).thumbnail }
              .and change { Ddr.query_service.find_by(id: resource.id).multires_image }
              .and(change { Ddr.query_service.find_by(id: resource.id).derived_image })
          end
        end

        context 'content is a jpeg image' do
          let(:resource) do
            create_for_repository(:component).tap do |resource|
              resource.content = FactoryBot.build(:ddr_file, :jpg)
              resource = Ddr.persister.save(resource:)
            end
          end

          it 'creates derivatives' do
            expect do
              described_class.call(resource:)
            end.to change { Ddr.query_service.find_by(id: resource.id).thumbnail }
              .and change { Ddr.query_service.find_by(id: resource.id).multires_image }
              .and(change { Ddr.query_service.find_by(id: resource.id).derived_image })
          end
        end

        context 'content is a gif image' do
          let(:resource) do
            create_for_repository(:component).tap do |resource|
              resource.content = FactoryBot.build(:ddr_file, :gif)
              resource = Ddr.persister.save(resource:)
            end
          end

          it 'creates derivatives' do
            expect do
              described_class.call(resource:)
            end.to change { Ddr.query_service.find_by(id: resource.id).thumbnail }
              .and change { Ddr.query_service.find_by(id: resource.id).multires_image }
              .and(change { Ddr.query_service.find_by(id: resource.id).derived_image })
          end
        end

        context 'content is a png image' do
          let(:resource) do
            create_for_repository(:component).tap do |resource|
              resource.content = FactoryBot.build(:ddr_file, :png)
              resource = Ddr.persister.save(resource:)
            end
          end

          it 'creates derivatives' do
            expect do
              described_class.call(resource:)
            end.to change { Ddr.query_service.find_by(id: resource.id).thumbnail }
              .and change { Ddr.query_service.find_by(id: resource.id).multires_image }
              .and(change { Ddr.query_service.find_by(id: resource.id).derived_image })
          end
        end

        context 'content is a bmp image' do
          let(:resource) do
            create_for_repository(:component).tap do |resource|
              resource.content = FactoryBot.build(:ddr_file, :bmp)
              resource = Ddr.persister.save(resource:)
            end
          end

          it 'creates derivatives' do
            expect do
              described_class.call(resource:)
            end.to change { Ddr.query_service.find_by(id: resource.id).thumbnail }
              .and change { Ddr.query_service.find_by(id: resource.id).multires_image }
              .and(change { Ddr.query_service.find_by(id: resource.id).derived_image })
          end
        end

        context 'content is an image not intended for ptif conversion' do
          let!(:resource) do
            create_for_repository(:component).tap do |resource|
              resource.content = FactoryBot.build(:ddr_file, :ico)
              resource = Ddr.persister.save(resource:)
            end
          end

          it 'creates a thumbnail' do
            expect do
              described_class.call(resource:)
            end.to(change { Ddr.query_service.find_by(id: resource.id).thumbnail })
          end

          it 'does not create a multires image' do
            expect do
              described_class.call(resource:)
            end.not_to(change { Ddr.query_service.find_by(id: resource.id).multires_image })
          end

          it 'does not create a derived image' do
            expect do
              described_class.call(resource:)
            end.not_to(change { Ddr.query_service.find_by(id: resource.id).derived_image })
          end
        end

        describe 'intermediate file handling' do
          let(:resource) do
            create_for_repository(:component, :with_content_file).tap do |resource|
              resource.intermediate_file = FactoryBot.build(:ddr_file, :tiff)
              resource = Ddr.persister.save(resource:)
            end
          end

          it 'uses the intermediate file as the source for the thumbnail and multires image' do
            expect do
              described_class.call(resource:)
            end.to change { Ddr.query_service.find_by(id: resource.id).thumbnail }
              .and change { Ddr.query_service.find_by(id: resource.id).multires_image }
          end

          xit 'does not use the intermediate file as the source for the derived image' do
            expect do
              described_class.call(resource:)
            end.not_to change { Ddr.query_service.find_by(id: resource.id).derived_image }
          end
        end
      end
    end
  end
end
