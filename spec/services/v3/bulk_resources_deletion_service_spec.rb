module Ddr::V3
  RSpec.describe ResourceDeletionService::Bulk do
    let(:collection) { create_for_repository(:collection) }
    let(:item_no_children) { create_for_repository(:item, parent_id: collection.id) }
    let(:item_with_children) { create_for_repository(:item, parent_id: collection.id) }
    let(:component) { create_for_repository(:component, parent_id: item_with_children.id) }
    let(:attachment) { create_for_repository(:attachment, attached_to_id: collection.id) }
    let(:target) { create_for_repository(:target, for_collection_id: collection.id) }

    before do
      allow(ArkUpdateService).to receive(:call)
      allow(ArkAssignmentService).to receive(:call)
    end

    it 'creates a DeletionEvent' do
      expect do
        described_class.call(resource_ids: [collection])
      end.to change { Ddr::Events::DeletionEvent.count }.by(1)
    end

    describe 'with published resource' do
      let(:collection) { create_for_repository(:collection, workflow_state: 'published') }

      it 'aborts' do
        described_class.call(resource_ids: [collection.id])
        expect { Ddr.query_service.find_by(id: collection.id) }.not_to raise_error
      end
    end

    describe 'tracking deleted files' do
      before { component }

      let(:component) do
        create_for_repository(:component,
                              :with_content_file,
                              :with_derived_image_file,
                              :with_multires_image_file,
                              :with_thumbnail_file,
                              parent_id: item_with_children.id)
      end

      it 'tracks deleted files' do
        expect do
          described_class.call(resource_ids: [item_with_children.id])
        end.to change { Ddr::DeletedFile.count }.by(4)
      end
    end

    describe 'ARK deletion' do
      let(:ark) { 'ark:/99999/fk4abcde' }
      let(:collection) { create_for_repository(:collection, permanent_id: ark) }
      let(:identifier) { Ezid::Identifier.new(ark) }

      before do
        identifier.status = status
        allow(Ezid::Identifier).to receive(:find).with(ark).and_return(identifier)
        allow(identifier).to receive(:persisted?).and_return(true)
        allow(identifier).to receive(:delete)
        allow(identifier).to receive(:unavailable!).with('deleted')
      end

      describe 'the ARK is reserved' do
        let(:status) { 'reserved' }

        it 'is deleted' do
          described_class.call(resource_ids: [collection])
          expect(identifier).to have_received(:delete)
        end
      end

      describe 'the ARK is not reserved' do
        let(:status) { 'public' }

        it 'is made unavailable' do
          described_class.call(resource_ids: [collection])
          expect(identifier).to have_received(:unavailable!).with('deleted')
        end
      end
    end

    describe 'called on a collection' do
      before do
        item_with_children
        item_no_children
        component
        attachment
        target
      end

      it 'destroys the collection and its items, components, attachments, and targets' do
        described_class.call(resource_ids: [collection])

        expect do
          Ddr.query_service.find_by(id: collection.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect do
          Ddr.query_service.find_by(id: item_no_children.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect do
          Ddr.query_service.find_by(id: item_with_children.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect do
          Ddr.query_service.find_by(id: component.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect do
          Ddr.query_service.find_by(id: attachment.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect do
          Ddr.query_service.find_by(id: target.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
      end
    end

    describe 'called on non-collection' do
      before do
        item_no_children
        component
        attachment
        target
      end

      it 'destroys the resource and its descendants' do
        described_class.call(resource_ids: [item_with_children])

        expect do
          Ddr.query_service.find_by(id: item_with_children.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect do
          Ddr.query_service.find_by(id: component.id)
        end.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)

        expect { Ddr.query_service.find_by(id: collection.id) }.not_to raise_error
        expect { Ddr.query_service.find_by(id: item_no_children.id) }.not_to raise_error
        expect { Ddr.query_service.find_by(id: attachment.id) }.not_to raise_error
        expect { Ddr.query_service.find_by(id: target.id) }.not_to raise_error
      end

      it 'updates the parent structure' do
        expect do
          described_class.call(resource_ids: [item_no_children])
        end.to(change { Ddr.find_resource(collection).struct_metadata })
      end
    end
  end
end
