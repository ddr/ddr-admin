module Ddr::V3
  RSpec.describe ArkAssignmentService do
    let(:ark) { 'ark:/99999/fk4abcde' }
    let(:identifier) { Ezid::Identifier.new(ark, status: 'reserved', profile: 'dc', export: 'no', 'ddr.resource.id': resource.id_s) }
    let(:resource) { create_for_repository(:item) }

    before do
      allow(Ezid::Identifier).to receive(:mint).and_return(identifier)
      allow(identifier).to receive(:save).and_return(identifier)
    end

    it 'creates an update event' do
      expect { described_class.call(resource:) }.to change(Ddr::Events::UpdateEvent, :count).by(1)
    end

    it 'mints a new identifier' do
      described_class.call(resource:)
      expect(Ezid::Identifier).to have_received(:mint)
    end

    it 'assigns the ark to the resource and sets the permanent URL' do
      expect do
        described_class.call(resource:)
      end.to change { Ddr.query_service.find_by(id: resource.id).permanent_id }.from(nil).to(ark)
                                                                               .and change {
                                                                                      Ddr.query_service.find_by(id: resource.id).permanent_url
                                                                                    }.from(nil).to("https://idn.duke.edu/#{ark}")
    end

    describe 'identifier metadata' do
      subject { identifier }

      before { described_class.call(resource:) }

      its(:target) { is_expected.to eq "https://repository.duke.edu/id/#{ark}" }
      its(:status) { is_expected.to eq 'reserved' }
      its(:profile) { is_expected.to eq 'dc' }
      its(:export) { is_expected.to eq 'no' }
      its(['ddr.resource.id']) { is_expected.to eq(resource.id_s) }
    end
  end
end
