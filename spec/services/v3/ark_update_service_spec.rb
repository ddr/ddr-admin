module Ddr::V3
  RSpec.describe ArkUpdateService do
    let(:ark) { 'ark:/99999/fk4abcde' }
    let(:identifier) { Ezid::Identifier.new(ark, { status: }) }
    let(:resource) { create_for_repository(:item, permanent_id: ark, workflow_state:) }

    before do
      allow(Ezid::Identifier).to receive(:find).with(ark).and_return(identifier)
      allow(identifier).to receive(:save).and_return(identifier)
    end

    describe 'when resource is published' do
      let(:workflow_state) { 'published' }

      describe 'and ark status is not public' do
        let(:status) { 'unavailable | not published' }

        it 'changes the ark status to public' do
          expect do
            described_class.call(resource:)
          end.to change(identifier, :status).to('public')
        end
      end

      describe 'and ark status is public' do
        let(:status) { 'public' }

        it 'does not change the ark status' do
          expect do
            described_class.call(resource:)
          end.not_to change(identifier, :status)
        end
      end
    end

    describe 'when resource is unpublished' do
      let(:workflow_state) { 'unpublished' }

      describe 'and ark status is not public' do
        let(:status) { 'unavailable | not published' }

        it 'does not change the ark status' do
          expect do
            described_class.call(resource:)
          end.not_to change(identifier, :status)
        end
      end

      describe 'and ark status is public' do
        let(:status) { 'public' }

        it 'changes the ark status to unavailable' do
          expect do
            described_class.call(resource:)
          end.to change(identifier, :status).to('unavailable | not published')
        end
      end
    end

    describe 'when previewable' do
      let(:workflow_state) { 'previewable' }

      describe 'and ark status is not public' do
        let(:status) { 'unavailable | not published' }

        it 'does not change the ark status' do
          expect do
            described_class.call(resource:)
          end.not_to change(identifier, :status)
        end
      end

      describe 'and ark status is public' do
        let(:status) { 'public' }

        it 'changes the ark status to unavailable' do
          expect do
            described_class.call(resource:)
          end.to change(identifier, :status).to('unavailable | not published')
        end
      end
    end

    describe 'when nonpublishable' do
      let(:workflow_state) { 'nonpublishable' }

      describe 'and ark status is not public' do
        let(:status) { 'unavailable | not published' }

        it 'does not change the ark status' do
          expect do
            described_class.call(resource:)
          end.not_to change(identifier, :status)
        end
      end

      describe 'and ark status is public' do
        let(:status) { 'public' }

        it 'changes the ark status to unavailable' do
          expect do
            described_class.call(resource:)
          end.to change(identifier, :status).to('unavailable | not published')
        end
      end
    end
  end
end
