module Ddr::V3
  RSpec.describe FixityCheckPruningService do
    let(:resource) { create_for_repository(:component, :with_content_file) }
    let!(:check1) { Ddr::Events::FixityCheckEvent.create(event_date_time: '2018-04-01 10:00:00', resource_id: resource.id_s) }
    let!(:check2) { Ddr::Events::FixityCheckEvent.create(event_date_time: '2019-04-01 10:00:00', resource_id: resource.id_s) }
    let!(:check3) { Ddr::Events::FixityCheckEvent.create(event_date_time: '2020-04-01 10:00:00', resource_id: resource.id_s) }
    let!(:check4) { Ddr::Events::FixityCheckEvent.create(event_date_time: '2021-04-01 10:00:00', resource_id: resource.id_s) }
    let!(:check5) { Ddr::Events::FixityCheckEvent.create(event_date_time: '2022-04-01 10:00:00', resource_id: resource.id_s) }

    it 'prunes fixity checks for a resource' do
      described_class.call(resource:)
      expect(Ddr::Events::FixityCheckEvent.for_resource(resource).pluck(:id)).to eq [check1.id, check5.id]
    end
  end
end
