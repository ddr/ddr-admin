module Ddr::V3
  RSpec.describe FixityCheckService do
    let(:resource) { create_for_repository(:component, :with_content_file, :with_thumbnail_file) }

    it 'creates an event' do
      expect { described_class.call(resource:) }.to change { Ddr::Events::FixityCheckEvent.for_resource(resource).count }.by(1)
    end

    describe 'failure' do
      before do
        allow(resource.thumbnail).to receive(:stored_checksums_valid?).and_return(false)
      end
    end
  end
end
