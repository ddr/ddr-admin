module Ddr
  RSpec.describe SetDefaultStructuresAfterSuccessfulBatchIngest, :batch, type: :service do
    let(:batch) { create(:batch) }

    context 'ingest objects' do
      let(:repo_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:batch_object) { Ddr::Batch::IngestBatchObject.create(resource_id: repo_id.id) }
      let(:repo_object) { Ddr::Component.new(id: repo_id) }

      before do
        batch.batch_objects << batch_object
        allow(Ddr.query_service).to receive(:find_by).with(id: repo_id.id) { repo_object }
        allow(repo_object).to receive(:parent).and_return(nil)
      end

      context 'can have structural metadata' do
        before do
          allow(repo_object).to receive(:can_have_struct_metadata?).and_return(true)
        end

        context 'existing structural metadata' do
          before do
            allow(repo_object).to receive(:has_struct_metadata?).and_return(true)
          end

          it 'does not enqueue GenerateDefaultStructureJob' do
            expect(GenerateDefaultStructureJob).not_to receive(:perform_later)
            ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
          end
        end

        context 'no existing structural metadata' do
          before do
            allow(repo_object).to receive(:has_struct_metadata?).and_return(false)
          end

          it 'enqueues GenerateDefaultStructureJob once for object' do
            expect(GenerateDefaultStructureJob).to receive(:perform_later).with(repo_id.id)
            ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
          end
        end
      end

      context 'cannot have structural metadata' do
        before do
          allow(repo_object).to receive(:can_have_struct_metadata?).and_return(false)
        end

        it 'does not enqueue GenerateDefaultStructureJob' do
          expect(GenerateDefaultStructureJob).not_to receive(:perform_later)
          ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
        end
      end

      context 'parent objects' do
        let(:parent_repo_id) { Valkyrie::ID.new(SecureRandom.uuid) }
        let(:parent_repo_object) { Ddr::Item.new(id: parent_repo_id) }

        before do
          allow(Ddr.query_service).to receive(:find_by).with(id: parent_repo_id.id) { parent_repo_object }
          allow(repo_object).to receive(:parent) { parent_repo_object }
          allow(parent_repo_object).to receive(:parent).and_return(nil)
          allow(parent_repo_object).to receive(:can_have_struct_metadata?).and_return(true)
          allow(GenerateDefaultStructureJob).to receive(:perform_later)
        end

        context 'included in batch' do
          let(:parent_batch_object) { Ddr::Batch::IngestBatchObject.create(resource_id: parent_repo_id) }

          before do
            batch.batch_objects << parent_batch_object
            allow(parent_repo_object).to receive(:has_struct_metadata?).and_return(false)
          end

          it 'enqueues GenerateDefaultStructureJob for the parent only once' do
            expect(GenerateDefaultStructureJob).to receive(:perform_later).with(parent_repo_id.id).once
            ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
          end
        end

        context 'not included in batch' do
          context 'parent has no existing structural metadata' do
            before do
              allow(parent_repo_object).to receive(:has_struct_metadata?).and_return(false)
            end

            it 'enqueues GenerateDefaultStructureJob for the parent only once' do
              expect(GenerateDefaultStructureJob).to receive(:perform_later).with(parent_repo_id.id).once
              ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
            end
          end

          context 'parent has existing structural metadata' do
            before do
              allow(parent_repo_object).to receive(:has_struct_metadata?).and_return(true)
            end

            context 'repository maintained' do
              before do
                allow(parent_repo_object).to receive_message_chain(:structure, :repository_maintained?) { true }
              end

              it 'enqueues GenerateDefaultStructureJob for the parent only once' do
                expect(GenerateDefaultStructureJob).to receive(:perform_later).with(parent_repo_id.id).once
                ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
              end
            end

            context 'externally provided' do
              before do
                allow(parent_repo_object).to receive_message_chain(:structure, :repository_maintained?) { false }
              end

              it 'does not enqueue GenerateDefaultStructureJob for the parent' do
                expect(GenerateDefaultStructureJob).not_to receive(:perform_later).with(parent_repo_id.id)
                ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
              end
            end
          end
        end
      end
    end

    context 'update objects' do
      let(:batch_object) { Ddr::Batch::UpdateBatchObject.create }

      before do
        batch.batch_objects << batch_object
      end

      it 'does not enqueue GenerateDefaultStructureJob' do
        expect(GenerateDefaultStructureJob).not_to receive(:perform_later)
        ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
      end
    end
  end
end
