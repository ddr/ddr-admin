require 'rails_helper'
require 'support/ingest_folder_helper'

shared_examples 'a properly modeled standard ingest filesystem node' do
  it 'determines the proper content model' do
    expect(described_class.new(node, intermediates_name, targets_name).call).to eq(proper_model)
  end
end

module Ddr
  RSpec.describe ModelStandardIngestContent, :batch, :standard_ingest, type: :service do
    let(:targets_name) { 'dpc_targets' }
    let(:intermediates_name) { 'intermediate_files' }
    let(:root_node) { Tree::TreeNode.new('/test/directory') }
    let(:nodeA) { Tree::TreeNode.new('A') }
    let(:nodeB) { Tree::TreeNode.new('B') }
    let(:nodeC) { Tree::TreeNode.new('C') }
    let(:nodeD) { Tree::TreeNode.new('D') }
    let(:nodeE) { Tree::TreeNode.new('E') }
    let(:nodeF) { Tree::TreeNode.new('F') }
    let(:nodeTargets) { Tree::TreeNode.new(targets_name) }
    let(:nodeTarget) { Tree::TreeNode.new('T') }
    let(:nodeIntermediates) { Tree::TreeNode.new(intermediates_name) }
    let(:nodeIntermediate) { Tree::TreeNode.new('I') }

    before do
      root_node << nodeA
      root_node << nodeB << nodeC
      root_node << nodeD << nodeE << nodeF
      root_node << nodeTargets << nodeTarget
      root_node << nodeIntermediates << nodeIntermediate
    end

    context 'root node' do
      let(:node) { root_node }
      let(:proper_model) { 'Ddr::Collection' }

      it_behaves_like 'a properly modeled standard ingest filesystem node'
    end

    context 'non-targets and non-intermediates' do
      context 'childless node in root node' do
        let(:node) { nodeA }
        let(:proper_model) { 'Ddr::Item' }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end

      context 'childed node in root node' do
        let(:node) { nodeB }
        let(:proper_model) { 'Ddr::Item' }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end

      context 'childless node in childed node in root node' do
        let(:node) { nodeC }
        let(:proper_model) { 'Ddr::Component' }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end
    end

    context 'targets' do
      context 'childed node in root node' do
        let(:node) { nodeTargets }
        let(:proper_model) { nil }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end

      context 'childless node in childed node in root node' do
        let(:node) { nodeTarget }
        let(:proper_model) { 'Ddr::Target' }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end
    end

    context 'intermediate files' do
      context 'childed node in root node' do
        let(:node) { nodeIntermediates }
        let(:proper_model) { nil }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end

      context 'childless node in childed node in root node' do
        let(:node) { nodeIntermediate }
        let(:proper_model) { nil }

        it_behaves_like 'a properly modeled standard ingest filesystem node'
      end
    end

    context 'childed node in childed node in root node' do
      let(:node) { nodeE }

      it 'raises a deepest node has children error' do
        expect { described_class.new(node).call }.to raise_error(Ddr::Batch::Error, /Deepest .* has children/)
      end
    end

    context 'third-level node' do
      let(:node) { nodeF }

      it 'raises a node too deep exception' do
        expect { described_class.new(node).call }.to raise_error(Ddr::Batch::Error, /too deep/)
      end
    end
  end
end
