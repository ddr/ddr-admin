module Ddr::Batch
  RSpec.describe ProcessBatchObject do
    let(:batch) { FactoryBot.create(:item_update_batch) }
    let(:batch_object) { batch.batch_objects.first }
    let(:pbo) { described_class.new(batch_object_id: batch_object.id, operator: batch.user) }

    describe 'execution' do
      before do
        allow(BatchObject).to receive(:find).with(batch_object.id) { batch_object }
        allow(batch_object).to receive(:results_message) {
          Ddr::Batch::BatchObject::ProcessingResultsMessage.new(Logger::INFO, 'Test message')
        }
      end

      describe 'valid object' do
        before { allow(batch_object).to receive(:validate).and_return([]) }

        describe 'messages' do
          before { allow(batch_object).to receive(:process).and_return(nil) }

          it 'adds appropriate messages' do
            pbo.execute
            expect(batch_object.batch_object_messages[0].level).to eq(Logger::INFO)
            expect(batch_object.batch_object_messages[0].message).to eq('Test message')
          end
        end

        describe 'processing' do
          it 'processes the object, mark it as handled, and return true' do
            expect(batch_object).to receive(:process).and_return(nil)
            success = pbo.execute
            expect(batch_object.handled?).to be(true)
            expect(success).to be(true)
          end
        end
      end

      describe 'invalid object' do
        before { allow(batch_object).to receive(:validate).and_return(['Validation message']) }

        describe 'messages' do
          it 'adds appropriate messages' do
            pbo.execute
            expect(batch_object.batch_object_messages[0].level).to eq(Logger::ERROR)
            expect(batch_object.batch_object_messages[0].message).to eq('Validation message')
          end
        end

        describe 'processing' do
          it 'does not process the object but mark it as handled and return false' do
            expect(batch_object).not_to receive(:process)
            success = pbo.execute
            expect(batch_object.handled?).to be(true)
            expect(success).to be(false)
          end
        end
      end
    end
  end
end
