module Ddr::Batch
  RSpec.describe ReprocessBatch do
    let(:rpb) { described_class.new(batch_id: batch.id, operator_id: batch.user.id) }

    describe 'notifications' do
      let(:batch) { FactoryBot.create(:batch) }

      it "issues a 'batch restarted' notification" do
        expect(ActiveSupport::Notifications).to receive(:instrument).with('restarted.batch.batch.ddr',
                                                                          batch_id: batch.id)
        rpb.execute
      end
    end

    describe 're-ingest batch' do
      context 'failed batch with loose components' do
        let(:batch) { create(:interrupted_ingest_batch_loose_components) }

        it 'calls the appropriate methods' do
          expect(rpb).not_to receive(:ingest_collection_object).with(batch.batch_objects[0])
          expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with(
            [batch.batch_objects[1].id, batch.batch_objects[2].id], batch.user.id
          )
          expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
            [batch.batch_objects[4].id, batch.batch_objects[5].id], batch.user.id
          )
          expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with([batch.batch_objects[6].id],
                                                                               batch.user.id)
          expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with([batch.batch_objects[7].id],
                                                                               batch.user.id)
          rpb.execute
        end
      end

      context 'failed batch with failed item' do
        let(:batch) { create(:interrupted_ingest_batch_failed_item) }

        it 'calls the appropriate methods' do
          expect(rpb).not_to receive(:ingest_collection_object).with(batch.batch_objects[0])
          expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with(
            [batch.batch_objects[1].id, batch.batch_objects[2].id], batch.user.id
          )
          expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
            [batch.batch_objects[3].id, batch.batch_objects[4].id, batch.batch_objects[5].id], batch.user.id
          )
          expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with([batch.batch_objects[6].id],
                                                                               batch.user.id)
          expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with([batch.batch_objects[7].id],
                                                                               batch.user.id)
          rpb.execute
        end
      end
    end

    describe 'reprocess failed update batch' do
      let(:batch) { FactoryBot.create(:failed_item_update_batch) }

      it 'calls the appropriate methods' do
        expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with([batch.batch_objects[0].id],
                                                                             batch.user.id)
        expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[1].id], batch.user.id)
        expect(BatchObjectsProcessorJob).not_to receive(:perform_later).with([batch.batch_objects[2].id],
                                                                             batch.user.id)
        rpb.execute
      end
    end
  end
end
