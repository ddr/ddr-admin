module Ddr::Batch
  RSpec.describe ProcessBatch do
    let(:pb) { described_class.new(batch_id: batch.id, operator_id: batch.user.id) }

    describe 'notifications' do
      let(:batch) { FactoryBot.create(:batch) }

      it "issues a 'batch started' notification" do
        expect(ActiveSupport::Notifications).to receive(:instrument).with('started.batch.batch.ddr', batch_id: batch.id)
        pb.execute
      end
    end

    describe 'ingest batch' do
      describe 'type of batch' do
        context 'collection creating' do
          let(:batch) { create(:collection_creating_ingest_batch) }

          it 'calls the appropriate methods' do
            expect(pb).to receive(:ingest_collection_object).with(batch.batch_objects[0])
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
              [batch.batch_objects[1].id, batch.batch_objects[2].id], batch.user.id
            )
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
              [batch.batch_objects[3].id, batch.batch_objects[4].id, batch.batch_objects[5].id], batch.user.id
            )
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[6].id],
                                                                             batch.user.id)
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[7].id],
                                                                             batch.user.id)
            pb.execute
          end

          describe 'collection creation failure' do
            before do
              allow(ActiveSupport::Notifications).to receive(:instrument).and_call_original
              allow(ActiveSupport::Notifications).to receive(:instrument).with('started.batch.batch.ddr',
                                                                               batch_id: batch.id)
              pbo = ProcessBatchObject.new(batch_object_id: batch.batch_objects[0].id,
                                           operator: User.find(batch.user.id))
              allow(ProcessBatchObject).to receive(:new).and_call_original
              allow(ProcessBatchObject).to receive(:new).with(batch_object_id: batch.batch_objects[0].id,
                                                              operator: User.find(batch.user.id)) {
                                             pbo
                                           }
              allow(pbo).to receive(:execute).and_return(false)
            end

            it "issues a 'batch finished' notification and raise an exception" do
              expect(ActiveSupport::Notifications).to receive(:instrument).with('finished.batch.batch.ddr',
                                                                                batch_id: batch.id)
              expect { pb.execute }.to raise_error(BatchObjectProcessingError)
            end
          end
        end

        context 'item adding' do
          let(:batch) { FactoryBot.create(:item_adding_ingest_batch) }

          it 'calls the appropriate methods' do
            expect(pb).not_to receive(:ingest_collection_object)
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
              [batch.batch_objects[0].id, batch.batch_objects[1].id], batch.user.id
            )
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
              [batch.batch_objects[2].id, batch.batch_objects[3].id, batch.batch_objects[4].id], batch.user.id
            )
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[5].id],
                                                                             batch.user.id)
            expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[6].id],
                                                                             batch.user.id)
            pb.execute
          end
        end
      end

      describe "previous batch with conflicting repository ID's" do
        let!(:prev_batch) { FactoryBot.create(:collection_creating_ingest_batch) }
        let(:batch) { FactoryBot.create(:collection_creating_ingest_batch) }

        it 'includes the correct batch objects in the processing jobs' do
          expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
            [batch.batch_objects[1].id, batch.batch_objects[2].id], batch.user.id
          )
          expect(BatchObjectsProcessorJob).to receive(:perform_later).with(
            [batch.batch_objects[3].id, batch.batch_objects[4].id, batch.batch_objects[5].id], batch.user.id
          )
          expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[6].id], batch.user.id)
          expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[7].id], batch.user.id)
          pb.execute
        end
      end
    end

    describe 'update batch' do
      let(:batch) { FactoryBot.create(:item_update_batch) }

      it 'calls the appropriate methods' do
        expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[0].id], batch.user.id)
        expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[1].id], batch.user.id)
        expect(BatchObjectsProcessorJob).to receive(:perform_later).with([batch.batch_objects[2].id], batch.user.id)
        pb.execute
      end
    end
  end
end
