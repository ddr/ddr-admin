module Ddr::Batch
  RSpec.describe ProcessBatchObjects do
    let(:batch) { FactoryBot.create(:item_adding_ingest_batch) }
    let(:pbos) do
      ProcessBatchObjects.new(batch_object_ids: [batch.batch_objects[0].id, batch.batch_objects[1].id],
                              operator: batch.user)
    end

    describe 'first object processes successfully' do
      before do
        allow(ProcessBatchObject).to receive(:new).with(batch_object_id: batch.batch_objects[0].id,
                                                        operator: batch.user).and_call_original
        allow_any_instance_of(ProcessBatchObject).to receive(:execute).and_return(true)
      end

      it 'processes the second object' do
        expect(ProcessBatchObject).to receive(:new).with(batch_object_id: batch.batch_objects[1].id,
                                                         operator: batch.user).and_call_original
        pbos.execute
      end
    end

    describe 'first object does not process successfully' do
      before do
        allow(ProcessBatchObject).to receive(:new).with(batch_object_id: batch.batch_objects[0].id,
                                                        operator: batch.user).and_call_original
        allow_any_instance_of(ProcessBatchObject).to receive(:execute).and_return(false)
      end

      it 'does not process the second object' do
        expect(ProcessBatchObject).not_to receive(:new).with(batch_object_id: batch.batch_objects[1].id,
                                                             operator: batch.user)
        pbos.execute
      end
    end

    describe 'some objects processed' do
      before do
        batch.batch_objects.first.update(verified: true)
        allow(ProcessBatchObject).to receive(:new).with(batch_object_id: batch.batch_objects[1].id,
                                                        operator: batch.user).and_call_original
        allow_any_instance_of(ProcessBatchObject).to receive(:execute).and_return(true)
      end

      it 'does not process the already processed objects' do
        expect(ProcessBatchObject).not_to receive(:new).with(batch_object_id: batch.batch_objects[0].id,
                                                             operator: batch.user)
        pbos.execute
      end
    end
  end
end
