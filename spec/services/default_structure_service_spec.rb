require 'rails_helper'
require 'equivalent-xml/rspec_matchers'

module Ddr
  RSpec.describe DefaultStructureService, type: :service do
    # Check that default structure responds with the right method call

    describe '#default_structure' do
      context 'with a Collection' do
        before do
          allow(SecureRandom).to receive(:uuid).and_return('ed7420a4-9e6a-4316-b606-cb3bf6213017',
                                                           'c88df0ff-d998-4805-97b8-80fb2769d14c',
                                                           '24f415d6-cc90-4dff-8d78-b5d1f0b3ec2c',
                                                           '834de993-0756-4622-8063-a41193c05da9',
                                                           'bd4a9d03-4b06-40ba-9ac3-ab95338532d5',
                                                           'e6b9fc4d-ae38-4dfc-98d7-dabe2cf97efa',
                                                           '50b85eb3-6701-4495-bd0e-fd7e71a9b268')
        end

        describe 'when the collection has no items' do
          let(:expected) do
            xml = <<-EOS
                <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <metsHdr>
                    <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                      <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                    </agent>
                  </metsHdr>
                  <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}" />
                </mets>
            EOS
            xml
          end
          let(:coll) { create_for_repository(:collection) }

          after { Ddr.persister.delete(resource: coll) }

          it 'is the appropriate structure' do
            expect(DefaultStructureService.default_structure(coll).to_xml).to be_equivalent_to(expected)
          end
        end

        describe 'when the collection has items' do
          let!(:coll) { create_for_repository(:collection) }
          let!(:item1) { create_for_repository(:item, local_id: 'test002', parent_id: coll.id) }
          let!(:item2) do
            create_for_repository(:item, local_id: 'test001', permanent_id: 'ark:/99999/fk4bbb', parent_id: coll.id)
          end

          describe 'without nested paths' do
            let(:expected) do
              xml = <<~EOS
                <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <metsHdr>
                    <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                      <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                    </agent>
                  </metsHdr>
                  <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}">
                    <div ORDER="1">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item2.id}" />
                    </div>
                    <div ORDER="2">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item1.id}" />
                    </div>
                  </structMap>
                </mets>
              EOS
              xml
            end

            it 'is the appropriate structure' do
              expect(DefaultStructureService.default_structure(coll).to_xml).to be_equivalent_to(expected)
            end
          end

          describe 'when the collection has items, add their URNs' do
            let(:item1_doc) { SolrDocument.new('id' => item1.id) }
            let(:item2_doc) { SolrDocument.new('id' => item2.id) }
            let(:expected) do
              xml = <<~EOS
                <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <metsHdr>
                    <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                      <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                    </agent>
                  </metsHdr>
                  <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}">
                    <div ORDER="1">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item2.id}" />
                    </div>
                    <div ORDER="2">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item1.id}" />
                    </div>
                  </structMap>
                </mets>
              EOS
              xml
            end

            it 'is the appropriate structure' do
              expect(DefaultStructureService.default_structure(coll).to_xml).to be_equivalent_to(expected)
            end
          end

          describe 'with nested paths' do
            let(:item3) { create_for_repository(:item) }
            let(:item4) { create_for_repository(:item) }
            let(:item5) { create_for_repository(:item) }
            let(:item6) { create_for_repository(:item) }
            let(:expected) do
              xml = <<~EOS
                <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <metsHdr>
                    <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                      <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                    </agent>
                  </metsHdr>
                  <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}">
                    <div LABEL="foo" ORDER="1" TYPE="Directory">
                      <div LABEL="b&amp;apos;a&amp;quot;r&amp;quot;" ORDER="1" TYPE="Directory">
                        <div ORDER="1">
                          <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item1.id}"/>
                        </div>
                        <div ORDER="2">
                          <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item2.id}"/>
                        </div>
                      </div>
                      <div LABEL="baz" ORDER="2" TYPE="Directory">
                        <div ORDER="1">
                          <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item4.id}"/>
                        </div>
                        <div ORDER="2">
                          <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item3.id}"/>
                        </div>
                      </div>
                      <div ORDER="3">
                        <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item6.id}"/>
                      </div>
                      <div ORDER="4">
                        <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{item5.id}"/>
                      </div>
                    </div>
                  </structMap>
                </mets>
              EOS
              xml
            end

            before do
              item1.nested_path = %(foo/b'a"r"/a.doc)
              Ddr.persister.save(resource: item1)
              item2.nested_path = %(foo/b'a"r"/b.txt)
              Ddr.persister.save(resource: item2)
              item3.local_id = 'test003'
              item3.parent_id = coll.id
              item3.nested_path = %(foo/baz/d.pdf)
              Ddr.persister.save(resource: item3)
              item4.local_id = 'test004'
              item4.parent_id = coll.id
              item4.nested_path = %(foo/baz/c.txt)
              Ddr.persister.save(resource: item4)
              item5.local_id = 'test005'
              item5.parent_id = coll.id
              item5.nested_path = %(foo/f.doc)
              Ddr.persister.save(resource: item5)
              item6.local_id = 'test006'
              item6.permanent_id = 'ark:/99999/fk4fff'
              item6.parent_id = coll.id
              item6.nested_path = %(foo/e.pdf)
              Ddr.persister.save(resource: item6)
            end

            after do
              Ddr.persister.delete(resource: coll)
              Ddr.persister.delete(resource: item1)
              Ddr.persister.delete(resource: item2)
              Ddr.persister.delete(resource: item3)
              Ddr.persister.delete(resource: item4)
              Ddr.persister.delete(resource: item5)
              Ddr.persister.delete(resource: item6)
            end

            it 'is the appropriate structure' do
              expect(DefaultStructureService.default_structure(coll).to_xml).to be_equivalent_to(expected)
            end
          end
        end
      end

      context 'with an Item' do
        let(:item) { create_for_repository(:item) }

        describe 'when the item has no components' do
          let(:expected) do
            xml = <<~EOS
              <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                <metsHdr>
                  <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                    <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                  </agent>
                </metsHdr>
                <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}" />
              </mets>
            EOS
            xml
          end

          it 'is the appropriate structure' do
            expect(DefaultStructureService.default_structure(item).to_xml).to be_equivalent_to(expected)
          end
        end

        describe 'when the item has components' do
          let(:comp1) { create(:component, id: build(:valkyrie_id)) }
          let(:comp2) { create(:component, id: build(:valkyrie_id)) }
          let(:comp3) { create(:component, id: build(:valkyrie_id)) }
          let(:comp4) { create(:component, id: build(:valkyrie_id)) }
          let(:expected) do
            xml = <<~EOS
              <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                <metsHdr>
                  <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                    <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                  </agent>
                </metsHdr>
                <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}">
                  <div TYPE="Documents">
                    <div ORDER="1">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{comp3.id}" />
                    </div>
                  </div>
                  <div TYPE="Images">
                    <div ORDER="1">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{comp2.id}" />
                    </div>
                    <div ORDER="2">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{comp1.id}" />
                    </div>
                  </div>
                  <div TYPE="Other">
                    <div ORDER="1">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{comp4.id}" />
                    </div>
                  </div>
                </structMap>
              </mets>
            EOS
            xml
          end

          before do
            comp1.local_id = 'test001002'
            comp1.parent_id = item.id
            file1 = Ddr.storage_adapter.upload(file: fixture_file_upload('imageB.tif', 'image/tiff'),
                                               resource: comp1, original_filename: 'imageB.tif')
            comp1.content = Ddr::File.new(file_identifier: file1.id, media_type: 'image/tiff',
                                          original_filename: 'imageB.tif')
            Ddr.persister.save(resource: comp1)
            comp2.local_id = 'test001001'
            comp2.parent_id = item.id
            file2 = Ddr.storage_adapter.upload(file: fixture_file_upload('imageA.tif', 'image/tiff'),
                                               resource: comp2, original_filename: 'imageA.tif')
            comp2.content = Ddr::File.new(file_identifier: file2.id, media_type: 'image/tiff',
                                          original_filename: 'imageA.tif')
            Ddr.persister.save(resource: comp2)
            comp3.local_id = 'test001'
            comp3.parent_id = item.id
            file3 = Ddr.storage_adapter.upload(file: fixture_file_upload('sample.pdf', 'application/pdf'),
                                               resource: comp3, original_filename: 'sample.pdf')
            comp3.content = Ddr::File.new(file_identifier: file3.id, media_type: 'application/pdf',
                                          original_filename: 'sample.pdf')
            Ddr.persister.save(resource: comp3)
            comp4.local_id = 'test123'
            comp4.parent_id = item.id
            file4 = Ddr.storage_adapter.upload(file: fixture_file_upload('abcd1234.vtt', 'application/octet-stream'),
                                               resource: comp3, original_filename: 'abcd1234.vtt')
            comp4.content = Ddr::File.new(file_identifier: file4.id, media_type: 'application/octet-stream',
                                          original_filename: 'abcd1234.vtt')
            Ddr.persister.save(resource: comp4)
          end

          after do
            Ddr.persister.delete(resource: item)
            Ddr.persister.delete(resource: comp1)
            Ddr.persister.delete(resource: comp2)
            Ddr.persister.delete(resource: comp3)
            Ddr.persister.delete(resource: comp4)
          end

          it 'is the appropriate structure' do
            expect(DefaultStructureService.default_structure(item).to_xml).to be_equivalent_to(expected)
          end
        end

        describe 'when the item has a component, add its URN' do
          let(:comp) { create(:component, id: build(:valkyrie_id), parent_id: item.id) }
          let(:file) do
            Ddr::File.new Ddr.storage_adapter.upload(
              file: fixture_file_upload('imageA.tif', 'image/tiff'),
              resource: comp,
              original_filename: 'imageA.tif'
            )
          end
          let(:expected) do
            xml = <<~EOS
              <?xml version="1.0"?>
              <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
                <metsHdr>
                  <agent ROLE="#{Ddr::Structures::Agent::ROLE_CREATOR}">
                    <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
                  </agent>
                </metsHdr>
                <structMap TYPE="#{Ddr::Structure::TYPE_DEFAULT}">
                  <div TYPE="Other">
                    <div ORDER="1">
                      <mptr LOCTYPE="URN" xlink:href="urn:uuid:#{comp.id}"/>
                    </div>
                  </div>
                </structMap>
              </mets>
            EOS
            xml
          end

          before do
            Ddr.persister.save(resource: comp)
          end

          it 'is the appropriate structure' do
            expect(DefaultStructureService.default_structure(item).to_xml).to be_equivalent_to(expected)
          end
        end
      end

      context 'With a Component' do
        describe 'default structure' do
          let(:component) { create(:component, id: build(:valkyrie_id)) }

          it 'refuses to generate a default structure' do
            expect { DefaultStructureService.default_structure(component).to raise_error(ArgumentError) }
          end
        end
      end

      context 'with a File' do
        let(:file) do
          Ddr::File.new Ddr.storage_adapter.upload(file: fixture_file_upload('imageB.tif', 'image/tiff'),
                                                   resource: comp1, original_filename: 'imageB.tif')
        end

        it 'refuses to generate a default structure' do
          expect { DefaultStructureService.default_structure(file).to raise_error(ArgumentError) }
        end
      end
    end
  end
end
