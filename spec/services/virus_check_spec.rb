module Ddr
  RSpec.describe VirusCheck do
    let(:file_path) { Rails.root.join('spec/fixtures/files/imageA.tif') }

    describe 'successful scan' do
      let(:scan_time) { Time.now.utc }
      let(:output) { 'Scanner output' }
      let(:version) { 'Scanner version' }
      let(:scanner_result) do
        Ddr::Antivirus::ScanResult.new(file_path, output, scanned_at: scan_time, version:)
      end
      let(:expected_result) do
        { detail: output, event_date_time: scan_time, software: version }
      end

      before do
        allow(Ddr::Antivirus).to receive(:scan).with(file_path).and_return(scanner_result)
      end

      it 'provides the appropriate result' do
        expect(described_class.call(file_path)).to include(expected_result)
      end
    end

    describe 'exceptions' do
      describe 'max file size exceeded' do
        let(:msg) { 'File is way too big for me to deal with' }
        let(:version) { "ddr-antivirus #{Ddr::Antivirus::VERSION}" }
        let(:expected_result) do
          { exception: [Ddr::Antivirus::MaxFileSizeExceeded.name, msg], software: version }
        end

        before do
          allow(Ddr::Antivirus).to receive(:scan).with(file_path).and_raise(Ddr::Antivirus::MaxFileSizeExceeded, msg)
        end

        it 'catches the exception and includes it in the result' do
          expect(described_class.call(file_path)).to include(expected_result)
        end
      end

      describe 'scanner error' do
        let(:msg) { 'Sorry, I had a problem' }
        let(:version) { "ddr-antivirus #{Ddr::Antivirus::VERSION}" }
        let(:expected_result) do
          { exception: [Ddr::Antivirus::ScannerError.name, msg], software: version }
        end

        before do
          allow(Ddr::Antivirus).to receive(:scan).with(file_path).and_raise(Ddr::Antivirus::ScannerError, msg)
        end

        it 'catches the exception and includes it in the result' do
          expect(described_class.call(file_path)).to include(expected_result)
        end
      end
    end
  end
end
