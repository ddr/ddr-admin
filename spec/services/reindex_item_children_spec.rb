module Ddr
  RSpec.describe ReindexItemChildren do
    let(:item) { FactoryBot.create_for_repository(:item) }

    before { allow(IndexService).to receive(:call) }

    describe 'when title changed' do
      it 'does not reindex' do
        change_set = Ddr::ResourceChangeSet.change_set_for(item)
        change_set.title = 'Changed Title'
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        expect(IndexService).not_to have_received(:call)
      end
    end

    describe 'when access_role changed' do
      it 'reindexes' do
        change_set = Ddr::ResourceChangeSet.change_set_for(item)
        change_set.roles += [FactoryBot.build(:role, :policy, :viewer, :public)]
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        expect(IndexService).to have_received(:call)
      end
    end

    describe 'when available changed' do
      it 'reindexes' do
        change_set = Ddr::ResourceChangeSet.change_set_for(item)
        change_set.available = DateTime.now + 1.day
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        expect(IndexService).to have_received(:call)
      end
    end

    describe 'when parent_id changed' do
      let(:coll) { create_for_repository(:collection) }

      it 'reindexes' do
        change_set = Ddr::ResourceChangeSet.change_set_for(item)
        change_set.parent_id = coll.id
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        expect(IndexService).to have_received(:call)
      end
    end

    describe 'when description changed' do
      it 'does not reindex' do
        change_set = Ddr::ResourceChangeSet.change_set_for(item)
        change_set.description = 'A new description for this resource'
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        expect(IndexService).not_to have_received(:call)
      end
    end
  end
end
