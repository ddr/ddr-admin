module Ddr
  module Services
    RSpec.describe ThumbnailsService do
      describe '.call' do
        let(:collection) { Ddr::Collection.new }

        it 'instantiates and executes the service' do
          expect(described_class).to receive(:new).with(collection).and_call_original
          expect_any_instance_of(described_class).to receive(:execute)
          described_class.call(collection)
        end
      end

      describe '#execute' do
        let!(:collection) { create_for_repository(:collection) }
        let!(:item) { create_for_repository(:item, parent_id: collection.id) }
        let!(:component) { create_for_repository(:component, parent_id: item.id) }
        let(:thumbnail) { fixture_file_upload('target.png') }
        let(:thumbnail_checksum) { Ddr::Utils.digest(::File.read(thumbnail), 'SHA-1') }

        context 'resource does not have thumbnail' do
          context "resource's first child does have thumbnail" do
            before do
              change_set = Ddr::ResourceChangeSet.change_set_for(component)
              change_set.add_file(thumbnail, :thumbnail)
              Ddr::ResourceChangeSetPersister.new.save(change_set:)
            end

            it 'populates the thumbnail attribute from the child thumbnail' do
              described_class.new(collection).execute
              thumb = Ddr.query_service.find_by(id: item.id).thumbnail
              expect { thumb.validate_checksum!(thumbnail_checksum, Ddr::Files::CHECKSUM_TYPE_SHA1) }.not_to raise_error
            end

            it 'does not trigger a structural metadata update when persisted' do
              expect_any_instance_of(Ddr::ResourceChangeSetPersister)
                .to receive(:save)
                .with(change_set: have_attributes(skip_structure_updates: true))
              described_class.new(collection).execute
            end
          end

          context "resource's first child does not have thumbnail" do
            it 'does not populate the thumbnail attribute' do
              described_class.new(collection).execute
              thumb = Ddr.query_service.find_by(id: item.id).thumbnail
              expect(thumb).to be_nil
            end
          end
        end

        context 'resource already has thumbnail' do
          let(:item_thumbnail) { fixture_file_upload('bird.jpg') }
          let(:item_thumbnail_checksum) { Ddr::Utils.digest(::File.read(item_thumbnail), 'SHA-1') }

          before do
            component_change_set = Ddr::ResourceChangeSet.change_set_for(component)
            component_change_set.add_file(thumbnail, :thumbnail)
            Ddr::ResourceChangeSetPersister.new.save(change_set: component_change_set)
            item_change_set = Ddr::ResourceChangeSet.change_set_for(item)
            item_change_set.add_file(item_thumbnail, :thumbnail)
            Ddr::ResourceChangeSetPersister.new.save(change_set: item_change_set)
          end

          it 'does not alter the existing thumbnail' do
            expect_any_instance_of(Ddr::ItemChangeSet).not_to receive(:add_file).with(String, 'thumbnail')
            described_class.new(collection).execute
          end
        end
      end
    end
  end
end
