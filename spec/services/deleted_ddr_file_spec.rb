module Ddr
  RSpec.describe DeletedDdrFile do
    describe '.call' do
      let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:stored_path) { 'app/tmp/files/75/32/a9/7532a95e-6cf2-4abd-afa1-1fdc0caffdb2' }
      let(:file_identifier) { Valkyrie::ID.new("disk://#{stored_path}") }
      let(:args) do
        ['delete.ddr_file', Time.now, Time.new, 'c899213a35664e5d770d',
         { resource_id:,
           file_profile: { ddr_file_type: :content,
                           file_identifier:,
                           file_path: stored_path } }]
      end

      it 'records the deletion of the stored file but does not actually delete it from disk' do
        expect(DeleteStoredFile).not_to receive(:delete_file_by_identifier).with(file_identifier)
        described_class.call(*args)
        expect(DeletedFile.where(repo_id: resource_id.id, file_id: 'content', path: stored_path)).not_to be_empty
      end
    end
  end
end
