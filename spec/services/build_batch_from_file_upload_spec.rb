require 'rails_helper'
require_relative '../support/ingest_folder_helper'

RSpec.shared_examples 'a successfully built file upload batch' do
  it 'builds an appropriate batch' do
    # Batch expectations
    expect(batch.id).to be_present
    expect(batch.name).to eq(batch_name)
    expect(batch.description).to eq(batch_description)
    expect(batch.collection_id).to eq(collection.id.to_s)
    expect(batch.collection_title).to eq('Test Collection')
    expect(batch.status).to eq(Ddr::Batch::Batch::STATUS_READY)

    # Batch object expectations
    expect(batch_objects.count).to eq(3)
    batch_object_filepaths = {}
    batch_object_checksums = {} if defined?(checksums)
    batch_objects.each do |obj|
      expect(obj.type).to eq('Ddr::Batch::UpdateBatchObject')
      expect(obj.resource_id).not_to be_nil
      batch_object_datastreams = obj.batch_object_datastreams
      expect(batch_object_datastreams.size).to eq(1)
      batch_object_datastream = batch_object_datastreams.first
      expect(batch_object_datastream.name).to eq(ddr_file_name.to_s)
      expect(batch_object_datastream.operation).to eq(Ddr::Batch::BatchObjectDatastream::OPERATION_ADDUPDATE)
      expect(batch_object_datastream.payload_type).to eq(Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME)
      batch_object_filepaths[obj.resource_id] = batch_object_datastream.payload
      if defined?(checksums)
        expect(batch_object_datastream.checksum_type).to eq(Ddr::Files::CHECKSUM_TYPE_SHA1)
        batch_object_checksums[obj.resource_id] = batch_object_datastream.checksum
      end
    end
    expect(batch_object_filepaths).to match(expected_filepaths)
    expect(batch_object_checksums).to match(expected_checksums) if defined?(checksums)
  end
end

module Ddr
  RSpec.describe BuildBatchFromFileUpload, :batch, type: :service do
    let(:batch_builder) { described_class.new(**builder_args) }
    let(:batch_description) { 'Testing file upload batch building' }
    let(:batch_name) { 'Test File Upload Batch' }
    let(:batch_objects) { batch.batch_objects }
    let(:collection) { create_for_repository(:collection, title: ['Test Collection'], admin_set: 'dc') }
    let(:item) { create_for_repository(:item, parent_id: collection.id, admin_policy_id: collection.id) }
    let(:component) { create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id) }
    let(:ddr_file_name) { :intermediate_file }

    let(:expected_filepaths) do
      { comp1.id.to_s => '/test/directory/abc001.jpg',
        comp2.id.to_s => '/test/directory/abc002.jpg',
        comp3.id.to_s => '/test/directory/abc003.jpg' }
    end
    let(:filesystem) { Filesystem.new }
    let(:user) { FactoryBot.create(:user) }

    describe 'file upload' do
      let!(:comp1) do
        create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id, local_id: 'abc001')
      end
      let!(:comp2) do
        create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id, local_id: 'abc002')
      end
      let!(:comp3) do
        create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id, local_id: 'abc003')
      end

      before do
        filesystem.tree = filesystem_file_uploads
      end

      describe 'checksum file not provided' do
        let(:builder_args) do
          { batch_description:, batch_name:,
            batch_user: user, collection_id: collection.id.to_s, ddr_file_name:,
            filesystem: }
        end

        it_behaves_like 'a successfully built file upload batch' do
          let(:batch) { batch_builder.call }
        end
      end

      describe 'checksum file provided' do
        let(:checksum_file_path) { '/tmp/checksums.txt' }
        let(:checksums) do
          { '/test/directory/abc001.jpg' => '03f717284d2f8c5ffb0714cb85d1d6689cffa0b0',
            '/test/directory/abc002.jpg' => '75e2e0cec6e807f6ae63610d46448f777591dd6b',
            '/test/directory/abc003.jpg' => '2cf23f0035c12b6242093e93d0f7eeba0b1e08e8' }
        end
        let(:expected_checksums) do
          { comp1.id.to_s => checksums['/test/directory/abc001.jpg'],
            comp2.id.to_s => checksums['/test/directory/abc002.jpg'],
            comp3.id.to_s => checksums['/test/directory/abc003.jpg'] }
        end
        let(:builder_args) do
          { batch_description:, batch_name:,
            batch_user: user, checksum_file_path:, collection_id: collection.id.to_s,
            ddr_file_name:, filesystem: }
        end

        before do
          checksums.each do |key, value|
            allow_any_instance_of(IngestChecksum).to receive(:checksum).with(key) { value }
          end
        end

        it_behaves_like 'a successfully built file upload batch' do
          let(:batch) { batch_builder.call }
        end
      end
    end

    describe 'component matching' do
      let(:builder_args) do
        { batch_description:, batch_name:,
          batch_user: user, collection_id: collection.id.to_s, ddr_file_name:,
          filesystem: }
      end

      before do
        filesystem.tree = filesystem_single_file_upload
      end

      describe 'no match by local_id or filename' do
        it 'raises an error' do
          expect { batch_builder.call }.to raise_error(Ddr::Error, /Unable to find matching component/)
        end
      end

      describe 'match by local_id' do
        let!(:component) do
          create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id, local_id: 'abc001')
        end

        it 'finds the correct component' do
          batch = batch_builder.call
          expect(batch.batch_objects.first.resource_id).to eq(component.id.to_s)
        end
      end

      describe 'match by filename but not local_id' do
        before do
          change_set = Ddr::ComponentChangeSet.new(component)
          change_set.add_file('spec/fixtures/abc001.tif', :content)
          Ddr::ResourceChangeSetPersister.new.save(change_set:)
        end

        it 'finds the correct component' do
          batch = batch_builder.call
          expect(batch.batch_objects.first.resource_id).to eq(component.id.to_s)
        end
      end

      describe 'match on entire filename except for extension' do
        let(:comp) { FactoryBot.build(:component) }

        before do
          cmp = Ddr.query_service.find_by(id: component.id)
          parent = create_for_repository(:item, parent_id: collection.id)
          cs = Ddr::ComponentChangeSet.new(cmp)
          cs.parent_id = parent.id
          cs.add_file('spec/fixtures/abc001.tif', :content)
          Ddr::ResourceChangeSetPersister.new.save(change_set: cs)
          cs = Ddr::ComponentChangeSet.new(comp)
          cs.add_file('spec/fixtures/abc001001.tif', :content)
          Ddr::ResourceChangeSetPersister.new.save(change_set: cs)
        end

        it 'finds the correct component' do
          batch = batch_builder.call
          expect(batch.batch_objects.first.resource_id).to eq(component.id.to_s)
        end
      end
    end
  end
end
