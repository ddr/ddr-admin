module Ddr
  RSpec.describe RoleService, :roles, :service do
    let(:role1) { Ddr::Auth::Roles::Role.new(agent: 'public', role_type: 'Downloader', scope: 'resource') }
    let(:role2) { Ddr::Auth::Roles::Role.new(agent: 'curators', role_type: 'Curator', scope: 'resource') }
    let(:role3) { Ddr::Auth::Roles::Role.new(agent: 'admins', role_type: 'Curator', scope: 'resource') }

    describe '.revoke_all' do
      it 'revokes matching roles' do
        resource1 = create_for_repository(:item, access_role: [role1])
        resource2 = create_for_repository(:item, access_role: [role2])
        resource3 = create_for_repository(:item, access_role: [role1, role2])

        described_class.revoke_all(agent: 'public')

        expect(Ddr.query_service.find_by(id: resource1.id).roles).to be_empty
        expect(Ddr.query_service.find_by(id: resource2.id).roles).to eq [role2]
        expect(Ddr.query_service.find_by(id: resource3.id).roles).to eq [role2]

        expect(Ddr::API::ResourceRepository.find_by_role(agent: 'public').size).to eq 0
      end
    end

    describe '.revoke' do
      it 'revokes matching roles from the resource' do
        resource = create_for_repository(:item, access_role: [role1, role2])
        described_class.revoke(agent: 'public', resource:)
        expect(Ddr.query_service.find_by(id: resource.id).roles).to eq [role2]
      end

      it 'is a no-op when there are no matching roles' do
        resource = create_for_repository(:item, access_role: [role2, role3])
        described_class.revoke(agent: 'public', resource:)
        expect(Ddr.query_service.find_by(id: resource.id).roles).to eq [role2, role3]
      end
    end

    describe '.reassign_all' do
      it 'revokes matching roles' do
        resource1 = create_for_repository(:item, access_role: [role1])
        resource2 = create_for_repository(:item, access_role: [role2])
        resource3 = create_for_repository(:item, access_role: [role1, role2])

        described_class.reassign_all(from_agent: 'curators', to_agent: 'admins')

        expect(Ddr.query_service.find_by(id: resource1.id).roles).to eq [role1]
        expect(Ddr.query_service.find_by(id: resource2.id).roles).to eq [role3]
        expect(Ddr.query_service.find_by(id: resource3.id).roles).to eq [role1, role3]

        expect(Ddr::API::ResourceRepository.find_by_role(agent: 'curators').size).to eq 0
      end
    end

    describe '.reassign' do
      it 'reassigns matching roles from the resource' do
        resource = create_for_repository(:item, access_role: [role1, role2])
        described_class.reassign(from_agent: 'curators', to_agent: 'admins', resource:)
        expect(Ddr.query_service.find_by(id: resource.id).roles).to eq [role1, role3]
      end

      it 'is a no-op when there are no matching roles' do
        resource = create_for_repository(:item, access_role: [role2, role3])
        described_class.reassign(from_agent: 'public', to_agent: 'registered', resource:)
        expect(Ddr.query_service.find_by(id: resource.id).roles).to eq [role2, role3]
      end
    end
  end
end
