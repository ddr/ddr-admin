module Ddr
  RSpec.describe DeletedResource do
    describe '.call' do
      let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:content_file_path) { '/tmp/files/d3/b4/30/d3b43043-9a4c-4ffc-9246-be2ad3c2d06e' }
      let(:content_file_identifier) { Valkyrie::ID.new("disk://#{content_file_path}") }
      let(:content_file_profile) do
        { ddr_file_type: :content, file_identifier: content_file_identifier, file_path: content_file_path }
      end
      let(:thumbnail_file_path) { '/tmp/files/04/95/ca/0495ca75-0c48-44ca-87ec-9c9e3c442748' }
      let(:thumbnail_file_identifier) { Valkyrie::ID.new("disk://#{thumbnail_file_path}") }
      let(:thumbnail_file_profile) do
        { ddr_file_type: :thumbnail, file_identifier: thumbnail_file_identifier, file_path: thumbnail_file_path }
      end
      let(:args) do
        ['delete.ddr_resource', 6.seconds.ago, 4.seconds.ago, '0c5b9696f8580198a216',
         { resource_id:, model: 'Ddr::Component', user_key: nil, permanent_id: nil, parent: nil,
           comment: nil, detail: nil, skip_structure_updates: false, summary: nil,
           file_profiles: [content_file_profile, thumbnail_file_profile],
           create_date: 10.days.ago, modified_date: 8.days.ago }]
      end

      it 'calls for recording the stored files as deleted but does not actually delete them' do
        expect(DeletedDdrFile).to receive(:record).with(resource_id, content_file_profile)
        expect(DeletedDdrFile).to receive(:record).with(resource_id, thumbnail_file_profile)
        expect(DeleteStoredFile).not_to receive(:delete_file_by_identifier).with(content_file_identifier)
        expect(DeleteStoredFile).not_to receive(:delete_file_by_identifier).with(thumbnail_file_identifier)
        described_class.call(*args)
      end
    end
  end
end
