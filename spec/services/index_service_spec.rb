module Ddr
  RSpec.describe IndexService, type: :service do
    describe '.call' do
      let(:resource_ids) { build_list(:valkyrie_id, 24) }
      let(:resource_ids_ids) { resource_ids.map(&:id) }

      around do |example|
        prev_solr_update_batch_size = Ddr::Admin.solr_update_batch_size
        Ddr::Admin.solr_update_batch_size = 10
        example.run
        Ddr::Admin.solr_update_batch_size = prev_solr_update_batch_size
      end

      it 'enqueues IndexJob with batches of resource ids' do
        expect(UpdateIndexJob).to receive(:perform_later).with(resource_ids_ids[0..9])
        expect(UpdateIndexJob).to receive(:perform_later).with(resource_ids_ids[10..19])
        expect(UpdateIndexJob).to receive(:perform_later).with(resource_ids_ids[20..23])
        described_class.call(resource_ids)
      end
    end

    describe '#index' do
      subject { described_class.new(resource_ids) }
      let(:resource_ids) { build_list(:valkyrie_id, 4) }
      let(:resources) { [Ddr::Resource.new, Ddr::Resource.new, Ddr::Resource.new, Ddr::Resource.new] }

      before do
        4.times do |i|
          allow(Ddr.query_service).to receive(:find_by).with(id: resource_ids[i]) { resources[i] }
        end
      end

      it 'calls #save_all on the solr persister with the corresponding resources' do
        expect(Valkyrie::MetadataAdapter.find(:index_solr)).to receive_message_chain(:persister, :save_all)
          .with(resources:)
        subject.index
      end
    end
  end
end
