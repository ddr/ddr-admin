module Ddr
  RSpec.describe FixityCheck do
    subject { described_class.call(resource) }

    let(:resource) { create_for_repository(:component, :with_content_file, :with_thumbnail_file) }

    describe 'success' do
      it { is_expected.to be_a(Ddr::FixityCheck::Result) }
      its(:resource_id) { is_expected.to eq(resource.id) }
      its(:success) { is_expected.to be true }
      its(:results) { is_expected.to include(content: true, thumbnail: true) }
    end

    describe 'failure' do
      before do
        allow(resource.thumbnail).to receive(:stored_checksums_valid?).and_return(false)
      end

      it { is_expected.to be_a(Ddr::FixityCheck::Result) }
      its(:resource_id) { is_expected.to eq(resource.id) }
      its(:success) { is_expected.to be false }
      its(:results) { is_expected.to include(content: true, thumbnail: false) }
    end
  end
end
