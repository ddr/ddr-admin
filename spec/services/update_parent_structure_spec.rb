module Ddr
  RSpec.describe UpdateParentStructure, type: :service do
    subject { described_class.new(resource_id, parent_id) }

    let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }
    let(:parent_id) { Valkyrie::ID.new(SecureRandom.uuid) }

    describe '.call' do
      let(:notification_args) { ['test', DateTime.now - 2.seconds, DateTime.now - 1.second, 'testid', payload] }
      let(:payload) { { pid: resource_id, parent: parent_id, skip_structure_updates: skip_parent_update } }

      around do |example|
        prev_auto_update_structures = Ddr::Admin.auto_update_structures
        example.run
        Ddr::Admin.auto_update_structures = prev_auto_update_structures
      end

      describe 'auto update parent structure' do
        before { Ddr::Admin.auto_update_structures = true }

        describe 'skip parent structure updating' do
          let(:skip_parent_update) { true }

          it "does not update the parent's structure" do
            expect_any_instance_of(UpdateParentStructure).not_to receive(:run)
            UpdateParentStructure.call(*notification_args)
          end
        end

        describe 'do not skip parent structure updating' do
          let(:skip_parent_update) { false }

          it "updates the parent's structure" do
            expect_any_instance_of(UpdateParentStructure).to receive(:run)
            UpdateParentStructure.call(*notification_args)
          end
        end
      end

      describe 'do not auto update parent structure' do
        before { Ddr::Admin.auto_update_structures = false }

        describe 'do not skip parent structure updating' do
          let(:skip_parent_update) { false }

          it "updates the parent's structure" do
            expect_any_instance_of(UpdateParentStructure).not_to receive(:run)
            UpdateParentStructure.call(*notification_args)
          end
        end
      end
    end

    describe '#run' do
      describe 'object does not have a parent' do
        let(:parent_id) { nil }

        it "does not update parent's structure" do
          expect(subject).not_to receive(:calculate_parent_structure)
          subject.run
        end
      end

      describe 'object has a parent' do
        it "updates parent's structure" do
          expect(subject).to receive(:calculate_parent_structure).with(parent_id)
          subject.run
        end
      end
    end
  end
end
