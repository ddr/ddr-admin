module Ddr
  RSpec.describe ReindexCollectionContents do
    describe 'triggering' do
      let(:collection) { FactoryBot.create_for_repository(:collection) }

      describe 'on collection update' do
        before { allow(IndexService).to receive(:call) }

        describe 'when admin_set changed' do
          it 'reindexes' do
            change_set = Ddr::ResourceChangeSet.change_set_for(collection)
            change_set.admin_set = 'dvs'
            Ddr::ResourceChangeSetPersister.new.save(change_set:)
            expect(IndexService).to have_received(:call)
          end
        end

        describe 'when title changed' do
          it 'reindexes' do
            change_set = Ddr::ResourceChangeSet.change_set_for(collection)
            change_set.title = 'Changed Title'
            Ddr::ResourceChangeSetPersister.new.save(change_set:)
            expect(IndexService).to have_received(:call)
          end
        end

        describe 'when access_role changed' do
          it 'reindexes' do
            change_set = Ddr::ResourceChangeSet.change_set_for(collection)
            change_set.roles += [FactoryBot.build(:role, :policy, :viewer, :public)]
            Ddr::ResourceChangeSetPersister.new.save(change_set:)
            expect(IndexService).to have_received(:call)
          end
        end

        describe 'when description changed' do
          it 'does not reindex' do
            change_set = Ddr::ResourceChangeSet.change_set_for(collection)
            change_set.description = 'A new description for this resource'
            Ddr::ResourceChangeSetPersister.new.save(change_set:)
            expect(IndexService).not_to have_received(:call)
          end
        end
      end
    end

    describe '.call' do
      describe 'error conditions' do
        specify do
          expect { described_class.call(Valkyrie::ID.new(SecureRandom.uuid)) }
            .to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
        end

        specify do
          expect { described_class.call(nil) }.to raise_error(TypeError)
        end

        specify do
          expect { described_class.call(Ddr::Collection.new) }.to raise_error(ArgumentError)
        end

        specify do
          collection = FactoryBot.create_for_repository(:collection)
          expect { described_class.call(collection) }.not_to raise_error
          expect { described_class.call(collection.id) }.not_to raise_error
        end
      end

      describe 'non-error condition' do
        let(:collection) { create_for_repository(:collection, admin_set: 'dc') }
        let(:items) do
          [].tap do |i|
            i << create_for_repository(:item, parent_id: collection.id, admin_policy_id: collection.id.to_s)
          end
        end
        let(:components) do
          [].tap do |c|
            c << create_for_repository(:component, parent_id: items.first.id, admin_policy_id: collection.id.to_s)
          end
        end

        it 'reindexes the contents', skip: 'Needs to be re-written' do
          allow(UpdateIndexJob).to receive(:perform_later)
          described_class.call(collection.id)
          ids = (items + components).map(&:id).map(&:to_s)
          expect(UpdateIndexJob).to have_received(:perform_later).with(ids)
        end
      end
    end
  end
end
