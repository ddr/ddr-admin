module Ddr
  RSpec.describe UpdateDerivatives do
    let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }

    before do
      allow(Ddr::V3::ImageDerivativesUpdateService).to receive(:call)
    end

    describe 'derivatives need updating' do
      describe 'Ddr Resource notifications' do
        describe 'ingest' do
          let(:args) do
            ['ingest.ddr_resource', Time.now, Time.now, 'aa5451d1f6939dc800fa',
             { resource_id:, model: 'Ddr::Component', files_changed: ['content'] }]
          end

          it 'calls the image derivatives service' do
            described_class.call(*args)
            expect(Ddr::V3::ImageDerivativesUpdateService).to have_received(:call)
          end
        end

        describe 'update' do
          let(:args) do
            ['update.ddr_resource', Time.now, Time.now, 'aa5451d1f6939dc800fa',
             { resource_id:, model: 'Ddr::Component', files_changed: ['content'] }]
          end

          it 'calls the image derivatives service' do
            described_class.call(*args)
            expect(Ddr::V3::ImageDerivativesUpdateService).to have_received(:call)
          end
        end
      end

      describe 'Ddr File notifications' do
        describe 'delete' do
          let(:args) do
            ['delete.ddr_file', Time.now, Time.now, 'e73195bb190c1c9052ea',
             { resource_id:, file_profile: { ddr_file_type: :intermediate_file, file_path: 'testing' } }]
          end

          it 'calls the image derivatives service' do
            described_class.call(*args)
            expect(Ddr::V3::ImageDerivativesUpdateService).to have_received(:call)
          end
        end
      end
    end

    describe 'derivatives do not need updating' do
      describe 'no files changed' do
        let(:args) do
          ['update.ddr_resource', Time.now, Time.now, 'aa5451d1f6939dc800fa',
           { resource_id:, model: 'Ddr::Component', files_changed: [] }]
        end

        it 'does not call the image derivatives service' do
          described_class.call(*args)
          expect(Ddr::V3::ImageDerivativesUpdateService).not_to have_received(:call)
        end
      end

      describe 'non-relevant files changed' do
        let(:args) do
          ['update.ddr_resource', Time.now, Time.now, 'aa5451d1f6939dc800fa',
           { resource_id:, model: 'Ddr::Component', files_changed: [:caption] }]
        end

        it 'does not call the image derivatives service' do
          described_class.call(*args)
          expect(Ddr::V3::ImageDerivativesUpdateService).not_to have_received(:call)
        end
      end
    end
  end
end
