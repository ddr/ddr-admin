module Ddr
  RSpec.describe DeleteStoredFile do
    describe '.delete_file_by_identifier' do
      let(:component) { FactoryBot.create_for_repository(:component, :with_content_file) }
      let(:file_identifier) { component.content.file_identifier }
      let(:file_path) { component.content.file_path }

      it 'removes the stored file' do
        expect(::File.exist?(file_path)).to be true
        described_class.delete_file_by_identifier(file_identifier)
        expect(::File.exist?(file_path)).to be false
      end
    end
  end
end
