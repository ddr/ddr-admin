FactoryBot.define do
  factory :collection, class: 'Ddr::Collection' do
    to_create do |instance|
      instance.id = Valkyrie::ID.new(SecureRandom.uuid)
      instance.admin_policy_id = instance.id
      Ddr.persister.save(resource: instance)
    end

    title { 'Test Collection' }
    admin_set { 'dc' }

    # Why?
    sequence(:identifier) { |n| ['coll%05d' % n] }
  end
end
