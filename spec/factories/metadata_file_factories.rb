FactoryBot.define do
  factory :metadata_file, class: 'Ddr::MetadataFile' do
    user { FactoryBot.create(:user) }

    factory :metadata_file_csv do
      metadata { File.new(Rails.root.join('spec/fixtures/batch_update/metadata_csv.csv')) }
      profile { Rails.root.join('spec/fixtures/batch_update/METADATA_CSV.yml').to_s }
    end
  end
end
