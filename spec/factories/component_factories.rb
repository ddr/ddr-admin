FactoryBot.define do
  factory :component, class: 'Ddr::Component' do
    to_create do |instance|
      Valkyrie.config.metadata_adapter.persister.save(resource: instance)
    end

    title { ['Test Component'] }
    sequence(:identifier) { |n| ['cmp%05d' % n] }

    trait :with_content_file do
      after(:build) do |c|
        c.content = build(:ddr_file)
      end
    end

    trait :with_multires_image_file do
      after(:build) do |c|
        c.multires_image = build(:ddr_file, :ptif)
      end
    end

    trait :with_derived_image_file do
      after(:build) do |c|
        c.derived_image = build(:ddr_file, :jpg)
      end
    end

    trait :with_pdf_content_file do
      after(:build) do |c|
        c.content = build(:ddr_file, :pdf)
      end
    end

    trait :with_thumbnail_file do
      after(:build) do |c|
        c.thumbnail = build(:ddr_file, :png)
      end
    end
  end
end
