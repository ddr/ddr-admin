FactoryBot.define do
  factory :ingest_folder, class: 'Ddr::IngestFolder' do
    model { 'Ddr::Component' }
    base_path { 'base/path' }
    sub_path { 'subpath' }
    checksum_type { Ddr::Files::CHECKSUM_TYPE_SHA1 }
    collection_id { '26aa6ec5-d530-44c7-ad97-2e1ecc3a9d25' }
    parent_id_length { 1 }
  end

  trait :existing_parents do
    after :build do |obj|
      obj.add_parents = false
    end
  end
end
