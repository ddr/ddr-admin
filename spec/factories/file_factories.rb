FactoryBot.define do
  factory :ddr_file, class: 'Ddr::File' do
    tiff

    trait :iiif do
      after(:build) do |ddr_file|
        source_file_name = 'manifest.json'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files', source_file_name),
                                                   'application/json')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.file(source_file.path).hexdigest)]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'application/json'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :tiff do
      after(:build) do |ddr_file|
        source_file_name = 'imageA.tif'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files', source_file_name),
                                                   'image/tiff')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/tiff'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :mp4 do
      after(:build) do |ddr_file|
        source_file_name = 'video.mp4'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', source_file_name), 'video/mp4')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'video/mp4'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :pdf do
      after(:build) do |ddr_file|
        source_file_name = 'sample.pdf'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', source_file_name),
                                                   'application/pdf')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'application/pdf'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :jpg do
      after(:build) do |ddr_file|
        source_file_name = 'imageA.jpg'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', source_file_name), 'image/jpeg')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/jpeg'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :ptif do
      after(:build) do |ddr_file|
        source_file_name = 'imageA.ptif'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', source_file_name), 'image/tiff')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/tiff'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :png do
      after(:build) do |ddr_file|
        source_file_name = 'target.png'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', source_file_name), 'image/png')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/png'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :gif do
      after(:build) do |ddr_file|
        source_file_name = 'arrow1rightred_e0.gif'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', source_file_name), 'image/gif')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/gif'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :bmp do
      after(:build) do |ddr_file|
        source_file_name = 'test_image.bmp'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', source_file_name), 'image/bmp')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/bmp'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :ico do
      after(:build) do |ddr_file|
        source_file_name = 'dul_favicon.ico'
        source_file = Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'files', source_file_name), 'image/ico')
        stored_file = Ddr.storage_adapter.upload(file: source_file, resource: ddr_file,
                                                 original_filename: source_file_name)

        ddr_file.digest =
          [build(:ddr_digest,
                 type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                 value: Digest::SHA1.hexdigest(File.read(source_file.path)))]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/ico'
        ddr_file.original_filename = source_file_name
      end
    end
  end

  factory :empty_ddr_file, class: 'Ddr::File'
end
