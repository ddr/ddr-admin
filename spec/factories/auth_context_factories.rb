FactoryBot.define do
  factory :auth_context, class: 'Ddr::Auth::AuthContext' do
    association :user, strategy: :build
    env { {} }

    initialize_with { Ddr::Auth::AuthContextFactory.call(user, env) }

    trait :anonymous do
      user { nil }
    end

    trait :duke do
      association :user, :duke, strategy: :build
    end
  end
end
