FactoryBot.define do
  factory :batch_object_datastream, class: 'Ddr::Batch::BatchObjectDatastream' do
    factory :batch_object_add_datastream do
      operation { Ddr::Batch::BatchObjectDatastream::OPERATION_ADD }

      factory :batch_object_add_content_datastream do
        # name { Ddr::Datastreams::CONTENT }
        name { 'content' }
        payload { Rails.root.join('spec/fixtures/id001.tif').to_s }
        payload_type { Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME }
        checksum { '257fa1025245d2d2a60ae81ac7922ca9581ca314' }
        checksum_type { Ddr::Files::CHECKSUM_TYPE_SHA1 }
        # checksum { '120ad0814f207c45d968b05f7435034ecfee8ac1a0958cd984a070dad31f66f3' }
        # checksum_type { Ddr::Files::CHECKSUM_TYPE_SHA256 }
      end
    end
  end
end
