FactoryBot.define do
  factory :batch, class: 'Ddr::Batch::Batch' do
    name { 'Batch' }
    description { 'This is a batch of stuff to do.' }
    user { FactoryBot.create(:user) }

    factory :collection_creating_ingest_batch do
      after(:create) do |batch, _evaluator|
        # Collection
        coll = create(:collection_ingest_batch_object, resource_id: '92f175ab-79fe-4fac-97df-73eb1f2b4ae2',
                                                       batch:)
        # Item 1
        item = create(:item_ingest_batch_object, resource_id: '95512b95-17a6-4d3d-8c80-7555ccc815ad', batch:)
        create(:batch_object_add_parent, object: coll.resource_id, batch_object: item)
        # Component for Item 1
        comp = create(:component_ingest_batch_object, batch:)
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Item 2
        item = create(:item_ingest_batch_object, resource_id: 'f2ef33e8-69e7-4cc5-80c4-d82a86360489', batch:)
        create(:batch_object_add_parent, object: coll.resource_id, batch_object: item)
        # Components for Item 2
        comp = create(:component_ingest_batch_object, batch:)
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        comp = create(:component_ingest_batch_object, batch:)
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Target
        create(:target_ingest_batch_object, batch:)
        # Attachment
        create(:attachment_ingest_batch_object, batch:)
      end
    end

    factory :item_adding_ingest_batch do
      after(:create) do |batch, _evaluator|
        # Item 1
        item = create(:item_ingest_batch_object, resource_id: '78a198da-7cbd-4d72-b31e-3de7d147e111', batch:)
        create(:batch_object_add_parent, object: '80ad0744-fe56-422f-9d2e-d05289278def', batch_object: item)
        # Component for Item 1
        comp = create(:component_ingest_batch_object, batch:)
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Item 2
        item = create(:item_ingest_batch_object, resource_id: '6f5c444d-b483-4767-a9ef-5991ae940b84', batch:)
        create(:batch_object_add_parent, object: '80ad0744-fe56-422f-9d2e-d05289278def', batch_object: item)
        # Components for Item 2
        comp = create(:component_ingest_batch_object, batch:)
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        comp = create(:component_ingest_batch_object, batch:)
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Target
        create(:target_ingest_batch_object, batch:)
        # Attachment
        create(:attachment_ingest_batch_object, batch:)
      end
    end

    factory :interrupted_ingest_batch_loose_components do
      after(:create) do |batch, _evaluator|
        # Collection
        coll = create(:collection_ingest_batch_object, resource_id: '92f175ab-79fe-4fac-97df-73eb1f2b4ae2',
                                                       batch:, verified: 't', handled: 't')
        # Item 1
        item = create(:item_ingest_batch_object, resource_id: '78a198da-7cbd-4d72-b31e-3de7d147e111',
                                                 batch:, verified: 't', handled: 't')
        create(:batch_object_add_parent, object: coll.resource_id, batch_object: item)
        # Component for Item 1
        comp = create(:component_ingest_batch_object, batch:, verified: 't', handled: 't')
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Item 2
        item = create(:item_ingest_batch_object, resource_id: '6f5c444d-b483-4767-a9ef-5991ae940b84',
                                                 batch:, verified: 't', handled: 't')
        create(:batch_object_add_parent, object: coll.resource_id, batch_object: item)
        # Components for Item 2 (Failed)
        comp = create(:component_ingest_batch_object, batch:, verified: 'f', handled: 't')
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        comp = create(:component_ingest_batch_object, batch:, verified: 'f', handled: 't')
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Target
        create(:target_ingest_batch_object, batch:, verified: 't', handled: 't')
        # Attachment
        create(:attachment_ingest_batch_object, batch:, verified: 't', handled: 't')
      end
    end

    factory :interrupted_ingest_batch_failed_item do
      after(:create) do |batch, _evaluator|
        # Collection
        coll = create(:collection_ingest_batch_object, resource_id: '92f175ab-79fe-4fac-97df-73eb1f2b4ae2',
                                                       batch:, verified: 't', handled: 't')
        # Item 1
        item = create(:item_ingest_batch_object, resource_id: '78a198da-7cbd-4d72-b31e-3de7d147e111',
                                                 batch:, verified: 't', handled: 't')
        create(:batch_object_add_parent, object: coll.resource_id, batch_object: item)
        # Component for Item 1
        comp = create(:component_ingest_batch_object, batch:, verified: 't', handled: 't')
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Item 2 (Failed)
        item = create(:item_ingest_batch_object, resource_id: '6f5c444d-b483-4767-a9ef-5991ae940b84',
                                                 batch:, verified: 'f', handled: 't')
        create(:batch_object_add_parent, object: coll.resource_id, batch_object: item)
        # Components for Item 2
        comp = create(:component_ingest_batch_object, batch:, verified: 'f', handled: 't')
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        comp = create(:component_ingest_batch_object, batch:, verified: 'f', handled: 't')
        create(:batch_object_add_parent, object: item.resource_id, batch_object: comp)
        # Target
        create(:target_ingest_batch_object, batch:, verified: 't', handled: 't')
        # Attachment
        create(:attachment_ingest_batch_object, batch:, verified: 't', handled: 't')
      end
    end

    factory :item_update_batch do
      after(:create) do |batch, _evaluator|
        create(:update_batch_object, model: 'Item', batch:)
        create(:update_batch_object, model: 'Item', batch:)
        create(:update_batch_object, model: 'Item', batch:)
      end
    end

    factory :failed_item_update_batch do
      after(:create) do |batch, _evaluator|
        create(:update_batch_object, model: 'Item', batch:, verified: 't', handled: 't')
        create(:update_batch_object, model: 'Item', batch:, verified: 'f', handled: 't')
        create(:update_batch_object, model: 'Item', batch:, verified: 't', handled: 't')
      end
    end

    factory :batch_with_basic_ingest_batch_objects do
      transient do
        object_count { 3 }
      end
      after(:create) do |batch, evaluator|
        FactoryBot.create_list(:basic_ingest_batch_object, evaluator.object_count, batch:)
      end
    end

    factory :batch_with_generic_ingest_batch_objects do
      transient do
        object_count { 3 }
      end
      after(:create) do |batch, evaluator|
        FactoryBot.create_list(:generic_ingest_batch_object_with_attributes, evaluator.object_count, batch:)
      end
    end

    factory :batch_with_basic_update_batch_object do
      after(:create) do |batch|
        FactoryBot.create(:basic_update_batch_object, batch:)
      end
    end

    factory :batch_with_basic_clear_attribute_batch_object do
      after(:create) do |batch|
        FactoryBot.create(:basic_update_clear_attribute_batch_object, batch:)
      end
    end

    factory :batch_with_basic_clear_all_and_add_batch_object do
      after(:create) do |batch|
        FactoryBot.create(:basic_clear_all_and_add_batch_object, batch:)
      end
    end
  end
end
