module Ddr
  module Batch
    describe BatchProcessorRunMailer, :batch, type: :mailer do
      let(:user) { FactoryBot.create(:user) }
      let(:collection_title) { 'Test & Drive Collection: An Auto Deposit' }
      let(:munged_collection_title) { 'Test_Drive_Collection_An_Auto_Deposit' }
      let(:batch) do
        Ddr::Batch::Batch.new(id: 54,
                              user_id: user.id,
                              status: Batch::STATUS_FINISHED,
                              outcome: Batch::OUTCOME_SUCCESS,
                              collection_title:)
      end

      it 'sends a notification' do
        Ddr::Batch::BatchProcessorRunMailer.send_notification(batch).deliver_now!
        expect(ActionMailer::Base.deliveries).not_to be_empty
        email = ActionMailer::Base.deliveries.first
        expect(email.to).to eq([user.email])
        expect(email.subject).to include("Batch Processor Run #{batch.status} #{batch.outcome} - #{collection_title}")
        expect(email.to_s).to include(collection_title)
        expect(email.to_s).to include("Objects in batch: #{batch.batch_objects.count}")
        expect(email.to_s).to include(Batch::OUTCOME_SUCCESS)
        expect(email.attachments.first.filename).to eq("details_#{munged_collection_title}.csv")
      end
    end
  end
end
