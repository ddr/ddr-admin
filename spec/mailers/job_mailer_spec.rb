module Ddr
  RSpec.describe JobMailer, type: :mailer do
    it 'generates an email' do
      described_class.basic(subject: 'Job', to: 'user@example.com', message: 'Job completed').deliver_now!
      expect(ActionMailer::Base.deliveries).not_to be_empty
    end
  end
end
