def factory_symbol
  object_model.split('::').last.underscore
end

def object_model
  described_class.to_s.sub('Controller', '').singularize
end

def object_class
  object_model.constantize
end

def update_metadata
  put :update, params: { id: object, optimistic_lock_token: [lock_token.first.to_s],
                         descMetadata: { title: ['Updated'] }, comment: 'Just for fun!' }
end

def update_admin_metadata
  patch :admin_metadata, params: { id: object, optimistic_lock_token: [object.optimistic_lock_token.first.to_s],
                                   adminMetadata: { local_id: 'foobar' }, comment: 'This is serious!' }
end

def update_roles
  patch :roles, params: { id: object, optimistic_lock_token: [object.optimistic_lock_token.first.to_s],
                          roles: '[ { "agent":"public","role_type":"Viewer","scope":"resource" } ]' }
end

shared_examples 'a repository object controller' do
  describe '#new' do
    context 'when the user can create an object of this type' do
      before { controller.current_ability.can(:create, object_class) }

      it 'renders the new template' do
        new_object.call
        expect(response).to render_template(:new)
      end
    end

    context 'when the user cannot create an object of this type' do
      before { controller.current_ability.cannot(:create, object_class) }

      it 'is unauthorized' do
        new_object.call
        expect(response.response_code).to eq(403)
      end
    end
  end

  describe '#create' do
    context 'when the user can create an object of this type' do
      before { controller.current_ability.can(:create, object_class) }

      it 'persists the object' do
        expect { create_object.call }.to change { Ddr.query_service.find_all_of_model(model: object_class).count }.by(1)
      end

      it 'grants roles to the creator' do
        allow(controller).to receive(:current_object).and_return(object_class.new)
        allow(controller.change_set).to receive(:grant_roles_to_creator).with(user)
        create_object.call
        expect(controller.change_set).to have_received(:grant_roles_to_creator).with(user)
      end

      it 'redirects after creating the new object' do
        expect(controller).to receive(:after_create_redirect).and_call_original
        create_object.call
        expect(response).to be_redirect
      end
    end

    context 'when the user cannot create objects of this type' do
      before { controller.current_ability.cannot(:create, object_class) }

      it 'is unauthorized' do
        create_object.call
        expect(response.response_code).to eq(403)
      end
    end
  end

  describe '#edit' do
    let(:object) { create_for_repository(factory_symbol) }

    context 'when the user can edit the object' do
      let(:role) { build(:role, agent: user.user_key, type: 'Editor', scope: 'resource') }

      before do
        object.roles += [role]
        Ddr.persister.save(resource: object)
      end

      it 'renders the edit template' do
        get :edit, params: { id: object.id.id }
        expect(response).to render_template('edit')
      end
    end

    context 'when the user cannot edit the object' do
      it 'is unauthorized' do
        get :edit, params: { id: object.id.id }
        expect(response.response_code).to eq(403)
      end
    end
  end

  describe '#update' do
    context 'when the user can edit' do
      let(:role) { build(:role, agent: user.user_key, type: 'Editor', scope: 'resource') }
      let(:object) { create_for_repository(factory_symbol, roles: [role]) }
      let!(:lock_token) { object.optimistic_lock_token }

      describe 'without a lock token conflict' do
        it 'redirects to the descriptive metadata tab of the show page' do
          update_metadata
          expect(response).to redirect_to(action: 'show', id: object, tab: 'descriptive_metadata')
        end

        it 'updates the object' do
          update_metadata
          updated_object = Ddr.query_service.find_by(id: object.id)
          expect(updated_object.title).to eq(['Updated'])
        end

        it 'creates an update success event' do
          expect { update_metadata }.to change {
                                          Ddr::Events::UpdateEvent.for_object(object).where(outcome: Ddr::Events::Event::SUCCESS).count
                                        }.by(1)
        end

        it 'records the comment in the update event' do
          update_metadata
          expect(Ddr::Events::UpdateEvent.for_object(object).last.comment).to eq 'Just for fun!'
        end

        it 'adds an action-specific summary to the event' do
          update_metadata
          expect(Ddr::Events::UpdateEvent.for_object(object).last.summary).to eq 'Descriptive metadata updated'
        end
      end

      describe 'with a lock token conflict' do
        before do
          object.creator = ['Sam']
          Ddr.persister.save(resource: object)
        end

        it 'redirects to the descriptive metadata tab of the show page' do
          update_metadata
          expect(response).to redirect_to(action: 'show', id: object, tab: 'descriptive_metadata')
        end

        it 'does not update the object' do
          update_metadata
          updated_object = Ddr.query_service.find_by(id: object.id)
          expect(updated_object.title).not_to eq(['Updated'])
        end

        it 'creates an update failure event' do
          expect { update_metadata }.to(change do
                                          Ddr::Events::UpdateEvent.for_object(object).where(outcome: Ddr::Events::Event::FAILURE).count
                                        end)
        end
      end
    end

    context 'when the user cannot edit' do
      let(:object) { create_for_repository(factory_symbol) }
      let!(:lock_token) { object.optimistic_lock_token }

      it 'is unauthorized' do
        update_metadata
        expect(response.response_code).to eq(403)
      end
    end
  end

  describe '#show' do
    context 'when the user can read the object' do
      it 'renders the show template' do
        object = FactoryBot.build(factory_symbol)
        object.roles += [FactoryBot.build(:role, :viewer, agent: user)]
        object = Ddr.persister.save(resource: object)
        expect(get(:show, params: { id: object })).to render_template(:show)
      end
    end

    context 'when the user cannot read the object' do
      it 'is unauthorized' do
        object = FactoryBot.build(factory_symbol)
        object = Ddr.persister.save(resource: object)
        get :show, params: { id: object }
        expect(response.response_code).to eq(403)
      end
    end
  end

  describe '#roles' do
    describe 'GET' do
      context 'when the user can grant roles' do
        it 'renders the roles template' do
          object = FactoryBot.build(factory_symbol)
          object.roles += [FactoryBot.build(:role, :curator, agent: user)]
          object = Ddr.persister.save(resource: object)
          expect(get(:roles, params: { id: object })).to render_template(:roles)
        end
      end

      context 'when the user cannot grant roles' do
        let(:object) { Ddr.persister.save(resource: FactoryBot.build(factory_symbol)) }

        it 'is unauthorized' do
          get :roles, params: { id: object }
          expect(response.response_code).to eq(403)
        end
      end
    end

    describe 'PATCH' do
      context 'when the user can update roles' do
        let(:role) { build(:role, agent: user.user_key, type: 'Curator', scope: 'resource') }
        let(:object) { create_for_repository(factory_symbol, roles: [role]) }
        let!(:lock_token) { object.optimistic_lock_token }

        describe 'without a lock token conflict' do
          it 'redirects to the roles tab of the show page' do
            update_roles
            expect(response).to redirect_to(action: 'show', id: object, tab: 'roles')
          end

          it 'updates the object' do
            update_roles
            updated_object = Ddr.query_service.find_by(id: object.id)
            expect(updated_object.roles)
              .to include(having_attributes(agent: 'public', role_type: 'Viewer', scope: 'resource'))
          end

          it 'creates an update success event' do
            expect { update_roles }.to change {
                                         Ddr::Events::UpdateEvent.for_object(object).where(outcome: Ddr::Events::Event::SUCCESS).count
                                       }.by(1)
          end

          it 'adds an action-specific summary to the event' do
            update_roles
            expect(Ddr::Events::UpdateEvent.for_object(object).last.summary).to eq 'Roles updated'
          end
        end

        describe 'with a lock token conflict' do
          before do
            object.creator = ['Sam']
            Ddr.persister.save(resource: object)
          end

          it 'redirects to the roles tab of the show page' do
            update_roles
            expect(response).to redirect_to(action: 'show', id: object, tab: 'roles')
          end

          it 'does not update the object' do
            update_roles
            updated_object = Ddr.query_service.find_by(id: object.id)
            expect(updated_object.roles)
              .not_to include(having_attributes(agent: 'public', role_type: 'Viewer', scope: 'resource'))
          end

          it 'creates an update failure event' do
            expect { update_roles }.to change {
                                         Ddr::Events::UpdateEvent.for_object(object).where(outcome: Ddr::Events::Event::FAILURE).count
                                       }.by(1)
          end
        end
      end

      context 'when the user cannot update admin metadata' do
        let(:object) { create_for_repository(factory_symbol) }

        it 'is unauthorized' do
          update_roles
          expect(response.response_code).to eq(403)
        end
      end
    end
  end

  describe '#admin_metadata' do
    describe 'GET' do
      context 'when the user can update admin metadata' do
        let(:role) { build(:role, agent: user.user_key, type: 'Editor', scope: 'resource') }
        let(:object) { create_for_repository(factory_symbol, roles: [role]) }

        it 'renders the admin_metadata template' do
          get :admin_metadata, params: { id: object.id.id }
          expect(response).to render_template('admin_metadata')
        end
      end

      context 'when the user cannot update admin metadata' do
        let(:object) { create_for_repository(factory_symbol) }

        it 'is unauthorized' do
          get :admin_metadata, params: { id: object.id.id }
          expect(response.response_code).to eq(403)
        end
      end
    end

    describe 'PATCH' do
      context 'when the user can update admin metadata' do
        let(:role) { build(:role, agent: user.user_key, type: 'Editor', scope: 'resource') }
        let(:object) { create_for_repository(factory_symbol, roles: [role]) }
        let!(:lock_token) { object.optimistic_lock_token }

        describe 'without a lock token conflict' do
          it 'redirects to the admin metadata tab of the show page' do
            update_admin_metadata
            expect(response).to redirect_to(action: 'show', id: object, tab: 'admin_metadata')
          end

          it 'updates the object' do
            update_admin_metadata
            updated_object = Ddr.query_service.find_by(id: object.id)
            expect(updated_object.local_id).to eq('foobar')
          end

          it 'creates an update success event' do
            expect { update_admin_metadata }.to change {
                                                  Ddr::Events::UpdateEvent.for_object(object).where(outcome: Ddr::Events::Event::SUCCESS).count
                                                }.by(1)
          end

          it 'records the comment in the update event' do
            update_admin_metadata
            expect(Ddr::Events::UpdateEvent.for_object(object).last.comment).to eq 'This is serious!'
          end

          it 'adds an action-specific summary to the event' do
            update_admin_metadata
            expect(Ddr::Events::UpdateEvent.for_object(object).last.summary).to eq 'Administrative metadata updated'
          end
        end

        describe 'with a lock token conflict' do
          before do
            object.creator = ['Sam']
            Ddr.persister.save(resource: object)
          end

          it 'redirects to the admin metadata tab of the show page' do
            update_admin_metadata
            expect(response).to redirect_to(action: 'show', id: object, tab: 'admin_metadata')
          end

          it 'does not update the object' do
            update_admin_metadata
            updated_object = Ddr.query_service.find_by(id: object.id)
            expect(updated_object.local_id).not_to eq('foobar')
          end

          it 'creates an update failure event' do
            expect { update_admin_metadata }.to change {
                                                  Ddr::Events::UpdateEvent.for_object(object).where(outcome: Ddr::Events::Event::FAILURE).count
                                                }.by(1)
          end
        end
      end

      context 'when the user cannot update admin metadata' do
        let(:object) { create_for_repository(factory_symbol) }

        it 'is unauthorized' do
          update_admin_metadata
          expect(response.response_code).to eq(403)
        end
      end
    end
  end
end
