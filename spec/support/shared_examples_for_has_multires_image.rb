RSpec.shared_examples 'a resource that can have a multires image' do
  it { is_expected.not_to have_multires_image }
  its(:can_have_multires_image?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:multires_image) }

  describe 'when multires image is present' do
    subject { described_class.new(multires_image:) }

    let(:resource) { Ddr::Resource.new }
    let(:multires_image_file_name) { 'imageC.ptif' }
    let(:multires_image_file_upload) do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', multires_image_file_name), 'image/tiff')
    end
    let(:multires_image_file) do
      Ddr.storage_adapter.upload(file: multires_image_file_upload, resource:,
                                 original_filename: multires_image_file_name)
    end
    let(:multires_image) do
      Ddr::File.new(file_identifier: multires_image_file.id, media_type: 'text/vtt',
                    original_filename: multires_image_file_name)
    end

    specify do
      expect(subject).to have_multires_image
    end

    describe '#multires_image_file_path' do
      it 'returns the disk path of the multires image file' do
        expect(subject.multires_image_file_path).to eq(multires_image.file.disk_path)
      end
    end
  end
end
