RSpec.shared_examples 'a publishable resource controller' do |model_class|
  let(:viewer_role) { FactoryBot.build(:role, :viewer, agent: user) }
  let(:curator_role) { FactoryBot.build(:role, :curator, agent: user) }
  let(:user) { FactoryBot.create(:user) }
  let(:resource) do
    if model_class.respond_to?(:parent_class)
      parent = create_for_repository(model_class.parent_class.to_sym, workflow_state: 'published')
      create_for_repository(model_class.to_sym, workflow_state:, parent_id: parent.id,
                                                access_role: [role])
    else
      create_for_repository(model_class.to_sym, workflow_state:, access_role: [role])
    end
  end

  before { sign_in user }

  describe '#publish' do
    let(:workflow_state) { 'unpublished' }

    describe 'when the user can publish the resource' do
      let(:role) { curator_role }

      xit 'publishes the resource' do
        allow(Ddr::V3::PublicationService::Publish.service_job).to receive(:perform_later).with({
                                                                                                  dependent: false, resource: resource.id_s, on_behalf_of: user.email
                                                                                                })
        get :publish, params: { id: resource, confirmed: 'true' }
        expect(Ddr::V3::PublicationService::Publish.service_job).to have_received(:perform_later).with({
                                                                                                         dependent: false, resource: resource.id_s, on_behalf_of: user.email
                                                                                                       })
      end
    end

    describe 'when the user cannot publish the resource' do
      let(:role) { viewer_role }

      it 'is unauthorized' do
        get :publish, params: { id: resource }
        expect(response.response_code).to eq 403
      end
    end
  end

  describe '#unpublish' do
    let(:workflow_state) { 'published' }

    describe 'when the user can un-publish the resource' do
      let(:role) { curator_role }

      xit 'unpublishes the resource' do
        allow(Ddr::V3::PublicationService::Unpublish.service_job).to receive(:perform_later).with({
                                                                                                    dependent: false, resource: resource.id_s, on_behalf_of: user.email
                                                                                                  })
        get :unpublish, params: { id: resource, confirmed: 'true' }
        expect(Ddr::V3::PublicationService::Unpublish.service_job).to have_received(:perform_later).with({
                                                                                                           dependent: false, resource: resource.id_s, on_behalf_of: user.email
                                                                                                         })
      end
    end

    describe 'when the user cannot unpublish the resource' do
      let(:role) { viewer_role }

      it 'is unauthorized' do
        get :unpublish, params: { id: resource }
        expect(response.response_code).to eq 403
      end
    end
  end
end
