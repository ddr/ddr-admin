RSpec.shared_examples 'a resource with administrative metadata' do
  describe 'contacts' do
    subject { FactoryBot.build(:item) }

    describe '#research_help' do
      before { subject.research_help_contact = 'dvs' }

      it 'returns the appropriate contact' do
        expect(subject.research_help.slug).to eq('dvs')
      end
    end
  end
end
