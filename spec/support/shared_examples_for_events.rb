def mock_object(opts = {})
  double('object', { create_date: '2014-01-01T01:01:01.000Z', modified_date: '2014-06-01T01:01:01.000Z' }.merge(opts))
end

RSpec.shared_examples 'a preservation-related event' do
  subject { described_class.new }

  it 'has an event_type' do
    expect(subject.preservation_event_type).not_to be_nil
  end
end

RSpec.shared_examples 'an event that reindexes its object after save' do
  it 'implements the reindexing concern' do
    expect(subject).to be_a Ddr::Events::ReindexObjectAfterSave
  end

  context 'when object is present' do
    let(:object) { Ddr::Resource.new }

    before do
      allow(subject).to receive(:object) { object }
    end

    it 'reindexes its object after save' do
      expect(Ddr::IndexService).to receive(:new).with(object.id).and_call_original
      expect_any_instance_of(Ddr::IndexService).to receive(:index)
      subject.save(validate: false)
    end
  end
end

RSpec.shared_examples 'an event' do
  describe 'validation' do
    it 'requires a resource_id' do
      expect(subject).not_to be_valid
      expect(subject.errors[:resource_id]).to include "can't be blank"
    end

    it 'requires an event_date_time' do
      allow(subject).to receive(:event_date_time).and_return(nil) # b/c set to default after init
      expect(subject).not_to be_valid
      expect(subject.errors[:event_date_time]).to include "can't be blank"
    end

    it 'requires a valid outcome' do
      subject.outcome = Ddr::Events::Event::SUCCESS
      subject.valid?
      expect(subject.errors).not_to have_key :outcome
      subject.outcome = Ddr::Events::Event::FAILURE
      subject.valid?
      expect(subject.errors).not_to have_key :outcome
      subject.outcome = 'Some other value'
      expect(subject).not_to be_valid
      expect(subject.errors[:outcome]).to include '"Some other value" is not a valid event outcome'
    end
  end

  describe 'outcome setters and getters' do
    it 'encapsulates access' do
      subject.success!
      expect(subject.outcome).to eq Ddr::Events::Event::SUCCESS
      expect(subject).to be_success
      subject.failure!
      expect(subject.outcome).to eq Ddr::Events::Event::FAILURE
      expect(subject).to be_failure
    end
  end

  describe 'setting defaults' do
    context 'after initialization' do
      its(:outcome) { is_expected.to eq Ddr::Events::Event::SUCCESS }
      its(:event_date_time) { is_expected.to be_present }
      its(:software) { is_expected.to eq Ddr::Events::Event::DDR_SOFTWARE }
      its(:summary) { is_expected.to eq described_class.description }
    end

    context 'when attributes are set, they are not eoverwritten' do
      subject do
        described_class.new(resource_id: obj.id, outcome: Ddr::Events::Event::FAILURE, event_date_time: Time.utc(2013),
                            software: 'Test', summary: 'A terrible disaster')
      end

      let(:obj) { Ddr::Resource.new }

      its(:outcome) { is_expected.to eq Ddr::Events::Event::FAILURE }
      its(:event_date_time) { is_expected.to eq Time.utc(2013) }
      its(:software) { is_expected.to eq 'Test' }
      its(:summary) { is_expected.to eq 'A terrible disaster' }
    end
  end

  describe 'object getter' do
    subject { described_class.new(resource_id: vid) }

    let(:vid) { build(:valkyrie_id) }

    let!(:resource) do
      Ddr.persister.save(resource: Ddr::Resource.new(id: vid))
    end

    it 'retrieves the object' do
      expect(subject.object.id).to eq resource.id
    end
  end

  describe 'object setter' do
    describe 'when the object is persisted' do
      let(:object) { create_for_repository(:item) }

      it 'sets the event resource_id' do
        subject.object = object
        expect(subject.resource_id).to eq object.id.to_s
      end
    end

    describe 'when the object is not persisted' do
      let(:object) { build(:item) }

      it 'raises an ArgumentError' do
        allow(object).to receive(:new_record?).and_return(true)
        expect { subject.object = object }.to raise_error ArgumentError
      end
    end
  end

  describe 'event_date_time string representation' do
    subject { described_class.new(event_date_time: Time.utc(2014, 6, 4, 11, 7, 35)) }

    it 'conforms to the specified format' do
      expect(subject.event_date_time_s).to eq '2014-06-04T11:07:35.000Z'
    end
  end

  describe 'rendering who/what performed the action' do
    context 'when performed by a user' do
      let(:user) { build(:user) }

      before do
        subject.user = user
      end

      it 'renders the user' do
        expect(subject.performed_by).to eq(user.user_key)
      end
    end

    context 'when not performed by a user' do
      it "renders 'SYSTEM'" do
        expect(subject.performed_by).to eq 'SYSTEM'
      end
    end
  end

  describe 'when the exception attribute is set' do
    before { subject.exception = 'Gah!' }

    it 'is persisted as a failure' do
      subject.save(validate: false)
      expect(subject).to be_failure
    end
  end
end
