RSpec.shared_examples 'a publishable resource' do
  subject { described_class.new(workflow_state:) }

  context 'when published' do
    let(:workflow_state) { 'published' }

    it { is_expected.to be_published }

    it { is_expected.not_to be_previewable } if Ddr::Admin.publication_preview_enabled
    it { is_expected.not_to be_unpublished }
    it { is_expected.not_to be_nonpublishable }
  end

  context 'when unpublished' do
    let(:workflow_state) { 'unpublished' }

    it { is_expected.not_to be_published }

    it { is_expected.not_to be_previewable } if Ddr::Admin.publication_preview_enabled
    it { is_expected.to be_unpublished }
    it { is_expected.not_to be_nonpublishable }
  end

  context 'when workflow_state is nil' do
    let(:workflow_state) { nil }

    it { is_expected.not_to be_published }

    it { is_expected.not_to be_previewable } if Ddr::Admin.publication_preview_enabled
    it { is_expected.to be_unpublished }
    it { is_expected.not_to be_nonpublishable }
  end

  context 'when nonpublishable' do
    let(:workflow_state) { 'nonpublishable' }

    it { is_expected.not_to be_published }

    it { is_expected.not_to be_previewable } if Ddr::Admin.publication_preview_enabled
    it { is_expected.not_to be_unpublished }
    it { is_expected.to be_nonpublishable }
  end

  if Ddr::Admin.publication_preview_enabled
    context 'when previewable' do
      let(:workflow_state) { 'previewable' }

      it { is_expected.not_to be_published }
      it { is_expected.to be_previewable }
      it { is_expected.not_to be_unpublished }
      it { is_expected.not_to be_nonpublishable }
    end
  end
end
