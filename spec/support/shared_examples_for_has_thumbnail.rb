RSpec.shared_examples 'a resource that can have a thumbnail' do
  its(:can_have_thumbnail?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:thumbnail) }

  describe 'when thumbnail file is present' do
    subject { described_class.new(thumbnail:) }

    let(:resource) { Ddr::Resource.new }
    let(:thumbnail_file_name) { 'imageA.jpg' }
    let(:thumbnail_file_upload) do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', thumbnail_file_name), 'image/jpeg')
    end
    let(:thumbnail_file) do
      Ddr.storage_adapter.upload(file: thumbnail_file_upload, resource:, original_filename: thumbnail_file_name)
    end
    let(:thumbnail) do
      Ddr::File.new(file_identifier: thumbnail_file.id, media_type: 'text/jpg', original_filename: thumbnail_file_name)
    end

    it { is_expected.to have_thumbnail }

    describe '#caption_path' do
      it 'returns the disk path of the thumbnail file' do
        expect(subject.thumbnail_path).to eq(thumbnail.file.disk_path)
      end
    end
  end

  describe 'when thumbnail file is not present' do
    it { is_expected.not_to have_thumbnail }
    its(:thumbnail_path) { is_expected.to be_nil }
  end
end
