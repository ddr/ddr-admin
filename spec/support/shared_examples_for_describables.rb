RSpec.shared_examples 'a describable resource' do
  describe '#desc_metadata_terms' do
    it 'has a default value' do
      expect(subject.desc_metadata_terms).to eq Ddr::Describable.term_names
    end
  end

  describe '#set_desc_metadata' do
    let(:term_values_hash) { subject.desc_metadata_terms.index_with { |_t| ['Value'] } }

    it 'sets the descMetadata terms to the values of the matching keys in the hash' do
      subject.desc_metadata_terms.each do |t|
        expect(subject).to receive(:set_value).with(t, ['Value'])
      end
      subject.set_desc_metadata(term_values_hash)
    end
  end
end
