RSpec.shared_examples 'a resource that can have structural metadata' do
  its(:can_have_struct_metadata?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:struct_metadata) }

  describe 'when struct_metadata file is present' do
    subject { described_class.new(struct_metadata:) }

    let(:resource) { Ddr::Resource.new }
    let(:struct_metadata_name) { 'mets.xml' }
    let(:struct_metadata_upload) do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', struct_metadata_name), 'application/xml')
    end
    let(:struct_metadata_file) do
      Ddr.storage_adapter.upload(file: struct_metadata_upload, resource:, original_filename: struct_metadata_name)
    end
    let(:struct_metadata) do
      Ddr::File.new(file_identifier: struct_metadata_file.id, media_type: 'application/xml',
                    original_filename: struct_metadata_name)
    end

    it { is_expected.to have_struct_metadata }

    describe '#structure' do
      its(:structure) { is_expected.to be_a Ddr::Structure }
    end
  end

  describe 'when struct_metadata file is not present' do
    it { is_expected.not_to have_struct_metadata }
    its(:struct_metadata) { is_expected.to be_nil }
  end
end
