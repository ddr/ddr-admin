RSpec.shared_examples 'a resource that can have a derived image' do
  it { is_expected.not_to have_derived_image }
  its(:can_have_derived_image?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:derived_image) }

  describe 'when derived image is present' do
    subject { described_class.new(derived_image:) }

    let(:resource) { Ddr::Resource.new }
    let(:derived_image_file_name) { 'imageA.jpg' }
    let(:derived_image_file_upload) do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', derived_image_file_name), 'image/jpeg')
    end
    let(:derived_image_file) do
      Ddr.storage_adapter.upload(file: derived_image_file_upload, resource:, original_filename: derived_image_file_name)
    end

    let(:derived_image) do
      Ddr::File.new(file_identifier: derived_image_file.id, media_type: 'image/jpeg',
                    original_filename: derived_image_file_name)
    end

    specify do
      expect(subject).to have_derived_image
    end

    describe '#derived_image_file_path' do
      it 'returns the disk path of the derived image file' do
        expect(subject.derived_image_file_path).to eq(derived_image.file.disk_path)
      end
    end
  end
end
