RSpec.shared_examples 'a resource that can be captioned' do
  its(:captionable?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:caption) }

  describe 'when caption file is present' do
    subject { described_class.new(caption:) }

    let(:resource) { Ddr::Resource.new }
    let(:caption_file_name) { 'abcd1234.vtt' }
    let(:caption_file_upload) do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', caption_file_name), 'text/vtt')
    end
    let(:caption_file) do
      Ddr.storage_adapter.upload(file: caption_file_upload, resource:, original_filename: caption_file_name)
    end
    let(:caption) do
      Ddr::File.new(file_identifier: caption_file.id, media_type: 'text/vtt', original_filename: caption_file_name)
    end

    it { is_expected.to be_captioned }

    describe '#caption_path' do
      it 'returns the disk path of the caption file' do
        expect(subject.caption_path).to eq(caption.file.disk_path)
      end
    end

    describe '#caption_type' do
      it 'returns the media type of the caption file' do
        expect(subject.caption_type).to eq(caption.media_type)
      end
    end

    describe '#caption_extension' do
      it 'returns the file extension of the caption file' do
        expect(subject.caption_extension).to eq(File.extname(caption_file_name))
      end
    end
  end

  describe 'when caption file is not present' do
    it { is_expected.not_to be_captioned }
    its(:caption_path) { is_expected.to be_nil }
    its(:caption_type) { is_expected.to be_nil }
    its(:caption_extension) { is_expected.to be_nil }
  end
end
