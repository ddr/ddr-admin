def factory_symbol
  object_model.split('::').last.underscore
end

def object_model
  described_class.to_s.sub('Controller', '').singularize
end

def object_class
  object_model.constantize
end

def upload_file
  patch :upload, params: { id: object.id.id, optimistic_lock_token: [object.optimistic_lock_token.first.to_s],
                           content: { ddr_file_type: :thumbnail, file: } }
end

shared_examples 'a content resource controller' do
  describe '#upload' do
    let(:file) { fixture_file_upload('../imageA.jpg') }

    context 'when the user can upload content' do
      let(:object) { create_for_repository(factory_symbol) }
      let!(:lock_token) { object.optimistic_lock_token }

      before do
        allow(controller.current_ability).to receive(:can?).with(:upload, object_class).and_return(true)
      end

      describe 'without a lock token conflict' do
        it 'updates the appropriate DDR File' do
          upload_file
          expect(Ddr.query_service.find_by(id: object.id).thumbnail).not_to be_nil
        end
      end

      describe 'with a lock token conflict' do
        before do
          object.title = 'Updated Title'
          Ddr.persister.save(resource: object)
        end

        it 'does not update the appropriate DDR File' do
          upload_file
          expect(Ddr.query_service.find_by(id: object.id).thumbnail).to be_nil
        end
      end
    end

    context 'when the user cannot upload content' do
      let(:object) { create_for_repository(factory_symbol) }
      let!(:lock_token) { object.optimistic_lock_token }

      it 'is unauthorized' do
        upload_file
        expect(response.response_code).to eq(403)
      end
    end
  end
end
