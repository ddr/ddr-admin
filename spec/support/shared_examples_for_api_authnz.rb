module Ddr::API
  RSpec.shared_examples 'an authorized API call' do |*api|
    it 'is not unauthorized' do
      send(*api)
      expect(response.status).not_to eq 401
    end
  end

  RSpec.shared_examples 'an unauthorized API call' do |*api|
    it 'is unauthorized' do
      send(*api)
      expect(response.status).to eq 401
    end
  end

  RSpec.shared_examples 'a forbidden API call' do |*api|
    it 'is forbidden' do
      send(*api)
      expect(response.status).to eq 403
    end
  end

  RSpec.shared_examples 'a successful API call' do |*api|
    it 'is successful' do
      send(*api)
      expect(response.status).to eq 200
    end
  end

  RSpec.shared_examples 'a not found API call' do |*api|
    it 'is not found' do
      send(*api)
      expect(response.status).to eq 404
    end
  end

  RSpec.shared_examples 'a bad request API call' do |*api|
    it 'is a bad request' do
      send(*api)
      expect(response.status).to eq 400
    end
  end

  RSpec.shared_examples 'an API that requires authentication' do |*api|
    describe 'without authentication' do
      it_behaves_like 'an unauthorized API call', *api
    end

    describe 'when logged in' do
      before { login_as user }

      it_behaves_like 'an authorized API call', *api
    end

    describe 'with a valid token' do
      before do
        allow(OAuthRequest).to receive(:call).with(:introspection_endpoint, token: 'zyzzyx')
                                             .and_return({ 'active' => true, 'user_id' => user.username })
      end

      it 'is not unauthorized' do
        send(*api, headers: { 'Authorization' => 'Bearer zyzzyx' })
        expect(response.status).not_to eq 401
      end
    end

    describe 'with an expired token' do
      before do
        allow(OAuthRequest).to receive(:call).with(:introspection_endpoint, token: 'zyzzyx')
                                             .and_return({ 'active' => false, 'user_id' => user.username })
      end

      it 'is unauthorized' do
        send(*api, headers: { 'Authorization' => 'Bearer zyzzyx' })
        expect(response.status).to eq 401
      end
    end
  end

  RSpec.shared_examples 'a curator-only API' do |*api|
    describe 'without authentication' do
      it_behaves_like 'an unauthorized API call', *api
    end

    describe 'when logged in and not curator' do
      before do
        login_as user
        allow_any_instance_of(::Ability).to receive(:curator?).and_return(false)
      end

      it_behaves_like 'a forbidden API call', *api
    end

    describe 'when logged in as curator' do
      before do
        login_as user
        allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
      end

      it_behaves_like 'an authorized API call', *api
    end
  end
end
