RSpec.shared_examples 'a DDR resource change set' do
  describe 'metadata field cardinality' do
    Ddr::Metadata.fields.select { |_, v| v.multiple? }.keys.each do |field|
      it "expects #{field.inspect} to be multiple" do
        expect(subject.multiple?(field)).to be true
      end

      it 'expects :access_role to be multiple' do
        expect(subject.multiple?(:thumbnail)).to be false
      end
    end

    Ddr::Metadata.fields.select { |_, v| v.single? }.keys.each do |field|
      it "expects #{field.inspect} to NOT be multiple" do
        expect(subject.multiple?(field)).to be false
      end

      it 'expects :thumbnail to NOT be multiple' do
        expect(subject.multiple?(:thumbnail)).to be false
      end
    end
  end

  describe 'validations' do
    describe 'admin policy model class' do
      let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }

      before do
        allow(Ddr.query_service).to receive(:find_by).with(id: valkyrie_id).and_return(admin_policy_resource)
      end

      describe 'when Ddr::Collection' do
        let(:admin_policy_resource) { Ddr::Collection.new }

        it 'considers the admin policy ID to be valid' do
          subject.validate(admin_policy_id: valkyrie_id)
          expect(subject.errors.messages).not_to have_key(:admin_policy_id)
        end
      end

      describe 'when not Ddr::Collection' do
        let(:admin_policy_resource) { Ddr::Resource.new }

        it 'considers the admin policy ID to not be valid' do
          subject.validate(admin_policy_id: valkyrie_id)
          expect(subject).to respond_to(:admin_policy_id)
          expect(Ddr.query_service.find_by(id: subject.admin_policy_id).class).to eq(Ddr::Resource)
          expect(Ddr.query_service.find_by(id: subject.admin_policy_id).class).not_to eq(Ddr::Collection)
          expect(subject.errors.added?(:admin_policy_id, I18n.t('ddr.change_set.errors.admin_policy_class'))).to be true
          expect(subject.errors.added?(:admin_policy_id, 'wrong error message')).to be false
          expect(subject.errors.added?(:test_nonsense_attribute,
                                       'no error should be added for a nonsense attribute')).to be false
        end
      end
    end
  end

  describe '#desc_metadata_field_names' do
    it 'has a default value' do
      expect(subject.desc_metadata_field_names).to eq Ddr::Describable.term_names.map(&:to_s)
    end

    describe 'arguments' do
      describe ':required' do
        let(:expected_fields) do
          Ddr::Describable.term_names.map(&:to_s).select { |f| subject.required?(f) }
        end

        it 'returns the required fields' do
          expect(subject.desc_metadata_field_names(:required)).to match_array(expected_fields)
        end
      end

      describe ':present' do
        before do
          subject.title = ['Object Title']
          subject.creator = ['Duke University Libraries']
          subject.identifier = ['id001']
        end

        it 'returns the non-empty fields' do
          expect(subject.desc_metadata_field_names(:present)).to match_array(%w[title creator identifier])
        end
      end
    end
  end

  describe '#new_record?' do
    it 'delegates the method to its resource' do
      expect(subject.resource).to receive(:new_record?)
      subject.new_record?
    end
  end

  describe '#set_admin_metadata' do
    before do
      subject.local_id = 'foo'
      subject.permanent_id = 'ark id'
      subject.permanent_url = 'ark id url'
      subject.contributor = ['Bar'] # desc metadata attribute
    end

    let(:provided_values) { { 'local_id' => 'bar', 'permanent_id' => 'new ark id' } }

    it 'sets admin metadata attributes to provided values' do
      subject.set_admin_metadata(provided_values)
      expect(subject.local_id).to eq('bar')
      expect(subject.permanent_id).to eq('new ark id')
      expect(subject.permanent_url).to eq('ark id url')
    end

    it 'does not affect non admin metadata attributes' do
      subject.set_admin_metadata(provided_values)
      expect(subject.contributor).to contain_exactly('Bar')
    end
  end

  describe '#set_desc_metadata' do
    before do
      subject.contributor = subject.resource.contributor = ['Bar']
      subject.title = subject.resource.title = 'Test Title'
      subject.local_id = subject.resource.local_id = 'foo' # admin metadata attribute
    end

    let(:provided_values) { { 'title' => 'Changed Title', 'creator' => %w[Baz Foo] } }

    it 'sets desc metadata attribute provided to the provided value' do
      expect { subject.set_desc_metadata(provided_values) }.to change(subject, :title).to(['Changed Title'])
    end

    it 'does not affect non desc metadata attributes' do
      expect { subject.set_desc_metadata(provided_values) }.not_to change(subject, :local_id)
    end
  end

  describe '#set_values' do
    describe 'multi-valued field' do
      before { subject.type = 'Photograph' }

      it 'sets the values of the term to those supplied' do
        subject.set_values :type, ['Image', 'Still Image']
        expect(subject.type).to contain_exactly('Image', 'Still Image')
      end
    end

    describe 'single-valued field' do
      before { subject.permanent_id = 'test_ark' }

      it 'sets the value of the term to the one supplied' do
        subject.set_values :permanent_id, 'another_test_ark'
        expect(subject.permanent_id).to eq('another_test_ark')
      end
    end

    describe 'field cleanup' do
      it 'does not add blank values' do
        subject.set_values :type, ['Image', '', nil, "\t"]
        expect(subject.type).to eq(['Image'])
      end

      it 'strips whitespace from values' do
        subject.set_values :type, ['Image', 'Still Image ']
        expect(subject.type).to eq(['Image', 'Still Image'])
      end

      it 'strips control characters from values' do
        subject.set_values :type, ['Image', "Still\f Image"]
        expect(subject.type).to eq(['Image', 'Still Image'])
      end

      it 'does not strip CR and LF characters from values' do
        subject.set_values :type, %W[Image Still\nImage]
        expect(subject.type).to eq(%W[Image Still\nImage])
      end
    end
  end

  describe '#add_value' do
    describe 'multi-valued field' do
      before { subject.type = 'Photograph' }

      it 'adds the supplied value to those of the term' do
        subject.add_value :type, 'Image'
        expect(subject.type).to eq(%w[Photograph Image])
      end
    end

    describe 'single-valued field' do
      describe 'no existing value for term' do
        it 'sets the value of the term to the supplied value' do
          subject.add_value :permanent_id, 'test_ark'
          expect(subject.permanent_id).to eq('test_ark')
        end
      end

      describe 'existing value for term' do
        before { subject.permanent_id = 'another_test_ark' }

        it 'raises an error' do
          expect { subject.add_value :permanent_id, 'test_ark' }.to raise_error(ArgumentError)
        end
      end
    end

    describe 'field cleanup' do
      before { subject.type = 'Photograph' }

      it 'strips whitespace from the value' do
        subject.add_value :type, 'Image '
        expect(subject.type).to eq(%w[Photograph Image])
      end

      it 'does not add blank values' do
        subject.add_value :type, nil
        subject.add_value :type, ''
        subject.add_value :type, "\t"
        expect(subject.type).to eq(['Photograph'])
      end
    end
  end

  describe '#copy_admin_policy_or_roles_from' do
    let(:other) { Ddr::Resource.new }

    describe 'copying admin policy ID' do
      it 'attempts to copy an admin policy ID' do
        expect(subject).to receive(:copy_admin_policy_from).with(other)
        subject.copy_admin_policy_or_roles_from(other)
      end
    end

    describe 'copying resource roles' do
      describe 'other resource provides an admin policy ID' do
        before do
          allow(subject).to receive(:copy_admin_policy_from) { Valkyrie::ID.new(SecureRandom.uuid) }
        end

        it 'does not attempt to copy resource roles' do
          expect(subject).not_to receive(:copy_resource_roles_from).with(other)
          subject.copy_admin_policy_or_roles_from(other)
        end
      end

      describe 'other resource does not provide an admin policy ID' do
        before do
          allow(subject).to receive(:copy_admin_policy_from).and_return(nil)
        end

        it 'attempts to copy resource roles' do
          expect(subject).to receive(:copy_resource_roles_from).with(other)
          subject.copy_admin_policy_or_roles_from(other)
        end
      end
    end

    describe 'other resource provides an admin policy ID' do
      before do
        allow(subject).to receive(:copy_admin_policy_from) { Valkyrie::ID.new(SecureRandom.uuid) }
      end

      it 'does not attempt to copy resource roles' do
      end
    end
  end

  describe '#copy_admin_policy_from' do
    describe 'other resource has an admin policy' do
      let(:admin_policy_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:other) { Ddr::Resource.new(admin_policy_id:) }

      it 'copies the admin policy ID' do
        subject.copy_admin_policy_from(other)
        expect(subject.admin_policy_id).to eq(admin_policy_id)
      end
    end

    describe 'other resource does not have an admin policy' do
      describe 'other resource is a Collection' do
        let(:collection_id) { Valkyrie::ID.new(SecureRandom.uuid) }
        let(:other) { Ddr::Collection.new(id: collection_id) }

        it 'sets admin policy ID to the Collection ID' do
          subject.copy_admin_policy_from(other)
          expect(subject.admin_policy_id).to eq(collection_id)
        end
      end

      describe 'other resource is not a Collection' do
        let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }
        let(:other) { Ddr::Resource.new(id: resource_id) }

        it 'does not set the admin policy ID' do
          expect { subject.copy_admin_policy_from(other) }.not_to change(subject, :admin_policy_id)
        end
      end
    end
  end

  describe '#copy_resource_roles_from' do
    let(:resource_roles) do
      [build(:role, :viewer, :public, :resource),
       build(:role, :editor, :group, :resource)]
    end
    let(:policy_roles) do
      [build(:role, :curator, :person, :policy)]
    end
    let(:other) { Ddr::Resource.new }

    before do
      other.roles += resource_roles
      other.roles += policy_roles
    end

    it 'copies the resource roles but not the policy roles' do
      subject.copy_resource_roles_from(other)
      expect(subject.roles).to include(*resource_roles)
      expect(subject.roles).not_to include(*policy_roles)
    end
  end
end

RSpec.shared_examples 'a Collection DDR resource change set' do
  describe '#grant_roles_to_creator' do
    let(:creator) { build(:user) }

    it "gives the creator the 'Curator' role in the 'policy' scope" do
      subject.grant_roles_to_creator(creator)
      expect(subject.roles).to include(FactoryBot.build(:role, :curator, :policy, agent: creator.agent))
    end
  end

  describe 'optimistic locking' do
    describe 'conflicting updates' do
      let(:resource) { FactoryBot.create_for_repository(:collection) }
      let(:change_set_persister) { Ddr::ResourceChangeSetPersister.new }

      it 'raises a stale object error' do
        # Instantiate two change sets for the same resource
        change_set_1 = Ddr::ResourceChangeSet.change_set_for(resource)
        change_set_2 = Ddr::ResourceChangeSet.change_set_for(resource)
        # Make a change to each change set
        change_set_1.title = 'Title A'
        change_set_2.title = 'Title B'
        # Persist the changes from the first change set
        change_set_persister.save(change_set: change_set_1)
        # Attempt to persist the changes from the second change set
        expect { change_set_persister.save(change_set: change_set_2) }
          .to raise_error(Valkyrie::Persistence::StaleObjectError)
      end
    end
  end
end

RSpec.shared_examples 'a non-Collection DDR resource change set' do
  describe '#grant_roles_to_creator' do
    let(:creator) { build(:user) }

    it "gives the creator the 'Editor' role in the 'resource' scope" do
      subject.grant_roles_to_creator(creator)
      expect(subject.roles).to include(FactoryBot.build(:role, :editor, :resource, agent: creator.agent))
    end
  end

  describe 'optimistic locking' do
    describe 'conflicting updates' do
      let(:persisted_resource) { Ddr.persister.save(resource:) }
      let(:change_set_persister) { Ddr::ResourceChangeSetPersister.new }

      it 'raises a stale object error' do
        # Instantiate two change sets for the same resource
        change_set_1 = Ddr::ResourceChangeSet.change_set_for(persisted_resource)
        change_set_2 = Ddr::ResourceChangeSet.change_set_for(persisted_resource)
        # Make a change to each change set
        change_set_1.title = 'Title A'
        change_set_2.title = 'Title B'
        # Persist the changes from the first change set
        change_set_persister.save(change_set: change_set_1)
        # Attempt to persist the changes from the second change set
        expect { change_set_persister.save(change_set: change_set_2) }
          .to raise_error(Valkyrie::Persistence::StaleObjectError)
      end
    end
  end
end

RSpec.shared_examples 'a DDR resource change set for a resource class that can be published' do
  describe '#admin_metadata_field_names' do
    it 'has a default value' do
      expect(subject.admin_metadata_field_names).to eq Ddr::HasAdminMetadata.term_names.map(&:to_s)
    end

    describe 'arguments' do
      describe ':required' do
        let(:expected_fields) do
          Ddr::HasAdminMetadata.term_names.map(&:to_s).select { |f| subject.required?(f) }
        end

        it 'returns the required fields' do
          expect(subject.admin_metadata_field_names(:required)).to match_array(expected_fields)
        end
      end

      describe ':present' do
        before do
          subject.local_id = 'foobar'
          subject.permanent_id = 'ark'
        end

        it 'returns the non-empty fields' do
          expect(subject.admin_metadata_field_names(:present))
            .to match_array(%w[local_id permanent_id workflow_state])
        end
      end
    end
  end
end

RSpec.shared_examples 'a DDR resource change set for a resource class that cannot be published' do
  describe '#admin_metadata_field_names' do
    it 'has a default value' do
      expect(subject.admin_metadata_field_names).to eq Ddr::HasAdminMetadata.term_names.map(&:to_s)
    end

    describe 'arguments' do
      describe ':required' do
        let(:expected_fields) do
          Ddr::HasAdminMetadata.term_names.map(&:to_s).select { |f| subject.required?(f) }
        end

        it 'returns the required fields' do
          expect(subject.admin_metadata_field_names(:required)).to match_array(expected_fields)
        end
      end

      describe ':present' do
        before do
          subject.local_id = 'foobar'
          subject.permanent_id = 'ark'
        end

        it 'returns the non-empty fields' do
          expect(subject.admin_metadata_field_names(:present)).to match_array(%w[local_id permanent_id workflow_state])
        end
      end
    end
  end
end
