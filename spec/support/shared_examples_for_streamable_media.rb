RSpec.shared_examples 'a resource that cannot be streamable' do
  its(:can_be_streamable?) { is_expected.to be false }
end

RSpec.shared_examples 'a resource that can be streamable' do
  its(:can_be_streamable?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:streamable_media) }

  describe 'when streamable media is present' do
    subject { described_class.new(streamable_media:) }

    let(:resource) { Ddr::Resource.new }
    let(:streamable_media_file_name) { 'video.mp4' }
    let(:streamable_media_file_upload) do
      Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures', streamable_media_file_name), 'video/mp4')
    end
    let(:streamable_media_file) do
      Ddr.storage_adapter.upload(file: streamable_media_file_upload, resource:,
                                 original_filename: streamable_media_file_name)
    end
    let(:streamable_media) do
      Ddr::File.new(file_identifier: streamable_media_file.id, media_type: 'video/mp4',
                    original_filename: streamable_media_file_name)
    end

    it { is_expected.to be_streamable }

    describe '#streamable_media_path' do
      it 'returns the disk path of the streamable media file' do
        expect(subject.streamable_media_path).to eq(streamable_media.file.disk_path)
      end
    end

    describe '#streamable_media_type' do
      it 'returns the media type of the streamable media file' do
        expect(subject.streamable_media_type).to eq(streamable_media.media_type)
      end
    end

    describe '#streamable_media_extension' do
      it 'returns the file extension of the streamable media file' do
        expect(subject.streamable_media_extension).to eq(File.extname(streamable_media_file_name))
      end
    end
  end

  describe 'when streamable media is not present' do
    it { is_expected.not_to be_streamable }
    its(:streamable_media_path) { is_expected.to be_nil }
    its(:streamable_media_type) { is_expected.to be_nil }
    its(:streamable_media_extension) { is_expected.to be_nil }
  end
end
