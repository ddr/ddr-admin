shared_examples 'a repository object router' do
  it 'has a new route' do
    unless controller == 'targets'
      expect(get: "/ddr/#{controller}/new").to route_to(controller: "ddr/#{controller}",
                                                        action: 'new')
    end
  end

  it 'has a create route' do
    unless controller == 'targets'
      expect(post: "/ddr/#{controller}").to route_to(controller: "ddr/#{controller}",
                                                     action: 'create')
    end
  end

  it 'has a show route' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5")
      .to route_to(controller: "ddr/#{controller}", action: 'show', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end

  it 'has an edit route' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/edit")
      .to route_to(controller: "ddr/#{controller}", action: 'edit', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end

  it 'has an update route' do
    expect(patch: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5")
      .to route_to(controller: "ddr/#{controller}", action: 'update', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end

  it 'does not have permissions routes' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/permissions").not_to be_routable
    expect(patch: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/permissions").not_to be_routable
  end

  it 'has roles routes' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/roles")
      .to route_to(controller: "ddr/#{controller}", action: 'roles', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
    expect(patch: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/roles")
      .to route_to(controller: "ddr/#{controller}", action: 'roles', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end

  it 'has an administrative metadata route' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/admin_metadata")
      .to route_to(controller: "ddr/#{controller}", action: 'admin_metadata',
                   id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
    expect(patch: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/admin_metadata")
      .to route_to(controller: "ddr/#{controller}", action: 'admin_metadata',
                   id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end

  it 'has an events route' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/events")
      .to route_to(controller: "ddr/#{controller}", action: 'events', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end

  it 'has an event route' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/events/1")
      .to route_to(controller: "ddr/#{controller}", action: 'event', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5',
                   event_id: '1')
  end
end

shared_examples 'a content-bearing object router' do
  it 'has a upload routes' do
    expect(get: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/upload")
      .to route_to(controller: "ddr/#{controller}", action: 'upload', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
    expect(patch: "/ddr/#{controller}/d655b5c3-e671-427a-8c76-44f0828dfcd5/upload")
      .to route_to(controller: "ddr/#{controller}", action: 'upload', id: 'd655b5c3-e671-427a-8c76-44f0828dfcd5')
  end
end
