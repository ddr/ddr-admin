RSpec.shared_examples 'a content-bearing resource that has a display title' do
  describe '#title_display' do
    subject { object.title_display }

    let(:object) { described_class.new }

    context 'has title' do
      before { object.title = ['Title'] }

      it 'returns the first title' do
        expect(subject).to eq('Title')
      end
    end

    context 'has no title, has identifier' do
      before { object.identifier = ['id001'] }

      it 'returns the first identifier' do
        expect(subject).to eq('id001')
      end
    end

    context 'has no title, no identifier, has original_filename' do
      before { allow(object).to receive(:original_filename).and_return('file.txt') }

      it 'returns original_filename' do
        expect(subject).to eq 'file.txt'
      end
    end

    context 'has no title, no identifier, no original_filename' do
      let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:object) { described_class.new(id: valkyrie_id) }

      it 'returns the resource ID in square brackets' do
        expect(subject).to eq "[#{valkyrie_id.id}]"
      end
    end
  end
end

RSpec.shared_examples 'a non-content-bearing resource that has a display title' do
  describe '#title_display' do
    subject { object.title_display }

    let(:object) { described_class.new }

    context 'has title' do
      before { object.title = ['Title'] }

      it 'returns the first title' do
        expect(subject).to eq('Title')
      end
    end

    context 'has no title, has identifier' do
      before { object.identifier = ['id001'] }

      it 'returns the first identifier' do
        expect(subject).to eq('id001')
      end
    end

    context 'has no title, no identifier' do
      let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:object) { described_class.new(id: valkyrie_id) }

      it 'returns the resource ID in square brackets' do
        expect(subject).to eq "[#{valkyrie_id.id}]"
      end
    end
  end
end
