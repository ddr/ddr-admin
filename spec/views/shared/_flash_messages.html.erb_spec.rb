RSpec.describe 'shared/_flash_messages.html.erb', type: :view do
  before do
    allow(Ddr::Admin).to receive(:alert_message).and_return('Alert!')
  end

  it 'includes the active message(s)' do
    render partial: 'shared/flash_messages'
    expect(rendered).to match(/Alert!/)
  end
end
