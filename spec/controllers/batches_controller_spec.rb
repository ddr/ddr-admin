module Ddr
  describe BatchesController, :batch, type: :controller do
    shared_examples 'a delete-able batch' do
      it 'deletes the batch and redirect to the index page' do
        expect(Ddr::Batch::BatchDeletionJob).to receive(:perform_later).with(batch.id)
        delete :destroy, params: { id: batch }
        expect(subject).to redirect_to(ddr_batches_path)
      end
    end

    shared_examples 'a non-delete-able batch' do
      it 'does not delete the batch and redirect to the index page' do
        expect(Ddr::Batch::BatchDeletionJob).not_to receive(:perform_later).with(batch.id)
        delete :destroy, params: { id: batch }
        expect(subject).to redirect_to(ddr_batches_path)
      end
    end

    describe '#index' do
      let!(:my_batch) { FactoryBot.create(:batch) }
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'lists my batch for others users' do
        get :index
        expect(assigns(:batches).size).to eq(1)
      end

      describe 'my batches' do
        it "doesn't list my batch for other users" do
          get :index, params: { filter: 'current_user' }
          expect(assigns(:batches).size).to eq(0)
        end
      end
    end

    describe '#show' do
      let(:my_batch) { FactoryBot.create(:batch) }
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'renders my batch for other users' do
        get :show, params: { id: my_batch }
        expect(response.response_code).to eq(200)
      end
    end

    describe '#destroy' do
      let(:batch) { FactoryBot.create(:batch_with_basic_ingest_batch_objects) }

      before { sign_in batch.user }

      context 'batch is pending (nil)' do
        it_behaves_like 'a delete-able batch'
      end

      context 'batch is validated' do
        before do
          batch.status = Ddr::Batch::Batch::STATUS_VALIDATED
          batch.save!
        end

        it_behaves_like 'a delete-able batch'
      end

      context 'batch is queued' do
        before do
          batch.status = Ddr::Batch::Batch::STATUS_QUEUED
          batch.save!
        end

        it_behaves_like 'a non-delete-able batch'
      end

      context 'batch is running' do
        before do
          batch.status = Ddr::Batch::Batch::STATUS_RUNNING
          batch.save!
        end

        it_behaves_like 'a non-delete-able batch'
      end

      context 'batch is finished' do
        before do
          batch.status = Ddr::Batch::Batch::STATUS_FINISHED
          batch.save!
        end

        it_behaves_like 'a non-delete-able batch'
      end

      context 'batch is interrupted' do
        context 'batch is not restartable' do
          before do
            batch.status = Ddr::Batch::Batch::STATUS_INTERRUPTED
            batch.save!
          end

          it_behaves_like 'a non-delete-able batch'
        end

        context 'batch is restartable' do
          before do
            batch.status = Ddr::Batch::Batch::STATUS_RESTARTABLE
            batch.save!
          end

          it_behaves_like 'a non-delete-able batch'
        end

        context 'batch is queued for deletion' do
          before do
            batch.status = Ddr::Batch::Batch::STATUS_QUEUED_FOR_DELETION
            batch.save!
          end

          it_behaves_like 'a non-delete-able batch'
        end

        context 'batch is deleting' do
          before do
            batch.status = Ddr::Batch::Batch::STATUS_DELETING
            batch.save!
          end

          it_behaves_like 'a non-delete-able batch'
        end
      end
    end

    describe '#procezz' do
      let(:batch) { FactoryBot.create(:batch_with_basic_ingest_batch_objects) }

      before { sign_in batch.user }

      it 'enqueues the job' do
        expect(Ddr::Batch::BatchProcessorJob).to receive(:perform_later).with(batch.id, batch.user.id)
        get :procezz, params: { id: batch.id }
      end

      it 'redirects to the batches url' do
        allow(Ddr::Batch::BatchProcessorJob).to receive(:perform_later).with(batch.id, batch.user.id)
        get :procezz, params: { id: batch.id }
        expect(response).to redirect_to(ddr_batch_url)
      end
    end

    describe '#retry' do
      let(:batch) { FactoryBot.create(:batch, outcome: 'FAILURE') }

      before do
        sign_in batch.user
      end

      it 'redirects to the batches index page' do
        get :retry, params: { id: batch }
        expect(response).to redirect_to(ddr_batches_url)
      end

      it 'starts the retry job' do
        allow(Ddr::Batch::BatchReprocessorJob).to receive(:perform_later).with(batch.id, batch.user.id)
        get :retry, params: { id: batch }
        expect(Ddr::Batch::BatchReprocessorJob).to have_received(:perform_later).with(batch.id, batch.user.id)
      end

      describe 'when there are unverified created components' do
        before do
          # create item and batch object
          item = create_for_repository(:item)
          create(:item_ingest_batch_object, resource_id: item.id.to_s,
                                            batch:, verified: 't', handled: 't')
          # create component and batch object
          comp = create_for_repository(:component, :with_content_file, parent_id: item.id.to_s)
          comp_bo = create(:component_ingest_batch_object, batch:)
          # link the comp bo to item as parent
          create(:batch_object_add_parent, object: item.id.to_s, batch_object: comp_bo)
          # link the comp bo to the comp content SHA1
          create(:batch_object_add_content_datastream, batch_object: comp_bo, checksum: comp.content.sha1)
        end

        it 'redirects to the batch page' do
          get :retry, params: { id: batch }
          expect(response).to redirect_to(ddr_batch_url(batch))
        end

        it 'renders a flash message' do
          get :retry, params: { id: batch }
          expect(request.flash[:danger]).to be_present
        end
      end
    end # retry
  end
end
