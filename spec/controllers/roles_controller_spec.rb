module Ddr
  RSpec.describe RolesController, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user }

    describe '#batch' do
      describe 'GET' do
        it 'renders the batch form' do
          get :batch
          expect(response).to render_template(:batch)
        end
      end

      describe 'PATCH' do
        let(:items) do
          [].tap do |a|
            3.times { a << FactoryBot.create_for_repository(:item) }
          end
        end
        let(:roles) do
          [
            Ddr::Auth::Roles::Role.new(role_type: 'Viewer', agent: 'public', scope: 'policy'),
            Ddr::Auth::Roles::Role.new(role_type: 'Editor', agent: 'ItemEditors', scope: 'resource')
          ]
        end
        let(:params) { { ids: items.map(&:id).join("\n"), roles: roles.to_json } }
        let(:curator) { Ddr::Auth::Roles::Role.new(role_type: 'Curator', agent: user, scope: 'policy') }

        describe 'when the user is not authorized to make the changes' do
          it 'is unauthorized' do
            patch(:batch, params:)
            expect(response).to be_unauthorized
          end
        end

        describe 'when the user is authorized to make the changes' do
          before do
            items.each do |item|
              change_set = ItemChangeSet.new(item)
              change_set.roles = [curator]
              ResourceChangeSetPersister.new.save(change_set:)
            end
          end

          it 'submits the batch for processing' do
            patch(:batch, params:)
            items.each do |item|
              _item = Ddr.query_service.find_by(id: item.id)
              expect(_item.roles).to match_array(roles)
            end
          end
        end
      end
    end
  end
end
