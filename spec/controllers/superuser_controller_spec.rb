module Ddr
  RSpec.describe SuperuserController, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user, scope: :user }

    describe '#create' do
      let(:previous_page) { 'https://library.duke.edu' }

      before do
        request.env['HTTP_REFERER'] = previous_page
      end

      describe 'when the current ability is authorized to act as superuser' do
        before do
          allow(controller).to receive(:authorized_to_act_as_superuser?).and_return(true)
        end

        it 'signs in' do
          expect(controller).to receive(:sign_in).with(:superuser, user)
          get :create
        end

        it 'redirects to the previous page' do
          get :create
          expect(response).to redirect_to(previous_page)
        end

        it 'deletes the :create_menu_models session key' do
          expect(session).to receive(:delete).with(:create_menu_models)
          get :create
        end
      end

      describe 'when the current ability is not authorized to act as superuser' do
        before do
          allow(controller).to receive(:authorized_to_act_as_superuser?).and_return(false)
        end

        it 'is unauthorized' do
          get :create
          expect(response.response_code).to eq(403)
        end
      end
    end

    describe '#destroy' do
      before { sign_in user, scope: :superuser }

      it 'signs out' do
        expect(controller).to receive(:sign_out).with(:superuser)
        get :destroy
      end
    end
  end
end
