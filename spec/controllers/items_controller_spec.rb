require 'rails_helper'
require 'support/shared_examples_for_repository_controllers'
require 'support/shared_examples_for_publishable_resource_controllers'

def create_item
  post :create, params: { parent_id: collection.id, descMetadata: { title: ['New Item'] } }
end

def create_item_and_component(opts = {})
  checksum, checksum_type = opts.values_at(:checksum, :checksum_type)
  post :create, params: { parent_id: collection.id,
                          descMetadata: { title: ['New Item'] },
                          content: { file: fixture_file_upload('../imageA.tif', 'image/tiff'),
                                     checksum:, checksum_type: } }
end

module Ddr
  RSpec.describe ItemsController, :items, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user }

    it_behaves_like 'a repository object controller' do
      let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }
      let(:collection) { create_for_repository(:collection, access_role: [role]) }
      let(:create_object) { -> { create_item } }
      let(:new_object) { -> { get :new, params: { parent_id: collection.id } } }
    end

    it_behaves_like 'a publishable resource controller', Ddr::Item

    describe '#new' do
      # see shared examples
      let(:collection) { FactoryBot.create_for_repository(:collection, access_role: [role]) }

      describe 'when the user can add children to the collection' do
        let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

        it 'is authorized' do
          get :new, params: { parent_id: collection.id }
          expect(response.response_code).to eq(200)
        end
      end

      describe 'when the user cannot add children to the collection' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          get :new, params: { parent_id: collection.id }
          expect(response.response_code).to eq(403)
        end
      end
    end

    describe '#create' do
      # see shared examples
      let(:collection) { FactoryBot.create_for_repository(:collection, access_role: [role]) }

      describe 'when the current user cannot add children to the collection' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          create_item
          expect(response.response_code).to eq(403)
        end
      end

      describe 'when the current user can add children to the collection' do
        let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

        it "sets the admin policy id to the collection's admin policy id" do
          create_item
          created_item = Ddr.query_service.find_all_of_model(model: Ddr::Item).first
          expect(created_item.admin_policy_id).to eq(collection.admin_policy_id)
        end

        describe 'adding a component file at creation time' do
          it 'creates the component' do
            expect { create_item_and_component }
              .to change { Ddr.query_service.find_all_of_model(model: Ddr::Component).count }.by(1)
          end

          #
          # Possible DRY: see Components controller spec
          #
          it 'has content' do
            create_item_and_component
            created_item = Ddr.query_service.find_all_of_model(model: Ddr::Item).first
            expect(created_item.children.first.content).not_to be_nil
          end

          it 'correctly sets the MIME type' do
            create_item_and_component
            created_item = Ddr.query_service.find_all_of_model(model: Ddr::Item).first
            expect(created_item.children.first.content_type).to eq('image/tiff')
          end

          it 'stores the original file name' do
            create_item_and_component
            created_item = Ddr.query_service.find_all_of_model(model: Ddr::Item).first
            expect(created_item.children.first.original_filename).to eq('imageA.tif')
          end

          it 'copies the admin policy id of the item to the component' do
            create_item_and_component
            created_item = Ddr.query_service.find_all_of_model(model: Ddr::Item).first
            expect(created_item.children.first.admin_policy_id).to eq(assigns(:current_object).admin_policy_id)
          end

          it 'updates derivatives' do
            allow(Ddr::V3::ImageDerivativesUpdateService).to receive(:call)
            create_item_and_component
            expect(Ddr::V3::ImageDerivativesUpdateService).to have_received(:call)
          end

          it 'creates ingestion events' do
            expect { create_item_and_component }.to change { Ddr::Events::IngestionEvent.count }.by(2)
          end

          it 'validates the checksum if provided' do
            expect_any_instance_of(Ddr::File).to receive(:validate_checksum!)
            create_item_and_component(checksum: '75e2e0cec6e807f6ae63610d46448f777591dd6b', checksum_type: 'SHA-1')
          end
        end
      end
    end

    describe '#components' do
      let(:item) { FactoryBot.create_for_repository(:item) }

      describe 'when the user cannot read the item' do
        it 'is unauthorized' do
          get :components, params: { id: item }
          expect(response.response_code).to eq 403
        end
      end
    end
  end
end
