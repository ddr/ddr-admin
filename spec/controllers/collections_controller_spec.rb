require 'rails_helper'
require 'support/collections_controller_spec_helper'
require 'support/shared_examples_for_repository_controllers'
require 'support/shared_examples_for_publishable_resource_controllers'

def create_collection
  post :create, params: { descMetadata: { title: ['New Collection'] }, admin_set: 'dc' }
end

def new_collection
  get :new
end

module Ddr
  RSpec.describe CollectionsController, :collections, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    let(:viewer_role) { FactoryBot.build(:role, :viewer, agent: user) }
    let(:curator_role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

    before do
      sign_in user
    end

    it_behaves_like 'a repository object controller' do
      let(:create_object) { proc { create_collection } }
      let(:new_object) { proc { new_collection } }
    end

    it_behaves_like 'a publishable resource controller', Ddr::Collection

    describe '#items' do
      let(:collection) { FactoryBot.create_for_repository(:collection) }
      let(:item) { FactoryBot.build(:item) }

      context 'when the user can read the collection' do
        before do
          allow_any_instance_of(Ddr::Collection).to receive(:children).and_return(item)
          collection.roles += [viewer_role]
          Ddr.persister.save(resource: collection)
        end

        it 'renders the items' do
          get :items, params: { id: collection.id.id }
          expect(response).to be_successful
          expect(response).to render_template(:items)
        end
      end

      context 'when the user cannot read the collection' do
        it 'is unauthorized' do
          get :items, params: { id: collection.id.id }
          expect(response.response_code).to eq 403
        end
      end
    end

    describe '#attachments' do
      let(:collection) { FactoryBot.create_for_repository(:collection) }
      let(:attachment) { FactoryBot.build(:attachment) }

      context 'when the user can read the collection' do
        before do
          allow_any_instance_of(Ddr::Collection).to receive(:attachments).and_return(attachment)
          collection.roles += [viewer_role]
          Ddr.persister.save(resource: collection)
        end

        it 'renders the attachments' do
          get :attachments, params: { id: collection.id.id }
          expect(response).to be_successful
          expect(response).to render_template(:attachments)
        end
      end

      context 'when the user cannot read the collection' do
        it 'is unauthorized' do
          get :attachments, params: { id: collection.id.id }
          expect(response.response_code).to eq 403
        end
      end
    end

    describe '#targets' do
      let(:collection) { FactoryBot.create_for_repository(:collection) }
      let(:target) { FactoryBot.build(:target) }

      context 'when the user can read the collection' do
        before do
          allow_any_instance_of(Ddr::Collection).to receive(:targets).and_return(target)
          collection.roles += [viewer_role]
          Ddr.persister.save(resource: collection)
        end

        it 'renders the targets' do
          get :targets, params: { id: collection.id.id }
          expect(response).to be_successful
          expect(response).to render_template(:targets)
        end
      end

      context 'when the user cannot read the collection' do
        it 'is unauthorized' do
          get :targets, params: { id: collection }
          expect(response.response_code).to eq 403
        end
      end
    end

    describe '#generate_structure' do
      let(:collection) { create_for_repository(:collection, access_role: curator_role) }

      it 'generates structural metadata' do
        allow(Ddr::GenerateDefaultStructureJob).to receive(:perform_later)
        create_for_repository(:item, parent_id: collection.id)
        get :generate_structure, params: { id: collection }
        expect(Ddr::GenerateDefaultStructureJob).to have_received(:perform_later).with(collection.id_s)
      end
    end

    describe '#collection_info' do
      context 'when the user can read the collection' do
        let(:collection) { create_for_repository(:collection, access_role: viewer_role) }

        before do
          3.times do
            item = create_for_repository(:item, parent_id: collection.id)
            2.times { create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id) }
          end

          allow_any_instance_of(Ddr::Component).to receive(:content_size).and_return(100)
        end

        it 'reports the statistics' do
          get :collection_info, params: { id: collection.id.id }
          expect(response).to render_template(:collection_info)
          expect(controller.send(:collection_report)[:components]).to eq(6)
          expect(controller.send(:collection_report)[:items]).to eq(3)
          expect(controller.send(:collection_report)[:total_file_size]).to eq(600)
        end
      end

      context 'when the user cannot read the collection' do
        let(:collection) { create_for_repository(:collection) }

        it 'is unauthorized' do
          get :collection_info, params: { id: collection.id.id }
          expect(response.response_code).to eq(403)
        end
      end
    end
  end
end
