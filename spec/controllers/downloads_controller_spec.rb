module Ddr
  RSpec.describe DownloadsController, type: :controller do
    describe '#show' do
      let(:user) { FactoryBot.create(:user) }
      let!(:component) { FactoryBot.create_for_repository(:component, :with_content_file) }

      before do
        sign_in user
      end

      describe 'authorized user' do
        before do
          component.roles += [FactoryBot.build(:role, :downloader, agent: user.user_key)]
          Ddr.persister.save(resource: component)
        end

        describe 'requested ddr file has file identifier' do
          describe 'head request' do
            specify do
              head :show, params: { id: component.id.id }
              expect(response).to be_successful
              expect(response.content_length).to eq(component.content.file_size)
              expect(response.content_type).to eq(component.content.media_type)
            end
          end

          describe 'get request' do
            specify do
              get :show, params: { id: component.id.id }
              expect(response).to be_successful
              expect(response.content_type).to eq(component.content.media_type)
              expect(response.headers['Content-Disposition'])
                .to match(/filename="#{component.content.original_filename}"/)
              expect(response.body).to eq(component.content.file.read)
            end
          end
        end

        describe 'requested ddr file does not have file identifier' do
          before do
            allow(subject).to receive(:ddr_file) { Ddr::File.new }
          end

          specify do
            get :show, params: { id: component.id.id }
            expect(response.status).to eq(404)
          end
        end
      end

      describe 'unauthorized user' do
        before do
          component.roles += [FactoryBot.build(:role, :viewer, agent: user.user_key)]
          Ddr.persister.save(resource: component)
        end

        describe 'head request' do
          specify do
            head :show, params: { id: component.id.id }
            expect(response.status).to eq(403)
          end
        end

        describe 'get request' do
          specify do
            get :show, params: { id: component.id.id }
            expect(response.status).to eq(403)
          end
        end
      end
    end

    describe '#ddr_file_type' do
      describe 'request params has ddr_file_type' do
        before do
          allow(subject).to receive(:params).and_return({ ddr_file_type: 'fits' })
        end

        it 'returns the specified ddr file type' do
          expect(subject.send(:ddr_file_type)).to eq('fits')
        end
      end

      describe 'request params does not have ddr_file_type' do
        it 'returns a default ddr file type' do
          expect(subject.send(:ddr_file_type)).to eq('content')
        end
      end
    end

    describe '#ddr_file_name' do
      let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }

      before do
        allow(subject).to receive(:asset) { resource }
        allow(subject).to receive(:ddr_file) { ddr_file }
      end

      describe 'ddr file original filename present' do
        let(:resource) { Ddr::Component.new(id: resource_id) }
        let(:ddr_file) { Ddr::File.new(original_filename: 'test.tif') }

        it 'returns the original filename' do
          expect(subject.send(:ddr_file_name)).to eq('test.tif')
        end
      end

      describe 'ddr file original filename not present' do
        let(:ddr_file) { Ddr::File.new }

        before do
          allow(subject).to receive(:ddr_file_type).and_return('content')
          allow(ddr_file).to receive(:default_file_extension).and_return('tif')
        end

        describe 'resource local id present' do
          let(:resource) { Ddr::Component.new(id: resource_id, local_id: 'abc123') }

          it 'returns a concatentation of the local id and the default file extension' do
            expect(subject.send(:ddr_file_name)).to eq('abc123.tif')
          end
        end

        describe 'resource local id not present' do
          let(:resource) { Ddr::Component.new(id: resource_id) }

          it 'returns a concatentation of the resource ID, the file type, and the default file extension' do
            expect(subject.send(:ddr_file_name)).to eq("#{resource_id}_content.tif")
          end
        end
      end
    end
  end
end
