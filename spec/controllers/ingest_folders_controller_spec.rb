module Ddr
  RSpec.describe IngestFoldersController, :ingest_folder, type: :controller do
    let(:checksum_directory) { FileUtils.mkdir_p(::File.join(@tempdir, 'checksums')).first }
    let(:default_checksum_file) { ::File.join(checksum_directory, 'sub_path-base_path-sha1.txt') }
    let(:checksum_type) { 'SHA1' }
    let(:role) { Ddr::Auth::Roles::Role.new(agent: user, role_type: 'Curator', scope: 'policy') }
    let(:collection) { create_for_repository(:collection, access_role: role) }
    let(:base_path) { FileUtils.mkdir_p(::File.join(@tempdir, 'base_path')).first }
    let(:sub_path) { 'sub_path' }
    let(:user) { FactoryBot.create(:user) }

    before do
      sign_in user
      @tempdir = Dir.mktmpdir('ingest_folder-', Rails.root.join('tmp'))
      allow(IngestFolder).to receive(:permitted_base_paths) { [base_path] }
      allow(IngestFolder).to receive(:checksum_file_dir) { checksum_directory }
      allow(::File).to receive(:readable?).and_return(true)
    end

    after { FileUtils.rm_rf(@tempdir) }

    describe '#create' do
      let(:ingest_folder_params) do
        { base_path:,
          sub_path:,
          checksum_type:,
          collection_id: collection.id.to_s,
          user:,
          parent_id_length: 1 }
      end

      before do
        post :create, params: { ddr_ingest_folder: ingest_folder_params }
      end

      it 'sets the model correctly' do
        expect(assigns[:ingest_folder].model).to eq IngestFolder::FILE_MODEL
      end

      it 'sets the base_path correctly' do
        expect(assigns[:ingest_folder].base_path).to eq base_path
      end

      it 'sets the sub_path correctly' do
        expect(assigns[:ingest_folder].sub_path).to eq sub_path
      end

      it 'sets the checksum_type correctly' do
        expect(assigns[:ingest_folder].checksum_type).to eq 'SHA1'
      end

      describe 'when not submitting a checksum file path' do
        it 'assigns checksum_file to the default' do
          expect(assigns[:ingest_folder].checksum_file).to eq default_checksum_file
        end
      end

      describe 'when submitting a checksum file path' do
        let(:checksum_file) { ::File.join(@tempdir, 'checksums.txt') }
        let(:ingest_folder_params) do
          { base_path:,
            sub_path:,
            checksum_type:,
            checksum_file:,
            collection_id: collection.id.to_s,
            user:,
            parent_id_length: 1 }
        end

        before { FileUtils.touch(checksum_file) }

        it 'assigns the checksum_file to the path' do
          expect(assigns[:ingest_folder].checksum_file).to eq checksum_file
        end
      end
    end
  end
end
