module Ddr
  RSpec.describe BatchObjectsController, type: :controller do
    describe '#show' do
      let(:batch) { FactoryBot.create(:batch_with_basic_update_batch_object) }
      let(:user) { FactoryBot.create(:user) }

      before { sign_in user }

      specify do
        get :show, params: { id: batch.batch_objects.first }
        expect(response.response_code).to eq(200)
      end
    end
  end
end
