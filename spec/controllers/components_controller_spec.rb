require 'rails_helper'
require 'support/shared_examples_for_repository_controllers'
require 'support/shared_examples_for_has_content_controllers'
require 'support/shared_examples_for_publishable_resource_controllers'

def create_component(opts = {})
  checksum, checksum_type = opts.values_at(:checksum, :checksum_type)
  post :create, params: { parent_id: item.id, content: { ddr_file_type: 'content',
                                                         file: fixture_file_upload('../imageA.tif', 'image/tiff'),
                                                         checksum:, checksum_type: } }
end

def parse_response_for_component_id(response)
  location = response.header['Location']
  # E.g., "http://test.host/ddr/components/eea8c56c-cf15-4173-be28-89e6d5cef5aa/edit"
  matches = location.match(%r{.*/components/(\h{8}-\h{4}-\h{4}-\h{4}-\h{12})/edit$})
  matches[1]
end

module Ddr
  RSpec.describe ComponentsController, :components, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user }

    it_behaves_like 'a repository object controller' do
      let(:role) { FactoryBot.build(:role, :curator, agent: user) }
      let(:item) { create_for_repository(:item, access_role: [role]) }
      let(:create_object) { proc { create_component } }
      let(:new_object) { proc { get :new, params: { parent_id: item.id } } }
    end

    it_behaves_like 'a content resource controller' do
      let(:resource) { FactoryBot.create_for_repository(:component) }
    end

    it_behaves_like 'a publishable resource controller', Ddr::Component

    describe '#new' do
      # see shared examples
      let(:item) { create_for_repository(:item, access_role: [role]) }

      context 'and user cannot add children to item' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          get :new, params: { parent_id: item.id }
          expect(response.response_code).to eq(403)
        end
      end

      context 'and user can add children to item' do
        let(:role) { FactoryBot.build(:role, :curator, agent: user) }

        it 'is authorized' do
          get :new, params: { parent_id: item.id }
          expect(response.response_code).to eq(200)
        end
      end
    end

    describe '#create' do
      let(:item) { create_for_repository(:item, access_role: [role]) }

      context 'when the user can add children to the item' do
        let(:role) { FactoryBot.build(:role, :curator, agent: user) }

        it 'creates a new object' do
          expect do
            create_component
          end.to change {
            Ddr.query_service.find_all_of_model(model: Ddr::Component).count
          }.by(1)
        end

        it 'has content' do
          comp_id = parse_response_for_component_id(create_component)
          created_component = Ddr.query_service.find_by(id: comp_id)
          expect(created_component.content).not_to be_nil
        end

        it 'correctly sets the MIME type' do
          comp_id = parse_response_for_component_id(create_component)
          created_component = Ddr.query_service.find_by(id: comp_id)
          expect(created_component.content_type).to eq('image/tiff')
        end

        it 'stores the original file name' do
          comp_id = parse_response_for_component_id(create_component)
          created_component = Ddr.query_service.find_by(id: comp_id)
          expect(created_component.original_filename).to eq('imageA.tif')
        end

        it 'grants the Editor role in resource scope to the user' do
          role = FactoryBot.build(:role, :editor, :resource, agent: user)
          comp_id = parse_response_for_component_id(create_component)
          created_component = Ddr.query_service.find_by(id: comp_id)
          expect(created_component.roles).to include(role)
        end

        it 'has a parent' do
          comp_id = parse_response_for_component_id(create_component)
          created_component = Ddr.query_service.find_by(id: comp_id)
          expect(created_component.parent_id).to eq(item.id)
        end

        it 'updates derivatives' do
          allow(Ddr::V3::ImageDerivativesUpdateService).to receive(:call)
          create_component
          expect(Ddr::V3::ImageDerivativesUpdateService).to have_received(:call)
        end

        it 'creates an event' do
          expect { create_component }.to change { Ddr::Events::IngestionEvent.count }.by(1)
        end

        it 'redirects to the component edit page' do
          comp_id = parse_response_for_component_id(create_component)
          created_component = Ddr.query_service.find_by(id: comp_id)
          expect(response).to redirect_to(action: 'edit', id: created_component)
        end

        it 'validates the checksum when provided' do
          expect_any_instance_of(Ddr::File).to receive(:validate_checksum!)
          create_component(checksum: '75e2e0cec6e807f6ae63610d46448f777591dd6b', checksum_type: 'SHA-1')
        end
      end

      context 'when the user cannot add children to the item' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          create_component
          expect(response.response_code).to eq(403)
        end
      end
    end
  end
end
