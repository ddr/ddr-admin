require 'rails_helper'
require 'support/shared_examples_for_repository_controllers'
require 'support/shared_examples_for_has_content_controllers'

def create_attachment(opts = {})
  checksum, checksum_type = opts.values_at(:checksum, :checksum_type)
  post :create, params: { attached_to_id: attach_to.id,
                          content: { ddr_file_type: 'content',
                                     file: fixture_file_upload('sample.docx',
                                                               'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                                     checksum:, checksum_type: },
                          descMetadata: { title: ['New Attachment '] } }
end

module Ddr
  describe AttachmentsController, :attachments, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user }

    it_behaves_like 'a repository object controller' do
      let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }
      let(:attach_to) { create_for_repository(:collection, access_role: [role]) }
      let(:create_object) { -> { create_attachment } }
      let(:new_object) { -> { get :new, params: { attached_to_id: attach_to.id } } }
    end

    it_behaves_like 'a content resource controller' do
      let(:resource) { FactoryBot.create_for_repository(:attachment) }
    end

    describe '#new' do
      # see shared examples
      let(:attach_to) { FactoryBot.create_for_repository(:collection, access_role: [role]) }

      describe 'when user cannot add attachments to object' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          get :new, params: { attached_to_id: attach_to.id.id }
          expect(response.response_code).to eq(403)
        end
      end

      describe 'when the user can add attachments to the resource' do
        let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

        it 'is authorized' do
          get :new, params: { attached_to_id: attach_to.id.id }
          expect(response.response_code).to eq(200)
        end
      end
    end

    describe '#create' do
      # see shared examples
      let(:attach_to) { FactoryBot.create_for_repository(:collection, access_role: [role]) }

      describe 'when user can add attachments to object' do
        let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

        it 'creates a new object' do
          expect { create_attachment }.to change { Ddr.query_service.find_all_of_model(model: Ddr::Attachment).count }
            .by(1)
        end

        it 'has content' do
          create_attachment
          created_attachment = Ddr.query_service.find_all_of_model(model: Ddr::Attachment).first
          expect(created_attachment.content).not_to be_nil
        end

        it 'correctly sets the MIME type' do
          create_attachment
          created_attachment = Ddr.query_service.find_all_of_model(model: Ddr::Attachment).first
          expect(created_attachment.content_type)
            .to eq('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        end

        it 'stores the original file name' do
          create_attachment
          created_attachment = Ddr.query_service.find_all_of_model(model: Ddr::Attachment).first
          expect(created_attachment.original_filename).to eq('sample.docx')
        end

        it 'is attached to the object' do
          create_attachment
          created_attachment = Ddr.query_service.find_all_of_model(model: Ddr::Attachment).first
          expect(created_attachment.attached_to_id).to eq(attach_to.id)
        end

        context 'and attached_to object is governed by a collection' do
          let(:collection) { FactoryBot.create_for_repository(:collection) }

          before do
            att_to = Ddr.query_service.find_by(id: attach_to.id)
            att_to.admin_policy = collection
            Ddr.persister.save(resource: att_to)
          end

          it 'applies the admin policy to the attachment' do
            create_attachment
            created_attachment = Ddr.query_service.find_all_of_model(model: Ddr::Attachment).first
            expect(created_attachment.admin_policy_id).to eq(collection.id)
          end
        end

        it 'validates the checksum when provided' do
          expect_any_instance_of(Ddr::File).to receive(:validate_checksum!)
          create_attachment(checksum: 'ff01aab0eada29d35bb423c5c73a9f67a22bc1fd', checksum_type: 'SHA-1')
        end
      end

      describe 'user cannot add attachments to object' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          create_attachment
          expect(response.response_code).to eq(403)
        end
      end
    end
  end
end
