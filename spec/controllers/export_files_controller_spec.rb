module Ddr
  RSpec.describe ExportFilesController, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user }

    describe '#index' do
      subject { get :index }

      it 'renders the index template' do
        expect(subject).to render_template(:index)
      end
    end

    describe '#new' do
      subject { get :new }

      it 'renders the new template' do
        expect(subject).to render_template(:new)
      end
    end

    describe '#create' do
      let(:component) { FactoryBot.create_for_repository(:component) }

      before do
        role = FactoryBot.build(:role, :public, :viewer)
        component.roles += [role]
        Ddr.persister.save(resource: component)
      end

      it 'succeeds' do
        post :create, params: { identifiers: component.id.id, basename: 'foo', confirmed: true }
        expect(response).to be_successful
      end
    end
  end
end
