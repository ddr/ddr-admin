module Ddr
  RSpec.describe NestedFolderIngestsController, type: :controller do
    describe '#create' do
      let(:user) { FactoryBot.create(:user) }
      let(:basepath) { '/foo/bar/' }
      let(:subpath) { 'baz/' }
      let(:folder_path) { ::File.join(basepath, subpath) }
      let(:checksum_file) { 'my_checksums.txt' }
      let(:metadata_file) { 'my_metadata.txt' }
      let(:test_config_file) do
        Rails.root.join('spec/fixtures/batch_ingest/nested_folder_ingest/nested_folder_ingest.yml')
      end
      let(:test_configuration) { YAML.load_file(test_config_file) }
      let(:checksums_location) { test_configuration[:checksums][:location] }
      let(:metadata_location) { test_configuration[:metadata][:location] }
      let(:checksum_path) { ::File.join(checksums_location, checksum_file) }
      let(:metadata_path) { ::File.join(metadata_location, metadata_file) }
      let(:job_params) do
        { 'admin_set' => 'foo', 'basepath' => basepath, 'batch_user' => user.user_key,
          'checksum_file' => checksum_file, 'collection_id' => '', 'collection_title' => 'Test',
          'config_file' => test_config_file.to_s, 'subpath' => subpath, 'metadata_file' => metadata_file }
      end

      before do
        sign_in user
        allow(Dir).to receive(:exist?).and_call_original
        allow(Dir).to receive(:exist?).with(folder_path).and_return(true)
        allow(::File).to receive(:exist?).and_call_original
        allow(::File).to receive(:exist?).with(checksum_path).and_return(true)
        allow(::File).to receive(:exist?).with(metadata_path).and_return(true)
        allow(::File).to receive(:read).and_call_original
        allow(NestedFolderIngest).to receive(:default_config) { test_configuration }
        allow_any_instance_of(NestedFolderIngest).to receive(:load_configuration) { test_configuration }
        allow_any_instance_of(NestedFolderIngest).to receive(:metadata_path) { metadata_path }
        allow_any_instance_of(IngestMetadata).to receive(:validate_headers).and_return([])
        allow_any_instance_of(IngestMetadata).to receive(:locators).and_return([])
      end

      describe 'when the user can create NestedFolderIngest' do
        before do
          controller.current_ability.can(:create, NestedFolderIngest)
          allow_any_instance_of(NestedFolderIngest).to receive(:inspection_results).and_return(nil)
        end

        it "enqueues the job and renders the 'queued' view" do
          expect(NestedFolderIngestJob).to receive(:perform_later).with(job_params)
          post :create, params: { ddr_nested_folder_ingest: { 'basepath' => basepath, 'subpath' => subpath,
                                                              'checksum_file' => checksum_file, 'collection_id' => '',
                                                              'collection_title' => 'Test',
                                                              'config_file' => test_config_file.to_s,
                                                              'admin_set' => 'foo', 'metadata_file' => metadata_file } }
          expect(response).to render_template(:queued)
        end

        describe 'and the collection is specified' do
          describe 'and the user can add children to the collection' do
            before do
              controller.current_ability.can(:add_children, 'test:1')
            end

            it 'is successful' do
              post :create, params: { ddr_nested_folder_ingest: { 'basepath' => basepath, 'subpath' => subpath,
                                                                  'checksum_file' => checksum_file,
                                                                  'collection_id' => 'test:1' } }
              expect(response.response_code).to eq(200)
            end
          end
        end

        describe 'and the user cannot add children to the collection' do
          before do
            controller.current_ability.cannot(:add_children, 'test:1')
          end

          it 'is forbidden' do
            post :create, params: { ddr_nested_folder_ingest: { 'basepath' => basepath, 'subpath' => subpath,
                                                                'checksum_file' => checksum_file,
                                                                'collection_id' => 'test:1' } }
            expect(response.response_code).to eq(403)
          end
        end
      end

      describe 'when the user cannot create NestedFolderIngest' do
        before do
          controller.current_ability.cannot(:create, NestedFolderIngest)
        end

        describe 'and the collection is not specified' do
          it 'is forbidden' do
            post :create, params: { ddr_nested_folder_ingest: { 'basepath' => basepath, 'subpath' => subpath,
                                                                'checksum_file' => checksum_file,
                                                                'collection_id' => '' } }
            expect(response.response_code).to eq(403)
          end
        end

        describe 'and the collection is specified' do
          describe 'and the user can add children to the collection' do
            before do
              controller.current_ability.can(:add_children, 'test:1')
            end

            it 'is successful' do
              post :create, params: { ddr_nested_folder_ingest: { 'basepath' => basepath, 'subpath' => subpath,
                                                                  'checksum_file' => checksum_file,
                                                                  'collection_id' => 'test:1' } }
              expect(response.response_code).to eq(200)
            end
          end
        end

        describe 'and the user cannot add children to the collection' do
          before do
            controller.current_ability.cannot(:add_children, 'test:1')
          end

          it 'is forbidden' do
            post :create, params: { ddr_nested_folder_ingest: { 'basepath' => basepath, 'subpath' => subpath,
                                                                'checksum_file' => checksum_file,
                                                                'collection_id' => 'test:1' } }
            expect(response.response_code).to eq(403)
          end
        end
      end
    end
  end
end
