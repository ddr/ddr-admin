require 'rails_helper'
require 'support/shared_examples_for_has_content_controllers'

def create_target(opts = {})
  checksum, checksum_type = opts.values_at(:checksum, :checksum_type)
  post :create, params: {
    for_collection_id: collection.id,
    content: {
      ddr_file_type: 'content',
      file: fixture_file_upload('../target.png', 'image/png'),
      checksum:, checksum_type:
    }
  }
end

def parse_response_for_target_id(response)
  location = response.header['Location']
  # E.g., "http://test.host/ddr/targets/eea8c56c-cf15-4173-be28-89e6d5cef5aa/edit"
  matches = location.match(%r{.*/targets/(\h{8}-\h{4}-\h{4}-\h{4}-\h{12})/edit$})
  matches[1]
end

module Ddr
  RSpec.describe TargetsController, :targets, type: :controller do
    let(:user) { FactoryBot.create(:user) }

    before { sign_in user }

    it_behaves_like 'a content resource controller' do
      let(:resource) { FactoryBot.create_for_repository(:target) }
    end

    describe '#show' do
      context 'when the user can read the object' do
        it 'is authorized' do
          obj = FactoryBot.build(:target)
          obj.roles += [FactoryBot.build(:role, :viewer, agent: user)]
          obj = Ddr.persister.save(resource: obj)
          get :show, params: { id: obj }
          expect(response.response_code).to eq(200)
        end
      end

      context 'when the user cannot read the object' do
        it 'is unauthorized' do
          obj = FactoryBot.build(:target)
          obj = Ddr.persister.save(resource: obj)
          get :show, params: { id: obj }
          expect(response.response_code).to eq(403)
        end
      end
    end

    describe '#new' do
      # see shared examples
      let(:collection) { create_for_repository(:collection, access_role: [role]) }

      context 'and user cannot add targets to the collection' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          get :new, params: { for_collection_id: collection.id }
          expect(response.response_code).to eq(403)
        end
      end

      context 'and user can add children to item' do
        let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

        it 'is authorized' do
          get :new, params: { for_collection_id: collection.id }
          expect(response.response_code).to eq(200)
        end
      end
    end

    describe '#create' do
      let(:collection) { create_for_repository(:collection, access_role: [role]) }

      context 'when the user can add targets to the collection' do
        let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }

        it 'creates a new object' do
          expect do
            create_target
          end.to change {
            Ddr.query_service.find_all_of_model(model: Ddr::Target).count
          }.by(1)
        end

        it 'has content' do
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(created_target.content).not_to be_nil
        end

        it 'correctly sets the MIME type' do
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(created_target.content_type).to eq('image/png')
        end

        it 'stores the original file name' do
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(created_target.original_filename).to eq('target.png')
        end

        it 'grants the Editor role in resource scope to the user' do
          role = FactoryBot.build(:role, :editor, :resource, agent: user)
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(created_target.roles).to include(role)
        end

        it 'has a collection' do
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(created_target.for_collection_id).to eq(collection.id)
        end

        it 'sets admin_policy_id' do
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(created_target.admin_policy_id).to eq(collection.admin_policy_id)
        end

        it 'updates derivatives' do
          allow(Ddr::V3::ImageDerivativesUpdateService).to receive(:call)
          create_target
          expect(Ddr::V3::ImageDerivativesUpdateService).to have_received(:call)
        end

        it 'creates an event' do
          expect { create_target }.to change { Ddr::Events::IngestionEvent.count }.by(1)
        end

        it 'redirects to the component edit page' do
          target_id = parse_response_for_target_id(create_target)
          created_target = Ddr.query_service.find_by(id: target_id)
          expect(response).to redirect_to(action: 'edit', id: created_target)
        end

        it 'validates the checksum when provided' do
          expect_any_instance_of(Ddr::File).to receive(:validate_checksum!)
          create_target(checksum: '7cc5abd7ed8c1c907d86bba5e6e18ed6c6ec995c', checksum_type: 'SHA-1')
        end
      end

      context 'when the user cannot add targets to the collection' do
        let(:role) { FactoryBot.build(:role, :downloader, agent: user) }

        it 'is unauthorized' do
          create_target
          expect(response.response_code).to eq(403)
        end
      end
    end
  end
end
