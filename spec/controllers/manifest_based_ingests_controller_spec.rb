module Ddr
  RSpec.describe ManifestBasedIngestsController, type: :controller do
    describe '#create' do
      let(:user) { FactoryBot.create(:user) }
      let(:admin_set) { 'foo' }
      let(:manifest_file) { '/path/to/manifest.txt' }
      let(:job_params) do
        { 'admin_set' => admin_set, 'batch_user' => user.user_key,
          'collection_id' => '', 'collection_title' => nil,
          'config_file' => ManifestBasedIngest::DEFAULT_CONFIG_FILE.to_s,
          'manifest_file' => manifest_file }
      end
      let(:collection_id) { Valkyrie::ID.new(SecureRandom.uuid).id }
      let(:collection_title) { 'Collection Title' }

      before do
        sign_in user
        allow(::File).to receive(:exist?).and_call_original
        allow(::File).to receive(:exist?).with(manifest_file).and_return(true)
        allow_any_instance_of(ManifestBasedIngest).to receive(:load_configuration).and_return({})
        allow(Ddr.query_service).to receive(:find_by).with(id: collection_id) { double('Ddr::Collection') }
      end

      describe 'when the user can create ManifestBasedIngest' do
        before do
          controller.current_ability.can(:create, ManifestBasedIngest)
        end

        it "enqueues the job and renders the 'queued' view" do
          expect(ManifestBasedIngestJob).to receive(:perform_later).with(job_params)
          post :create, params: { ddr_manifest_based_ingest: { 'manifest_file' => manifest_file, 'collection_id' => '',
                                                               'admin_set' => admin_set } }
          expect(response).to render_template(:queued)
        end

        describe 'and the collection is specified' do
          describe 'and the user can add children to the collection' do
            before do
              controller.current_ability.can(:add_children, collection_id)
            end

            it 'is successful' do
              post :create,
                   params: { ddr_manifest_based_ingest: { 'manifest_file' => manifest_file,
                                                          'collection_id' => collection_id } }
              expect(response.response_code).to eq(200)
            end
          end
        end

        describe 'and the user cannot add children to the collection' do
          before do
            controller.current_ability.cannot(:add_children, collection_id)
          end

          it 'is forbidden' do
            post :create,
                 params: { ddr_manifest_based_ingest: { 'manifest_file' => manifest_file,
                                                        'collection_id' => collection_id } }
            expect(response.response_code).to eq(403)
          end
        end

        describe 'when the user cannot create ManifestBasedIngest' do
          before do
            controller.current_ability.cannot(:create, ManifestBasedIngest)
          end

          describe 'and the collection is not specified' do
            it 'is forbidden' do
              post :create,
                   params: { ddr_manifest_based_ingest: { 'manifest_file' => manifest_file, 'collection_id' => '' } }
              expect(response.response_code).to eq(403)
            end
          end

          describe 'and the collection is specified' do
            describe 'and the user can add children to the collection' do
              before do
                controller.current_ability.can(:add_children, collection_id)
              end

              it 'is successful' do
                post :create,
                     params: { ddr_manifest_based_ingest: { 'manifest_file' => manifest_file,
                                                            'collection_id' => collection_id } }
                expect(response.response_code).to eq(200)
              end
            end

            describe 'and the user cannot add children to the collection' do
              before do
                controller.current_ability.cannot(:add_children, collection_id)
              end

              it 'is forbidden' do
                post :create,
                     params: { ddr_manifest_based_ingest: { 'manifest_file' => manifest_file,
                                                            'collection_id' => collection_id } }
                expect(response.response_code).to eq(403)
              end
            end
          end
        end
      end
    end
  end
end
