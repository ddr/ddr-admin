module Ddr
  RSpec.describe StandardIngestsController, :standard_ingest, type: :controller do
    describe '#create' do
      let(:user) { FactoryBot.create(:user) }
      let(:admin_set) { 'foo' }
      let(:basepath) { '/foo/bar/' }
      let(:subpath) { 'baz' }
      let(:folder_path) { ::File.join(basepath, subpath) }
      let(:checksum_path) { ::File.join(folder_path, StandardIngest::CHECKSUM_FILE).to_s }
      let(:data_path) { ::File.join(folder_path, StandardIngest::DATA_DIRECTORY).to_s }
      let(:metadata_path) { ::File.join(folder_path, StandardIngest::METADATA_FILE).to_s }
      let(:job_params) do
        { 'admin_set' => admin_set, 'basepath' => basepath, 'batch_user' => user.user_key,
          'collection_id' => '', 'config_file' => StandardIngest::DEFAULT_CONFIG_FILE.to_s,
          'subpath' => subpath }
      end
      let(:collection_id) { Valkyrie::ID.new(SecureRandom.uuid).id }

      before do
        sign_in user
        allow(Dir).to receive(:exist?).and_call_original
        allow(Dir).to receive(:exist?).with(folder_path).and_return(true)
        allow(Dir).to receive(:exist?).with(data_path).and_return(true)
        allow(::File).to receive(:exist?).and_call_original
        allow(::File).to receive(:exist?).with(checksum_path).and_return(true)
        allow(::File).to receive(:exist?).with(metadata_path).and_return(true)
        allow_any_instance_of(StandardIngest).to receive(:load_configuration).and_return({})
        allow_any_instance_of(StandardIngest).to receive(:validate_metadata_file).and_return(nil)
        allow(Ddr.query_service).to receive(:find_by).with(id: collection_id) { double('Ddr::Collection') }
      end

      describe 'when the user can create StandardIngest' do
        before do
          controller.current_ability.can(:create, StandardIngest)
          allow_any_instance_of(StandardIngest).to receive(:inspection_results).and_return(nil)
        end

        it "enqueues the job and renders the 'queued' view" do
          expect(StandardIngestJob).to receive(:perform_later).with(job_params)
          post :create, params: { ddr_standard_ingest: { 'basepath' => basepath, 'collection_id' => '',
                                                         'admin_set' => admin_set, 'subpath' => subpath } }
          expect(response).to render_template(:queued)
        end

        describe 'and the collection is specified' do
          describe 'and the user can add children to the collection' do
            before do
              controller.current_ability.can(:add_children, collection_id)
            end

            it 'is successful' do
              post :create, params: { ddr_standard_ingest: { 'basepath' => basepath, 'collection_id' => collection_id,
                                                             'subpath' => subpath } }
              expect(response.response_code).to eq(200)
            end
          end
        end

        describe 'and the user cannot add children to the collection' do
          before do
            controller.current_ability.cannot(:add_children, collection_id)
          end

          it 'is forbidden' do
            post :create, params: { ddr_standard_ingest: { 'basepath' => basepath, 'collection_id' => collection_id,
                                                           'subpath' => subpath } }
            expect(response.response_code).to eq(403)
          end
        end
      end

      describe 'when the user cannot create StandardIngest' do
        before do
          controller.current_ability.cannot(:create, StandardIngest)
        end

        describe 'and the collection is not specified' do
          it 'is forbidden' do
            post :create, params: { ddr_standard_ingest: { 'basepath' => basepath, 'collection_id' => '',
                                                           'subpath' => subpath } }
            expect(response.response_code).to eq(403)
          end
        end

        describe 'and the collection is specified' do
          describe 'and the user can add children to the collection' do
            before do
              controller.current_ability.can(:add_children, collection_id)
            end

            it 'is successful' do
              post :create, params: { ddr_standard_ingest: { 'basepath' => basepath, 'collection_id' => collection_id,
                                                             'subpath' => subpath } }
              expect(response.response_code).to eq(200)
            end
          end
        end

        describe 'and the user cannot add children to the collection' do
          before do
            controller.current_ability.cannot(:add_children, collection_id)
          end

          it 'is forbidden' do
            post :create, params: { ddr_standard_ingest: { 'basepath' => basepath, 'collection_id' => collection_id,
                                                           'subpath' => subpath } }
            expect(response.response_code).to eq(403)
          end
        end
      end
    end
  end
end
