describe CatalogController, type: :controller do
  let(:user) { FactoryBot.create(:user) }

  before { sign_in user }

  it 'uses HTTP POST for Solr' do
    expect(controller.blacklight_config.http_method).to eq(:post)
  end
end
