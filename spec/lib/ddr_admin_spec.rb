module Ddr
  RSpec.describe Admin do
    describe 'configuration settings' do
      describe 'defaults' do
        subject { described_class }
        its(:fixity_check_limit) { is_expected.to eq(10_000) }
        its(:fixity_check_period_in_days) { is_expected.to eq(60) }
        its(:batches_per_page) { is_expected.to eq(10) }
        its(:stored_file_permissions) { is_expected.to eq(0o644) }
      end
    end
  end
end
