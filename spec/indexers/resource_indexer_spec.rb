module Ddr
  RSpec.describe ResourceIndexer do
    subject { described_class.new(resource:) }

    describe 'general indexing' do
      subject { indexer.to_solr }

      let(:indexer) { described_class.new(resource:) }
      let(:resource) { build(:item, new_record: false) }

      let(:role1) { build(:role, :curator, :person, :resource) }
      let(:role2) { build(:role, :downloader, :person, :policy) }
      let(:role3) { build(:role, :viewer, :group, :policy) }
      let(:role4) { build(:role, :metadata_viewer, :person, :resource) }

      before do
        resource.aspace_id = 'aspace_dccea43034e1b8261e14cf999e86449d'
        resource.biblical_book = 'Ecclesiastes'
        resource.bibsys_id = 'alma-foo-bar'
        resource.category = 'Category Value'
        resource.chapter_and_verse = 'Ecclesiastes 3:19-4:3'
        resource.company = 'Company Value'
        resource.contentdm_id = '123456'
        resource.display_format = 'image'
        resource.fcrepo3_pid = 'test:1234'
        resource.folder = 'Folder Value'
        resource.genre = 'Genre Value'
        resource.ingested_by = 'foo@bar.com'
        resource.ingestion_date = '2017-01-13T18:55:29Z'
        resource.isFormatOf = 'ark:/99999/fk4aaa'
        resource.isPartOf = 'RL10059CS1010'
        resource.local_id = 'foo'
        resource.medium = 'Medium Value'
        resource.permanent_id = 'ark:/99999/fk4zzz'
        resource.permanent_url = 'http://id.library.duke.edu/ark:/99999/fk4zzz'
        resource.placement_company = 'Placement Company Value'
        resource.product = 'Product Value'
        resource.publication = 'Publication Value'
        resource.rights_note = ['Public domain']
        resource.roles = [role1, role2, role3, role4]
        resource.setting = 'Setting Value'
        resource.temporal = 'Temporal Value'
        resource.tone = 'Tone Value'
        resource.volume = '100'
      end

      its([Index::Fields::ACCESS_ROLE]) { is_expected.to eq(resource.roles.to_json) }
      its([Index::Fields::ADMIN_SET_TITLE]) { is_expected.to be_nil }
      its([Index::Fields::ASPACE_ID]) { is_expected.to eq('aspace_dccea43034e1b8261e14cf999e86449d') }
      its([Index::Fields::BIBSYS_ID]) { is_expected.to eq 'alma-foo-bar' }
      its([Index::Fields::BIBLICAL_BOOK_FACET]) { is_expected.to eq(['Ecclesiastes']) }
      its([Index::Fields::CATEGORY_FACET]) { is_expected.to eq(['Category Value']) }
      its([Index::Fields::CHAPTER_AND_VERSE_FACET]) { is_expected.to eq(['Ecclesiastes 3:19-4:3']) }
      its([Index::Fields::COMMON_MODEL_NAME]) { is_expected.to eq('Item') }
      its([Index::Fields::COMPANY_FACET]) { is_expected.to eq(['Company Value']) }
      its([Index::Fields::CONTENTDM_ID]) { is_expected.to eq('123456') }
      its([Index::Fields::DC_IS_PART_OF]) { is_expected.to eq(['RL10059CS1010']) }
      its([Index::Fields::DISPLAY_FORMAT]) { is_expected.to eq('image') }

      its([Index::Fields::EFFECTIVE_ROLE]) do
        is_expected.to contain_exactly(role1.agent, role2.agent, role3.agent, role4.agent)
      end

      its(['effective_read_ssim']) { is_expected.to contain_exactly(role1.agent, role2.agent, role3.agent) }
      its(['effective_download_ssim']) { is_expected.to contain_exactly(role1.agent, role2.agent) }

      its([Index::Fields::FCREPO3_PID]) { is_expected.to eq('test:1234') }
      its([Index::Fields::FOLDER_FACET]) { is_expected.to eq(['Folder Value']) }
      its([Index::Fields::GENRE_FACET]) { is_expected.to eq(['Genre Value']) }
      its([Index::Fields::INGESTED_BY]) { is_expected.to eq('foo@bar.com') }
      its([Index::Fields::INGESTION_DATE]) { is_expected.to eq('2017-01-13T18:55:29Z') }
      its([Index::Fields::MEDIUM_FACET]) { is_expected.to eq(['Medium Value']) }
      its([Index::Fields::PLACEMENT_COMPANY_FACET]) { is_expected.to eq(['Placement Company Value']) }
      its([Index::Fields::PRODUCT_FACET]) { is_expected.to eq(['Product Value']) }
      its([Index::Fields::PUBLICATION_FACET]) { is_expected.to eq(['Publication Value']) }
      its([Index::Fields::SETTING_FACET]) { is_expected.to eq(['Setting Value']) }
      its([Index::Fields::STREAMABLE_MEDIA_TYPE]) { is_expected.to be_nil }
      its([Index::Fields::TEMPORAL_FACET]) { is_expected.to eq(['Temporal Value']) }
      its([Index::Fields::TONE_FACET]) { is_expected.to eq(['Tone Value']) }
      its([Index::Fields::VOLUME_FACET]) { is_expected.to eq(['100']) }
    end

    describe 'content-bearing object indexing' do
      let(:resource) { build(:component, :with_content_file) }
      let!(:create_date) { Time.parse('2016-01-22T21:50:33Z') }

      before do
        allow(resource.content).to receive(:created_at) { create_date }
      end

      specify do
        expect(subject.to_solr[Index::Fields::CONTENT_CREATE_DATE]).to eq '2016-01-22T21:50:33Z'
        expect(subject.to_solr[Index::Fields::ATTACHED_FILES_HAVING_CONTENT]).to contain_exactly('content')
        expect(subject.to_solr[Index::Fields::CONTENT_SHA1]).to eq '75e2e0cec6e807f6ae63610d46448f777591dd6b'
        expect(subject.to_solr[Index::Fields::CONTENT_SIZE]).to eq 230_714
        expect(subject.to_solr[Index::Fields::CONTENT_SIZE_HUMAN]).to eq '225 KB'
        expect(subject.to_solr[Index::Fields::MEDIA_TYPE]).to eq 'image/tiff'
        expect(subject.to_solr[Index::Fields::MEDIA_MAJOR_TYPE]).to eq 'image'
        expect(subject.to_solr[Index::Fields::MEDIA_SUB_TYPE]).to eq 'tiff'
        expect(subject.to_solr[Index::Fields::ORIGINAL_FILENAME]).to eq(resource.original_filename)
      end

      describe 'image object indexing' do
        before do
          resource.multires_image = build(:ddr_file, :ptif)
          resource.derived_image = build(:ddr_file, :jpg)
        end

        it 'indexes the multires image path' do
          expect(subject.to_solr[Index::Fields::MULTIRES_IMAGE_FILE_PATH]).to exist
        end

        it 'indexes the derived image path' do
          expect(subject.to_solr[Index::Fields::DERIVED_IMAGE_FILE_PATH]).to exist
        end
      end

      describe 'streamable object indexing' do
        before do
          resource.streamable_media = build(:ddr_file, :mp4)
        end

        specify do
          expect(subject.to_solr[Index::Fields::STREAMABLE_MEDIA_TYPE]).to eq 'video/mp4'
        end
      end
    end

    describe 'admin set title' do
      let(:resource) { build(:item, parent_id: coll.id, admin_policy_id: coll.id, new_record: false) }
      let(:coll) { create(:collection, id: build(:valkyrie_id)) }

      specify do
        expect(subject.to_solr[Index::Fields::ADMIN_SET_TITLE]).to eq 'Digital Collections'
      end
    end

    describe 'description' do
      let(:description) do
        <<~EOT
          neque vitae tempus quam pellentesque nec nam aliquam sem et tortor consequat id porta nibh venenatis
          cras sed felis eget velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum integer
          enim neque volutpat ac tincidunt vitae semper quis lectus nulla at volutpat diam ut venenatis tellus
          in metus vulputate eu scelerisque felis imperdiet proin fermentum leo vel orci porta non pulvinar neque
          laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt eget nullam non nisi est sit
          amet facilisis magna etiam tempor orci eu lobortis elementum nibh tellus molestie nunc non blandit massa
          enim nec dui nunc mattis enim ut tellus elementum sagittis vitae et leo duis ut diam quam nulla porttitor
          massa id neque aliquam vestibulum morbi blandit cursus risus at ultrices mi tempus imperdiet nulla malesuada
          pellentesque elit eget gravida cum sociis natoque penatibus et magnis dis parturient montes nascetur
          ridiculus mus mauris vitae ultricies leo integer malesuada nunc vel risus commodo viverra maecenas accumsan
          lacus vel
        EOT
      end
      let(:resource) { build(:item, new_record: false) }

      it 'indexes description_tesim (Ddr::Index::Fields::DESCRIPTION' do
        resource.description = [description.strip]
        expect(subject.to_solr[Ddr::Index::Fields::DESCRIPTION]).to eq(Array(description.strip))
      end
    end

    describe 'language name' do
      let(:resource) { build(:item, new_record: false) }

      before do
        resource.language = ['cym']
      end

      specify do
        expect(subject.to_solr[Index::Fields::LANGUAGE_FACET]).to eq ['Welsh']
        expect(subject.to_solr[Index::Fields::LANGUAGE_NAME]).to eq ['Welsh']
      end
    end

    describe 'nested path' do
      let(:resource) { build(:item, new_record: false) }

      before do
        resource.nested_path = '/foo/bar/baz'
      end

      specify do
        expect(subject.to_solr[Index::Fields::NESTED_PATH_TEXT]).to eq '/foo/bar/baz'
      end
    end

    describe 'extracted text' do
      let(:resource) { build(:item, new_record: false) }
      let(:children_extracted_texts) do
        [
          'Fusce urna erat, tincidunt vel.',
          'Aliquam dictum finibus venenatis. Sed.',
          'Fusce mattis nibh eget ipsum.'
        ]
      end

      before do
        allow(resource).to receive(:all_text) { children_extracted_texts }
      end

      it 'indexes the combined text of its children' do
        expect(subject.to_solr[Index::Fields::ALL_TEXT]).to match_array(children_extracted_texts)
      end
    end

    describe 'backwards compatible parentage indexing' do
      describe 'item' do
        let(:collection) { create_for_repository(:collection) }
        let(:resource) { build(:item, parent_id: collection.id, new_record: false) }

        it 'indexes the parent id as IS_MEMBER_OF_COLLECTION' do
          expect(subject.to_solr[Index::Fields::IS_MEMBER_OF_COLLECTION]).to eq(collection.id.to_s)
        end
      end

      describe 'compatible' do
        let(:item) { create_for_repository(:item) }
        let(:resource) { build(:component, parent_id: item.id, new_record: false) }

        it 'indexes the parent id as IS_PART_OF' do
          expect(subject.to_solr[Index::Fields::IS_PART_OF]).to eq(item.id.id)
        end
      end
    end

    describe 'exception handling' do
      let(:resource) { create_for_repository(:item) }

      it 'rescues from Ddr::SolrDocumentBehavior::NotFound exception and raises Ddr::IndexError' do
        allow(subject).to receive(:index_fields).and_raise(Ddr::SolrDocumentBehavior::NotFound)
        expect { subject.to_solr }.to raise_error(Ddr::IndexError)
      end
    end
  end
end
