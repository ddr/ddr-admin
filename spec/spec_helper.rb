require_relative 'support/create_strategy_for_repository_pattern'

module SpecHelper
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.order = :random

  Kernel.srand config.seed

  config.before(:suite) do
    FactoryBot.register_strategy(:create_for_repository, CreateStategyForRepositoryPattern)
  end
end
