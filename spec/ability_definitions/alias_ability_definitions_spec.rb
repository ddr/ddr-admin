require 'rails_helper'
require 'cancan/matchers'

module Ddr::Admin
  RSpec.describe AliasAbilityDefinitions do
    subject { described_class.call(ability) }

    let(:ability) { FactoryBot.build(:abstract_ability) }

    it 'aliases actions to :read' do
      expect(subject.aliased_actions[:read])
        .to include(:attachments, :captions, :collection_info, :components, :event, :events, :files, :intermediate,
                    :items, :stream, :structural_metadata, :targets)
    end

    it 'aliases actions to :grant' do
      expect(subject.aliased_actions[:grant]).to include(:roles)
    end
  end
end
