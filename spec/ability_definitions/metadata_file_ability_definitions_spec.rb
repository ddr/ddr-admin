require 'rails_helper'
require 'cancan/matchers'

module Ddr
  module Admin
    RSpec.describe MetadataFileAbilityDefinitions do
      subject { described_class.call(ability) }

      let(:ability) { FactoryBot.build(:abstract_ability) }

      describe 'create' do
        describe 'when the user is a curator' do
          before { allow(ability).to receive(:curator?).and_return(true) }

          it { is_expected.to be_able_to(:create, MetadataFile) }
        end

        describe 'when the user is not a member of the metadata file creators group' do
          before { allow(ability).to receive(:curator?).and_return(false) }

          it { is_expected.not_to be_able_to(:create, MetadataFile) }
        end
      end

      describe 'show' do
        let(:resource) { FactoryBot.create(:metadata_file_csv) }

        describe 'when the user is the creator of the MetadataFile' do
          before { allow(ability).to receive(:user) { resource.user } }

          it { is_expected.to be_able_to(:show, resource) }
        end

        describe 'when the user is not the creator of the MetadataFile' do
          it { is_expected.not_to be_able_to(:show, resource) }
        end
      end

      describe 'procezz' do
        let(:resource) { FactoryBot.create(:metadata_file_csv) }

        describe 'when the user is the creator of the MetadataFile' do
          before { allow(ability).to receive(:user) { resource.user } }

          it { is_expected.to be_able_to(:procezz, resource) }
        end

        describe 'when the user is not the creator of the MetadataFile' do
          it { is_expected.not_to be_able_to(:procezz, resource) }
        end
      end
    end
  end
end
