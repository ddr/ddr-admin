require 'rails_helper'
require 'cancan/matchers'

module Ddr
  module Admin
    RSpec.describe IngestFolderAbilityDefinitions do
      subject { described_class.call(ability) }

      let(:ability) { FactoryBot.build(:ability) }
      let(:collection) { create_for_repository(:collection) }
      let(:ingest_folder) { Ddr::IngestFolder.new(collection_id: collection.id.to_s) }

      describe 'when able to add children to the collection' do
        before do
          ability.can :add_children, ingest_folder.collection
        end

        it { is_expected.to be_able_to(:create, ingest_folder) }
      end

      describe 'when not able to add children to the collection' do
        before do
          ability.cannot :add_children, ingest_folder.collection
        end

        it { is_expected.not_to be_able_to(:create, ingest_folder) }
      end
    end
  end
end
