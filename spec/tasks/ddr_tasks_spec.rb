require 'rake'
Rails.application.load_tasks

RSpec.describe 'DDR rake tasks' do
  describe 'ddr:destroy_objects' do
    let(:ids) { %w[6842f70b-824f-425b-9b9a-cd6db5769710 1cfdc307-b770-4826-a06f-6706a2a3a852] }

    before do
      allow(Ddr::V3::ResourceDeletionService).to receive(:bulk)
    end

    it 'strips blank lines and spaces', skip: 'Race condition?' do
      allow(STDIN).to receive(:gets).and_return("y\n")

      Tempfile.open do |f|
        f.puts(ids.map { |id| "#{id} " })
        f.puts "\n" * 3
        f.close

        Rake::Task['ddr:destroy_objects'].invoke(f.path)
      end

      expect(Ddr::V3::ResourceDeletionService).to have_received(:bulk).with(resource_ids: ids)
    end

    it 'aborts when not confirmed' do
      allow(STDIN).to receive(:gets).and_return("N\n")

      Tempfile.open do |f|
        f.puts ids
        f.close
        Rake::Task['ddr:destroy_objects'].invoke(f.path)
      end

      expect(Ddr::V3::ResourceDeletionService).not_to have_received(:bulk)
    end
  end

  describe 'ddr:index:delete_by_id' do
    let(:id) { SecureRandom.uuid }

    it 'calls the API' do
      allow(Ddr::API::IndexRepository).to receive(:delete_by_id).with(id)
      Rake::Task['ddr:index:delete_by_id'].invoke(id)
      expect(Ddr::API::IndexRepository).to have_received(:delete_by_id).with(id)
    end
  end

  describe 'ddr:index:item_children' do
    before do
      @item = create_for_repository(:item)
      @comp = create_for_repository(:component, parent_id: @item.id)
    end

    it 'indexes the children of the item' do
      allow(Ddr::IndexBatchJob).to receive(:perform_later).with([@comp.id.to_s])
      Rake::Task['ddr:index:item_children'].invoke(@item.id.to_s)
      expect(Ddr::IndexBatchJob).to have_received(:perform_later).with([@comp.id.to_s])
    end
  end
end
