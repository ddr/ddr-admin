module Ddr::Auth
  RSpec.describe Group do
    subject { described_class.new('admins', label: 'Administrators') }

    its(:agent) { is_expected.to eq('admins') }
    its(:label) { is_expected.to eq('Administrators') }
    its(:id) { is_expected.to eq('admins') }
    its(:to_s) { is_expected.to eq('admins') }
    it { is_expected.to be_frozen }

    describe 'default label' do
      subject { described_class.new('admins') }
      its(:label) { is_expected.to eq('admins') }
    end

    describe 'equality' do
      it { is_expected.to eq('admins') }
      it { is_expected.to eq(described_class.new('admins')) }
      it { is_expected.not_to eq(described_class.new('administrators', label: 'Administrators')) }
    end

    describe '#has_member?' do
      let(:auth_context) { FactoryBot.build(:auth_context) }

      describe "when the group doesn't have a rule" do
        describe "and the group is in the user's groups" do
          before { allow(auth_context).to receive(:groups) { [subject] } }

          it 'is true' do
            expect(subject.has_member?(auth_context)).to be true
          end
        end

        describe "and the group is not in the user's groups" do
          before { allow(auth_context).to receive(:groups).and_return([]) }

          it 'is false' do
            expect(subject.has_member?(auth_context)).to be false
          end
        end
      end

      describe 'when the group has a rule' do
        describe 'and the user passes the rule' do
          subject do
            described_class.new('admins', label: 'Administrators') do |_user|
              true
            end
          end
          it 'is true' do
            expect(subject.has_member?(auth_context)).to be true
          end
        end

        describe 'and the user fails the rule' do
          subject do
            described_class.new('admins', label: 'Administrators') do |_user|
              false
            end
          end
          it 'is false' do
            expect(subject.has_member?(auth_context)).to be false
          end
        end
      end
    end
  end
end
