module Ddr::Auth
  RSpec.describe Groups do
    describe '.call' do
      subject { Groups.call(auth_context) }

      describe 'anonymous' do
        let(:auth_context) { FactoryBot.build(:auth_context, :anonymous) }

        it { is_expected.to include(Groups::PUBLIC) }
        it { is_expected.not_to include(Groups::REGISTERED) }
        it { is_expected.not_to include(Groups::DUKE_ALL) }

        it 'does not include affiliation groups' do
          expect(subject.any? { |g| AffiliationGroups::ALL.include?(g) }).to be false
        end
      end

      describe 'authenticated (not Duke)' do
        let(:auth_context) { FactoryBot.build(:auth_context) }

        it { is_expected.to include(Groups::PUBLIC) }
        it { is_expected.to include(Groups::REGISTERED) }
        it { is_expected.not_to include(Groups::DUKE_ALL) }

        it 'does not include affiliation groups' do
          expect(subject.any? { |g| AffiliationGroups::ALL.include?(g) }).to be false
        end
      end

      describe 'authenticated (Duke)' do
        let(:auth_context) { FactoryBot.build(:auth_context, :duke) }

        it { is_expected.to include(Groups::PUBLIC) }
        it { is_expected.to include(Groups::REGISTERED) }
        it { is_expected.to include(Groups::DUKE_ALL) }

        describe 'with affiliations' do
          before do
            allow(auth_context).to receive(:affiliation).and_return([Affiliation::STAFF, Affiliation::STUDENT])
          end

          it { is_expected.to include(AffiliationGroups::STAFF) }
          it { is_expected.to include(AffiliationGroups::STUDENT) }
          it { is_expected.not_to include(AffiliationGroups::FACULTY) }
          it { is_expected.not_to include(AffiliationGroups::AFFILIATE) }
          it { is_expected.not_to include(AffiliationGroups::ALUMNI) }
          it { is_expected.not_to include(AffiliationGroups::EMERITUS) }
        end

        describe 'with remote groups' do
          before do
            allow(auth_context).to receive(:ismemberof).and_return(['duke:foo:bar', 'duke:spam:eggs'])
          end

          it { is_expected.to include(Group.new('duke:foo:bar')) }
          it { is_expected.to include(Group.new('duke:spam:eggs')) }
        end
      end
    end
  end
end
