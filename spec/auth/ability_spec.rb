require 'rails_helper'
require 'cancan/matchers'

module Ddr::Auth
  RSpec.describe Ability, :abilities, type: :model do
    subject { described_class.new(auth_context) }

    let(:auth_context) { FactoryBot.build(:auth_context) }

    describe 'aliases' do
      it 'has :replace aliases' do
        expect(subject.aliased_actions[:replace]).to contain_exactly(:upload)
      end

      it 'has :add_children aliases' do
        expect(subject.aliased_actions[:add_children]).to contain_exactly(:add_attachment, :add_target)
      end
    end

    describe 'File download abilities' do
      let(:obj) { FactoryBot.build(:component) }

      AliasAbilityDefinitions::DOWNLOAD_ALIASES.each do |action, permission|
        describe "#{action}" do
          describe "can #{permission} resource" do
            before { subject.can permission, obj }

            it { is_expected.to be_able_to(action, obj) }
          end

          describe "cannot #{permission} object" do
            before { subject.cannot permission, obj }

            it { is_expected.not_to be_able_to(action, obj) }
          end
        end
      end
    end

    describe 'AdminSet abilities'

    describe 'Item abilities' do
      describe 'when the item has a parent' do
        let(:parent) { create_for_repository(:collection, admin_set: 'dc', access_role: role) }
        let(:item) { FactoryBot.build(:item, parent_id: parent.id) }

        describe 'and can add children to the parent' do
          let(:role) { FactoryBot.build(:role, :curator, agent: auth_context.user.agent) }

          it { is_expected.to be_able_to(:create, item) }
        end

        describe 'and cannot add children to the parent' do
          let(:role) { FactoryBot.build(:role, :public, :downloader) }

          it { is_expected.not_to be_able_to(:create, item) }
        end
      end

      describe 'when the item does not have a parent' do
        let(:item) { FactoryBot.build(:item) }

        it { is_expected.not_to be_able_to(:create, item) }
      end
    end

    describe 'Component abilities' do
      let(:component) { FactoryBot.build(:component) }

      describe 'when the component has a parent' do
        let(:parent) { create_for_repository(:item, access_role: role) }

        before { component.parent_id = parent.id }

        describe 'and can add children to the parent' do
          let(:role) { FactoryBot.build(:role, :curator, agent: auth_context.user.agent) }

          it { is_expected.to be_able_to(:create, component) }
        end

        describe 'and cannot add children to the parent' do
          let(:role) { FactoryBot.build(:role, :public, :downloader) }

          it { is_expected.not_to be_able_to(:create, component) }
        end
      end

      describe 'when the component does not have a parent' do
        it { is_expected.not_to be_able_to(:create, component) }
      end
    end

    describe 'Attachment abilities' do
      let(:attachment) { FactoryBot.build(:attachment) }

      describe 'when the attachment is attached' do
        let(:attached_to) { create_for_repository(:collection, admin_set: 'dc', access_role: role) }

        before { attachment.attached_to_id = attached_to.id }

        describe 'and can add attachment to the attached' do
          let(:role) { FactoryBot.build(:role, :curator, agent: auth_context.user.agent) }

          it { is_expected.to be_able_to(:create, attachment) }
        end

        describe 'and cannot add attachment to the attached' do
          let(:role) { FactoryBot.build(:role, :public, :downloader) }

          it { is_expected.not_to be_able_to(:create, attachment) }
        end
      end

      describe 'when the attachment is not attached' do
        it { is_expected.not_to be_able_to(:create, attachment) }
      end
    end

    describe 'publication abilities' do
      let(:workflow_state) { 'unpublished' }
      let(:resource) { create_for_repository(:collection, workflow_state:) }

      describe 'publish' do
        describe 'when role-based permissions permit publish' do
          before do
            allow(resource).to receive(:effective_permissions).and_return([Permissions::PUBLISH])
          end

          describe 'when the resource is published' do
            let(:workflow_state) { 'published' }

            it { is_expected.to be_able_to(:publish, resource) }
          end

          describe 'when the resource is not published' do
            describe 'when the resource is publishable' do
              it { is_expected.to be_able_to(:publish, resource) }
            end

            describe 'when the resource is not publishable' do
              let(:workflow_state) { 'nonpublishable' }

              it { is_expected.to be_able_to(:publish, resource) }
            end
          end
        end

        describe 'when role-based permissions do not permit publish' do
          it { is_expected.not_to be_able_to(:publish, resource) }
        end
      end

      describe 'unpublish' do
        describe 'when role-based permissions permit unpublish' do
          before do
            allow(resource).to receive(:effective_permissions).and_return([Permissions::UNPUBLISH])
          end

          describe 'when the resource is published' do
            let(:workflow_state) { 'published' }

            it { is_expected.to be_able_to(:unpublish, resource) }
          end

          describe 'when the resource is not published' do
            it { is_expected.to be_able_to(:unpublish, resource) }
          end
        end

        describe 'when role-based permissions do not permit unpublish' do
          it { is_expected.not_to be_able_to(:unpublish, resource) }
        end
      end

      describe 'make_nonpublishable' do
        describe 'when role-based permissions permit making nonpublishable' do
          before do
            allow(resource).to receive(:effective_permissions).and_return([Permissions::MAKE_NONPUBLISHABLE])
          end

          describe 'when the resource is published' do
            let(:workflow_state) { 'published' }

            it { is_expected.to be_able_to(:make_nonpublishable, resource) }
          end

          describe 'when the resource is not published' do
            it { is_expected.to be_able_to(:make_nonpublishable, resource) }
          end
        end

        describe 'when role-based permissions do not permit making nonpublishable' do
          it { is_expected.not_to be_able_to(:make_nonpublishable, resource) }
        end
      end
    end # publication

    describe 'embargoes' do
      let(:parent) { Ddr::Item.new }
      let(:obj) { Ddr::Component.new }

      describe 'resource is embargoed' do
        before do
          allow(obj).to receive(:effective_permissions).and_return([Permissions::READ, Permissions::DOWNLOAD])
          allow(parent).to receive(:available) { DateTime.now + 10 }
          allow(obj).to receive(:parent) { parent }
        end

        it { is_expected.not_to be_able_to(:read, obj) }
        it { is_expected.not_to be_able_to(:download, obj) }
      end

      describe 'resource is embargoed, but user can edit' do
        before do
          allow(obj).to receive(:effective_permissions).and_return([Permissions::READ, Permissions::DOWNLOAD,
                                                                    Permissions::UPDATE])
          allow(parent).to receive(:available) { DateTime.now + 10 }
          allow(obj).to receive(:parent) { parent }
        end

        it { is_expected.to be_able_to(:read, obj) }
        it { is_expected.to be_able_to(:download, obj) }
      end

      describe 'resource is not embargoed' do
        before do
          allow(obj).to receive(:effective_permissions).and_return([Permissions::READ, Permissions::DOWNLOAD])
          allow(parent).to receive(:available) { DateTime.now - 10 }
          allow(obj).to receive(:parent) { parent }
        end

        it { is_expected.to be_able_to(:read, obj) }
        it { is_expected.to be_able_to(:download, obj) }
      end

      describe 'resource is not embargoed; no available value' do
        before do
          allow(obj).to receive(:effective_permissions).and_return([Permissions::READ, Permissions::DOWNLOAD])
          allow(parent).to receive(:available).and_return(nil)
          allow(obj).to receive(:parent) { parent }
        end

        it { is_expected.to be_able_to(:read, obj) }
        it { is_expected.to be_able_to(:download, obj) }
      end

      describe 'resource is not embargoed; legacy data' do
        before do
          allow(obj).to receive(:effective_permissions).and_return([Permissions::READ, Permissions::DOWNLOAD])
          allow(parent).to receive(:available).and_return([])
          allow(obj).to receive(:parent) { parent }
        end

        it { is_expected.to be_able_to(:read, obj) }
        it { is_expected.to be_able_to(:download, obj) }
      end
    end

    describe 'role based abilities' do
      shared_examples 'it has role based abilities' do
        describe 'when permissions are cached' do
          before { subject.cache[cache_key] = [Permissions::READ] }

          it 'uses the cached permissions' do
            expect(perm_obj).not_to receive(:effective_permissions)
            expect(subject).to be_able_to(:read, obj)
            expect(subject).not_to be_able_to(:edit, obj)
          end
        end

        describe 'when permissions are not cached' do
          describe 'and user context has role based permission' do
            before do
              allow(perm_obj).to receive(:effective_permissions).and_return([Permissions::UPDATE])
            end

            it { is_expected.to be_able_to(:edit, obj) }
          end

          describe 'and user context does not have role based permission' do
            before do
              allow(perm_obj).to receive(:effective_permissions).and_return([Permissions::READ])
            end

            it { is_expected.not_to be_able_to(:edit, obj) }
          end
        end
      end

      describe 'with a Ddr model instance' do
        let(:obj) { Ddr::Collection.new(id: 'test:1') }
        let(:cache_key) { obj.id }
        let(:perm_obj) { obj }

        it_behaves_like 'it has role based abilities'
      end

      describe 'with a Solr document' do
        let(:obj) { ::SolrDocument.new({ 'id' => 'test:1' }) }
        let(:cache_key) { obj.pid }
        let(:perm_obj) { obj }

        before do
          allow(perm_obj).to receive(:resource) do
            Ddr::Resource.new({ 'id' => 'test:1' })
          end
        end

        it_behaves_like 'it has role based abilities'
      end

      describe 'with a String' do
        let(:obj) { 'test:1' }
        let(:cache_key) { obj }
        let(:perm_obj) { ::SolrDocument.new({ 'id' => 'test:1' }) }

        before do
          allow(::SolrDocument).to receive(:find).with('test:1') { perm_obj }
        end

        it_behaves_like 'it has role based abilities'
      end
    end
  end
end
