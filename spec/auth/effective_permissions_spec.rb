module Ddr::Auth
  RSpec.describe EffectivePermissions do
    let(:resource) { Ddr::Item.new }
    let(:policy) { Ddr::Collection.new }
    let(:agents) { ['Editors', 'bob@example.com'] }
    let(:editor) { FactoryBot.build(:role, :editor, :policy, agent: 'Editors') }
    let(:downloader) { FactoryBot.build(:role, :downloader, :public, :resource) }

    before do
      policy.access_role = [editor]
      resource.admin_policy = Ddr.persister.save(resource: policy)
      resource.access_role = [downloader]
    end

    it "returns the list of permissions granted to the agents on the resource in resource scope, plus the permissions granted to the agents on the resource's policy in policy scope" do
      expect(described_class.call(resource, agents))
        .to contain_exactly(:discover, :read, :download, :add_children, :update, :replace)
    end
  end
end
