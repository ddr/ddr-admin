module Ddr::Auth
  module Roles
    RSpec.describe RoleType do
      subject { described_class.new('Role Type', 'Role Description', %i[read write]) }

      it { is_expected.to be_frozen }
      its(:title) { is_expected.to eq('Role Type') }
      its(:title) { is_expected.to be_frozen }
      its(:label) { is_expected.to eq('Role Type') }
      its(:to_s) { is_expected.to eq('Role Type') }
      its(:description) { is_expected.to eq('Role Description') }
      its(:description) { is_expected.to be_frozen }
      its(:permissions) { is_expected.to eq(%i[read write]) }
      its(:permissions) { is_expected.to be_frozen }
    end
  end
end
