module Ddr::Auth
  RSpec.describe EffectiveRoles do
    let(:group_editor) { FactoryBot.build(:role, :editor, :policy, agent: 'Editors') }
    let(:public_downloader) { FactoryBot.build(:role, :downloader, :public) }
    let(:public_metadata_viewer) { FactoryBot.build(:role, :metadata_viewer, :public, :policy) }
    let(:collection) { create_for_repository(:collection, admin_set: 'dc', access_role: [group_editor]) }
    let(:item) { create_for_repository(:item, admin_policy: collection, access_role: [public_downloader]) }
    let(:component) { create_for_repository(:component, admin_policy: collection, parent_id: item.id) }

    describe 'when called with a list of agents' do
      specify do
        expect(described_class.call(item, ['bob@example.com', 'public']))
          .to contain_exactly(public_downloader)
      end

      specify do
        expect(described_class.call(item, ['Admins']))
          .to be_empty
      end
    end

    describe 'when called without a list of agents' do
      specify do
        expect(described_class.call(item))
          .to contain_exactly(public_downloader, group_editor)
      end
    end

    describe 'item policy roles' do
      before do
        change_set = Ddr::ItemChangeSet.new(item)
        change_set.access_role = [public_downloader, public_metadata_viewer]
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
      end

      describe 'on the item' do
        subject { described_class.call(item) }
        it { is_expected.to contain_exactly(public_downloader, group_editor, public_metadata_viewer) }
      end

      describe 'on the component' do
        subject { described_class.call(component) }
        it { is_expected.to contain_exactly(group_editor, public_metadata_viewer) }
      end
    end
  end
end
