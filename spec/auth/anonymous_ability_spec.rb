module Ddr::Auth
  RSpec.describe AnonymousAbility do
    subject { described_class.new(auth_context) }
    let(:auth_context) { FactoryBot.build(:auth_context, :anonymous) }

    its(:ability_definitions) { is_expected.to eq(Ability.ability_definitions) }
  end
end
