RSpec.describe 'Collection requests', :collection, type: :request do
  let(:user) { FactoryBot.create(:user) }
  let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }
  let(:collection) { create_for_repository(:collection, admin_set: 'dc', access_role: role) }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'renders the show page' do
    get '/ddr/collections/%s' % collection.id
    expect(response).to have_http_status(:ok)
  end

  it 'renders the metadata export form' do
    get '/ddr/collections/%s/export' % collection.id
    expect(response).to have_http_status(:ok)
  end

  it 'redirects a collection metadata export request to the API' do
    get '/ddr/collections/%s/export.csv?type=descmd&scope=collection' % collection.id
    expect(response).to redirect_to('/api/resources/%s/members.csv?csv_fields=parent_id' % collection.id)
  end

  it 'handles the original_filename option properly' do
    get '/ddr/collections/%s/export.csv?type=descmd&scope=collection&include_original_filename=1' % collection.id
    expect(response).to redirect_to("/api/resources/#{collection.id}/members.csv?csv_fields=parent_id%2Coriginal_filename")
  end

  it 'redirects an admin set metadata export request to the API' do
    get '/ddr/collections/%s/export.csv?type=descmd&scope=admin_set' % collection.id
    expect(response).to redirect_to('/api/resources.csv?csv_fields=parent_id&admin_set=Digital+Collections')
  end

  it 'renders the items page' do
    create_for_repository(:item, parent_id: collection.id)
    get '/ddr/collections/%s/items' % collection.id
    expect(response).to have_http_status(:ok)
  end

  it 'renders the targets page' do
    create_for_repository(:target, for_collection_id: collection.id)
    get '/ddr/collections/%s/targets' % collection.id
    expect(response).to have_http_status(:ok)
  end

  it 'renders the attachments page' do
    create_for_repository(:attachment, attached_to_id: collection.id)
    get '/ddr/collections/%s/attachments' % collection.id
    expect(response).to have_http_status(:ok)
  end
end
