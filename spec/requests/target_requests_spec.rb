RSpec.describe 'Target requests', :target, type: :request do
  let(:user) { FactoryBot.create(:user) }
  let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }
  let(:collection) { create_for_repository(:collection, admin_set: 'dc', access_role: role) }
  let(:target) { create_for_repository(:target, for_collection_id: collection.id, admin_policy_id: collection.id) }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'renders the new Target page' do
    get '/ddr/targets/new', params: { for_collection_id: collection.id }
    expect(response).to have_http_status(:ok)
  end

  it 'renders the show page' do
    get '/ddr/targets/%s' % target.id
    expect(response).to have_http_status(:ok)
  end
end
