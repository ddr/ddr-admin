RSpec.describe 'Metadata file requests', :metadata_file, type: :request do
  after { Warden.test_reset! }

  it 'renders a new form' do
    allow(Ddr::Admin).to receive(:curators_group).and_return(Ddr::Auth::Groups::REGISTERED)
    user = FactoryBot.create(:user)
    sign_in user
    get '/ddr/metadata_files/new'
    expect(response).to have_http_status(:ok)
  end

  it 'renders a show page' do
    metadata_file = FactoryBot.create(:metadata_file_csv)
    sign_in metadata_file.user
    get '/ddr/metadata_files/%s' % metadata_file.id
    expect(response).to have_http_status(:ok)
  end
end
