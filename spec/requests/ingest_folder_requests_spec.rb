RSpec.describe 'IngestFolder requests', :ingest_folder, type: :request do
  let(:user) { FactoryBot.create(:user) }
  let(:role) { Ddr::Auth::Roles::Role.new(agent: user, role_type: 'Curator', scope: 'policy') }
  let(:collection) { create_for_repository(:collection, admin_set: 'dc', access_role: role) }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'render the new ingest folder form' do
    get '/ddr/ingest_folders/new', params: { ddr_ingest_folder: { collection_id: collection.id } }
    expect(response).to have_http_status(:ok)
  end
end
