RSpec.describe 'Component requests', :component, type: :request do
  let(:user) { FactoryBot.create(:user) }
  let(:role) { FactoryBot.build(:role, :resource, :curator, agent: user) }
  let(:component) { create_for_repository(:component, access_role: [role]) }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'renders the show page' do
    get '/ddr/components/%s' % component.id
    expect(response).to have_http_status(:ok)
  end
end
