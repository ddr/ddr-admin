RSpec.describe 'Item requests', :item, type: :request do
  let(:user) { FactoryBot.create(:user) }
  let(:role) { FactoryBot.build(:role, :resource, :curator, agent: user) }
  let(:item) { create_for_repository(:item, access_role: [role]) }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'renders the show page' do
    get '/ddr/items/%s' % item.id
    expect(response).to have_http_status(:ok)
  end
end
