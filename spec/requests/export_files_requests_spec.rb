RSpec.describe 'Export files requests' do
  let(:user) { FactoryBot.create(:user) }
  let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }
  let(:collection) { create_for_repository(:collection, admin_set: 'dc', access_role: role) }
  let(:item) { create_for_repository(:item, parent_id: collection.id) }
  let(:component) { create_for_repository(:component, :with_content_file, parent_id: item.id) }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'renders the new form' do
    get '/ddr/export_files/new'
    expect(response).to have_http_status(:ok)
  end

  it 'renders the create form' do
    component
    post '/ddr/export_files', params: { identifiers: collection.id_s, basename: 'test' }
    expect(response).to have_http_status(:ok)
  end
end
