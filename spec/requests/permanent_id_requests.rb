RSpec.describe 'Permanent ID requests', type: :request do
  let(:user) { FactoryBot.create(:user) }
  # let(:role) { FactoryBot.build(:role, :curator, :policy, agent: user) }
  let(:resource) { create_for_repository(:collection, permanent_id: 'ark:/99999/fk4zzzzz', admin_set: 'dc') }

  before { sign_in user }
  after { Warden.test_reset! }

  it 'redirects to the show page' do
    get '/id/%s' % resource.permanent_id
    expect(response).to redirect_to(resource)
  end
end
