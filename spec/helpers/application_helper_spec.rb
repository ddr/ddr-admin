RSpec.describe ApplicationHelper, type: :helper do
  describe '#values_as_array' do
    describe 'already an Array' do
      let(:values) { ['abcdef'] }

      it 'returns the Array of values' do
        expect(helper.values_as_array(values)).to match_array(values)
      end
    end

    describe 'not an Array' do
      describe 'string' do
        let(:values) { 'abcdef' }

        it 'returns an Array containing the string value' do
          expect(helper.values_as_array(values)).to contain_exactly(values)
        end
      end

      describe 'time' do
        let(:time) { Time.now }
        let(:values) { time }

        it 'returns an Array containing the time value' do
          expect(helper.values_as_array(values)).to contain_exactly(values)
        end
      end

      describe 'nil' do
        let(:values) { nil }

        it 'returns an empty Array' do
          expect(helper.values_as_array(values)).to be_empty
        end
      end
    end
  end

  describe '#render_content_type_and_size' do
    context 'with a document' do
      let(:doc) { SolrDocument.new({}) }
      let(:resource) { double(content_human_size: '5K') }

      before do
        allow(doc).to receive(:content_type).and_return('application/pdf')
        allow(doc).to receive(:resource) { resource }
      end

      it 'renders the content type and size' do
        expect(helper.render_content_type_and_size(doc)).to eq('application/pdf 5K')
      end
    end

    context 'with an object' do
      let(:obj) { Ddr::Component.new }

      before do
        allow(obj).to receive(:content_type).and_return('application/pdf')
        allow(obj).to receive(:content_human_size).and_return('5K')
      end

      it 'renders the content type and size' do
        expect(helper.render_content_type_and_size(obj)).to eq('application/pdf 5K')
      end
    end
  end

  describe '#render_stream_link' do
    let(:id) { SecureRandom.uuid }
    let(:solr_doc) { SolrDocument.new('id' => id, Ddr::Index::Fields::RESOURCE_MODEL => 'Ddr::Component') }

    it 'returns the correct link to a streamable file' do
      expect(helper.render_stream_link(solr_doc)).to include("ddr/components/#{id}/stream")
    end
  end

  describe '#render_intermediate_link' do
    let(:id) { SecureRandom.uuid }
    let(:solr_doc) { SolrDocument.new('id' => id, Ddr::Index::Fields::RESOURCE_MODEL => 'Ddr::Component') }

    it 'returns the correct link to an intermediate file' do
      expect(helper.render_intermediate_link(solr_doc)).to include("ddr/download/#{id}?ddr_file_type=intermediate_file")
    end
  end

  describe '#render_caption_link' do
    let(:id) { SecureRandom.uuid }
    let(:solr_doc) { SolrDocument.new('id' => id, Ddr::Index::Fields::RESOURCE_MODEL => 'Ddr::Component') }

    it 'returns the correct link to a caption file' do
      expect(helper.render_caption_link(solr_doc)).to include("ddr/components/#{id}/captions")
    end
  end

  describe '#link_to_object' do
    let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
    let(:solr_doc) { SolrDocument.new('id' => valkyrie_id, Ddr::Index::Fields::RESOURCE_MODEL => 'Ddr::Item') }

    before { allow(SolrDocument).to receive(:find).with(valkyrie_id.id) { solr_doc } }

    context 'can access' do
      before { allow(helper).to receive(:can?).and_return(true) }

      it 'returns the correct link to the object' do
        expect(helper.link_to_object(valkyrie_id)).to include(ddr_item_path(valkyrie_id.id))
      end
    end

    context 'cannot access' do
      before { allow(helper).to receive(:can?).and_return(false) }

      it 'returns the id without a link' do
        expect(helper.link_to_object(valkyrie_id)).to eq(valkyrie_id.id)
      end
    end
  end
end
