require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe GroupsAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }

      after { logout }

      describe 'GET /groups' do
        before do
          allow(GroupRepository).to receive(:repository_groups).and_return([])
        end

        it_behaves_like 'a curator-only API', :get, '/api/groups'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/groups'
        end
      end
    end
  end
end
