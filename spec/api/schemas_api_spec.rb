module Ddr
  module API
    RSpec.describe SchemasAPI, :api, type: :request do
      Schema.names.each do |schema_name|
        it "returns the '#{schema_name}' schema" do
          get '/api/schemas/%s' % schema_name
          schema = JSON.parse(response.body)
          expect(schema).to eq Schema[schema_name]
        end
      end
    end
  end
end
