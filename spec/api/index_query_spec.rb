module Ddr
  module API
    RSpec.describe IndexQuery, :api, :index do
      let(:query) { described_class.new(params) }
      let(:params) { {} }

      describe '#one' do
        subject { query.one }

        describe 'with results' do
          before { 3.times { create_for_repository(:item) } }

          it { is_expected.to be_a Ddr::Item }
        end

        it { is_expected.to be_nil }
      end

      describe '#first' do
        subject { query.first }

        describe 'with results' do
          before { 3.times { create_for_repository(:item) } }

          it { is_expected.to be_a Ddr::Item }
        end

        it { is_expected.to be_nil }
      end

      describe '#none?' do
        subject { query.none? }

        describe 'with results' do
          before { create_for_repository(:item) }

          it { is_expected.to be false }
        end

        it { is_expected.to be true }
      end

      describe '#any?' do
        subject { query.any? }

        describe 'with results' do
          before { create_for_repository(:item) }

          it { is_expected.to be true }
        end

        it { is_expected.to be false }
      end

      describe '#empty?' do
        subject { query.empty? }

        describe 'with results' do
          before { create_for_repository(:item) }

          it { is_expected.to be false }
        end

        it { is_expected.to be true }
      end

      describe '#page'

      describe '#each'

      describe '#docs' do
        before { 3.times { create_for_repository(:item) } }

        it 'coverts each result to a SolrDocument' do
          query.docs.each do |doc|
            expect(doc).to be_a(::SolrDocument)
          end
        end
      end

      describe '#raw' do
        before { 3.times { create_for_repository(:item) } }

        it 'does not covert raw results' do
          query.raw.each do |doc|
            expect(doc).to be_a(::Hash)
          end
        end
      end

      describe '#csv' do
        before { 3.times { create_for_repository(:item) } }

        it 'returns a String' do
          expect(query.csv).to be_a(String)
        end
      end

      describe '#ids' do
        let(:items) { [create_for_repository(:item), create_for_repository(:item), create_for_repository(:item)] }

        it 'returns the ids' do
          ids = items.map(&:id).map(&:to_s)

          expect(query.ids.to_a).to match_array(ids)
        end
      end

      describe '#resources' do
        before { 3.times { create_for_repository(:item) } }

        it 'coverts each result to a Ddr::Resource' do
          query.resources.each do |doc|
            expect(doc).to be_a(Ddr::Item)
          end
        end
      end
    end
  end
end
