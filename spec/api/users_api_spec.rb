require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe UsersAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }

      before(:all) { ::User.delete_all }
      after { logout }

      describe 'GET /users' do
        it_behaves_like 'a curator-only API', :get, '/api/users'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/users'

          describe 'username filter' do
            it 'is successful' do
              user = FactoryBot.create(:user)
              get '/api/users?username=%s' % user.username
              data = JSON.parse(response.body)
              expect(data.first['username']).to eq user.username
            end
          end

          describe 'duid filter' do
            it 'is successful' do
              user = FactoryBot.create(:user, :duke)
              user.update!(duid: '1234')
              get '/api/users?duid=%s' % user.duid
              data = JSON.parse(response.body)
              expect(data.first['duid']).to eq user.duid
            end
          end

          describe 'netid filter' do
            it 'is successful' do
              user = FactoryBot.create(:user, :duke)
              get '/api/users?netid=%s' % user.netid
              data = JSON.parse(response.body)
              expect(data.first['netid']).to eq user.netid
            end
          end

          describe 'first_name filter' do
            it 'is successful' do
              user = FactoryBot.create(:user, :duke)
              user.update!(first_name: 'Foobar')
              get '/api/users?first_name=%s' % user.first_name
              data = JSON.parse(response.body)
              expect(data.first['first_name']).to eq user.first_name
            end
          end

          describe 'last_name filter' do
            it 'is successful' do
              user = FactoryBot.create(:user, :duke)
              user.update!(last_name: 'Foobar')
              get '/api/users?last_name=%s' % user.last_name
              data = JSON.parse(response.body)
              expect(data.first['last_name']).to eq user.last_name
            end
          end
        end
      end # GET /users

      describe 'POST /users' do
        let(:headers) { { 'CONTENT_TYPE' => 'application/json' } }
        let(:user) { FactoryBot.build(:user) }
        let(:data) { { username: user.username, email: user.email } }

        # FIXME
        # it_behaves_like 'a curator-only API', :post, '/api/users'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          describe 'with a new user' do
            it 'is created' do
              post('/api/users', params: JSON.dump(data), headers:)
              expect(response.status).to eq 201
            end

            it 'returns the user info' do
              post('/api/users', params: JSON.dump(data), headers:)
              info = JSON.parse(response.body)
              expect(info['username']).to eq user.username
            end
          end

          describe 'with a username that is taken' do
            let(:user) { FactoryBot.create(:user) }

            it 'is a conflict' do
              post('/api/users', params: JSON.dump(data), headers:)
              expect(response.status).to eq 409
            end
          end
        end
      end # POST /users

      describe 'GET /users/:id' do
        let(:duke_user) { FactoryBot.create(:user, :duke) }

        it_behaves_like 'a curator-only API', :get, '/api/users/1'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          describe 'with an existing user' do
            it 'is successful' do
              get '/api/users/%s' % duke_user.id
              expect(response.status).to eq 200
            end
          end

          describe 'with a non-existent user' do
            it_behaves_like 'a not found API call', :get, '/api/users/1000'
          end
        end
      end # GET /users/:id
    end
  end
end
