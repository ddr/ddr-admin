module Ddr::API
  RSpec.describe BatchObjectEntity, :api do
    it 'has the right exposures' do
      expect(described_class.root_exposures.map(&:key))
        .to include(*described_class.properties.keys.map(&:to_sym))
    end
  end
end
