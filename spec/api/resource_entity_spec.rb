module Ddr
  module API
    RSpec.describe ResourceEntity, :api do
      let(:resource) { item }

      let(:collection) { create_for_repository(:collection) }
      let(:item) { create_for_repository(:item) }
      let(:component) { create_for_repository(:component, :with_content_file) }

      describe 'root exposures' do
        subject { described_class.represent(resource, serializable: true) }

        its([:id]) { is_expected.to eq item.id.id }
        its([:model]) { is_expected.to eq 'Item' }
        its([:created_at]) { is_expected.to eq item.created_at }
        its([:updated_at]) { is_expected.to eq item.updated_at }
        its([:optimistic_lock_token]) { is_expected.to be_present }
      end

      describe 'metadata' do
        let(:resource) { item }

        subject { described_class.represent(resource, serializable: true).fetch(:metadata) }

        describe 'available fix (DDK-131)' do
          before { allow(resource).to receive(:available).and_return([]) }

          its([:available]) { is_expected.to be_nil }
        end
      end

      describe 'files' do
        let(:resource) { component }

        subject { described_class.represent(resource, serializable: true).fetch(:files) }

        its([:content]) { is_expected.to be_present }
      end

      describe 'roles' do
        before do
          resource.roles = [
            Ddr::Auth::Roles::Role.new(role_type: 'Curator', agent: 'bob@example.com', scope: 'resource')
          ]
        end

        subject { described_class.represent(resource, serializable: true).as_json }

        its(['roles']) do
          is_expected.to eq([{ 'role_type' => 'Curator', 'agent' => 'bob@example.com', 'scope' => 'resource' }])
        end
      end

      describe 'links'

      describe 'related'
    end
  end
end
