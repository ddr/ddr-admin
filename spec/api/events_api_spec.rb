require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe EventsAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }
      let(:role) { FactoryBot.build(:role, :public, :resource, :viewer) }

      after { logout }

      describe 'GET /events' do
        it_behaves_like 'a curator-only API', :get, '/api/events'

        describe 'as curator' do
          before do
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            login_as user
          end

          it_behaves_like 'a successful API call', :get, '/api/events.json'
          it_behaves_like 'a successful API call', :get, '/api/events.csv'

          describe 'for a resource' do
            let(:resource) { create_for_repository(:item, access_role: role) }

            it 'is successful' do
              get '/api/events.json?resource_id=%s' % resource.id
              expect(response.status).to eq(200)
            end

            it 'is available as PREMIS XML' do
              get '/api/events.xml?resource_id=%s' % resource.id
              expect(response.status).to eq(200)
            end
          end
        end
      end # GET /events

      describe 'POST /events' do
        let(:data) { { type:, resource_id: resource.id.to_s, outcome:, event_date_time: DateTime.now } }
        let(:outcome) { Ddr::Events::Event::SUCCESS }
        let(:resource) { create_for_repository(:item) }
        let(:headers) { { 'content-type' => 'application/json', 'accept' => 'application/json' } }

        describe 'as curator' do
          before do
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            login_as user
          end

          describe 'with valid type' do
            let(:type) { Ddr::Events::EventTypes::FIXITY_CHECK }

            describe 'and valid data' do
              it 'creates an event of the correct type' do
                post('/api/events', params: JSON.dump(data), headers:)
                expect(response.status).to eq 201
                expect(JSON.parse(response.body)['type']).to eq type
              end
            end

            describe 'but invalid data' do
              let(:outcome) { 'wonky' }

              it 'is a bad request' do
                post('/api/events', params: JSON.dump(data), headers:)
                expect(response.status).to eq 400
              end
            end
          end

          describe 'with invalid type' do
            let(:type) { 'foo' }

            it 'is a bad request' do
              post('/api/events', params: JSON.dump(data), headers:)
              expect(response.status).to eq 400
            end
          end
        end
      end # POST /events

      describe 'GET /events/:id' do
        let(:resource) { create_for_repository(:item, access_role: role) }
        let(:event) { Ddr::Events::UpdateEvent.create!(resource_id: resource.id.to_s) }

        it_behaves_like 'a curator-only API', :get, '/api/events/1'

        describe 'as curator' do
          before do
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            login_as user
          end

          it 'is successful' do
            get '/api/events/%s.json' % event.id
            expect(response.status).to eq(200)
          end

          it 'is available as PREMIS XML' do
            get '/api/events/%s.xml' % event.id
            expect(response.status).to eq(200)
          end
        end
      end
    end
  end
end
