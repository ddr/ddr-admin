require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe AdminSetsAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }

      describe 'GET /admin_sets' do
        it_behaves_like 'an API that requires authentication', :get, '/api/admin_sets'

        describe 'authenticated' do
          before { login_as user }
          after { logout }

          it_behaves_like 'a successful API call', :get, '/api/admin_sets'
        end
      end

      describe 'GET /admin_sets/:code' do
        it_behaves_like 'an API that requires authentication', :get, '/api/admin_sets/dc'

        describe 'authenticated' do
          before { login_as user }
          after { logout }

          it_behaves_like 'a successful API call', :get, '/api/admin_sets/dc'
        end
      end
    end
  end
end
