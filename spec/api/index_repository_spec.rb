module Ddr
  module API
    RSpec.describe IndexRepository, :api, :index do
      let(:resource) { create_for_repository(:item) }

      describe '.find_resources' do
        let!(:item) { create_for_repository(:item) }
        let!(:component) { create_for_repository(:component) }
        let!(:collection) { create_for_repository(:collection) }

        it 'filters by one model' do
          results = described_class.find_resources({ model: 'Item' }).to_enum
          ids = results.map(&:id)
          expect(ids).to eq [item.id.to_s]
        end

        it 'filters by multiple models' do
          results = described_class.find_resources({ model: %w[Item Collection] }).to_enum
          ids = results.map(&:id)
          expect(ids).to contain_exactly(item.id.to_s, collection.id.to_s)
        end

        it 'filters by fully qualified model names' do
          results = described_class.find_resources({ model: 'Ddr::Item' }).to_enum
          ids = results.map(&:id)
          expect(ids).to eq [item.id.to_s]
        end

        it 'filters by multiple fully qualified models' do
          results = described_class.find_resources({ model: ['Ddr::Item', 'Ddr::Collection'] }).to_enum
          ids = results.map(&:id)
          expect(ids).to contain_exactly(item.id.to_s, collection.id.to_s)
        end
      end

      describe '.find_by_identifiers' do
        let!(:item1) { create_for_repository(:item) }
        let!(:item2) { create_for_repository(:item, local_id: 'foobar123') }
        let!(:item3) { create_for_repository(:item, permanent_id: 'ark:/99999/fk456789') }

        it 'finds local ids' do
          results = described_class.find_by_identifiers(['foobar123']).to_enum
          ids = results.map(&:id)
          expect(ids).to contain_exactly(item2.id.to_s)
        end

        it 'finds permanent ids' do
          results = described_class.find_by_identifiers(['ark:/99999/fk456789']).to_enum
          ids = results.map(&:id)
          expect(ids).to contain_exactly(item3.id.to_s)
        end

        it 'finds resource ids' do
          results = described_class.find_by_identifiers([item1.id.to_s, item2.id.to_s, item3.id.to_s]).to_enum
          ids = results.map(&:id)
          expect(ids).to contain_exactly(item1.id.to_s, item2.id.to_s, item3.id.to_s)
        end
      end

      describe '.get_doc' do
        subject { described_class.get_doc(resource.id) }

        it { is_expected.to be_a ::SolrDocument }

        it 'raises an NotFoundError when the document is not found' do
          expect { described_class.get_doc(SecureRandom.uuid) }.to raise_error(NotFoundError)
        end
      end

      describe '.schema'

      describe '.delete_by_id' do
        let(:resource_id) { resource.id.to_s }

        describe 'when the resource exists' do
          it 'raises an ConflictError' do
            expect { described_class.delete_by_id(resource_id) }.to raise_error(ConflictError)
          end
        end

        describe 'when the resource does not exist' do
          before { DatabaseRepository.delete(resource:) }

          it 'deletes the document' do
            expect(described_class.get_doc(resource_id).id).to eq resource_id
            described_class.delete_by_id(resource_id)
            expect { described_class.get_doc(resource_id) }.to raise_error(NotFoundError)
          end
        end

        describe 'when the document is not found' do
          it 'is a no-op (this is the behavior from RSolr)' do
            expect { described_class.delete_by_id(SecureRandom.uuid) }.not_to raise_error
          end
        end
      end
    end
  end
end
