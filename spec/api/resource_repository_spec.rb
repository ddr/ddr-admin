module Ddr
  module API
    RSpec.describe ResourceRepository, :api do
      describe '.find'

      describe '.find_by_role' do
        let(:role1) { Ddr::Auth::Roles::Role.new(agent: 'public', role_type: 'Downloader', scope: 'resource') }
        let(:role2) { Ddr::Auth::Roles::Role.new(agent: 'curators', role_type: 'Curator', scope: 'policy') }

        before do
          @resource1 = create_for_repository(:item, access_role: [role1])
          @resource2 = create_for_repository(:item, access_role: [role2])
          @resource3 = create_for_repository(:item, access_role: [role1, role2])
        end

        it 'finds by agent' do
          expect(described_class.find_by_role(agent: 'public').map(&:id).map(&:to_s))
            .to contain_exactly(@resource1.id.to_s, @resource3.id.to_s)
        end

        it 'finds by role_type' do
          expect(described_class.find_by_role(role_type: 'Downloader').map(&:id).map(&:to_s))
            .to contain_exactly(@resource1.id.to_s, @resource3.id.to_s)
        end

        it 'finds by scope' do
          expect(described_class.find_by_role(scope: 'policy').map(&:id).map(&:to_s))
            .to contain_exactly(@resource2.id.to_s, @resource3.id.to_s)
        end

        it 'finds by complete role spec' do
          expect(described_class.find_by_role(agent: 'curators', role_type: 'Curator',
                                              scope: 'policy').map(&:id).map(&:to_s))
            .to contain_exactly(@resource2.id.to_s, @resource3.id.to_s)
        end

        it 'returns an empty list if there are no matches' do
          expect(described_class.find_by_role(agent: 'public', role_type: 'Curator').size).to eq 0
        end
      end

      describe '.find_all_of_model'
    end
  end
end
