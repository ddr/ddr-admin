require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe BatchesAPI, :api, :batch, type: :request do
      let(:user) { FactoryBot.create(:user) }
      let(:batch) { FactoryBot.create(:collection_creating_ingest_batch) }

      before(:all) do
        Ddr::Batch::Batch.delete_all
        Ddr::Batch::BatchObject.delete_all
      end

      after do
        logout
        Ddr::Batch::Batch.delete_all
        Ddr::Batch::BatchObject.delete_all
      end

      describe 'GET /batches' do
        it_behaves_like 'a curator-only API', :get, '/api/batches'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/batches'
          it_behaves_like 'a successful API call', :get, '/api/batches?status=READY'
          it_behaves_like 'a successful API call', :get, '/api/batches?outcome=SUCCESS'
          it_behaves_like 'a successful API call', :get, '/api/batches?user_id=1'
        end
      end

      describe 'GET /batches/:id' do
        it_behaves_like 'a curator-only API', :get, '/api/batches/1'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a not found API call', :get, "/api/batches/#{Random.rand(10_000)}"

          it 'contains the batch information' do
            get '/api/batches/%s' % batch.id
            expect(response.content_type).to eq 'application/json'
            expect(response.status).to eq 200
            result = JSON.parse(response.body)
            expect(result['id']).to eq batch.id
          end
        end
      end

      describe 'GET /batches/:id/log' do
        it_behaves_like 'a curator-only API', :get, '/api/batches/1/log'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it 'redirects to /batches/:id/messages' do
            get '/api/batches/%s/log' % batch.id
            expect(response).to redirect_to('/api/batches/%s/messages.csv' % batch.id)
          end
        end
      end

      describe 'GET /batches/:id/messages' do
        it_behaves_like 'a curator-only API', :get, '/api/batches/1/messages'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a not found API call', :get, "/api/batches/#{Random.rand(10_000)}/messages"

          describe 'when the batch exists' do
            before do
              batch.batch_objects.each_with_index do |bo, idx|
                Ddr::Batch::BatchObjectMessage.create(batch_object: bo, message: "Hello World, #{idx}!")
              end
            end

            it 'is successful' do
              get '/api/batches/%s/messages' % batch.id
              expect(response.status).to eq 200
              messages = JSON.parse(response.body)
              expect(messages.length).to eq batch.batch_objects.count
              expect(messages.first['message']).to match(/^Hello World/)
            end

            %i[processed handled verified validated].each do |field|
              describe "with batch object param #{field.inspect}" do
                it 'is successful' do
                  get format('/api/batches/%s/messages?%s=true', batch.id, field)
                  expect(response.status).to eq 200
                  messages = JSON.parse(response.body)
                  expect(messages).to be_empty
                end
              end
            end

            it 'is available as CSV' do
              get '/api/batches/%s/messages.csv' % batch.id
              expect(response.status).to eq 200
            end
          end
        end
      end

      describe 'GET /batches/:id/batch_objects' do
        it_behaves_like 'a curator-only API', :get, '/api/batches/1/batch_objects'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a not found API call', :get, "/api/batches/#{Random.rand(10_000)}/batch_objects"

          it 'is successful' do
            get '/api/batches/%s/batch_objects' % batch.id
            expect(response.status).to eq 200
            batch_objects = JSON.parse(response.body)
            expect(batch_objects.first['batch_id']).to eq batch.id
          end

          describe 'with a boolean field params' do
            %i[processed handled verified validated].each do |field|
              let(:batch_object) { FactoryBot.create(:basic_update_batch_object, :has_batch, field => false) }

              it 'is successful' do
                get format('/api/batches/%s/batch_objects?%s=true', batch_object.batch_id, field)
                expect(response.status).to eq 200
                batch_objects = JSON.parse(response.body)
                expect(batch_objects).to be_empty
              end
            end
          end
        end
      end
    end
  end
end
