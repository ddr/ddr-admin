module Ddr::API
  RSpec.describe Schema, :api do
    Schema.names.each do |schema_name|
      describe "#{schema_name} schema" do
        subject { Schema[schema_name] }

        it 'is a valid schema' do
          expect(JSONSchemer.valid_schema?(subject)).to be true
        end
      end
    end
  end
end
