module Ddr::API
  RSpec.describe SchemaType, :api do
    describe 'type' do
      [{ 'type' => 'array', 'items' => { 'type' => 'string' } },
       { 'type' => %w[array string null], 'items' => { 'type' => 'string' } },
       { 'type' => %w[string null] },
       { 'type' => 'string' }].each do |schema|
        describe schema.inspect do
          subject { described_class.call(schema).first }
          it { is_expected.to eq 'string' }
        end
      end
    end

    describe 'is_array' do
      subject { described_class.call(schema).last }

      [%w[array string null], 'array', %w[object array], %w[array string]].each do |type|
        describe type.inspect do
          let(:schema) { { 'type' => type } }

          it { is_expected.to be true }
        end
      end
    end
  end
end
