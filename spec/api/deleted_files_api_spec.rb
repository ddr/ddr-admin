require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe DeletedFilesAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }

      after { logout }

      describe 'GET /deleted_files' do
        it_behaves_like 'a curator-only API', :get, '/api/deleted_files'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/deleted_files.json'
          it_behaves_like 'a successful API call', :get, '/api/deleted_files.csv'

          describe 'for a resource' do
            let(:resource) { create_for_repository(:item) }

            it 'is successful' do
              get '/api/deleted_files.json?resource_id=%s' % resource.id
              expect(response.status).to eq(200)
            end
          end
        end
      end
    end
  end
end
