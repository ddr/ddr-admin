require 'csv'
require 'rack/multipart'
require 'rack/multipart/uploaded_file'
require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe ResourcesAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }
      let(:curators_policy) { FactoryBot.build(:role, :policy, :curator, agent: Ddr::Admin.curators_group) }
      let(:curators_resource) { FactoryBot.build(:role, :resource, :curator, agent: Ddr::Admin.curators_group) }
      let(:user_policy_editor) { FactoryBot.build(:role, :policy, :editor, agent: user) }
      let(:user_resource_editor) { FactoryBot.build(:role, :resource, :editor, agent: user) }
      let(:public_policy_viewer) { FactoryBot.build(:role, :policy, :public, :viewer) }
      let(:public_policy_metadata_viewer) { FactoryBot.build(:role, :policy, :public, :metadata_viewer) }
      let(:public_resource_viewer) { FactoryBot.build(:role, :public, :resource, :viewer) }
      let(:public_resource_downloader) { FactoryBot.build(:role, :public, :resource, :downloader) }

      after { logout }

      describe 'GET /resources' do
        before do
          coll = create_for_repository(:collection, access_role: public_policy_viewer)
          item = create_for_repository(:item, parent_id: coll.id, admin_policy_id: coll.id)
          create_for_repository(:component, parent_id: item.id, admin_policy_id: coll.id)
        end

        it_behaves_like 'an API that requires authentication', :get, '/api/resources'

        describe 'authenticated' do
          before { login_as user }

          it_behaves_like 'a successful API call', :get, '/api/resources'
          it_behaves_like 'a successful API call', :get, '/api/resources.json'
          it_behaves_like 'a successful API call', :get, '/api/resources.csv'

          describe 'Default JSON response' do
            it 'has expected data for each resource' do
              get '/api/resources.json'
              data = JSON.parse(response.body)

              data.each do |resource|
                expect(resource['id']).to be_present
                expect(resource['model']).to be_present
                expect(resource['optimistic_lock_token']).to be_present
                expect(resource['created_at']).to be_present
                expect(resource['updated_at']).to be_present
                expect(resource['metadata']).to be_present
                expect(resource['metadata']).to be_a Hash
                expect(resource['files']).to be_present
                expect(resource['files']).to be_a Hash
                expect(resource['related']).to be_present
                expect(resource['related']).to be_a Hash
              end
            end
          end

          describe 'CSV requests' do
            it "responds to 'accept: text/csv' header" do
              get '/api/resources', headers: { 'accept' => 'text/csv' }
              expect(response.status).to eq(200)
              expect(response.content_type).to eq 'text/csv'
            end

            it "handles 'remove_empty_columns=true' param" do
              get '/api/resources.csv?remove_empty_columns=true'
              expect(response.status).to eq(200)
              expect(response.content_type).to eq 'text/csv'
              csv = CSV.parse(response.body, headers: true)
              expect(csv.headers).to include('id')
              expect(csv.headers).not_to include('description')
            end

            it 'has the requested csv fields' do
              get '/api/resources.csv?csv_fields=parent_title,collection_id,original_filename'
              expect(response.status).to eq(200)
              csv = CSV.parse(response.body, headers: true)
              expect(csv.headers).to include('parent_title', 'collection_id', 'original_filename')
            end

            it 'has the requested file fields' do
              get '/api/resources.csv?file_fields=thumbnail,content'
              expect(response.status).to eq(200)
              csv = CSV.parse(response.body, headers: true)
              expect(csv.headers).to include('thumbnail', 'content')
            end
          end

          describe 'model param' do
            it 'is a bad request with an invalid model' do
              get '/api/resources?model=Turtle'
              expect(response.status).to eq(400)
            end

            it 'accepts a single model' do
              get '/api/resources?model=Collection'
              expect(response.status).to eq(200)
            end

            it 'takes a comma-separated list' do
              get '/api/resources?model=Collection,Item'
              expect(response.status).to eq(200)
            end
          end

          describe 'sha1 param' do
            let(:sha1) { '75e2e0cec6e807f6ae63610d46448f777591dd6b' }

            before { create_for_repository(:component, :with_content_file, access_role: public_resource_downloader) }

            it 'takes a single value' do
              get '/api/resources?sha1=%s' % sha1
              expect(response.status).to eq(200)
              resources = JSON.parse(response.body)
              expect(resources).to be_present
              expect(resources.first.dig('files', 'content', 'sha1')).to eq sha1
            end

            it 'returns an empty list for no matches' do
              get '/api/resources?sha1=38b87e605f19b5ce866d6eb49d3c850dc220e8dd'
              expect(response.status).to eq(200)
              resources = JSON.parse(response.body)
              expect(resources).to be_empty
            end

            it 'takes a csv list' do
              get '/api/resources?sha1=38b87e605f19b5ce866d6eb49d3c850dc220e8dd,%s' % sha1
              expect(response.status).to eq(200)
              resources = JSON.parse(response.body)
              expect(resources).to be_present
              expect(resources.first.dig('files', 'content', 'sha1')).to eq sha1
            end
          end

          describe 'original_filename param' do
            before { create_for_repository(:component, :with_content_file, access_role: public_resource_downloader) }

            let(:original_filename) { 'imageA.tif' }

            it 'is successful' do
              get '/api/resources?original_filename=%s' % original_filename
              expect(response.status).to eq(200)
              resources = JSON.parse(response.body)
              expect(resources).to be_present
              expect(resources.first.dig('files', 'content', 'original_filename')).to eq original_filename
            end

            it 'returns an empty list for no matches' do
              get '/api/resources?original_filename=foobar.xyz'
              expect(response.status).to eq(200)
              resources = JSON.parse(response.body)
              expect(resources).to be_empty
            end
          end
        end
      end # GET /resources

      describe 'POST /resources' do
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:data) { JSON.dump({ model: 'Collection', metadata: { title: ['API-created Resource'] } }) }

        describe 'without authentication' do
          it 'is unauthorized' do
            post('/api/resources', params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          it 'is forbidden' do
            post('/api/resources', params: data, headers:)
            expect(response.status).to eq 403
          end

          describe 'as curator' do
            before do
              allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            end

            describe 'with invalid params' do
              let(:data) do
                JSON.dump({ model: 'Item', metadata: { display_format: 'hazy' } })
              end

              it 'is a bad request' do
                post('/api/resources', params: data, headers:)
                expect(response.status).to eq(400)
              end
            end

            describe 'with valid data' do
              it 'creates a new resource' do
                post('/api/resources', params: data, headers:)
                expect(response.status).to eq(201)
                resource = JSON.parse(response.body)
                expect(resource['id']).to be_present
                expect(resource['model']).to eq('Collection')
                expect(resource.dig('metadata', 'title')).to eq ['API-created Resource']
              end
            end

            describe 'with no params' do
              it 'is a bad request' do
                post('/api/resources', params: '{}', headers:)
                expect(response.status).to eq(400)
              end
            end
          end
        end
      end # POST /resources

      describe 'POST /resources/find_by_identifiers' do
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:data) { JSON.dump({ identifiers: }) }
        let(:identifiers) { ['foobar123'] }

        describe 'without authentication' do
          it 'is unauthorized' do
            post('/api/resources/find_by_identifiers', params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          let(:resource) { create_for_repository(:item, access_role: public_resource_viewer) }
          let(:identifiers) { [resource.id.to_s] }

          before { login_as user }

          it 'finds resources' do
            post('/api/resources/find_by_identifiers', params: data, headers:)
            expect(response.status).to eq 200
            expect(JSON.parse(response.body).first['id']).to eq resource.id.to_s
          end
        end
      end # POST /resources/find_by_identifiers

      describe 'GET /resources/by_role' do
        it_behaves_like 'a curator-only API', :get, '/api/resources/by_role?agent=public'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          describe 'with no params' do
            it 'is a bad request' do
              get '/api/resources/by_role'
              expect(response.status).to eq(400)
            end
          end

          describe 'finding matching resources' do
            let(:role1) { Ddr::Auth::Roles::Role.new(role_type: 'Curator', agent: 'curator@duke.edu', scope: 'policy') }
            let(:role2) { Ddr::Auth::Roles::Role.new(role_type: 'Viewer', agent: 'public', scope: 'resource') }
            let!(:res1) { create_for_repository(:item, access_role: role1) }
            let!(:res2) { create_for_repository(:item, access_role: role2) }

            it 'finds by agent' do
              get '/api/resources/by_role?agent=public'
              expect(JSON.parse(response.body).first['id']).to eq res2.id.to_s
            end

            it 'finds by scope' do
              get '/api/resources/by_role?scope=resource'
              expect(JSON.parse(response.body).first['id']).to eq res2.id.to_s
            end

            it 'finds by role type' do
              get '/api/resources/by_role?role_type=Curator'
              expect(JSON.parse(response.body).first['id']).to eq res1.id.to_s
            end
          end
        end
      end # GET /resources/by_role

      describe 'POST /resources/collections' do
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:data) { JSON.dump({ metadata: { title: ['API-created Collection'], admin_set: 'dc' } }) }

        describe 'without authentication' do
          it 'is unauthorized' do
            post('/api/resources/collections', params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          it 'is forbidden' do
            post('/api/resources/collections', params: data, headers:)
            expect(response.status).to eq 403
          end

          describe 'as curator' do
            before do
              allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            end

            describe 'with invalid params' do
              let(:data) { JSON.dump({ metadata: { admin_set: 'foobar' } }) }

              it 'is a bad request' do
                post('/api/resources/collections', params: data, headers:)
                expect(response.status).to eq(400)
              end
            end

            describe 'with valid data' do
              it 'creates a new collection' do
                post('/api/resources/collections', params: data, headers:)
                expect(response.status).to eq(201)
                collection = JSON.parse(response.body)
                expect(collection['id']).to be_present
                expect(collection['model']).to eq('Collection')
                expect(collection.dig('metadata', 'title')).to eq ['API-created Collection']
                expect(collection.dig('metadata', 'admin_set')).to eq 'dc'
              end
            end

            describe 'with no params' do
              it 'is a bad request' do
                post('/api/resources/collections', params: '{}', headers:)
                expect(response.status).to eq(400)
              end
            end
          end
        end
      end # POST /resources/collections

      # POST /resources/export_files
      describe 'POST /resources/export_files' do
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:data) { JSON.dump({ identifiers:, basename:, files: }) }
        let(:role) { public_resource_downloader }
        let(:resources) do
          [].tap do |r|
            3.times do
              r << create_for_repository(:component, :with_content_file, :with_thumbnail_file, access_role: role)
            end
          end
        end
        let(:identifiers) { resources.map(&:id).map(&:to_s) }
        let(:basename) { SecureRandom.hex(4) }
        let(:files) { %w[content thumbnail] }

        before do
          allow(Ddr::ExportFilesJob).to receive(:perform_later)
        end

        describe 'without authentication' do
          it 'is unauthorized' do
            post('/api/resources/export_files', params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'user cannot download' do
            let(:role) { public_resource_viewer }

            it 'is a bad request' do
              post('/api/resources/export_files', params: data, headers:)
              expect(response.status).to eq 400
            end
          end

          describe 'user can download' do
            it 'is successful' do
              post('/api/resources/export_files', params: data, headers:)
              expect(response.status).to eq 202
              expect(Ddr::ExportFilesJob).to have_received(:perform_later)
            end
          end
        end
      end # POST /resources/export_files

      #
      # Resource requests
      #
      shared_examples 'a resources API that requires read permission' do |method, path|
        describe 'unauthenticated' do
          it 'is unauthorized' do
            send(method, path % resource.id)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated by login' do
          before { login_as user }

          describe 'when the resource does not exist' do
            it_behaves_like 'a not found API call', method, path % SecureRandom.uuid
          end

          describe 'when the user cannot view the resource' do
            let(:role) { curators_policy }

            it 'is forbidden' do
              send(method, path % resource.id)
              expect(response.status).to eq 403
            end
          end

          describe 'when the user can view the resource' do
            let(:role) { [public_policy_viewer, public_resource_viewer] }

            it 'is successful' do
              send(method, path % resource.id)
              expect(response.status).to eq 200
            end
          end
        end

        describe 'authenticated by token' do
          let(:headers) { { 'authorization' => 'Bearer zyzzyx' } }

          before do
            allow(OAuthRequest).to receive(:call).with(:introspection_endpoint, token: 'zyzzyx')
                                                 .and_return({ 'active' => true, 'user_id' => user.username })
          end

          describe 'when the resource does not exist' do
            it 'is not found' do
              send(method, path % SecureRandom.uuid, headers:)
              expect(response.status).to eq 404
            end
          end

          describe 'when the user cannot view the resource' do
            let(:role) { curators_policy }

            it 'is forbidden' do
              send(method, path % resource.id, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'when the user can view the resource' do
            let(:role) { [public_policy_viewer, public_resource_viewer] }

            it 'is successful' do
              send(method, path % resource.id, headers:)
              expect(response.status).to eq 200
            end
          end
        end
      end # a resources API that requires read permission

      describe 'GET /resources/:id' do
        let(:resource) { create_for_repository(:item, access_role: role) }
        let(:role) { public_resource_viewer }

        it_behaves_like 'a resources API that requires read permission', :get, '/api/resources/%s'

        describe 'authenticated' do
          before { login_as user }

          describe 'attributes' do
            let(:data) do
              get '/api/resources/%s' % resource.id
              JSON.parse(response.body)
            end

            subject { data }

            its(['id']) { is_expected.to eq resource.id.to_s }
            its(['model']) { is_expected.to eq 'Item' }
            its(['optimistic_lock_token']) { is_expected.to be_present }

            describe 'metadata' do
              subject { data['metadata'] }

              its(['title']) { is_expected.to eq ['Test Item'] }
            end
          end
        end
      end # GET /resources/:id

      describe 'PATCH /resources/:id' do
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:resource) { create_for_repository(:item, title: ['Original Title'], access_role: role) }
        let(:data) do
          JSON.dump({ optimistic_lock_token: resource.optimistic_lock_token.first.to_s,
                      metadata: { title: ['Updated Title'] },
                      roles: [{ agent: 'public', role_type: 'Downloader', scope: 'resource' }] })
        end

        describe 'unauthenticated' do
          let(:role) { user_resource_editor }

          it 'is unauthorized' do
            patch('/api/resources/%s' % resource.id, params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'user cannot update the resource' do
            let(:role) { public_resource_viewer }

            it 'is forbidden' do
              patch('/api/resources/%s' % resource.id, params: data, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'user can update the resource' do
            let(:role) { user_resource_editor }

            describe 'when the data is empty' do
              describe 'when the resource does not exist' do
                it 'is a bad request' do # b/c resource existence is checked after parameter validation
                  patch("/api/resources/#{SecureRandom.uuid}", params: '{}', headers:)
                  expect(response.status).to eq 400
                end
              end

              describe 'when the resource exists' do
                let(:resource) { create_for_repository(:item, access_role: user_policy_editor) }

                it 'is a bad request' do
                  patch("/api/resources/#{resource.id}", params: '{}', headers:)
                  expect(response.status).to eq 400
                end
              end
            end

            describe 'when the data is valid' do
              describe 'when the resource does not exist' do
                it 'is not found' do
                  patch("/api/resources/#{SecureRandom.uuid}", params: data, headers:)
                  expect(response.status).to eq 404
                end
              end

              describe 'when the resource exists' do
                it 'is successful' do
                  patch("/api/resources/#{resource.id}", params: data, headers:)
                  expect(response.status).to eq 200
                end

                it 'updates the resource metadata' do
                  expect do
                    patch("/api/resources/#{resource.id}", params: data, headers:)
                  end.to change {
                    Ddr.query_service.find_by(id: resource.id).title
                  }.from(['Original Title']).to(['Updated Title'])
                end

                it 'updates the resource roles' do
                  headers = { 'content-type' => 'application/json' }
                  expect do
                    patch "/api/resources/#{resource.id}", params: data, headers:
                  end.to(change do
                           Ddr.query_service.find_by(id: resource.id).roles
                         end)
                end
              end
            end
          end
        end
      end # PATCH /resources/:id

      describe 'GET /resources/:id/attachments' do
        let(:resource) { create_for_repository(:collection, access_role: role) }
        let(:role) { public_policy_viewer }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/attachments'
      end # GET /resources/:id/attachments

      describe 'GET /resources/:id/batches' do
        let(:resource) { create_for_repository(:collection, access_role: role) }
        let(:role) { public_policy_viewer }

        it_behaves_like 'a resources API that requires read permission', :get, '/api/resources/%s/batches'

        describe 'authenticated' do
          before { login_as user }

          it 'returns a list of batch records' do
            batch_object = FactoryBot.create(:update_batch_object, :has_batch,
                                             model: resource.class.name, resource_id: resource.id.to_s)
            get '/api/resources/%s/batches' % resource.id
            expect(response.status).to eq 200
            batches = JSON.parse(response.body)
            expect(batches).to be_a(Array)
            expect(batches.detect { |b| b['id'] = batch_object.batch_id }).to be_present
          end
        end
      end # GET /resources/:id/batches

      describe 'GET /resources/:id/children' do
        let(:resource) { create_for_repository(:item, access_role: role) }
        let(:role) { public_policy_viewer }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/children'

        describe 'authenticated' do
          before { login_as user }

          it 'returns the children' # TODO
        end
      end # GET /resources/:id/children

      describe 'POST /resources/:id/children' do
        let(:parent) { create_for_repository(:collection, access_role: role) }
        let(:role) { user_policy_editor }
        let(:headers) { { 'content-type' => 'application/json', 'accept' => 'application/json' } }
        let(:data) { JSON.dump({ metadata: { title: ['API-created Item'] } }) }

        describe 'without authentication' do
          it 'is unauthorized' do
            post("/api/resources/#{parent.id}/children", params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'with invalid params' do
            let(:data) { JSON.dump({ metadata: { display_format: 'skibidi' } }) }

            it 'is a bad request' do
              post("/api/resources/#{parent.id}/children", params: data, headers:)
              expect(response.status).to eq(400)
            end
          end

          describe 'with valid data' do
            it 'creates a new child resource' do
              post("/api/resources/#{parent.id}/children", params: data, headers:)
              expect(response.status).to eq(201)
              child = JSON.parse(response.body)
              expect(child['id']).to be_present
              expect(child['model']).to eq('Item')
              expect(child.dig('metadata', 'title')).to eq ['API-created Item']
              expect(child.dig('related', 'parent_id')).to eq parent.id.to_s
            end
          end
        end
      end # POST /resources/:id/children

      describe 'GET /resources/:id/download' do
        let(:resource) { create_for_repository(:component, :with_content_file, access_role: role) }
        let(:role) { public_resource_downloader }

        describe 'unauthenticated' do
          it 'is unauthorized' do
            get '/api/resources/%s/download' % resource.id
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'when the user cannot download' do
            let(:role) { public_resource_viewer }

            it 'is forbidden' do
              get '/api/resources/%s/download' % resource.id
              expect(response.status).to eq 403
            end
          end

          describe 'when the resource does not support download' do
            let(:resource) { create_for_repository(:item, access_role: role) }

            it 'is a bad request' do
              get '/api/resources/%s/download' % resource.id
              expect(response.status).to eq 400
            end
          end

          describe 'when the resource supports content' do
            describe 'but has no content' do
              let(:resource) { create_for_repository(:component, access_role: role) }

              it 'is not found' do
                get '/api/resources/%s/download' % resource.id
                expect(response.status).to eq 404
              end
            end

            describe 'and has content' do
              it 'is successful' do
                get '/api/resources/%s/download' % resource.id
                expect(response.status).to eq 200
              end

              it 'streams the content' do
                get '/api/resources/%s/download' % resource.id
                sha1 = ::Digest::SHA1.new
                sha1 << response.body
                expect(sha1.hexdigest).to eq resource.content.digest.first.value
              end
            end
          end
        end
      end # GET /resources/:id/download

      describe 'GET /resources/:id/files' do
        let(:resource) { create_for_repository(:component, :with_content_file, access_role: role) }
        let(:role) { public_resource_viewer }

        it_behaves_like 'a resources API that requires read permission', :get, '/api/resources/%s/files'

        describe 'authenticated' do
          before { login_as user }

          it 'returns a representation of the files' do
            get '/api/resources/%s/files' % resource.id
            expect(response.status).to eq 200
            data = JSON.parse(response.body)
            expect(data).to have_key('content')
          end
        end
      end # GET /resources/:id/files

      describe 'GET /resources/:id/files/:file_field' do
        let(:resource) do
          create_for_repository(:component, :with_content_file, :with_thumbnail_file, access_role: role)
        end
        let(:role) { public_resource_viewer }

        it_behaves_like 'a resources API that requires read permission', :get, '/api/resources/%s/files/thumbnail'

        describe 'authenticated' do
          before { login_as user }

          describe 'when the file_field is "content"' do
            it 'requires the downloader role' do
              get '/api/resources/%s/files/content' % resource.id
              expect(response.status).to eq 403
            end

            describe 'with the downloader role' do
              let(:role) { public_resource_downloader }

              it 'is successful' do
                get '/api/resources/%s/files/content' % resource.id
                expect(response.status).to eq 200
              end
            end
          end

          describe 'when the file field is not valid' do
            it 'is a bad request' do
              get '/api/resources/%s/files/foo' % resource.id
              expect(response.status).to eq 400
            end
          end

          describe 'when the resource type does not support the file field' do
            let(:resource) { create_for_repository(:item, access_role: role) }

            it 'is a bad request' do
              get '/api/resources/%s/files/caption' % resource.id
              expect(response.status).to eq 400
            end
          end

          describe 'when the file is not present on the resource' do
            it 'is not found' do
              get '/api/resources/%s/files/caption' % resource.id
              expect(response.status).to eq 404
            end
          end

          it 'downloads the file' do
            get '/api/resources/%s/files/thumbnail' % resource.id
            sha1 = ::Digest::SHA1.new
            sha1 << response.body
            expect(sha1.hexdigest).to eq resource.thumbnail.digest.first.value
          end
        end
      end # GET /resources/:id/files/:file_field

      describe 'POST /resources/:id/files/:file_field' do
        let(:resource) { create_for_repository(:component, access_role: role) }
        let(:role) { user_resource_editor }
        let(:file_path) { Rails.root.join('spec/fixtures/files/fits.xml') }
        let(:sha1) { '07e173b6b64a844366fb62238f37547847ee4277' }
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:params) { JSON.dump({ file_path:, sha1: }) }

        before do
          allow(Ddr::API).to receive(:safe_upload_paths).and_return([::File.dirname(file_path)])
        end

        describe 'unauthenticated' do
          it 'is unauthorized' do
            post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'user cannot update the resource' do
            let(:role) { public_resource_viewer }

            it 'is forbidden' do
              post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'user can update the resource' do
            describe 'when the file field is not valid' do
              it 'is a bad request' do
                post('/api/resources/%s/files/foo' % resource.id, params:, headers:)
                expect(response.status).to eq 400
              end
            end

            describe 'when the resource type does not support the file field' do
              let(:resource) { create_for_repository(:item, access_role: user_resource_editor) }

              it 'is a bad request' do
                post('/api/resources/%s/files/content' % resource.id, params:, headers:)
                expect(response.status).to eq 400
              end
            end

            describe 'when the file field is valid' do
              let(:updated_resource) { Ddr.query_service.find_by(id: resource.id) }

              it 'sets the SHA1 digest' do
                post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                expect(updated_resource.fits_file.sha1).to eq sha1
              end

              it 'sets the original filename' do
                post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                expect(updated_resource.fits_file.original_filename).to eq 'fits.xml'
              end

              it 'sets the media type' do
                post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                expect(updated_resource.fits_file.media_type).to eq 'application/xml'
              end

              describe 'and there is a SHA1 digest validation failure' do
                # changed last digit
                let(:params) { JSON.dump({ file_path:, sha1: '07e173b6b64a844366fb62238f37547847ee4276' }) }

                it 'is a bad request' do
                  post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                  expect(response.status).to eq 400
                end
              end

              describe 'when the file field is not set on the resource' do
                it 'creates the file' do
                  post('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                  expect(response.status).to eq 201
                  data = JSON.parse(response.body)
                  expect(data['sha1']).to eq sha1
                end
              end

              describe 'when the file field is set on the resource' do
                let(:resource) do
                  create_for_repository(:component, :with_content_file, access_role: user_resource_editor)
                end

                it 'updates the file' do
                  post('/api/resources/%s/files/content' % resource.id, params:, headers:)
                  expect(response.status).to eq 200
                  data = JSON.parse(response.body)
                  expect(data['sha1']).to eq sha1
                end
              end
            end
          end
        end
      end # POST /resources/:id/files/:file_field

      describe 'PUT /resources/:id/files/:file_field' do
        let(:resource) { create_for_repository(:component, access_role: role) }
        let(:role) { user_resource_editor }
        let(:file) { fixture_file_upload('fits.xml', 'text/xml') }
        let(:sha1) { '07e173b6b64a844366fb62238f37547847ee4277' }
        let(:size) { file.size }
        let(:headers) { { 'content-type' => 'multipart/form-data' } }
        let(:params) { { file:, sha1: } }

        describe 'unauthenticated' do
          it 'is unauthorized' do
            put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'user cannot update the resource' do
            let(:role) { public_resource_viewer }

            it 'is forbidden' do
              put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'user can update the resource' do
            describe 'when the file field is not valid' do
              it 'is a bad request' do
                put('/api/resources/%s/files/foo' % resource.id, params:, headers:)
                expect(response.status).to eq 400
              end
            end

            describe 'when the resource type does not support the file field' do
              let(:resource) { create_for_repository(:item, access_role: user_resource_editor) }

              it 'is a bad request' do
                put('/api/resources/%s/files/content' % resource.id, params:, headers:)
                expect(response.status).to eq 400
              end
            end

            describe 'when the file field is valid' do
              let(:updated_resource) { Ddr.query_service.find_by(id: resource.id) }

              it 'sets the SHA1 digest' do
                put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                expect(updated_resource.fits_file.sha1).to eq sha1
              end

              it 'sets the original filename' do
                put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                expect(updated_resource.fits_file.original_filename).to eq 'fits.xml'
              end

              it 'sets the media type' do
                put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                expect(updated_resource.fits_file.media_type).to eq 'text/xml'
              end

              describe 'and there is a SHA1 digest validation failure' do
                # changed last digit
                let(:params) { { file:, sha1: '07e173b6b64a844366fb62238f37547847ee4276' } }

                it 'is a bad request' do
                  put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                  expect(response.status).to eq 400
                end
              end

              describe 'when the file field is not set on the resource' do
                it 'creates the file' do
                  put('/api/resources/%s/files/fits_file' % resource.id, params:, headers:)
                  expect(response.status).to eq 201
                  data = JSON.parse(response.body)
                  expect(data['sha1']).to eq sha1
                end
              end

              describe 'when the file field is set on the resource' do
                let(:resource) do
                  create_for_repository(:component, :with_content_file, access_role: user_resource_editor)
                end

                it 'updates the file' do
                  put('/api/resources/%s/files/content' % resource.id, params:, headers:)
                  expect(response.status).to eq 200
                  data = JSON.parse(response.body)
                  expect(data['sha1']).to eq sha1
                end
              end
            end
          end
        end
      end # PUT /resources/:id/files/:file_field

      describe 'GET /resources/:id/fixity_check' do
        let(:resource) { create_for_repository(:component, :with_content_file, access_role: role) }
        let(:role) { public_resource_viewer }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/fixity_check'

        describe 'authenticated' do
          before { login_as user }

          it 'performs a fixity check on the resource' do
            get '/api/resources/%s/fixity_check' % resource.id
            expect(response.status).to eq 200
            result = JSON.parse(response.body)
            expect(result['success']).to be true
          end

          it 'processes a large resource later' do
            allow_any_instance_of(Ddr::Component).to receive(:content_size).and_return(10_000_000_000)
            get '/api/resources/%s/fixity_check' % resource.id
            expect(response.status).to eq 202
            result = JSON.parse(response.body)
            expect(result['arguments']).to eq [resource.id.to_s]
          end
        end
      end # GET /resources/:id/fixity_check

      describe 'POST /resources/:id/fixity_check' do
        let(:headers) { { 'content-type' => 'application/json' } }
        let(:resource) { create_for_repository(:component, :with_content_file, access_role: role) }
        let(:fixity_check_result) { { outcome: 'success', checked_at: DateTime.now, details: 'Woohoo!' } }
        let(:data) { JSON.dump(fixity_check_result) }
        let(:role) { user_resource_editor }

        describe 'without authentication' do
          it 'is unauthorized' do
            post('/api/resources/%s/fixity_check' % resource.id, params: data, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated, not curator' do
          before { login_as user }

          it 'is forbidden' do
            post('/api/resources/%s/fixity_check' % resource.id, params: data, headers:)
            expect(response.status).to eq 403
          end
        end

        describe 'as curator' do
          before do
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            login_as user
          end

          it 'is successful' do
            post('/api/resources/%s/fixity_check' % resource.id, params: data, headers:)
            expect(response.status).to eq 201
          end

          it 'creates a fixity check result' do
            expect do
              post('/api/resources/%s/fixity_check' % resource.id, params: data, headers:)
            end.to change { Ddr::Events::FixityCheckEvent.count }.by(1)
          end
        end
      end # POST /resources/:id/fixity_check

      describe 'GET /resources/:id/iiif' do
        let(:resource) do
          create_for_repository(:collection,
                                access_role: role,
                                permanent_id: 'ark:/99999/fk4cz3dh0',
                                permanent_url: 'https://idn.duke.edu/ark:/99999/fk4cz3dh0',
                                workflow_state: 'published')
        end
        let(:role) { public_policy_metadata_viewer }
        let(:headers) { { 'accept' => 'application/json' } }
        let(:params) { {} }

        describe 'unauthenticated' do
          it 'is unauthorized' do
            get('/api/resources/%s/iiif' % resource.id, params:, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated by login' do
          before { login_as user }

          describe 'when the resource does not exist' do
            it 'is not found' do
              get('/api/resources/%s/iiif' % SecureRandom.uuid, params:, headers:)
              expect(response.status).to eq 404
            end
          end

          describe 'when the user cannot view metadata on the resource' do
            let(:role) { curators_policy }

            it 'is forbidden' do
              get('/api/resources/%s/iiif' % resource.id, params:, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'when the user can view metadata on the resource' do
            let(:role) { [public_policy_metadata_viewer] }

            before do
              item = create_for_repository(:item, parent_id: resource.id, workflow_state: 'published',
                                                  permanent_id: 'ark:/99999/fk4cz3dh1', permanent_url: 'https://idn.duke.edu/ark:/99999/fk4cz3dh1')
              create_for_repository(:component, parent_id: item.id, workflow_state: 'published',
                                                permanent_id: 'ark:/99999/fk4cz3dh2', permanent_url: 'https://idn.duke.edu/ark:/99999/fk4cz3dh2')
            end

            it 'is successful' do
              get('/api/resources/%s/iiif' % resource.id, params:, headers:)
              expect(response.status).to eq 200
            end
          end
        end

        describe 'authenticated by token' do
          let(:headers) { { 'accept' => 'application/json', 'authorization' => 'Bearer zyzzyx' } }

          before do
            allow(OAuthRequest).to receive(:call).with(:introspection_endpoint, token: 'zyzzyx')
                                                 .and_return({ 'active' => true, 'user_id' => user.username })
          end

          describe 'when the resource does not exist' do
            it 'is not found' do
              get('/api/resources/%s/iiif' % SecureRandom.uuid, params:, headers:)
              expect(response.status).to eq 404
            end
          end

          describe 'when the user cannot view metadata on the resource' do
            let(:role) { curators_policy }

            it 'is forbidden' do
              get('/api/resources/%s/iiif' % resource.id, params:, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'when the user can view metadata on the resource' do
            let(:role) { [public_policy_metadata_viewer] }

            it 'is successful' do
              get('/api/resources/%s/iiif' % resource.id, params:, headers:)
              expect(response.status).to eq 200
            end
          end
        end
      end # 'GET /resources/:id/iiif'

      describe 'GET /resources/:id/manifest' do
        let(:resource) { create_for_repository(:collection, access_role: role) }
        let(:role) { public_policy_viewer }

        it_behaves_like 'a resources API that requires read permission', :get, '/api/resources/%s/manifest.csv'

        describe 'authenticated' do
          before { login_as user }

          it 'is successful' do
            item = create_for_repository(:item, parent_id: resource.id, admin_policy_id: resource.id)
            create_for_repository(:component, :with_content_file,
                                  parent_id: item.id, admin_policy_id: resource.id)
            get "/api/resources/#{resource.id}/manifest", headers: { 'content-type' => 'text/csv' }
            expect(response.status).to eq 200
          end
        end
      end # GET /resources/:id/manifest

      describe 'GET /resources/:id/members' do
        let(:resource) { create_for_repository(:collection, access_role: role) }
        let(:role) { public_policy_viewer }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/members'

        describe 'authenticated' do
          before { login_as user }

          it 'returns the members' do
            item = create_for_repository(:item, parent_id: resource.id, admin_policy_id: resource.id)
            component = create_for_repository(:component, parent_id: item.id, admin_policy_id: resource.id)
            get '/api/resources/%s/members' % resource.id
            expect(response.status).to eq 200
            data = JSON.parse(response.body)
            ids = data.map { |resource| resource['id'] }
            expect(ids).to contain_exactly(resource.id.to_s, item.id.to_s, component.id.to_s)
          end

          it 'can filter by model' do
            item = create_for_repository(:item, parent_id: resource.id, admin_policy_id: resource.id)
            component = create_for_repository(:component, parent_id: item.id, admin_policy_id: resource.id)
            get '/api/resources/%s/members?model=Component' % resource.id
            expect(response.status).to eq 200
            data = JSON.parse(response.body)
            ids = data.map { |resource| resource['id'] }
            expect(ids).to contain_exactly(component.id.to_s)
          end
        end
      end # GET /resources/:id/members

      describe 'GET /resources/:id/permanent_id' do
        let(:ark) { 'ark:/99999/fk4zzzzz' }
        let(:identifier) { Ezid::Identifier.new(ark) }
        let(:resource) { create_for_repository(:item, permanent_id: identifier.to_s, access_role: role) }
        let(:role) { public_resource_viewer }

        before do
          allow(Ezid::Identifier).to receive(:find).with(ark) { identifier }
        end

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/permanent_id'

        describe 'authenticated' do
          before { login_as user }

          describe 'when the resource has no permanent id' do
            let(:resource) { create_for_repository(:item, permanent_id: nil, access_role: role) }

            it 'is not found' do
              get '/api/resources/%s/permanent_id' % resource.id
              expect(response.status).to eq 404
            end
          end

          describe 'when the resource has a permanent id' do
            describe 'and the ARK is found' do
              it 'is successful' do
                get '/api/resources/%s/permanent_id' % resource.id
                expect(response.status).to eq 200
              end
            end

            describe 'and the ARK is not found' # TODO
          end
        end
      end # GET /resources/:id/permanent_id

      describe 'GET /resources/:id/permissions' do
        let(:resource) { create_for_repository(:item, access_role: role) }
        let(:role) { user_policy_editor }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/permissions'

        describe 'authenticated' do
          before { login_as user }

          it 'returns the permissions' do
            get '/api/resources/%s/permissions' % resource.id
            expect(response.status).to eq 200
            expect(JSON.parse(response.body)['read']).to eq [user.agent]
          end
        end
      end # GET /resources/:id/permissions

      describe 'GET /resources/:id/targets' do
        let(:resource) { create_for_repository(:collection, access_role: role) }
        let(:role) { public_policy_viewer }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/targets'
      end # GET /resources/:id/targets

      describe 'GET /resources/:id/technical_metadata' do
        let(:resource) { create_for_repository(:component, access_role: role) }
        let(:role) { public_resource_viewer }

        it_behaves_like 'a resources API that requires read permission',
                        :get, '/api/resources/%s/technical_metadata'

        describe 'authenticated' do
          before { login_as user }

          let(:collection) { create_for_repository(:collection, access_role: public_policy_viewer) }
          let(:item) { create_for_repository(:item, parent_id: collection.id, admin_policy_id: collection.id) }

          describe 'and is a Collection' do
            before do
              3.times { create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id) }
            end

            describe 'CSV media' do
              it 'is successful' do
                get '/api/resources/%s/technical_metadata.csv' % collection.id
                expect(response.status).to eq 200
              end
            end

            describe 'JSON media' do
              it 'is successful' do
                get '/api/resources/%s/technical_metadata' % collection.id
                expect(response.status).to eq 200
                expect(JSON.parse(response.body)).to be_present
              end
            end
          end

          describe 'and is an Item' do
            before do
              3.times { create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id) }
            end

            describe 'CSV media' do
              it 'is successful' do
                get '/api/resources/%s/technical_metadata.csv' % item.id
                expect(response.status).to eq 200
              end
            end

            describe 'JSON media' do
              it 'is successful' do
                get '/api/resources/%s/technical_metadata' % item.id
                expect(response.status).to eq 200
                expect(JSON.parse(response.body)).to be_present
              end
            end
          end

          describe 'and is a content-bearing resource' do
            let(:resource) { create_for_repository(:component, access_role: public_resource_viewer) }

            describe 'CSV media' do
              it 'is successful' do
                get '/api/resources/%s/technical_metadata.csv' % resource.id
                expect(response.status).to eq 200
              end
            end

            describe 'JSON media' do
              it 'is successful' do
                get '/api/resources/%s/technical_metadata' % resource.id
                expect(response.status).to eq 200
                expect(JSON.parse(response.body)).to be_present
              end
            end
          end
        end
      end # GET /resources/:id/technical_metadata

      describe 'POST /resources/:id/technical_metadata' do
        let(:resource) { create_for_repository(:component, :with_content_file, access_role: role) }
        let(:role) { user_resource_editor }

        describe 'unauthenticated' do
          it 'is unauthorized' do
            post '/api/resources/%s/technical_metadata' % resource.id
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'user cannot update the resource' do
            let(:role) { public_resource_viewer }

            it 'is forbidden' do
              post '/api/resources/%s/technical_metadata' % resource.id
              expect(response.status).to eq 403
            end
          end

          describe 'user can update the resource' do
            describe 'when the resource does not exist' do
              it 'is not found' do
                post '/api/resources/%s/technical_metadata' % SecureRandom.uuid
                expect(response.status).to eq 404
              end
            end

            describe 'when the resource exists' do
              describe 'and it is a Collection' do
                let(:resource) { create_for_repository(:collection, access_role: user_policy_editor) }

                it 'is a bad request' do
                  post '/api/resources/%s/technical_metadata' % resource.id
                  expect(response.status).to eq 400
                end
              end

              describe 'and it is an Item' do
                let(:resource) { create_for_repository(:item, access_role: user_policy_editor) }

                it 'is a bad request' do
                  post '/api/resources/%s/technical_metadata' % resource.id
                  expect(response.status).to eq 400
                end
              end

              describe 'and it is a content-bearing resource' do
                describe 'which has no content' do
                  let(:resource) { create_for_repository(:component, access_role: user_resource_editor) }

                  it 'is not found' do
                    post '/api/resources/%s/technical_metadata' % resource.id
                    expect(response.status).to eq 404
                  end
                end

                describe 'which has content' do
                  let(:resource) do
                    create_for_repository(:component, :with_content_file, access_role: user_resource_editor)
                  end

                  describe 'without a FITS output xml file' do
                    it 'is accepted' do
                      post '/api/resources/%s/technical_metadata' % resource.id
                      expect(response.status).to eq 202
                    end

                    it 'updates the FITS file' do
                      expect do
                        post '/api/resources/%s/technical_metadata' % resource.id
                      end.to(change { Ddr.find_resource(resource).fits_file })
                    end
                  end

                  describe 'with a FITS output xml file' do
                    let(:file) { fixture_file_upload('fits.xml', 'text/xml') }

                    it 'updates the FITS file' do
                      post '/api/resources/%s/technical_metadata' % resource.id,
                           headers: { 'content' => 'multipart/form-data' }, params: { file: }
                      expect(response.status).to eq 200
                      resource_ = Ddr.query_service.find_by(id: resource.id.to_s)
                      expect(resource_.fits_file.sha1).to eq '07e173b6b64a844366fb62238f37547847ee4277'
                    end
                  end
                end
              end
            end
          end
        end
      end # POST /resources/:id/technical_metadata

      describe 'POST /resources/:id/workflow' do
        let(:resource) { create_for_repository(:collection, access_role: role) }
        let(:role) { user_policy_editor }
        let(:params) { JSON.dump({ action: 'publish' }) }
        let(:headers) { { 'content-type' => 'application/json' } }

        describe 'unauthenticated' do
          it 'is unauthorized' do
            post('/api/resources/%s/workflow' % resource.id, params:, headers:)
            expect(response.status).to eq 401
          end
        end

        describe 'authenticated' do
          before { login_as user }

          describe 'user cannot update the resource' do
            let(:role) { public_resource_viewer }

            it 'is forbidden' do
              post('/api/resources/%s/workflow' % resource.id, params:, headers:)
              expect(response.status).to eq 403
            end
          end

          describe 'user can update the resource' do
            describe 'with no params' do
              it 'is a bad request' do
                post('/api/resources/%s/workflow' % resource.id, params: '', headers:)
                expect(response.status).to eq 400
              end
            end
          end
        end
      end # POST /resources/:id/workflow
    end
  end
end
