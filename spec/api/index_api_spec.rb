require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe IndexAPI, :api, :index, type: :request do
      let(:user) { FactoryBot.create(:user) }

      after { logout }

      describe 'GET /index'

      describe 'POST /index'

      describe 'GET /index/schema' do
        it_behaves_like 'a curator-only API', :get, '/api/index/schema'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it 'returns the schema' do
            get '/api/index/schema'
            expect(response.status).to eq 200
            expect(JSON.parse(response.body)['uniqueKey']).to eq 'id'
          end
        end
      end # GET /index/schema

      describe 'GET /index/:id' do
        let(:resource) { create_for_repository(:item) }

        it_behaves_like 'a curator-only API', :get, "/api/index/#{SecureRandom.uuid}"

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it 'returns the document' do
            get "/api/index/#{resource.id}"
            expect(response.status).to eq 200
            expect(JSON.parse(response.body)['id']).to eq resource.id.to_s
          end

          it_behaves_like 'a not found API call', :get, "/api/index/#{SecureRandom.uuid}"
        end
      end # GET /index/:id

      describe 'PUT /index/:id'

      describe 'DELETE /index/:id' do
        let(:resource) { create_for_repository(:item) }

        it_behaves_like 'a curator-only API', :delete, "/api/index/#{SecureRandom.uuid}"

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it 'deletes the document' do
            DatabaseRepository.delete(resource:)
            delete "/api/index/#{resource.id}"
            expect(response.status).to eq 200
            expect { IndexRepository.get_doc(resource.id) }.to raise_error(NotFoundError)
          end

          it 'is a conflict if the resource exists' do
            delete "/api/index/#{resource.id}"
            expect(response.status).to eq 409
          end

          it_behaves_like 'a not found API call', :delete, "/api/index/#{SecureRandom.uuid}"
        end
      end # DELETE /index/:id
    end
  end
end
