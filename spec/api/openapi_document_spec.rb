module Ddr::API
  RSpec.describe OpenAPIDocument, :api do
    it 'is a valid OpenAPI document' do
      validator = JSONSchemer.openapi(described_class.instance)
      expect(validator).to be_valid
    end
  end
end
