require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe 'BatchObjectsAPI', :api, :batch, type: :request do
      let(:user) { FactoryBot.create(:user) }

      before(:all) do
        Ddr::Batch::Batch.delete_all
        Ddr::Batch::BatchObject.delete_all
      end

      after do
        logout
        Ddr::Batch::Batch.delete_all
        Ddr::Batch::BatchObject.delete_all
      end

      describe 'GET /batch_objects' do
        let!(:batch_object) { FactoryBot.create(:ingest_batch_object, :has_batch) }

        it_behaves_like 'a curator-only API', :get, '/api/batch_objects'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/batch_objects'

          it 'returns data' do
            get '/api/batch_objects'
            batch_objects = JSON.parse(response.body)
            expect(batch_objects.first['id']).to eq batch_object.id
          end

          it 'is available as CSV' do
            get '/api/batch_objects.csv'
            expect(response.status).to eq 200
          end

          describe 'with a resource_id param' do
            let(:batch_object) { FactoryBot.create(:update_batch_object, :has_batch) }

            it 'is successful' do
              get '/api/batch_objects?resource_id=%s' % batch_object.resource_id
              expect(response.status).to eq 200
              batch_objects = JSON.parse(response.body)
              expect(batch_objects.first['id']).to eq batch_object.id
            end
          end

          it 'accepts a "type" param' do
            get '/api/batch_objects?type=Ingest'
            expect(response.status).to eq 200
            batch_objects = JSON.parse(response.body)
            expect(batch_objects.first['id']).to eq batch_object.id
          end

          describe 'with a model param' do
            let(:batch_object) { FactoryBot.create(:basic_update_batch_object, :has_batch) }

            it 'is successful' do
              get '/api/batch_objects?model=Component'
              expect(response.status).to eq 200
              batch_objects = JSON.parse(response.body)
              expect(batch_objects.first['id']).to eq batch_object.id
            end
          end

          %i[processed handled verified validated].each do |field|
            describe "with boolean field param #{field.inspect}" do
              let(:batch_object) { FactoryBot.create(:basic_update_batch_object, :has_batch, field => false) }

              it 'is successful' do
                get '/api/batch_objects?%s=true' % field
                expect(response.status).to eq 200
                batch_objects = JSON.parse(response.body)
                expect(batch_objects).to be_empty
              end
            end
          end
        end
      end

      describe 'GET /batch_objects/:id' do
        let(:batch_object) { FactoryBot.create(:basic_ingest_batch_object, :has_batch) }

        it_behaves_like 'a curator-only API', :get, '/api/batch_objects/1'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          describe 'when the batch object does not exist' do
            it 'is not found' do
              get '/api/batch_objects/%s' % Random.rand(10_000)
              expect(response.status).to eq 404
            end
          end

          describe 'when the batch object exists' do
            it 'is successful' do
              get '/api/batch_objects/%s' % batch_object.id
              expect(response.status).to eq 200
            end
          end
        end
      end
    end
  end
end
