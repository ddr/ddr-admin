require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe ReportsAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }

      after { logout }

      describe 'Collection summary report' do
        it_behaves_like 'a curator-only API', :get, '/api/reports/collection_summary.csv'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/reports/collection_summary.csv'
        end
      end

      describe 'Duplicate content report' do
        it_behaves_like 'a curator-only API', :get, '/api/reports/duplicate_content.csv'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it_behaves_like 'a successful API call', :get, '/api/reports/duplicate_content.csv'
        end
      end

      describe 'Roles report' do
        it_behaves_like 'a curator-only API', :get, '/api/reports/duplicate_content.csv'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
            user = FactoryBot.create(:user)
            role = FactoryBot.build(:role, :curator, :policy, agent: user)
            @coll = create_for_repository(:collection, access_role: [role])
            @item = create_for_repository(:item, parent_id: @coll.id)
            create_for_repository(:component, parent_id: @item.id)
          end

          it_behaves_like 'a successful API call', :get, '/api/reports/roles.csv'
          it_behaves_like 'a successful API call', :get, '/api/reports/roles.csv?include_inherited=true'
          it_behaves_like 'a not found API call', :get, "/api/reports/roles.csv?collection_id=#{SecureRandom.uuid}"

          it 'accepts a collection id param' do
            get '/api/reports/roles.csv?collection_id=%s' % @coll.id
            expect(response.status).to eq 200
          end

          it 'is a bad request to use a non-collection for collection_id' do
            get '/api/reports/roles.csv?collection_id=%s' % @item.id
            expect(response.status).to eq 400
          end
        end
      end
    end
  end
end
