require 'rails_helper'
require 'support/shared_examples_for_api_authnz'

module Ddr
  module API
    RSpec.describe QueuesAPI, :api, type: :request do
      let(:user) { FactoryBot.create(:user) }

      after { logout }

      describe 'DELETE /queues/locks' do
        it_behaves_like 'a curator-only API', :delete, '/api/queues/locks'

        describe 'as curator' do
          before do
            login_as user
            allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
          end

          it 'deletes all locks' do
            allow(ActiveJob::Uniqueness).to receive(:unlock!).and_call_original
            delete '/api/queues/locks'
            expect(response.status).to eq 200
            expect(ActiveJob::Uniqueness).to have_received(:unlock!)
          end
        end
      end # DELETE /queues/locks

      # GET /queues/stats - redirect to Sidekiq web
      it 'redirects' do
        login_as user
        allow_any_instance_of(::Ability).to receive(:curator?).and_return(true)
        get '/api/queues/stats'
        expect(response).to redirect_to('/queues/stats')
      end
    end
  end
end
