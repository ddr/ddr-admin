module Ddr
  module API
    RSpec.describe Root, :api, type: :request do
      it 'serves the OpenAPI doc' do
        get '/api/openapi.json'
        expect(response.status).to eq 200
        expect(JSON.parse(response.body)).to eq OpenAPIDocument.instance
      end

      it 'serves the Swagger doc (deprecated)' do
        get '/api/swagger_doc.json'
        expect(response.status).to eq 301
        expect(response.headers['location']).to eq '/api/openapi.json'
      end

      Schema.names.each do |schema_name|
        it "redirects to the '#{schema_name}' schema" do
          get '/api/schema/%s' % schema_name
          expect(response).to be_redirect
          follow_redirect!
          schema = JSON.parse(response.body)
          expect(schema).to eq Schema[schema_name]
        end
      end
    end
  end
end
