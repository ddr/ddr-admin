module Ddr::API
  RSpec.describe FullyQualifiedModel do
    subject { described_class.fq_model(value) }

    describe 'class' do
      let(:value) { Ddr::Item }

      it { is_expected.to eq 'Ddr::Item' }
    end

    describe 'model name' do
      let(:value) { 'Ddr::Item' }

      it { is_expected.to eq 'Ddr::Item' }
    end

    describe 'model short name' do
      let(:value) { 'Item' }

      it { is_expected.to eq 'Ddr::Item' }
    end

    describe 'symbol' do
      let(:value) { :item }

      it { is_expected.to eq 'Ddr::Item' }
    end

    describe 'array' do
      let(:value) { %w[Collection Item] }

      it { is_expected.to eq %w[Ddr::Collection Ddr::Item] }
    end
  end
end
