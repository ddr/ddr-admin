module Ddr
  RSpec.describe ThumbnailsJob do
    let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }

    before do
      allow(Ddr.query_service).to receive(:find_by).with(id: valkyrie_id) { resource }
    end

    describe 'called with the ID of a Ddr::Collection' do
      let(:resource) { Ddr::Collection.new(id: valkyrie_id) }

      it 'calls the thumbnails service with the collection' do
        expect(ThumbnailsService).to receive(:call).with(resource)
        described_class.new.perform(valkyrie_id)
      end
    end

    describe 'called with the ID of a resource that is not a Ddr::Collection' do
      let(:resource) { Ddr::Item.new(id: valkyrie_id) }

      it 'raises an error' do
        expect { described_class.new.perform(valkyrie_id) }.to raise_error(ArgumentError,
                                                                           I18n.t('ddr.thumbnails.model_error'))
      end
    end
  end
end
