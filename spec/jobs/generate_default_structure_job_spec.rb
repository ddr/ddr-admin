module Ddr
  RSpec.describe GenerateDefaultStructureJob do
    let(:resource) { create_for_repository(:collection) }

    describe '#perform' do
      it 'suppresses (parent) structure updating' do
        expect(Ddr::UpdateParentStructure).not_to receive(:call)
        subject.perform(resource.id)
      end

      it 'updates the structural metadata' do
        create_for_repository(:item, parent_id: resource.id)
        expect { subject.perform(resource.id) }
          .to change { Ddr.query_service.find_by(id: resource.id).struct_metadata }.from(nil)
      end
    end
  end
end
