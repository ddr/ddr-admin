module Ddr
  RSpec.describe UpdateIndexJob do
    it 'uses the :index queue' do
      expect(described_class.queue_name).to eq('default')
    end

    describe '#perform' do
      let(:valkyrie_ids) { [build(:valkyrie_id), build(:valkyrie_id)] }
      let(:valkyrie_ids_ids) { valkyrie_ids.map(&:id) }

      it 'calls `#index` on IndexService initialized with the resource ids' do
        expect(IndexService).to receive(:new).with(valkyrie_ids_ids).and_call_original
        expect_any_instance_of(IndexService).to receive(:index)
        described_class.new.perform(valkyrie_ids_ids)
      end
    end
  end
end
