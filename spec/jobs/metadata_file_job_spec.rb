module Ddr
  RSpec.describe MetadataFileJob, type: :job do
    describe '#perform' do
      let(:metadata_file_id) { 1 }
      let(:metadata_file) { Ddr::MetadataFile.new(id: metadata_file_id) }
      let(:batch_id) { 7 }
      let(:batch) { Ddr::Batch::Batch.new(id: batch_id) }

      before do
        allow(MetadataFile).to receive(:find).with(metadata_file_id).and_return(metadata_file)
        allow(Batch::Batch).to receive(:find).with(batch_id).and_return(batch)
      end

      describe 'metadata file processing' do
        before do
          allow(MonitorMetadataFile).to receive(:call)
        end

        it 'processes the metadata file' do
          expect(metadata_file).to receive(:procezz).and_return(batch_id)
          subject.perform({ 'id' => metadata_file_id })
        end
      end

      describe 'notification' do
        before do
          allow(metadata_file).to receive(:procezz).and_return(batch_id)
        end

        it 'issues a Metadata File FINISHED notification' do
          expect(ActiveSupport::Notifications).to receive(:instrument).with(MetadataFile::FINISHED,
                                                                            metadata_file_id:,
                                                                            batch_id:)
          subject.perform({ 'id' => metadata_file_id })
        end
      end
    end
  end
end
