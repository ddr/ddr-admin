module Ddr
  RSpec.describe UploadFileJob do
    let(:resource) { FactoryBot.create_for_repository(:component, :with_content_file) }
    let(:file_path) { Rails.root.join('spec/fixtures/video.mp4').to_s }

    it 'updates a file' do
      expect(resource.content.original_filename).to eq 'imageA.tif'
      described_class.perform_now(resource_id: resource.id.id, file_path:, file_type: 'content')

      updated = Ddr.query_service.find_by(id: resource.id.id)
      expect(updated.content.original_filename).to eq 'video.mp4'
    end
  end
end
