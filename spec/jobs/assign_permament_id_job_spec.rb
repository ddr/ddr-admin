module Ddr
  RSpec.describe AssignPermanentIdJob do
    describe '.perform' do
      let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }

      it 'calls the assign permament id (arkid) service on the resource' do
        expect(AssignPermanentIdService).to receive(:call).with(resource_id)
        described_class.new.perform(resource_id)
      end
    end
  end
end
