module Ddr
  RSpec.describe ManifestBasedIngestJob, type: :job do
    let(:user_key) { 'joe@test.edu' }
    let(:collection_id) { 'test:1' }
    let(:batch) { double('Ddr::Batch::Batch', id: 5) }
    let(:item_count) { 7 }
    let(:component_count) { 10 }
    let(:target_count) { 2 }
    let(:results_struct) { Struct.new(:batches, :errors, :manifest) }
    let(:filepath) { Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/manifest.txt').to_s }
    let(:volumes) { { '/path/to/spec/fixtures' => Rails.root.join('spec/fixtures').to_s } }
    let(:job_params) { { 'batch_user' => user_key, 'manifest_file' => filepath } }

    before do
      allow_any_instance_of(ManifestBasedIngest).to receive(:load_configuration) { { volumes: } }
    end

    describe 'finished' do
      describe 'success' do
        let(:file_count) { 13 }
        let(:model_stats) do
          { collections: collection_count, items: item_count,
            components: component_count, targets: target_count }
        end
        let(:results) { results_struct.new(batches: [batch], errors: [], manifest: Manifest.new) }

        before do
          allow_any_instance_of(ManifestBasedIngest).to receive(:build_batches) { [batch] }
          allow_any_instance_of(Manifest).to receive(:content_model_stats) { model_stats }
          allow_any_instance_of(Manifest).to receive(:file_count) { file_count }
        end

        describe 'collection ID present' do
          let(:collection_count) { 0 }

          before { job_params.merge!({ 'collection_id' => collection_id }) }

          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(ManifestBasedIngest::FINISHED,
                                                                              user_key:,
                                                                              collection_id:,
                                                                              file_count:,
                                                                              model_stats:,
                                                                              errors: [],
                                                                              batch_ids: batch.id.to_s)
            subject.perform(job_params)
          end
        end

        describe 'collection ID not present' do
          let(:collection_count) { 1 }

          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(ManifestBasedIngest::FINISHED,
                                                                              user_key:,
                                                                              collection_id: nil,
                                                                              file_count:,
                                                                              model_stats:,
                                                                              errors: [],
                                                                              batch_ids: batch.id.to_s)
            subject.perform(job_params)
          end
        end
      end

      describe 'failure' do
        let(:error_message) { 'Something went wrong' }
        let(:error) { Ddr::Batch::Error.new(error_message) }

        before do
          allow_any_instance_of(ManifestBasedIngest).to receive(:build_batches) { raise error }
          allow_any_instance_of(Manifest).to receive(:content_model_stats).and_return({})
          allow_any_instance_of(Manifest).to receive(:file_count).and_return(0)
        end

        describe 'collection ID present' do
          before { job_params.merge!({ 'collection_id' => collection_id }) }

          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(ManifestBasedIngest::FINISHED,
                                                                              user_key:,
                                                                              collection_id:,
                                                                              file_count: 0,
                                                                              model_stats: {},
                                                                              errors: [error_message],
                                                                              batch_ids: nil)
            subject.perform(job_params)
          end
        end

        describe 'collection ID not present' do
          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(ManifestBasedIngest::FINISHED,
                                                                              user_key:,
                                                                              collection_id: nil,
                                                                              file_count: 0,
                                                                              model_stats: {},
                                                                              errors: [error_message],
                                                                              batch_ids: nil)
            subject.perform(job_params)
          end
        end
      end
    end
  end
end
