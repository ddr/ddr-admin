module Ddr
  RSpec.describe UpdatePermanentIdJob do
    let(:resource) { create_for_repository(:item) }

    it 'updates the permanent ID' do
      allow(PermanentId).to receive(:update!).with(resource.id.to_s).and_call_original
      described_class.perform_now(resource.id.to_s)
      expect(PermanentId).to have_received(:update!).with(resource.id.to_s)
    end
  end
end
