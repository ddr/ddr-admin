require 'rails_helper'
require 'fileutils'

module Ddr
  RSpec.describe ExportFilesJob, type: :job do
    let(:identifiers) { [Valkyrie::ID.new(SecureRandom.uuid).id] }
    let(:basename) { 'testing_file_export' }
    let(:files) { %w[content thumbnail] }
    let(:user) { FactoryBot.create(:user) }
    let(:export) { ExportFiles::Package.new(identifiers:, basename:, files:) }

    after do
      Dir["#{Ddr::Admin.export_files_store}/*"].each { |ent| FileUtils.remove_entry_secure(ent, true) }
    end

    context 'success notification' do
      before do
        allow(ExportFiles::Package).to receive(:call).and_return(export)
      end

      it 'sends an email for success notification' do
        expect(ExportFilesMailer).to receive(:notify_success).and_call_original
        subject.perform(identifiers, basename, user.id, files)
        expect(ActionMailer::Base.deliveries).not_to be_empty
      end
    end

    context 'failure notification' do
      before do
        allow(ExportFiles::Package).to receive(:call).and_raise(RuntimeError)
      end

      it 'sends an email for failure notification' do
        expect(ExportFilesMailer).to receive(:notify_failure).and_call_original
        begin
          subject.perform(identifiers, basename, user.id, files)
        rescue RuntimeError => _e
          # do nothing
        end
        expect(ActionMailer::Base.deliveries).not_to be_empty
      end
    end
  end
end
