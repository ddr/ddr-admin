module Ddr
  RSpec.describe FixityCheckJob do
    it 'is queued as low priority by default' do
      expect(described_class.queue_name).to eq('low_priority')
    end

    describe 'fixity check results' do
      describe 'success' do
        subject { described_class.new.perform(resource.id) }
        let(:resource) { FactoryBot.create_for_repository(:component, :with_content_file) }

        it { is_expected.to be_a FixityCheck::Result }
      end

      describe 'failure' do
        let(:resource) { FactoryBot.create_for_repository(:component) }
        let(:result) do
          FixityCheck::Result.new({ resource_id: resource.id, success: false, results: { content: false } })
        end
        let(:error_msg) do
          I18n.t('ddr.fixity.results_msg', repo_id: resource.id.id, file_results: result.results)
        end

        before do
          allow(FixityCheck).to receive(:call).and_return(result)
        end

        it 'throws an error' do
          expect { described_class.new.perform(resource.id) }.to raise_error(Ddr::ChecksumInvalid, error_msg)
        end
      end
    end
  end
end
