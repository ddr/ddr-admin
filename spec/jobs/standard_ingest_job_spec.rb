module Ddr
  RSpec.describe StandardIngestJob, type: :job do
    let(:user_key) { 'joe@test.edu' }
    let(:basepath) { '/foo/bar/' }
    let(:subpath) { 'baz' }
    let(:collection_pid) { 'test:1' }
    let(:batch) { double('Ddr::Batch::Batch', id: 5) }
    let(:item_count) { 7 }
    let(:component_count) { 10 }
    let(:target_count) { 2 }
    let(:job_params) { { 'batch_user' => user_key, 'basepath' => basepath, 'subpath' => subpath } }

    before do
      allow_any_instance_of(StandardIngest).to receive(:load_configuration).and_return({})
      allow_any_instance_of(InspectStandardIngest).to receive(:call) { inspection_results }
    end

    describe 'finished' do
      describe 'success' do
        let(:file_count) { 13 }
        let(:model_stats) do
          { collections: collection_count, items: item_count,
            components: component_count, targets: target_count }
        end
        let(:inspection_results) do
          InspectStandardIngest::Results.new(file_count, ['metadata.txt'], model_stats, Filesystem.new)
        end

        before do
          allow_any_instance_of(StandardIngest).to receive(:build_batch) { batch }
        end

        describe 'collection ID present' do
          let(:collection_count) { 0 }

          before { job_params.merge!({ 'collection_id' => collection_pid }) }

          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(StandardIngest::FINISHED,
                                                                              user_key:,
                                                                              basepath:,
                                                                              subpath:,
                                                                              collection_id: collection_pid,
                                                                              file_count:,
                                                                              model_stats:,
                                                                              errors: [],
                                                                              batch_id: batch.id)
            subject.perform(job_params)
          end
        end

        describe 'collection ID not present' do
          let(:collection_count) { 1 }

          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(StandardIngest::FINISHED,
                                                                              user_key:,
                                                                              basepath:,
                                                                              subpath:,
                                                                              collection_id: nil,
                                                                              file_count:,
                                                                              model_stats:,
                                                                              errors: [],
                                                                              batch_id: batch.id)
            subject.perform(job_params)
          end
        end
      end

      describe 'errors' do
        let(:error_message) { 'Error' }
        let(:error) { Ddr::Batch::Error.new(error_message) }
        let(:inspection_results) do
          InspectStandardIngest::Results.new
        end

        before do
          allow_any_instance_of(StandardIngest).to receive(:build_batch) { raise error }
        end

        describe 'collection ID present' do
          before { job_params.merge!({ 'collection_id' => collection_pid }) }

          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(StandardIngest::FINISHED,
                                                                              user_key:,
                                                                              basepath:,
                                                                              subpath:,
                                                                              collection_id: collection_pid,
                                                                              file_count: nil,
                                                                              model_stats: nil,
                                                                              errors: [error_message],
                                                                              batch_id: nil)
            subject.perform(job_params)
          end
        end

        describe 'collection ID not present' do
          it 'publishes the appropriate notification' do
            expect(ActiveSupport::Notifications).to receive(:instrument).with(StandardIngest::FINISHED,
                                                                              user_key:,
                                                                              basepath:,
                                                                              subpath:,
                                                                              collection_id: nil,
                                                                              file_count: nil,
                                                                              model_stats: nil,
                                                                              errors: [error_message],
                                                                              batch_id: nil)
            subject.perform(job_params)
          end
        end
      end
    end
  end
end
