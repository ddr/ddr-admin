module Ddr
  RSpec.describe IngestFolderJob, type: :job do
    describe '#perform' do
      let(:ingest_folder_id) { 1 }
      let(:ingest_folder) { Ddr::IngestFolder.new(id: ingest_folder_id) }
      let(:batch_id) { 7 }
      let(:batch) { Ddr::Batch::Batch.new(id: batch_id) }

      before do
        allow(IngestFolder).to receive(:find).with(ingest_folder_id).and_return(ingest_folder)
        allow(Batch::Batch).to receive(:find).with(batch_id).and_return(batch)
      end

      describe 'ingest folder processing' do
        before do
          allow(MonitorIngestFolder).to receive(:call)
        end

        it 'processes the ingest folder' do
          expect(ingest_folder).to receive(:procezz).and_return(batch_id)
          subject.perform({ 'id' => ingest_folder_id })
        end
      end

      describe 'notification' do
        before do
          allow(ingest_folder).to receive(:procezz).and_return(batch_id)
        end

        it 'issues an IngestFolder FINISHED notification' do
          expect(ActiveSupport::Notifications).to receive(:instrument).with(IngestFolder::FINISHED,
                                                                            ingest_folder_id:,
                                                                            batch_id:)
          subject.perform({ 'id' => ingest_folder_id })
        end
      end
    end
  end
end
