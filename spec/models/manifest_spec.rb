module Ddr
  RSpec.describe Manifest do
    subject { described_class.new(filepath, volumes) }

    let(:volumes) { { '/path/to/spec/fixtures' => Rails.root.join('spec/fixtures').to_s } }

    describe 'valid manifest' do
      let(:filepath) do
        Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/manifest.txt').to_s
      end

      it 'behaves like a valid manifest' do
        expect(subject).to be_valid
        expect(subject.length).to eq 4
        expect(subject.first).to be_a Ddr::Manifest::ManifestResource
        expect(subject.first.model).to eq 'Ddr::Target'
        expect(subject.next).to be_a Ddr::Manifest::ManifestResource
        # There is a space at the end of the title to test the whitespace converter
        item = subject.next
        expect(item.title).to eq 'Item A Title'
        expect(item.metadata).to be_a Hash
      end
    end

    describe 'invalid manifest' do
      let(:filepath) do
        Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/invalid_manifest.txt').to_s
      end

      it 'behaves like an invalid manifest' do
        expect(subject).not_to be_valid
        expect(subject.errors.length).to eq 3
      end
    end
  end
end
