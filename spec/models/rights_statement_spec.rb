module Ddr
  RSpec.describe RightsStatement, :aux do
    describe '.all' do
      subject { described_class.all }

      it { is_expected.to be_a Array }
      it { is_expected.to all(be_a(described_class)) }
    end

    describe '.call' do
      let(:obj) { Item.new }

      subject { described_class.call(obj) }

      describe 'when the object has a rights statement' do
        before { obj.rights = 'https://creativecommons.org/publicdomain/mark/1.0' }

        it { is_expected.to be_a described_class }
      end

      describe 'when the object does not have a rights statement' do
        it { is_expected.to be_nil }
      end
    end

    describe 'string representation' do
      subject { described_class.keystore.fetch('https://creativecommons.org/publicdomain/mark/1.0') }

      its(:to_s) { is_expected.to eq 'Creative Commons Public Domain Mark 1.0' }
    end
  end
end
