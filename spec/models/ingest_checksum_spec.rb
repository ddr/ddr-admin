module Ddr
  RSpec.describe IngestChecksum, :batch, :ingest, type: :model do
    let(:checksum_filepath) { Rails.root.join('spec/fixtures/batch_ingest/miscellaneous/checksums.txt') }
    let(:ic) { IngestChecksum.new(checksum_filepath) }

    describe 'checksum' do
      it 'provides the recorded checksum' do
        expect(ic.checksum('/base/path/subpath/file01002.tif'))
          .to eq('ea14084df3e55b170e7063d6ac705b33423921fc69e4edcbc843743b6651b1cb')
      end

      it 'handles paths with spaces properly' do
        expect(ic.checksum('/base/path/subpath/file with spaces.txt'))
          .to eq('2316b3884e810e12b248caaf531b8a1ecae2bd3b02ebfb11ac43f117f5fb7113')
      end

      it 'returns nil if the path is not present in the file' do
        expect(ic.checksum('/base/path/subpath/image001.tif')).to be_nil
      end
    end
  end
end
