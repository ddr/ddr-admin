require 'rails_helper'
require 'support/ingest_folder_helper'

module Ddr
  RSpec.describe FileUpload, :batch, type: :model do
    subject { described_class.new(ingest_args) }

    describe '#build_batch' do
      let(:user) { FactoryBot.create(:user) }
      let(:basepath) { '/test/' }
      let(:batch_description) { filesystem.root.name }
      let(:batch_name) { 'File Upload' }
      let(:ddr_file_name) { :intermediate_file }
      let(:collection) { double('Collection', id: Valkyrie::ID.new(SecureRandom.uuid)) }
      let(:subpath) { 'directory/' }
      let(:filesystem) { sample_filesystem_without_dot_files }
      let(:fs_node_paths) { filesystem.each_leaf.map { |leaf| Filesystem.node_locator(leaf) } }
      let(:test_config_file) do
        Rails.root.join('spec/fixtures/batch_update/file_upload/file_upload.yml')
      end

      before do
        allow(::File).to receive(:read).and_call_original
        allow(subject).to receive(:filesystem) { filesystem }
        allow_any_instance_of(BuildBatchFromFileUpload).to receive(:call).and_return(nil)
      end

      describe 'without checksums' do
        let(:ingest_args) do
          { 'basepath' => basepath,
            'batch_user' => user.user_key,
            'collection_id' => collection.id.id,
            'ddr_file_name' => ddr_file_name,
            'subpath' => subpath }
        end
        let(:batch_builder_args) do
          { batch_description:,
            batch_name:,
            batch_user: user,
            ddr_file_name:,
            filesystem:,
            collection_id: collection.id.id }
        end

        it 'calls the batch builder correctly' do
          expect(BuildBatchFromFileUpload).to receive(:new).with(batch_builder_args).and_call_original
          subject.build_batch
        end
      end

      describe 'with checksums' do
        let(:checksum_file) { '/tmp/test/checksums.txt' }
        let(:ingest_args) do
          { 'basepath' => basepath,
            'batch_user' => user.user_key,
            'checksum_file' => checksum_file,
            'collection_id' => collection.id.id,
            'ddr_file_name' => ddr_file_name,
            'subpath' => subpath }
        end
        let(:batch_builder_args) do
          { batch_description:,
            batch_name:,
            batch_user: user,
            ddr_file_name:,
            filesystem:,
            checksum_file_path: checksum_file,
            collection_id: collection.id.id }
        end

        it 'calls the batch builder correctly' do
          expect(BuildBatchFromFileUpload).to receive(:new).with(batch_builder_args).and_call_original
          subject.build_batch
        end
      end
    end
  end
end
