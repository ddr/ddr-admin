module Ddr
  RSpec.describe StandardIngestChecksum, :batch, :standard_ingest, type: :model do
    let(:checksum_filepath) do
      Rails.root.join('spec/fixtures/batch_ingest/standard_ingest/manifest-sha1.txt')
    end
    let(:sic) { StandardIngestChecksum.new(checksum_filepath) }

    describe 'checksum' do
      it 'provides the recorded checksum' do
        expect(sic.checksum('file01001.tif')).to eq('7cc5abd7ed8c1c907d86bba5e6e18ed6c6ec995c')
        expect(sic.checksum('targets/T001.tiff')).to eq('2cf23f0035c12b6242093e93d0f7eeba0b1e08e8')
        expect(sic.checksum('image001.tif')).to be_nil
        expect(sic.checksum('file name with spaces.jpg')).to eq('64a67ed6270474bdf21302bbf35f99b53519e9b7')
      end
    end
  end
end
