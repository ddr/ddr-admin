require 'rails_helper'
require 'support/ingest_folder_helper'

shared_examples 'an invalid ingest folder' do
  it { is_expected.to be_invalid }

  it 'has an error message' do
    subject.valid?
    expect(subject.errors).to have_key(error_field)
  end
end

shared_examples 'a proper set of batch objects' do
  it 'has the appropriate attributes, datastreams, and relationships' do
    expect(user.batches.count).to eql(1)
    expect(user.batches.first.name).to eql(I18n.t('ddr.ingest_folder.batch_name'))

    expect(user.batches.first.description).to eql(subject.full_path)

    expect(user.batches.first.status).to eql(Ddr::Batch::Batch::STATUS_READY)
    expect(user.batches.first.collection_id).to eq(collection.id.id)
    expect(user.batches.first.collection_title).to eq(collection.title.first)
    expect(objects.fetch('f').model).to eql('Ddr::Item')
    expect(objects.fetch('file01001').model).to eql('Ddr::Component')
    expect(objects.fetch('file01002').model).to eql('Ddr::Component')
    expect(objects.fetch('file01').model).to eql('Ddr::Component')
    expect(objects.fetch('T001').model).to eql('Ddr::Target')
    expect(objects.fetch('T002').model).to eql('Ddr::Target')
    expect(atts.fetch('f').fetch('local_id').value).to eql('f')
    expect(atts.fetch('file01001').fetch('local_id').value).to eql('file01001')
    expect(atts.fetch('file01002').fetch('local_id').value).to eql('file01002')
    expect(atts.fetch('file01').fetch('local_id').value).to eql('file01')
    expect(atts.fetch('T001').fetch('local_id').value).to eql('T001')
    expect(atts.fetch('T002').fetch('local_id').value).to eql('T002')
    expect(dss.fetch('file01001').fetch('content').payload).to eql(File.join(subject.full_path, 'file01001.tif'))
    expect(dss.fetch('file01002').fetch('content').payload).to eql(File.join(subject.full_path, 'file01002.tif'))
    expect(dss.fetch('file01').fetch('content').payload).to eql(File.join(subject.full_path, 'pdf/file01.pdf'))
    expect(dss.fetch('T001').fetch('content').payload).to eql(File.join(subject.full_path, 'targets/T001.tiff'))
    expect(dss.fetch('T002').fetch('content').payload).to eql(File.join(subject.full_path, 'targets/T002.tiff'))
    expect(rels.fetch('f').fetch('parent').object).to eql(subject.collection_id)
    expect(rels.fetch('file01001').fetch('parent').object).to eql(objects.fetch('f').resource_id)
    expect(rels.fetch('file01002').fetch('parent').object).to eql(objects.fetch('f').resource_id)
    expect(rels.fetch('file01').fetch('parent').object).to eql(objects.fetch('f').resource_id)
    expect(rels.fetch('T001').fetch('for_collection').object).to eql(subject.collection_id)
    expect(rels.fetch('T002').fetch('for_collection').object).to eql(subject.collection_id)
  end

  it 'does not set the source descriptive metadata attribute' do
    expect(atts.fetch('file01001')['source']).to be_nil
    expect(atts.fetch('file01002')['source']).to be_nil
    expect(atts.fetch('file01')['source']).to be_nil
    expect(atts.fetch('T001')['source']).to be_nil
    expect(atts.fetch('T002')['source']).to be_nil
  end
end

module Ddr
  RSpec.describe IngestFolder, :ingest, :ingest_folder, type: :model do
    subject do
      described_class.new(
        base_path:,
        sub_path:,
        checksum_type:,
        collection_id:,
        user:,
        parent_id_length: 1
      )
    end

    let(:base_path) { FileUtils.mkdir_p(::File.join(@tempdir, 'base_path')).first }
    let(:sub_path) { 'sub_path' }
    let(:checksum_file_dir) { FileUtils.mkdir_p(::File.join(@tempdir, 'checksums')).first }
    let(:checksum_file_location) { ::File.join(checksum_file_dir, checksum_file_name) }
    let(:checksum_file_name) { 'sub_path-base_path-sha1.txt' }
    let(:checksum_type) { 'SHA1' }
    let(:collection_id) { '26aa6ec5-d530-44c7-ad97-2e1ecc3a9d25' }
    let(:user) { FactoryBot.create(:user) }

    before do
      @tempdir = Dir.mktmpdir('ingest_folder-', Rails.root.join('tmp'))
      FileUtils.mkdir_p ::File.join(base_path, sub_path)
      allow(described_class).to receive(:checksum_file_dir).and_return(checksum_file_dir)
      begin
        FileUtils.touch checksum_file_location
      rescue StandardError
        nil
      end # The rescue clause is a hack
      allow(described_class).to receive(:permitted_base_paths) { [::File.join(@tempdir, 'base_path')] }
    end

    after { FileUtils.rm_rf(@tempdir) }

    context 'validation' do
      it { is_expected.to be_valid }

      context 'base path not permitted' do
        let(:error_field) { :base_path }
        let(:base_path) { ::File.join(@tempdir, 'not_permitted') }

        it_behaves_like 'an invalid ingest folder'
      end

      context 'subpath missing' do
        let(:error_field) { :sub_path }

        before { subject.sub_path = '' }

        it_behaves_like 'an invalid ingest folder'
      end

      context 'subpath readable' do
        let(:error_field) { :sub_path }

        before { subject.sub_path = 'unreadable' }

        it_behaves_like 'an invalid ingest folder'
      end

      context 'collection ID missing' do
        let(:error_field) { :collection_id }
        let(:collection_id) { '' }

        it_behaves_like 'an invalid ingest folder'
      end

      context 'checksum file unreadable' do
        let(:error_field) { :checksum_file }

        before { subject.checksum_file = 'unreadable.txt' }

        it_behaves_like 'an invalid ingest folder'
      end

      context 'checksum file is not permitted' do
        let(:error_field) { :checksum_file }

        before { subject.checksum_file = '/etc/passwd' }

        it_behaves_like 'an invalid ingest folder'
      end
    end

    context 'methods' do
      describe '#checksum_file_location' do
        context 'checksum file not specified' do
          its(:checksum_file_location) { is_expected.to eq checksum_file_location }
        end

        context 'relative path to checksum file' do
          let(:checksum_file_name) { 'checksum_file.txt' }

          before do
            subject.checksum_file = checksum_file_name
          end

          its(:checksum_file_location) { is_expected.to eq checksum_file_location }
        end

        context 'absolute path to checksum file' do
          let(:checksum_file_location) { ::File.join(@tempdir, 'fixity', 'checksum_file.txt') }

          before do
            FileUtils.mkdir_p ::File.dirname(checksum_file_location)
            FileUtils.touch checksum_file_location
            subject.checksum_file = checksum_file_location
          end

          its(:checksum_file_location) { is_expected.to eq checksum_file_location }
        end
      end
    end

    context 'operations' do
      let(:collection) { FactoryBot.create_for_repository(:collection) }
      let(:collection_id) { collection.id }

      before do
        FileUtils.cd subject.full_path do
          FileUtils.touch ['Thumbs.db', 'movie.mp4', 'file01001.tif', 'file01002.tif']

          FileUtils.mkdir_p %w[pdf targets]

          FileUtils.cd 'pdf' do
            FileUtils.touch ['file01.pdf', 'track01.wav']
          end

          FileUtils.cd 'targets' do
            FileUtils.touch ['Thumbs.db', 'T001.tiff', 'T002.tiff']
          end
        end
      end

      context 'scan' do
        context 'no warnings' do
          let(:scan_results) { subject.scan }

          it 'reports the correct scan results' do
            expect(scan_results.total_count).to eql(9)
            expect(scan_results.file_count).to eql(5)
            expect(scan_results.parent_count).to eql(3)
            expect(scan_results.target_count).to eql(2)
            expect(scan_results.excluded_files).to contain_exactly(::File.join(base_path, sub_path, 'Thumbs.db'),
                                                                   ::File.join(base_path, sub_path, 'targets',
                                                                               'Thumbs.db'))
            expect(subject.errors).to be_empty
          end
        end

        context 'checksum warnings' do
          before do
            subject.checksum_file = 'test.txt'
            allow_any_instance_of(IngestFolder).to receive(:checksums).and_return({})
            subject.scan
          end

          it 'has missing checksum warnings' do
            expect(subject.errors.size).to eql(7)
            expect(subject.errors.messages[:base])
              .to include(I18n.t('ddr.ingest_folder.checksum_missing',
                                 entry: ::File.join(base_path, sub_path, 'file01001.tif')))
          end
        end

        context 'no included files' do
          let(:error_message) { I18n.t('ddr.ingest_folder.no_ingestable_files', path: subject.sub_path) }

          before { allow(Find).to receive(:find) }

          it 'has no batch objects warning' do
            subject.scan
            expect(subject.errors.messages[:base]).to include(error_message)
          end
        end
      end

      context 'procezz' do
        let(:objects) { {} }
        let(:atts) { {} }
        let(:dss) { {} }
        let(:rels) { {} }
        let(:parent_model) { 'Ddr::Item' }

        before do
          allow_any_instance_of(IngestFolder).to receive(:checksum_file_location).and_return(Rails.root.join('spec/fixtures/batch_ingest/miscellaneous/checksums.txt').to_s)
        end

        context 'batch objects present' do
          describe 'operation' do
            before do
              subject.procezz
              populate_comparison_hashes(user.batches.first.batch_objects)
            end

            it_behaves_like 'a proper set of batch objects'

            it "has an admin_policy relationship with the collection's admin policy" do
              user.batches.first.batch_objects.each do |obj|
                expect(rels.fetch(obj.identifier).fetch('admin_policy').object).to eql(collection.admin_policy_id.id)
              end
            end
          end

          describe 'return type' do
            it 'returns the batch ID' do
              expect(subject.procezz).to eq(user.batches.first.id)
            end
          end
        end

        context 'batch objects not present' do
          before { allow(subject).to receive(:scan_files) }

          it 'does not create a batch' do
            expect { subject.procezz }.not_to(change { Ddr::Batch::Batch.count })
          end

          it 'returns nil' do
            expect(subject.procezz).to be_nil
          end
        end
      end
    end
  end
end
