module Ddr::Batch
  shared_examples 'a valid batch object role object' do
    before { batch_object_role.valid? }

    it 'does not report error' do
      expect(batch_object_role.errors).to be_empty
    end
  end

  shared_examples 'an invalid batch object role object' do
    before { batch_object_role.valid? }

    it 'reports the appropriate error' do
      expect(batch_object_role.errors.attribute_names).to include(error_key)
    end
  end

  RSpec.describe BatchObjectRole, :batch, type: :model do
    describe 'validation' do
      describe 'add operation' do
        let(:batch_object_role) do
          BatchObjectRole.new(batch_object:, operation: BatchObjectRole::OPERATION_ADD,
                              agent: 'test@test.com', role_type: Ddr::Auth::Roles::RoleTypes::EDITOR.title,
                              role_scope:)
        end

        describe 'scope' do
          let(:error_key) { :role_scope }

          describe 'policy' do
            let(:role_scope) { Ddr::Auth::Roles::POLICY_SCOPE }

            describe 'collection batch object' do
              let(:batch_object) { BatchObject.new(model: 'Ddr::Collection') }

              it_behaves_like 'a valid batch object role object'
            end

            describe 'non-collection batch object' do
              let(:batch_object) { BatchObject.new(model: 'TestModel') }

              it_behaves_like 'an invalid batch object role object'
            end
          end
        end
      end
    end
  end
end
