module Ddr::Batch
  describe Batch, :batch, type: :model do
    let(:batch) { FactoryBot.create(:batch_with_basic_ingest_batch_objects) }

    context 'completed count' do
      before { batch.batch_objects.first.update(handled: true) }

      it 'returns the number of handled batch objects' do
        expect(batch.handled_count).to eq(1)
      end
    end

    context 'time to complete' do
      before do
        batch.batch_objects.first.update(handled: true)
        batch.update(start: DateTime.now - 5.minutes)
      end

      it 'estimates the time to complete processing' do
        expect(batch.time_to_complete).to be_within(3).of(600)
      end
    end

    context 'reset!' do
      before do
        batch.batch_objects.first.update(verified: false, handled: true)
        batch.reset!
      end

      it 'resets unverified handled objects to unhandled' do
        expect(batch.handled_count).to eq(0)
      end
    end

    context 'destroy' do
      before do
        batch.user.destroy
        batch.destroy
      end

      it 'destroys all the associated dependent objects' do
        expect(Batch.all).to be_empty
        expect(BatchObject.all).to be_empty
        expect(BatchObjectAttribute.all).to be_empty
        expect(BatchObjectDatastream.all).to be_empty
        expect(BatchObjectRelationship.all).to be_empty
        expect(BatchObjectRole.all).to be_empty
      end
    end

    describe '#deletable?' do
      it 'determines if the batch is deletable' do
        expect(Ddr::Batch::Batch.new).to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_DELETING)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_FINISHED)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_INTERRUPTED)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_INVALID)).to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_PROCESSING)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_QUEUED)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_QUEUED_FOR_DELETION)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_READY)).to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_RESTARTABLE)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_RUNNING)).not_to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_VALIDATED)).to be_deletable
        expect(Ddr::Batch::Batch.new(status: Ddr::Batch::Batch::STATUS_VALIDATING)).not_to be_deletable
      end
    end
  end
end
