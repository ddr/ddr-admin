module Ddr::Batch
  shared_examples 'a valid update object' do
    it 'is valid' do
      expect(object.validate).to be_empty
    end
  end

  shared_examples 'an invalid update object' do
    it 'is not valid' do
      expect(object.validate).to include(error_message)
    end
  end

  shared_examples 'a loggable event has occurred' do
    it 'logs the event' do
      expect(Ddr::Events::UpdateEvent.for_object(repo_object).last.comment)
        .to eq("Updated by batch process (Batch #{object.batch.id}, BatchObject #{object.id})")
    end
  end

  describe UpdateBatchObject, :batch, type: :model do
    let(:object) { batch.batch_objects.first }

    context 'validate', :validation do
      let(:batch) { FactoryBot.create(:batch_with_basic_update_batch_object) }
      let(:role) { FactoryBot.build(:role, :metadata_editor, :resource, agent: batch.user.user_key) }

      context 'valid object' do
        let(:repo_object) { Ddr::Component.new(id: object.resource_id) }

        before do
          repo_object.roles = role
          Ddr.persister.save(resource: repo_object)
        end

        it_behaves_like 'a valid update object'
      end

      context 'invalid object' do
        let(:error_prefix) { "#{object.identifier}:" }

        context 'missing resource_id' do
          let(:error_message) { "#{error_prefix} resource_id required for UPDATE operation" }

          before do
            object.resource_id = nil
            object.save!
          end

          it_behaves_like 'an invalid update object'
        end

        context 'resource_id not found in repository' do
          let(:error_message) { "#{error_prefix} resource_id #{object.resource_id} not found in repository" }

          it_behaves_like 'an invalid update object'
        end

        context 'batch user not permitted to edit repository object' do
          let!(:repo_object) { Ddr.persister.save(resource: Ddr::Component.new(id: object.resource_id)) }
          let(:error_message) { "#{error_prefix} #{batch.user.user_key} not permitted to edit #{object.resource_id}" }

          it_behaves_like 'an invalid update object'
        end
      end
    end

    context 'update' do
      let(:resource) do
        Ddr::Component.new(id: object.resource_id, identifier: %w[id1 id2])
      end
      let(:repo_object) { Ddr.query_service.find_by(id: resource.id) }

      before do
        Ddr.persister.save(resource:)
        batch.user.can :edit, repo_object
      end

      context 'attributes' do
        before { object.process(batch.user) }

        context 'add' do
          let(:batch) { FactoryBot.create(:batch_with_basic_update_batch_object) }

          it_behaves_like 'a loggable event has occurred'

          it 'adds the attribute value to the repository object' do
            expect(Ddr.query_service.find_by(id: repo_object.id).title).to eq(['Test Object Title'])
          end
        end

        context 'clear' do
          let(:batch) { FactoryBot.create(:batch_with_basic_clear_attribute_batch_object) }

          it_behaves_like 'a loggable event has occurred'

          it 'clears the attribute in the repository object' do
            expect(Ddr.query_service.find_by(id: repo_object.id).title).to be_empty
          end
        end
      end

      context 'verifications' do
        let(:batch) { FactoryBot.create(:batch_with_basic_update_batch_object) }

        context 'no verification failure' do
          before { object.process(batch.user) }

          it 'logs an appropriate validation event' do
            validation_events = Ddr::Events::ValidationEvent.for_object(repo_object)
            expect(validation_events).not_to be_empty
            expect(validation_events.first.outcome).to eq(Ddr::Events::Event::SUCCESS)
            expect(validation_events.first.detail).to include('PASS')
            expect(validation_events.first.detail).not_to include('FAIL')
          end
        end

        context 'verification failure' do
          before do
            allow_any_instance_of(described_class).to receive(:verify_repository_object).and_return({ 'Checking' => Ddr::Batch::BatchObject::VERIFICATION_FAIL })
            object.process(batch.user)
          end

          it 'logs an appropriate validation event' do
            validation_events = Ddr::Events::ValidationEvent.for_object(repo_object)
            expect(validation_events).not_to be_empty
            expect(validation_events.first.outcome).to eq(Ddr::Events::Event::FAILURE)
            expect(validation_events.first.detail).to include('FAIL')
          end
        end
      end
    end
  end
end
