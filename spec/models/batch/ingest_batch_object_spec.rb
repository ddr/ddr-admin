module Ddr
  module Batch
    shared_examples 'a valid ingest object' do
      it 'is valid' do
        expect(object.validate).to be_empty
      end
    end

    shared_examples 'an invalid ingest object' do
      it 'is not valid' do
        expect(object.validate).to include(error_message)
      end
    end

    shared_examples 'a successful ingest' do
      let(:repo_object) { Ddr.query_service.find_by(id: Valkyrie::ID.new(object.resource_id)) }
      let(:verification_event) { Ddr::Events::ValidationEvent.for_object(repo_object).first }

      before { object.process(user) }

      it 'results in a verified repository object' do
        expect(object.verified).to be_truthy
        expect(object.resource_id).to eq(assigned_id) if assigned_id.present?
        expect(repo_object.ingested_by).to eq(user.user_key)

        expect(repo_object.title).to eq(['Test Object Title']) if desc_metadata_provided

        unless object.batch_object_roles.empty?
          expect(repo_object.roles.first.role_type).to eq(Ddr::Auth::Roles::RoleTypes::EDITOR.title)
          expect(repo_object.roles.first.agent).to eq('user@test.com')
          expect(repo_object.roles.first.scope).to eq(Ddr::Auth::Roles::RESOURCE_SCOPE)
        end

        unless object.batch_object_attributes.empty?
          expect(verification_event.detail).to include("title attribute set correctly...#{BatchObject::VERIFICATION_PASS}")
        end

        unless object.batch_object_roles.empty?
          expect(verification_event.detail).to include("resource Editor user@test.com role is correct...#{BatchObject::VERIFICATION_PASS}")
        end
      end
    end

    describe IngestBatchObject, :batch, :ingest, type: :model do
      before do
        allow(Ddr::Resource).to receive(:canonical_model_name).with(String).and_call_original
        allow(::File).to receive(:readable?).and_call_original
        allow(object).to receive(:relationship_object_class).and_call_original
      end

      context 'validate' do
        context 'valid object' do
          context 'generic object' do
            let(:object) { create(:generic_ingest_batch_object, :has_batch) }

            it_behaves_like 'a valid ingest object'
          end

          context 'collection object' do
            let(:object) { create(:collection_ingest_batch_object, :has_batch) }

            before { object.model = 'Ddr::Collection' }

            it_behaves_like 'a valid ingest object'
          end

          context 'target object' do
            let(:object) { create(:target_ingest_batch_object, :has_batch) }

            it_behaves_like 'a valid ingest object'
          end

          context 'object related to an uncreated object with pre-assigned resource_id' do
            let(:batch) { create(:batch) }
            let(:object) { create(:generic_ingest_batch_object, batch:) }
            let(:parent) { create(:generic_ingest_batch_object, model: Ddr::Item, batch:) }
            let(:parent_id) { SecureRandom.uuid }
            let(:relationship) do
              BatchObjectRelationship.create(
                name: BatchObjectRelationship::RELATIONSHIP_PARENT,
                object: parent_id,
                object_type: BatchObjectRelationship::OBJECT_TYPE_ID,
                operation: BatchObjectRelationship::OPERATION_ADD
              )
            end

            before do
              object.batch_object_relationships << relationship
              object.save
              parent.resource_id = parent_id
              parent.save
            end

            it_behaves_like 'a valid ingest object'
          end
        end

        context 'invalid object' do
          let(:error_prefix) { "#{object.identifier}:" }

          context 'missing model' do
            let(:object) { create(:ingest_batch_object, :has_batch) }
            let(:error_message) { "#{error_prefix} Model required for INGEST operation" }

            it_behaves_like 'an invalid ingest object'
          end

          context 'invalid model' do
            let(:object) { create(:ingest_batch_object, :has_batch) }
            let(:error_message) { "#{error_prefix} Invalid model name: #{object.model}" }

            before { object.model = 'BadModel' }

            it_behaves_like 'an invalid ingest object'
          end

          context 'collection missing title' do
            let(:object) { create(:ingest_batch_object, :has_batch) }
            let(:error_message) { "#{error_prefix} Ddr::Collection Title can't be blank" }

            before { object.model = 'Ddr::Collection' }

            it_behaves_like 'an invalid ingest object'
          end

          context 'collection missing admin set' do
            let(:object) { create(:ingest_batch_object, :has_batch) }
            let(:error_message) { "#{error_prefix} Ddr::Collection Admin set can't be blank" }

            before { object.model = 'Ddr::Collection' }

            it_behaves_like 'an invalid ingest object'
          end

          context 'invalid admin policy' do
            let(:object) { create(:ingest_batch_object, :has_batch, :has_model) }

            context 'admin policy resource_id object does not exist' do
              let(:admin_policy_id) { 'bogus:AdminPolicy' }
              let(:error_message) do
                "#{error_prefix} admin_policy relationship object does not exist: #{admin_policy_id}"
              end

              before do
                relationship = build(:batch_object_add_relationship, name: 'admin_policy', object: admin_policy_id,
                                                                     object_type: BatchObjectRelationship::OBJECT_TYPE_ID)
                object.batch_object_relationships << relationship
                object.save
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'admin policy resource_id object exists but is not admin policy' do
              let(:error_message) do
                "#{error_prefix} admin_policy relationship object #{@not_admin_policy.id} exists but is not a(n) Ddr::Collection"
              end

              before do
                @not_admin_policy = create_for_repository(:component)
                relationship = build(:batch_object_add_relationship, name: 'admin_policy',
                                                                     object: @not_admin_policy.id,
                                                                     object_type: BatchObjectRelationship::OBJECT_TYPE_ID)
                object.batch_object_relationships << relationship
                object.save
              end

              it_behaves_like 'an invalid ingest object'
            end
          end

          context 'invalid datastreams' do
            let(:object) { create(:ingest_batch_object, :has_batch, :has_model, :with_add_content_datastream) }

            context 'invalid datastream name' do
              let(:error_message) do
                "#{error_prefix} Invalid datastream name for #{object.model}: #{object.batch_object_datastreams.first[:name]}"
              end

              before do
                datastream = object.batch_object_datastreams.first
                datastream.name = 'invalid_name'
                datastream.save!
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'invalid payload type' do
              let(:error_message) do
                "#{error_prefix} Invalid payload type for #{object.batch_object_datastreams.first[:name]} datastream: #{object.batch_object_datastreams.first[:payload_type]}"
              end

              before do
                datastream = object.batch_object_datastreams.first
                datastream.payload_type = 'invalid_type'
                datastream.save!
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'missing data file' do
              let(:error_message) do
                "#{error_prefix} Missing or unreadable file for #{object.batch_object_datastreams.last[:name]} datastream: #{object.batch_object_datastreams.last[:payload]}"
              end

              before do
                datastream = object.batch_object_datastreams.last
                datastream.payload = 'non_existent_file.xml'
                datastream.save!
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'checksum without checksum type' do
              let(:error_message) do
                "#{error_prefix} Must specify checksum type if providing checksum for #{object.batch_object_datastreams.first.name} datastream"
              end

              before do
                datastream = object.batch_object_datastreams.first
                datastream.checksum = '123456'
                datastream.checksum_type = nil
                datastream.save!
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'invalid checksum type' do
              let(:error_message) do
                "#{error_prefix} Invalid checksum type for #{object.batch_object_datastreams.first.name} datastream: #{object.batch_object_datastreams.first.checksum_type}"
              end

              before do
                datastream = object.batch_object_datastreams.first
                datastream.checksum_type = 'SHA-INVALID'
                datastream.save!
              end

              it_behaves_like 'an invalid ingest object'
            end
          end

          context 'invalid parent' do
            let(:object) { create(:ingest_batch_object, :has_batch, :has_model) }

            context 'parent id object does not exist' do
              let(:parent_id) { 'bogus:TestParent' }
              let(:error_message) { "#{error_prefix} parent relationship object does not exist: #{parent_id}" }

              before do
                relationship = build(:batch_object_add_relationship, name: 'parent', object: parent_id,
                                                                     object_type: BatchObjectRelationship::OBJECT_TYPE_ID)
                object.batch_object_relationships << relationship
                object.save
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'parent id object exists but is not correct parent object type' do
              let(:error_message) do
                "#{error_prefix} parent relationship object #{@not_parent.id} exists but is not a(n) Ddr::Item"
              end

              before do
                @not_parent = create_for_repository(:component)
                relationship = build(:batch_object_add_relationship, name: 'parent', object: @not_parent.id,
                                                                     object_type: BatchObjectRelationship::OBJECT_TYPE_ID)
                object.batch_object_relationships << relationship
                object.save
              end

              it_behaves_like 'an invalid ingest object'
            end
          end

          context 'invalid target_for' do
            let(:object) { create(:ingest_batch_object, :has_batch) }

            context 'target_for resource_id object does not exist' do
              let(:collection_id) { 'bogus:Collection' }
              let(:error_message) do
                "#{error_prefix} for_collection relationship object does not exist: #{collection_id}"
              end

              before do
                object.model = 'Ddr::Target'
                relationship = build(:batch_object_add_relationship, name: 'for_collection', object: collection_id,
                                                                     object_type: BatchObjectRelationship::OBJECT_TYPE_ID)
                object.batch_object_relationships << relationship
                object.save
              end

              it_behaves_like 'an invalid ingest object'
            end

            context 'target_for resource_id object exists but is not collection' do
              let(:error_message) do
                "#{error_prefix} for_collection relationship object #{@not_collection.id} exists but is not a(n) Ddr::Collection"
              end

              before do
                @not_collection = create_for_repository(:item)
                object.model = 'Ddr::Target'
                relationship = build(:batch_object_add_relationship, name: 'for_collection',
                                                                     object: @not_collection.id,
                                                                     object_type: BatchObjectRelationship::OBJECT_TYPE_ID)
                object.batch_object_relationships << relationship
                object.save
              end

              it_behaves_like 'an invalid ingest object'
            end
          end
        end
      end # validate

      context 'ingest' do
        let(:user) { create(:user) }

        context 'successful ingest' do
          context 'object without a pre-assigned ID' do
            let(:assigned_id) { nil }

            context 'payload type file' do
              let(:object) { create(:generic_ingest_batch_object, :has_batch) }
              let(:desc_metadata_provided) { false }

              it_behaves_like 'a successful ingest'
            end

            context 'attributes' do
              let(:object) { FactoryBot.create(:generic_ingest_batch_object_with_attributes, :has_batch) }
              let(:desc_metadata_provided) { true }

              it_behaves_like 'a successful ingest'
            end

            context 'roles' do
              let(:object) { FactoryBot.create(:generic_ingest_batch_object_with_roles, :has_batch) }
              let(:desc_metadata_provided) { false }

              it_behaves_like 'a successful ingest'
            end
          end

          context 'object with a pre-assigned resource_id' do
            let(:object) { create(:generic_ingest_batch_object_with_attributes, :has_batch) }
            let(:desc_metadata_provided) { true }
            let(:assigned_id) { SecureRandom.uuid }

            before do
              object.resource_id = assigned_id
              object.save
            end

            it_behaves_like 'a successful ingest'
          end

          context 'previously ingested object (e.g., during restart)' do
            let(:object) { create(:generic_ingest_batch_object_with_attributes, :has_batch) }
            let(:desc_metadata_provided) { true }
            let(:assigned_id) { SecureRandom.uuid }

            before do
              object.resource_id = assigned_id
              object.verified = true
              object.save
              repo_object = object.model.constantize.new(id: assigned_id, title: ['Test Object Title'])
              # repo_object.save(validate: false)
              Ddr.persister.save(resource: repo_object)
            end

            it 'does not (re-)ingest the object' do
              expect(object).not_to receive(:ingest)
              object.process(user)
            end
          end
        end

        context 'exception during ingest' do
          let(:object) { create(:generic_ingest_batch_object_with_attributes, :has_batch) }

          before { allow_any_instance_of(IngestBatchObject).to receive(:populate_datastream).and_raise(RuntimeError) }

          context 'error during processing' do
            it 'logs a fatal message and re-raise the exception' do
              expect(Rails.logger).to receive(:fatal).with(/Error in creating repository object/)
              expect { object.process(user) }.to raise_error(RuntimeError)
            end
          end
        end

        context 'external checksum verification failure' do
          let(:object) { create(:generic_ingest_batch_object_with_attributes, :has_batch) }

          before do
            object.batch_object_datastreams.each do |ds|
              if ds.name == 'content'
                ds.checksum = 'badabcdef0123456789'
                object.save!
              end
            end
          end

          it 'does not result in a verified ingest' do
            object.process(user)
            expect(object.verified).to be_falsey
            Ddr::Events::Event.for_object(object.resource_id).each do |e|
              if e.is_a?(Ddr::Events::ValidationEvent)
                expect(e.outcome).to eq(Ddr::Events::Event::FAILURE)
                expect(e.detail).to include('content external checksum match...FAIL')
              end
            end
          end
        end

        describe 'skipping structure updates during ingest' do
          let(:assigned_id) { nil }
          let(:object) { create(:generic_ingest_batch_object, :has_batch) }

          it 'sets the change set to skip structure updates' do
            expect_any_instance_of(Ddr::ResourceChangeSet).to receive(:skip_structure_updates=).with(true)
            object.process(user)
          end
        end
      end # ingest
    end
  end
end
