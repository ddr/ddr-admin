module Ddr::Batch
  shared_examples 'an invalid batch object attribute object' do
    before { object.valid? }

    it 'shouild report the appropriate error' do
      expect(object.errors.attribute_names).to include(error_key)
    end
  end

  RSpec.describe BatchObjectAttribute, :batch, type: :model do
    describe 'validation' do
      let(:batch_object) { BatchObject.new(model: 'Ddr::Resource') }

      context 'invalid' do
        ### DDRevo BatchObjectAttribute's are no longer concerned about "datastreams"
        # context "datastream" do
        #   let(:error_key) { :datastream }
        #   let(:object) { BatchObjectAttribute.new(batch_object: batch_object, datastream: 'foo', name: 'bar') }
        #   it_should_behave_like 'an invalid batch object attribute object'
        # end
        context 'name' do
          let(:error_key) { :name }
          let(:object) { BatchObjectAttribute.new(batch_object:, name: 'bar') }

          it_behaves_like 'an invalid batch object attribute object'
        end
      end
    end
  end
end
