RSpec.describe User do
  subject { described_class.new(username: 'foo@duke.edu', email: 'foo.bar@duke.edu') }

  its(:to_s) { is_expected.to eq 'foo@duke.edu' }
  its(:agent) { is_expected.to eq 'foo@duke.edu' }
  its(:aspace_username) { is_expected.to eq 'foo.bar' }
  its(:netid) { is_expected.to eq 'foo' }

  describe '.from_omniauth' do
    let(:auth) do
      OmniAuth::AuthHash.new(
        uid: user.username,
        info: {
          email: 'my-new-email@duke.edu',
          display_name: 'Someone Special',
          first_name: 'Someone',
          last_name: 'Special',
          nickname: 'Daisy'
        },
        extra: {
          raw_info: {
            'duDukeID' => '1234'
          }
        }
      )
    end

    describe 'with an existing user' do
      let(:user) { FactoryBot.create(:user) }

      it 'returns the user' do
        retval = described_class.from_omniauth(auth)
        expect(retval).to be_a User
        expect(retval.id).to eq user.id
      end

      it 'updates the user' do
        expect do
          described_class.from_omniauth(auth)
          user.reload
        end.to change(user, :duid).to('1234') &
               change(user, :last_name).to('Special') &
               change(user, :first_name).to('Someone') &
               change(user, :nickname).to('Daisy') &
               change(user, :display_name).to('Someone Special') &
               change(user, :email).to('my-new-email@duke.edu')
      end
    end

    describe 'without an existing user' do
      subject { described_class.from_omniauth(auth) }

      let(:user) { FactoryBot.build(:user) }

      it { is_expected.to be_a(User).and be_persisted }
      its(:duid) { is_expected.to eq '1234' }
      its(:last_name) { is_expected.to eq 'Special' }
      its(:first_name) { is_expected.to eq 'Someone' }
      its(:nickname) { is_expected.to eq 'Daisy' }
      its(:email) { is_expected.to eq 'my-new-email@duke.edu' }
      its(:display_name) { is_expected.to eq 'Someone Special' }
    end
  end
end
