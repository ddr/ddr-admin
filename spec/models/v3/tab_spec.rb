module Ddr::V3
  RSpec.describe Tab do
    let(:tabs) { Tabs.new(resource:, ability:) }
    let(:resource) { create_for_repository(:collection) }
    let(:ability) { FactoryBot.build(:ability) }
    let(:config) { { name: 'cool_tab', title: 'Cool Tab' } }
    let(:action1) { { name: 'cool_action', title: 'Cool Action' } }
    let(:action2) { { name: 'uncool_action', title: 'Uncool Action' } }

    subject { described_class.new(tabs, config) }

    it { is_expected.to be_a(Tab) }
    its(:name) { is_expected.to eq 'cool_tab' }
    its(:title) { is_expected.to eq 'Cool Tab' }
    its(:actions) { is_expected.to be_a(Array) }
  end
end
