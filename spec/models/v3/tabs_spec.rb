module Ddr::V3
  RSpec.describe Tabs do
    let(:ability) { FactoryBot.build(:ability) }
    let(:active) { nil }

    %i[collection item component target attachment].each do |model|
      describe "#{model} tabs" do
        subject { described_class.new(resource:, ability:, active:) }

        let(:resource) { create_for_repository(model) }

        its(:tabs) { is_expected.to be_a(Array) }
        its(:first) { is_expected.to be_a(Tab) }
        its(:active) { is_expected.to eq subject.first }

        describe 'active tab' do
          let(:active) { 'files' }

          its(:active) { is_expected.to eq subject['files'] }
        end
      end
    end
  end
end
