module Ddr::V3
  RSpec.describe TabAction do
    let(:resource) { create_for_repository(:collection) }
    let(:ability) { FactoryBot.build(:ability) }
    let(:tab) { double(resource:, ability:) }
    let(:config) { { name: 'cool_action', title: 'Cool Action' } }
    let(:permission) { 'read' }
    let(:if_cond) { 'can_publish?' }
    let(:unless_cond) { 'published?' }
    let(:action) { TabAction.new(tab, config) }

    subject { action }

    its(:name) { is_expected.to eq 'cool_action' }
    its(:title) { is_expected.to eq 'Cool Action' }

    describe 'authorization' do
      subject { action.can? }

      describe 'permission is nil' do
        describe 'if-condition is absent' do
          describe 'unless-condition is absent' do
            it { is_expected.to be true }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be true }
          end
        end

        describe 'if-condition is true' do
          before do
            config[:if] = if_cond
            allow(action).to receive(if_cond.to_sym).and_return(true)
          end

          describe 'unless-condition is absent' do
            it { is_expected.to be true }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be true }
          end
        end

        describe 'if-condition is false' do
          before do
            config[:if] = if_cond
            allow(action).to receive(if_cond.to_sym).and_return(false)
          end

          describe 'unless-condition is absent' do
            it { is_expected.to be false }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be false }
          end
        end
      end

      describe 'has permission' do
        before do
          config[:permission] = permission
          allow(ability).to receive(:can?).with(permission.to_sym, resource).and_return(true)
        end

        describe 'if-condition is absent' do
          describe 'unless-condition is absent' do
            it { is_expected.to be true }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be true }
          end
        end

        describe 'if-condition is true' do
          before do
            config[:if] = if_cond
            allow(action).to receive(if_cond.to_sym).and_return(true)
          end

          describe 'unless-condition is absent' do
            it { is_expected.to be true }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be true }
          end
        end

        describe 'if-condition is false' do
          before do
            config[:if] = if_cond
            allow(action).to receive(if_cond.to_sym).and_return(false)
          end

          describe 'unless-condition is absent' do
            it { is_expected.to be false }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be false }
          end
        end
      end

      describe 'lacks permission' do
        before do
          config[:permission] = permission
          allow(ability).to receive(:can?).with(permission.to_sym, resource).and_return(false)
        end

        describe 'if-condition is absent' do
          describe 'unless-condition is absent' do
            it { is_expected.to be false }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be false }
          end
        end

        describe 'if-condition is true' do
          before do
            config[:if] = if_cond
            allow(action).to receive(if_cond.to_sym).and_return(true)
          end

          describe 'unless-condition is absent' do
            it { is_expected.to be false }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be false }
          end
        end

        describe 'if-condition is false' do
          before do
            config[:if] = if_cond
            allow(action).to receive(if_cond.to_sym).and_return(false)
          end

          describe 'unless-condition is absent' do
            it { is_expected.to be false }
          end

          describe 'unless-condition is true' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(true)
            end

            it { is_expected.to be false }
          end

          describe 'unless-condition is false' do
            before do
              config[:unless] = unless_cond
              allow(action).to receive(unless_cond.to_sym).and_return(false)
            end

            it { is_expected.to be false }
          end
        end
      end
    end
  end
end
