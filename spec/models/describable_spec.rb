module Ddr
  RSpec.describe Describable do
    describe '.term_names' do
      subject { described_class.term_names }

      it { is_expected.to eq(Ddr::Metadata.descriptive.keys) }
    end
  end
end
