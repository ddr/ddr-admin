module Ddr
  module Structures
    RSpec.describe Agent, :structure, type: :model do
      let(:attrs) do
        { id: 'abc', role: 'OTHER', otherrole: 'INVESTIGATOR', type: 'OTHER', othertype: 'SOMETHING',
          name: 'Sam Spade' }
      end
      let(:doc) { Ddr::Structure.xml_template }
      let(:node) do
        node = Nokogiri::XML::Node.new('agent', doc)
        node['ID'] = attrs[:id]
        node['ROLE'] = attrs[:role]
        node['OTHERROLE'] = attrs[:otherrole]
        node['TYPE'] = attrs[:type]
        node['OTHERTYPE'] = attrs[:othertype]
        doc.root.add_child(node)
        name_node = Nokogiri::XML::Node.new('name', doc)
        name_node.content = 'Sam Spade'
        node.add_child(name_node)
        node
      end

      describe '.build' do
        it 'builds the correct node' do
          expect(described_class.build(attrs.merge(document: doc)).to_xml).to be_equivalent_to(node.to_xml)
        end
      end
    end
  end
end
