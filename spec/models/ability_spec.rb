require 'rails_helper'
require 'cancan/matchers'

describe Ability, :abilities, type: :model do
  subject { described_class.new(auth_context) }

  let(:auth_context) { FactoryBot.build(:auth_context) }

  describe 'aliases' do
    it 'aliases actions to :read' do
      expect(subject.aliased_actions[:read])
        .to include(:attachments, :components, :event, :events, :files, :items, :targets)
    end

    it 'aliases actions to :grant' do
      expect(subject.aliased_actions[:grant]).to include(:roles)
    end

    it 'aliases actions to :update' do
      expect(subject.aliased_actions[:update]).to include(:admin_metadata)
    end
  end

  describe 'Collection abilities' do
    describe 'create' do
      describe 'when the user is a curator' do
        before do
          allow(auth_context).to receive(:curator?).and_return(true)
        end

        it { is_expected.to be_able_to(:create, Ddr::Collection) }
      end

      describe 'when the user is not a curator' do
        before do
          allow(auth_context).to receive(:curator?).and_return(false)
        end

        it { is_expected.not_to be_able_to(:create, Ddr::Collection) }
      end
    end

    describe 'export' do
      let(:collection) { create_for_repository(:collection, admin_set: 'dc') }

      before do
        collection.roles = [role]
      end

      describe 'when the user has read permission via policy scope role' do
        let(:role) { FactoryBot.build(:role, :viewer, :policy, agent: auth_context.user) }

        it { is_expected.to be_able_to(:export, collection) }
      end

      describe 'when the user does not have read permission via policy scope role' do
        let(:role) { FactoryBot.build(:role, :viewer, :resource, agent: auth_context.user) }

        it { is_expected.not_to be_able_to(:export, collection) }
      end
    end
  end

  describe 'AdminSet abilities' do
    let(:admin_set) { Ddr::AdminSet.find_by_code('dc') }

    describe 'when the user is a curator' do
      before { allow(auth_context).to receive(:curator?).and_return(true) }

      it { is_expected.to be_able_to(:export, admin_set) }
    end

    describe 'when the user is not a curator' do
      before { allow(auth_context).to receive(:curator?).and_return(false) }

      it { is_expected.not_to be_able_to(:export, admin_set) }
    end
  end

  describe 'Event abilities' do
    let(:resource) { Valkyrie.config.metadata_adapter.persister.save(resource: Ddr::Item.new) }
    let(:event) { Ddr::Events::Event.new(resource_id: resource.id) }

    describe 'can read object of the event' do
      before { subject.can :read, event.resource_id }

      it { is_expected.to be_able_to(:read, event) }
    end

    describe 'cannot read object of the event' do
      before { subject.cannot :read, event.resource_id }

      it { is_expected.not_to be_able_to(:read, event) }
    end
  end

  describe 'MetadataFile abilities' do
    describe 'create' do
      describe 'when the user is a curator' do
        before { allow(auth_context).to receive(:curator?).and_return(true) }

        it { is_expected.to be_able_to(:create, Ddr::MetadataFile) }
      end

      describe 'when the user is not a curator' do
        before { allow(auth_context).to receive(:curator?).and_return(false) }

        it { is_expected.not_to be_able_to(:create, Ddr::MetadataFile) }
      end
    end

    describe 'show' do
      let(:resource) { FactoryBot.create(:metadata_file_csv) }

      describe 'when the user is the creator of the MetadataFile' do
        before { allow(auth_context).to receive(:user) { resource.user } }

        it { is_expected.to be_able_to(:show, resource) }
      end

      describe 'when the user is not the creator of the MetadataFile' do
        it { is_expected.not_to be_able_to(:show, resource) }
      end
    end

    describe 'procezz' do
      let(:resource) { FactoryBot.create(:metadata_file_csv) }

      describe 'when the user is the creator of the MetadataFile' do
        before { allow(auth_context).to receive(:user) { resource.user } }

        it { is_expected.to be_able_to(:procezz, resource) }
      end

      describe 'when the user is not the creator of the MetadataFile' do
        it { is_expected.not_to be_able_to(:procezz, resource) }
      end
    end
  end

  describe 'StandardIngest abilities' do
    let(:resource) do
      Ddr::StandardIngest.new({ 'basepath' => '/foo', 'batch_user' => auth_context.user.user_key, subpath: 'bar' })
    end

    before do
      auth_context.user.save!
      allow_any_instance_of(Ddr::StandardIngest).to receive(:load_configuration).and_return({})
    end

    describe 'create' do
      before { allow_any_instance_of(described_class).to receive(:can?).and_call_original }

      describe 'when the user can create collections' do
        before do
          allow_any_instance_of(described_class).to receive(:can?).with(:create, Ddr::Collection).and_return(true)
        end

        it { is_expected.to be_able_to(:create, resource) }
      end

      describe 'when the user cannot create collections' do
        before do
          allow_any_instance_of(described_class).to receive(:can?).with(:create, Ddr::Collection).and_return(false)
        end

        it { is_expected.not_to be_able_to(:create, resource) }
      end
    end

    describe 'show' do
      describe 'when the user is the creator of the StandardIngest' do
        it { is_expected.to be_able_to(:show, resource) }
      end

      describe 'when the user is not the creator of the StandardIngest' do
        before { resource.user = FactoryBot.build(:user) }

        it { is_expected.not_to be_able_to(:show, resource) }
      end
    end
  end

  describe '#curator?' do
    before do
      allow(auth_context).to receive(:member_of?).and_return(false)
    end

    describe 'when user is a member of the curators group' do
      before do
        allow(auth_context).to receive(:member_of?).with(Ddr::Admin.curators_group).and_return(true)
      end

      it { is_expected.to be_curator }
    end

    describe 'when the user is not a member of the curators group' do
      before do
        allow(auth_context).to receive(:member_of?).with(Ddr::Admin.curators_group).and_return(false)
      end

      it { is_expected.not_to be_curator }
    end
  end
end
