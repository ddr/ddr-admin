module Ddr::Types
  RSpec.shared_examples 'a DDR type' do
    describe 'when it is optional' do
      subject { described_class.optional[value] }

      describe 'with nil' do
        let(:value) { nil }

        it { is_expected.to be_nil }
      end
    end
  end

  RSpec.shared_examples 'a DDR CSV type' do
    subject { described_class[value] }

    describe 'with an array' do
      let(:value) { %w[foo bar baz] }

      it { is_expected.to eq 'foo,bar,baz' }
    end

    describe 'with a string' do
      let(:value) { 'foo,bar,baz' }

      it { is_expected.to eq 'foo,bar,baz' }
    end

    describe 'with mixed content' do
      let(:value) { [['foo'], 'bar,baz'] }

      it { is_expected.to eq 'foo,bar,baz' }
    end
  end

  RSpec.describe FileField do
    it_behaves_like 'a DDR type'

    it 'coerces a string' do
      expect(described_class['content']).to eq :content
    end

    it 'returns a string' do
      expect(described_class[:content]).to eq :content
    end

    it 'raises an exception on invalid input' do
      expect { described_class['foobar'] }.to raise_error(Dry::Types::ConstraintError)
    end
  end

  RSpec.describe FileLike do
    it_behaves_like 'a DDR type'

    describe 'responds to :path and :size' do
      let(:value) { double(path: 'foo', size: 46) }

      it 'returns the input' do
        expect(described_class[value]).to eq value
      end
    end

    describe 'does not respond to :path' do
      let(:value) { double }

      it 'raises and exception' do
        expect { described_class[value] }.to raise_error(Dry::Types::ConstraintError)
      end
    end
  end

  RSpec.describe DigestType do
    it_behaves_like 'a DDR type'

    it 'returns a digest type given a string' do
      expect(described_class['sha-1']).to eq 'SHA1'
    end

    it 'returns a digest type given a symbol' do
      expect(described_class[:md5]).to eq 'MD5'
    end

    it 'raises an exception given an invalid value' do
      expect { described_class['foobar'] }.to raise_error(Dry::Types::ConstraintError)
    end
  end

  RSpec.describe MediaType do
    it_behaves_like 'a DDR type'

    it 'returns a media type' do
      expect(described_class['text/plain']).to eq 'text/plain'
    end

    it 'returns a fallback' do
      expect(described_class['text/foobar']).to eq 'application/octet-stream'
    end
  end

  RSpec.describe FilePath do
    let(:real_path) { Rails.root.join('spec/fixtures/files/imageC.tif').to_s }

    it_behaves_like 'a DDR type'

    describe 'nil' do
      it 'raises an exception' do
        expect { described_class[nil] }.to raise_error(Dry::Types::CoercionError)
      end
    end

    describe 'fake file path' do
      it 'returns the path' do
        expect(described_class['foo/bar/zyzzyx']).to eq 'foo/bar/zyzzyx'
      end
    end

    describe 'responds to #path' do
      it 'returns the path' do
        expect(described_class[double(path: real_path)]).to eq real_path
      end
    end

    describe 'responds to #to_path' do
      it 'returns the path' do
        expect(described_class[double(to_path: real_path)]).to eq real_path
      end
    end

    describe 'real file path' do
      it 'returns the path' do
        expect(described_class[real_path]).to eq real_path
      end
    end
  end

  RSpec.describe FileMagic do
    let(:real_path) { Rails.root.join('spec/fixtures/files/imageC.tif') }

    it_behaves_like 'a DDR type'

    describe 'nil' do
      it 'raises an exception' do
        expect { described_class[nil] }.to raise_error(Dry::Types::CoercionError)
      end
    end

    describe 'fake file path' do
      it 'raises an exception' do
        expect { described_class['foo/bar/zyzzyx'] }.to raise_error(Dry::Types::CoercionError)
      end
    end

    describe 'responds to :path' do
      it 'returns the media type' do
        expect(described_class[double(path: real_path)]).to eq 'image/tiff'
      end
    end

    describe 'responds to :to_path' do
      it 'returns the media type' do
        expect(described_class[double(to_path: real_path)]).to eq 'image/tiff'
      end
    end

    describe 'real file path' do
      it 'returns the media type' do
        expect(described_class[real_path]).to eq 'image/tiff'
      end
    end
  end

  RSpec.describe OriginalFilename do
    let(:real_path) { Rails.root.join('spec/fixtures/files/imageC.tif').to_s }

    it_behaves_like 'a DDR type'

    describe 'nil' do
      it 'raises an exception' do
        expect { described_class[nil] }.to raise_error(Dry::Types::CoercionError)
      end
    end

    describe 'fake file path' do
      it 'returns the file name' do
        expect(described_class['foo/bar.jpg']).to eq 'bar.jpg'
      end
    end

    describe 'real file path' do
      it 'returns the file name' do
        expect(described_class[real_path]).to eq 'imageC.tif'
      end
    end

    describe 'responds to :original_filename' do
      it 'returns the file name' do
        expect(described_class[double(original_filename: real_path)]).to eq 'imageC.tif'
      end
    end
  end

  RSpec.describe SanitizedString do
    it_behaves_like 'a DDR type'

    subject { described_class[value] }

    describe 'with a space-padded string' do
      let(:value) { ' padded ' }

      it { is_expected.to eq 'padded' }
    end

    describe 'with a string containing control characters' do
      let(:value) { " foo\x00\rbar " }

      it { is_expected.to eq 'foobar' }
    end
  end

  RSpec.describe CommaSeparatedValues do
    it_behaves_like 'a DDR type'
    it_behaves_like 'a DDR CSV type'
  end

  RSpec.describe UniqueCommaSeparatedValues do
    it_behaves_like 'a DDR type'
    it_behaves_like 'a DDR CSV type'

    subject { described_class[value] }

    describe 'with duplicate values' do
      let(:value) { %w[foo foo bar baz baz baz] }

      it { is_expected.to eq 'foo,bar,baz' }
    end
  end

  RSpec.describe EmailAddress do
    it_behaves_like 'a DDR type'

    subject { described_class[value] }

    describe 'with a valid email' do
      let(:value) { 'noreply@example.com' }

      it { is_expected.to eq 'noreply@example.com' }
    end

    describe 'with an invalid email' do
      it 'raises an exception' do
        expect { described_class['foobar'] }.to raise_error(Dry::Types::ConstraintError)
      end
    end
  end

  RSpec.describe ResourceID do
    it_behaves_like 'a DDR type'

    subject { described_class[value] }

    describe 'with a new resource' do
      it 'raises an exception' do
        expect { described_class[Ddr::Item.new] }.to raise_error(Dry::Types::ConstraintError)
      end
    end

    describe 'with a persisted resource' do
      let(:value) { create_for_repository(:item) }

      it { is_expected.to eq value.id_s }
    end

    describe 'with a Valkyrie ID' do
      let(:value) { create_for_repository(:item).id }

      it { is_expected.to eq value.id }
    end

    describe 'with a resource ID string' do
      let(:value) { create_for_repository(:item).id_s }

      it { is_expected.to eq value }
    end

    describe 'with a UUID' do
      let(:value) { SecureRandom.uuid }

      it { is_expected.to eq value }
    end

    describe 'with an invalid ID' do
      it 'raises an exception' do
        expect { described_class['foobar'] }.to raise_error(Dry::Types::ConstraintError)
      end
    end
  end

  RSpec.describe Resource do
    it_behaves_like 'a DDR type'

    subject { described_class[value] }

    describe 'with a new resource' do
      let(:value) { Ddr::Item.new }

      it { is_expected.to eq value }
    end

    describe 'with a persisted resource' do
      let(:value) { create_for_repository(:item) }

      it { is_expected.to eq value }
    end

    describe 'with a Valkyrie ID' do
      let(:value) { create_for_repository(:item).id }

      it { is_expected.to eq Ddr.query_service.find_by(id: value) }
    end

    describe 'with a resource ID string' do
      let(:value) { create_for_repository(:item).id_s }

      it { is_expected.to eq Ddr.query_service.find_by(id: value) }
    end

    describe 'with a UUID, not a resource ID' do
      it 'raises an exception' do
        expect { described_class[SecureRandom.uuid] }.to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
      end
    end
  end

  RSpec.describe URI do
    it_behaves_like 'a DDR type'

    subject { described_class[value] }

    describe 'with a ::URI instance' do
      let(:value) { URI('https://example.com') }

      it { is_expected.to eq 'https://example.com' }
    end

    describe 'with an "http" URL string' do
      let(:value) { 'http://example.com' }

      it { is_expected.to eq 'http://example.com' }
    end

    describe 'with an "https" URL string' do
      let(:value) { 'https://example.com' }

      it { is_expected.to eq 'https://example.com' }
    end

    describe 'with an invalid URI' do
      it 'raises an exception' do
        expect { described_class['foobar'] }.to raise_error(Dry::Types::ConstraintError)
      end
    end
  end
end
