require 'support/shared_examples_for_events'

module Ddr
  module Events
    RSpec.describe EventFactory, :events do
      describe '.call' do
        describe 'with valid values' do
          it 'returns an event instance for the event type' do
            expect(described_class.call(EventTypes::DELETION)).to be_a DeletionEvent
            expect(described_class.call(EventTypes::FIXITY_CHECK)).to be_a FixityCheckEvent
            expect(described_class.call(EventTypes::INGESTION)).to be_a IngestionEvent
            expect(described_class.call(EventTypes::MIGRATION)).to be_a MigrationEvent
            expect(described_class.call(EventTypes::MODIFICATION)).to be_a UpdateEvent
            expect(described_class.call(EventTypes::VALIDATION)).to be_a ValidationEvent
            expect(described_class.call(EventTypes::VIRUS_CHECK)).to be_a VirusCheckEvent
          end
        end

        describe 'with a nil value' do
          it 'returns an Event instance' do
            expect(described_class.call(nil)).to be_a Event
          end
        end

        describe 'with an invalid value' do
          it 'raises an exception' do
            expect { described_class.call('foo') }.to raise_error(ArgumentError)
          end
        end
      end
    end
  end

  module Events
    RSpec.describe Event, :events, type: :model do
      it_behaves_like 'an event'

      describe '.for_resource_id' do
        describe 'resource id is nil' do
          specify { expect(described_class.for_resource_id(nil)).to be_empty }
        end

        describe 'resource id is string' do
          let(:resource_id) { SecureRandom.uuid }
          let!(:event) { described_class.create(resource_id:) }

          specify { expect(described_class.for_resource_id(resource_id)).to contain_exactly(event) }
        end

        describe 'resource id is Valkyrie::ID' do
          let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }
          let!(:event) { described_class.create(resource_id: resource_id.id) }

          specify { expect(described_class.for_resource_id(resource_id)).to contain_exactly(event) }
        end
      end
    end

    RSpec.describe UpdateEvent, :events, type: :model do
      it_behaves_like 'an event'
      its(:display_type) { is_expected.to eq 'Update' }
      its(:event_type) { is_expected.to eq 'modification' }
      its(:preservation_event_type) { is_expected.to eq 'modification' }
      its(:retain_for_deleted_resources) { is_expected.to be false }
    end

    RSpec.describe FixityCheckEvent, :events, type: :model do
      it_behaves_like 'an event'
      it_behaves_like 'a preservation-related event'
      it_behaves_like 'an event that reindexes its object after save'
      its(:display_type) { is_expected.to eq 'Fixity Check' }
      its(:event_type) { is_expected.to eq 'fixity check' }
      its(:preservation_event_type) { is_expected.to eq 'fixity check' }
      its(:retain_for_deleted_resources) { is_expected.to be false }

      describe 'pruning' do
        let(:resource) { create_for_repository(:component, :with_content_file) }
      end
    end

    RSpec.describe VirusCheckEvent, :events, type: :model do
      it_behaves_like 'an event'
      it_behaves_like 'a preservation-related event'
      it_behaves_like 'an event that reindexes its object after save'
      its(:display_type) { is_expected.to eq 'Virus Check' }
      its(:event_type) { is_expected.to eq 'virus check' }
      its(:preservation_event_type) { is_expected.to eq 'virus check' }
      its(:retain_for_deleted_resources) { is_expected.to be false }
    end

    RSpec.describe IngestionEvent, :events, type: :model do
      it_behaves_like 'an event'
      it_behaves_like 'a preservation-related event'
      its(:display_type) { is_expected.to eq 'Ingestion' }
      its(:event_type) { is_expected.to eq 'ingestion' }
      its(:preservation_event_type) { is_expected.to eq 'ingestion' }
      its(:retain_for_deleted_resources) { is_expected.to be true }
    end

    RSpec.describe ValidationEvent, :events, type: :model do
      it_behaves_like 'an event'
      it_behaves_like 'a preservation-related event'
      its(:display_type) { is_expected.to eq 'Validation' }
      its(:event_type) { is_expected.to eq 'validation' }
      its(:preservation_event_type) { is_expected.to eq 'validation' }
      its(:retain_for_deleted_resources) { is_expected.to be false }
    end

    RSpec.describe DeletionEvent, :events, type: :model do
      it_behaves_like 'an event'
      it_behaves_like 'a preservation-related event'
      its(:display_type) { is_expected.to eq 'Deletion' }
      its(:event_type) { is_expected.to eq 'deletion' }
      its(:preservation_event_type) { is_expected.to eq 'deletion' }
      its(:retain_for_deleted_resources) { is_expected.to be true }
    end

    RSpec.describe MigrationEvent, :events, type: :model do
      it_behaves_like 'an event'
      it_behaves_like 'a preservation-related event'
      its(:display_type) { is_expected.to eq 'Migration' }
      its(:event_type) { is_expected.to eq 'migration' }
      its(:preservation_event_type) { is_expected.to eq 'migration' }
      its(:retain_for_deleted_resources) { is_expected.to be false }
    end
  end
end
