module Ddr
  RSpec.describe File do
    let(:resource) { Ddr::Resource.new }

    let(:source_file) { fixture_file_upload('imageA.tif', 'image/tiff') }
    let(:source_file_size) { ::File.size(source_file.path) }

    let(:stored_file) do
      Ddr.storage_adapter.upload(file: source_file, resource:, original_filename: 'imageA.tif')
    end

    subject { described_class.new(file_identifier: stored_file.id) }

    it { is_expected.not_to be_new_record }
    it { is_expected.to be_persisted }
    it { is_expected.to be_stored }

    describe 'when new' do
      subject { described_class.new }

      it { is_expected.to be_new_record }
      it { is_expected.not_to be_persisted }
      it { is_expected.not_to be_stored }
      its(:file) { is_expected.to be_nil }
      its(:file_path) { is_expected.to be_nil }
      its(:file_size) { is_expected.to be_nil }
    end

    describe '#content' do
      it 'returns the contents of the file' do
        expect(::Digest::SHA1.new.hexdigest(subject.content))
          .to eq(::Digest::SHA1.new.hexdigest(::File.read(source_file.path)))
      end
    end

    describe '#file' do
      it 'returns a Valkyrie stored file' do
        expect(subject.file).to be_a(Valkyrie::StorageAdapter::File)
      end
    end

    describe '#timestamps' do
      describe 'created' do
        subject { file.timestamps.created }

        describe 'when #created_at is not nil' do
          let(:file) { described_class.new(created_at: DateTime.now.utc, file_identifier: stored_file.id) }

          it { is_expected.to eq file.created_at }
        end

        describe 'when #created_at is nil' do
          let(:file) { described_class.new(created_at: nil, file_identifier: stored_file.id) }

          it { is_expected.to eq file.file_created_at }
        end
      end

      describe 'updated' do
        subject { file.timestamps.updated }

        describe 'when #updated_at is not nil' do
          let(:file) { described_class.new(updated_at: DateTime.now.utc, file_identifier: stored_file.id) }

          it { is_expected.to eq file.updated_at }
        end

        describe 'when #updated_at is nil' do
          let(:file) { described_class.new(updated_at: nil, file_identifier: stored_file.id) }

          it { is_expected.to eq file.file_updated_at }
        end
      end
    end

    describe '#file_created_at' do
      # NOTE: Disk adapter-specific
      let(:file_ctime) { Time.new(2019, 5, 23, 13, 38, 28) }

      before do
        allow(::File).to receive(:ctime) { file_ctime }
      end

      it 'returns the creation time of the file on disk' do
        expect(subject.file_created_at).to eq(file_ctime.utc.to_datetime)
      end
    end

    describe '#file_updated_at' do
      # NOTE: Disk adapter-specific
      let(:file_mtime) { Time.new(2023, 5, 23, 13, 38, 28) }

      before do
        allow(::File).to receive(:mtime) { file_mtime }
      end

      it 'returns the modification time of the file on disk' do
        expect(subject.file_updated_at).to eq(file_mtime.utc.to_datetime)
      end
    end

    describe '#file_path' do
      # NOTE: Disk adapter-specific
      let(:expected_path) { stored_file.id.id.gsub('disk://', '') }

      it 'returns the full path to the stored file' do
        expect(subject.file_path).to eq(expected_path)
      end
    end

    describe '#file_size' do
      specify { expect(subject.file_size).to eq(source_file_size) }
    end

    describe '#sha1' do
      let(:digest) do
        Ddr::Digest.new(type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                        value: '75e2e0cec6e807f6ae63610d46448f777591dd6b')
      end

      before do
        subject.digest = digest
      end

      its(:sha1) { is_expected.to eq '75e2e0cec6e807f6ae63610d46448f777591dd6b' }
    end

    describe '#validate_checksum!' do
      subject { build(:ddr_file) }
      let(:checksum_type) { Ddr::Files::CHECKSUM_TYPE_SHA1 }

      describe 'file has not been stored' do
        subject { build(:empty_ddr_file) }
        let(:checksum_value) { 'SOME CHECKSUM VALUE' }

        it 'throws an error' do
          expect { subject.validate_checksum!(checksum_value, checksum_type) }
            .to raise_error(Ddr::Error, I18n.t('ddr.checksum.validation.must_be_stored'))
        end
      end

      describe 'stored checksum validation' do
        describe 'fails' do
          let(:checksum_value) { 'SOME CHECKSUM VALUE' }

          before do
            allow(subject).to receive(:stored_checksums_valid?).and_return(false)
          end

          it 'throws an error' do
            expect { subject.validate_checksum!(checksum_value, checksum_type) }
              .to raise_error(Ddr::ChecksumInvalid, I18n.t('ddr.checksum.validation.internal_check_failed'))
          end
        end
      end

      describe 'provided checksum validation' do
        before do
          allow(subject).to receive(:stored_checksums_valid?).and_return(true)
        end

        describe 'checksum matches' do
          let(:checksum_value) do
            subject.digest.select { |digest| digest.type == Ddr::Files::CHECKSUM_TYPE_SHA1 }.first.value
          end

          it 'returns a valid checksum message' do
            expect(subject.validate_checksum!(checksum_value, checksum_type))
              .to eq(I18n.t('ddr.checksum.validation.valid', type: checksum_type, value: checksum_value))
          end
        end

        describe 'checksum does not match' do
          let(:checksum_value) { 'MISMATCHED CHECKSUM' }

          it 'raises an error' do
            expect { subject.validate_checksum!(checksum_value, checksum_type) }
              .to raise_error(Ddr::ChecksumInvalid,
                              I18n.t('ddr.checksum.validation.invalid', type: checksum_type, value: checksum_value))
          rescue Ddr::ChecksumInvalid
          end
        end
      end
    end

    describe '#stored_checksums_valid?' do
      subject { build(:ddr_file) }
      describe 'checksum matches' do
        it 'returns true' do
          expect(subject.stored_checksums_valid?).to be true
        end
      end

      describe 'checksum does not match' do
        before do
          allow(subject).to receive(:digest) {
                              [Ddr::Digest.new(type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                                               value: 'MISMATCHED_CHECKSUM')]
                            }
        end

        it 'returns false' do
          expect(subject.stored_checksums_valid?).to be false
        end
      end
    end
  end
end
