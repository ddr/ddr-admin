module Ddr
  describe IngestMetadata, :batch, type: :model do
    let(:metadata_profile) do
      { csv: {
        encoding: 'UTF-8',
        headers: true,
        header_converters: :canonicalize
      } }
    end

    describe 'initialization' do
      context 'header validation' do
        context 'no invalid headers' do
          let(:metadata_filepath) { Rails.root.join('spec/fixtures/batch_ingest/ingest-metadata.txt') }

          it 'does not raise an exception' do
            expect { described_class.new(metadata_filepath, metadata_profile) }.not_to raise_error
          end
        end

        context 'invalid headers' do
          let(:metadata_filepath) do
            Rails.root.join('spec/fixtures/batch_ingest/bad-headers-ingest-metadata.txt')
          end

          it 'raises an exception' do
            expect do
              described_class.new(metadata_filepath, metadata_profile)
            end.to raise_error(ArgumentError, /bad, alsoBad/)
          end
        end
      end
    end

    describe 'metadata' do
      let(:metadata_filepath) { Rails.root.join('spec/fixtures/batch_ingest/standard_ingest/metadata.txt') }
      let(:sim) { described_class.new(metadata_filepath, metadata_profile) }
      let(:expected_metadata) do
        {
          nil => { 'identifier' => ['test'],
                   'title' => ['Top Title'],
                   'description' => ['Top Description'],
                   'creator' => ['Spade, Sam'],
                   'local_id' => ['spade'],
                   'setting' => ['Europe'] },
          'foo' => { 'identifier' => ['test123'],
                     'title' => ['My Title'],
                     'description' => ['Description'],
                     'subject' => %w[Alpha Beta],
                     'dateSubmitted' => ['2014-02-03'],
                     'creator' => ['Jane Smith'],
                     'local_id' => ['spade001'],
                     'setting' => ['Great Britain'] },
          'foo/bar.doc' => { 'identifier' => ['test12345'],
                             'title' => ['Updated Title'],
                             'description' => ['This is some description; this is "some more" description.'],
                             'subject' => %w[Alpha Beta Gamma Delta Epsilon],
                             'contributor' => ['Jane Doe'],
                             'dateSubmitted' => ['2010-01-22'],
                             'creator' => ['John Doe'],
                             'local_id' => ['spade001001'] }
        }
      end

      it 'returns the correct metadata for a locator' do
        expect(sim.metadata('foo')).to eql(expected_metadata['foo'])
        expect(sim.metadata('foo/bar.doc')).to eql(expected_metadata['foo/bar.doc'])
        expect(sim.metadata('foo/not.doc')).to be_empty
      end
    end

    describe 'locators' do
      let(:metadata_filepath) { Rails.root.join('spec/fixtures/batch_ingest/standard_ingest/metadata.txt') }
      let(:sim) { described_class.new(metadata_filepath, metadata_profile) }

      it 'returns the locators' do
        expect(sim.locators).to contain_exactly(nil, 'foo', 'foo/bar.doc')
      end
    end
  end
end
