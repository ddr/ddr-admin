module Ddr
  RSpec.describe DefaultRoles do
    specify do
      expect(described_class['dc']).to all(be_a(Ddr::Auth::Roles::Role))
    end

    specify do
      expect(described_class[:dc]).to all(be_a(Ddr::Auth::Roles::Role))
    end

    specify do
      expect(described_class['rubenstein']).to all(be_a(Ddr::Auth::Roles::Role))
    end

    specify do
      expect(described_class[:rubenstein]).to all(be_a(Ddr::Auth::Roles::Role))
    end

    specify do
      expect(described_class['foo']).to be_empty
    end
  end
end
