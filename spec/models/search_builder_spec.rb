RSpec.describe SearchBuilder do
  let(:user) { create(:user) }
  let(:auth_context) { Ddr::Auth::AuthContextFactory.call(user) }
  let(:config) { CatalogController.blacklight_config }
  let(:scope) do
    double('The scope',
           blacklight_config: config,
           context: { current_ability: Ability.new(auth_context),
                      current_user: user })
  end
  let(:builder) { described_class.new(scope) }

  describe 'gated discovery' do
    subject { builder.query }

    let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }

    before do
      policy_role = FactoryBot.build(:role, :viewer, :policy, agent: user.user_key)
      collection = Ddr::Collection.new(id: valkyrie_id)
      collection.admin_policy_id = valkyrie_id
      collection.roles += Array(policy_role)
      Ddr.persister.save(resource: collection)
    end

    describe 'superuser' do
      before { allow(auth_context).to receive(:superuser?).and_return(true) }

      it 'has an empty query filter' do
        expect(subject[:fq]).to eq([''])
      end
    end

    describe 'not superuser' do
      it 'has an appropriate non-empty query filter' do
        expect(subject[:fq]).to include(/public/)
        expect(subject[:fq]).to include(/registered/)
        expect(subject[:fq]).to include(/#{user.user_key}/)
      end
    end
  end
end
