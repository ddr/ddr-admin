module Ddr
  RSpec.describe AdminSet, :admin_set, :aux do
    describe '.all' do
      subject { described_class.all }

      it { is_expected.to be_a Array }
      it { is_expected.to all(be_a(described_class)) }
    end

    describe '.call' do
      let(:obj) { Item.new }

      subject { described_class.call(obj) }

      describe 'when the object has an admin set' do
        before { obj.admin_set = 'dvs' }

        it { is_expected.to be_a described_class }
      end

      describe 'when the object does not have an admin set' do
        it { is_expected.to be_nil }
      end
    end

    describe '.keys' do
      subject { described_class.keys }

      it {
        expect(subject).to contain_exactly('dul_collections', 'duson', 'ebooks', 'nescent', 'dc', 'dvs', 'rubenstein',
                                           'duke_scholarship', 'researchdata')
      }
    end

    describe 'instance methods' do
      subject { described_class.keystore.fetch('dvs') }

      its(:to_s) { is_expected.to eq('Data and Visualization Services') }
    end
  end
end
