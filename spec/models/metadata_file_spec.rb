shared_examples 'an invalid metadata file' do
  it 'is not valid' do
    expect(metadata_file).not_to be_valid
    expect(metadata_file.errors).to have_key(error_field)
  end
end

shared_examples 'a successful metadata file processing' do
  it 'creates a batch with an appropriate UpdateBatchObject' do
    expect(@batch.status).to eq(Ddr::Batch::Batch::STATUS_READY)
    expect(@batch.batch_objects.count).to eq(1)
    expect(@batch_object).to be_a(Ddr::Batch::UpdateBatchObject)
    expect(@attributes.size).to eq(20)
    # Attribute 'clear' entries
    clears = @attributes.select { |att| att.operation == Ddr::Batch::BatchObjectAttribute::OPERATION_CLEAR }
    expect(clears.map { |att| att.name }).to match_array(%w[local_id ead_id nested_path title description
                                                            subject dateSubmitted rights])
    # Attribute 'add' entries
    adds = @attributes.select { |att| att.operation == Ddr::Batch::BatchObjectAttribute::OPERATION_ADD }
    expect(adds.map { |att| att.name }).to match_array(%w[local_id ead_id nested_path title description
                                                          subject subject subject subject subject
                                                          dateSubmitted rights])

    # The 'add' entries do not include the non-user-editable fields
    expect(adds.map { |att| att.name }).not_to include(metadata_file.non_user_editable_fields)

    actual_md = {}
    adds.each do |att|
      expect(att.value_type).to eq(Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING)
      actual_md[att.name] ||= []
      actual_md[att.name] << att.value
    end
    expect(actual_md.keys).to match_array(expected_md.keys)
    actual_md.each do |key, value|
      expect(Array(expected_md[key])).to match_array(value)
    end
  end
end

module Ddr
  RSpec.describe MetadataFile, :metadata_file, type: :model do
    context 'validation' do
      let(:metadata_file) { FactoryBot.create(:metadata_file_csv) }

      context 'valid' do
        before do
          allow(Ddr::AdminSet).to receive(:keys).and_return(['dc'])
          allow(Ddr::RightsStatement).to receive(:keys).and_return(['https://creativecommons.org/licenses/by/4.0/'])
        end

        it 'has a valid factory' do
          expect(metadata_file).to be_valid
          expect(metadata_file.validate_data).to be_empty
        end
      end

      context 'metadata file missing' do
        let(:error_field) { :metadata }

        before { metadata_file.metadata = nil }

        it_behaves_like 'an invalid metadata file'
      end

      context 'metadata file not parseable with profile' do
        before do
          metadata_file.update(metadata: ::File.new(Rails.root.join('spec/fixtures/batch_update/metadata_csv_malformed.csv')))
        end

        it 'has a parse error' do
          expect(metadata_file.validate_data.messages[:metadata].first)
            .to include(I18n.t('ddr.metadata_file.error.parse_error'))
        end
      end

      context 'metadata file invalid attribute names' do
        context 'invalid metadata header' do
          before do
            metadata_file.update(metadata: ::File.new(Rails.root.join('spec/fixtures/batch_update/metadata_csv_invalid_column.csv')))
          end

          it 'has an attribute name error' do
            expect(metadata_file.validate_data.messages[:metadata])
              .to include("#{I18n.t('ddr.metadata_file.error.attribute_name')}: invalid")
          end
        end
      end

      context 'metadata file invalid controlled vocabulary value' do
        before do
          allow(Ddr::AdminSet).to receive(:keys).and_return(%w[dc dvs])
          allow(Ddr::RightsStatement).to receive(:keys).and_return(['https://creativecommons.org/publicdomain/zero/1.0/'])
          metadata_file.update(metadata: ::File.new(Rails.root.join('spec/fixtures/batch_update/metadata_csv_invalid_value.csv')))
        end

        it 'has an attribute value error' do
          expect(metadata_file.validate_data.messages[:metadata])
            .to include("#{I18n.t('ddr.metadata_file.error.attribute_value')}: admin_set -> foo")
        end
      end
    end

    context 'successful processing', :batch do
      let(:metadata_file) { FactoryBot.build(:metadata_file) }

      let(:expected_md) do
        { 'local_id' => 'test12345',
          'title' => ['Updated Title'],
          'description' => ['This is some description; this is "some more" description.'],
          'subject' => %w[Alpha Beta Gamma Delta Epsilon],
          'dateSubmitted' => ['2010-01-22'],
          'ead_id' => 'abcdef',
          'rights' => ['https://creativecommons.org/licenses/by/4.0/'],
          'nested_path' => '/foo/bar' }
      end

      before do
        allow_any_instance_of(MetadataFile).to receive_message_chain(:metadata, :path).and_return(delimited_file)
      end

      context 'desc metadata csv file' do
        let(:delimited_file) { Rails.root.join('spec/fixtures/batch_update/metadata_csv.csv').to_s }

        describe 'operation' do
          before do
            metadata_file.procezz
            @batch = Ddr::Batch::Batch.all.last
            @batch_object = @batch.batch_objects.first
            @attributes = @batch_object.batch_object_attributes
          end

          it_behaves_like 'a successful metadata file processing'
        end

        describe 'return type' do
          it 'returns the batch ID' do
            expect(metadata_file.procezz).to eq(Ddr::Batch::Batch.all.last.id)
          end
        end
      end
    end
  end
end
