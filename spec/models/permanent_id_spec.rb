module Ddr
  RSpec.describe PermanentId do
    describe 'auto assigment' do
      let(:obj) do
        Ddr::ResourceChangeSetPersister.new.save(change_set: Ddr::ItemChangeSet.new(Ddr::Item.new))
      end

      describe 'when enabled' do
        let!(:id) { described_class.identifier_class.new('foo') }

        before do
          allow(id).to receive(:save).and_return(nil)
          allow(described_class.identifier_class).to receive(:mint) { id }
          allow(described_class.identifier_class).to receive(:find).with('foo') { id }
          allow(Ddr::Admin).to receive(:auto_assign_permanent_id).and_return(true)
        end

        it 'assigns a permanent id to the object' do
          expect(Ddr.query_service.find_by(id: obj.id).permanent_id).to eq('foo')
          expect(Ddr.query_service.find_by(id: obj.id).permanent_url).to eq('https://idn.duke.edu/foo')
        end
      end

      describe 'when disabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_assign_permanent_id).and_return(false)
        end

        it 'does not assign a permanent id to the object' do
          expect(obj.permanent_id).to be_nil
        end
      end
    end

    describe 'assignment' do
      let(:obj) { FactoryBot.create_for_repository(:item) }
      let!(:id) { described_class.identifier_class.new('foo') }

      before do
        allow(id).to receive(:save).and_return(nil)
        allow(described_class.identifier_class).to receive(:mint) { id }
        allow(described_class.identifier_class).to receive(:find).with('foo') { id }
      end

      it 'creates an update event' do
        expect { described_class.assign!(obj) }.to change(Ddr::Events::UpdateEvent, :count).by(1)
      end
    end

    describe 'auto updating' do
      let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:obj) { FactoryBot.create_for_repository(:item, id: valkyrie_id) }
      let!(:id) { described_class.identifier_class.new('foo') }

      before do
        allow(described_class.identifier_class).to receive(:find).with('foo') { id }
      end

      describe 'when enabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_update_permanent_id).and_return(true)
        end

        describe 'when the object workflow state changes' do
          before do
            allow(obj).to receive(:publishable?).and_return(true)
            obj.permanent_id = 'foo'
          end

          after do
            ob = Ddr.query_service.find_by(id: obj.id)
            ob.permanent_id = nil
            Ddr.persister.save(resource: ob)
          end
        end

        describe 'when the object workflow state does not change' do
          it 'does not update the permanent id' do
            expect_any_instance_of(described_class).not_to receive(:update!)
            Ddr::ResourceChangeSetPersister.new.save(change_set: Ddr::ItemChangeSet.new(obj))
          end
        end
      end

      describe 'when disabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_update_permanent_id).and_return(false)
        end

        describe 'when the object workflow state changes' do
          before do
            allow(id).to receive(:save).and_return(nil)
            obj.permanent_id = 'foo'
          end

          after do
            ob = Ddr.query_service.find_by(id: obj.id)
            ob.permanent_id = nil
            Ddr.persister.save(resource: ob)
          end
        end
      end
    end

    describe 'auto deleting / marking unavailable' do
      describe 'when enabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_update_permanent_id).and_return(true)
        end

        describe 'when an object has a permanent id' do
          let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
          let(:obj) { Ddr.persister.save(resource: Ddr::Item.new(id: valkyrie_id, permanent_id: 'foo')) }
          let!(:id) { described_class.identifier_class.new('foo') }

          before do
            allow(id).to receive(:save).and_return(nil)
            allow(described_class.identifier_class).to receive(:find).with('foo') { id }
          end

          describe "and it's deleted" do
            specify do
              expect(described_class).to receive(:delete!).with(valkyrie_id, 'foo', nil).and_return(nil)
              Ddr::ResourceChangeSetPersister.new.delete(change_set: Ddr::ItemChangeSet.new(obj))
            end
          end

          describe "and it's destroyed" do
            specify do
              expect(described_class).to receive(:delete!).with(valkyrie_id, 'foo', nil).and_return(nil)
              Ddr::ResourceChangeSetPersister.new.destroy(change_set: Ddr::ItemChangeSet.new(obj))
            end
          end
        end

        describe 'when an object does not have a permanent id' do
          let(:obj) { FactoryBot.create_for_repository(:item) }

          describe "and it's deleted" do
            specify do
              expect(described_class).not_to receive(:delete!)
              Ddr::ResourceChangeSetPersister.new.delete(change_set: Ddr::ItemChangeSet.new(obj))
            end
          end

          describe "and it's destroyed" do
            specify do
              expect(described_class).not_to receive(:delete!)
              Ddr::ResourceChangeSetPersister.new.destroy(change_set: Ddr::ItemChangeSet.new(obj))
            end
          end
        end
      end

      describe 'when disabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_update_permanent_id).and_return(false)
        end

        describe 'when an object has a permanent id' do
          let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
          let(:obj) { Ddr.persister.save(resource: Ddr::Item.new(id: valkyrie_id, permanent_id: 'foo')) }
          let!(:id) { described_class.identifier_class.new('foo') }

          before do
            allow(id).to receive(:save).and_return(nil)
            allow(described_class.identifier_class).to receive(:find).with('foo') { id }
          end

          describe "and it's deleted" do
            specify do
              expect(described_class).not_to receive(:delete!)
              Ddr::ResourceChangeSetPersister.new.delete(change_set: Ddr::ItemChangeSet.new(obj))
            end
          end

          describe "and it's destroyed" do
            specify do
              expect(described_class).not_to receive(:delete!)
              Ddr::ResourceChangeSetPersister.new.delete(change_set: Ddr::ItemChangeSet.new(obj))
            end
          end
        end
      end
    end

    describe 'constructor' do
      describe 'when the object is new' do
        let(:obj) { FactoryBot.build(:item) }

        it 'raises an error' do
          expect { described_class.new(obj) }.to raise_error(PermanentId::RepoObjectNotPersisted)
        end
      end

      describe 'when passed a repo object id' do
        let(:obj) { FactoryBot.create(:item) }

        subject { described_class.new(obj.id) }
        its(:repo_id) { is_expected.to eq(obj.id) }
      end
    end

    describe 'instance methods' do
      let(:obj) { FactoryBot.create_for_repository(:item) }

      subject { described_class.new(obj) }

      describe '#assign!' do
        describe 'when the object already has a permanent identifier' do
          before { obj.permanent_id = 'foo' }

          it 'raises an error' do
            expect { subject.assign! }.to raise_error(PermanentId::AlreadyAssigned)
            expect { subject.assign!('bar') }.to raise_error(PermanentId::AlreadyAssigned)
          end
        end

        describe 'when the object does not have a permanent identifier' do
          let(:obj) { FactoryBot.create_for_repository(:item) }
          let!(:id) { described_class.identifier_class.new('foo') }

          before do
            allow(described_class.identifier_class).to receive(:find).with('foo') { id }
            allow(id).to receive(:save).and_return(nil)
          end

          describe 'when passed an ARK' do
            before do
              subject.assign!('foo')
            end

            it 'assigns the ARK' do
              expect(Ddr.query_service.find_by(id: obj.id).permanent_id).to eq('foo')
            end

            it 'sets the target on the identifier' do
              expect(id.target).to eq('https://repository.duke.edu/id/foo')
            end

            it 'sets the status on the identifier' do
              expect(id.status).to eq('reserved')
            end

            it 'sets the repository id on the identifier' do
              expect(id['ddr.resource.id']).to eq(obj.id.id)
            end
          end

          describe 'when not passed an ARK' do
            before do
              subject.assign!('foo')
              allow(described_class.identifier_class).to receive(:mint) { id }
            end

            it 'mints and assigns an ARK' do
              expect(Ddr.query_service.find_by(id: obj.id).permanent_id).to eq('foo')
            end

            it 'sets the target on the identifier' do
              expect(id.target).to eq('https://repository.duke.edu/id/foo')
            end

            it 'sets the status on the identifier' do
              expect(id.status).to eq('reserved')
            end

            it 'sets the repository id on the identifier' do
              expect(id['ddr.resource.id']).to eq(obj.id.id)
            end
          end
        end
      end

      describe '#update!' do
        describe 'when the object has not been assigned a permanent id' do
          it 'raises an error' do
            expect { subject.update! }.to raise_error(PermanentId::IdentifierNotAssigned)
          end
        end

        describe 'when the object has been assigned a permanent id' do
          let!(:id) { described_class.identifier_class.new('foo') }

          before do
            allow(described_class.identifier_class).to receive(:find).with('foo') { id }
            obj.permanent_id = 'foo'
          end

          it 'sets the status on the permanent id' do
            expect(subject).to receive(:set_status!).and_return(nil)
            subject.update!
          end
        end
      end

      describe '#delete!' do
        let!(:id) { described_class.identifier_class.new('foo') }
        let(:item) { FactoryBot.create_for_repository(:item) }

        subject { described_class.new(item, 'foo') }
        before do
          allow(described_class.identifier_class).to receive(:find).with('foo') { id }
        end

        specify do
          expect(id).to receive(:delete)
          subject.delete!
        end

        describe 'when the identifier is not reserved' do
          before do
            allow(id).to receive(:save).and_return(nil)
            id.public!
          end

          specify do
            expect { subject.delete! }.to change(id, :status).to('unavailable | deleted')
          end
        end

        describe 'when the identifier repo id is set' do
          before do
            subject.set_identifier_repo_id
            allow(id).to receive(:delete).and_return(nil)
          end

          specify do
            expect { subject.delete! }.not_to raise_error
          end
        end

        describe 'when the identifier is associated with another repo id' do
          before { subject.identifier_repo_id = SecureRandom.uuid }

          specify do
            expect { subject.delete! }.to raise_error(PermanentId::Error)
          end
        end
      end

      describe 'identifier metadata' do
        let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
        let(:obj) { Ddr.persister.save(resource: Ddr::Item.new(id: valkyrie_id)) }
        let!(:id) { described_class.identifier_class.new('foo') }

        subject { described_class.new(obj) }
        before do
          allow(subject).to receive(:identifier) { id }
        end

        describe '#identifier_repo_id' do
          before do
            id['ddr.resource.id'] = valkyrie_id.id
          end

          its(:identifier_repo_id) { is_expected.to eq(valkyrie_id.id) }
        end

        describe '#identifier_repo_id=' do
          specify do
            subject.identifier_repo_id = valkyrie_id.id
            expect(id['ddr.resource.id']).to eq(valkyrie_id.id)
          end

          describe 'when a value was previously assigned' do
            before { subject.identifier_repo_id = valkyrie_id.id }

            specify do
              expect do
                subject.identifier_repo_id = Valkyrie::ID.new(SecureRandom.uuid)
              end.to raise_error(PermanentId::Error)
            end
          end
        end

        describe '#set_identifier_repo_id' do
          specify do
            subject.set_identifier_repo_id
            expect(subject.identifier_repo_id).to eq(valkyrie_id.id)
          end
        end

        describe '#set_target' do
          specify do
            subject.set_target
            expect(subject.target).to eq('https://repository.duke.edu/id/foo')
          end
        end

        describe '#set_status' do
          describe 'when object is published' do
            before { obj.workflow_state = 'published' }

            describe 'and identifier is public' do
              before { subject.public! }

              it 'does not change' do
                expect { subject.set_status }.not_to change(subject, :status)
              end
            end

            describe 'and identifier is reserved' do
              it 'changes to public' do
                expect { subject.set_status }.to change(subject, :status).to('public')
              end
            end

            describe 'and identifier is unavailable' do
              before { subject.unavailable! }

              it 'changes to public' do
                expect { subject.set_status }.to change(subject, :status).to('public')
              end
            end
          end

          describe 'when object is unpublished' do
            before { obj.workflow_state = 'unpublished' }

            describe 'and identifier is public' do
              before { subject.public! }

              it 'changes to unavailable' do
                expect { subject.set_status }.to change(subject, :status).to('unavailable | not published')
              end
            end

            describe 'and identifier is reserved' do
              it 'does not change' do
                expect { subject.set_status }.not_to change(subject, :status)
              end
            end

            describe 'and identifier is unavailable' do
              before { subject.unavailable! }

              it 'does not change' do
                expect { subject.set_status }.not_to change(subject, :status)
              end
            end
          end

          describe 'when object has no workflow state' do
            describe 'and identifier is public' do
              before { subject.public! }

              it 'does not change', skip: 'This should not happen' do
                expect { subject.set_status }.not_to change(subject, :status)
              end
            end

            describe 'and identifier is reserved' do
              it 'does not change' do
                expect { subject.set_status }.not_to change(subject, :status)
              end
            end

            describe 'and identifier is unavailable' do
              before { subject.unavailable! }

              it 'does not change' do
                expect { subject.set_status }.not_to change(subject, :status)
              end
            end
          end
        end
      end
    end
  end
end
