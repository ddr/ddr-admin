module Ddr
  RSpec.describe DeletedFile do
    describe 'resource' do
      let(:resource) { FactoryBot.create_for_repository(:component, :with_content_file, :with_thumbnail_file) }
      let(:content_path) { resource.content.file_path }
      let(:thumbnail_path) { resource.thumbnail.file_path }

      before do
        Ddr::ResourceChangeSetPersister.new.delete(change_set: Ddr::ResourceChangeSet.change_set_for(resource))
      end

      describe 'ddr_files' do
        subject { described_class.where(repo_id: resource.id.id) }
        it 'has entries for content and thumbnail files' do
          expect(subject.count).to eq(2)
          expect(subject.where(file_id: 'content').first.path).to eq(content_path)
          expect(subject.where(file_id: 'thumbnail').first.path).to eq(thumbnail_path)
        end
      end
    end

    describe 'files' do
      let(:resource) { FactoryBot.create_for_repository(:component, :with_content_file, :with_thumbnail_file) }
      let!(:content_path) { resource.content.file_path }

      before do
        change_set = Ddr::ComponentChangeSet.new(resource)
        change_set.content = nil
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
      end

      subject { DeletedFile.where(repo_id: resource.id.id, file_id: 'content').first }
      its(:path) { is_expected.to eq content_path }

      describe 'deleting the DeletedFile record' do
        it 'actually removes the file' do
          expect { subject.destroy }.to change { ::File.exist?(subject.path) }.from(true).to(false)
        end

        it 'is ok if the file does not exist' do
          ::File.unlink(content_path)
          expect { subject.destroy }.not_to raise_error
        end

        it 'is ok if the path attribute is nil' do
          subject.update(path: nil)
          expect { subject.destroy }.not_to raise_error
        end
      end
    end
  end
end
