require 'rails_helper'
require 'support/shared_examples_for_ddr_resources'
require 'support/shared_examples_for_display_title'
require 'support/shared_examples_for_has_attachments'
require 'support/shared_examples_for_has_children'
require 'support/shared_examples_for_has_struct_metadata'
require 'support/shared_examples_for_streamable_media'
require 'support/shared_examples_for_publishable_resources'

module Ddr
  RSpec.describe Collection, type: :model do
    it_behaves_like 'a DDR resource'
    it_behaves_like 'a non-content-bearing resource that has a display title'
    it_behaves_like 'a resource that can have children'
    it_behaves_like 'a resource that can have attachments'
    it_behaves_like 'a resource that can have structural metadata'
    it_behaves_like 'a resource that cannot be streamable'
    it_behaves_like 'a publishable resource'

    specify do
      expect(described_class.attachable_files).to match_array(%i[iiif_file struct_metadata thumbnail])
      expect(described_class.can_be_streamable?).to eq(false)
      expect(described_class.can_have_content?).to eq(false)
      expect(described_class.can_have_derived_image?).to eq(false)
      expect(described_class.can_have_extracted_text?).to eq(false)
      expect(described_class.can_have_fits_file?).to eq(false)
      expect(described_class.can_have_intermediate_file?).to eq(false)
      expect(described_class.can_have_multires_image?).to eq(false)
      expect(described_class.can_have_struct_metadata?).to eq(true)
      expect(described_class.can_have_thumbnail?).to eq(true)
      expect(described_class.captionable?).to eq(false)
      expect(described_class.governable?).to eq(true)
    end

    describe 'workflow state' do
      subject { create_for_repository(:collection, workflow_state:) }

      context 'when published' do
        let(:workflow_state) { 'published' }

        its(:can_publish?) { is_expected.to be true }
        its(:can_unpublish?) { is_expected.to be true }

        its(:can_preview?) { is_expected.to be false } if Ddr::Admin.publication_preview_enabled
        its(:can_make_nonpublishable?) { is_expected.to be false }
      end

      context 'when unpublished' do
        let(:workflow_state) { 'unpublished' }

        its(:can_publish?) { is_expected.to be true }
        its(:can_unpublish?) { is_expected.to be true }

        its(:can_preview?) { is_expected.to be true } if Ddr::Admin.publication_preview_enabled
        its(:can_make_nonpublishable?) { is_expected.to be true }
      end

      context 'when workflow_state is nil' do
        let(:workflow_state) { nil }

        its(:can_publish?) { is_expected.to be true }
        its(:can_unpublish?) { is_expected.to be true }

        its(:can_preview?) { is_expected.to be true } if Ddr::Admin.publication_preview_enabled
        its(:can_make_nonpublishable?) { is_expected.to be true }
      end

      context 'when nonpublishable' do
        let(:workflow_state) { 'nonpublishable' }

        its(:can_publish?) { is_expected.to be false }
        its(:can_unpublish?) { is_expected.to be true }

        its(:can_preview?) { is_expected.to be false } if Ddr::Admin.publication_preview_enabled
        its(:can_make_nonpublishable?) { is_expected.to be true }
      end

      if Ddr::Admin.publication_preview_enabled
        context 'when previewable' do
          let(:workflow_state) { 'previewable' }

          its(:can_publish?) { is_expected.to be true }
          its(:can_unpublish?) { is_expected.to be true }
          its(:can_preview?) { is_expected.to be true }
          its(:can_make_nonpublishable?) { is_expected.to be true }
        end
      end
    end

    describe '#components' do
      let(:collection) { create_for_repository(:collection, admin_set: 'dc') }
      let(:item) { create_for_repository(:item, parent_id: collection.id) }
      let!(:component) { create_for_repository(:component, parent_id: item.id, admin_policy_id: collection.id) }

      subject { collection }
      it 'returns the components' do
        expect(subject.components.first.id).to eq(component.id)
      end
    end

    describe '#attachments' do
      let(:collection) { create_for_repository(:collection, admin_set: 'dc') }
      let!(:attachment) { create_for_repository(:attachment, attached_to_id: collection.id) }

      subject { collection }
      it 'returns the attachments that are attached to the collection' do
        expect(subject.attachments.first.id).to eq(attachment.id)
      end
    end

    describe '#targets' do
      let!(:collection) { create_for_repository(:collection, admin_set: 'dc') }
      let!(:target) { create_for_repository(:target, for_collection_id: collection.id) }

      subject { collection }
      it 'returns the targets that have it as their collection' do
        expect(subject.targets.first.id).to eq(target.id)
      end
    end
  end
end
