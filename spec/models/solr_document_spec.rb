require 'rails_helper'
require 'support/structural_metadata_helper'

RSpec.describe SolrDocument do
  after { persister.wipe! }

  let(:metadata_adapter) do
    Valkyrie::Persistence::Solr::MetadataAdapter.new(
      connection: Blacklight.default_index.connection,
      resource_indexer: Ddr::ResourceIndexer
    )
  end

  let(:persister) { metadata_adapter.persister }

  describe 'class methods' do
    let(:id) { build(:valkyrie_id) }

    describe '.find' do
      describe 'when it exists' do
        subject { described_class.find(id) }

        let!(:item) { create_for_repository(:item, id:) }

        its(:id) { is_expected.to eq(id.to_s) }
      end

      describe 'when not found' do
        it 'raises an error' do
          expect { described_class.find('foo') }.to raise_error(SolrDocument::NotFound)
        end
      end
    end

    describe '.find_by_permanent_id' do
      describe 'when passed a nil argument value' do
        it 'raises an ArgumentError' do
          expect { described_class.find_by_permanent_id(nil) }.to raise_error(ArgumentError)
        end
      end

      describe 'when it exists' do
        subject { described_class.find_by_permanent_id('foo') }

        let!(:item) { create_for_repository(:item, id:, permanent_id: 'foo') }

        its(:id) { is_expected.to eq(id.to_s) }
      end

      describe 'when not found' do
        it 'raises an error' do
          expect { described_class.find_by_permanent_id('foo') }.to raise_error(SolrDocument::NotFound)
        end
      end
    end
  end

  describe 'instance methods' do
    subject { described_class.new(source) }

    let(:source) { {} } # Default - redefine as needed for specific tests

    describe 'index field method access' do
      describe 'when there is an index field' do
        before { Ddr::Index::Fields.const_set(:FOO_BAR, 'foo_bar_ssim') }
        after { Ddr::Index::Fields.send(:remove_const, :FOO_BAR) }

        describe 'and the field is not present' do
          its(:foo_bar) { is_expected.to be_nil }
        end

        describe 'and the field is a single value (not an array)' do
          let(:source) { { Ddr::Index::Fields::FOO_BAR => 'foo' } }

          its(:foo_bar) { is_expected.to eq('foo') }
        end

        describe 'and the field value is an array' do
          let(:source) { { Ddr::Index::Fields::FOO_BAR => %w[foo bar] } }

          its(:foo_bar) { is_expected.to eq('foo, bar') }
        end
      end

      describe 'when there is no index field' do
        it 'raises an exception' do
          expect { subject.foo_bar }.to raise_error(NoMethodError)
        end
      end
    end

    describe '#resource' do
      subject { described_class.find(id) }

      let(:id) { build(:valkyrie_id) }
      let!(:resource) { create_for_repository(:item, id:) }

      it 'returns the resource' do
        expect(subject.resource).to be_a(Ddr::Item)
        expect(subject.resource.id).to eq(id)
      end
    end

    describe '#admin_policy_id' do
      describe 'when not set' do
        its(:admin_policy_id) { is_expected.to be_nil }
      end

      describe 'when set' do
        let(:source) { { Ddr::Index::Fields::ADMIN_POLICY_ID => 'info:fedora/test:1' } }

        its(:admin_policy_id) { is_expected.to eq('info:fedora/test:1') }
      end
    end

    describe '#admin_policy' do
      describe 'when there is not an admin policy' do
        its(:admin_policy) { is_expected.to be_nil }
      end

      describe 'where there is an admin policy' do
        let(:admin_policy) { create_for_repository(:collection, admin_set: 'dc') }
        let(:source) { { Ddr::Index::Fields::ADMIN_POLICY_ID => ["id-#{admin_policy.id}"] } }

        it 'gets the admin policy document' do
          expect(subject.admin_policy.id).to eq(admin_policy.id.id)
        end
      end
    end

    describe '#parent_id' do
      let(:parent_id) { build(:valkyrie_id) }

      describe 'when is_part_of is present' do
        let(:source) { { Ddr::Index::Fields::IS_PART_OF => parent_id.to_s } }

        its(:parent_id) { is_expected.to eq(parent_id.to_s) }
      end

      describe 'when is_part_of is not present' do
        describe 'when is_member_of_collection is present' do
          let(:source) { { Ddr::Index::Fields::IS_MEMBER_OF_COLLECTION => parent_id.to_s } }

          its(:parent_id) { is_expected.to eq(parent_id.to_s) }
        end

        describe 'when is_member_of_collection is not present' do
          its(:parent_id) { is_expected.to be_nil }
        end
      end
    end

    describe '#parent' do
      let(:parent_id) { build(:valkyrie_id) }

      describe 'when there is a parent id' do
        let(:doc) { described_class.new({ 'id' => parent_id.to_s }) }

        before do
          allow(subject).to receive(:parent_id) { parent_id.to_s }
          allow(described_class).to receive(:find).with(parent_id.to_s) { doc }
        end

        its(:parent) { is_expected.to eq(doc) }
      end

      describe 'when there is no parent id' do
        its(:parent) { is_expected.to be_nil }
      end
    end

    describe '#roles' do
      subject { described_class.find(resource.id) }

      let(:editor) { FactoryBot.build(:role, :editor, :policy, agent: 'Editors') }
      let(:downloader) { FactoryBot.build(:role, :downloader, :resource, :public) }
      let(:resource) { create_for_repository(:item, access_role: [editor, downloader]) }

      it 'deserializes the roles' do
        expect(subject.roles).to contain_exactly(editor, downloader)
      end
    end

    describe '#structure' do
      context 'no indexed structures' do
        it 'returns nil' do
          expect(subject.structure).to be_nil
        end
      end

      context 'indexed structure' do
        let(:source) { { Ddr::Index::Fields::STRUCTURE => simple_structure_to_json } }

        it 'returns the structures map' do
          expect(subject.structure).to eq(JSON.parse(simple_structure_to_json))
        end
      end
    end

    describe 'contacts' do
      describe '#research_help' do
        context 'object has research help contact' do
          let(:source) { { Ddr::Index::Fields::RESEARCH_HELP_CONTACT => 'dvs' } }

          it "returns the object's research help contact" do
            expect(subject.research_help.slug).to eq('dvs')
          end
        end

        context 'object does not have research help contact' do
          context 'collection has research help contact' do
            let(:admin_policy) { described_class.new({ Ddr::Index::Fields::RESEARCH_HELP_CONTACT => ['dvs'] }) }

            before do
              allow(subject).to receive(:admin_policy) { admin_policy }
            end

            it "returns the collection's research help contact" do
              expect(subject.research_help.slug).to eq('dvs')
            end
          end

          context 'collection does not have research help contact' do
            it 'returns nil' do
              expect(subject.research_help).to be_nil
            end
          end
        end
      end
    end

    describe 'structural metadata' do
      describe '#multires_image_file_paths' do
        context 'no structural metadata' do
          its(:multires_image_file_paths) { is_expected.to match([]) }
        end

        context 'structural metadata' do
          before { allow(subject).to receive(:structure) { JSON.parse(simple_structure_to_json) } }

          context 'no structural objects with multi-res images' do
            before do
              allow(SolrDocument).to receive(:find) { double(multires_image_file_path: nil) }
            end

            its(:multires_image_file_paths) { is_expected.to match([]) }
          end

          context 'structural objects with multi-res images' do
            let(:expected_result) { ['/path/file1.ptif', '/path/file2.ptif'] }

            before do
              allow(SolrDocument).to receive(:find).with('43db27e0-fc60-445e-9080-d385fc159ed9') {
                                       double(multires_image_file_path: '/path/file1.ptif')
                                     }
              allow(SolrDocument).to receive(:find).with('d9616b83-9505-42d1-9443-f72fad8cd33b') {
                                       double(multires_image_file_path: nil)
                                     }
              allow(SolrDocument).to receive(:find).with('86a05825-9a58-4b55-83c0-cd8ce2a1bbc3') {
                                       double(multires_image_file_path: '/path/file2.ptif')
                                     }
            end

            its(:multires_image_file_paths) { is_expected.to match(expected_result) }
          end
        end
      end
    end

    describe '#captionable?' do
      specify do
        allow(subject).to receive(:resource) { double(captionable?: false) }
        expect(subject).not_to be_captionable
      end

      specify do
        allow(subject).to receive(:resource) { double(captionable?: true) }
        expect(subject).to be_captionable
      end
    end

    describe '#captioned?' do
      specify do
        allow(subject).to receive(:resource) { double(captioned?: false) }
        expect(subject).not_to be_captioned
      end

      specify do
        allow(subject).to receive(:resource) { double(captioned?: true) }
        expect(subject).to be_captioned
      end
    end

    describe '#caption_type' do
      describe 'non-captioned resource' do
        specify do
          allow(subject).to receive(:captioned?).and_return(false)
          expect(subject.caption_type).to be_nil
        end
      end

      describe 'captioned resource' do
        let(:mime_type) { 'text/vtt' }
        let(:resource) { double(caption_type: mime_type) }

        specify do
          allow(subject).to receive(:captioned?).and_return(true)
          allow(subject).to receive(:resource) { resource }
          expect(subject.caption_type).to eq mime_type
        end
      end
    end

    describe '#caption_path' do
      describe 'non-captioned resource' do
        specify do
          allow(subject).to receive(:captioned?).and_return(false)
          expect(subject.caption_path).to be_nil
        end
      end

      describe 'captioned resource' do
        let(:media_path) { '/foo/bar/baz.txt' }
        let(:resource) { double(caption_path: media_path) }

        specify do
          allow(subject).to receive(:captioned?).and_return(true)
          allow(subject).to receive(:resource) { resource }
          expect(subject.caption_path).to eq media_path
        end
      end
    end

    describe '#caption_extension' do
      let(:extensions) { { 'text/vtt' => 'vtt', 'application/zip' => 'zip' } }

      before { allow(Ddr).to receive(:preferred_file_extensions) { extensions } }

      specify do
        allow(subject).to receive(:captioned?).and_return(false)
        expect(subject.caption_extension).to be_nil
      end

      specify do
        allow(subject).to receive(:captioned?).and_return(true)
        allow(subject).to receive(:caption_type).and_return('text/vtt')
        expect(subject.caption_extension).to eq 'vtt'
      end

      specify do
        allow(subject).to receive(:captioned?).and_return(true)
        allow(subject).to receive(:caption_type).and_return('application/zip')
        expect(subject.caption_extension).to eq 'zip'
      end

      specify do
        allow(subject).to receive(:captioned?).and_return(true)
        allow(subject).to receive(:caption_type).and_return('application/foo')
        allow(subject).to receive(:caption_extension_default).and_return('bin')
        expect(subject.caption_extension).to eq 'bin'
      end
    end

    describe '#streamable?' do
      specify do
        allow(subject).to receive(:resource) { double(streamable?: false) }
        expect(subject).not_to be_streamable
      end

      specify do
        allow(subject).to receive(:resource) { double(streamable?: true) }
        expect(subject).to be_streamable
      end
    end

    describe '#streamable_media_path' do
      describe 'non-streamable resource' do
        specify do
          allow(subject).to receive(:streamable?).and_return(false)
          expect(subject.streamable_media_path).to be_nil
        end
      end

      describe 'streamable resource' do
        let(:media_path) { '/foo/bar/baz.txt' }
        let(:resource) { double(streamable_media_path: media_path) }

        specify do
          allow(subject).to receive(:streamable?).and_return(true)
          allow(subject).to receive(:resource) { resource }
          expect(subject.streamable_media_path).to eq media_path
        end
      end
    end

    describe '#streamable_media_extension' do
      let(:extensions) { { 'audio/mpeg' => 'mp3', 'video/mp4' => 'mp4' } }

      before do
        allow(Ddr).to receive(:preferred_file_extensions) { extensions }
        allow(subject).to receive(:streamable?).and_return(true)
      end

      specify do
        allow(subject).to receive(:streamable?).and_return(false)
        expect(subject.streamable_media_extension).to be_nil
      end

      specify do
        allow(subject).to receive(:streamable_media_type).and_return('audio/mpeg')
        expect(subject.streamable_media_extension).to eq 'mp3'
      end

      specify do
        allow(subject).to receive(:streamable_media_type).and_return('application/foo')
        allow(subject).to receive(:streamable_media_extension_default).and_return('bin')
        expect(subject.streamable_media_extension).to eq 'bin'
      end
    end

    describe '#thumbnail_path' do
      describe 'resource does not have a thumbnail' do
        specify do
          allow(subject).to receive(:has_thumbnail?).and_return(false)
          expect(subject.thumbnail_path).to be_nil
        end
      end

      describe 'resource has thumbnail' do
        let(:image_path) { '/foo/bar/baz.jpg' }
        let(:resource) { double(thumbnail_path: image_path) }

        specify do
          allow(subject).to receive(:has_thumbnail?).and_return(true)
          allow(subject).to receive(:resource) { resource }
          expect(subject.thumbnail_path).to eq image_path
        end
      end
    end

    describe '#rights' do
      let(:rights) { 'https://creativecommons.org/publicdomain/mark/1.0' }
      let(:id) { build(:valkyrie_id) }
      let!(:item) { create_for_repository(:item, id:, rights:) }

      specify do
        doc = described_class.find(id)
        expect(doc.rights).to eq [rights]
      end
    end

    describe '#rights_statement' do
      let(:rights_statement) { Ddr::RightsStatement.keystore.fetch('https://creativecommons.org/publicdomain/mark/1.0') }

      before do
        allow(subject).to receive(:rights).and_return(['https://creativecommons.org/publicdomain/mark/1.0'])
      end

      its(:rights_statement) { is_expected.to eq rights_statement }
    end

    describe '#embargo' do
      let(:now) { DateTime.now.to_time.utc.iso8601 }

      specify 'on the resource' do
        resource = create_for_repository(:item, available: now)
        doc = described_class.find(resource.id.to_s)
        expect(doc.embargo).to eq Time.parse(now).localtime
      end

      specify 'on the child' do
        item = create_for_repository(:item, available: now)
        component = create_for_repository(:component, parent_id: item.id)
        doc = described_class.find(component.id.to_s)
        expect(doc.embargo).to eq Time.parse(now).localtime
      end
    end
  end
end
