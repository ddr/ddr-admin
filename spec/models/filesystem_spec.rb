require 'rails_helper'
require 'support/ingest_folder_helper'

module Ddr
  RSpec.describe Filesystem, :batch, :standard_ingest, type: :model do
    let(:filesystem) { Filesystem.new }

    describe '.path_to_node' do
      before { filesystem.tree = sample_filesystem_without_dot_files }

      context 'full path' do
        it 'provides the full path' do
          expect(described_class.path_to_node(filesystem['movie.mp4'])).to eq('/test/directory/movie.mp4')
          expect(described_class.path_to_node(filesystem['itemA']['file01.pdf'])).to eq('/test/directory/itemA/file01.pdf')
        end
      end

      context 'relative path' do
        it 'provides the relative path' do
          expect(described_class.path_to_node(filesystem['movie.mp4'], 'relative')).to eq('movie.mp4')
          expect(described_class.path_to_node(filesystem['itemA']['file01.pdf'], 'relative')).to eq('itemA/file01.pdf')
        end
      end
    end

    describe '.node_locator' do
      before { filesystem.tree = sample_filesystem_without_dot_files }

      context 'root node' do
        it 'include no nodes in the locator' do
          expect(described_class.node_locator(filesystem.root)).to be_nil
        end
      end

      context 'first level nodes' do
        it 'includes node in locator' do
          expect(described_class.node_locator(filesystem['itemA'])).to eq('itemA')
        end
      end

      context 'second level nodes' do
        it 'includes both nodes in locator' do
          expect(described_class.node_locator(filesystem['itemA']['file01.pdf'])).to eq('itemA/file01.pdf')
        end
      end
    end

    describe '#method_missing' do
      context 'method missing' do
        it 'sends the method to the inner tree' do
          expect(filesystem.tree).to receive(:each_leaf)
          filesystem.each_leaf
        end
      end

      context 'method not missing' do
        it 'does not invoke #method_missing' do
          expect(filesystem).not_to receive(:method_missing).with(:file_count)
          filesystem.file_count
        end
      end
    end
  end
end
