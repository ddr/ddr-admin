module Ddr
  RSpec.describe ManifestBasedIngest, :batch, :ingest, type: :model do
    let(:user) { FactoryBot.create(:user) }
    let(:admin_set) { 'foo' }
    let(:valid_manifest_file) do
      Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/manifest.txt')
    end
    let(:invalid_manifest_file) do
      Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/invalid_manifest.txt')
    end
    # does not exist
    let(:missing_manifest_file) do
      Rails.root.join('spec/fixtures/batch_ingest/manifest_based_ingest/missing_manifest.txt')
    end
    let(:volumes) { { '/path/to/spec/fixtures' => Rails.root.join('spec/fixtures').to_s } }

    subject { ManifestBasedIngest.new(ingest_args) }

    before do
      allow_any_instance_of(ManifestBasedIngest).to receive(:load_configuration) { { volumes: } }
    end

    #       @admin_set = args['admin_set']
    #       @collection_id = args['collection_id']
    #       @collection_title = args['collection_title']
    #       @config_file = args['config_file'] || DEFAULT_CONFIG_FILE.to_s
    #       @manifest_file = args['manifest_file']
    #       @configuration = load_configuration
    #       @user = User.find_by_user_key(args['batch_user'])

    describe 'validation' do
      describe 'with valid manifest file' do
        let(:ingest_args) do
          { 'admin_set' => admin_set,
            'batch_user' => user.user_key,
            'collection_title' => 'Test Collection',
            'manifest_file' => valid_manifest_file }
        end

        it 'is valid' do
          expect(subject).to be_valid
        end
      end

      describe 'with missing manifest file' do
        let(:ingest_args) do
          { 'admin_set' => admin_set,
            'batch_user' => user.user_key,
            'collection_title' => 'Test Collection',
            'manifest_file' => missing_manifest_file }
        end

        it 'is not valid' do
          expect(subject).not_to be_valid
          expect(subject.errors.added?(:manifest_file, 'does not exist')).to be true
        end
      end
    end

    #      builder_args = {
    #         user: user,
    #         batch_name: "Manifest-Based Ingest",
    #         batch_description: Time.now
    #     }
    #     builder_args.merge!(admin_set: admin_set) if admin_set
    #     builder_args.merge!(collection_repo_id: collection_id) if collection_id
    #     builder_args.merge!(collection_title: collection_title) if collection_title
    #     builder_args.merge!(manifest: @manifest)

    describe '#process' do
      describe 'with a valid manifest file' do
        let(:ingest_args) do
          { 'admin_set' => admin_set,
            'batch_user' => user.user_key,
            'collection_title' => 'Test Collection',
            'manifest_file' => valid_manifest_file }
        end
        let(:manifest) { Manifest.new(valid_manifest_file, volumes) }
        let(:batch_builder_args) do
          {  admin_set:,
             batch_name: 'Manifest-Based Ingest',
             collection_title: 'Test Collection',
             manifest:,
             user: }
        end

        before do
          allow(User).to receive(:find_by_user_key).with(user.user_key) { user }
          expect(BuildBatchesFromManifestBasedIngest).to receive(:new).with(batch_builder_args).and_call_original
          allow_any_instance_of(BuildBatchesFromManifestBasedIngest).to receive(:call).and_return(nil)
        end

        it 'calls the batch builder correctly' do
          subject.process
        end
      end

      describe 'with an invalid manifest file' do
        let(:ingest_args) do
          { 'admin_set' => admin_set,
            'batch_user' => user.user_key,
            'collection_title' => 'Test Collection',
            'manifest_file' => invalid_manifest_file }
        end

        before do
          expect(BuildBatchesFromManifestBasedIngest).not_to receive(:new)
        end

        it "doesn't call the batch builder" do
          subject.process
        end
      end

      describe 'with a missing manifest file' do
        let(:ingest_args) do
          { 'admin_set' => admin_set,
            'batch_user' => user.user_key,
            'collection_title' => 'Test Collection',
            'manifest_file' => missing_manifest_file }
        end

        before do
          expect(BuildBatchesFromManifestBasedIngest).not_to receive(:new)
        end

        it "doesn't call the batch builder" do
          subject.process
        end
      end
    end
  end
end
