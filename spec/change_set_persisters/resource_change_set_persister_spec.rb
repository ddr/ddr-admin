require 'rails_helper'
require 'valkyrie/specs/shared_specs'

module Ddr
  RSpec.describe ResourceChangeSetPersister do
    describe '#save' do
      let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:resource) { Ddr::Item.new(id: valkyrie_id) }
      let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

      describe 'invalid change set' do
        let(:errs) { { admin_set: ["can't be blank"], title: ["can't be blank"] } }

        before do
          allow(change_set).to receive(:valid?).and_return(false)
          allow(change_set).to receive(:errors) { double('Reform::Form::ActiveModel::Errors', messages: errs) }
        end

        it 'logs error messages' do
          expect(Rails.logger).to receive(:error).with(I18n.t('ddr.change_set.wont_save', id_or_new: valkyrie_id.id))
          expect(Rails.logger).to receive(:error).with(I18n.t('ddr.change_set.error', id_or_new: valkyrie_id.id,
                                                                                      error: errs))
          begin
            subject.save(change_set:)
          rescue Ddr::ResourceChangeSetPersister::WontSaveError
          end
        end

        it 'does not sync the change set to the resource' do
          expect(change_set).not_to receive(:sync)
          begin
            subject.save(change_set:)
          rescue Ddr::ResourceChangeSetPersister::WontSaveError
          end
        end

        it 'does not persist the resource' do
          expect_any_instance_of(subject.persister.class).not_to receive(:save).with({ resource: change_set.resource })
                                                                               .and_call_original
          begin
            subject.save(change_set:)
          rescue Ddr::ResourceChangeSetPersister::WontSaveError
          end
        end

        it 'raises an exception' do
          expect { subject.save(change_set:) }.to raise_error(Ddr::ResourceChangeSetPersister::WontSaveError)
        end
      end

      describe 'valid change set' do
        before do
          allow(change_set).to receive(:valid?).and_return(true)
        end

        it 'syncs the change set to the resource' do
          expect(change_set).to receive(:sync)
          subject.save(change_set:)
        end

        it 'persists the resource' do
          expect_any_instance_of(subject.persister.class).to receive(:save).with({ resource: change_set.resource })
                                                                           .and_call_original
          subject.save(change_set:)
        end

        it 'creates a virus check event' do
          expect(subject).to receive(:create_virus_check_event)
          subject.save(change_set:)
        end

        it 'returns the persisted resource' do
          persisted_resource = subject.save(change_set:)
          expect(persisted_resource).to be_a(Ddr::Resource)
          expect(persisted_resource).to be_persisted
        end
      end
    end

    describe '#save_all' do
      let(:change_sets) do
        [Ddr::ResourceChangeSet.new(Ddr::Resource.new), Ddr::ResourceChangeSet.new(Ddr::Resource.new)]
      end

      it 'saves all the change sets' do
        change_sets.each do |change_set|
          expect(subject).to receive(:save).with({ change_set: })
        end
        subject.save_all(change_sets:)
      end
    end

    describe '#delete' do
      # Specifically using the 'disk' storage adapter here to more realistically test removal of disk files when
      # resource is deleted
      let(:storage_adapter) { Valkyrie::StorageAdapter.find(:disk) }
      let(:comp) { Ddr.persister.save(resource: Ddr::Component.new(id: build(:valkyrie_id))) }
      let(:image_file) { fixture_file_upload('imageA.tif', 'image/tiff') }
      let(:xml_file)   { fixture_file_upload('fits.xml', 'text/xml') }
      let(:file1) { storage_adapter.upload(file: image_file, resource: comp, original_filename: 'imageA.tif') }
      let(:file2) { storage_adapter.upload(file: xml_file, resource: comp, original_filename: 'fits.xml') }
      let(:component) do
        comp.content = Ddr::File.new(file_identifier: file1.id, original_filename: 'imageA.tif')
        comp.fits_file = Ddr::File.new(file_identifier: file2.id, original_filename: 'files.xml')
        Ddr.persister.save(resource: comp)
      end
      let(:change_set) { Ddr::ComponentChangeSet.new(component) }

      subject { described_class.new(storage_adapter:) }

      it 'deletes the resource but not the associated stored files' do
        subject.delete(change_set:)
        expect { subject.query_service.find_by(id: component.id) }
          .to raise_error(Valkyrie::Persistence::ObjectNotFoundError)
        expect { subject.storage_adapter.find_by(id: file1.id) }.not_to raise_error
        expect { subject.storage_adapter.find_by(id: file2.id) }.not_to raise_error
      end
    end

    describe 'ingested by' do
      let(:user) { User.new(username: 'foo@bar.com') }
      let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

      describe 'on create' do
        describe 'when change set has user' do
          before { change_set.user = user }

          describe 'and ingested_by is not set' do
            let(:resource) { Ddr::Resource.new }

            it 'sets ingested by' do
              expect do
                subject.save(change_set:)
              end.to change(resource, :ingested_by).from(nil).to('foo@bar.com')
            end
          end

          describe 'and ingested_by is set' do
            let(:resource) { Ddr::Resource.new(ingested_by: 'bob@example.com') }

            it 'does not set ingested by' do
              expect { subject.save(change_set:) }.not_to change(resource, :ingested_by)
            end
          end
        end
      end

      describe 'saving after create' do
        let(:user) { User.new(username: 'foo@bar.com') }
        let(:change_set) { Ddr::ResourceChangeSet.new(resource, user:) }
        let(:resource) { Ddr::Resource.new(ingested_by: 'bob@example.com') }

        before { Ddr.persister.save(resource:) }

        it 'does not set ingested_by' do
          expect { subject.save(change_set:) }.not_to change(resource, :ingested_by)
        end
      end
    end

    describe 'ingestion date' do
      let(:resource) { Ddr::Resource.new }
      let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

      describe 'on create' do
        describe "when it's set" do
          before { resource.ingestion_date = '2017-01-13T19:03:15Z' }

          it 'preserves the date' do
            expect { subject.save(change_set:) }.not_to change(resource, :ingestion_date)
          end
        end

        describe "when it's not set" do
          it 'sets the date' do
            expect { subject.save(change_set:) }.to change(resource, :ingestion_date)
          end
        end
      end
    end

    describe 'granting default roles' do
      let(:resource) { Ddr::Resource.new }
      let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

      let(:metadata_editors) do
        Ddr::Auth::Roles::Role.new(
          role_type: Ddr::Auth::Roles::METADATA_EDITOR.title,
          agent: 'metadata_editors',
          scope: Ddr::Auth::Roles::POLICY_SCOPE
        )
      end
      let(:curators) do
        Ddr::Auth::Roles::Role.new(
          role_type: Ddr::Auth::Roles::CURATOR.title,
          agent: 'curators',
          scope: Ddr::Auth::Roles::POLICY_SCOPE
        )
      end
      let(:downloaders) do
        Ddr::Auth::Roles::Role.new(
          role_type: Ddr::Auth::Roles::DOWNLOADER.title,
          agent: 'downloaders',
          scope: Ddr::Auth::Roles::POLICY_SCOPE
        )
      end

      describe 'change set has default roles' do
        before do
          allow(change_set).to receive(:default_roles) { [metadata_editors, curators, downloaders] }
          subject.save(change_set:)
        end

        it 'add the roles to the resource' do
          expect(resource.roles).to contain_exactly(metadata_editors, curators, downloaders)
        end
      end

      describe 'change set does not have default roles' do
        before do
          allow(change_set).to receive(:default_roles).and_return([])
        end

        it 'adds the default roles' do
          expect { subject.save(change_set:) }.not_to change(resource, :access_role)
        end
      end
    end

    describe 'events' do
      let(:resource_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

      describe 'ingestion' do
        let(:resource) { Ddr::Item.new(id: resource_id) }

        it 'creates an ingestion event' do
          expect { subject.save(change_set:) }.to change {
                                                    Ddr::Events::IngestionEvent.for_object(resource).count
                                                  }.by(1)
        end
      end

      describe 'update' do
        let(:resource) { Ddr.persister.save(resource: Ddr::Item.new(id: resource_id)) }

        before { change_set.title = 'Test Title' }

        it 'creates an update event' do
          expect { subject.save(change_set:) }.to change {
                                                    Ddr::Events::UpdateEvent.for_object(resource).count
                                                  }.by(1)
        end

        describe 'with change_set virtual attributes' do
          let(:user) { build(:user) }

          before do
            change_set.title = 'Changed Title'
            change_set.user = user
            change_set.summary = 'This event rocks!'
            change_set.comment = 'I was testing things'
            change_set.detail = 'A bunch of extra stuff I want to record'
          end

          it 'creates an update event with the attributes' do
            persisted_resource = subject.save(change_set:)
            event = Ddr::Events::UpdateEvent.for_object(persisted_resource).last
            expect(event.user_key).to eq(user.to_s)
            expect(event.summary).to eq 'This event rocks!'
            expect(event.comment).to eq 'I was testing things'
          end
        end
      end

      describe 'delete' do
        let(:resource) { Ddr.persister.save(resource: Ddr::Resource.new(id: resource_id)) }

        it 'creates a deletion event' do
          expect { subject.delete(change_set:) }.to change {
                                                      Ddr::Events::DeletionEvent.for_object(resource).count
                                                    }.from(0).to(1)
        end

        it 'records attributes with the event' do
          allow(resource).to receive(:permanent_id).and_return('foo')
          subject.delete(change_set:)
          event = Ddr::Events::DeletionEvent.for_object(resource).first
          expect(event.permanent_id).to eq 'foo'
        end
      end
    end

    describe 'setting admin policy' do
      describe 'resource is a collection' do
        describe 'change set does not explicitly set admin policy ID' do
          let(:resource) { Ddr::Collection.new(title: 'Test', admin_set: 'dc') }
          let(:change_set) { Ddr::CollectionChangeSet.new(resource) }

          it 'the admin policy id is set to the resource ID' do
            persisted_resource = subject.save(change_set:)
            expect(Ddr.query_service.find_by(id: persisted_resource.id).admin_policy_id).to eq(persisted_resource.id)
          end
        end

        describe 'change set does explicitly set admin policy ID' do
          let(:another_resource) { Ddr.persister.save(resource: Ddr::Collection.new) }
          let(:resource) { Ddr::Collection.new(title: 'Test', admin_set: 'dc') }
          let(:change_set) { Ddr::CollectionChangeSet.new(resource) }

          before do
            change_set.admin_policy_id = another_resource.id
          end

          it 'the admin policy id is set to the one set in the change set' do
            persisted_resource = subject.save(change_set:)
            expect(persisted_resource.admin_policy_id).to eq(another_resource.id)
          end
        end
      end

      describe 'resource is not a collection' do
        let(:resource) { Ddr::Resource.new }
        let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

        it 'the admin policy id is not set' do
          persisted_resource = subject.save(change_set:)
          expect(persisted_resource.admin_policy_id).to be_nil
        end
      end
    end

    describe 'delete ddr file callback' do
      before { allow(ActiveSupport::Notifications).to receive(:instrument).and_call_original }

      let(:resource) { create_for_repository(:component, :with_content_file) }
      let(:change_set) { Ddr::ComponentChangeSet.new(resource) }

      describe 'existing ddr file is not removed' do
        before do
          change_set.title = 'Foo'
          change_set.skip_iiif_updates = true
        end

        it 'does not issue a DELETE_FILE notification' do
          expect(ActiveSupport::Notifications).not_to receive(:instrument).with(described_class::DELETE_FILE)
          subject.save(change_set:)
        end
      end

      describe 'existing ddr file is removed' do
        let(:content_identifier) { resource.content.file_identifier }
        let(:content_path) { resource.content.file_path }
        let(:expected_payload) do
          { file_profile: { ddr_file_type: :content, file_identifier: content_identifier, file_path: content_path },
            resource_id: resource.id }
        end

        before do
          change_set.content = nil
        end

        it 'issues a DELETE_FILE notification' do
          expect(ActiveSupport::Notifications).to receive(:instrument).with(described_class::DELETE_FILE,
                                                                            expected_payload)
          subject.save(change_set:)
        end
      end

      describe 'existing ddr file is replaced' do
        let(:content_identifier) { resource.content.file_identifier }
        let(:content_path) { resource.content.file_path }
        let(:replacement_file) { fixture_file_upload('imageB.tif', 'image/tiff') }
        let(:expected_payload) do
          { file_profile: { ddr_file_type: :content, file_identifier: content_identifier, file_path: content_path },
            resource_id: resource.id }
        end

        before do
          change_set.add_file(replacement_file, :content)
        end

        it 'issues a DELETE_FILE notification' do
          expect(ActiveSupport::Notifications).to receive(:instrument).with(described_class::DELETE_FILE,
                                                                            expected_payload)
          subject.save(change_set:)
        end
      end
    end

    describe 'permanent ID assignment' do
      let(:resource) { Ddr::Resource.new }
      let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

      describe 'when auto assignment is enabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_assign_permanent_id).and_return(true)
        end

        describe 'and a permanent ID is pre-assigned' do
          before do
            change_set.permanent_id = 'foo'
          end

          it 'does not assign a permanent ID' do
            expect(subject.save(change_set:).permanent_id).to eq('foo')
          end
        end

        describe 'and no permanent ID has been pre-assigned' do
          it 'assigns a permanent ID' do
            expect(Ddr::PermanentId).to receive(:assign!).with(an_instance_of(Valkyrie::ID), any_args).and_return(nil)
            subject.save(change_set:)
          end
        end
      end

      describe 'when auto assignment is disabled' do
        before do
          allow(Ddr::Admin).to receive(:auto_assign_permanent_id).and_return(false)
        end

        it 'does not assign a permanent ID' do
          expect(Ddr::PermanentId).not_to receive(:assign!)
          subject.save(change_set:)
        end
      end
    end

    describe 'workflow_state change' do
      let(:resource) do
        Ddr.persister.save(resource: Ddr::Resource.new(id: build(:valkyrie_id),
                                                       workflow_state: 'published'))
      end

      # expect it to fail if resource is published
      describe 'when setting a published workflow_state to nonpublishable' do
        let(:change_set) { Ddr::ResourceChangeSet.new(resource) }

        before do
          change_set.workflow_state = 'nonpublishable'
        end

        it 'raises an error' do
          expect { subject.save(change_set:) }.to raise_error(Ddr::ResourceChangeSetPersister::WontSaveError)
        end
      end
    end
  end
end
