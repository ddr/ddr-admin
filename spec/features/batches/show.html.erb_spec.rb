describe 'ddr/batches/queued.html.erb', type: :feature do
  context 'batch' do
    let(:batch) { FactoryBot.create(:batch_with_basic_ingest_batch_objects) }

    context 'batch info' do
      before { login_as batch.user }

      context 'new batch' do
        before { visit ddr_batch_path(batch) }

        it 'does not have a link to process the batch' do
          expect(page).to have_no_link(I18n.t('ddr.batch.web.action_names.procezz'),
                                       href: procezz_ddr_batch_path(batch))
        end
      end

      context 'ready to process batch' do
        before do
          batch.status = Ddr::Batch::Batch::STATUS_READY
          batch.save
          visit ddr_batch_path(batch)
        end

        it 'has a link to process the batch' do
          expect(page).to have_link(I18n.t('ddr.batch.web.action_names.procezz'), href: procezz_ddr_batch_path(batch))
        end
      end

      context 'validate action' do
        context 'validated and valid' do
          before do
            batch.status = Ddr::Batch::Batch::STATUS_VALIDATED
            batch.save
            visit ddr_batch_path(batch)
          end

          it 'has a link to process the batch' do
            expect(page).to have_link(I18n.t('ddr.batch.web.action_names.procezz'), href: procezz_ddr_batch_path(batch))
          end
        end

        context 'validated and invalid' do
          before do
            batch.status = Ddr::Batch::Batch::STATUS_INVALID
            batch.save
            visit ddr_batch_path(batch)
          end

          it 'has a link to process the batch' do
            expect(page).to have_link(I18n.t('ddr.batch.web.action_names.retry'), href: procezz_ddr_batch_path(batch))
          end
        end
      end

      context 'delete action' do
        context 'not delete-able' do
          [Ddr::Batch::Batch::STATUS_QUEUED, Ddr::Batch::Batch::STATUS_RUNNING,
           Ddr::Batch::Batch::STATUS_VALIDATING, Ddr::Batch::Batch::STATUS_PROCESSING,
           Ddr::Batch::Batch::STATUS_FINISHED, Ddr::Batch::Batch::STATUS_INTERRUPTED,
           Ddr::Batch::Batch::STATUS_RESTARTABLE].each do |status|
            context "status #{status}" do
              before do
                batch.status = status
                batch.save
                visit ddr_batch_path(batch)
              end

              it 'does not have a link to delete the batch' do
                expect(page).to have_no_link("ddr_batch_delete_#{batch.id}")
              end
            end
          end
        end
      end
    end
  end
end
