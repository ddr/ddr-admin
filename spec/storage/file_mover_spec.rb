module Ddr
  module Storage
    RSpec.describe FileMover do
      describe '.call' do
        let(:source_path) { Rails.root.join('spec/fixtures/abc001.tif') }
        let(:source_sha1) { ::Digest::SHA1.hexdigest(::File.read(target_path)) }
        let(:tempdir) { Dir.mktmpdir }
        let(:target_path) { ::File.join(tempdir, 'abc001.tif') }

        after { FileUtils.rm_r(tempdir) }

        describe 'copies file' do
          it 'copies the source file to the target location' do
            described_class.call(source_path, target_path)
            expect(::Digest::SHA1.hexdigest(::File.read(target_path))).to eq(source_sha1)
          end
        end

        describe 'file permissions' do
          before do
            allow(Ddr::Admin).to receive(:stored_file_permissions).and_return(0o400)
          end

          it 'sets the target files permissions correctly' do
            described_class.call(source_path, target_path)
            expect(::File.stat(target_path).mode & 0o777).to eq(Ddr::Admin.stored_file_permissions)
          end
        end
      end
    end
  end
end
