module Ddr
  module Storage
    RSpec.describe FilenameGenerator do
      describe '#generate' do
        let(:base_path) { '/my/base/path/' }
        let(:uuid) { 'ac97a2a9-fab6-4494-9cca-818d7f065486' }

        before { allow(SecureRandom).to receive(:uuid) { uuid } }

        subject { described_class.new(base_path:) }
        it 'generates a pathified path based on a UUID' do
          expect(subject.generate(resource: nil, file: nil, original_filename: nil))
            .to eq(Pathname.new(::File.join(base_path, '/ac/97/a2/', uuid)))
        end
      end
    end
  end
end
