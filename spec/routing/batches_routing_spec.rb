describe 'batches routing', :batch, type: :routing do
  describe 'RESTful routes' do
    it 'has an index route' do
      route = { controller: 'ddr/batches', action: 'index' }
      expect(get: '/ddr/batches').to route_to(route)
      expect(get: ddr_batches_path).to route_to(route)
    end

    it 'has a show route' do
      route = { controller: 'ddr/batches', action: 'show', id: '1' }
      expect(get: '/ddr/batches/1').to route_to(route)
      expect(get: ddr_batch_path(1)).to route_to(route)
    end

    it 'has a destroy route' do
      route = { controller: 'ddr/batches', action: 'destroy', id: '1' }
      expect(delete: '/ddr/batches/1').to route_to(route)
      expect(delete: ddr_batch_path(1)).to route_to(route)
    end
  end

  describe 'non-RESTful routes' do
    it 'has a route for validating a batch' do
      route = { controller: 'ddr/batches', action: 'validate', id: '1' }
      expect(get: '/ddr/batches/1/validate').to route_to(route)
    end

    it 'has a route for processing a batch' do
      route = { controller: 'ddr/batches', action: 'procezz', id: '1' }
      expect(get: 'ddr/batches/1/procezz').to route_to(route)
    end

    it "has a route for 'my batches'" do
      route = { controller: 'ddr/batches', action: 'index', filter: 'current_user' }
      expect(get: 'ddr/my_batches').to route_to(route)
      expect(get: ddr_my_batches_path).to route_to(route)
    end
  end
end
