require 'rails_helper'
require 'support/shared_examples_for_repository_routers'

describe 'components router', :components, type: :routing do
  it_behaves_like 'a repository object router' do
    let(:controller) { 'components' }
  end
  it_behaves_like 'a content-bearing object router', skip: true do
    let(:controller) { 'components' }
  end

  let(:id) { SecureRandom.uuid }

  specify do
    expect(get: "/ddr/components/#{id}/stream").to route_to(controller: 'ddr/components', action: 'stream', id:)
  end

  specify do
    expect(get: "/ddr/components/#{id}/captions").to route_to(controller: 'ddr/components', action: 'captions', id:)
  end
end
