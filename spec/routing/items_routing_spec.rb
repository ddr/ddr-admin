require 'rails_helper'
require 'support/shared_examples_for_repository_routers'

describe 'items router', :items, type: :routing do
  it_behaves_like 'a repository object router' do
    let(:controller) { 'items' }
  end
  it 'has a components route' do
    expect(get: '/ddr/items/841c70d0-8fd4-4e43-b76f-233f9d636eec/components')
      .to route_to(controller: 'ddr/items', action: 'components', id: '841c70d0-8fd4-4e43-b76f-233f9d636eec')
  end
end
