RSpec.describe 'users router', type: :routing do
  it 'has a shibboleth authentication path' do
    expect(get: '/users/auth/shibboleth').to route_to(controller: 'users/omniauth_callbacks', action: 'passthru')
  end

  it 'has a shibboleth authentication path helper' do
    expect(get: user_shibboleth_omniauth_authorize_path).to route_to(controller: 'users/omniauth_callbacks',
                                                                     action: 'passthru')
  end
end
