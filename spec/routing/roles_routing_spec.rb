RSpec.describe 'roles router', type: :routing do
  specify do
    expect(get: '/ddr/roles/batch')
      .to route_to(controller: 'ddr/roles', action: 'batch')
  end

  specify do
    expect(patch: '/ddr/roles/batch')
      .to route_to(controller: 'ddr/roles', action: 'batch')
  end
end
