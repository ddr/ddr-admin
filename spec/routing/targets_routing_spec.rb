require 'rails_helper'
require 'support/shared_examples_for_repository_routers'

describe 'targets router', :targets, type: :routing do
  it 'does not have a new route' do
    expect(get: '/targets/new').not_to be_routable
  end

  it 'does not have a create route' do
    expect(post: '/targets').not_to be_routable
  end

  it_behaves_like 'a repository object router' do
    let(:controller) { 'targets' }
  end
  it_behaves_like 'a content-bearing object router' do
    let(:controller) { 'targets' }
  end
end
