describe 'download routing', :downloads, type: :routing do
  it 'has ddr_file routes' do
    expect(get: '/ddr/download/37940de8-63ad-43ba-a842-3bd881ca7f9c/fits').to route_to(controller: 'ddr/downloads',
                                                                                       action: 'show', id: '37940de8-63ad-43ba-a842-3bd881ca7f9c', ddr_file_type: 'fits')
  end

  it 'has a download route' do
    expect(get: '/ddr/download/37940de8-63ad-43ba-a842-3bd881ca7f9c').to route_to(controller: 'ddr/downloads',
                                                                                  action: 'show', id: '37940de8-63ad-43ba-a842-3bd881ca7f9c')
  end
end
