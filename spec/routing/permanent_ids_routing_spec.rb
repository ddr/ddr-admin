RSpec.describe 'permanent id router', :permanent_ids, type: :routing do
  it 'has an id route' do
    expect(get: '/id/ark:/99999/fk4').to route_to(controller: 'ddr/permanent_ids', action: 'show',
                                                  permanent_id: 'ark:/99999/fk4')
  end
end
