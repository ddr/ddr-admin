require 'rails_helper'
require 'support/shared_examples_for_repository_routers'

describe 'collections router', :collections, type: :routing do
  it_behaves_like 'a repository object router' do
    let(:controller) { 'collections' }
  end
  it 'has a collection_info route' do
    expect(get: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/collection_info')
      .to route_to(controller: 'ddr/collections', action: 'collection_info',
                   id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
  end

  it 'has an items route' do
    expect(get: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/items')
      .to route_to(controller: 'ddr/collections', action: 'items', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
  end

  it 'has an attachments route' do
    expect(get: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/attachments')
      .to route_to(controller: 'ddr/collections', action: 'attachments', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
  end

  it 'has a targets route' do
    expect(get: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/targets')
      .to route_to(controller: 'ddr/collections', action: 'targets', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
  end

  it 'has an export route' do
    expect(post: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/export.csv', type: 'techmd')
      .to route_to(controller: 'ddr/collections', action: 'export', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc',
                   format: 'csv')
    expect(get: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/export')
      .to route_to(controller: 'ddr/collections', action: 'export', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
  end

  it 'has an aspace route' do
    expect(get: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/aspace')
      .to route_to(controller: 'ddr/collections', action: 'aspace', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
    expect(post: '/ddr/collections/5b3b022b-d77e-4de9-a27e-7f5495d965fc/aspace')
      .to route_to(controller: 'ddr/collections', action: 'aspace', id: '5b3b022b-d77e-4de9-a27e-7f5495d965fc')
  end
end
