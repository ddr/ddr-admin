module Ddr
  RSpec.describe ResourceChangeSet do
    describe '.change_set_for' do
      it 'returns an instance of the appropriate change set class initialized with the resource' do
        ['Ddr::Attachment', 'Ddr::Collection', 'Ddr::Component', 'Ddr::Item', 'Ddr::Target'].each do |model|
          resource = model.constantize.new
          expect(described_class.change_set_for(resource)).to be_an_instance_of("#{model}ChangeSet".constantize)
          expect(described_class.change_set_for(resource).resource).to eq(resource)
        end
      end
    end

    describe '#default_roles' do
      subject { described_class.change_set_for(Item.new) }

      its(:default_roles) { is_expected.to be_empty }
    end
  end
end
