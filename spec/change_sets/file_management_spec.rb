module Ddr
  RSpec.describe FileManagement do
    let(:change_set) { Ddr::ComponentChangeSet.new(Ddr::Component.new) }
    let(:image_file) { fixture_file_upload('imageA.tif', 'image/tiff') }
    let(:xml_file)   { fixture_file_upload('fits.xml', 'text/xml') }
    let(:sha1)       { '75e2e0cec6e807f6ae63610d46448f777591dd6b' }

    describe '#add_file' do
      it 'runs a virus scan on the file' do
        expect(change_set).to receive(:virus_scan)
        change_set.add_file image_file, :content
      end

      it 'calls add_ddr_file' do
        expect(change_set).to receive(:add_ddr_file)
        change_set.add_file image_file, :content
      end
    end

    describe '#add_ddr_file' do
      it 'adds a Ddr::File to the relevant attribute' do
        expect(change_set).to receive(:content=).with(Ddr::File)
        change_set.add_ddr_file(image_file, :content, original_filename: image_file.original_filename)
      end

      it 'sets the media type' do
        change_set.add_ddr_file(image_file, :content, original_filename: image_file.original_filename,
                                                      mime_type: 'image/tiff')
        expect(change_set.content.media_type).to eq('image/tiff')
      end

      it 'sets the identifier for the stored file' do
        change_set.add_ddr_file(image_file, :content, original_filename: image_file.original_filename)
        expect(change_set.content.file_identifier).to be_an_instance_of(Valkyrie::ID)
      end

      it 'sets the timestamps' do
        change_set.add_ddr_file(image_file, :content, original_filename: image_file.original_filename)
        expect(change_set.content.created_at).not_to be_nil
        expect(change_set.content.updated_at).not_to be_nil
      end

      it 'sets new_record to false' do
        change_set.add_ddr_file(image_file, :content, original_filename: image_file.original_filename)
        expect(change_set.content.new_record).to be false
      end

      describe 'storage adapter used' do
        describe 'content' do
          it 'uses the default storage adapter' do
            expect(Ddr.storage_adapter).to receive(:upload).and_call_original
            change_set.add_ddr_file(image_file, :content, original_filename: image_file.original_filename)
          end
        end

        describe 'multires_image' do
          it 'uses the multires image disk storage adapter' do
            expect(Valkyrie::StorageAdapter.find(:multires_image_disk)).to receive(:upload).and_call_original
            change_set.add_ddr_file(image_file, :multires_image, original_filename: image_file.original_filename)
          end
        end
      end
    end

    describe '#ddr_file_digest' do
      it 'calculates the file digest' do
        expect(change_set.ddr_file_digest(image_file).value).to eq sha1
      end

      it 'returns a Ddr::Digest' do
        expect(change_set.ddr_file_digest(image_file)).to be_an_instance_of(Ddr::Digest)
      end
    end

    describe '#ddr_file_storage_adapter' do
      describe 'multires_image' do
        let(:expected_adapter_base_path) { Valkyrie::StorageAdapter.find(:multires_image_disk).base_path }

        it 'returns the multires image disk storage adapter' do
          expect(change_set.ddr_file_storage_adapter(:multires_image).base_path).to eq(expected_adapter_base_path)
        end
      end

      describe 'not multires_image' do
        let(:expected_adapter_base_path) { Ddr.storage_adapter.base_path }

        it 'returns the default Ddr storage adapter' do
          expect(change_set.ddr_file_storage_adapter(:content).base_path).to eq(expected_adapter_base_path)
        end
      end
    end
  end
end
