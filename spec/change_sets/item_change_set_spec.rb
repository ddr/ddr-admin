require 'rails_helper'
require 'support/shared_examples_for_resource_change_sets'
require 'valkyrie/specs/shared_specs'

module Ddr
  RSpec.describe ItemChangeSet do
    let(:resource) { Item.new }
    let(:change_set) { described_class.new(resource) }

    it_behaves_like 'a Valkyrie::ChangeSet'

    subject { change_set }

    it_behaves_like 'a DDR resource change set'
    it_behaves_like 'a non-Collection DDR resource change set'
    it_behaves_like 'a DDR resource change set for a resource class that can be published'

    describe 'field cardinality' do
      describe 'single-valued fields' do
        specify do
          %i[parent_id struct_metadata].each do |field|
            expect(subject.multiple?(field)).to be false
          end
        end
      end
    end

    describe 'validations' do
      describe 'parent model class' do
        let(:valkyrie_id) { Valkyrie::ID.new(SecureRandom.uuid) }

        before do
          allow(Ddr.query_service).to receive(:find_by).with(id: valkyrie_id).and_return(parent_resource)
        end

        describe 'when Ddr::Collection' do
          let(:parent_resource) { Ddr::Collection.new }

          it 'considers the parent ID to be valid' do
            subject.validate(parent_id: valkyrie_id)
            expect(subject.errors.messages).not_to have_key(:parent_id)
          end
        end

        describe 'when not Ddr::Collection' do
          let(:parent_resource) { Ddr::Resource.new }

          it 'considers the parent ID to not be valid' do
            subject.validate(parent_id: valkyrie_id)
            expect(subject.errors.added?(:parent_id, I18n.t('ddr.change_set.errors.parent_class',
                                                            parent_class: subject.resource.parent_class.name))).to be true
            expect(subject.errors.added?(:parent_id, 'wrong error message')).to be false
          end
        end
      end
    end
  end
end
