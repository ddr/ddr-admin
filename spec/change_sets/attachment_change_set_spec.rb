require 'rails_helper'
require 'support/shared_examples_for_resource_change_sets'
require 'valkyrie/specs/shared_specs'

module Ddr
  RSpec.describe AttachmentChangeSet do
    let(:resource) { Attachment.new }
    let(:change_set) { described_class.new(resource) }

    it_behaves_like 'a Valkyrie::ChangeSet'

    subject { change_set }

    it_behaves_like 'a DDR resource change set'
    it_behaves_like 'a non-Collection DDR resource change set'
    it_behaves_like 'a DDR resource change set for a resource class that cannot be published'

    describe 'field cardinality' do
      describe 'single-valued fields' do
        specify do
          %i[attached_to_id content extracted_text fits_file].each do |field|
            expect(subject.multiple?(field)).to be false
          end
        end
      end
    end
  end
end
