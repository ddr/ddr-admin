require 'rails_helper'
require 'support/shared_examples_for_resource_change_sets'
require 'valkyrie/specs/shared_specs'

module Ddr
  RSpec.describe CollectionChangeSet do
    let(:resource) { Collection.new }
    let(:change_set) { described_class.new(resource) }

    it_behaves_like 'a Valkyrie::ChangeSet'

    subject { change_set }

    it_behaves_like 'a DDR resource change set'
    it_behaves_like 'a Collection DDR resource change set'
    it_behaves_like 'a DDR resource change set for a resource class that can be published'

    describe 'field cardinality' do
      describe 'single-valued fields' do
        specify do
          %i[admin_set struct_metadata].each do |field|
            expect(subject.multiple?(field)).to be false
          end
        end
      end
    end

    describe 'required fields' do
      specify do
        expect(subject.required?(:admin_set)).to be true
        expect(subject.required?(:title)).to be true
      end
    end

    describe 'validation' do
      describe 'without an admin set' do
        let(:resource) { Collection.new(title: 'Foo') }

        specify { expect(subject).not_to be_valid }
      end

      describe 'without a title' do
        let(:resource) { Collection.new(admin_set: 'dc') }

        specify { expect(subject).not_to be_valid }
      end

      describe 'with an admin set id and a title' do
        let(:resource) { Collection.new(admin_set: 'dc', title: 'Foo') }

        specify { expect(subject).to be_valid }
      end
    end

    # Role settings from config/default_roles.yml
    describe '#default_roles' do
      describe "for 'dc' admin set" do
        let(:expected_roles) do
          [
            # rubenstein admin set roles
            Ddr::Auth::Roles::Role.new(agent: Ddr::Admin.dc_program_group,
                                       scope: 'policy',
                                       role_type: 'Curator'),
            Ddr::Auth::Roles::Role.new(agent: Ddr::Admin.rubenstein_staff_group,
                                       scope: 'policy',
                                       role_type: 'Downloader'),
            # Default roles
            Ddr::Auth::Roles::Role.new(agent: Ddr::Admin.curators_group,
                                       scope: 'policy',
                                       role_type: 'Curator')
          ]
        end

        before { change_set.admin_set = 'dc' }

        # Use match_array becasue we don't care about the order of roles
        its(:default_roles) { is_expected.to match_array(expected_roles) }
      end

      describe "for 'rubenstein' admin set" do
        let(:expected_roles) do
          [
            # rubenstein admin set roles
            Ddr::Auth::Roles::Role.new(agent: Ddr::Admin.dc_program_group,
                                       scope: 'policy',
                                       role_type: 'Curator'),
            Ddr::Auth::Roles::Role.new(agent: Ddr::Admin.rubenstein_staff_group,
                                       scope: 'policy',
                                       role_type: 'Downloader'),
            # Default roles
            Ddr::Auth::Roles::Role.new(agent: Ddr::Admin.curators_group,
                                       scope: 'policy',
                                       role_type: 'Curator')
          ]
        end

        before { change_set.admin_set = 'rubenstein' }

        # Use match_array becasue we don't care about the order of roles
        its(:default_roles) { is_expected.to match_array(expected_roles) }
      end
    end
  end
end
