---
$id: https://api.repository.duke.edu/schemas/resource

$schema: https://json-schema.org/draft/2020-12/schema

title: DDR Resource Schema

description: >-
  The public API of a resource in the [Duke Digital Repository](https://repository.duke.edu).
  More specific requirements may apply to individual resource types and operations, such
  as create and update.

type: object

additionalProperties: false

properties:
  id:
    title: Resource ID
    description: Unique resource identifier assigned by the repository.
    type: string
    format: uuid
    readOnly: true

  model:
    title: Model
    description: The model type of the resource.
    type: string
    enum: <%= Ddr::Resource::MODELS %>
    $comment: >-
      The values used are not namespaced -- i.e., they are the class names
      of the resource models within the 'Ddr' namespace. For example, the
      value 'Collection' is used in lieu of the resource class 'Ddr::Collection'.
      This property is required for POST operations, but is read-only otherwise.

  created_at:
    title: Created At
    description: Creation time of the resource record (may differ from ingestion date).
    $comment: System-managed property.
    type: string
    format: date-time
    readOnly: true

  updated_at:
    title: Updated At
    description: Modification time of the resource record.
    $comment: System-managed property.
    type: string
    format: date-time
    readOnly: true

  optimistic_lock_token:
    title: Optimistic Lock Token
    description: A token required for update with optimistic locking.
    type: string
    $comment: >-
      This property is required for PATCH operations, but is read-only otherwise.

  metadata:
    $ref: '/api/schemas/metadata.json'

  roles:
    title: Roles
    description: Role-based access control assertions on the resource
    type: array
    uniqueItems: true
    items:
      title: Role
      description: Role-based access control assertion applied to a resource
      type: object
      required:
        - role_type
        - agent
        - scope
      additionalProperties: false
      properties:
        role_type:
          title: Role Type
          description: Type of access granted
          type: string
          enum: <%= Ddr::Auth::Roles.titles %>
        agent:
          title: Agent
          description: Person or group to whom access is granted
          type: string
        scope:
          title: Scope
          description: Scope of the access granted
          type: string
          enum: <%= Ddr::Auth::Roles::SCOPES %>

  files:
    title: Files
    description: Files attached to the resource
    type: object
    additionalProperties: false
    readOnly: true
    $comment: >-
      This property is marked readOnly because it cannot be modified by a PATCH operation
      on the the resource.  Files are added by PUT operations on file field endpoints.
    properties:
      caption:
        title: Caption File
        description: >-
          A file containing captions for an A/V file.
        $ref: "#/$defs/file"
      content:
        title: Original File
        description: >-
          The original 'content' of the resource.
        $ref: "#/$defs/file"
      derived_image:
        title: Derived Image
        description: >-
          A reduced-size service file derived from the original image file.
        $ref: "#/$defs/file"
      extracted_text:
        title: Extracted Text
        description: >-
          Plain text content extracted from the original file.
          Not currently implemented.
        $ref: "#/$defs/file"
      fits_file:
        title: FITS File
        description: >-
          [FITS XML](https://projects.iq.harvard.edu/fits/fits-xml)
          file characterization data for the original file.
        $ref: "#/$defs/file"
      iiif_file:
        title: IIIF Presentation Manifest
        description: >-
          IIIF Presentation API 3.0 Manifest for the resource.
        $ref: "#/$defs/file"
      intermediate_file:
        title: Intermediate File
        description: >-
          A modified version of the original file used for derivative creation.
        $ref: "#/$defs/file"
      multires_image:
        title: Multi-resolution Image
        description: >-
          A derivative file to support zooming and panning in a GUI viewer.
        $ref: "#/$defs/file"
      streamable_media:
        title: Streamable Media
        description: >-
          A derivative of an original A/V file used for streaming services.
        $ref: "#/$defs/file"
      struct_metadata:
        title: Structural Metadata
        description: >-
          Structural metadata for the resource.
          Currently, this implemented for Collection and Item resources using a
          [METS StructMap](https://www.loc.gov/standards/mets/METSOverview.v2.html#structmap)
        $ref: "#/$defs/file"
      thumbnail:
        title: Thumbnail Image
        description: >-
          A small derivative or representational image of the resource.
        $ref: "#/$defs/file"

  related:
    title: Related Resource IDs
    type: object
    additionalProperties: false
    properties:
      collection_id:
        title: Collection ID
        description: Collection ID of which this resource is a member.
        $comment: >-
          For historical reasons this property is internally named 'admin_policy_id'.
        type: string
        format: uuid
        x-original-name: admin_policy_id
      attached_to_id:
        title: Attached To ID
        description: ID of the resource to which this resource is attached.
        type: string
        format: uuid
      parent_id:
        title: Parent ID
        description: ID of the 'parent' of this resource.
        type: string
        format: uuid
      target_id:
        title: Target ID
        description: ID of the Target associated with this resource.
        type: string
        format: uuid

$defs:
  file:
    title: File
    description: Representation of a file (bitstream) associated with a resource.
    type: object
    required:
      - original_filename
    additionalProperties: false
    properties:
      original_filename:
        title: Original Filename
        description: >-
          Original name of file as ingested into the repository.
        type: string
      media_type:
        title: Media Type
        description: Standard internet media type descriptor for file content.
        type: string
      file_path:
        title: File Path
        description: >-
          Path to the file on a storage device.
        type: string
        readOnly: true
      sha1:
        title: SHA1 Digest
        description: SHA1 digest of file contents.
        type: string
        readOnly: true
      created:
        title: Creation Date/Time
        description: >-
          Creation date/time of the file in the repository (not creation time of original file).
        type: string
        format: date-time
        readOnly: true
      updated:
        title: Modification Date/Time
        description: >-
          Modification date/time of the file in the repository (not modification time of original file).
        type: string
        format: date-time
        readOnly: true
