bind 'tcp://0.0.0.0:%s' % ENV.fetch('RAILS_PORT', nil)
activate_control_app 'tcp://0.0.0.0:%s' % ENV.fetch('PUMA_CONTROL_APP_PORT', nil), { no_token: true }
