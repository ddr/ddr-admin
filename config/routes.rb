Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/queues'

  mount Ddr::API::Root => Ddr::API.path

  mount Blacklight::Engine => '/'
  root to: 'catalog#index'
  concern :searchable, Blacklight::Routes::Searchable.new

  resource :catalog, only: [:index], as: 'catalog', path: '/catalog', controller: 'catalog' do
    concerns :searchable
  end
  concern :exportable, Blacklight::Routes::Exportable.new

  resources :solr_documents, only: [:show], path: '/catalog', controller: 'catalog' do
    concerns :exportable
  end

  resources :bookmarks do
    concerns :exportable

    collection do
      delete 'clear'
    end
  end

  if Rails.env.production?
    devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  else
    devise_for :users, controllers: {
      omniauth_callbacks: 'users/omniauth_callbacks',
      sessions: 'users/sessions',
      registrations: 'users/registrations'
    }
  end

  get 'id/*permanent_id', to: 'ddr/permanent_ids#show'

  get 'admin/api'
  get 'admin/expire_public_rails_cache'
  get 'admin/queues'

  def id_constraint
    /\h{8}-\h{4}-\h{4}-\h{4}-\h{12}/
  end

  def content_routes
    get 'upload'
    patch 'upload'
    get 'files'
  end

  def event_routes
    get 'events'
    get 'events/:event_id', action: :event
  end

  def publication_routes
    Ddr::V3::PublicationService.workflow_event_names.each do |event|
      get event
    end
  end

  def roles_routes
    get 'roles'
    patch 'roles'
  end

  def structure_routes
    get 'generate_structure'
    get 'structural_metadata'
  end

  def iiif_routes
    get 'iiif'
    get 'generate_iiif'
  end

  def amd_routes
    get 'admin_metadata'
    patch 'admin_metadata'
  end

  def index_doc_routes
    get 'index_doc'
  end

  def repository_routes
    event_routes
    roles_routes
    amd_routes
    index_doc_routes
  end

  def repository_contraints
    { id: id_constraint }
  end

  def no_repository_routes_for(_name)
    %i[index destroy]
  end

  def repository_options(name)
    { except: no_repository_routes_for(name),
      constraints: repository_contraints }
  end

  namespace :ddr do
    scope 'superuser', as: 'superuser' do
      get 'sign_in', to: 'superuser#create'
      get 'sign_out', to: 'superuser#destroy'
    end

    def repository_resource(name)
      resources name, repository_options(name) do
        member do
          repository_routes
          yield if block_given?
        end
      end
    end

    def repository_content_resource(name)
      repository_resource name do
        content_routes
      end
    end

    repository_resource :collections do
      content_routes
      publication_routes
      structure_routes
      iiif_routes
      get 'collection_info'
      get 'items'
      get 'attachments'
      get 'targets'
      get 'export'
      post 'export'
      get 'aspace'
      post 'aspace'
    end

    repository_resource :items do
      content_routes
      publication_routes
      structure_routes
      iiif_routes
      get 'components'
    end

    repository_resource :components do
      content_routes
      publication_routes
      get 'intermediate'
      get 'stream'
      get 'captions'
    end

    repository_content_resource :attachments

    repository_content_resource :targets

    resources :batches, only: %i[index show destroy] do
      member do
        get 'procezz'
        get 'retry'
        post 'retry'
        get 'validate'
      end
    end

    get 'my_batches' => 'batches#index', filter: 'current_user'

    resources :batch_objects, only: :show do
      resources :batch_object_datastreams, only: :index
      resources :batch_object_relationships, only: :index
    end

    resources :export_files, only: %i[index new create]

    resources :file_uploads, only: %i[new create show]

    resources :ingest_folders, only: %i[new create show] do
      member do
        get 'procezz'
      end
    end

    resources :metadata_files, only: %i[new create show] do
      member do
        get 'procezz'
      end
    end

    resources :manifest_based_ingests, only: %i[new create show]

    resources :nested_folder_ingests, only: %i[new create show]

    resources :standard_ingests, only: %i[new create show]

    # Downloads
    get 'download/:id(/:ddr_file_type)' => 'downloads#show', :constraints => { id: id_constraint }, as: 'download'

    get 'roles/batch',   to: 'roles#batch'
    patch 'roles/batch', to: 'roles#batch'
  end
end
