Rails.application.config.after_initialize do
  # Batch Processing events
  ActiveSupport::Notifications.subscribe('started.batch.batch.ddr', Ddr::Batch::MonitorBatchStarted)
  ActiveSupport::Notifications.subscribe('restarted.batch.batch.ddr', Ddr::Batch::MonitorBatchRestarted)
  ActiveSupport::Notifications.subscribe('finished.batch.batch.ddr', Ddr::Batch::MonitorBatchFinished)

  # Ingestion
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::INGEST, Ddr::Events::IngestionEvent)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::INGEST, Ddr::UpdateDerivatives)

  # Update
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::UPDATE, Ddr::Events::UpdateEvent)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::UPDATE, Ddr::PermanentId)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::UPDATE, Ddr::UpdateDerivatives)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::UPDATE) do |*args|
    case args.last[:model]
    when 'Ddr::Collection'
      Ddr::ReindexCollectionContents.call(*args)
    when 'Ddr::Item'
      Ddr::ReindexItemChildren.call(*args)
    end
  end
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::UPDATE) do |*args|
    Ddr::V3::FileUpdateService[:iiif_file].handle_notification_event(*args) unless args.last[:skip_iiif_updates]
  end

  # Delete
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::DELETE, Ddr::PermanentId)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::DELETE, Ddr::Events::DeletionEvent)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::DELETE, Ddr::DeletedResource)

  # File deletion
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::DELETE_FILE, Ddr::UpdateDerivatives)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::DELETE_FILE, Ddr::DeletedDdrFile)

  # Ingest/upload batch preparation
  ActiveSupport::Notifications.subscribe(Ddr::FileUpload::FINISHED, Ddr::MonitorFileUpload)
  ActiveSupport::Notifications.subscribe(Ddr::IngestFolder::FINISHED, Ddr::MonitorIngestFolder)
  ActiveSupport::Notifications.subscribe(Ddr::ManifestBasedIngest::FINISHED, Ddr::MonitorManifestBasedIngest)
  ActiveSupport::Notifications.subscribe(Ddr::MetadataFile::FINISHED, Ddr::MonitorMetadataFile)
  ActiveSupport::Notifications.subscribe(Ddr::NestedFolderIngest::FINISHED, Ddr::MonitorNestedFolderIngest)
  ActiveSupport::Notifications.subscribe(Ddr::StandardIngest::FINISHED, Ddr::MonitorStandardIngest)

  # Fixity check
  ActiveSupport::Notifications.subscribe(Ddr::FixityCheck::FIXITY_CHECK, Ddr::Events::FixityCheckEvent)

  # Structural metadata creation and maintenance subscriptions
  ActiveSupport::Notifications.subscribe('success.batch.batch.ddr', Ddr::SetDefaultStructuresAfterSuccessfulBatchIngest)
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::INGEST) do |*args|
    unless args.last[:skip_structure_updates]
      Ddr::SetDefaultStructure.call(*args) if ['Ddr::Collection', 'Ddr::Item'].include?(args.last[:model])
      Ddr::UpdateParentStructure.call(*args) if ['Ddr::Item', 'Ddr::Component'].include?(args.last[:model])
    end
  end
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::UPDATE) do |*args|
    Ddr::UpdateParentStructure.call(*args) if !args.last[:skip_structure_updates] && ['Ddr::Item',
                                                                                      'Ddr::Component'].include?(args.last[:model])
  end
  ActiveSupport::Notifications.subscribe(Ddr::ResourceChangeSetPersister::DELETE) do |*args|
    Ddr::UpdateParentStructure.call(*args) if !args.last[:skip_structure_updates] && ['Ddr::Item',
                                                                                      'Ddr::Component'].include?(args.last[:model])
  end
end
