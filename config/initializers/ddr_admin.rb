Rails.application.config.to_prepare do
  Blacklight::Configuration.default_values[:http_method] = :post
  Blacklight::Configuration.default_values[:bootstrap_version] = '4'
end
