# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf

# The following sets preferred file extensions for exports

Rails.application.config.to_prepare do
  require 'ddr'

  Ddr.preferred_file_extensions.keys.each do |key|
    MIME::Types[key].first.preferred_extension = Ddr.preferred_file_extensions[key]
  end

  # Add any additional extension preferences like this:
  MIME::Types['image/jpeg'].first.preferred_extension = 'jpg'

  # Monkey patches Paperclip issue in which a CSV file gets the
  # invalid media type 'application/csv' from the `file' command
  # and is considered a "content type spoof".
  # Credit: https://github.com/thoughtbot/paperclip/issues/1924#issuecomment-740673420
  MIME::Types.add(MIME::Type.new(['application/csv', 'csv']), true)
end
