# ActiveSupport::Dependencies.
#    autoload_paths.
#    delete("#{Rails.root}/spec")

Rails.autoloaders.main.ignore(
  'lib/ddr/scripts',
  'lib/valkyrie/specs',
  'spec'
)

Rails.autoloaders.each do |autoloader|
  autoloader.inflector = Zeitwerk::Inflector.new
  autoloader.inflector.inflect(
    'abstract_api' => 'AbstractAPI',
    'admin_sets_api' => 'AdminSetsAPI',
    'api' => 'API',
    'api_auth_context' => 'APIAuthContext',
    'api_auth_middleware' => 'APIAuthMiddleware',
    'api_data_type' => 'APIDataType',
    'api_generator' => 'APIGenerator',
    'api_param' => 'APIParam',
    'batches_api' => 'BatchesAPI',
    'batch_objects_api' => 'BatchObjectsAPI',
    'cancan' => 'CanCan',
    'csv_formatter' => 'CSVFormatter',
    'csv_helper' => 'CSVHelper',
    'csv_stream' => 'CSVStream',
    'deleted_files_api' => 'DeletedFilesAPI',
    'events_api' => 'EventsAPI',
    'groups_api' => 'GroupsAPI',
    'index_api' => 'IndexAPI',
    'oauth_config' => 'OAuthConfig',
    'oauth_request' => 'OAuthRequest',
    'openapi' => 'OpenAPI',
    'openapi_document' => 'OpenAPIDocument',
    'premis_xml_document' => 'PremisXMLDocument',
    'premis_xml_event' => 'PremisXMLEvent',
    'queues_api' => 'QueuesAPI',
    'reports_api' => 'ReportsAPI',
    'resources_api' => 'ResourcesAPI',
    'schemas_api' => 'SchemasAPI',
    'users_api' => 'UsersAPI'
  )
end
