Ezid::Client.configure do |config|
  config.password = ENV['EZID_PASSWORD'] || Rails.application.credentials.ezid_password
end
