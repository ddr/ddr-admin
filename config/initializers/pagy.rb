# frozen_string_literal: true

# Pagy initializer file
# Customize only what you really need and notice that the core Pagy works also without any of the following lines.
# Should you just cherry pick part of this file, please maintain the require-order of the extras

Pagy::DEFAULT[:items] = 20 # default number of results per page
Pagy::DEFAULT[:count_args] = [] # don't send args to count() method call on collection

# Headers extra: http response headers (and other helpers) useful for API pagination
require 'pagy/extras/headers'
# Pagy::DEFAULT[:headers] = { page: 'Current-Page',
#                            items: 'Page-Items',
#                            count: 'Total-Count',
#                            pages: 'Total-Pages' }     # default

# Items extra: Allow the client to request a custom number of items per page with an optional selector UI
require 'pagy/extras/items'
# set to false only if you want to make :items_extra an opt-in variable
# Pagy::DEFAULT[:items_extra] = false    # default true
Pagy::DEFAULT[:items_param] = :per_page # default
Pagy::DEFAULT[:max_items] = ENV.fetch('DDR_API_MAX_PER_PAGE', 1000).to_i

# Overflow extra: Allow for easy handling of overflowing pages
require 'pagy/extras/overflow'
# Pagy::DEFAULT[:overflow] = :empty_page    # default  (other options: :last_page and :exception)

# Trim extra: Remove the page=1 param from links
require 'pagy/extras/trim'
# set to false only if you want to make :trim_extra an opt-in variable
# Pagy::DEFAULT[:trim_extra] = false # default true

# When you are done setting your own default freeze it, so it will not get changed accidentally
Pagy::DEFAULT.freeze
