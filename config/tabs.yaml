##
## Tabs and Actions
##
# See Ddr::V3::Tabs, Ddr::V3::Tab, and Ddr::V3::TabAction.
#
# Tab Keys:
#   name
#     A string used to identify the tab in the UI:
#     - As part of the CSS id "tab_#{name}"
#     - As the default name of the partial for the tab content
#
#   partial
#     Optional custom partial name for the tab (i.e., different from the value of `name')
#
#   title
#     The text to render on the tab
#
#   async
#     A boolean (default: false) indicating whether the tab content should load asynchronously (XHR)
#
#   actions
#     A list of zero or more actions associated with the tab. The actions which are "exposed" on
#     any given request depend on the roles assigned to the resource and any conditions attached
#     to the action. See the 'Action Keys' section
#
#
# Action Keys:
#
#   name
#     A string used to identify the action
#
#   title
#     A string to display for the action, e.g., in a link
#
#   if
#     A public method to call on the action, where the result
#     is interpreted as a condition on exposing the action.
#
#   unless
#     A public method to call on the action, where the result
#     is interpreted as a negative condition on exposing the action.
#
#   permission
#     A DDR permission required on the resource for the action.
#
#   url_options
#     A hash or string to be used by the `url_for' helper to
#     generate a URL for the action.
#
---
#
# Generic tab definitions
#
# N.B. The partial rendered for a tab corresponds to its `name' value.
#
tabs:
  admin_metadata: &admin_metadata
    name: admin_metadata
    title: Admin MD
    actions:
      - name: edit_admin_metadata
        title: Edit
        permission: admin_metadata
        url_options:
          action: admin_metadata
          id: <%%= resource.id %>

  descriptive_metadata: &descriptive_metadata
    name: descriptive_metadata
    title: Descriptive MD
    actions:
      - name: edit_descriptive_metadata
        title: Edit
        permission: edit
        url_options:
          action: edit
          id: <%%= resource.id %>

  embedded_viewer: &embedded_viewer
    name: embedded_viewer
    title: View
    navbar: true
    async: false
    actions:
      - name: view_on_public
        title: View On Public Site
        if: published?
        url_options: <%%= Ddr::V3::IiifPresentationService::SERVER_URL %>/id/<%%= resource.permanent_id %>
        target: _blank
      - name: view_on_preview
        title: View On Preview Site
        if: previewable?
        url_options: https://preview.repository.duke.edu/id/<%%= resource.permanent_id %>
        target: _blank

  files: &files
    name: files
    title: Files
    actions: []

  iiif: &iiif
    name: iiif
    title: IIIF
    async: true
    actions:
      - name: generate_iiif
        title: Generate IIIF Manifest
        unless: has_iiif_file?
        permission: generate_iiif
        url_options:
          action: generate_iiif
          id: <%%= resource.id %>
      - name: regenerate_iiif
        title: Re-Generate IIIF Manifest
        if: has_iiif_file?
        permission: generate_iiif
        url_options:
          action: generate_iiif
          id: <%%= resource.id %>

  index_doc: &index_doc
    name: index_doc
    title: Index
    async: false
    actions: []

  roles: &roles
    name: roles
    title: Roles
    async: true
    actions:
      - name: modify_roles
        title: Modify
        permission: grant
        url_options:
          action: roles
          id: <%%= resource.id %>

  structural_metadata: &structural_metadata
    name: structural_metadata
    title: Structural MD
    async: true
    actions:
      - name: generate_struct_metadata
        title: Generate Structural Metadata
        unless: has_struct_metadata?
        permission: generate_structure
        url_options:
          action: generate_structure
          id: <%%= resource.id %>
      - name: regenerate_struct_metadata
        title: Re-Generate Structural Metadata
        if: has_struct_metadata?
        permission: generate_structure
        url_options:
          action: generate_structure
          id: <%%= resource.id %>

  tech_metadata: &tech_metadata
    name: tech_metadata
    title: Technical MD
    async: false
    actions:
      - name: download_fits_file
        title: FITS XML
        if: has_fits_file?
        permission: download_fits_file
        url_options:
          controller: ddr/downloads
          action: show
          ddr_file_type: fits_file
          inline: 'true'

  workflow: &workflow
    name: workflow
    title: Workflow
    navbar: true
    actions:
<% Ddr::V3::PublicationService::WORKFLOW_EVENTS.each do |event| %>
      - name: <%= event %>
  <% if event == :publish %>
        title: <%% if published? %>Re-Publish<%% else %>Publish<%% end %>
  <% else %>
        title: <%= event.to_s.titleize %>
  <% end %>
        if: can_<%= event %>?
        permission: <%= event %>
        url_options:
          action: <%= event %>
          id: <%%= resource.id %>
<% end %>

#
# Tabs for each resource model
#
collection:
  - *descriptive_metadata
  - *admin_metadata
  - *roles
  - *files
  - *structural_metadata
  - *iiif
  - *index_doc
  - *workflow
  - name: actions
    title: Actions
    navbar: false
    actions:
      - name: ingest_manifest
        title: Manifest-Based Ingest
        permission: add_children
        url_options:
          controller: ddr/manifest_based_ingests
          action: new
          ddr_manifest_based_ingest:
            collection_id: <%%= resource.id %>
      - name: ingest_folder
        title: DPC Folder Ingest
        permission: add_children
        url_options:
          controller: ddr/ingest_folders
          action: new
          ddr_ingest_folder:
            collection_id: <%%= resource.id %>
      - name: ingest_nested_folder
        title: Nested Folder Ingest
        permission: add_children
        url_options:
          controller: ddr/nested_folder_ingests
          action: new
          ddr_nested_folder_ingest:
            collection_id: <%%= resource.id %>
      - name: ingest_standard
        title: Standard Ingest
        permission: add_children
        url_options:
          controller: ddr/standard_ingests
          action: new
          ddr_standard_ingest:
            collection_id: <%%= resource.id %>
      - name: upload_captions
        title: Upload Component Caption Files
        permission: upload
        url_options:
          controller: ddr/file_uploads
          action: new
          ddr_file_upload:
            collection_id: <%%= resource.id %>
            ddr_file_name: caption
      - name: upload_intermediate_files
        title: Upload Component Intermediate Files
        permission: upload
        url_options:
          controller: ddr/file_uploads
          action: new
          ddr_file_upload:
            collection_id: <%%= resource.id %>
            ddr_file_name: intermediate_file
      - name: upload_streamable_media
        title: Upload Component Streamable Media Files
        permission: upload
        url_options:
          controller: ddr/file_uploads
          action: new
          ddr_file_upload:
            collection_id: <%%= resource.id %>
            ddr_file_name: streamable_media
      - name: upload_thumbnails
        title: Upload Component Thumbnail Files
        permission: upload
        url_options:
          controller: ddr/file_uploads
          action: new
          ddr_file_upload:
            collection_id: <%%= resource.id %>
            ddr_file_name: thumbnail
      - name: export_metadata
        title: 'Export Descriptive & Administrative Metadata'
        permission: export
        url_options:
          action: export
          id: <%%= resource.id %>
      - name: export_aspace
        title: Export Digital Object info for ArchivesSpace
        permission: export
        url_options: '/api/resources/<%%= resource.id %>/children.csv?fields=local_id,aspace_id,ead_id,permanent_id,permanent_url,display_format,title'
      - name: create_aspace_digital_objects
        title: Create Digital Objects in ArchivesSpace
        url_options:
          action: aspace
          id: <%%= resource.id %>
      - name: export_techmd
        title: Export Technical Metadata
        permission: export
        url_options: '/api/resources/<%%= resource.id %>/technical_metadata.csv'
      - name: export_checksums
        title: Export Checksums
        permission: export
        url_options: '/api/resources/<%%= resource.id %>/members.csv?model=Component&fields=permanent_id&csv_fields=sha1,original_filename'
  - name: collection_info
    title: Collection Info
    async: true
    actions:
      - title: Download
        url_options:
          action: collection_info
          format: csv

item:
  - *descriptive_metadata
  - *admin_metadata
  - *roles
  - *files
  - *structural_metadata
  - *iiif
  - *index_doc
  - *workflow
  - *embedded_viewer

component:
  - *tech_metadata
  - *descriptive_metadata
  - *admin_metadata
  - *roles
  - *files
  - *index_doc
  - *workflow

target:
  - *tech_metadata
  - *descriptive_metadata
  - *admin_metadata
  - *roles
  - *files
  - *index_doc

attachment:
  - *tech_metadata
  - *descriptive_metadata
  - *admin_metadata
  - *roles
  - *files
  - *index_doc
