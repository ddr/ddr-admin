module BlacklightHelper
  include Blacklight::BlacklightHelperBehavior

  # Override of Blacklight::UrlHelperBehavior#link_to_document
  def link_to_document(doc, field_or_opts = nil, _opts = { counter: nil })
    label = case field_or_opts
            when NilClass, Hash
              document_presenter(doc).heading
            else
              field_or_opts
            end

    link_to label, document_or_object_url(doc)
  end

  # Overrides Blacklight core helper
  # We want facets on the left (index view) but any other sidebars on the right
  # https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/layout_helper_behavior.rb
  def main_content_classes
    classes = %w[col-md-8 col-lg-9]
    classes << if action_name == 'index'
                 'order-2'
               else
                 'order-1'
               end
    classes.join(' ')
  end

  # Overrides Blacklight core helper
  # We want facets on the left (index view) but other sidebars on the right
  # https://github.com/projectblacklight/blacklight/blob/main/app/helpers/blacklight/layout_helper_behavior.rb
  def sidebar_classes
    classes = %w[col-md-4 col-lg-3]
    classes << if action_name == 'index'
                 'order-1'
               else
                 'order-2'
               end
    classes.join(' ')
  end
end
