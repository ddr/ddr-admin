module IngestFoldersHelper
  def permitted_ingest_folder_bases
    Ddr::IngestFolder.permitted_base_paths
  end

  def excluded_file_list
    display = ''
    @scan_results.excluded_files.each do |exc|
      display << content_tag(:li, exc)
    end
    display.html_safe
  end

  def file_count
    pluralize(@scan_results.file_count, Ddr::IngestFolder::FILE_MODEL.demodulize)
  end

  def parent_count
    pluralize(@scan_results.parent_count, parent_model)
  end

  def parent_model
    Ddr::IngestFolder::PARENT_MODEL
  end

  def target_count
    pluralize(@scan_results.target_count, Ddr::IngestFolder::TARGET_MODEL.demodulize)
  end
end
