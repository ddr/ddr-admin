module ApplicationHelper
  def embedded_viewer_url
    prefix = if current_object.workflow_state == 'published'
               Ddr::V3::IiifPresentationService::SERVER_URL
             else # previewable
               'https://preview.repository.duke.edu'
             end

    "#{prefix}/catalog/#{current_object.id}?embed=true"
  end

  def embedded_viewer?
    return false unless %w[published previewable].include?(current_object.workflow_state)
    return true if current_object.item? && %w[image folder audio video].include?(current_object.display_format)

    false
  end

  def supports_policy_roles
    current_object.is_a?(Ddr::Collection) || current_object.is_a?(Ddr::Item)
  end

  def initial_roles_to_modify
    current_object.roles.map { |r| r.to_h.slice(:role_type, :agent, :scope) }
  end

  def underscored_model_name(model)
    model.gsub('::', '_').tableize.singularize
  end

  def render_admin_metadata_form_field(form, field)
    render partial: "admin_metadata_form/#{field}", locals: { f: form }
  rescue ActionView::MissingTemplate
    if change_set.multiple?(field)
      render partial: 'admin_metadata_form/multi_valued', locals: { field: }
    else
      render partial: 'admin_metadata_form/generic', locals: { f: form, field: }
    end
  end

  def admin_metadata_form_field_label(field)
    I18n.t("ddr.resource.#{field}", default: field.to_s.titleize)
  end

  def research_help_contact_options_for_select
    options_from_collection_for_select(Ddr::Contact.all, :slug, :name, current_object.research_help_contact)
  end

  def rights_options_for_select(value)
    options_from_collection_for_select(Ddr::RightsStatement.all, :url, :title, value)
  end

  def admin_set_options_for_select
    options_from_collection_for_select(Ddr::AdminSet.all, :code, :title, current_object.admin_set)
  end

  def checksum_type_options_for_select
    Ddr::Files::CHECKSUM_TYPES.values.map { |v| [v, v] }
  end

  def viewing_direction_options_for_select
    Ddr::API::ResourceEntity.metadata_properties.dig('viewing_direction', 'enum').map { |v| [v.tr('-', ' '), v] }
  end

  def internal_uri_to_pid(args)
    ActiveFedora::Base.pid_from_uri(args[:document][args[:field]])
  end

  def internal_uri_to_link(args)
    pid = internal_uri_to_pid(args).first
    # Depends on Blacklight::SolrHelper#get_solr_response_for_doc_id
    # having been added as a helper method to CatalogController
    _, doc = get_solr_response_for_doc_id(pid)
    # XXX This is not consistent with Ddr::Models::Base#title_display
    title = doc.nil? ? pid : doc.fetch(Ddr::IndexFields::TITLE, pid)
    link_to(title, catalog_path(pid), class: 'parent-link').html_safe
  end

  def render_object_title
    current_document.title
  end

  def object_display_title(id)
    return unless id.present?

    begin
      object = Ddr.query_service.find_by(id:)
      object.title_display if object.respond_to?(:title_display)
    rescue Valkyrie::Persistence::ObjectNotFoundError
      log.error("Unable to find #{id} in repository")
    end
  end

  def file_upload_link(ddr_file)
    permission = current_object.has_file?(ddr_file) ? :replace : :update

    link_to_if can?(permission, current_object),
               '<i class="fas fa-upload"></i>'.html_safe,
               url_for(id: current_object, action: 'upload', ddr_file:),
               { title: "Upload #{ddr_file} file", 'data-toggle' => 'tooltip' }
  end

  def file_download_link(ddr_file)
    permission = :"download_#{ddr_file}"

    link_to_if can?(permission, current_object),
               '<i class="fas fa-download"></i>'.html_safe,
               ddr_download_path(current_object.id, ddr_file),
               { 'title' => "Download #{ddr_file} file", 'data-toggle' => 'tooltip' }
  end

  def user_icon
    content_tag :span, nil, class: 'fas fa-user'
  end

  def group_icon(group = nil)
    case group
    when Ddr::Auth::Groups::PUBLIC
      public_icon
    else
      content_tag :span, nil, class: 'fas fa-users'
    end
  end

  def public_icon
    content_tag :span, nil, class: 'fas fa-globe-americas'
  end

  def render_object_identifier
    if current_object.identifier.respond_to?(:join)
      current_object.identifier.join('<br />')
    else
      current_object.identifier
    end
  end

  def object_info_item(label:, value: nil, status: nil)
    render partial: 'object_info_item', locals: { value:, label:, status: }
  end

  def object_workflow_state
    case current_object.workflow_state
    when 'published'
      object_info_item label: 'Published', status: 'success'
    when nil, 'unpublished'
      object_info_item label: 'Not Published', status: 'warning'
    when 'nonpublishable'
      object_info_item label: 'Non-Publishable', status: 'danger'
    when 'previewable'
      object_info_item label: 'Previewable', status: 'info'
    end
  end

  def render_event_outcome(outcome)
    label = outcome == Ddr::Events::Event::SUCCESS ? 'success' : 'danger'
    render_label outcome.capitalize, label
  end

  def render_content_type_and_size(doc_or_obj)
    "#{doc_or_obj.content_type} #{doc_or_obj.content_human_size}"
  end

  def render_download_link(args = {})
    return unless args[:document]

    label = args.fetch(:label, 'Download')
    link_to [label, icon('fas', 'download')].join(' ').html_safe, ddr_download_path(args[:document]),
            class: args[:css_class], id: args[:css_id]
  end

  def render_intermediate_link(document, args = {})
    label = args.fetch(:label, 'Intermediate File')
    link_to [label, icon('fas', 'download')].join(' ').html_safe,
            ddr_download_path(document) + '?ddr_file_type=intermediate_file',
            class: args[:css_class], target: args[:target]
  end

  def render_stream_link(document, args = {})
    label = args.fetch(:label, 'Stream')
    link_to [label, icon('fas', 'external-link')].join(' ').html_safe,
            url_for(controller: document.tableized_name, id: document.id, action: 'stream'),
            class: args[:css_class], target: args[:target]
  end

  def render_caption_link(document, args = {})
    label = args.fetch(:label, 'Captions')
    link_to [label, icon('fas', 'download')].join(' ').html_safe,
            url_for(controller: document.tableized_name, id: document.id, action: 'captions'),
            class: args[:css_class], target: args[:target]
  end

  def thumbnail_image_tag(document, _image_options = {})
    if document.has_thumbnail?
      image_tag(thumbnail_path(document), alt: 'Thumbnail', class: 'img-thumbnail', size: '100x100')
    else
      content_tag :i, nil, class: "fas fa-#{default_thumbnail(document)} i-thumbnail", title: 'Thumbnail'
    end
  end

  def render_thumbnail(doc_or_obj, linked = false)
    thumbnail = if doc_or_obj.has_thumbnail?
                  image_tag(thumbnail_path(doc_or_obj), alt: 'Thumbnail', size: '100', class: 'img-thumbnail')
                else
                  content_tag :i, nil, class: "fas fa-#{default_thumbnail(doc_or_obj)} i-thumbnail"
                end
    if linked && can?(:read, doc_or_obj)
      link_to thumbnail, document_or_object_url(doc_or_obj)
    else
      thumbnail
    end
  end

  def thumbnail_path(doc_or_obj)
    return unless doc_or_obj.has_thumbnail?

    ddr_download_path(id: doc_or_obj.id, ddr_file_type: :thumbnail)
  end

  def render_document_summary(document)
    render partial: 'document_summary', locals: { document: }
  end

  def format_date(date)
    return unless date

    date = Time.parse(date.to_s) unless date.respond_to?(:localtime)
    date.localtime.to_s
  end

  def event_outcome_label(event)
    return unless event.outcome

    label = event.success? ? 'success' : 'danger'
    content_tag :span, event.outcome.capitalize, class: "event-outcome label label-#{label}"
  end

  def render_document_model_and_id(document)
    "#{document.common_model_name} #{document.id}"
  end

  # e.g., "Collection <uuid>"
  def render_resource_model_and_id(resource)
    "#{resource.common_model_name} #{resource.id.id}"
  end

  def link_to_document_or_object(doc_or_obj, access = :read)
    link_to_if can?(access, doc_or_obj), doc_or_obj.title_display, document_or_object_url(doc_or_obj)
  end

  def link_to_object(resource_id, access = :read)
    doc = SolrDocument.find(resource_id.id)
    link_to_if can?(access, doc), resource_id.id, document_or_object_url(doc)
  end

  def link_to_create_model(model)
    model_name = underscored_model_name(model)
    common_model_name = model.constantize.common_model_name
    underscored_common_model_name = common_model_name.tableize.singularize
    link_to t("ddr.#{underscored_common_model_name}.new_menu", default: common_model_name),
            send(:"new_#{model_name}_path"), class: 'dropdown-item'
  end

  def document_or_object_url(doc_or_obj)
    url_for controller: doc_or_obj.tableized_name, action: 'show', id: doc_or_obj
  end

  def create_menu_models
    session[:create_menu_models] ||= Ddr::Admin.create_menu_models.select { |model| can? :create, model.constantize }
  end

  def cancel_button(args = {})
    return_to = args.delete(:return_to) || :back
    opts = { class: ['btn', 'btn-danger', args.delete(:class)].compact.join(' ') }
    opts.merge! args
    link_to 'Cancel', return_to, opts
  end

  def all_user_options
    @all_user_options ||= user_options(User.order(:last_name, :first_name))
  end

  def group_options(groups)
    groups.map { |group| group_option(group) }
  end

  def group_option(group)
    [group.label, group.to_s]
  end

  def all_group_options
    # TODO: List public first, then registered, then rest in alpha order (?)
    @all_group_options ||= group_options(Ddr::Auth::Groups.all)
  end

  def user_options(users)
    users.map { |user| user_option(user) }
  end

  def user_option(user)
    option_text = user.display_name ? "#{user.display_name} (#{user})" : user.to_s
    option_value = user.to_s
    [option_text, option_value]
  end

  def desc_metadata_form_fields
    if change_set.new_record?
      change_set.desc_metadata_field_names(:required)
    else
      change_set.desc_metadata_field_names(:present, :required)
    end
  end

  def desc_metadata_form_field_id(field, counter = 1)
    "descMetadata__#{field}__#{counter}"
  end

  def desc_metadata_form_field_label(field, counter = 1)
    label_tag field, nil, for: desc_metadata_form_field_id(field, counter)
  end

  def desc_metadata_form_field_tag(field, value = nil, counter = 1)
    name = "descMetadata[#{field}][]"
    opts = {
      class: 'form-control field-value-input',
      id: desc_metadata_form_field_id(field, counter)
    }
    case field
    when 'abstract', 'description'
      text_area_tag(name, value, opts)
    when 'rights'
      select_tag(name,
                 rights_options_for_select(value),
                 opts.merge(include_blank: '(Select rights statement)'))
    else
      text_field_tag(name, value, opts)
    end
  end

  # Values may be an array or a single value.
  def desc_metadata_form_field_values(field)
    values = change_set.values field
    if values.is_a?(Array)
      values.empty? ? values << '' : values
    else
      [values]
    end
  end

  def desc_metadata_field_lists
    render partial: 'ddr/desc_metadata_form/field_list',
           locals: { label: 'Terms', terms: current_object.desc_metadata_terms }
  end

  def render_label(text, label)
    content_tag :span, text, class: "label label-#{label}"
  end

  def default_thumbnail(doc_or_obj)
    if doc_or_obj.has_content?
      default_mime_type_thumbnail(doc_or_obj.content_type)
    else
      default_model_thumbnail(doc_or_obj.resource_model)
    end
  end

  def default_mime_type_thumbnail(mime_type)
    case mime_type
    when /^image/
      'file-image'
    when /^video/
      'file-video'
    when /^audio/
      'file-audio'
    when %r{^application/(x-)?pdf}
      'file-pdf'
    when /^application/
      'file-code'
    else
      'file'
    end
  end

  def default_model_thumbnail(model)
    case model
    when 'Ddr::Collection'
      'copy'
    else
      'file'
    end
  end

  # Overrides corresponding method in Blacklight::FacetsHelperBehavior
  def render_facet_limit_list(paginator, solr_field, wrapping_element = :li)
    items = case solr_field
            when Ddr::Index::Fields::ADMIN_SET_FACET
              # apply custom sort for 'admin set' facet
              admin_set_facet_sort(paginator.items)
            else
              paginator.items
            end
    safe_join(items
        .map { |item| render_facet_item(solr_field, item) }.compact
        .map { |item| content_tag(wrapping_element, item) })
  end

  # Facet field view helper
  # Also used in custom sort for admin set facet
  def admin_set_title(code)
    admin_set_titles[code]
  end

  # Custom sort for 'admin set' facet
  # Sort by full name of admin set normalized to lower case for case-independent sorting
  # The 'value' attribute of each 'item' in the facet is the admin set code
  def admin_set_facet_sort(items = [])
    items.sort { |a, b| admin_set_title(a.value).downcase <=> admin_set_title(b.value).downcase }
  end

  def admin_set_titles
    @admin_set_titles ||= Ddr::AdminSet.all.each_with_object({}) { |a, memo| memo[a.code] = a.title }
  end

  def exportable_admin_metadata_fields
    Ddr::HasAdminMetadata.term_names - %i[access_role]
  end

  # This is needed because there are cases where calling `Array(values)` on single-valued property values does not
  # produce the same results as `[ values ]`.  One case in point is where `values` is a `Time` object; e.g.:
  # ```
  # > Array(Time.now) ==> [32, 45, 18, 5, 6, 2019, 3, 156, false, "UTC"]
  # > [ Time.now ] ==> [2019-06-05 18:46:17 +0000] # the desired result
  # ````
  def values_as_array(values)
    values.is_a?(Array) ? values : [values].compact
  end

  # Determines whether to include a link to generate structural metadata on the Structural Metadata tab of the
  # show view of relevant resources.
  def show_generate_structure_link?
    current_object.can_have_struct_metadata? &&
      (current_object.structure.nil? || current_object.structure.repository_maintained?) &&
      current_ability.can?(:generate_structure, current_object)
  end
end
