module MetadataFilesHelper
  def metadata_file_profiles
    profiles = {}
    files = Dir.glob(File.join(metadata_file_profiles_dir, '*.yml'))
    files.each do |f|
      profiles[metadata_file_profile_name(f)] = f
    end
    profiles
  end

  def metadata_file_profile_name(file)
    File.basename(file, '.yml').tr('_', ' ')
  end

  def metadata_file_profiles_dir
    Rails.root.join('config/metadata_file_profiles').to_s
  end

  def metadata_file_rows(metadata_file)
    csv_table = CSV.read(metadata_file.metadata.path, **metadata_file.csv_options)
    csv_table.length
  end

  def metadata_field_disposition(field, metadata_file)
    metadata_file.user_editable_fields.map(&:to_s).include?(field) ? 'update' : 'ignore'
  end
end
