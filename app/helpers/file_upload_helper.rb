module FileUploadHelper
  def file_upload_checksum_location(ddr_file_name)
    Ddr::FileUpload.default_checksum_location(ddr_file_name)
  end

  def permitted_file_upload_bases(ddr_file_name)
    Ddr::FileUpload.default_basepaths(ddr_file_name)
  end

  def file_upload_checksum_files(ddr_file_name)
    Dir.entries(file_upload_checksum_location(ddr_file_name)).select do |e|
      File.file?(File.join(file_upload_checksum_location(ddr_file_name), e))
    end
  end

  def file_upload_checksum_files_options_for_select(ddr_file_name)
    options_for_select(file_upload_checksum_files(ddr_file_name).collect { |f| [f, f] }, @file_upload.checksum_file)
  end
end
