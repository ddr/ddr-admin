module Ddr
  class FileUploadJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(args)
      file_upload = FileUpload.new(args)
      results = file_upload.process
      batch_id = results.batch.id if results.batch
      file_count = results.inspection_results.filesystem.file_count if results.inspection_results
      ActiveSupport::Notifications.instrument(FileUpload::FINISHED,
                                              user_key: args['batch_user'],
                                              basepath: args['basepath'],
                                              subpath: args['subpath'],
                                              collection_id: args['collection_id'],
                                              file_count:,
                                              errors: results.errors,
                                              batch_id:)
    end

    def on_failure_send_email(_e, args)
      folder_path = File.join(args['basepath'], args['subpath'])
      send_email(email: User.find_by_user_key(args['batch_user']).email,
                 subject: "FAILED - File Upload Job - #{folder_path}",
                 message: "File Upload processing for folder #{folder_path} FAILED.")
    end
  end
end
