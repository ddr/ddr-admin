module Ddr
  class IngestFolderJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(args)
      ingest_folder = IngestFolder.find(args['id'])
      batch_id = ingest_folder.procezz
      ActiveSupport::Notifications.instrument(IngestFolder::FINISHED,
                                              ingest_folder_id: ingest_folder.id,
                                              batch_id:)
    end

    def on_failure_send_email(_e, args)
      ingest_folder = IngestFolder.find(args['id'])
      send_email(email: ingest_folder.user.email,
                 subject: "FAILED - DPC Folder Ingest Job - #{ingest_folder.abbreviated_path}",
                 message: "DPC Folder Ingest processing for folder #{ingest_folder.abbreviated_path} FAILED.")
    end
  end
end
