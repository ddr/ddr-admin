module Ddr
  class FileMigrationJob < ::ApplicationJob
    def perform(resource_id:, source:, destination:)
      resource = Ddr.query_service.find_by(id: resource_id)
      service = FileMigrationService.new(source:, destination:)
      service.migrate!(resource:)
    end
  end
end
