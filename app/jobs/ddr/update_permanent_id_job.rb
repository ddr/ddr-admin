module Ddr
  class UpdatePermanentIdJob < ApplicationJob
    def perform(repo_id)
      PermanentId.update!(repo_id)
    end
  end
end
