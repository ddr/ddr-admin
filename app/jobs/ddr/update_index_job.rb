module Ddr
  class UpdateIndexJob < ApplicationJob
    def perform(resource_ids)
      IndexService.new(resource_ids).index
    end
  end
end
