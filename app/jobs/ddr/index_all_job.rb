module Ddr
  class IndexAllJob < ApplicationJob
    def perform(batch_size: 20)
      %w[Ddr::Component Ddr::Item Ddr::Attachment Ddr::Target Ddr::Collection].each do |model|
        #
        # Using orm_class directly to avoid extra record-to-resource conversion.
        # We just need the resource ids at this stage.
        #
        query = Ddr.query_service.orm_class.where(internal_resource: model)
        count = query.count

        logger.debug { "#{self.class}: #{count} #{model} resources found." }

        offset = 0
        while offset < count
          # SELECT id FROM orm_resources WHERE internal_resource = 'Ddr::Component' ORDER BY id ASC OFFSET 0 LIMIT 1000;
          result = query.order(:id).offset(offset).limit(1000).pluck(:id)
          result.each_slice(batch_size) do |ids|
            IndexBatchJob.perform_later(ids)
          end
          offset += result.size
        end

        logger.debug { "#{self.class}: #{offset} #{model} resources queued for indexing." }

        logger.error { "#{self.class}: Offset (#{offset}) != count (#{count})" } if offset != count
      end
    end
  end
end
