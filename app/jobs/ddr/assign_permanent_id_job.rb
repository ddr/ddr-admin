module Ddr
  class AssignPermanentIdJob < ApplicationJob
    def perform(resource_ids)
      AssignPermanentIdService.call(resource_ids)
    end
  end
end
