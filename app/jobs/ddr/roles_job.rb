module Ddr
  class RolesJob < ::ApplicationJob
    def perform(id, roles)
      _roles = JSON.parse(roles).map { |r| Ddr::Auth::Roles::Role.new(r) }
      resource = Ddr.query_service.find_by(id:)
      change_set = ResourceChangeSet.change_set_for(resource)
      change_set.roles = _roles
      ResourceChangeSetPersister.new.save(change_set:)
    end
  end
end
