module Ddr
  class ExportFilesJob < ApplicationJob
    def perform(identifiers, basename, user_id, files)
      user = User.find(user_id)
      ability = Ddr::Auth::AbilityFactory.call(user)
      export = ExportFiles::Package.call(identifiers:, ability:, basename:, files:)
      ExportFilesMailer.notify_success(export, user).deliver_now
    rescue Exception
      ExportFilesMailer.notify_failure(identifiers, basename, user).deliver_now if user
      raise
    end
  end
end
