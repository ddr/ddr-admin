module Ddr
  class StandardIngestJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(args)
      standard_ingest = StandardIngest.new(args)
      results = standard_ingest.process
      batch_id = results.batch.id if results.batch
      file_count = results.inspection_results.file_count if results.inspection_results
      model_stats = results.inspection_results.content_model_stats if results.inspection_results
      ActiveSupport::Notifications.instrument(StandardIngest::FINISHED,
                                              user_key: args['batch_user'],
                                              basepath: args['basepath'],
                                              subpath: args['subpath'],
                                              collection_id: args['collection_id'],
                                              file_count:,
                                              model_stats:,
                                              errors: results.errors,
                                              batch_id:)
    end

    def on_failure_send_email(_e, args)
      send_email(email: User.find_by_user_key(args['batch_user']).email,
                 subject: "FAILED - Standard Ingest Job - #{args['folder_path']}",
                 message: "Standard Ingest processing for folder #{args['folder_path']} FAILED.")
    end
  end
end
