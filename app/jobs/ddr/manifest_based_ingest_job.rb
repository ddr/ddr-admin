module Ddr
  class ManifestBasedIngestJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(args)
      ingest = ManifestBasedIngest.new(args)
      results = ingest.process
      batch_ids = results.batches.map { |b| b.id }.join(',') if results.batches
      file_count = results&.manifest&.file_count
      model_stats = results&.manifest&.content_model_stats
      ActiveSupport::Notifications.instrument(ManifestBasedIngest::FINISHED,
                                              user_key: args['batch_user'],
                                              collection_id: args['collection_id'],
                                              file_count:,
                                              model_stats:,
                                              errors: results.errors,
                                              batch_ids:)
    end

    def on_failure_send_email(_e, args)
      send_email(email: User.find_by_user_key(args['batch_user']).email,
                 subject: 'FAILED - Manifest Based Ingest Job',
                 message: 'Manifest Based Ingest processing FAILED.')
    end
  end
end
