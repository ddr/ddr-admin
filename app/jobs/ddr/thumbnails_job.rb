module Ddr
  class ThumbnailsJob < ApplicationJob
    def perform(id)
      collection = Ddr.query_service.find_by(id:)
      raise ArgumentError, I18n.t('ddr.thumbnails.model_error') unless collection.is_a?(Ddr::Collection)

      ThumbnailsService.call(collection)
    end
  end
end
