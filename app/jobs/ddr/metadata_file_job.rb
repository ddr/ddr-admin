module Ddr
  class MetadataFileJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(args)
      metadata_file = MetadataFile.find(args['id'])
      batch_id = metadata_file.procezz
      ActiveSupport::Notifications.instrument(MetadataFile::FINISHED,
                                              metadata_file_id: metadata_file.id,
                                              batch_id:)
    end

    def on_failure_send_email(_e, args)
      metadata_file = MetadataFile.find(args['id'])
      send_email(email: metadata_file.user.email,
                 subject: "FAILED - Metadata File Job - #{metadata_file.metadata_file_name}",
                 message: "Metadata File processing for file #{metadata_file.metadata_file_name} FAILED")
    end
  end
end
