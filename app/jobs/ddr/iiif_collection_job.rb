module Ddr
  class IiifCollectionJob < ApplicationJob
    def perform(id)
      Ddr::IiifPresentationService.update_collection!(id)
    end
  end
end
