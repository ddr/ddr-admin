module Ddr
  class IndexBatchJob < ApplicationJob
    def perform(resource_ids)
      resources = Ddr.query_service.find_many_by_ids(ids: resource_ids)
      index = Valkyrie::MetadataAdapter.find(:index_solr)
      index.persister.save_all(resources:)
    end
  end
end
