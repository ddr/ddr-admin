module Ddr
  class UploadFileJob < ApplicationJob
    def perform(resource_id:, file_path:, file_type:)
      ::File.open(file_path, 'rb') do |file|
        resource = Ddr.query_service.find_by(id: resource_id)
        change_set = Ddr::ResourceChangeSet.change_set_for(resource)
        change_set.add_file(file, file_type)
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
      end
    end
  end
end
