module Ddr::Batch
  class BatchReprocessorJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(batch_id, operator_id)
      ReprocessBatch.new(batch_id:, operator_id:).execute
    end
  end
end
