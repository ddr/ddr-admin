module Ddr::Batch
  class BatchObjectsProcessorJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(batch_object_ids, operator_id)
      operator = User.find(operator_id)
      ProcessBatchObjects.new(batch_object_ids:, operator:).execute
    end
  end
end
