module Ddr::Batch
  class BatchProcessorJob < ApplicationJob
    sidekiq_options retry: 0

    def perform(batch_id, operator_id)
      ProcessBatch.new(batch_id:, operator_id:).execute
    end
  end
end
