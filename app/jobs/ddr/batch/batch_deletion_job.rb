module Ddr::Batch
  class BatchDeletionJob < ApplicationJob
    def perform(batch_id)
      batch = Batch.find(batch_id)
      batch.status = Batch::STATUS_DELETING
      batch.save!
      batch.destroy!
    end

    def before_enqueue_set_status(batch_id)
      batch = Batch.find(batch_id)
      batch.status = Batch::STATUS_QUEUED_FOR_DELETION
      batch.save
    end
  end
end
