module Ddr
  class FixityCheckJob < ApplicationJob
    def perform(id)
      resource = Ddr.query_service.find_by(id:)

      result = FixityCheck.call(resource)

      unless result.success
        raise Ddr::ChecksumInvalid,
              I18n.t('ddr.fixity.results_msg', repo_id: result.resource_id.id, file_results: result.results)
      end

      result
    end
  end
end
