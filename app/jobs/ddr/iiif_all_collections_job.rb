module Ddr
  class IiifAllCollectionsJob < ApplicationJob
    def perform
      query = Ddr.query_service.orm_class.where(internal_resource: 'Ddr::Collection')
      count = query.count

      logger.debug { "#{self.class}: #{count} Ddr::Collection resources found." }

      result = query.order(:id).pluck(:id)
      result.each do |id|
        Ddr::IiifCollectionJob.perform_later(id)
      end
    end
  end
end
