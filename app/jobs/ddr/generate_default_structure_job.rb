module Ddr
  class GenerateDefaultStructureJob < ApplicationJob
    unique :until_executing

    class DefaultStructureGenerationError < Ddr::Error; end

    def perform(object_id)
      object = Ddr.query_service.find_by(id: object_id)
      structure = DefaultStructureService.default_structure(object)

      Tempfile.open("#{object_id}_struct_metadata") do |file|
        file.write(structure.to_xml)
        file.rewind

        # Reload object to avoid stale object error
        object = Ddr.query_service.find_by(id: object_id)

        change_set = ResourceChangeSet.change_set_for(object)
        change_set.add_file(file, :struct_metadata, mime_type: 'application/xml')
        # the addition of structural metadata to a resource is not itself structurally relevant
        change_set.skip_structure_updates = true
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
      end
    rescue DefaultStructureGenerationError => e
      logger.error e.message
    end
  end
end
