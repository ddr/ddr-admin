module Ddr
  module ArchivesSpace
    class CreateDigitalObjectsJob < ActiveJob::Base
      sidekiq_options retry: 0

      def perform(collection_id, options)
        csv = ExportDigitalObjectInfo.call(collection_id)
        output = CreateDigitalObjects.call(csv, **options.slice(:user, :publish, :debug))
        subject = "REPORT: Create Digital Objects for #{collection_id}"
        message = 'The report is attached.'
        ::ReportMailer.basic(
          subject:,
          content: output,
          filename: options[:filename],
          to: options[:notify],
          message:
        ).deliver_now
      end
    end
  end
end
