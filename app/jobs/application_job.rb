class ApplicationJob < ActiveJob::Base
  # Subclasses have their queues initially set as the return value of the `priority_queue' method.
  # @see .priority_queue
  def self.inherited(klass)
    klass.queue_as(klass.priority_queue)
  end

  def self.send_email(email:, subject:, message:)
    JobMailer.basic(to: email, subject:, message:).deliver
  end

  # @return [Boolean] whether the job is configured to be automatically retried
  def self.retriable?
    get_sidekiq_options['retry']
  end

  # @return [Boolean] whether the job is high priority
  # @see Ddr::Admin.high_priority_jobs
  def self.high_priority?
    Ddr::Admin.high_priority_jobs.include?(name.demodulize)
  rescue StandardError
    false
  end

  # @return [Boolean] whether the job is low priority
  # @see Ddr::Admin.low_priority_jobs
  def self.low_priority?
    Ddr::Admin.low_priority_jobs.include?(name.demodulize)
  rescue StandardError
    false
  end

  # The name of the queue for the job as determined by ddr-admin job priority settings.
  # @return [Symbol] name of queue
  def self.priority_queue
    if high_priority?
      :high_priority
    elsif low_priority?
      :low_priority
    else
      :default
    end
  end
end
