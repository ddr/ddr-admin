
$(document).ready(function () {

    $("#new_ddr_metadata_file").submit(function (e) {

        // disable submit button and update text
        $("#metadata_file_submit_button").attr("disabled", true);
        $("#metadata_file_submit_button").val("Uploading and parsing metadata file ...");

    });

    $("#create-update-batch-btn").click(function (e) {

        // disable button and update text
        $("#create-update-batch-btn").attr("disabled", true);
        $("#create-update-batch-btn").html("Queueing Metadata File Job ...");

    });

})
