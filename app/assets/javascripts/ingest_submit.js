
$(document).ready(function () {

  $("#new_ddr_ingest_folder").submit(function (e) {

    // disable submit button and update text
    $("#ingest_folders_submit_button").attr("disabled", true);
    $("#ingest_folders_submit_button").val("Scanning ...");

  });

  $("#ingest_folder_create_batch_btn").click(function (e) {

    // disable button and update text
    $("#ingest_folder_create_batch_btn").attr("disabled", true);
    $("#ingest_folder_create_batch_btn").html("Queueing DPC Folder Ingest Job ...");

  });

})
