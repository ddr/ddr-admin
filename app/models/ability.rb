class Ability < Ddr::Auth::Ability
  self.ability_definitions += [Ddr::Admin::AliasAbilityDefinitions,
                               Ddr::Admin::AdminSetAbilityDefinitions,
                               Ddr::Admin::CollectionAbilityDefinitions,
                               Ddr::Admin::EventAbilityDefinitions,
                               Ddr::Admin::BatchAbilityDefinitions,
                               Ddr::Admin::ManifestBasedIngestAbilityDefinitions,
                               Ddr::Admin::MetadataFileAbilityDefinitions,
                               Ddr::Admin::IngestFolderAbilityDefinitions,
                               Ddr::Admin::NestedFolderIngestAbilityDefinitions,
                               Ddr::Admin::StandardIngestAbilityDefinitions]

  delegate :curator?, to: :auth_context
end
