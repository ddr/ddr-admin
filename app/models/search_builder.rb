# frozen_string_literal: true

class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior

  def current_ability
    scope.context[:current_ability]
  end

  self.default_processor_chain += [:apply_gated_discovery]

  def self.effective_role_filter(ability)
    # https://lucene.apache.org/solr/guide/7_7/other-parsers.html#terms-query-parser
    "{!terms f=#{Ddr::Index::Fields::EFFECTIVE_ROLE} method=booleanQuery}" +
      ability.agents.join(',')
  end

  def self.gated_discovery_filters(ability)
    ability.auth_context.superuser? ? '' : effective_role_filter(ability)
  end

  def apply_gated_discovery(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << gated_discovery_filters
  end

  protected

  def effective_role_filter
    self.class.effective_role_filter(current_ability)
  end

  def gated_discovery_filters
    self.class.gated_discovery_filters(current_ability)
  end
end
