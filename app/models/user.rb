class User < ApplicationRecord
  # Connects this user object to Blacklight's Bookmarks.
  # Since we don't use bookmarks, these methods should be considered
  # deprecated/unsupported.
  include Blacklight::User

  has_many :batches, inverse_of: :user, class_name: 'Ddr::Batch::Batch'
  has_many :ingest_folders, inverse_of: :user, class_name: 'Ddr::IngestFolder'
  has_many :metadata_files, inverse_of: :user, class_name: 'Ddr::MetadataFile'

  delegate :can, :can?, :cannot, :cannot?, to: :ability

  validates :username, uniqueness: { case_sensitive: false }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/ }

  devise :database_authenticatable, :omniauthable, omniauth_providers: [:shibboleth]

  devise :registerable unless Rails.env.production?

  def self.find_by_user_key(value)
    find_by(username: value)
  end

  def self.from_omniauth(auth)
    find_or_initialize_by(username: auth.uid).tap do |user|
      user.password = Devise.friendly_token if user.new_record?

      user.assign_attributes(
        email: auth.info.email,
        display_name: auth.info.name,
        first_name: auth.info.first_name,
        last_name: auth.info.last_name,
        nickname: auth.info.nickname,
        duid: auth.extra.raw_info['duDukeID']
      )

      user.save! if user.changed?
    end
  end

  def to_s
    username
  end

  def user_key
    username
  end

  def agent
    username
  end

  def aspace_username
    email.delete_suffix('@duke.edu')
  end

  def netid
    @netid ||= username.delete_suffix('@duke.edu') if username.end_with?('@duke.edu')
  end

  def ability
    @ability ||= Ddr::Auth::AbilityFactory.call(self)
  end
end
