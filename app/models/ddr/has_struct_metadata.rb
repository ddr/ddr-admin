module Ddr
  module HasStructMetadata
    extend ActiveSupport::Concern

    included do
      attribute :struct_metadata, Ddr::File.optional
    end

    # @return [Ddr::Structure] the structure, or nil
    # @raise [Valkyrie::StorageAdapter::FileNotFound]
    def structure
      return unless has_struct_metadata?

      # Using stream for S3 compat
      xml_doc = Nokogiri::XML(struct_metadata.stream)

      Ddr::Structure.new(xml_doc)
    end
  end
end
