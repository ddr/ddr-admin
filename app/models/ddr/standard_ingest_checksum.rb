module Ddr
  class StandardIngestChecksum < IngestChecksum
    DATA_PREFIX = 'data'

    def checksum(relative_filepath)
      checksums[::File.join(DATA_PREFIX, relative_filepath)]
    end
  end
end
