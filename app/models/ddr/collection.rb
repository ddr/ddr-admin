module Ddr
  class Collection < Resource
    include V3::Publishable
    include HasAttachments
    include HasChildren
    include HasStructMetadata
    include HasIiif

    alias items children

    def components
      Ddr.query_service.find_inverse_references_by(
        resource: self,
        model: 'Ddr::Component',
        property: :admin_policy_id
      )
    end

    # Collection resources are publishable unless they have been marked nonpublishable
    def publishable?
      !nonpublishable? && !new_record?
    end

    def targets
      Ddr.query_service.find_inverse_references_by(resource: self, property: 'for_collection_id')
    end
  end
end
