module Ddr
  class DeletedFile < ApplicationRecord
    before_destroy :check_file, if: :path?
    after_destroy :remove_file, if: :path?

    # See policy document: https://duldev.atlassian.net/wiki/x/AYAP5g
    IMMEDIATELY_REMOVABLE = %w[derived_image fits_file iiif_file multires_image struct_metadata thumbnail].freeze
    REMOVABLE_WITH_RETENTION_PERIOD = %w[caption content extracted_text intermediate_file streamable_media].freeze
    RETENTION_POLICY = 6.months

    def self.removable
      where(file_id: IMMEDIATELY_REMOVABLE)
        .or(
          where(file_id: REMOVABLE_WITH_RETENTION_PERIOD).where('created_at < ?', RETENTION_POLICY.ago)
        )
    end

    def check_file
      return unless ::File.exist?(path)

      # Don't destroy the record if the file exists but cannot be deleted.
      throw :abort unless ::File.writable?(::File.dirname(path))
    end

    def remove_file
      FileUtils.rm_f(path) if ::File.exist?(path)
    end
  end
end
