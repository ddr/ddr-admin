require 'digest'

module Ddr
  class Digest < Valkyrie::Resource
    attribute? :type, Types::DigestType.default('SHA1'.freeze)
    attribute  :value, Types::Coercible::String

    def sha1?
      type == 'SHA1'
    end

    def ==(other)
      return false unless other.is_a?(self.class)

      type == other.type && value == other.value
    end
  end
end
