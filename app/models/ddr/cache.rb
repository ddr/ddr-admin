module Ddr
  class Cache < Hash
    def get(key)
      self[key]
    end

    def put(key, value)
      self[key] = value
    end

    def with(options)
      merge!(options)
      block_result = yield
      reject! { |k, _v| options.include?(k) }
      block_result
    end
  end
end
