module Ddr
  module Ingestible
    def permitted_paths
      @permitted_paths ||= Ddr::Admin.ingest_base_paths.map { |path| "#{path}/**" }.freeze
    end

    def permitted_path?(path)
      permitted_paths.any? { |permitted| ::File.fnmatch?(permitted, path) }
    end
  end
end
