module Ddr
  module Batch
    class Error < ::StandardError; end

    # Error processing batch object
    class BatchObjectProcessingError < Error; end

    # Logging level for batch processing - defaults to Logger::INFO
    mattr_accessor :processor_logging_level do
      Logger::INFO
    end

    def self.table_name_prefix; end
  end
end
