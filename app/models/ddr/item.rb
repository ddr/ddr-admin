module Ddr
  class Item < Resource
    include V3::Publishable
    include Embargoable
    include HasChildren
    include HasParent
    include HasStructMetadata
    include HasIiif

    alias collection_id parent_id
    alias collection parent

    alias components children

    self.parent_class = Ddr::Collection

    def children_having_extracted_text
      children.select { |child| child.attached_files_having_content.include?(:extracted_text) }
    end

    def all_text
      children_having_extracted_text.map { |child| child.extracted_text.content }.to_a.flatten
    end
  end
end
