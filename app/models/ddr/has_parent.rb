module Ddr
  module HasParent
    extend ActiveSupport::Concern

    included do
      class_attribute :parent_class
      attribute :parent_id, Valkyrie::Types::ID.optional
    end

    def has_parent?
      parent_id.present?
    end

    def parent
      Ddr.query_service.find_by(id: parent_id) if has_parent?
    end

    # Resources the can have parents (currently, Items and Components) are publishable only if they:
    # - are persisted
    # - have not been marked nonpublishable
    # - have a parent
    # - and the parent is published
    #
    # @deprecated Use `can_publish?'.
    def publishable?
      # persisted? && !nonpublishable? && parental_publication_guard
      can_publish?
    end

    private

    def parental_publication_guard
      parent.present? && parent.published?
    end
  end
end
