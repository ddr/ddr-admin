module Ddr::Events
  class ValidationEvent < Event
    self.description = 'Object validated'
    self.preservation_event_type = EventTypes::VALIDATION
    self.system_agent_role = AgentRoles::VALIDATOR
    self.retain_for_deleted_resources = false
  end
end
