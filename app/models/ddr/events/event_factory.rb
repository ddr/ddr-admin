module Ddr
  module Events
    class EventFactory
      # @param event_type [String] the event type
      # @param attributes [Hash] event instance attributes
      # @raise [ArgumentError] invalid event type
      # @return [Ddr::Events::Event] event instance
      # @see Ddr::Events::EventTypes
      def self.call(event_type, **attributes)
        class_for_event_type(event_type).new(**attributes)
      end

      # @param event_type [String] the event type
      # @raise [ArgumentError] invalid event type
      # @raise [RuntimeError] class not found for event type
      # @return [Class] Ddr::Events::Event subclass
      # @see Ddr::Events::EventTypes
      def self.class_for_event_type(event_type)
        return Event if event_type.nil?

        raise ArgumentError, 'Event type is not valid' unless Ddr::Events.event_types.include?(event_type)

        if klass = Event.subclasses.detect { |sub| sub.preservation_event_type == event_type }
          return klass
        end

        raise "Event class not found for type: #{event_type}"
      end
    end
  end
end
