module Ddr::Events
  class MigrationEvent < Event
    self.description = 'Object migrated'
    self.preservation_event_type = EventTypes::MIGRATION
    self.retain_for_deleted_resources = false
  end
end
