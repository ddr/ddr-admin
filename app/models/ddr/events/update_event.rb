module Ddr::Events
  class UpdateEvent < Event
    self.description = 'Object updated'
    self.retain_for_deleted_resources = false

    # This event is used for both metadata and file updates,
    # but "modification" seems to be the closest preservation event
    # type in the LoC vocabulary.
    # https://id.loc.gov/vocabulary/preservation/eventType/mod.html
    self.preservation_event_type = EventTypes::MODIFICATION

    # @deprecated Legacy handler for ActiveSupport::Notifications::Event.
    def self.call(*args)
      super do |event, notification|
        attrs_changed = notification.payload[:attributes_changed]
        files_changed = notification.payload[:files_changed]
        detail = [
          "Files changed: #{files_changed.join(', ')}",
          event.detail
        ]
        detail << 'Attributes changed' if attrs_changed.present?
        event.detail = detail.compact.join("\n\n")
      end
    end
  end
end
