module Ddr::Events
  class DeletionEvent < Event
    self.description = 'Object deleted'
    self.preservation_event_type = EventTypes::DELETION
    self.retain_for_deleted_resources = true
  end
end
