module Ddr::Events
  class FixityCheckEvent < Event
    include ReindexObjectAfterSave

    self.preservation_event_type = EventTypes::FIXITY_CHECK
    self.description = 'Validation of attached file checksums'
    self.system_agent_role = AgentRoles::VALIDATOR
    self.retain_for_deleted_resources = false

    DETAIL_PREAMBLE = 'Attached File checksum validation results:'
    DETAIL_TEMPLATE = '%<file_id>s ... %<validation>s'

    # @deprecated Legacy handler for ActiveSupport::Notifications::Event.
    def self.call(*)
      notification = ActiveSupport::Notifications::Event.new(*)
      result = notification.payload[:result] # FixityCheck::Result instance
      detail = [DETAIL_PREAMBLE]
      result.results.each do |file_id, success|
        validation = success ? VALID : INVALID
        detail << (format(DETAIL_TEMPLATE, file_id:, validation:))
      end
      create(resource_id: result.resource_id,
             event_date_time: notification.time,
             outcome: result.success ? SUCCESS : FAILURE,
             detail: detail.join("\n"))
    end

    def to_solr
      { Ddr::Index::Fields::LAST_FIXITY_CHECK_ON => event_date_time_s,
        Ddr::Index::Fields::LAST_FIXITY_CHECK_OUTCOME => outcome }
    end
  end
end
