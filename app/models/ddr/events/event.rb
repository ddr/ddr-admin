# frozen_string_literal: true

module Ddr::Events
  #
  # An event represents a discrete action taken in the repository
  # related to a specific resource.
  #
  # We have attempted to model this data using concepts from PREMIS
  # and LoC vocabularies, although some deviations from these
  # standards may be present.
  #
  # @see Ddr::Events.
  # @abstract Use subclasses defined in this module.
  #
  class Event < ApplicationRecord
    include Outcomes

    OUTCOMES = [SUCCESS, FAILURE, WARNING].freeze

    # ActiveSupport::Notifications::Instrumenter sets payload[:exception]
    #   to an array of [<exception class name>, <exception message>]
    #   and we want to store this data in a string field.
    serialize :exception, coder: JSON

    # Event date time - for PREMIS and Solr
    DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%LZ'

    # set default ordering
    DEFAULT_SORT_ORDER = 'event_date_time ASC'
    default_scope { order(DEFAULT_SORT_ORDER) }

    # Resource scope
    scope :for_resource, ->(resource) { where(resource_id: Ddr::Types::ResourceID.optional[resource]) }

    # Outcome scopes
    OUTCOMES.each do |value|
      scope value.to_sym, ->{ where(outcome: value) }

      # @return [Boolean]
      define_method "#{value}?" do
        outcome == value
      end

      # @return [String]
      define_method "#{value}!" do
        self.outcome = value
      end
    end

    # Deleted resources scope
    scope :for_deleted_resources, ->{ joins('left join orm_resources on events.resource_id = orm_resources.id::text')
                                        .where('orm_resources.id is null') }

    # Existing resources scope
    scope :for_existing_resources, ->{ joins('inner join orm_resources on events.resource_id = orm_resources.id::text') }

    # Validation constants
    # @deprecated Use Ddr::Events::Event::SUCCESS instead
    VALID = 'VALID'
    # @deprecated Use Ddr::Events::Event::FAILURE instead
    INVALID = 'INVALID'

    # For rendering "performed by" when there no associated user agent (i.e., user_key is nil)
    SYSTEM = 'SYSTEM'

    # The default value for the `software' field
    DDR_SOFTWARE = "ddr-admin #{Ddr::Admin::VERSION}".freeze

    # @!attribute description [rw] Generic text description of the event type
    #   @return [String]
    class_attribute :description

    # @!attribute preservation_event_type [r]
    #   @return [String]
    #   @see https://id.loc.gov/vocabulary/preservation/eventType.html
    class_attribute :preservation_event_type, instance_writer: false

    # @!attribute retain_for_deleted_resources [r] Whether events of this type should be retained for deleted resources
    #   @return [Boolean]
    class_attribute :retain_for_deleted_resources, instance_writer: false

    # The preservation event type
    # @note It's somewhat regrettable that 'event_type' is overloaded, since
    # elsewhere it may be used to refer to the type (class) of the event record.
    # Going forward, it's probably best to use 'preservation_event_type' to refer
    # to the type of event as defined by the LoC vocabulary.
    # @return [String]
    # @see https://id.loc.gov/vocabulary/preservation/eventType.html
    def self.event_type
      preservation_event_type
    end

    alias event_type preservation_event_type

    ##
    ## PREMIS-related attributes
    ##

    # @!attribute object_role [rw] the role of the object (i.e., resource) associated with an event
    #   @return [String]
    #   @see https://id.loc.gov/vocabulary/preservation/eventRelatedObjectRole.html
    class_attribute :object_role
    self.object_role = ObjectRoles::SOURCE

    # @!attribute user_agent_role [rw] the role of the user agent in relation to the event
    #   @return [String]
    #   @see https://id.loc.gov/vocabulary/preservation/eventRelatedAgentRole.html
    class_attribute :user_agent_role
    # https://id.loc.gov/vocabulary/preservation/eventRelatedAgentRole/imp.html
    self.user_agent_role = AgentRoles::IMPLEMENTER

    # @!attribute system_agent_role [rw] the role of the system agent (i.e., the repository itself as a software system)
    #   in relation to the event
    #   @return [String]
    #   @see https://id.loc.gov/vocabulary/preservation/eventRelatedAgentRole.html
    class_attribute :system_agent_role
    # https://id.loc.gov/vocabulary/preservation/eventRelatedAgentRole/exe.html
    self.system_agent_role = AgentRoles::EXECUTING_PROGRAM

    # Validation
    validates :event_date_time, :resource_id, presence: true
    validates :outcome, inclusion: { in: OUTCOMES, message: '"%<value>s" is not a valid event outcome' }

    # Callbacks
    after_initialize if: :new_record? do
      self.event_date_time ||= Time.now.utc
      self.summary         ||= description
      self.software        ||= DDR_SOFTWARE
      self.outcome         ||= exception? ? FAILURE : SUCCESS
      self.permanent_id    ||= resource&.permanent_id
    end

    before_create :failure!, if: :exception?

    # Event attributes as symbols
    # @return [Array<Symbol>]
    def self.attribute_symbols
      attribute_names.map(&:to_sym)
    end

    # @deprecated Receive message sent by legacy ActiveSupport::Notifications.
    # @return [Ddr::Events::Event] a newly created event
    def self.call(*)
      notification = ActiveSupport::Notifications::Event.new(*)
      payload = notification.payload.slice(*attribute_symbols)
      payload[:event_date_time] ||= notification.time
      create(payload) do |event|
        yield [event, notification] if block_given?
      end
    end

    def self.method_missing(name, *)
      return for_resource(*) if %i[for_object for_resource_id].include?(name)

      super
    end

    # A shortened version of the event class name (i.e., value of the type field in the events table)
    # @todo Use preservation_event_type instead.
    # @example
    #   Ddr::Events::UpdateEvent.new.display_type => "Update"
    # @return [String]
    def display_type
      @display_type ||= type.demodulize.sub('Event', '').titleize
    end

    # The agent who performed the operation recording in the event
    # I.e., the user agent or 'SYSTEM'.
    # @return [String]
    def performed_by
      user_key || SYSTEM
    end

    def comment_or_summary
      comment.presence || summary
    end

    # The resource related to this event (nil if not found)
    # @return [Ddr::Resource, nil]
    def resource
      Ddr.query_service.find_by(id: resource_id) if resource_id
    rescue Valkyrie::Persistence::ObjectNotFoundError => _e
      # N.B. Resource may have been legitimately deleted after initial event persistence.
      nil
    end

    alias object resource

    # Sets the resource related to this event.
    # @param resource [Ddr::Types::ResourceID] the resource coerced to an ID string
    # @return [String]
    def resource=(resource)
      self.resource_id = Ddr::Types::ResourceID[resource]
    rescue Dry::Types::ConstraintError => e
      raise ArgumentError, e.message
    end

    alias object= resource=

    # Return a date/time formatted as a string suitable for use as a PREMIS eventDateTime.
    # Format also works for Solr.
    # Force to UTC.
    # @return [String]
    def event_date_time_s
      event_date_time.utc.strftime DATE_TIME_FORMAT
    end

    # Sets the user_key to the `user_key` of the user instance.
    # @param user [User]
    # @return [String]
    def user=(user)
      self.user_key = user.user_key
    end

    # The resource ID rendered as a URN, for PREMIS XML output
    # @todo Refactoring other places that do this.
    # @return [String]
    def resource_urn
      "urn:uuid:#{resource_id}"
    end

    def exception_type
      exception&.first
    end

    def exception_message
      exception&.last
    end

    # The ARK for the resource, taken either from the event record, if available,
    # or from the resource's `permanent_id` attribute (if the resource exists).
    # @return [String]
    def resource_ark
      permanent_id || resource&.permanent_id
    end

    # Render the event as PREMIS XML.
    # @see Ddr::API::PremisXMLEvent
    # @return [String]
    def to_xml
      Ddr::API::PremisXMLEvent.call(self).to_xml
    end
  end
end

# Eager load subclasses
Dir.glob("#{__dir__}/*_event.rb").each { |mod| require(mod) }
