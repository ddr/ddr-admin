module Ddr::Events
  class IngestionEvent < Event
    self.preservation_event_type = EventTypes::INGESTION
    self.description = 'Object ingested into the repository'
    self.object_role = ObjectRoles::OUTCOME
    self.retain_for_deleted_resources = true
  end
end
