module Ddr::Events
  class VirusCheckEvent < Event
    include ReindexObjectAfterSave

    self.preservation_event_type = EventTypes::VIRUS_CHECK
    self.description = 'Content file scanned for viruses'
    self.retain_for_deleted_resources = false

    def to_solr
      { Ddr::Index::Fields::LAST_VIRUS_CHECK_ON => event_date_time_s,
        Ddr::Index::Fields::LAST_VIRUS_CHECK_OUTCOME => outcome }
    end
  end
end
