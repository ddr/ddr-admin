module Ddr
  class Attachment < Resource
    include V3::Unpublishable
    include HasContent
    include HasExtractedText

    attribute :attached_to_id, Valkyrie::Types::ID.optional

    # @return [Ddr::Collection] the collection to which this attachment is attached
    # @raise [Valkyrie::Persistence::ObjectNotFoundError] if the collection cannot be found
    def attached_to
      Ddr.query_service.find_by(id: attached_to_id) if attached_to_id
    end
  end
end
