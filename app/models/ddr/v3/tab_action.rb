module Ddr::V3
  #
  # An action that can be taken on a resource and is associated with a UI tab.
  #
  class TabAction
    delegate :controller_path, :resource, :ability, to: :tab

    delegate :has_fits_file?,
             :has_iiif_file?,
             :has_struct_metadata?,
             to: :resource

    PublicationService::WORKFLOW_STATES.each do |wf_state|
      delegate :"#{wf_state}?", to: :resource
    end

    PublicationService::WORKFLOW_EVENTS.each do |event|
      delegate :"can_#{event}?", to: :resource
    end

    attr_reader :tab, :config

    def initialize(tab, config)
      @tab = tab
      @config = config
    end

    def title
      b = binding
      ERB.new(config[:title]).result(b)
    end

    def if_condition
      method(config.fetch(:if)).call
    rescue KeyError => _e
      true
    end

    def unless_condition
      method(config.fetch(:unless)).call
    rescue KeyError => _e
      false
    end

    def valid?
      # :if condition must pass, if present
      return false unless if_condition
      # :unless condition must fail, if present
      return false if unless_condition

      true
    end

    def permitted?
      # Skip ability check if no permission given
      return true unless config[:permission].present?

      # Ability check
      ability.can?(permission.to_sym, resource)
    end

    def can?
      valid? && permitted?
    end

    def default_url_options
      { controller: controller_path }
    end

    def url_options
      b = binding

      case config[:url_options]
      when String
        ERB.new(config[:url_options]).result(b)
      when Hash
        default_url_options.merge(config[:url_options].deep_transform_values { |v| ERB.new(v).result(b) })
      end
    end

    def target
      config.fetch(:target, '_self')
    end

    def method_missing(name, *args)
      config.fetch(name)
    rescue KeyError => _e
      super
    end
  end
end
