module Ddr::V3
  module Publishable
    Ddr::V3::PublicationService::WORKFLOW_STATES.each do |wf_state|
      define_method(:"#{wf_state}?") { workflow_state == wf_state }
    end

    Ddr::V3::PublicationService::WORKFLOW_EVENTS.each do |event|
      define_method(:"can_#{event}?") { Ddr::V3::PublicationService.can?(resource: self, event:) }
    end

    # The current workflow state of the resource
    # @deprecated Use #workflow_state
    # @return [String]
    def current_workflow_state
      workflow_state
    end
  end
end
