module Ddr::V3
  module Unpublishable
    Ddr::V3::PublicationService::WORKFLOW_STATES.each do |wf_state|
      define_method(:"#{wf_state}?") { false }
    end

    Ddr::V3::PublicationService::WORKFLOW_EVENTS.each do |event|
      define_method(:"can_#{event}?") { false }
    end

    def nonpublishable?
      true
    end
  end
end
