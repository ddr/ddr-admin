module Ddr::V3
  #
  # A set of UI tabs for a resource.
  #
  # See config/tabs.yaml for the source data.
  #
  class Tabs
    include Enumerable

    delegate :each, to: :tabs

    def self.config
      @config ||= Ddr::API::DeepFreeze.call(load_config)
    end

    def self.load_config
      erb = ERB.new(File.read(Rails.root.join('config/tabs.yaml')))
      YAML.load(erb.result, aliases: true).deep_symbolize_keys
    end

    attr_reader :resource, :ability

    delegate :config, to: :class

    def initialize(resource:, ability: nil, active: nil)
      @resource = resource
      @ability = ability || Ddr::Auth::AbilityFactory.call
      @active = active
    end

    def tabs
      @tabs ||= config.fetch(resource.to_sym).map { |conf| Tab.new(self, conf) }
    end

    def tab?(name)
      any? { |tab| tab.name == name }
    end

    def [](name)
      find { |tab| tab.name == name }
    end

    def active
      return first unless @active

      find(-> { first }) { |tab| tab.name == @active }
    end
  end
end
