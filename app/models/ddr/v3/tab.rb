module Ddr::V3
  #
  # A UI tab for a resource.
  #
  # A tab has zero or more actions that are associated with the tab.
  #
  class Tab
    delegate :resource, :ability, to: :tabs

    attr_reader :tabs, :config

    def initialize(tabs, config)
      @tabs = tabs
      @config = config
    end

    def navbar?
      config.fetch(:navbar, true)
    end

    def async?
      config.fetch(:async, false)
    end

    def actions
      @actions ||= config.fetch(:actions, []).map { |conf| TabAction.new(self, conf) }
    end

    def valid_actions
      actions.select(&:valid?)
    end

    def [](name)
      actions.find { |a| a.name == name }
    end

    def css_id
      "tab_#{name}"
    end

    def url_options
      { controller: controller_path, action: name, id: resource }
    end

    def controller_path
      "Ddr::#{resource.class.name.demodulize.pluralize}Controller".constantize.controller_path
    end

    def partial
      config.fetch(:partial, name)
    end

    def method_missing(name, *args)
      config.fetch(name)
    rescue KeyError => _e
      super
    end
  end
end
