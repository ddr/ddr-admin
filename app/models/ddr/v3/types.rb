module Ddr::V3
  module Types
    include Ddr::Types

    PublishableResource = Resource.constrained(type: Publishable)
  end
end
