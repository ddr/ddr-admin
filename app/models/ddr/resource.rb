module Ddr
  #
  # Abstract base class for DDR repository objects.
  #
  # The DDR repository object model is comprised of five resource types:
  #
  #   - Collection - a primary container of repository resources
  #   - Item       - a member of a Collection that represents an individual "work" or digitized object.
  #   - Component  - a content object representing a part or whole of an Item
  #   - Target     - a digitization artifact
  #   - Attachment - a resource that provides additional information about,
  #                  or context for understanding the resource to which it is attached.
  #
  # Each resource type is represented by concrete class in the 'Ddr' namespace:
  #
  #   - Ddr::Collection
  #   - Ddr::Item
  #   - Ddr::Component
  #   - Ddr::Target
  #   - Ddr::Attachment
  #
  class Resource < Valkyrie::Resource
    MODELS = %w[Collection Item Component Target Attachment].freeze

    enable_optimistic_locking

    # Define common metadata attributes
    Ddr::Metadata.fields.each do |name, field|
      attribute name, field.attribute_type
    end

    include Files
    include Describable
    include Governable
    include HasAdminMetadata
    include GlobalID::Identification

    attribute :thumbnail, Ddr::File.optional

    FILE_FIELDS.each do |file_field|
      # Defines "can_have_<field>?" class method
      define_singleton_method :"can_have_#{file_field}?" do
        can_have_file?(file_field)
      end

      delegate "can_have_#{file_field}?", to: :class

      # Defines "has_<field>?" instance method
      define_method :"has_#{file_field}?" do
        has_file?(file_field)
      end
    end

    MODELS.each do |model|
      # def collection?, etc.
      define_method(:"#{model.downcase}?") { human_readable_type == model }
    end

    # Global ID
    def self.find(id)
      Ddr.query_service.find_by(id:)
    end

    # File fields for the resource
    # @return [Array<Symbol>] the list of file fields
    def self.attachable_files
      @attachable_files ||= FILE_FIELDS & fields
    end

    # Is field name a file field for the resource?
    # @param file_field [#to_sym] the file field name
    # @return [Boolean]
    def self.can_have_file?(field_name)
      attachable_files.include?(field_name.to_sym)
    end

    def self.governable?
      fields.include? :admin_policy_id
    end

    def self.captionable?
      can_have_file?(:caption)
    end

    def self.can_be_streamable?
      can_have_file?(:streamable_media)
    end

    # @param model_name [String] model name to canonicalize
    # @return [String] namespace-qualified model name
    # @raise [ValueError] invalid resource model name
    # @example
    #   Ddr::Resource.canonical_model_name('Component') #=> 'Ddr::Component'
    def self.canonical_model_name(model_name)
      demodulized = model_name.to_s.demodulize

      raise ValueError, "Invalid resource model name: #{model_name}" unless MODELS.include?(demodulized)

      "Ddr::#{demodulized}"
    end

    # @return [String] the unqualified (de-modulized) model name
    # @example
    #   Ddr::Component.common_model_name #=> 'Component'
    def self.common_model_name
      name.demodulize
    end

    def self.metadata_fields
      fields - attachable_files - reserved_attributes
    end

    def self.tableized_name
      name.tableize
    end

    def self.to_sym
      name.demodulize.underscore.to_sym
    end

    delegate :can_have_file?,
             :attachable_files,
             :can_be_streamable?,
             :captionable?,
             :common_model_name,
             :governable?,
             :tableized_name,
             :to_sym,
             to: :class

    alias new_record? new_record
    alias resource_model internal_resource

    def rights_statement
      RightsStatement.call(self)
    end

    def title_display
      return title.first if title.present?
      return identifier.first if identifier.present?
      return original_filename if respond_to?(:original_filename) && original_filename.present?

      "[#{id}]"
    end

    # @return [Hash<Symbol, Ddr::File>] the set of files attached to the resource
    def attached_files
      attachable_files.index_with { |f| send(f) }.compact
    end

    # @return [Array<Symbol>] the list of file fields having content
    def attached_files_having_content
      attachable_files.select { |file_field| has_file?(file_field) }
    end

    # Does the file filed have a persisted file?
    # @param file_field [#to_sym] the file field name
    # @return [Boolean]
    def has_file?(file_field)
      return false unless can_have_file?(file_field)

      send(file_field)&.persisted? || false
    end

    # @param file_field [#to_sym] the file field name
    # @raise [ValueError] file_field is not a file field for the resource
    # @return [Ddr::File] the file, or nil if not present
    def get_file(file_field)
      raise ValueError, "'#{file_field}' is not a file field for this resource." unless can_have_file?(file_field)

      self[file_field]
    end

    # By default, no resources are publishable.
    # To enable publication of a particular class of resource,
    # override the `publishable?` method to provide the logic
    # for determining if a particular resource is publishable.
    # @deprecated
    def publishable?
      can_publish?
    end

    # Embargoes are enforced by the `Embargoable` concern,
    # which overrides the `embargo` and `embargoed?` methods
    def embargo
      nil
    end

    def embargoed?
      false
    end

    def has_admin_policy?
      governable? && admin_policy_id.present?
    end

    def streamable?
      has_file?(:streamable_media)
    end

    def captioned?
      has_file?(:caption)
    end

    # @deprecated Use {#[]} instead.
    def values(term)
      # Suppressing deprecation warning due to verbosity.
      # warn "[DEPRECATION] `Ddr::Resource#values(term)' is deprecated; use `#[](term)' instead."
      self[term]
    end

    def thumbnail_path
      thumbnail&.file&.disk_path
    end

    def can_have_children?
      respond_to?(:children)
    end

    def all_identifiers
      identifier + [
        aspace_id,
        bibsys_id,
        contentdm_id,
        ead_id,
        fcrepo3_pid,
        local_id,
        permanent_id,
        id.to_s
      ].compact
    end

    # Render the resource ID as a string
    # @return [String]
    def id_s
      id.to_s
    end
  end
end
