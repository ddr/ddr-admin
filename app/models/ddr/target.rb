module Ddr
  class Target < Resource
    include V3::Unpublishable
    include HasContent

    attribute :for_collection_id, Valkyrie::Types::ID.optional

    # @return [Ddr::Collection] the associated collection, or nil
    # @raise [Valkyrie::Persistence::ObjectNotFoundError] collection not found
    def for_collection
      Ddr.query_service.find_by(id: for_collection_id) if for_collection_id
    end

    # @return [Array<Ddr::Component>] the associated components
    def components
      Ddr.query_service.find_inverse_references_by(resource: self, property: 'target_id')
    end
  end
end
