require 'json'

module Ddr
  module SolrDocumentBehavior
    extend ActiveSupport::Concern

    included do
      alias_method :pid, :id
    end

    class NotFound < Error; end

    module ClassMethods
      def find(id)
        Ddr::API::IndexRepository.get_doc(id)
      rescue Ddr::API::NotFoundError => e
        raise NotFound, e
      end

      def find_by_permanent_id(ark)
        raise ArgumentError, 'ARK argument must be present.' if ark.blank?

        query = Ddr::API::IndexRepository.query(q: 'permanent_id_ssi:%s' % RSolr.solr_escape(ark))
        if doc = query.one
          return doc
        end

        raise NotFound, "SolrDocument not found for permanent id \"#{ark}\"."
      end
    end

    def inspect
      "#<#{self.class.name} id=#{id.inspect}>"
    end

    def method_missing(name, *args, &block)
      if args.empty? && !block
        begin
          field = Ddr::Index::Fields.get(name)
        rescue NameError
          # pass
        else
          # Preserves the default behavior of the deprecated method
          # Blacklight::Solr::Document#get, which this procedure
          # effectively replaces.
          val = self[field]
          return val.is_a?(Array) ? val.join(', ') : val
        end
      end

      super
    end

    def resource
      @resource ||= Valkyrie::MetadataAdapter.find(:index_solr).query_service.find_by(id:)
    end

    def object_create_date
      parse_date(self[Ddr::Index::Fields::RESOURCE_CREATE_DATE])
    end

    def object_modified_date
      parse_date(self[Ddr::Index::Fields::RESOURCE_MODIFIED_DATE])
    end

    def last_fixity_check_on
      get_date(Ddr::Index::Fields::LAST_FIXITY_CHECK_ON)
    end

    def last_virus_check_on
      get_date(Ddr::Index::Fields::LAST_VIRUS_CHECK_ON)
    end

    def has_admin_policy?
      admin_policy_id.present?
    end

    def admin_policy
      return unless has_admin_policy?

      self.class.find(admin_policy_id.gsub(/^id-/, ''))
    end

    def has_children?
      resource.children.present?
    end

    def title_display
      title
    end

    def identifier
      # We want the multivalued version here
      self['identifier_tesim']
    end

    def source
      self['source_tesim']
    end

    delegate :has_thumbnail?, to: :resource

    delegate :has_content?, to: :resource

    delegate :has_intermediate_file?, to: :resource

    delegate :has_extracted_text?, to: :resource

    def content_mime_type
      resource.content_type
    end
    # For duck-typing with Ddr::HasContent
    alias content_type content_mime_type

    delegate :content_human_size, to: :resource

    def targets
      @targets ||= query_service.find_inverse_references_by(resource:, property: 'for_collection_id')
    end

    def targets_count
      @targets_count = targets.count
    end

    def has_target?
      targets_count > 0
    end

    def tableized_name
      resource_model.tableize
    end

    def rights_statement
      @rights_statement ||= RightsStatement.call(self)
    end

    def roles
      @roles ||= resource.roles
    end

    delegate :resource_roles, to: :resource

    delegate :inherited_roles, to: :resource

    def structure
      JSON.parse(fetch(Ddr::Index::Fields::STRUCTURE))
    rescue StandardError
      nil
    end

    def effective_permissions(agents)
      Ddr::Auth::EffectivePermissions.call(self, agents)
    end

    def research_help
      research_help_contact = self[Ddr::Index::Fields::RESEARCH_HELP_CONTACT] || inherited_research_help_contact
      Ddr::Contact.call(research_help_contact) if research_help_contact
    end

    def parent_id
      is_part_of || is_member_of_collection
    end

    def has_parent?
      parent_id.present?
    end

    def parent
      return unless has_parent?

      self.class.find(parent_id)
    end

    def multires_image_file_paths
      if structure
        structure_docs.map { |doc| doc.multires_image_file_path }.compact
      else
        []
      end
    end

    delegate :finding_aid, to: :resource

    delegate :captionable?, to: :resource

    delegate :captioned?, to: :resource

    def caption_type
      return unless captioned?

      resource.caption_type
    end

    def caption_extension
      return unless captioned?

      extensions = Ddr.preferred_file_extensions
      if extensions.include? caption_type
        extensions[caption_type]
      else
        caption_extension_default
      end
    end

    def caption_path
      return unless captioned?

      resource.caption_path
    end

    delegate :streamable?, to: :resource

    def streamable_media_extension
      return unless streamable?

      extensions = Ddr.preferred_file_extensions
      if extensions.include? streamable_media_type
        extensions[streamable_media_type]
      else
        streamable_media_extension_default
      end
    end

    def streamable_media_type
      return unless streamable?

      resource.streamable_media_type
    end

    def streamable_media_path
      return unless streamable?

      resource.streamable_media_path
    end

    def thumbnail_path
      return unless has_thumbnail?

      resource.thumbnail_path
    end

    delegate :rights, to: :resource

    delegate :children, to: :resource

    def embargo
      @embargo ||= get_date(Ddr::Index::Fields::EMBARGO_DATE)
    end

    def embargoed?
      return false unless embargo

      embargo > DateTime.now
    end

    private

    def query_service
      @query_service ||= Valkyrie::MetadataAdapter.find(:index_solr).query_service
    end

    def get_date(field)
      parse_date(self[field])
    end

    def get_json(field)
      JSON.parse Array(self[field]).first
    end

    def parse_date(date)
      Time.parse(date).localtime if date
    end

    def inherited_research_help_contact
      return unless doc = admin_policy

      doc.research_help_contact
    end

    def structure_docs
      structure_repo_ids.map { |repo_id| self.class.find(repo_id) }.compact
    end

    # For simplicity, initial implementation returns repo ID's only from top-level
    # (i.e., not nested) contents.  This is done since we have not clarified what
    # an _ordered_ list of repo ID's should look like if structure contains nested
    # contents.
    def structure_repo_ids
      default_struct_map['contents'].map { |content| content['contents'].map { |content| content['repo_id'] } }.flatten
    end

    def default_struct_map
      structure['default'] || structure.values.first
    end

    def caption_extension_default
      resource.caption.default_file_extension
    end

    def streamable_media_extension_default
      resource.streamable_media.default_file_extension
    end
  end
end
