module Ddr
  class IngestChecksum
    attr_reader :checksum_filepath

    def initialize(checksum_filepath)
      @checksum_filepath = checksum_filepath
      @checksum_hash = {}
    end

    def checksum(filepath)
      checksums[filepath]
    end

    private

    def checksums
      if @checksum_hash.empty?

        ::File.open(checksum_filepath, 'r') do |file|
          file.each_line(chomp: true) do |line|
            sum, path = line.split(/\s+/, 2)
            @checksum_hash[path] = sum
          end
        end

      end
      @checksum_hash
    end
  end
end
