module Ddr
  class Component < Resource
    include V3::Publishable
    include Embargoable
    include HasContent
    include HasExtractedText
    include HasParent

    alias item_id parent_id
    alias item parent

    attribute :caption, Ddr::File.optional
    attribute :derived_image, Ddr::File.optional
    attribute :intermediate_file, Ddr::File.optional
    attribute :multires_image, Ddr::File.optional
    attribute :streamable_media, Ddr::File.optional
    attribute :target_id, Valkyrie::Types::ID.optional

    self.parent_class = Ddr::Item

    # @return [Ddr::Collection] the collection
    def collection
      parent&.parent
    end

    # @return [Valkyrie::ID] the collection ID
    def collection_id
      collection&.id
    end

    # @return [Ddr::Target] the target, or nil
    def target
      Ddr.query_service.find_by(id: target_id) if target_id
    end

    # Roles inherited from parent Item and Colection.
    # @return [Array<Ddr::Auth::Roles::Role>] the roles
    def inherited_roles
      iroles = super
      return iroles | parent.policy_roles if has_parent?

      iroles
    end

    def multires_image_file_path
      multires_image&.file&.disk_path
    end

    def streamable_media_type
      streamable_media&.media_type
    end

    # This method is used in dul-hydra 'ComponentsController' and ddr-public 'StreamController'
    def streamable_media_extension
      return unless filename = streamable_media&.original_filename

      ::File.extname(filename)
    end

    def streamable_media_path
      streamable_media&.file&.disk_path
    end

    def derived_image_file_path
      derived_image&.file&.disk_path
    end

    def caption_type
      caption&.media_type
    end

    # This method is used in dul-hydra 'ComponentsController' and ddr-public 'CaptionsController'
    def caption_extension
      return unless filename = caption&.original_filename

      ::File.extname(filename)
    end

    def caption_path
      caption&.file&.disk_path
    end

    def embargo
      super || parent&.embargo
    end
  end
end
