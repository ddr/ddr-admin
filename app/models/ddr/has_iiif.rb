module Ddr
  module HasIiif
    extend ActiveSupport::Concern
    require 'iiif/v3/presentation'

    included do
      attribute :iiif_file, Ddr::File.optional
    end

    # @return [Hash] the IIIF manifest, or nil
    # @raise [Valkyrie::StorageAdapter::FileNotFound]
    def iiif
      return unless has_iiif_file?

      JSON.parse(iiif_file.file.read)
    end
  end
end
