module Ddr
  module Types
    include Dry.Types()

    # Hex 00-1F, except 0A (LF), plus 7F (DEL)
    REM_CONTROL_CHARS  = Regexp.new('[\x00-\x09\x0B-\x1F\x7F]')

    DEFAULT_MEDIA_TYPE = 'application/octet-stream'.freeze

    ##
    ## Types copied from Valkyrie::Types into Ddr::Types namespace
    ##
    # This array wraps scalar values and Hashes, and defaults to an empty array
    Array = Valkyrie::Types::Array

    # A set is an Array (as above) of unique values
    Set = Valkyrie::Types::Set

    # Used for when an input may be an array, but the output needs to be a single string.
    SingleValuedString = Valkyrie::Types::SingleValuedString

    # Valkyrie::ID
    ID = Valkyrie::Types::ID

    # Valkyrie::Persistence::OptimisticLockToken
    OptimisticLockToken = Valkyrie::Types::OptimisticLockToken

    ##
    ## Procs for custom type constructors
    ##
    # @api private
    Sanitize = ->(input) { input.scrub.force_encoding(Encoding::UTF_8).gsub(REM_CONTROL_CHARS, '').strip }

    # @api private
    ToCSV = ->(input) { ::Array.wrap(input).join(',') }

    ToUniqueCSV = ->(input) { ::Array.wrap(input).uniq.join(',') }

    ToResourceID = proc do |input|
      output = input.try(:id) || input.try(:model_id) || input

      output.to_s
    end

    ToFilePath = proc do |input|
      path = input.try(:path) || input.try(:to_path) || input.try(:file_path) || Pathname.new(input)

      path.to_s
    end

    GLOBAL_ID_RE = Regexp.new('^gid://')

    ToResource = proc do |input|
      case input
      when Ddr::Resource
        input
      when ::SolrDocument
        Ddr::API::IndexRepository.to_resource(object: input.to_h)
      when Valkyrie::ID, Ddr::API::UUID_RE
        Ddr.query_service.find_by(id: input)
      when Ezid::Identifier, Ddr::API::ARK_RE
        Ddr::API::ResourceRepository.find_by_permanent_id(input.to_s)
      when GlobalID, URI::GID, GLOBAL_ID_RE
        GlobalID::Locator.locate(input)
      end
    end

    ToDigestType = proc do |input|
      case input
      when String
        input.delete('-').upcase
      when Symbol
        input.to_s.upcase
      when Ddr::Digest
        input.type
      when ::Digest::Class, OpenSSL::Digest
        input.class.name.demodulize # best effort
      end
    end

    ##
    ## DDR custom types
    ##

    # Sanitized string
    # - scrubs invalid bytes
    # - forces UTF-8 encoding
    # - removes control sequences, except newline
    # - strips leading and trailing whitespace
    SanitizedString = Constructor(Coercible::String, Sanitize)

    # A string of comma-separated values
    CommaSeparatedValues = Constructor(String, ToCSV)

    # A string of unique, comma-separated values
    UniqueCommaSeparatedValues = Constructor(String, ToUniqueCSV)

    # A valid email address string, with coercion
    EmailAddress = String.constrained(format: Ddr::API::EMAIL_RE)

    # A Ddr::Resource, ::SolrDocument, resource ID or resource ID string (UUID), with coercion
    ResourceID = Constructor(Any, ToResourceID).constrained(uuid_v4: true)

    # A Ddr::Resource, resource ID or resource ID string (UUID), with coercion
    Resource = Constructor(Any, ToResource).constrained(type: Ddr::Resource)

    # A valid http/https URI string, with coercion
    URI = Coercible::String.constrained(uri: /https?/)

    # The name of a file field -- e.g., :content, :thumbnail, 'streamable_media', etc.
    FileField = Coercible::Symbol.enum(*Ddr::Files::FILE_FIELDS)

    # File-like object that has a `path' and `size' -- includes ::File, Rack::Test::UploadedFile, etc.
    FileLike = Interface(:path, :size)

    # A file digest algorithm acknowledged by DDR -- e.g., 'SHA1', :md5, 'SHA-256'
    # stringifies, upcases and removes hyphen
    DigestType = Constructor(Any, ToDigestType).constrained(included_in: %w[MD5 SHA1 SHA256 SHA384 SHA512])

    # A registered/known media type, with fallback
    MediaType = String.constrained(included_in: MIME::Types.map(&:to_s)).fallback(DEFAULT_MEDIA_TYPE)

    # Coerces to a String that might be an actual file path, not guaranteed to exist or be a File.
    FilePath = Constructor(Any, ToFilePath)

    # Coerces file path to real absolute path
    ExistingFilePath = FilePath.constructor do |path|
      ::File.realpath(path)
    rescue Errno::ENOENT => e
      raise Dry::Types::CoercionError, e.message
    end

    # Get media type for file at path
    FileMagic = ExistingFilePath.constructor { |path| IO.popen(['file', '-b', '--mime-type', path], &:read).strip }

    # Coerces to a sanitized string for use as original_filename.
    OriginalFilename = Any.constructor do |input|
      filename = ::File.basename(input.try(:original_filename) || FilePath[input])

      SanitizedString[filename]
    end

    Agent = Any.constructor { |input| EmailAddress[input.try(:agent) || input] }

    PermanentID = Coercible::String.constrained(format: Ddr::API::ARK_RE)

    # resource model types with coercion
    Collection = Resource.constrained(type: Ddr::Collection)
    Item       = Resource.constrained(type: Ddr::Item)
    Component  = Resource.constrained(type: Ddr::Component)
    Target     = Resource.constrained(type: Ddr::Target)
    Attachment = Resource.constrained(type: Ddr::Attachment)

    DdrFile = Instance(Ddr::File)

    ContentResource = Resource.constrained(type: Ddr::HasContent)

    ImageResource = ContentResource.constrained(true: :image?)

    IiifResource = Resource.constrained(type: Ddr::HasIiif)

    StructuralResource = Resource.constrained(type: Ddr::HasStructMetadata)

    PublishedResource = Resource.constrained(true: :published?)

    PersistedResource = Resource.constrained(true: :persisted?)
  end
end
