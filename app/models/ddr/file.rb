module Ddr
  #
  # Represents a file (a.k.a. datastream or bytestream) attached to a repository resource
  #
  class File < Valkyrie::Resource
    NULL = ::File::NULL

    attribute :digest, Valkyrie::Types::Set.of(Ddr::Digest)
    attribute :file_identifier, Valkyrie::Types::ID
    attribute :media_type, Valkyrie::Types::Strict::String.optional
    attribute :original_filename, Valkyrie::Types::Strict::String

    DEFAULT_FILE_EXTENSION = 'bin'

    Timestamps = Struct.new(:created, :updated)

    delegate :created, :updated, to: :timestamps

    def content
      # warn "[DEPRECATION] 'Ddr::File#content' is deprecated and potentially unsafe."

      file&.read
    end

    def stream
      file&.stream
    end

    # Bridge API to cover a transition to setting the `created_at` and `updated_at` attributes
    # for persisted files.  We fall back to the timestamps of the files on disk (or in storage).
    # @return [Ddr::File::Timestamps] the timestamps
    def timestamps
      Timestamps.new(created_at || file_created_at, updated_at || file_updated_at)
    end

    # Is the file record persisted (new_record=false) *OR* is it stored?
    # @return [Boolean]
    # @see #stored?
    # @note We cannot rely on Valkyrie::Resource#persisted? because we do not persist file records separately
    #   from the resources to which they are attached -- so 'new_record' is not set to false automatically --
    #   and we did not bother initially to manually set new_record=false on persistence of the data.
    def persisted?
      super || stored?
    end

    # Does the file have a persistent identifier for its storage location?
    # @return [Boolean]
    def stored?
      file_identifier.present?
    end

    # @note Provides continuity with `Ddr::Resource` API.
    # @return [Boolean] whether the file has not been persisted to storage.
    def new_record?
      !persisted?
    end

    # @api private
    def storage_adapter
      return nil unless stored?

      @storage_adapter ||= Valkyrie::StorageAdapter.adapter_for(id: file_identifier)
    end

    # @return [Valkyrie::StorageAdapter::File] the file object returned from the storage adapter
    def file
      return nil unless stored?

      storage_adapter.find_by(id: file_identifier)
    end

    # File creation date from the storage adapter, if the adapter supported timestamps and the file is stored.
    # @return [Valkyrie::Types::DateTime]
    def file_created_at
      return nil unless stored?

      storage_adapter.created_at(file_identifier) if storage_adapter.supports?(:timestamps)
    end

    # File modification date from the storage adapter, if the adapter supports timestamps and the file is stored.
    # @return [Valkyrie::Types::DateTime]
    def file_updated_at
      return nil unless stored?

      storage_adapter.updated_at(file_identifier) if storage_adapter.supports?(:timestamps)
    end

    # File path from the storage adapter, if the adapter supports `file_path' and the file is stored.
    # @return [String]
    def file_path
      return nil unless stored?

      storage_adapter.file_path(file_identifier) if storage_adapter.supports?(:file_path)
    end

    # File size, if the file is stored.
    # Attempts first to retrieve file size from the adapter (because that may be more efficient),
    # then from the file object returned by the adapter, if necessary.
    # @return [Integer]
    def file_size
      return nil unless stored?

      storage_adapter.supports?(:file_size) ? storage_adapter.file_size(file_identifier) : file.size
    end

    # SHA1 digest from the file metadata, if available.
    # @return [String]
    def sha1
      digest.detect(&:sha1?)&.value
    end

    # Return default file extension for file based on MIME type
    # @return [String] the extension
    def default_file_extension
      mimetypes = MIME::Types[media_type] # empty array when media_type is nil

      mimetypes.first&.preferred_extension || DEFAULT_FILE_EXTENSION
    end

    def stored_checksums_valid?
      checksums = digest.to_h { |d| [d.type, d.value] }

      file.valid?(digests: checksums)
    end

    def validate_checksum!(checksum_value, checksum_type)
      raise Ddr::Error, I18n.t('ddr.checksum.validation.must_be_stored') unless file_identifier.present?
      raise Ddr::ChecksumInvalid, I18n.t('ddr.checksum.validation.internal_check_failed') unless stored_checksums_valid?

      # TODO: We can return early if checksum_type, checksum_value matches a stored digest.

      if file.valid? digests: { checksum_type => checksum_value }
        I18n.t('ddr.checksum.validation.valid', type: checksum_type, value: checksum_value)
      else
        raise Ddr::ChecksumInvalid,
              I18n.t('ddr.checksum.validation.invalid', type: checksum_type, value: checksum_value)
      end
    end
  end
end
