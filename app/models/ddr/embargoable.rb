module Ddr
  module Embargoable
    extend ActiveSupport::Concern

    # Embargo date on the resource
    # @return [DateTime] embargo date, or nil
    def embargo
      normalize(available)
    end

    # Is the resource embargoed?
    # @return [Boolean]
    def embargoed?
      return false if embargo.nil?

      embargo > DateTime.now
    end

    private

    def normalize(value)
      case value
      when ::Time
        value.to_datetime
      when ::Array
        normalize(value.first)
      else
        value
      end
    end
  end
end
