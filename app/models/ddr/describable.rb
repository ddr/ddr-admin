module Ddr
  module Describable
    extend ActiveSupport::Concern

    def self.term_names
      Ddr::Metadata.descriptive.keys
    end

    # Used in ddr-admin view
    def has_desc_metadata?
      desc_metadata_terms.any? { |t| self[t].present? }
    end

    def desc_metadata_terms
      Ddr::Describable.term_names
    end

    # Update all descMetadata terms with values in hash
    # Note that term not having key in hash will be set to nil!
    def set_desc_metadata(term_values_hash)
      desc_metadata_terms.each { |t| set_value(t, term_values_hash[t]) }
    end

    def descriptive_metadata
      to_h.slice(*Metadata.descriptive.keys)
    end
  end
end
