module Ddr
  class ManifestBasedIngest
    include ActiveModel::Model

    attr_reader :admin_set, :collection_id, :collection_title, :config_file, :configuration, :manifest_file
    attr_accessor :results, :user

    # Lifecycle events
    FINISHED = 'finished.manifest_based_ingest'

    Results = Struct.new(:batches, :errors, :manifest)

    DEFAULT_CONFIG_FILE = Rails.root.join('config/deploy/manifest_based_ingest.yml')

    validates(:user, presence: true)
    validate :collection_must_exist, if: proc { collection_id.present? }
    validate :manifest_must_exist, if: proc { manifest_file.present? }
    validates(:admin_set, presence: { unless: proc { collection_id.present? } })

    def self.common_model_name
      'ManifestBasedIngest'
    end

    def self.default_config
      YAML.load_file(DEFAULT_CONFIG_FILE).deep_symbolize_keys
    end

    def initialize(args)
      @admin_set = args['admin_set']
      @collection_id = args['collection_id']
      @collection_title = args['collection_title']
      @config_file = args['config_file'] || DEFAULT_CONFIG_FILE.to_s
      @manifest_file = args['manifest_file']
      @configuration = load_configuration
      @user = User.find_by_user_key(args['batch_user'])
      @results = Results.new
    end

    def process
      processing_errors = []
      if ::File.exist?(@manifest_file)
        @manifest = Manifest.new(@manifest_file, @configuration[:volumes])
        results.manifest = @manifest
        unless @manifest.valid?
          results.errors = results.manifest.errors
          return results
        end
        begin
          results.batches = build_batches
        rescue Ddr::Batch::Error => e
          processing_errors << e.message
        end
        results.errors = processing_errors
      else
        results.errors = ["Manifest file #{@manifest_file} does not exist"]
      end
      results
    end

    private

    def build_batches
      builder_args = {
        user:,
        batch_name: 'Manifest-Based Ingest'
      }
      builder_args.merge!(admin_set:) if admin_set
      builder_args.merge!(collection_repo_id: collection_id) if collection_id
      builder_args.merge!(collection_title:) if collection_title
      builder_args.merge!(manifest: @manifest)
      batch_builder = BuildBatchesFromManifestBasedIngest.new(**builder_args)
      batch_builder.call
    end

    def collection_must_exist
      return if Ddr.query_service.find_by(id: collection_id)

      errors.add(:collection_id, 'must point to existing collection')
    end

    def manifest_must_exist
      return if ::File.exist?(@manifest_file)

      errors.add(:manifest_file, 'does not exist')
    end

    def load_configuration
      YAML.load_file(config_file).symbolize_keys
    end
  end
end
