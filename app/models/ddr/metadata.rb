module Ddr
  #
  # Encapulates DDR metadata definitions
  #
  module Metadata
    #
    # For legacy reasons, we distinguish between "descriptive" and "administrative"
    # metadata in the admin UI. In the future, we may fully remove this division.
    #
    ADMINISTRATIVE = %i[ admin_set
                         aspace_id
                         bibsys_id
                         contentdm_id
                         display_format
                         display_label
                         ead_id
                         fcrepo3_pid
                         ingested_by
                         ingestion_date
                         local_id
                         nested_path
                         permanent_id
                         permanent_url
                         research_help_contact
                         rights_note
                         viewing_direction
                         workflow_state].freeze

    class << self
      # Descriptive metadata fields
      # @return [Hash<Symbol, Ddr::Metadata::Field>]
      def descriptive
        @descriptive ||= fields.except(*ADMINISTRATIVE)
      end

      # Administrative metadata fields
      # @return [Hash<Symbol, Ddr::Metadata::Field>]
      def administrative
        @administrative ||= fields.slice(*ADMINISTRATIVE)
      end

      # All metadata fields
      # @return [Hash<Symbol, Ddr::Metadata::Field>]
      def fields
        @fields ||= config.fetch('properties').each_with_object(FieldList.new) do |(name, schema), memo|
          memo[name.to_sym] = Field.new(**schema.symbolize_keys, name:)
        end
      end

      # Metadata field names
      # @return [Array<Symbol>]
      def field_names
        fields.keys
      end

      # Metadata field defaults
      # @return [Hash]
      def defaults
        fields.transform_values(&:default)
      end

      # Metadata configuration
      # @return [Hash]
      def config
        @config ||= Ddr::API::DeepFreeze.call(load_config)
      end

      # Retrieve a metadata field
      # @param name [Symbol, String] field name
      # @return [Ddr::Metadata::Field]
      # @raise [KeyError] if requested field name not found
      def [](name)
        fields.fetch(name.to_sym)
      end

      def config_file
        Rails.root.join('config/schemas/metadata.yaml')
      end

      def load_config
        erb = ERB.new ::File.read(config_file)
        YAML.load(erb.result)
      end
    end

    class FieldList < ::Hash
      def names
        keys
      end

      def defaults
        transform_values(&:default)
      end
    end

    #
    # A metadata field definition from the JSON schema file.
    #
    class Field < Dry::Struct
      attribute  :name,        Ddr::Types::String
      attribute  :title,       Ddr::Types::String
      attribute? :description, Ddr::Types::String
      attribute  :type,        Ddr::Types::String.enum('string', 'array')
      attribute? :format,      Ddr::Types::String
      attribute? :readOnly,    Ddr::Types::Bool.default(false)
      attribute? :deprecated,  Ddr::Types::Bool.default(false)
      attribute? :items,       Ddr::Types::Hash
      attribute? :maxItems,    Ddr::Types::Coercible::Integer
      attribute? :enum,        Ddr::Types::Array.of(Ddr::Types::String)
      attribute? :default,     Ddr::Types::String

      def required?
        false
      end

      def multiple?
        type == 'array' && maxItems != 1
      end

      def single?
        !multiple?
      end

      def index_field
        "#{name}_tsim"
      end

      # @return [Dry::Types::Type] the model attribute type for the field
      def attribute_type
        return array_type if type == 'array'

        string_type
      end

      def string_type
        t = string_base_type
        t = t.default(default).constructor { |value| value.nil? ? Dry::Types::Undefined : value } if default
        t = t.enum(*enum) if enum.present?
        t = t.optional unless required? || default
        t
      end

      # @api private
      # @return [Dry::Types::Type] the model attribute type for the array field
      # @note Due to https://github.com/samvera/valkyrie/issues/747 we have to be careful
      #   constraining fields in ways that could cause existing data to "break".
      def array_type
        # See @note, above
        # raise "Unexpected type: #{inspect}" unless items['type'] == 'string'

        # item_type = array_item_type
        # item_type = item_type.enum(*items['enum']) if items.key?('enum')

        t = Ddr::Types::Set # .of(item_type)
        t = t.constrained(max_size: maxItems) if maxItems
        t
      end

      # See @note in #array_type
      # @api private
      # def array_item_type
      #   return date_type if items['format'] == 'date-time'

      #   Ddr::Types::String
      # end

      def date_type
        Ddr::Types::JSON::DateTime
      end

      # @api private
      def string_base_type
        return date_type if format == 'date-time'

        Ddr::Types::SingleValuedString
      end
    end
  end
end
