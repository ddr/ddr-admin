module Ddr
  module Files
    # Supported checksum algorithms
    CHECKSUM_TYPE_MD5    = 'MD5'.freeze
    CHECKSUM_TYPE_SHA1   = 'SHA1'.freeze
    CHECKSUM_TYPE_SHA256 = 'SHA256'.freeze
    CHECKSUM_TYPE_SHA384 = 'SHA384'.freeze
    CHECKSUM_TYPE_SHA512 = 'SHA512'.freeze

    # @see Ddr::Digest
    CHECKSUM_TYPES = Valkyrie::Types::Strict::String.enum(CHECKSUM_TYPE_MD5,
                                                          CHECKSUM_TYPE_SHA1,
                                                          CHECKSUM_TYPE_SHA256,
                                                          CHECKSUM_TYPE_SHA384,
                                                          CHECKSUM_TYPE_SHA512)

    # File field names
    CAPTION             = :caption
    DERIVED_IMAGE       = :derived_image
    EXTRACTED_TEXT      = :extracted_text
    FITS_FILE           = :fits_file
    IIIF_FILE           = :iiif_file
    INTERMEDIATE_FILE   = :intermediate_file
    MULTIRES_IMAGE      = :multires_image
    ORIGINAL_FILE       = :content
    STREAMABLE_MEDIA    = :streamable_media
    STRUCTURAL_METADATA = :struct_metadata
    THUMBNAIL           = :thumbnail

    # aliases
    CONTENT = ORIGINAL_FILE
    STRUCT_METADATA = STRUCTURAL_METADATA

    # File fields - maps attribute names to file types
    FILE_FIELDS = %i[caption
                     content
                     derived_image
                     extracted_text
                     fits_file
                     iiif_file
                     intermediate_file
                     multires_image
                     streamable_media
                     struct_metadata
                     thumbnail].freeze

    def self.file_field?(name)
      FILE_FIELDS.include?(name.to_sym)
    end
  end
end
