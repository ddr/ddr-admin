module Ddr::Admin
  class MetadataFileAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :create, Ddr::MetadataFile if curator?

      return unless authenticated?

      can %i[show procezz], Ddr::MetadataFile, user_id: user.id
    end
  end
end
