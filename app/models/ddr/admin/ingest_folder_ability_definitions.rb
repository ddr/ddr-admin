module Ddr::Admin
  class IngestFolderAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :create, Ddr::IngestFolder do |ingest_folder|
        ingest_folder.collection_id? && can?(:add_children, ingest_folder.collection)
      end

      can %i[show procezz], Ddr::IngestFolder, user:
    end
  end
end
