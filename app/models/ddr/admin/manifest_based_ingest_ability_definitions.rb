module Ddr::Admin
  class ManifestBasedIngestAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :create, Ddr::ManifestBasedIngest if can?(:create, Ddr::Collection)
      can :show, Ddr::ManifestBasedIngest, user:
    end
  end
end
