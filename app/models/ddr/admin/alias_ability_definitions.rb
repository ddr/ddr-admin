module Ddr::Admin
  class AliasAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      alias_action :attachments,
                   :captions,
                   :collection_info,
                   :components,
                   :event,
                   :events,
                   :files,
                   :iiif,
                   :intermediate,
                   :items,
                   :stream,
                   :structural_metadata,
                   :targets,
                   to: :read

      alias_action :roles, to: :grant

      alias_action :admin_metadata,
                   :generate_iiif,
                   :generate_structure,
                   to: :update

      alias_action :preview, to: :publish if Ddr::Admin.publication_preview_enabled
    end
  end
end
