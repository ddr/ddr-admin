module Ddr::Admin
  #
  # Ported from ddr-core v1.16.0, with changes.
  #
  class AdminSetAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :export, Ddr::AdminSet if curator?
    end
  end
end
