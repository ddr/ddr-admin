module Ddr::Admin
  #
  # Ported from ddr-core, with changes.
  #
  class CollectionAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :create, Ddr::Collection if curator?

      can :export, Ddr::Collection do |obj|
        has_policy_permission?(obj, Ddr::Auth::Permissions::READ)
      end
    end

    private

    def policy_permissions(obj)
      obj.policy_roles
         .select { |r| agents.include?(r.agent) }
         .map(&:permissions).flatten.uniq
    end

    def has_policy_permission?(obj, perm)
      policy_permissions(obj).include?(perm)
    end
  end
end
