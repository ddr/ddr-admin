module Ddr::Admin
  class NestedFolderIngestAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :create, Ddr::NestedFolderIngest if can?(:create, Ddr::Collection)
      can :show, Ddr::NestedFolderIngest, user:
    end
  end
end
