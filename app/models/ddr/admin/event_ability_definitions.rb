module Ddr::Admin
  class EventAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :read, Ddr::Events::Event do |event|
        can? :read, event.resource_id
      end
    end
  end
end
