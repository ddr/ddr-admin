module Ddr::Admin
  class StandardIngestAbilityDefinitions < Ddr::Auth::AbilityDefinitions
    def call
      can :create, Ddr::StandardIngest if can?(:create, Ddr::Collection)
      can :show, Ddr::StandardIngest, user:
    end
  end
end
