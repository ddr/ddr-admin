module Ddr::Batch
  class UpdateBatchObject < BatchObject
    def local_validations
      errs = []
      errs << "#{@error_prefix} resource_id required for UPDATE operation" unless resource_id
      if resource_id
        begin
          resource = Ddr.query_service.find_by(id: resource_id)
          errs << "#{@error_prefix} #{batch.user.user_key} not permitted to edit #{resource_id}" unless
              batch.user.can?(:edit, resource)
        rescue Valkyrie::Persistence::ObjectNotFoundError
          errs << "#{@error_prefix} resource_id #{resource_id} not found in repository"
        end
      end
      errs
    end

    def model_datastream_keys
      return unless resource_id

      begin
        resource = Ddr.query_service.find_by(id: resource_id)
        resource.attachable_files.map(&:to_s)
      rescue Valkyrie::Persistence::ObjectNotFoundError
        nil
      end
    end

    def process(user, opts = {})
      return if verified

      begin
        repo_object = update_repository_object(user, opts)
      rescue Valkyrie::Persistence::StaleObjectError
        # Retry once. Other exceptions will be caught by Ddr::Batch::ProcessBatchObject
        repo_object = update_repository_object(user, opts)
      end
      verifications = verify_repository_object
      verification_outcome_detail = []
      verified = true
      verifications.each do |key, value|
        verification_outcome_detail << "#{key}...#{value}"
        verified = false if value.eql?(VERIFICATION_FAIL)
      end
      update(verified:)
      Ddr::Events::ValidationEvent.new.tap do |event|
        event.object = repo_object
        event.failure! unless verified
        event.summary = format(EVENT_SUMMARY, label: 'Object update validation', batch_id: id,
                                              identifier:, model:)
        event.detail = verification_outcome_detail.join("\n")
        event.save!
      end
      repo_object
    end

    def results_message
      if resource_id
        message_level = verified ? Logger::INFO : Logger::WARN
        verification_result = verified ? 'Verified' : 'VERIFICATION FAILURE'
        ProcessingResultsMessage.new(message_level, "Updated #{resource_id}...#{verification_result}")
      else
        ProcessingResultsMessage.new(Logger::ERROR, "Attempt to update #{model} #{identifier} FAILED")
      end
    end

    def event_log_comment
      "Updated by batch process (Batch #{batch.id}, BatchObject #{id})"
    end

    private

    def update_repository_object(_user, _opts = {})
      repo_object = nil
      begin
        has_changes = false
        repo_object = Ddr.query_service.find_by(id: resource_id)
        update!(model: repo_object.class.name) unless model.present?
        change_set = Ddr::ResourceChangeSet.change_set_for(repo_object)
        # changes to workflow state are handled by jobs and so are ignored.
        batch_object_attributes.each do |a|
          if a.operation.eql?(BatchObjectAttribute::OPERATION_ADD)
            if a.name == 'workflow_state'
              # Email is disabled here, because batch publication jobs can end
              # up sending hundreds of emails. TODO: refactor so that 1 email per
              # batch gets sent.
              Ddr::V3::PublicationService.transition_to(state: a.value, resource: repo_object, async: true)
            else
              add_attribute(change_set, a)
              has_changes = true
            end
          elsif a.operation.eql?(BatchObjectAttribute::OPERATION_CLEAR) && a.name != 'workflow_state'
            clear_attribute(change_set, a)
            has_changes = true
          end
        end
        batch_object_datastreams.each do |d|
          repo_object = if d.operation.eql?(BatchObjectDatastream::OPERATION_ADDUPDATE)
                          populate_datastream(change_set, d)
                          has_changes = true
                        end
        end
        if has_changes # No-op if only a workflow_state change
          change_set.comment = event_log_comment
          Ddr::ResourceChangeSetPersister.new.save(change_set:)
        end
      rescue Exception => e
        logger.error("Error in updating repository object #{resource_id} for #{identifier} : : #{e}")
        raise e
      end
      change_set.resource
    end
  end
end
