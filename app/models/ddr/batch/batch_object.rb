module Ddr
  module Batch
    # This is a superclass containing methods common to all batch object objects.  It is not intended to be instantiated directly.
    # This superclass and its subclasses are designed following the ActiveRecord single-table inheritance pattern.
    class BatchObject < ::ApplicationRecord
      belongs_to :batch, inverse_of: :batch_objects
      has_many :batch_object_attributes, -> { order 'id ASC' }, inverse_of: :batch_object, dependent: :destroy
      has_many :batch_object_datastreams, inverse_of: :batch_object, dependent: :destroy
      has_many :batch_object_messages, inverse_of: :batch_object, dependent: :destroy
      has_many :batch_object_relationships, inverse_of: :batch_object, dependent: :destroy
      has_many :batch_object_roles, inverse_of: :batch_object, dependent: :destroy

      VERIFICATION_PASS = 'PASS'
      VERIFICATION_FAIL = 'FAIL'

      EVENT_SUMMARY = <<-EOS
  %<label>s
  Batch object database id: %<batch_id>s
  Batch object identifier: %<identifier>s
  Model: %<model>s
      EOS

      ProcessingResultsMessage = Struct.new(:level, :message)

      def self.resource_id_from_identifier(identifier, batch_id)
        query = 'identifier = :identifier'
        query << ' and batch_id = :batch_id' if batch_id
        params = { identifier: }
        params[:batch_id] = batch_id if batch_id
        sort = 'updated_at asc'
        found_objects = BatchObject.where(query, params).order(sort)
        resource_ids = []
        found_objects.each { |obj| resource_ids << obj.resource_id }
        resource_ids
      end

      def validate
        @error_prefix = I18n.t('ddr.batch.errors.prefix', identifier:, id:)
        errors = []
        errors += validate_model if model
        errors += validate_datastreams if batch_object_datastreams
        errors += validate_relationships if batch_object_relationships
        errors += local_validations
        errors
      end

      def local_validations
        []
      end

      def model_datastream_keys
        raise NotImplementedError
      end

      def process(user, opts = {})
        raise NotImplementedError
      end

      def results_message
        raise NotImplementedError
      end

      Results = Struct.new(:repository_object, :verified, :verifications)

      private

      def validate_model
        errs = []
        begin
          Ddr::Resource.canonical_model_name(model).constantize
        rescue NameError
          errs << "#{@error_prefix} Invalid model name: #{model}"
        end
        errs
      end

      def validate_datastreams
        errs = []
        batch_object_datastreams.each do |d|
          if model_datastream_keys.present? && !model_datastream_keys.include?(d.name)
            errs << "#{@error_prefix} Invalid datastream name for #{model}: #{d.name}"
          end
          unless BatchObjectDatastream::PAYLOAD_TYPES.include?(d.payload_type)
            errs << "#{@error_prefix} Invalid payload type for #{d.name} datastream: #{d.payload_type}"
          end
          if d.payload_type.eql?(BatchObjectDatastream::PAYLOAD_TYPE_FILENAME) && !::File.readable?(d.payload)
            errs << "#{@error_prefix} Missing or unreadable file for #{d[:name]} datastream: #{d[:payload]}"
          end
          if d.checksum && !d.checksum_type
            errs << "#{@error_prefix} Must specify checksum type if providing checksum for #{d.name} datastream"
          end
          next unless d.checksum_type

          unless Ddr::Files::CHECKSUM_TYPES.values.include?(d.checksum_type)
            errs << "#{@error_prefix} Invalid checksum type for #{d.name} datastream: #{d.checksum_type}"
          end
        end
        errs
      end

      def validate_relationships
        errs = []
        batch_object_relationships.each do |r|
          obj_model = nil
          unless BatchObjectRelationship::OBJECT_TYPES.include?(r[:object_type])
            errs << "#{@error_prefix} Invalid object_type for #{r[:name]} relationship: #{r[:object_type]}"
          end
          next unless r[:object_type].eql?(BatchObjectRelationship::OBJECT_TYPE_ID)

          if batch.present? && batch.found_resource_ids.keys.include?(r[:object])
            obj_model = batch.found_resource_ids[r[:object]]
          else
            begin
              obj_model = ::SolrDocument.find(r[:object]).resource_model
              batch.add_found_resource_id(r[:object], obj_model) if batch.present?
            rescue ::SolrDocument::NotFound
              resource_id_in_batch = false
              resource_id_in_batch = true if batch.present? && batch.pre_assigned_resource_ids.include?(r[:object])
              unless resource_id_in_batch
                errs << "#{@error_prefix} #{r[:name]} relationship object does not exist: #{r[:object]}"
              end
            end
          end
          next unless obj_model

          klass = case r[:name]
                  when 'admin_policy', 'for_collection'
                    'Ddr::Collection'
                  when 'parent'
                    model.constantize.parent_class.to_s
                  end
          unless batch.found_resource_ids[r[:object]].eql?(klass)
            errs << "#{@error_prefix} #{r[:name]} relationship object #{r[:object]} exists but is not a(n) #{klass}"
          end
        end
        errs
      end

      def verify_repository_object
        verifications = {}
        begin
          repo_object = Ddr.query_service.find_by(id: resource_id)
        rescue Valkyrie::Persistence::ObjectNotFoundError
          verifications['Object exists in repository'] = VERIFICATION_FAIL
        else
          verifications['Object exists in repository'] = VERIFICATION_PASS
          verifications['Object is correct model'] = verify_model(repo_object) if model
          unless batch_object_attributes.empty?
            batch_object_attributes.each do |a|
              if a.operation == BatchObjectAttribute::OPERATION_ADD
                verifications["#{a.name} attribute set correctly"] = verify_attribute(repo_object, a)
              end
            end
          end
          unless batch_object_datastreams.empty?
            batch_object_datastreams.each do |d|
              verifications["#{d.name} datastream present and not empty"] = verify_datastream(repo_object, d)
              if d.checksum
                verifications["#{d.name} external checksum match"] =
                  verify_datastream_external_checksum(repo_object, d)
              end
            end
          end
          unless batch_object_relationships.empty?
            batch_object_relationships.each do |r|
              verifications["#{r.name} relationship is correct"] = verify_relationship(repo_object, r)
            end
          end
          unless batch_object_roles.empty?
            batch_object_roles.each do |r|
              verifications["#{r.role_scope} #{r.role_type} #{r.agent} role is correct"] = verify_role(repo_object, r)
            end
          end
          result = Ddr::FixityCheck.call(repo_object)
          verifications['Fixity check'] = result.success ? VERIFICATION_PASS : VERIFICATION_FAIL
        end
        verifications
      end

      def verify_model(repo_object)
        if repo_object.instance_of?(Ddr::Resource.canonical_model_name(model).constantize)
          VERIFICATION_PASS
        else
          VERIFICATION_FAIL
        end
      rescue NameError
        VERIFICATION_FAIL
      end

      def verify_attribute(repo_object, attribute)
        case repo_object.attributes[attribute.name.to_sym]
        when ::Array
          repo_object.attributes[attribute.name.to_sym].include?(attribute.value.strip) ? VERIFICATION_PASS : VERIFICATION_FAIL
        when ::Time
          repo_object.attributes[attribute.name.to_sym].to_s[0..9] == attribute.value.strip[0..9] ? VERIFICATION_PASS : VERIFICATION_FAIL
        when ::DateTime
          repo_object.attributes[attribute.name.to_sym].to_s[0..9] == attribute.value.strip[0..9] ? VERIFICATION_PASS : VERIFICATION_FAIL
        end
      end

      def verify_datastream(repo_object, datastream)
        if repo_object.attributes.include?(datastream.name.to_sym) &&
           repo_object.send(datastream.name.to_sym)&.file_identifier.present?
          VERIFICATION_PASS
        else
          VERIFICATION_FAIL
        end
      end

      def verify_datastream_external_checksum(repo_object, datastream)
        repo_object.send(datastream.name.to_sym).validate_checksum! datastream.checksum, datastream.checksum_type
        VERIFICATION_PASS
      rescue Ddr::ChecksumInvalid
        VERIFICATION_FAIL
      end

      def verify_relationship(repo_object, relationship)
        relationship_object = repo_object.send(:"#{relationship.name}")
        if !relationship_object.nil? &&
           relationship_object.id.id.eql?(relationship.object) &&
           relationship_object.is_a?(relationship_object_class(repo_object, relationship.name))
          VERIFICATION_PASS
        else
          VERIFICATION_FAIL
        end
      end

      def verify_role(repo_object, role)
        role_obj = Ddr::Auth::Roles::Role.new(type: role.role_type, agent: role.agent, scope: role.role_scope)
        repo_object.roles.include?(role_obj) ? VERIFICATION_PASS : VERIFICATION_FAIL
      end

      def add_attribute(change_set, attribute)
        change_set.add_value(attribute.name, attribute.value)
        change_set
      end

      def clear_attribute(change_set, attribute)
        change_set.send(:"#{attribute.name.to_sym}=", nil)
        change_set
      end

      def add_role(change_set, role)
        auth_role = Ddr::Auth::Roles::Role.new(type: role.role_type, agent: role.agent, scope: role.role_scope)
        change_set.roles += Array(auth_role)
        change_set
      end

      def populate_datastream(change_set, datastream)
        change_set.add_file(datastream[:payload], datastream[:name])
        change_set
      end

      def add_relationship(change_set, relationship)
        change_set.send(:"#{relationship[:name]}_id=", relationship[:object])
        change_set
      end

      def relationship_object_class(resource, relationship_name)
        case relationship_name
        when 'admin_policy', 'attached_to', 'for_collection'
          Ddr::Collection
        when 'parent'
          case resource.class.name
          when 'Ddr::Item'
            Ddr::Collection
          when 'Ddr::Component'
            Ddr::Item
          end
        end
      end
    end
  end
end
