module Ddr::Batch
  class BatchObjectRole < ::ApplicationRecord
    belongs_to :batch_object, inverse_of: :batch_object_roles

    OPERATION_ADD = 'ADD'.freeze # Add the principal and role to the object in the indicated scope
    OPERATIONS = [OPERATION_ADD].freeze

    validates :operation, inclusion: { in: OPERATIONS }
    validate :valid_role, if: :operation_requires_valid_role?
    validate :policy_role_scope_only_for_collections, if: proc { role_scope == Ddr::Auth::Roles::POLICY_SCOPE }

    def operation_requires_valid_role?
      [OPERATION_ADD].include? operation
    end

    def valid_role
      Ddr::Auth::Roles::Role.new(type: role_type, agent:, scope: role_scope)
    rescue Dry::Types::ConstraintError => e
      errors.add(:role, e.message)
    end

    def policy_role_scope_only_for_collections
      return if batch_object.model == 'Ddr::Collection'

      errors.add(:role_scope, 'policy role scope is valid only for Collections')
    end
  end
end
