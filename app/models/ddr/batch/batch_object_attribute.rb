module Ddr::Batch
  class BatchObjectAttribute < ::ApplicationRecord
    belongs_to :batch_object, inverse_of: :batch_object_attributes

    OPERATION_ADD = 'ADD'              # Add the provided value to the attribute
    OPERATION_DELETE = 'DELETE'        # Delete the provided value from the attribute
    OPERATION_CLEAR = 'CLEAR'          # Clear all values from the attribute

    OPERATIONS = [OPERATION_ADD, OPERATION_DELETE, OPERATION_CLEAR].freeze

    VALUE_TYPE_STRING = 'STRING'

    VALUE_TYPES = [VALUE_TYPE_STRING].freeze

    validates :operation, inclusion: { in: OPERATIONS }
    with_options if: :operation_requires_name? do
      validates :name, presence: true
    end
    validate :valid_attribute_name, if: proc { batch_object.model && name }
    with_options if: :operation_requires_value? do
      validates :value, presence: true
      validates :value_type, inclusion: { in: VALUE_TYPES }
    end

    def operation_requires_name?
      [OPERATION_ADD, OPERATION_DELETE, OPERATION_CLEAR].include? operation
    end

    def operation_requires_value?
      [OPERATION_ADD, OPERATION_DELETE].include? operation
    end

    def valid_attribute_name
      errors.add(:name, 'is not valid') unless attribute_name_valid?
    end

    def attribute_name_valid?
      (Ddr::Describable.term_names + Ddr::HasAdminMetadata.term_names).include? name.to_sym
    end
  end
end
