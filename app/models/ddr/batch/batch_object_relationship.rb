module Ddr::Batch
  class BatchObjectRelationship < ::ApplicationRecord
    belongs_to :batch_object, inverse_of: :batch_object_relationships

    RELATIONSHIP_ADMIN_POLICY = 'admin_policy'
    RELATIONSHIP_FOR_COLLECTION = 'for_collection'
    RELATIONSHIP_PARENT = 'parent'
    RELATIONSHIP_ITEM = 'item'
    RELATIONSHIP_COMPONENT = 'component'
    RELATIONSHIP_ATTACHED_TO = 'attached_to'

    RELATIONSHIPS = [RELATIONSHIP_ADMIN_POLICY, RELATIONSHIP_FOR_COLLECTION, RELATIONSHIP_PARENT, RELATIONSHIP_ITEM,
                     RELATIONSHIP_COMPONENT, RELATIONSHIP_ATTACHED_TO].freeze

    OPERATION_ADD = 'ADD'
    OPERATION_UPDATE = 'UPDATE'
    OPERATION_DELETE = 'DELETE'
    OPERATIONS = [OPERATION_ADD, OPERATION_UPDATE, OPERATION_DELETE].freeze

    OBJECT_TYPE_ID = 'ID'

    # Legacy note: There are also records having object_type = 'PID'
    OBJECT_TYPES = [OBJECT_TYPE_ID]
  end
end
