module Ddr::Batch
  class Batch < ::ApplicationRecord
    belongs_to :user, inverse_of: :batches, class_name: '::User'
    has_many :batch_objects, -> { order('id ASC') }, inverse_of: :batch, dependent: :destroy

    OUTCOME_SUCCESS = 'SUCCESS'
    OUTCOME_FAILURE = 'FAILURE'
    OUTCOMES = [OUTCOME_SUCCESS, OUTCOME_FAILURE].freeze

    STATUS_READY = 'READY'
    STATUS_VALIDATING = 'VALIDATING'
    STATUS_INVALID = 'INVALID'
    STATUS_VALIDATED = 'VALIDATED'
    STATUS_QUEUED = 'QUEUED'
    STATUS_PROCESSING = 'PROCESSING'
    STATUS_RUNNING = 'RUNNING'
    STATUS_FINISHED = 'FINISHED'
    STATUS_INTERRUPTED = 'INTERRUPTED'
    STATUS_RESTARTABLE = 'INTERRUPTED - RESTARTABLE'
    STATUS_QUEUED_FOR_DELETION = 'QUEUED FOR DELETION'
    STATUS_DELETING = 'DELETING'

    STATUS_VALUES = [
      STATUS_READY,
      STATUS_VALIDATING,
      STATUS_INVALID,
      STATUS_VALIDATED,
      STATUS_QUEUED,
      STATUS_PROCESSING,
      STATUS_RUNNING,
      STATUS_FINISHED,
      STATUS_INTERRUPTED,
      STATUS_RESTARTABLE,
      STATUS_QUEUED_FOR_DELETION,
      STATUS_DELETING
    ].freeze

    def handled_count
      batch_objects.where(handled: true).count
    end

    def success_count
      batch_objects.where(verified: true).count
    end

    def time_to_complete
      return if start.nil?
      return unless handled_count > 0

      handled = handled_count
      ((Time.now - start.to_time) / handled) * (batch_objects.count - handled)
    end

    def found_resource_ids
      @found_resource_ids ||= {}
    end

    def add_found_resource_id(resource_id, model)
      @found_resource_ids[resource_id] = model
    end

    def pre_assigned_resource_ids
      @pre_assigned_resource_ids ||= collect_pre_assigned_resource_ids
    end

    def collect_pre_assigned_resource_ids
      batch_objects.map { |x| x.resource_id.presence }.compact
    end

    def unhandled_objects?
      batch_objects.any? { |batch_object| !batch_object.handled? }
    end

    def finished?
      status == STATUS_FINISHED
    end

    def deletable?
      [nil,
       Ddr::Batch::Batch::STATUS_READY,
       Ddr::Batch::Batch::STATUS_VALIDATED,
       Ddr::Batch::Batch::STATUS_INVALID].include?(status)
    end

    # Used in reprocessing failed batches, reset 'handled' to false for failed batch objects
    def reset!
      batch_objects.where(verified: 'f', handled: 't').in_batches do |objects|
        objects.update_all(handled: 'f')
      end
    end
  end
end
