module Ddr::Batch
  class BatchObjectMessage < ::ApplicationRecord
    belongs_to :batch_object, inverse_of: :batch_object_messages

    validates :message, presence: true
  end
end
