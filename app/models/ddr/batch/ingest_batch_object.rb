module Ddr::Batch
  class IngestBatchObject < BatchObject
    def local_validations
      errors = []
      errors << "#{@error_prefix} Model required for INGEST operation" unless model
      errors += validate_collection if model == 'Ddr::Collection'
      errors
    end

    def model_datastream_keys
      model.constantize.attachable_files.map(&:to_s)
    end

    def process(user, opts = {})
      if verified
        # Ensure batch object is marked 'handled' so batch can finish
        # (batch will only set status to finished when all batch objects
        # have been 'handled')
        update!(handled: true) unless handled?
      else
        ingest(user, opts)
      end
    end

    def results_message
      if resource_id
        message_level = verified ? Logger::INFO : Logger::WARN
        verification_result = verified ? 'Verified' : 'VERIFICATION FAILURE'
        ProcessingResultsMessage.new(message_level,
                                     "Ingested #{model} #{identifier} into #{resource_id}...#{verification_result}")
      else
        ProcessingResultsMessage.new(Logger::ERROR, "Attempt to ingest #{model} #{identifier} FAILED")
      end
    end

    private

    def validate_collection
      errs = []
      coll_change_set = Ddr::CollectionChangeSet.new(Ddr::Collection.new)
      batch_object_attributes.each { |attr| coll_change_set = add_attribute(coll_change_set, attr) }
      unless coll_change_set.valid?
        coll_change_set.errors.full_messages.each do |v|
          errs << "#{@error_prefix} Ddr::Collection #{v}"
        end
      end
      errs
    end

    def ingest(user, _opts = {})
      repo_object = create_repository_object(user)
      if !repo_object.nil? && !repo_object.new_record?
        verifications = verify_repository_object
        verification_outcome_detail = []
        verified = true
        verifications.each do |key, value|
          verification_outcome_detail << "#{key}...#{value}"
          verified = false if value.eql?(VERIFICATION_FAIL)
        end
        update(verified:)
        Ddr::Events::ValidationEvent.new.tap do |event|
          event.object = repo_object
          event.failure! unless verified
          event.summary = format(EVENT_SUMMARY, label: 'Object ingestion validation', batch_id: id,
                                                identifier:, model:)
          event.detail = verification_outcome_detail.join("\n")
          event.save!
        end
      end
      repo_object
    end

    # If the resource already exists, don't attempt to create it. Possible we are re-running
    # a batch that was interrupted in the middle of processing and the repo object was created.
    # If so, just find it and return it.
    def create_repository_object(user)
      repo_resource_id = resource_id if resource_id.present?

      begin
        repo_object = Ddr.query_service.find_by(id: repo_resource_id) if repo_resource_id
      rescue Valkyrie::Persistence::ObjectNotFoundError => _e
        # Valkyrie throws an error if the object doesn't exist 😱
      end

      return repo_object if repo_object

      begin
        repo_object = model.constantize.new(id: repo_resource_id)
        change_set = Ddr::ResourceChangeSet.change_set_for(repo_object)
        batch_object_attributes.each { |a| change_set = add_attribute(change_set, a) }
        batch_object_datastreams.each { |d| change_set = populate_datastream(change_set, d) }
        batch_object_relationships.each { |r| change_set = add_relationship(change_set, r) }
        batch_object_roles.each { |r| change_set = add_role(change_set, r) }
        change_set.user = user
        change_set.skip_structure_updates = true
        change_set.summary = format(EVENT_SUMMARY, label: 'Object ingestion', batch_id: id,
                                                   identifier:, model:)
        persisted_resource = Ddr::ResourceChangeSetPersister.new.save(change_set:)
        update(resource_id: persisted_resource.id)
      rescue Exception => e
        resource = persisted_resource || repo_object
        logger.fatal("Error in creating repository object #{resource.id} for #{identifier} : #{e}")
        logger.error(e.backtrace.join("\n"))

        # Leaving this code in though it's not clear what circumstances, if any, would cause the resource to be
        # partially saved, as happened with the Fedora backend in DDR Current
        if resource && !resource.new_record?
          begin
            logger.info("Deleting potentially incomplete #{resource.id} due to error in ingest batch processing")
            Ddr::ResourceChangeSetPersister.new.delete(change_set:)
          rescue Exception => e2
            logger.fatal("Error deleting repository object #{resource.id}: #{e2}")
          end
        end

        raise e
      end

      persisted_resource
    end
  end
end
