require 'csv'

module Ddr
  module Batch
    class BatchMessages
      attr_reader :batch, :filters

      def initialize(batch, filters: {})
        @batch = batch
        @filters = filters.dup
      end

      # The message records
      # @return [ActiveRecord::Relation]
      def records
        BatchObjectMessage
          .joins(:batch_object)
          .where(batch_objects: filters.merge(batch_id: batch.id))
          .order(:created_at)
      end

      # CSV headers
      # @return [Array<String>]
      def headers
        BatchObjectMessage.attribute_names
      end

      # CSV output
      # @return [String]
      def to_csv
        CSV.generate do |csv|
          csv << headers

          records.each do |record|
            csv << record.serializable_hash.values_at(*headers)
          end
        end
      end
    end
  end
end
