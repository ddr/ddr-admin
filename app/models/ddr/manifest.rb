require 'csv'

module Ddr
  # Wraps an ingest manifest and provides methods for iterating over it.
  # Manifests are not thread safe.

  # A manifest is valid if:
  #
  #  * all rows have a 'Collection', 'Item', 'Component', 'Target', 'Attachment', or 'File.xxx' value in the 'model' column
  #  * all Components, Targets, and Attachments have paths and checksums unless they already exist, in which case they must have an id
  #  * all Files have paths and all Files except for captions and thumbnails have checksums
  #  * all Components have a parent Item, unless they already exist, in which case they must have an id
  #  * all Files have a parent Component

  # Metadata fields containing a '-' are ignored.

  class Manifest
    attr_reader :filepath, :errors, :current, :current_item, :current_component,
                :current_collection, :index, :file_count, :content_model_stats

    # Used in accommodation of case and spacing errors in column headings
    METADATA_TERMS = Ddr::Describable.term_names +
                     Ddr::Admin.user_editable_admin_metadata_fields +
                     Ddr::Admin.user_editable_item_admin_metadata_fields +
                     Ddr::Admin.user_editable_component_admin_metadata_fields +
                     %i[research_help_contact workflow_state]
    TERMS = %i[model path sha1 id] + METADATA_TERMS
    NORMALIZED_TERMS = TERMS.map(&:downcase).map(&:to_s)

    # https://ruby-doc.org/stdlib-3.1.1/libdoc/csv/rdoc/CSV.html#class-CSV-label-Custom+Field+Converters
    WHITESPACE_CONVERTER = proc { |field| field.nil? ? field : field.strip }

    def initialize(filepath, volumes)
      @filepath = filepath
      @volumes = volumes.transform_keys { |k| Regexp.new(k, Regexp::IGNORECASE) }
      begin
        @csv_table = CSV.table(filepath, converters: WHITESPACE_CONVERTER) # don't convert numeric values to numbers
        @index = -1
        @file_count = 0
        @errors = validate
      rescue CSV::MalformedCSVError => e
        @errors = ["The manifest is not a valid CSV file. #{e.message}"]
        @index = 1 # prevents the manifest from being iterated
        @length = 0
      end
    end

    def ==(other)
      @csv_table == other.instance_eval { @csv_table } &&
        @current_collection == other.instance_eval { @current_collection }
    end

    def ===(other)
      @csv_table === other.instance_eval { @csv_table } &&
        @current_collection === other.instance_eval { @current_collection }
    end

    def valid?
      @errors.empty?
    end

    def headers
      @csv_table.headers
    end

    def length
      @length ||= @csv_table.length
    end

    # A manifest need not supply a collection, so one may be "inserted"
    # ahead of row 0. This does not modify the internal CVS Table
    def prepend(id, title)
      @current_collection = if title.nil?
                              ManifestResource.new(CSV::Row.new(%i[model id], ['Collection', id]), @volumes)
                            else
                              ManifestResource.new(CSV::Row.new(%i[model id title], ['Collection', id, title]),
                                                   @volumes)
                            end
      @index -= 1
    end

    def each
      while current = self.next
        yield(current)
      end
    end

    # Peeks at the first row of the manifest without advancing the index
    def first
      ManifestResource.new(@csv_table[0], @volumes)
    end

    def next
      if @index < length
        # if an externally-defined collection has been prepended, the index will be at -2 to start
        if @index < -1 && @current_collection
          @index += 1
          return @current_collection
        end
        @index += 1
        return false unless @index < @csv_table.length

        @current = ManifestResource.new(@csv_table[@index], @volumes)
        if @current
          case @current.model
          when 'Ddr::Collection'
            @current_collection = @current
            @current_item = nil
            @current_component = nil
          when 'Ddr::Item'
            @current_item = @current
            @current_component = nil
          when 'Ddr::Component'
            @current_component = @current
          end
        end
      else
        @current = nil
      end
      @current
    end

    def current_has_file?
      @csv_table[@index + 1].field(:model).start_with?('File')
    end

    # Returns the parent row of current
    def parent
      return unless @current

      case @current.model
      when 'Ddr::Item'
        @current_collection
      when 'Ddr::Component'
        @current_item
      when 'Ddr::File'
        @current_component
      when 'Ddr::Target'
        @current_collection
      when 'Ddr::Attachment'
        @current_collection
      end
    end

    # Represents a row in the manifest with convenience methods for accessing data
    class ManifestResource
      def initialize(row, volumes)
        @row = row
        @volumes = volumes
        @model_parts = @row.field(:model).split('.')
      end

      def model
        @model ||= "Ddr::#{@model_parts[0]}"
      end

      def file_type
        @model_parts[1]
      end

      # ids may be given in the manifest or generated and supplied during batch creation
      def id
        return @row.field(:id) if @row.field(:id)

        @id
      end

      attr_writer :id

      # Returns a hash of metadata terms and values
      def metadata
        @metadata ||= gather_metadata
      end

      # Rewrites the path to the server location defined in the ingest config
      def path
        @path ||= calculate_path
      end

      # Directly access field values. Multivalued fields will return as arrays
      def method_missing(m, *_args)
        result = @row.field(m.to_sym)
        return nil unless result
        if result.to_s.include?(Ddr::Admin.csv_mv_separator)
          return result.split(Ddr::Admin.csv_mv_separator).map(&:strip)
        end

        result
      end

      # DDK-153: We can't rely on method_missing b/c :format is a Kernel method.
      def format
        result = @row.field(:format)
        return nil unless result
        if result.to_s.include?(Ddr::Admin.csv_mv_separator)
          return result.split(Ddr::Admin.csv_mv_separator).map(&:strip)
        end

        result
      end

      private

      def calculate_path
        return nil if ['Ddr::Collection', 'Ddr::Item'].include? model
        return nil if @row.field(:path).blank?

        result = @row.field(:path).tr('\\', '/')
        volume = @volumes.keys.select do |v|
          v.match?(result)
        end
        return @row.field(:path) if volume.empty?

        result.gsub(volume[0], @volumes[volume[0]])
      end

      def gather_metadata
        result = {}
        Ddr::Manifest::METADATA_TERMS.each do |term|
          if @row.headers.include?(term) && send(term).present?
            result[term] = send(term) == '-' ? nil : send(term)
          end
        end
        result
      end
    end

    private

    def validate
      errors = validate_headers
      unless @csv_table.headers.include?(:model) &&
             (@csv_table.headers.include?(:id) || @csv_table.headers.include?(:path))
        errors << 'The manifest must have a model and id or path.'
        return errors
      end
      index = 1
      collections = 0
      attachments = 0
      targets = 0
      items = 0
      components = 0
      files = 0
      item, component = nil
      @csv_table.each do |row|
        if row.field(:model).blank?
          errors << "Row #{index} does not have a value in the model column."
        else
          # Truncate, e.g. "File.streamable_media" to "File"
          model = if i = row.field(:model).index('.')
                    row.field(:model)[0, i]
                  else
                    row.field(:model)
                  end
        end
        case model
        when 'Collection'
          item = nil
          collections += 1
        when 'Item'
          item = row
          component = nil
          items += 1
        when 'Attachment'
          item = nil
          component = nil
          unless (row.field(:path) && row.field(:sha1)) || row.field(:id)
            errors << "The Attachment in line #{index} must have a path and checksum."
          end
          if row.field(:path) && ::File.exist?(normalize_path(row.field(:path)))
            @file_count += 1
          else
            errors << "The Attachment file #{row.field(:path)} in line #{index} does not exist."
          end
          attachments += 1
        when 'Target'
          item = nil
          component = nil
          unless (row.field(:path) && row.field(:sha1)) || row.field(:id)
            errors << "The Target in line #{index} must have a path and checksum."
          end
          if row.field(:path) && ::File.exist?(normalize_path(row.field(:path)))
            @file_count += 1
          else
            errors << "The Target file #{row.field(:path)} in line #{index} does not exist."
          end
          targets += 1
        when 'Component'
          errors << "The Component in line #{index} must have a parent Item." unless item || row.field(:id)
          unless (row.field(:path) && row.field(:sha1)) || row.field(:id)
            errors << "The Component in line #{index} must have a path and checksum."
          end
          if row.field(:path)
            if ::File.exist?(normalize_path(row.field(:path)))
              @file_count += 1
            else
              errors << "The Component file #{row.field(:path)} in line #{index} does not exist."
            end
          end
          component = row
          components += 1
        when 'File'
          errors << "The File in line #{index} must have a parent Component." unless component
          errors << "The File in line #{index} must have a path." unless row.field(:path)
          unless ['File.caption', 'File.thumbnail'].include?(row.field(:model)) || row.field(:sha1)
            errors << "The File in line #{index} must have a checksum."
          end
          if row.field(:path) && ::File.exist?(normalize_path(row.field(:path)))
            @file_count += 1
          else
            errors << "The File #{row.field(:path)} in line #{index} does not exist."
          end
          files += 1
        else
          errors << "Row #{index} does not have a valid value in the model column."
        end
        index += 1
      end
      @content_model_stats = { collections:, attachments:, targets:,
                               items:, components:, files: }
      errors
    end

    def validate_headers
      invalid_headers = []
      @csv_table.headers[1..-1].each do |header|
        invalid_headers << header unless valid_header?(header)
      end
      return ["Invalid metadata terms in header row: #{invalid_headers.join(', ')}"] unless invalid_headers.empty?

      invalid_headers
    end

    def valid_header?(header)
      NORMALIZED_TERMS.include?(normalize_header(header))
    end

    def normalize_header(header)
      header.to_s.downcase.gsub(/\s+/, '')
    end

    def normalize_path(path)
      rp = path.tr('\\', '/')
      volume = @volumes.keys.select do |v|
        v.match?(rp)
      end
      return path if volume.empty?

      rp.gsub(volume[0], @volumes[volume[0]])
    end

    # Accommodate case and spacing errors in column headings
    CSV::HeaderConverters[:canonicalize] = lambda { |h|
      if idx = NORMALIZED_TERMS.index(h.downcase.gsub(/\s+/, ''))
        TERMS[idx].to_s
      else
        h
      end
    }
  end
end
