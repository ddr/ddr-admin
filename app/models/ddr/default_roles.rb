module Ddr
  class DefaultRoles
    def self.config
      @config ||= load_config.tap do |h|
        h.each_key do |k|
          h[k].map! { |r| Ddr::Auth::Roles::Role.new(r) }
        end
      end
    end

    def self.load_config
      erb = ERB.new(::File.read(Rails.root.join('config/default_roles.yml')))
      YAML.load(erb.result)
    end

    def self.[](key)
      config[key.to_s] || []
    end
  end
end
