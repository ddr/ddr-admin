require 'find'

module Ddr
  class IngestFolder < ApplicationRecord
    include Ingestible

    belongs_to :user, inverse_of: :ingest_folders, class_name: '::User'

    # Lifecycle events
    FINISHED = 'finished.ingest_folder'.freeze

    TARGET_FOLDER = 'targets'.freeze

    FILE_MODEL   = 'Ddr::Component'.freeze
    PARENT_MODEL = 'Ddr::Item'.freeze
    TARGET_MODEL = 'Ddr::Target'.freeze

    validates :collection_id, :sub_path, presence: true
    validates :checksum_type, inclusion: { in: %w[MD5 SHA1 SHA256 SHA384 SHA512] }
    validate :path_must_be_permitted
    validate :path_must_be_readable
    validate :checksum_file_must_be_permitted, if: :checksum_file?
    validate :checksum_file_must_be_readable, if: :checksum_file?

    after_initialize :set_model, if: :new_record?

    ScanResults = Struct.new(:total_count, :file_count, :parent_count, :target_count, :excluded_files)

    def self.checksum_file_dir
      ENV.fetch('INGEST_FOLDER_CHECKSUM_FILE_DIR')
    end

    def self.default_checksum_type
      'SHA1'
    end

    def self.permitted_base_paths
      warn "[DEPRECATION] #{name}.permitted_base_paths is deprecated. Use Ddr::Admin.ingest_base_paths instead."

      Ddr::Admin.ingest_base_paths
    end

    def self.permitted_base_path?(base_path)
      permitted_base_paths.include? base_path
    end

    def target_folder
      ::File.join(full_path, TARGET_FOLDER)
    end

    def full_path
      ::File.join(base_path, sub_path)
    end

    def abbreviated_path
      warn '[DEPRECATED] IngestFolder#abbreviated_path is deprecated; use #full_path instead.'
      full_path
    end

    def checksum_file_location
      if checksum_file.blank?
        default_checksum_file
      elsif checksum_file.start_with?(::File::SEPARATOR)
        checksum_file
      else
        ::File.join(self.class.checksum_file_dir, checksum_file)
      end
    end

    def default_checksum_file
      path_parts = sub_path.split(::File::SEPARATOR)
      base_path_base = ::File.basename(base_path)
      file_name = "#{path_parts.first}-#{base_path_base}-#{checksum_type.downcase}.txt"

      ::File.join(self.class.checksum_file_dir, file_name)
    end

    def collection
      @collection ||= Ddr.query_service.find_by(id: collection_id)
    end

    def collection_title
      collection.title.first
    end

    delegate :admin_policy, to: :collection, prefix: true

    def collection_permissions_attributes
      collection.permissions.collect { |p| p.to_hash }
    end

    def pre_scan
      @parent_hash    = {}
      @total_count    = 0
      @file_count     = 0
      @parent_count   = 0
      @target_count   = 0
      @excluded_files = []

      return unless checksum_file.present?

      @checksum_hash = checksums
    end

    def scan
      scan_files

      errors.add :base, I18n.t('ddr.ingest_folder.no_ingestable_files', path: sub_path) if @file_count == 0

      ScanResults.new(@total_count, @file_count, @parent_count, @target_count, @excluded_files)
    end

    def procezz
      @batch = Ddr::Batch::Batch.create(
        user:,
        name: I18n.t('ddr.ingest_folder.batch_name'),
        description: full_path,
        collection_id:,
        collection_title:
      )

      scan_files(true)

      batch_id = nil

      if @batch.batch_objects.count == 0
        @batch.destroy
      else
        @batch.update(status: Ddr::Batch::Batch::STATUS_READY)
        batch_id = @batch.id
      end

      batch_id
    end

    def file_checksum(path)
      @checksum_hash.fetch(checksum_hash_key(path), nil)
    end

    def checksums
      checksum_file_path = ::File.dirname(checksum_file_location)
      @checksum_file_directory = checksum_file_path.split(::File::SEPARATOR).last
      checksum_hash = {}

      ::File.open(checksum_file_location, 'r') do |file|
        file.each_line do |line|
          sum, path = line.split
          checksum_hash[checksum_hash_key(path)] = sum
        end
      end

      checksum_hash
    end

    def checksum_hash_key(file_path)
      file_path
    end

    def scan_files(create_batch_objects = false)
      pre_scan

      Find.find(full_path) do |path|
        next if ::FileTest.directory?(path)

        @total_count += 1

        if exclude?(path)
          # I guess we only track/report excluded files on the dry run?
          @excluded_files << path unless create_batch_objects

          next
        end

        if target?(path)
          @target_count += 1

        else # not a target file
          @file_count += 1

          file_identifier = extract_identifier_from_filename(path)
          parent_id = parent_identifier(file_identifier)

          unless @parent_hash.key?(parent_id)
            @parent_count += 1
            @parent_hash[parent_id] = nil
          end
        end

        if checksum_file.present? && file_checksum(path).blank?
          errors.add :base,
                     I18n.t('ddr.ingest_folder.checksum_missing',
                            entry: path)
        end

        create_batch_object_for_file(path) if create_batch_objects
      end
    end

    def exclude?(path)
      Ddr::Admin.ingest_exclude_files.any? { |ex| ::File.fnmatch?(ex, path) }
    end

    def create_batch_object_for_parent(parent_identifier)
      parent_id = nil

      # Replaced an index query with database query here
      results = Ddr::API::DatabaseRepository.find_by_metadata({ local_id: parent_identifier }, model: PARENT_MODEL)

      case results.size
      when 0 # parent does not exist
        parent_id = SecureRandom.uuid
        policy_id = collection_admin_policy ? collection_admin_policy.id : collection_id
        obj = Ddr::Batch::IngestBatchObject.create(
          batch: @batch,
          identifier: parent_identifier,
          model: PARENT_MODEL,
          resource_id: parent_id
        )
        Ddr::Batch::BatchObjectAttribute.create(
          batch_object: obj,
          name: 'local_id',
          operation: Ddr::Batch::BatchObjectAttribute::OPERATION_ADD,
          value: parent_identifier,
          value_type: Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING
        )
        Ddr::Batch::BatchObjectRelationship.create(
          batch_object: obj,
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY,
          object: policy_id,
          object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID,
          operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD
        )
        if collection_id
          Ddr::Batch::BatchObjectRelationship.create(
            batch_object: obj,
            name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT,
            object: collection_id,
            object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID,
            operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD
          )
        end

      when 1 # parent exists in repo
        parent_id = results.next.id.to_s

        # We create the batch object as 'verified' so
        # that it will not actually be ingested, since
        # the resource already exists in the repository.
        # This is a hack around the batch ingest processing
        # workflow in which Component processing is
        # dependent on the parent Item.
        Ddr::Batch::IngestBatchObject.find_or_create_by!(
          batch: @batch,
          identifier: parent_identifier,
          model: PARENT_MODEL,
          resource_id: parent_id,
          verified: true
        )

      else
        errors.add(:base, 'Multiple resources found matching local ID "%s"' % parent_identifier)
      end

      parent_id
    end

    def parent_identifier(child_identifier)
      case parent_id_length
      when nil then child_identifier
      when 0 then child_identifier
      else child_identifier[0, parent_id_length]
      end
    end

    def target?(path)
      path.start_with? target_folder
    end

    def create_batch_object_for_file(path)
      file_identifier = extract_identifier_from_filename(path)

      unless target?(path)
        parent_id = parent_identifier(file_identifier)
        parent_resource_id = @parent_hash.fetch(parent_id, nil)

        if parent_resource_id.blank?
          parent_resource_id = create_batch_object_for_parent(parent_id)
          @parent_hash[parent_id] = parent_resource_id
        end
      end

      policy_id = collection_admin_policy ? collection_admin_policy.id : collection_id

      obj = Ddr::Batch::IngestBatchObject.create(
        batch: @batch,
        identifier: file_identifier,
        model: target?(path) ? TARGET_MODEL : FILE_MODEL
      )

      Ddr::Batch::BatchObjectAttribute.create(
        batch_object: obj,
        name: 'local_id',
        operation: Ddr::Batch::BatchObjectAttribute::OPERATION_ADD,
        value: file_identifier,
        value_type: Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING
      )

      Ddr::Batch::BatchObjectDatastream.create(
        batch_object: obj,
        name: 'content',
        operation: Ddr::Batch::BatchObjectDatastream::OPERATION_ADD,
        payload: path,
        payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME,
        checksum: checksum_file.present? ? file_checksum(path) : nil,
        checksum_type: checksum_file.present? ? checksum_type : nil
      )

      Ddr::Batch::BatchObjectRelationship.create(
        batch_object: obj,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY,
        object: policy_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD
      )

      if parent_resource_id
        Ddr::Batch::BatchObjectRelationship.create(
          batch_object: obj,
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT,
          object: parent_resource_id,
          object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID,
          operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD
        )
      end

      if target?(path) && collection_id
        Ddr::Batch::BatchObjectRelationship.create(
          batch_object: obj,
          name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_FOR_COLLECTION,
          object: collection_id,
          object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID,
          operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD
        )
      end

      obj.save # ??? Use save!
    end

    def extract_identifier_from_filename(path)
      ::File.basename path, ::File.extname(path)
    end

    def permitted_base_path?
      self.class.permitted_base_path? base_path
    end

    def path_must_be_permitted
      return if permitted_base_path?

      errors.add :base_path, I18n.t('ddr.ingest_folder.base_path.forbidden', path: base_path)
    end

    def path_must_be_readable
      return if ::File.readable?(full_path)

      errors.add :sub_path, I18n.t('ddr.ingest_folder.not_readable', path: sub_path)
    end

    def checksum_file_must_be_readable
      return if ::File.readable?(checksum_file_location)

      errors.add :checksum_file, I18n.t('ddr.ingest_folder.not_readable', path: checksum_file_location)
    end

    def checksum_file_must_be_permitted
      return unless ::File.absolute_path?(checksum_file) # relative path must be in permitted base path

      unless permitted_path?(checksum_file) ||
             ::File.fnmatch?("#{self.class.checksum_file_dir}/**", checksum_file)
        errors.add :checksum_file, I18n.t('ddr.ingest_folder.forbidden', path: checksum_file)
      end
    end

    def set_model
      self.model = FILE_MODEL
    end
  end
end
