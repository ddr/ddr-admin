module Ddr
  module HasAdminMetadata
    extend ActiveSupport::Concern

    def self.term_names
      Ddr::Metadata.administrative.keys
    end

    included do
      # `access_role' is here for legacy reasons -- i.e., it's not "administrative metadata",
      # but that's where we put it at the time.
      attribute :access_role, Valkyrie::Types::Set.of(Ddr::Auth::Roles::Role)
      alias_method :roles, :access_role
      alias_method :roles=, :access_role=
    end

    def research_help
      Ddr::Contact.call(research_help_contact) if research_help_contact
    end

    # Usually won't be called directly. See `publishable?` on Resource and its derivatives

    def resource_roles
      roles.select(&:in_resource_scope?)
    end

    def policy_roles
      roles.select(&:in_policy_scope?)
    end

    def inherited_roles
      (has_admin_policy? && admin_policy.policy_roles) || []
    end

    def effective_roles(agents = nil)
      Ddr::Auth::EffectiveRoles.call(self, agents)
    end

    def effective_permissions(agents)
      Ddr::Auth::EffectivePermissions.call(self, agents)
    end

    def finding_aid
      return unless ead_id

      FindingAid.new(ead_id)
    end
  end
end
