require 'csv'

module Ddr
  class MetadataFile < ::ApplicationRecord
    belongs_to :user, inverse_of: :metadata_files, class_name: '::User'
    has_attached_file :metadata
    do_not_validate_attachment_file_type :metadata

    # Lifecycle events
    FINISHED = 'finished.metadata_file'.freeze

    validates :metadata, presence: true

    def self.common_model_name
      'MetadataFile'
    end

    def validate_data
      begin
        as_csv_table.headers.each_with_index do |header, idx|
          unless valid_headers.include?(header.to_sym)
            errors.add(:metadata,
                       "#{I18n.t('ddr.metadata_file.error.attribute_name')}: #{header}")
          end
          validate_controlled_values(header, idx) if controlled_value_headers.include?(header.to_sym)
        end
      rescue CSV::MalformedCSVError => e
        errors.add(:metadata, "#{I18n.t('ddr.metadata_file.error.parse_error')}: #{e}")
      end
      errors
    end

    def validate_controlled_values(header, idx)
      valid_values = case header.to_sym
                     when :admin_set
                       Ddr::AdminSet.keys
                     when :research_help_contact
                       Ddr::Contact.keys
                     when :rights
                       Ddr::RightsStatement.keys
                     when :workflow_state
                       Ddr::V3::PublicationService.workflow_state_names
                     end
      as_csv_table.by_col.values_at(idx).each do |value|
        unless value.first.nil? || valid_values.include?(value.first)
          errors.add(:metadata, "#{I18n.t('ddr.metadata_file.error.attribute_value')}: #{header} -> #{value.first}")
        end
      end
    end

    def valid_headers
      %i[id model is_governed_by] +
        Ddr::Describable.term_names +
        Ddr::HasAdminMetadata.term_names +
        non_user_editable_fields
    end

    def user_editable_fields
      Ddr::Describable.term_names + editable_admin_metadata_fields
    end

    def editable_admin_metadata_fields
      Ddr::Admin.user_editable_admin_metadata_fields +
        Ddr::Admin.user_editable_item_admin_metadata_fields +
        Ddr::Admin.user_editable_component_admin_metadata_fields +
        %i[research_help_contact workflow_state]
    end

    # Fields accepted in the header where their values are non-editable, therefore ignored
    def non_user_editable_fields
      %i[original_filename parent_id]
    end

    def controlled_value_headers
      %i[admin_set research_help_contact rights workflow_state]
    end

    def csv_options
      { encoding: 'UTF-8',
        headers: true,
        write_headers: true,
        header_converters: proc { |h| h.strip } }
    end

    def procezz
      @batch = Ddr::Batch::Batch.create(
        user:,
        name: I18n.t('ddr.metadata_file.batch_name'),
        description: metadata_file_name
      )
      # Create batch object for each row in file
      CSV.foreach(metadata.path, **csv_options) do |row|
        # DDK-28 - Skip row if all fields are nil or whitespace only
        next if row.fields.all? { |f| f.nil? || f.match?(/\A\s*\z/) }

        obj = Ddr::Batch::UpdateBatchObject.new(batch: @batch)
        obj.resource_id = row.field('id') if row.headers.include?('id')
        obj.model = row.field('model') if row.headers.include?('model')
        obj.identifier = row.field('local_id') if row.headers.include?('local_id')
        obj.save
        # Create CLEAR operation for each editable field in file
        editable_headers = row.headers.select { |hdr| user_editable_fields.include?(hdr.to_sym) }
        editable_headers.uniq.each do |header|
          next if ignore?(header, row.field(header))

          att = Ddr::Batch::BatchObjectAttribute.new(
            batch_object: obj,
            name: header,
            operation: Ddr::Batch::BatchObjectAttribute::OPERATION_CLEAR
          )
          obj.batch_object_attributes << att
        end

        # Create ADD operation for each editable field in file
        row.headers.each_with_index do |header, idx|
          next unless row.field(header, idx).present?

          parse_field(row.field(header, idx)).each do |value|
            next unless editable_headers.include?(header) && !ignore?(header, value)

            att = Ddr::Batch::BatchObjectAttribute.new(
              batch_object: obj,
              name: header,
              operation: Ddr::Batch::BatchObjectAttribute::OPERATION_ADD,
              value:,
              value_type: Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING
            )
            obj.batch_object_attributes << att
          end
        end
      end

      # Mark batch as ready for processing
      @batch.update(status: Ddr::Batch::Batch::STATUS_READY)

      @batch.id
    end

    def parse_field(value)
      value.split(Ddr::Admin.csv_mv_separator).map(&:strip)
    end

    def as_csv_table
      @csv_table ||= CSV.read(metadata.path, **csv_options)
    end

    private

    # Check whether a particular value should be ignored.
    #  - workflow_state must not be un-set if it is blank. Blank values for workflow_state
    #    are ignored.
    def ignore?(header, value)
      return false unless header == 'workflow_state' &&
                          Ddr::V3::PublicationService.workflow_state_names.exclude?(value)

      true
    end
  end
end
