module Ddr
  class FileUpload
    include ActiveModel::Model
    include Ingestible

    attr_accessor :checksum_path, :folder_path, :results, :user
    attr_reader :basepath, :checksum_file, :collection_id, :ddr_file_name, :subpath

    # Lifecycle events
    FINISHED = 'finished.file_upload'

    EXCLUDE = %w[.DS_Store Thumbs.db]

    Results = Struct.new(:batch, :errors, :inspection_results)

    validates :basepath, :ddr_file_name, :subpath, :user, presence: true
    validates :ddr_file_name, inclusion: { in: %w[caption intermediate_file streamable_media thumbnail] }
    validate :folder_directory_must_be_permitted
    validate :folder_directory_must_exist
    validate :checksum_file_must_be_permitted, if: -> { checksum_file.present? }
    validate :checksum_file_must_exist, if: -> { checksum_file.present? }

    def self.default_basepaths(_ddr_file_name)
      warn "[DEPRECATION] #{name}.default_basepaths is deprecated; use Ddr::Admin.ingest_base_paths instead."

      Ddr::Admin.ingest_base_paths
    end

    def initialize(args)
      @basepath = args['basepath']
      @checksum_file = args['checksum_file']
      @collection_id = args['collection_id']
      @ddr_file_name = args['ddr_file_name']
      @subpath = args['subpath']
      @user = User.find_by_user_key(args['batch_user'])
      @results = Results.new
    end

    def process
      processing_errors = []
      begin
        results.batch = build_batch
      rescue Ddr::Error => e
        processing_errors << e.message
      end
      results.inspection_results = inspection_results
      results.errors = processing_errors
      results
    end

    def build_batch
      builder_args = {
        batch_description: filesystem.root.name,
        batch_name: 'File Upload',
        batch_user: user,
        ddr_file_name:,
        filesystem:
      }
      builder_args.merge!(checksum_file_path: checksum_file) if checksum_file.present?
      builder_args.merge!(collection_id:) if collection_id
      batch_builder = BuildBatchFromFileUpload.new(**builder_args)
      batch_builder.call
    end

    def folder_directory_must_exist
      return if Dir.exist?(folder_path)

      errors.add(:subpath, "#{subpath} does not exist in #{basepath} or is not a directory")
    end

    def folder_directory_must_be_permitted
      return if permitted_path?(folder_path)

      errors.add(:basepath, "#{folder_path} is not a permitted path")
    end

    def checksum_file_must_exist
      return if ::File.exist?(checksum_file)

      errors.add(:checksum_file, "#{checksum_file} does not exist")
    end

    def checksum_file_must_be_permitted
      return if permitted_path?(checksum_file)

      errors.add(:checksum_file, "#{checksum_file} is not a permitted path")
    end

    def inspection_results
      @inspection_results ||= ScanFilesystem.new(folder_path, { exclude: EXCLUDE }).call
    end

    def filesystem
      @filesystem ||= inspection_results.filesystem
    end

    def folder_path
      @folder_path ||= ::File.join(basepath, subpath)
    end
  end
end
