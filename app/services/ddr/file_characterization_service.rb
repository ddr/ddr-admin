module Ddr
  # @deprecated Use Ddr::V3::FitsFileUpdateService.
  class FileCharacterizationService
    class FITSError < Error; end

    attr_reader :resource

    def self.call(resource)
      Ddr::V3::FileUpdateService[:fits_file].call(resource:)
    end

    def initialize(resource)
      @resource = resource
    end

    def path
      @path ||= Ddr.storage_adapter.file_path(resource.content.file_path)
    end

    def call
      if resource.content.present?
        update_resource(run_fits)
      else
        Rails.logger.warn(no_content)
      end
    rescue RuntimeError => e
      raise FITSError, e.message
    end

    def run_fits
      # Send error stream to /dev/null b/c it pollutes XML output (DDR-2469)
      out = IO.popen(['fits.sh', '-i', path], err: '/dev/null') { |io| io.read }

      raise FITSError, 'FITS exited with an error code. See logs for details.' unless $?.success?

      out
    end

    def update_resource(fits_xml)
      Tempfile.open("fits_#{resource.id.id}") do |outfile|
        outfile.binmode
        outfile.write(fits_xml)
        outfile.rewind
        change_set = ResourceChangeSet.change_set_for(resource)
        change_set.add_file(outfile, :fits_file, mime_type: 'application/xml')
        change_set.skip_structure_updates = true # characterizing a file is not structurally relevant
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
      end
    end

    def no_content
      I18n.t 'ddr.resource.warnings.no_content_file',
             model: resource.class.name,
             id: resource.id.id
    end
  end
end
