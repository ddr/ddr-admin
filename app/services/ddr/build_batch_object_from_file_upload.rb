module Ddr
  class BuildBatchObjectFromFileUpload
    attr_reader :batch, :checksum, :file_path, :ddr_file_name, :repo_id

    def initialize(batch:, ddr_file_name:, file_path:, repo_id:, checksum: nil)
      @batch = batch
      @checksum = checksum
      @ddr_file_name = ddr_file_name
      @file_path = file_path
      @repo_id = repo_id
    end

    def call
      create_update_object
    end

    def create_update_object
      update_object = Ddr::Batch::UpdateBatchObject.create(batch:, resource_id: repo_id)
      add_uploaded_datastream(update_object)
      update_object
    end

    def add_uploaded_datastream(batch_object)
      ds_attrs = { name: ddr_file_name,
                   operation: Ddr::Batch::BatchObjectDatastream::OPERATION_ADDUPDATE,
                   payload: file_path,
                   payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME }
      if checksum.present?
        ds_attrs.merge!({ checksum:,
                          checksum_type: Ddr::Files::CHECKSUM_TYPE_SHA1 })
      end
      ds = Ddr::Batch::BatchObjectDatastream.create(ds_attrs)
      batch_object.batch_object_datastreams << ds
    end
  end
end
