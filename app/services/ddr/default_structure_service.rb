require 'htmlentities'

module Ddr
  class DefaultStructureService
    class << self
      include Ddr::Index::Fields

      def default_structure(object)
        document = Ddr::Structure.xml_template
        structure = Ddr::Structure.new(document)
        metshdr = structure.add_metshdr
        structure.add_agent(parent: metshdr, role: Ddr::Structures::Agent::ROLE_CREATOR,
                            name: Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT)
        case object
        when Ddr::Collection
          collection_structure(object, structure)
        when Ddr::Item
          item_structure(object, structure)
        else
          raise ArgumentError, "No default structure for #{object.class}"
        end
      end

      private

      def collection_structure(collection, structure)
        structmap = structure.add_structmap(type: Ddr::Structure::TYPE_DEFAULT)

        # query = Ddr::Index::Query.new do
        #   is_member_of_collection collection.id
        #   sort "#{NESTED_PATH} ASC,#{LOCAL_ID} ASC,#{INGESTION_DATE} ASC,#{RESOURCE_CREATE_DATE} ASC"
        #   fields :id, :nested_path
        # end

        query = Ddr::API::IndexRepository.query(
          q: "parent_id_tsim:id-#{collection.id}",
          fl: %w[id nested_path_ssi],
          sort: "#{NESTED_PATH} ASC,#{LOCAL_ID} ASC,#{INGESTION_DATE} ASC,#{RESOURCE_CREATE_DATE} ASC"
        )

        query.docs.each do |item|
          nest = item[NESTED_PATH]&.split(::File::SEPARATOR) || []
          find_or_create_div(structure, structmap, nest, item.id)
        end

        structure
      end

      def find_or_create_div(structure, parent, nest, repo_id)
        label = nest.shift
        order = parent.elements.count + 1

        if nest.empty?
          div = structure.add_div(parent:, order:)
          structure.add_mptr(parent: div, loctype: 'URN', href: "urn:uuid:#{repo_id}")
        else
          label = HTMLEntities.new.encode(label)
          div = parent.xpath(%(xmlns:div[@LABEL="#{label}"])).first ||
                structure.add_div(parent:, type: 'Directory', label:, order:)
          find_or_create_div(structure, div, nest, repo_id)
        end
      end

      def item_structure(item, structure)
        structmap = structure.add_structmap(type: Ddr::Structure::TYPE_DEFAULT)

        component_structure_types(item).each do |type_term, children|
          div = structure.add_div(parent: structmap, type: type_term)

          children.each_with_index do |child, index|
            sub_div = structure.add_div(parent: div, order: index + 1)
            structure.add_mptr(parent: sub_div, loctype: 'URN', href: "urn:uuid:#{child.id}")
          end
        end

        structure
      end

      def component_structure_types(item)
        type_divs = {}

        query = Ddr::API::IndexRepository.find_children(item.id).sort_by(
          "#{LOCAL_ID} ASC,#{TITLE} ASC,#{ORIGINAL_FILENAME} ASC,#{INGESTION_DATE} ASC,#{RESOURCE_CREATE_DATE} ASC"
        )

        query.docs.each do |child|
          term = type_term(child) || 'Other'

          if type_divs.has_key?(term)
            type_divs[term] << child
          else
            type_divs[term] = [child]
          end
        end

        type_divs
      end

      def type_term(component_doc)
        return if component_doc[Ddr::Index::Fields::MEDIA_TYPE].nil?

        media_type = component_doc[Ddr::Index::Fields::MEDIA_TYPE].first
        Ddr::Structures::ComponentTypeTerm.term(media_type)
      end
    end
  end
end
