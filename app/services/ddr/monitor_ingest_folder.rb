module Ddr
  class MonitorIngestFolder
    class << self
      def call(*)
        event = ActiveSupport::Notifications::Event.new(*)
        ingest_folder = IngestFolder.find(event.payload[:ingest_folder_id])
        batch = Ddr::Batch::Batch.find(event.payload[:batch_id]) if event.payload[:batch_id]
        coll_title = collection_title(ingest_folder.collection_id, batch)
        email_user(ingest_folder.user, coll_title, ingest_folder.abbreviated_path, batch)
      end

      private

      def email_user(user, coll_title, folder_path, batch)
        msg = <<~EOS
          DPC Folder Ingest has created batch ##{batch.id}
          For collection: #{coll_title}
          From folder: #{folder_path}

          To review and process the batch, go to #{batch_url(batch)}
        EOS
        JobMailer.basic(to: user.email,
                        subject: "BATCH CREATED - DPC Folder Ingest - #{coll_title}",
                        message: msg).deliver_now
      end

      def collection_title(collection_id, batch)
        if collection_id.present?
          Ddr.query_service.find_by(id: collection_id).title.first
        elsif batch
          batch_object = batch.batch_objects.where(model: 'Ddr::Collection').first
          titles = batch_object.batch_object_attributes.where(name: 'title')
          titles.empty? ? nil : titles.first.value
        end
      end

      def batch_url(batch)
        Rails.application.routes.url_helpers.ddr_batch_url(batch, host: Ddr::Admin.application_hostname,
                                                                  protocol: 'https')
      end
    end
  end
end
