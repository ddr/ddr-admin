module Ddr
  class ThumbnailsService
    attr_reader :collection

    def self.call(collection)
      new(collection).execute
    end

    def initialize(collection)
      @collection = collection
    end

    def execute
      collection.children.each do |item|
        next if item.has_thumbnail?

        component = item.first_child
        next unless component&.has_thumbnail?

        change_set = ResourceChangeSet.change_set_for(item)
        change_set.add_file(component.thumbnail.file_path, 'thumbnail')
        change_set.skip_structure_updates = true
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
      end
      return if collection.has_thumbnail?

      item = collection.first_child
      return unless item&.has_thumbnail?

      change_set = ResourceChangeSet.change_set_for(collection)
      change_set.add_file(item.thumbnail.file_path, 'thumbnail')
      change_set.skip_structure_updates = true
      Ddr::ResourceChangeSetPersister.new.save(change_set:)
    end
  end
end
