require 'csv'

module Ddr
  module ArchivesSpace
    #
    # This service is intended to function as a non-interactive version of
    # https://github.com/duke-libraries/archivesspace-duke-scripts/blob/master/python/duke_update_archival_object.py
    #
    class CreateDigitalObjects
      FAILURE = 'FAILURE'
      SUCCESS = 'SUCCESS'
      SKIPPED = 'NO ACTION'

      SKIP_REMAINING_ON_ERRORS = [Errno::ECONNREFUSED]

      PERMISSIONS = %w[update_resource_record update_digital_object_record]

      CSV_IN_OPTS = {
        headers: true,
        return_headers: false
      }

      CSV_OUT_OPTS = {
        headers: true,
        write_headers: true
      }

      USE_STATEMENT_MAPPING = {
        'image' => 'image-service',
        'multi_image' => 'image-service',
        'folder' => 'image-service',
        'audio' => 'audio-streaming',
        'video' => 'video-streaming',
        'document' => 'text-service'
      }

      CSV_IN_HEADERS = [
        Ddr::Index::Fields::ID,
        Ddr::Index::Fields::LOCAL_ID,
        Ddr::Index::Fields::ASPACE_ID,
        Ddr::Index::Fields::EAD_ID,
        Ddr::Index::Fields::PERMANENT_ID,
        Ddr::Index::Fields::PERMANENT_URL,
        Ddr::Index::Fields::DISPLAY_FORMAT,
        Ddr::Index::Fields::TITLE
      ]

      CSV_OUT_HEADERS = %i[
        repo_id
        ref_id
        digital_object_id
        digital_object_title
        file_uri
        use_statement
        archival_object_uri
        digital_object_uri
        outcome
        published
        user
        message
      ]

      delegate :logger, to: :Rails

      def self.authorized?(user)
        Client.authorized?(user, PERMISSIONS)
      end

      def self.call(csv, **kw)
        new(csv, **kw).call
      end

      attr_reader :csv_in, :publish, :user, :debug

      def initialize(csv, user: nil, publish: false, debug: false)
        csv_in = csv.respond_to?(:read) ? csv.read : csv
        @csv_in = CSV.parse(csv_in.to_s, **CSV_IN_OPTS)
        raise "Input CSV missing one or more required headers: #{CSV_IN_HEADERS}" unless CSV_IN_HEADERS.all? do |h|
                                                                                           @csv_in.headers.include?(h)
                                                                                         end

        @user = user
        @publish = publish
        @debug = debug
      end

      def use_statement_for(value)
        USE_STATEMENT_MAPPING[value]
      end

      def call
        client = Client.new
        client.become(user) if user
        client.http do |conn|
          conn.authorize!(PERMISSIONS)

          skip_remaining = false

          CSV.generate(**CSV_OUT_OPTS) do |csv_out|
            csv_out << CSV_OUT_HEADERS

            csv_in.each do |row|
              outcome, published, archival_object_uri, digital_object_uri,
              digital_object_id, file_uri, digital_object_title, use_statement, message = nil

              repo_id, ref_id, ead_id = row.values_at(Ddr::Index::Fields::ID, Ddr::Index::Fields::ASPACE_ID,
                                                      Ddr::Index::Fields::EAD_ID)

              if skip_remaining
                outcome = SKIPPED

              elsif !ref_id
                message = "No ASpace ref id for repository object #{repo_id} - skipping digital object creation."
                logger.debug('ASPACE') { message }
                outcome = SKIPPED

              else
                begin
                  archival_object      = conn.find_ao(ref_id, ead_id)
                  archival_object_uri  = archival_object['uri']
                  digital_object_id    = row[Ddr::Index::Fields::PERMANENT_ID]
                  digital_object_title = row[Ddr::Index::Fields::TITLE] || archival_object['display_string']
                  file_uri             = row[Ddr::Index::Fields::PERMANENT_URL]
                  use_statement        = use_statement_for(row[Ddr::Index::Fields::DISPLAY_FORMAT])

                  unless debug
                    data = {
                      title: digital_object_title,
                      digital_object_id:,
                      file_versions: [
                        { file_uri:,
                          use_statement: }
                      ]
                    }

                    digital_object_uri = conn.post('/repositories/2/digital_objects', data.to_json, json_header)['uri']
                    logger.info('ASPACE') { "Digital object created: #{digital_object_uri}." }

                    # Publish DO, if required
                    if publish
                      conn.post("#{digital_object_uri}/publish", nil)
                      logger.info('ASPACE') { "Digital object #{digital_object_uri} published." }
                      published = true
                    else
                      published = false
                    end

                    # Link DO to AO
                    digital_object_instance = {
                      instance_type: 'digital_object',
                      digital_object: { ref: digital_object_uri }
                    }
                    archival_object['instances'] << digital_object_instance

                    conn.post(archival_object_uri, archival_object.to_json, json_header)
                    logger.info('ASPACE') do
                      "Digital object #{digital_object_uri} linked to archival object #{archival_object_uri}."
                    end
                  end # unless debug

                  outcome = SUCCESS
                rescue Client::Error => e
                  message = e.message
                  logger.error('ASPACE') { message }
                  outcome = FAILURE
                rescue *SKIP_REMAINING_ON_ERRORS => e
                  message = "Skipping remaining records due to error: #{e.message}"
                  logger.error('ASPACE') { message }
                  outcome = SKIPPED
                  skip_remaining = true
                end # begin
              end # if

              csv_out << {
                repo_id:,
                ref_id:,
                digital_object_id:,
                digital_object_title:,
                file_uri:,
                use_statement:,
                archival_object_uri:,
                digital_object_uri:,
                outcome:,
                published:,
                user: conn.user.username,
                message:
              }
            end # csv_in.each
          end # client.http
        end # CSV.generate
      end

      def json_header
        { 'Content-Type' => 'application/json' }
      end
    end
  end
end
