module Ddr
  module ArchivesSpace
    class Client
      class Error < ::StandardError; end
      class Unauthorized < Error; end
      class AuthenticationError < Error; end

      class_attribute :user, :password, :backend_url
      self.user        = ENV['ASPACE_USER'] || Rails.application.credentials.aspace_user
      self.password    = ENV['ASPACE_PASSWORD'] || Rails.application.credentials.aspace_password
      self.backend_url = ENV['ASPACE_BACKEND_URL'] || Rails.application.credentials.aspace_backend_url

      delegate :logger, to: :Rails

      attr_reader :backend_uri
      attr_accessor :original_auth, :became_auth

      class << self
        delegate :http, :get, :post, :become, :authorized?, to: :new
      end

      def initialize
        @backend_uri = URI(backend_url)
        @original_auth = nil
        @became_auth = nil
        authenticate!
      end

      def auth
        became_auth || original_auth
      end

      def become(other)
        username = other.respond_to?(:aspace_username) ? other.aspace_username : other.to_s
        self.became_auth = post("/users/#{username}/become-user", nil)
        logger.debug('ASPACE') { "Became user '#{username}'." }
        return unless block_given?

        # yields to block, "unbecomes" user, and returns value of block
        yield(self).tap { unbecome }
      end

      def unbecome
        previous_user = current_user
        self.became_auth = nil
        logger.debug('ASPACE') { "User returned to '#{current_user}' (previously '#{previous_user}')" }
      end

      def current_user
        ArchivesSpace::User.new auth['user']
      end

      def get(*)
        http { |conn| conn.get(*) }
      end

      def post(*)
        http { |conn| conn.post(*) }
      end

      def authenticated?
        !!original_auth
      end

      def authenticate!
        Net::HTTP.start(backend_uri.host, backend_uri.port, use_ssl: backend_uri.scheme == 'https') do |conn|
          path = format('%s/users/%s/login', backend_uri.path, user)
          response = conn.post(path, URI.encode_www_form(password:))
          raise AuthenticationError, response.body unless response.is_a?(Net::HTTPSuccess)

          self.original_auth = JSON.parse(response.body)
          logger.debug('ASPACE') { "login: #{original_auth}" }
        end
      end

      def http
        Net::HTTP.start(backend_uri.host, backend_uri.port, use_ssl: backend_uri.scheme == 'https') do |conn|
          yield Http.new(conn, auth, backend_uri.path)
        end
      end

      def authorized?(username, permissions)
        become(username) do
          http { |conn| conn.authorized?(permissions) }
        end
      rescue Error => e
        raise unless /404/.match?(e.message) # i.e., user not found

        false
      end

      class Http
        attr_reader :conn, :auth, :base_path

        delegate :logger, to: :Rails

        def initialize(conn, auth, base_path)
          @conn = conn
          @auth = auth
          @base_path = base_path
        end

        def get(path, initheader = {})
          logger.debug('ASPACE') { "GET #{path} (headers: #{initheader}" }
          headers = initheader.merge(session_header)
          response = conn.get(base_path + path, headers)
          handle_response(response)
        end

        def post(path, data = nil, initheader = {})
          logger.debug('ASPACE') { "POST #{path} (data: #{data}, headers: #{initheader}" }
          headers = initheader.merge(session_header)
          response = conn.post(base_path + path, data, headers)
          handle_response(response)
        end

        def user
          @user ||= ArchivesSpace::User.new(auth['user'])
        end

        def authorize!(perms)
          return if authorized?(perms)

          raise Client::Unauthorized, "ASpace requires permission(s): #{perms}"
        end

        def authorized?(perms)
          logger.debug('ASPACE') { "Checking whether user #{user} has permissions: #{perms}" }
          Array(perms).all? { |p| user.permissions.include?(p) }
        end

        def find_resource_by_ead_id(ead_id)
          query = { 'query' => { 'field' => 'ead_id', 'value' => ead_id, 'jsonmodel_type' => 'field_query', 'negated' => false,
                                 'literal' => false } }
          params = URI.encode_www_form('page' => '1', 'aq' => query.to_json)
          results = get("/repositories/2/search?#{params}")
          raise Client::Error, "No resource found for EAD ID: #{ead_id}" if results['total_hits'] == 0

          result = results['results'].first
          get(result['id'])
        end

        def find_ao(ref_id, ead_id = nil)
          params = URI.encode_www_form('ref_id[]' => ref_id)
          results = get("/repositories/2/find_by_id/archival_objects?#{params}")['archival_objects']
          case results.length
          when 0
            raise Client::Error, "Archival object not found for ref id #{ref_id}."
          when 1
            get(results.first['ref'])
          else
            unless ead_id
              raise Client::Error,
                    "Multiple archival objects found for ref id #{ref_id} and EAD ID not provided."
            end
            resource = find_resource_by_ead_id(ead_id)
            results.each do |result|
              ao = get(result['ref'])
              return ao if ao['resource']['ref'] == resource['uri']
            end
            raise Client::Error,
                  "Multiple archival objects found for ref id #{ref_id} and none match EAD ID #{ead_id}."
          end
        end

        private

        def session
          auth['session']
        end

        def handle_response(response)
          logger.debug('ASPACE') { "Response: #{response.body}" }

          return JSON.parse(response.body) if response.is_a?(Net::HTTPSuccess)

          raise Client::Error, "#{response.code} #{response.message}"
        end

        def session_header
          { 'X-ArchivesSpace-Session' => session }
        end
      end
    end
  end
end
