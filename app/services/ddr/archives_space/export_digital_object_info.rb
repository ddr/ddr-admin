module Ddr
  module ArchivesSpace
    class ExportDigitalObjectInfo
      FIELDS = [
        Ddr::Index::Fields::ID,
        Ddr::Index::Fields::LOCAL_ID,
        Ddr::Index::Fields::ASPACE_ID,
        Ddr::Index::Fields::EAD_ID,
        Ddr::Index::Fields::PERMANENT_ID,
        Ddr::Index::Fields::PERMANENT_URL,
        Ddr::Index::Fields::DISPLAY_FORMAT,
        Ddr::Index::Fields::TITLE
      ]

      # @return [String]
      def self.call(collection_id)
        Ddr::API::IndexRepository.query(fl: FIELDS)
                                 .with(:parent_id_filter, collection_id)
                                 .sort_by('local_id_ssi asc,created_at_dtsi asc')
                                 .csv
      end
    end
  end
end
