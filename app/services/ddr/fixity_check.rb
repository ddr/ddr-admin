module Ddr
  class FixityCheck
    FIXITY_CHECK = 'fixity_check.events.ddr'

    # Return result of fixity check - wrapped by a notifier
    def self.call(repo_object)
      ActiveSupport::Notifications.instrument(FIXITY_CHECK) do |payload|
        payload[:result] = _execute(repo_object)
      end
    end

    # Return result of fixity check
    def self._execute(object)
      Result.new(resource_id: object.id, results: {}, success: true, checked_at: Time.now.utc).tap do |r|
        object.attached_files_having_content.each do |file_id|
          file_result = check_file(object.send(:"#{file_id}"))
          r.success &&= file_result
          r.results[file_id] = file_result
        end
      end
    end

    def self.check_file(repo_file)
      repo_file.stored_checksums_valid?
    end

    Result = Struct.new(:resource_id, :success, :results, :checked_at, keyword_init: true)
  end
end
