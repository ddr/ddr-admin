module Ddr
  class ModelStandardIngestContent
    attr_reader :node, :intermediate_files_name, :targets_name

    def initialize(node, intermediate_files_name = nil, targets_name = nil)
      @node = node
      @intermediate_files_name = intermediate_files_name
      @targets_name = targets_name
    end

    def call
      raise Ddr::Batch::Error, "Node #{node.name} too deep for standard ingest" if node.node_depth > 2

      if node.node_depth == 2 && node.has_children?
        raise Ddr::Batch::Error,
              "Deepest permitted node #{node.name} has children"
      end

      if node.is_root?
        'Ddr::Collection'
      elsif node.node_depth == 1
        if [intermediate_files_name, targets_name].include?(node.name)
          nil
        else
          'Ddr::Item'
        end
      elsif node.node_depth == 2
        if node.parent.name == intermediate_files_name
          nil
        elsif node.parent.name == targets_name
          'Ddr::Target'
        else
          'Ddr::Component'
        end
      end
    end
  end
end
