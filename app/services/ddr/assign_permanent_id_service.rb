module Ddr
  class AssignPermanentIdService
    class AssignPermanentIdError < Ddr::Error; end

    def self.call(resource_ids)
      resource_ids.each do |resource_id|
        resource = Ddr.query_service.find_by(id: resource_id)
        PermanentId.assign!(resource_id, nil, skip_structure_updates: true) unless resource.permanent_id
      rescue StandardError => e
        message = "Error in assigning permanent_id for #{resource_id}, #{e.message}"
        Rails.logger.error(message)
      end
    rescue StandardError => e
      message = "Skipping remaining records due to error: #{e.message}"
      Rails.logger.error(message)
    end
  end
end
