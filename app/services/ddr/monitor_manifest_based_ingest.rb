module Ddr
  class MonitorManifestBasedIngest
    class << self
      def call(*)
        event = ActiveSupport::Notifications::Event.new(*)
        user = User.find_by_user_key(event.payload[:user_key])
        collection_id = event.payload[:collection_id]
        file_count = event.payload[:file_count]
        model_stats = event.payload[:model_stats]
        errors = event.payload[:errors]
        batches = Array(Ddr::Batch::Batch.find(event.payload[:batch_ids])) if event.payload[:batch_ids]
        coll_title = collection_title(collection_id, batches&.first)
        if errors.present?
          email_errors(user, coll_title, errors)
        else
          email_success(user, coll_title, file_count, model_stats, batches)
        end
      end

      private

      def email_success(user, coll_title, file_count, model_stats, batches)
        msg = <<~EOS
          Manifest Based Ingest has created batch#{batches.length > 1 ? 'es' : ''} ##{batches.map { |b| b.id }.join(', #')}
          For collection: #{coll_title}
          Files found: #{file_count}
          Object model stats
            Collection: #{model_stats.fetch(:collections, 0)}
                  Item: #{model_stats.fetch(:items, 0)}
             Component: #{model_stats.fetch(:components, 0)}
                Target: #{model_stats.fetch(:targets, 0)}

          To review and process the batch, go to #{batches.map { |b| batch_url(b) }.join(', ')}
        EOS
        JobMailer.basic(to: user.email,
                        subject: "BATCH CREATED - Manifest Based Ingest Job - #{coll_title}",
                        message: msg).deliver_now
      end

      def email_errors(user, coll_title, errors)
        msg = <<~EOS
          ERRORS in Manifest Based Ingest
          For collection: #{coll_title}

          ERRORS:
          - #{errors.join("\n- ")}
        EOS
        JobMailer.basic(to: user.email,
                        subject: "ERRORS - Manifest Based Ingest Job - #{coll_title}",
                        message: msg).deliver_now
      end

      def collection_title(collection_id, batch)
        if collection_id.present?
          Ddr.query_service.find_by(id: collection_id).title.first
        elsif batch
          batch_object = batch.batch_objects.where(model: 'Ddr::Collection').first
          titles = batch_object.batch_object_attributes.where(name: 'title')
          titles.empty? ? nil : titles.first.value
        end
      end

      def batch_url(batch)
        Rails.application.routes.url_helpers.ddr_batch_url(batch, host: Ddr::Admin.application_hostname,
                                                                  protocol: 'https')
      end
    end
  end
end
