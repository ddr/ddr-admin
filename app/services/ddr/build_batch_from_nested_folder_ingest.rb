module Ddr
  class BuildBatchFromNestedFolderIngest
    attr_reader :user, :filesystem, :content_modeler, :checksum_provider, :metadata_provider, :admin_set,
                :collection_title, :batch_name, :batch_description
    attr_accessor :batch, :collection_repo_id, :nested_path_base

    def initialize(user:, filesystem:, content_modeler:, checksum_provider:, metadata_provider: nil, admin_set: nil,
                   collection_repo_id: nil, collection_title: nil, batch_name: nil, batch_description: nil)
      @user = user
      @filesystem = filesystem
      @content_modeler = content_modeler
      @checksum_provider = checksum_provider
      @metadata_provider = metadata_provider
      @admin_set = admin_set
      @collection_repo_id = collection_repo_id
      @collection_title = collection_title
      @batch_name = batch_name
      @batch_description = batch_description
    end

    def call
      @batch = create_batch
      traverse_filesystem
      batch.update(status: Ddr::Batch::Batch::STATUS_READY,
                   collection_id: collection_repo_id,
                   collection_title: collection_title || collection_title_lookup)
      batch
    end

    private

    def collection_title_lookup
      collection_batch_objects = batch.batch_objects.where(model: 'Ddr::Collection')
      if collection_batch_objects.present?
        collection_batch_object = collection_batch_objects.first
        titles = collection_batch_object.batch_object_attributes.where(name: 'title')
        titles.empty? ? nil : titles.first.value
      else
        Ddr.query_service.find_by(id: collection_repo_id).title.first
      end
    rescue Valkyrie::Persistence::ObjectNotFoundError
      nil
    end

    def create_batch
      Ddr::Batch::Batch.create(user:, name: batch_name, description: batch_description)
    end

    def traverse_filesystem
      filesystem.each do |node|
        node.content ||= {}
        object_model = content_modeler.new(node).call
        case object_model
        when 'Ddr::Collection'
          collection_repo_id.present? ? node.content[:resource_id] = collection_repo_id : create_collection(node)
        when 'Ddr::Component'
          resource_id = create_item(node)
          create_component(node, resource_id)
        end
      end
    end

    def create_collection(node)
      resource_id = assign_resource_id(node)
      self.collection_repo_id = resource_id
      batch_object = Ddr::Batch::IngestBatchObject.create(batch:, model: 'Ddr::Collection',
                                                          resource_id:)
      add_attribute(batch_object, 'admin_set', admin_set)
      add_metadata(batch_object, nil) if metadata_provider.present?
      if batch_object.batch_object_attributes.where(name: 'title').empty?
        add_attribute(batch_object, 'title',
                      collection_title)
      end
      add_role(batch_object)
    end

    def create_item(node)
      resource_id = assign_resource_id(node)
      batch_object = Ddr::Batch::IngestBatchObject.create(batch:, model: 'Ddr::Item', resource_id:)
      add_admin_policy_relationship(batch_object)
      add_parent_relationship(batch_object, collection_repo_id)
      nested_path = ::File.join(nested_path_base, Filesystem.path_to_node(node, 'relative'))
      add_attribute(batch_object, 'nested_path', nested_path)
      add_metadata(batch_object, Filesystem.path_to_node(node)) if metadata_provider.present?
      resource_id
    end

    def create_component(node, item_id)
      batch_object = Ddr::Batch::IngestBatchObject.create(batch:, model: 'Ddr::Component')
      add_admin_policy_relationship(batch_object)
      add_parent_relationship(batch_object, item_id)
      add_content_datastream(batch_object, node)
    end

    def assign_resource_id(node)
      node.content ||= {}
      node.content[:resource_id] = Valkyrie::ID.new(SecureRandom.uuid)
    end

    def add_metadata(batch_object, file_path)
      metadata = metadata_provider.metadata(file_path)
      metadata.each do |key, value|
        Array(value).each do |v|
          add_attribute(batch_object, key, v)
        end
      end
    end

    def add_admin_policy_relationship(batch_object)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: collection_repo_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_parent_relationship(batch_object, parent_id)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: parent_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_role(batch_object)
      Ddr::Batch::BatchObjectRole.create(
        batch_object:,
        operation: Ddr::Batch::BatchObjectRole::OPERATION_ADD,
        agent: user.user_key,
        role_type: Ddr::Auth::Roles::CURATOR.title,
        role_scope: Ddr::Auth::Roles::POLICY_SCOPE
      )
    end

    def add_attribute(batch_object, term, value)
      Ddr::Batch::BatchObjectAttribute.create(
        batch_object:,
        name: term,
        operation: Ddr::Batch::BatchObjectAttribute::OPERATION_ADD,
        value:,
        value_type: Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING
      )
    end

    def add_content_datastream(batch_object, node)
      filepath = Filesystem.path_to_node(node)
      ds = Ddr::Batch::BatchObjectDatastream.create(
        name: :content,
        operation: Ddr::Batch::BatchObjectDatastream::OPERATION_ADD,
        payload: filepath,
        payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME,
        checksum: checksum_provider.checksum(filepath),
        checksum_type: Ddr::Files::CHECKSUM_TYPE_SHA1
      )
      batch_object.batch_object_datastreams << ds
    end

    def nested_path_base
      @nested_path_base ||= filesystem.root.name.split(::File::SEPARATOR).last
    end
  end
end
