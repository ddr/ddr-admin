module Ddr
  class BatchFileCharacterization
    class << self
      def default_limit
        @default_limit || 1000
      end

      def call(limit = nil)
        new(limit).call
      end
    end

    attr_reader :limit

    def initialize(limit = nil)
      @limit = (limit || self.class.default_limit).to_i
    end

    # @return [Fixnum] the number of file characterization jobs queued
    def call
      queued = 0

      Ddr::API::DatabaseRepository.run_query(query, limit).each do |resource|
        Ddr::V3::FileUpdateService[:fits_file].call(resource:, async: true)

        queued += 1
      end

      queued
    end

    private

    def query
      <<-SQL
         SELECT * FROM orm_resources
         WHERE internal_resource IN ('Ddr::Component', 'Ddr::Attachment', 'Ddr::Target')
         AND metadata -> 'fits_file' IS NULL
         ORDER BY created_at ASC
         LIMIT ?
      SQL
    end
  end
end
