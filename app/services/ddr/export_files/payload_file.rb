require 'digest'

module Ddr
  module ExportFiles
    class PayloadFile
      attr_reader :resource, :ddr_file_type

      delegate :sha1, :content, :original_filename, to: :ddr_file

      def initialize(resource, ddr_file_type)
        @resource = resource
        @ddr_file_type = ddr_file_type
      end

      def ddr_file
        @ddr_file ||= resource.send(ddr_file_type)
      end

      alias file ddr_file

      def to_s
        "resource #{repo_id.id}, file #{file_id}"
      end

      def file_id
        ddr_file_type
      end

      def repo_id
        resource.id
      end

      def source_path
        ddr_file.file_path
      end

      def parent
        return unless resource.respond_to?(:parent)

        resource.parent
      end

      def default_filename
        # datastream.default_file_name
        "#{resource.id.id}_#{ddr_file_type}.#{ddr_file.default_file_extension}"
      end

      def original_file?
        file_id == :content
      end
    end
  end
end
