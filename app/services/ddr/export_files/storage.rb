require 'fileutils'
require 'securerandom'

module Ddr
  module ExportFiles
    class Storage
      SUBPATH_FORMAT = '%Y%m%d-%H%M%S-%L'
      DISALLOWED_CHARS = /[^\w.-]/

      def self.sanitize_path(path)
        path.gsub(DISALLOWED_CHARS, '_')
      end

      def self.generate_subpath
        Time.now.utc.strftime(SUBPATH_FORMAT)
      end

      def self.store
        Ddr::Admin.export_files_store
      end

      def self.call(basename)
        new(basename).call
      end

      attr_reader :path

      def initialize(basename)
        @path = ::File.join(ExportFiles::Storage.store,
                            ExportFiles::Storage.sanitize_path(basename))

        return unless ::File.exist?(@path)

        @path += '-%s' % SecureRandom.alphanumeric(8)
      end

      def call
        create! unless created?
        self
      end

      def create!
        FileUtils.mkdir_p(path)
      end

      def created?
        ::File.directory?(path)
      end
    end
  end
end
