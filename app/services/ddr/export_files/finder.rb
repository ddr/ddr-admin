module Ddr
  module ExportFiles
    class Finder
      attr_reader :identifiers, :ability, :files

      def initialize(identifiers, ability: nil, files: [:content])
        @identifiers = identifiers
        @ability = ability
        @files = files
      end

      def resources
        @resources ||= Ddr::API::IndexRepository
                       .find_by_identifiers(identifiers)
                       .with_effective_download_filter(ability)
      end

      def repo_ids
        @repo_ids ||= resources.map(&:id_s)
      end

      def content_ids
        @content_ids ||= results.map(&:id_s)
      end

      def not_found
        @not_found ||= identifiers - repo_ids
      end

      def results
        @results ||= resources.map do |resource|
          case resource
          when Ddr::Item
            Ddr::API::ResourceRepository.children(resource.id).to_a
          when Ddr::Collection
            Ddr::API::ResourceRepository.components(resource.id).to_a
          else
            resource
          end
        end.flatten
      end

      def num_files
        results.map { |resource| (files & resource.attached_files_having_content).length }.reduce(:+)
      end

      def total_content_size
        results.map do |resource|
          resource_files = files & resource.attached_files_having_content
          resource_size = resource_files.map { |file| resource[file].file_size }.reduce(:+)
          resource_size.nil? ? 0 : resource_size
        end.reduce(:+)
      end
    end
  end
end
