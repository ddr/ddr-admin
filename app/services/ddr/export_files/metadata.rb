require 'csv'

module Ddr
  module ExportFiles
    class Metadata
      HEADERS = %i[repo_id object_type parent_id permanent_id local_id content_type original_filename]

      attr_reader :package

      delegate :finder, to: :package

      def initialize(package)
        @package = package
      end

      def csv
        CSV.generate('', headers: HEADERS, write_headers: true) do |rows|
          finder.results.each do |result|
            rows << [result.id,
                     result.common_model_name,
                     result.parent_id,
                     result.permanent_id,
                     result.local_id,
                     result.content_type,
                     result.original_filename]
          end
        end
      end
    end
  end
end
