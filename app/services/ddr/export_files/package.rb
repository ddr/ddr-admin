require 'bagit'
require 'uri'

module Ddr
  module ExportFiles
    class Package
      include ActiveModel::Validations

      METADATA_FILENAME = 'METADATA.csv'

      attr_reader :finder, :basename, :files, :id

      delegate :bag_files, :data_dir, :add_file, to: :bag
      delegate :path, to: :storage
      delegate :repo_ids, :content_ids, :identifiers, :not_found, :results,
               to: :finder
      delegate :logger, to: :Rails

      validates_presence_of :identifiers, :basename
      validates :expected_payload_size, numericality: { less_than: Ddr::Admin.export_files_max_payload_size }
      validates_presence_of :results, message: 'payload is empty (identifiers not found or access denied)'

      def self.base_url
        if Ddr::Admin.application_hostname.present?
          'https://' + Ddr::Admin.application_hostname + Ddr::Admin.export_files_base_url
        else
          Ddr::Admin.export_files_base_url
        end
      end

      def self.call(**args)
        new(**args).tap do |pkg|
          pkg.export!
        end
      end

      def initialize(identifiers: [], ability: nil, basename: nil, files: ['content'])
        @id = SecureRandom.uuid
        @basename = basename.to_s.strip
        @files = files
        @filetypes = files.map { |f| f.to_sym }
        @finder = Finder.new(identifiers, ability:, files: @filetypes)
      end

      def export!
        logger.debug("ExportFiles #{id}") { 'Export started' }

        validate!
        logger.debug("ExportFiles #{id}") { 'Package validated' }

        logger.debug("ExportFiles #{id}") { 'Adding payload files ...' }
        add_payload_files
        logger.debug("ExportFiles #{id}") { 'Payload files added' }

        logger.debug("ExportFiles #{id}") { 'Adding metadata file ...' }
        add_metadata
        logger.debug("ExportFiles #{id}") { 'Metadata file added' }

        add_manifest

        logger.debug("ExportFiles #{id}") { 'Export complete' }
      end

      def storage
        @storage ||= ExportFiles::Storage.call(basename)
      end

      def bag
        @bag ||= BagIt::Bag.new(path)
      end

      def metadata
        @metadata ||= Metadata.new(self)
      end

      # Map of paths to SHA1 digests
      def checksums
        @checksums ||= {}
      end

      def add_manifest
        logger.debug("ExportFiles #{id}") { 'Adding manifest to bag ...' }
        bag.manifest!(algo: 'sha1')
        logger.debug("ExportFiles #{id}") { 'Manifest added to bag' }

        logger.debug("ExportFiles #{id}") { 'Verifying checksums ...' }
        verify_checksums
        logger.debug("ExportFiles #{id}") { 'Checksums verified' }
      end

      def url
        self.class.base_url + relative_path
      end

      def relative_path
        path.sub(ExportFiles::Storage.store, '')
      end

      def payload_size
        bag_files.map { |f| ::File.size(f) }.reduce(:+)
      end

      def add_metadata
        add_file(METADATA_FILENAME) { |io| io.write(metadata.csv) }
      end

      def add_payload_files
        payload_files.each { |payload_file| add_payload_file(payload_file) }
      end

      def add_payload_file(payload_file)
        logger.debug("ExportFiles #{id}") { "Adding payload file #{payload_file} ..." }

        # path relative to data dir
        destination_path = PayloadFilePackager.call(self, payload_file)

        logger.debug("ExportFiles #{id}") { "Payload file added: #{payload_file}" }

        unless sha1 = payload_file.sha1
          raise Ddr::Error, "Original SHA1 checksum not found for payload file: #{payload_file}"
        end

        # Re-add data dir here to match manifest path
        path = ::File.join('data', destination_path)
        checksums[path] = sha1
      end

      def expected_payload_size
        finder.total_content_size
      end

      def expected_num_files
        finder.num_files
      end

      def verify_checksums
        ::File.readlines(bag.manifest_file('sha1'), chomp: true) do |line|
          manifest_sha1, path = line.split(' ', 2)

          begin
            original_sha1 = checksums.fetch(path)

            unless original_sha1 == manifest_sha1
              raise Ddr::ChecksumInvalid,
                    "Path: #{path}; Original SHA1: #{original_sha1}; manifest SHA1: #{manifest_sha1}"
            end

            logger.debug("ExportFiles #{id}") { "Verified SHA1 digest: #{manifest_sha1} #{path}" }
          rescue KeyError
            raise Ddr::Error, "Original SHA1 checksum missing for file at path #{path.inspect}"
          end
        end
      end

      def payload_files
        Enumerator.new do |e|
          finder.results.each do |resource|
            (@filetypes & resource.attached_files_having_content).each do |f|
              e << PayloadFile.new(resource, f)
            end
          end
        end
      end
    end
  end
end
