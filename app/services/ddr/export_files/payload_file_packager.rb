module Ddr
  module ExportFiles
    class PayloadFilePackager
      PAYLOAD_PATH = 'objects'

      attr_reader :package, :payload_file

      delegate :source_path, :content, :parent, :original_file?,
               :original_filename, :default_filename,
               to: :payload_file
      delegate :add_file, :data_dir, to: :package

      def self.call(*)
        new(*).call
      end

      def initialize(package, payload_file)
        @package = package
        @payload_file = payload_file
      end

      def copyable?
        !!source_path
      end

      def call
        copyable? ? copy : download

        destination_path
      end

      def copy
        add_file(destination_path, source_path)
      end

      def download
        warn '[DEPRECATION] This method is potentially unsafe and is therefore deprecated.'

        add_file(destination_path) do |io|
          io.binmode
          io.write(content)
        end
      end

      def destination_path
        @destination_path ||=
          if subdir = destination_subdir
            ::File.join(PAYLOAD_PATH, subdir, default_path)
          else
            ::File.join(PAYLOAD_PATH, default_path)
          end
      end

      def destination_subdir
        return unless parent

        if parent.respond_to?(:nested_path) && parent.nested_path.present?
          ::File.dirname(parent.nested_path)
        else
          parent.local_id || parent.id.to_s
        end
      end

      def nested_path
        return unless parent && parent.respond_to?(:nested_path)

        ::File.join(::File.dirname(parent.nested_path), default_path)
      end

      def parent_path
        return unless parent

        ::File.join(parent_path_element, default_path)
      end

      def default_path
        if original_file? && original_filename
          original_filename
        else
          default_filename
        end
      end
    end
  end
end
