# frozen_string_literal: true

require 'net/http'
require 'uri'
require 'json'

module Ddr
  class PolicyGroupService
    GROUPER_WS_URL       = URI(ENV.fetch('GROUPER_WS_URL', 'https://groups.oit.duke.edu/grouper-ws/servicesRest/json/v2_6_000')).freeze
    STEM_NAME            = 'duke:policies:duke-digital-repository'
    GROUP_LIST_EXPIRY    = ENV.fetch('POLICY_GROUP_LIST_EXPIRY', 24.hours).to_i
    GROUP_MEMBERS_EXPIRY = ENV.fetch('POLICY_GROUP_MEMBERS_EXPIRY', 1.hour).to_i

    # @return [Array<Ddr::Auth::Group>] the policy groups
    def self.repository_groups
      Rails.cache.fetch("#{cache_key}/repository_groups", expires_in: GROUP_LIST_EXPIRY) do
        data = {
          'WsRestFindGroupsRequest' => {
            'wsQueryFilter' => {
              'stemName' => STEM_NAME,
              'queryFilterType' => 'FIND_BY_STEM_NAME'
            }
          }
        }
        response = request('/groups', data)
        results = response.dig('WsFindGroupsResults', 'groupResults') || []
        results_to_groups(results)
      end
    end

    # @param group [String]
    # @return [ActiveRecord::Relation] the User members of the group
    def self.group_users(group)
      duids = group_persons(group).map { |m| m['id'] }
      ::User.where(duid: duids)
    end

    # @param group [String]
    # @return [Array<Hash>] the person members of the group
    def self.group_persons(group)
      group_members(group).select { |m| m['type'] == 'person' }
    end

    # @param group [String]
    # @return [Array<Hash>] the members (persons and/or groups) of the group
    def self.group_members(group)
      Rails.cache.fetch("#{cache_key}/group_members/#{group}", expires_in: GROUP_MEMBERS_EXPIRY) do
        data = {
          'WsRestGetMembersRequest' => {
            'wsGroupLookups' => [{ 'groupName' => group }],
            'subjectAttributeNames' => ['name']
          }
        }
        response = request('/groups', data)

        results = response.dig('WsGetMembersResults', 'results')
        return [] unless results.present?

        subjects = results.first['wsSubjects'] || []
        subjects.map do |subject|
          {
            'id' => subject.fetch('id'),
            'name' => subject['attributeValues']&.first,
            'type' => (subject['sourceId'] == 'jndiperson' ? 'person' : 'group')
          }
        end
      end
    end

    # @param user [::User]
    # @return [Array<Ddr::Auth::Groups>] the policy groups of which the user is a member
    def self.user_groups(user)
      return [] unless user.duid?

      Rails.cache.fetch("#{cache_key}/user_groups/#{user.duid}", expires_in: GROUP_MEMBERS_EXPIRY) do
        data = {
          'WsRestGetGroupsRequest' => {
            'subjectLookups' => [{ 'subjectId' => user.duid }],
            'wsStemLookup' => { 'stemName' => STEM_NAME },
            'stemScope' => 'ONE_LEVEL'
          }
        }
        response = request('/subjects', data)
        results = response.dig('WsGetGroupsResults', 'results')
        return [] unless results.present?

        results_to_groups(results.first['wsGroups'])
      end
    end

    # @private
    def self.request(path, data)
      if service_user.blank? || service_password.blank?
        raise 'Ddr::PolicyGroupService is missing authentication credentials' if Rails.env.production?

        return {}

      end

      req = Net::HTTP::Post.new(GROUPER_WS_URL.path + path,
                                { 'Content-Type' => 'application/json', 'Accept' => 'application/json' })
      req.body = JSON.dump(data)
      req.basic_auth(service_user, service_password)
      response = Net::HTTP.start(GROUPER_WS_URL.host, use_ssl: true) do |http|
        http.request(req)
      end

      JSON.parse(response.body).tap do |content|
        Rails.logger.debug { "[Ddr::PolicyGroupService] RESPONSE: #{content}" }
      end
    end
    private_class_method :request

    # @private
    # @param results [Array<Hash>]
    # @return [Array<Ddr::Auth::Group>]
    def self.results_to_groups(results)
      return [] unless results.present?

      results
        .select { |g| g['typeOfGroup'] == 'group' }
        .map    { |g| Ddr::Auth::Group.new(g['name'], label: g['displayExtension']) }
    end

    # @private
    def self.cache_key
      name
    end

    # @private
    def self.service_user
      ENV.fetch('POLICY_GROUP_SERVICE_USER', nil)
    end
    private_class_method :service_user

    # @private
    def self.service_password
      ENV.fetch('POLICY_GROUP_SERVICE_PASSWORD', nil)
    end
    private_class_method :service_password
  end
end
