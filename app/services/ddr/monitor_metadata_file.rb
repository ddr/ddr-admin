module Ddr
  class MonitorMetadataFile
    class << self
      def call(*)
        event = ActiveSupport::Notifications::Event.new(*)
        metadata_file = MetadataFile.find(event.payload[:metadata_file_id])
        batch = Ddr::Batch::Batch.find(event.payload[:batch_id])
        email_user(metadata_file.user, metadata_file.metadata_file_name, batch)
      end

      private

      def email_user(user, metadata_file_name, batch)
        msg = <<~EOS
          Metadata File Upload has created batch ##{batch.id}
          From file: #{metadata_file_name}

          To review and process the batch, go to #{batch_url(batch)}
        EOS
        JobMailer.basic(to: user.email,
                        subject: "BATCH CREATED - Metadata File Upload - #{metadata_file_name}",
                        message: msg).deliver_now
      end

      def batch_url(batch)
        Rails.application.routes.url_helpers.ddr_batch_url(batch, host: Ddr::Admin.application_hostname,
                                                                  protocol: 'https')
      end
    end
  end
end
