module Ddr
  class ReindexItemChildren
    def self.call(*args)
      return handle_notification(*args) if args.size > 1

      resource_or_id = args.first
      resource = validate!(resource_or_id)

      IndexService.call(resource.child_ids)
    end

    def self.handle_notification(*)
      event = ActiveSupport::Notifications::Event.new(*)
      return unless %w[access_role available parent_id].any? do |attr|
                      event.payload[:attributes_changed].include?(attr)
                    end

      call(event.payload[:resource_id])
    end

    def self.validate!(resource_or_id)
      case resource_or_id
      when Ddr::Item
        if resource_or_id.new_record?
          raise ArgumentError,
                "Resource is not persisted: #{resource_or_id.inspect}."
        else
          resource_or_id
        end
      when Valkyrie::ID, String
        Ddr.query_service.find_by(id: resource_or_id)
      else
        raise TypeError, 'Argument must be a Ddr::Item, Valkyrie::ID, or String.'
      end
    end
  end
end
