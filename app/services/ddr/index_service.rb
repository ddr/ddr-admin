module Ddr
  class IndexService
    attr_reader :resource_ids

    # @param resource_ids [String, Valkyrie::ID, Enumerable<String>, Enumerable<Valkyrie::ID>]
    def self.call(resource_ids)
      resource_ids = Array(resource_ids) # Cast resource_ids to Array if not already
      resource_ids.map!(&:to_s)
      resource_ids.each_slice(Ddr::Admin.solr_update_batch_size) do |resource_id_batch|
        UpdateIndexJob.perform_later(resource_id_batch)
      end
    end

    def initialize(resource_ids)
      @resource_ids = Array(resource_ids) # Cast resource_ids to Array if not already
    end

    def index
      resources = resource_ids.map { |resource_id| Ddr.query_service.find_by(id: resource_id) }
      solr_persister.save_all(resources:)
    end

    private

    def solr_persister
      Valkyrie::MetadataAdapter.find(:index_solr).persister
    end
  end
end
