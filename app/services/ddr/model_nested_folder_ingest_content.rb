module Ddr
  class ModelNestedFolderIngestContent
    attr_reader :node

    def initialize(node)
      @node = node
    end

    def call
      if node.is_root?
        'Ddr::Collection'
      elsif node.is_leaf?
        'Ddr::Component'
      end
    end
  end
end
