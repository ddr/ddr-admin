module Ddr::Batch
  class ProcessBatch
    attr_accessor :batch, :operator_id

    def initialize(batch_id:, operator_id:)
      @batch = Ddr::Batch::Batch.find(batch_id)
      @operator_id = operator_id
      @handled = []
    end

    def execute
      ActiveSupport::Notifications.instrument('started.batch.batch.ddr', batch_id: batch.id)
      batch.batch_objects.each do |batch_object|
        if batch_object.is_a?(IngestBatchObject)
          handle_ingest_batch_object(batch_object)
        elsif batch_object.is_a?(UpdateBatchObject)
          handle_update_batch_object(batch_object)
        end
      end
      # Process any "loose" components that aren't being ingested along with an Item
      return if @handled.length == batch.batch_objects.length

      puts "Processing loose components #{@handled.length}, #{batch.batch_objects.length}}"
      components = batch.batch_objects.select { |batch_object| !@handled.include?(batch_object.id) }.map(&:id)
      BatchObjectsProcessorJob.perform_later(components, operator_id)
    end

    def handle_ingest_batch_object(batch_object)
      case batch_object.model
      when 'Ddr::Collection'
        ingest_collection_object(batch_object)
        @handled << batch_object.id
      when 'Ddr::Item'
        enqueue_item_component_ingest(batch_object)
      when 'Ddr::Component'
      # skip -- will be handled along with associated Item
      when 'Ddr::Target', 'Ddr::Attachment'
        BatchObjectsProcessorJob.perform_later([batch_object.id], operator_id)
        @handled << batch_object.id
      end
    end

    def handle_update_batch_object(batch_object)
      BatchObjectsProcessorJob.perform_later([batch_object.id], operator_id)
      @handled << batch_object.id
    end

    def ingest_collection_object(batch_object)
      # Collection batch objects are processed synchronously because they need to exist in the repository
      # prior to the processing of any objects (e.g., Item, Component, Target) associated with them.
      # If the Collection batch object does not process successfully, consider the batch finished (albeit unsuccessfully)
      # and raise an exception.
      return if ProcessBatchObject.new(batch_object_id: batch_object.id, operator: User.find(operator_id)).execute

      ActiveSupport::Notifications.instrument('finished.batch.batch.ddr', batch_id: batch.id)
      raise Ddr::Batch::BatchObjectProcessingError, batch_object.id
    end

    def enqueue_item_component_ingest(batch_object)
      query = ["object = '#{batch_object.resource_id}'",
               "batch_object_relationships.name = '#{Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT}'",
               "batches.id = #{batch_object.batch.id}"].join(' AND ')
      recs = Ddr::Batch::BatchObjectRelationship.joins(batch_object: :batch).where(query)
      batch_object_ids = recs.map { |rec| rec.batch_object.id }.unshift(batch_object.id)
      BatchObjectsProcessorJob.perform_later(batch_object_ids, operator_id)
      @handled.concat(batch_object_ids)
    end
  end
end
