module Ddr::Batch
  class ReprocessBatch < ProcessBatch
    attr_accessor :batch, :operator_id

    def execute
      ActiveSupport::Notifications.instrument('restarted.batch.batch.ddr', batch_id: batch.id)
      batch.reset!

      ## Ingest
      # Collections
      # It's unlikely there will have been a failure at this level, but for the sake of completeness...
      batch.batch_objects.where(handled: 'f', model: 'Ddr::Collection',
                                type: 'Ddr::Batch::IngestBatchObject').each do |batch_object|
        ingest_collection_object(batch_object)
      end

      # Components
      # Find loose components where the item has been ingested, but not all its components
      # Do this before Items to avoid a race condition
      loose_objects = batch.batch_objects.find_by_sql(["SELECT bo1.*
                                        FROM batch_objects AS bo1, batch_object_relationships AS bor, batch_objects AS bo2
                                        WHERE bo1.id = bor.batch_object_id
                                        AND bo1.batch_id = ?
                                        AND bo1.type = 'Ddr::Batch::IngestBatchObject'
                                        AND bo1.handled = 'f'
                                        AND bor.name='parent'
                                        AND bo1.model='Ddr::Component'
                                        AND bor.object=bo2.resource_id
                                        AND bo2.model='Ddr::Item'
                                        AND bo2.verified = 't'
                                        ORDER BY bo1.id", batch.id])
      BatchObjectsProcessorJob.perform_later(loose_objects.map { |obj| obj.id }, operator_id) if loose_objects.size > 0

      # Items
      batch.batch_objects.where(handled: 'f', model: 'Ddr::Item',
                                type: 'Ddr::Batch::IngestBatchObject').each do |batch_object|
        enqueue_item_component_ingest(batch_object)
      end

      # Targets and Attachments
      batch.batch_objects.where(handled: 'f', model: 'Ddr::Target',
                                type: 'Ddr::Batch::IngestBatchObject').each do |batch_object|
        handle_ingest_batch_object(batch_object)
      end

      batch.batch_objects.where(handled: 'f', model: 'Ddr::Attachment',
                                type: 'Ddr::Batch::IngestBatchObject').each do |batch_object|
        handle_ingest_batch_object(batch_object)
      end

      ## Update
      batch.batch_objects.where(handled: 'f', type: 'Ddr::Batch::UpdateBatchObject').each do |batch_object|
        handle_update_batch_object(batch_object)
      end
    end
  end
end
