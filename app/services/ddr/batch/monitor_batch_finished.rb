module Ddr::Batch
  class MonitorBatchFinished
    class << self
      def call(*)
        event = ActiveSupport::Notifications::Event.new(*)
        batch = Ddr::Batch::Batch.find(event.payload[:batch_id])
        batch_finished(batch)
        return unless batch.outcome == Ddr::Batch::Batch::OUTCOME_SUCCESS

        ActiveSupport::Notifications.instrument('success.batch.batch.ddr', batch_id: batch.id)
      end

      private

      def batch_finished(batch)
        update_batch(batch)
        send_email(batch) if batch.user && batch.user.email
      end

      def track_result(results_tracker, batch_object)
        type = batch_object.type
        model = batch_object.model || 'Missing Model'
        results_tracker[type] = {} unless results_tracker.has_key?(type)
        results_tracker[type][model] = {} unless results_tracker[type].has_key?(model)
        results_tracker[type][model][:successes] = 0 unless results_tracker[type][model].has_key?(:successes)
        results_tracker[type][model][:successes] += 1 if batch_object.verified
      end

      def update_batch(batch)
        outcome = batch.success_count.eql?(batch.batch_objects.size) ? Ddr::Batch::Batch::OUTCOME_SUCCESS : Ddr::Batch::Batch::OUTCOME_FAILURE
        batch.update!(stop: DateTime.now,
                      status: Ddr::Batch::Batch::STATUS_FINISHED,
                      outcome:)
      end

      def send_email(batch)
        Ddr::Batch::BatchProcessorRunMailer.send_notification(batch).deliver!
      rescue StandardError => e
        Rails.logger.error("An error occurred while attempting to send a notification for batch #{batch.id}")
        Rails.logger.error(e.message)
        Rails.logger.error(e.backtrace)
      end
    end
  end
end
