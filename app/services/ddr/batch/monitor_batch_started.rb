module Ddr::Batch
  class MonitorBatchStarted
    def self.call(*)
      event = ActiveSupport::Notifications::Event.new(*)
      batch = Ddr::Batch::Batch.find(event.payload[:batch_id])
      batch.update!(start: DateTime.now,
                    status: Ddr::Batch::Batch::STATUS_RUNNING,
                    version: Ddr::Admin::VERSION)
    end
  end
end
