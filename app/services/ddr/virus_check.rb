module Ddr
  # @deprecated Use Ddr::V3::AntivirusService
  class VirusCheck
    # @return [Hash] result data
    # @raise [Ddr::Antivirus::VirusFoundError]
    def self.call(file_path)
      {}.tap do |result|
        scan_result = Ddr::Antivirus.scan(file_path)
        result[:event_date_time] = scan_result.scanned_at
        result[:software]        = scan_result.version
        result[:detail]          = scan_result.output
      rescue Ddr::Antivirus::MaxFileSizeExceeded, Ddr::Antivirus::ScannerError => e
        result[:event_date_time] = Time.now.utc
        result[:exception] = [e.class.name, e.to_s]
        result[:software] = "ddr-antivirus #{Ddr::Antivirus::VERSION}"
      end
    end
  end
end
