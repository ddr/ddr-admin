module Ddr
  class BuildBatchesFromManifestBasedIngest
    attr_reader :user, :admin_set, :batch_name, :batch_description, :collection_title
    attr_accessor :batch, :collection_repo_id

    def initialize(user:, manifest:, admin_set: nil, collection_repo_id: nil, collection_title: nil,
                   batch_name: nil)
      @user = user
      @manifest = manifest
      raise Ddr::Batch::Error, 'Invalid manifest' unless @manifest.valid?

      @admin_set = admin_set
      # Collections may:
      # 1. Be created via the manifest (i.e., the manifest contains one or more rows with a model of 'Collection' with no id)
      # 2. Be supplied via the manifest (i.e., the manifest contains one or more rows with a model of 'Collection' with an id)
      # 3. Be created as a side effect, with the collection_title argument supplied.
      # 4. Be supplied, with the collection_repo_id argument specified.
      @collection_repo_id = collection_repo_id
      @collection_title = collection_title || 'Untitled'
      # If the manifest does not start with a collection, we will prepend a Collection row to the manifest.
      if @collection_repo_id.blank? && @manifest.first.model != 'Ddr::Collection'
        @manifest.prepend(nil, @collection_title)
      elsif @manifest.first.model != 'Ddr::Collection'
        @manifest.prepend(@collection_repo_id, nil)
      end

      @batch_name = batch_name
      @batch_description = Time.now.to_s
    end

    def call
      @batches = []
      process_manifest
      @batches.each do |batch|
        batch.update(status: Ddr::Batch::Batch::STATUS_READY)
      end
      @batches
    end

    private

    def create_batch(collection_id, collection_title)
      Ddr::Batch::Batch.create(user:, name: batch_name, description: batch_description,
                               collection_id:, collection_title:)
    end

    def process_manifest
      @manifest.each do |current|
        if current.id.blank? || 'Ddr::File' == current.model
          create_object(current)
        else
          update_object(current)
        end
      end
    end

    def create_object(current)
      resource_id = current.id
      resource_id ||= Valkyrie::ID.new(SecureRandom.uuid).id if ['Ddr::Collection', 'Ddr::Item'].include?(current.model)
      current.id = resource_id unless current.id
      if current.model == 'Ddr::Collection'
        @collection_repo_id = current.id
        @batches << create_batch(resource_id, (current.title.presence || collection_title))
      end
      # Files are a special case: they are always attached to a Component and they are not distinct Resources
      # so they are not created as BatchObjects. Instead, they are added to the parent Component's BatchObject.
      if 'Ddr::File' == current.model
        set_ddr_file(@parent_batch_object, current, Ddr::Batch::BatchObjectDatastream::OPERATION_ADDUPDATE)
      else
        batch_object = Ddr::Batch::IngestBatchObject.create(batch: @batches.last, model: current.model,
                                                            resource_id:)
        add_relationships(batch_object, @manifest.parent)
        @parent_batch_object = batch_object if 'Ddr::Component' == current.model
        add_attribute(batch_object, 'admin_set', admin_set) if admin_set.present? && current.model == 'Ddr::Collection'
        add_role(batch_object) if current.model == 'Ddr::Collection'
        add_metadata(batch_object, current.metadata) unless current.metadata.empty?
        if ['Ddr::Component', 'Ddr::Target', 'Ddr::Attachment'].include?(current.model)
          set_content_ddr_file(batch_object, current, Ddr::Batch::BatchObjectDatastream::OPERATION_ADD)
        end
      end
    end

    def update_object(current)
      @batches << create_batch(current.id, @collection_title) if current.model == 'Ddr::Collection'
      # Build an update batch object if there are any metadata values, a path, or
      # this is a Ddr::Component with a Ddr::File in the next row
      unless !current.metadata.empty? || current.path.present? || ('Ddr::Component' == current.model && @manifest.current_has_file?)
        return
      end

      batch_object = Ddr::Batch::UpdateBatchObject.new(batch: @batches.last)
      batch_object.resource_id = current.id
      batch_object.model = current.model
      batch_object.identifier = current.local_id if current.local_id
      batch_object.save
      @parent_batch_object = batch_object if 'Ddr::Component' == current.model
      # Create CLEAR operation for each editable field in file
      current.metadata.keys.each do |header|
        next if ignore?(header, current.send(header))

        att = Ddr::Batch::BatchObjectAttribute.new(
          batch_object:,
          name: header,
          operation: Ddr::Batch::BatchObjectAttribute::OPERATION_CLEAR
        )
        batch_object.batch_object_attributes << att
      end
      # Create ADD operation for each editable field in file
      add_metadata(batch_object, current.metadata) unless current.metadata.empty?
      return unless current.path.present?
      return unless ['Ddr::Component', 'Ddr::Target', 'Ddr::Attachment'].include?(current.model)

      set_content_ddr_file(batch_object, current, Ddr::Batch::BatchObjectDatastream::OPERATION_ADDUPDATE)
    end

    def add_relationships(batch_object, parent)
      add_admin_policy_relationship(batch_object) unless batch_object.model == 'Ddr::Collection'

      add_parent_relationship(batch_object, parent.id) if ['Ddr::Item', 'Ddr::Component'].include?(batch_object.model)
      add_collection_relationship(batch_object) if 'Ddr::Target' == batch_object.model
      return unless 'Ddr::Attachment' == batch_object.model

      add_attached_relationship(batch_object)
    end

    def add_admin_policy_relationship(batch_object)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: collection_repo_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_parent_relationship(batch_object, parent_id)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: parent_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_collection_relationship(batch_object)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_FOR_COLLECTION,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: collection_repo_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_attached_relationship(batch_object)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ATTACHED_TO,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: collection_repo_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_role(batch_object)
      Ddr::Batch::BatchObjectRole.create(
        batch_object:,
        operation: Ddr::Batch::BatchObjectRole::OPERATION_ADD,
        agent: user.user_key,
        role_type: Ddr::Auth::Roles::CURATOR.title,
        role_scope: Ddr::Auth::Roles::POLICY_SCOPE
      )
    end

    def add_metadata(batch_object, metadata)
      metadata.each do |key, value|
        Array(value).each do |v|
          add_attribute(batch_object, key, v) if v.present?
        end
      end
    end

    def add_attribute(batch_object, term, value)
      Ddr::Batch::BatchObjectAttribute.create(
        batch_object:,
        name: term,
        operation: Ddr::Batch::BatchObjectAttribute::OPERATION_ADD,
        value:,
        value_type: Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING
      )
    end

    def set_content_ddr_file(batch_object, current, operation)
      ds = Ddr::Batch::BatchObjectDatastream.create(
        name: :content,
        operation:,
        payload: current.path,
        payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME,
        checksum: current.sha1,
        checksum_type: Ddr::Files::CHECKSUM_TYPE_SHA1
      )
      batch_object.batch_object_datastreams << ds
    end

    def set_ddr_file(batch_object, current, operation)
      ds = Ddr::Batch::BatchObjectDatastream.create(
        name: current.file_type.to_s,
        operation:,
        payload: current.path,
        payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME,
        checksum: current.sha1,
        checksum_type: Ddr::Files::CHECKSUM_TYPE_SHA1
      )
      batch_object.batch_object_datastreams << ds
    end

    # Check whether a particular value should be ignored.
    #  - workflow_state must not be un-set if it is blank. Blank values for workflow_state
    #    are ignored.
    def ignore?(header, value)
      return false unless header == 'workflow_state' && Ddr::V3::PublicationService.workflow_state_names.exclude?(value)

      true
    end
  end
end
