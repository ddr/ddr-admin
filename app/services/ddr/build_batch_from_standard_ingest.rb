module Ddr
  class BuildBatchFromStandardIngest
    attr_reader :user, :filesystem, :intermediate_files_name, :targets_name, :content_modeler, :metadata_provider,
                :checksum_provider, :admin_set, :batch_name, :batch_description
    attr_accessor :batch, :collection_repo_id

    def initialize(user:, filesystem:, content_modeler:, checksum_provider:, intermediate_files_name: nil, targets_name: nil,
                   metadata_provider: nil, admin_set: nil, collection_repo_id: nil, batch_name: nil,
                   batch_description: nil)
      @user = user
      @filesystem = filesystem
      @intermediate_files_name = intermediate_files_name
      @targets_name = targets_name
      @content_modeler = content_modeler
      @metadata_provider = metadata_provider
      @checksum_provider = checksum_provider
      @admin_set = admin_set
      @collection_repo_id = collection_repo_id
      @batch_name = batch_name
      @batch_description = batch_description
    end

    def call
      @batch = create_batch
      traverse_filesystem
      batch.update(status: Ddr::Batch::Batch::STATUS_READY,
                   collection_id: collection_repo_id,
                   collection_title:)
      batch
    end

    private

    def collection_title
      collection_batch_objects = batch.batch_objects.where(model: 'Ddr::Collection')
      if collection_batch_objects.present?
        collection_batch_object = collection_batch_objects.first
        titles = collection_batch_object.batch_object_attributes.where(name: 'title')
        titles.empty? ? nil : titles.first.value
      else
        Ddr.query_service.find_by(id: collection_repo_id).title.first
      end
    rescue Valkyrie::Persistence::ObjectNotFoundError
      nil
    end

    def create_batch
      Ddr::Batch::Batch.create(user:, name: batch_name, description: batch_description)
    end

    def traverse_filesystem
      filesystem.each do |node|
        node.content ||= {}
        object_model = content_modeler.new(node, intermediate_files_name, targets_name).call
        if object_model == 'Ddr::Collection' && collection_repo_id.present?
          node.content[:resource_id] =
            collection_repo_id
        end
        create_object(node, object_model) if object_model && !node.content[:resource_id].present?
      end
    end

    def create_object(node, object_model)
      resource_id = assign_resource_id(node) if ['Ddr::Collection', 'Ddr::Item'].include?(object_model)
      self.collection_repo_id = resource_id if object_model == 'Ddr::Collection'
      batch_object = Ddr::Batch::IngestBatchObject.create(batch:, model: object_model, resource_id:)
      add_relationships(batch_object, node.parent)
      add_attribute(batch_object, 'admin_set', admin_set) if admin_set.present? && object_model == 'Ddr::Collection'
      add_role(batch_object) if object_model == 'Ddr::Collection'
      add_metadata(batch_object, node) if metadata_provider
      add_content_ddr_file(batch_object, node) if ['Ddr::Component', 'Ddr::Target'].include?(object_model)
      return unless object_model == 'Ddr::Component' && intermediate_node = intermediate_file(node)

      add_intermediate_file_ddr_file(batch_object, intermediate_node)
    end

    def assign_resource_id(node)
      node.content ||= {}
      node.content[:resource_id] = Valkyrie::ID.new(SecureRandom.uuid).id
    end

    def add_relationships(batch_object, parent_node)
      add_admin_policy_relationship(batch_object) unless batch_object.model == 'Ddr::Collection'
      if ['Ddr::Item', 'Ddr::Component'].include?(batch_object.model)
        add_parent_relationship(batch_object, parent_node.content[:resource_id])
      elsif batch_object.model == 'Ddr::Target'
        add_collection_relationship(batch_object)
      end
    end

    def add_admin_policy_relationship(batch_object)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_ADMIN_POLICY,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: collection_repo_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_parent_relationship(batch_object, parent_id)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_PARENT,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: parent_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_collection_relationship(batch_object)
      Ddr::Batch::BatchObjectRelationship.create(
        batch_object:,
        name: Ddr::Batch::BatchObjectRelationship::RELATIONSHIP_FOR_COLLECTION,
        operation: Ddr::Batch::BatchObjectRelationship::OPERATION_ADD,
        object: collection_repo_id,
        object_type: Ddr::Batch::BatchObjectRelationship::OBJECT_TYPE_ID
      )
    end

    def add_role(batch_object)
      Ddr::Batch::BatchObjectRole.create(
        batch_object:,
        operation: Ddr::Batch::BatchObjectRole::OPERATION_ADD,
        agent: user.user_key,
        role_type: Ddr::Auth::Roles::CURATOR.title,
        role_scope: Ddr::Auth::Roles::POLICY_SCOPE
      )
    end

    def add_metadata(batch_object, node)
      locator = Filesystem.node_locator(node)
      metadata_provider.metadata(locator).each do |key, value|
        Array(value).each do |v|
          add_attribute(batch_object, key, v)
        end
      end
    end

    def add_attribute(batch_object, term, value)
      Ddr::Batch::BatchObjectAttribute.create(
        batch_object:,
        name: term,
        operation: Ddr::Batch::BatchObjectAttribute::OPERATION_ADD,
        value:,
        value_type: Ddr::Batch::BatchObjectAttribute::VALUE_TYPE_STRING
      )
    end

    def add_content_ddr_file(batch_object, node)
      full_filepath = Filesystem.path_to_node(node)
      rel_filepath = Filesystem.path_to_node(node, 'relative')
      ds = Ddr::Batch::BatchObjectDatastream.create(
        name: :content,
        operation: Ddr::Batch::BatchObjectDatastream::OPERATION_ADD,
        payload: full_filepath,
        payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME,
        checksum: checksum_provider.checksum(rel_filepath),
        checksum_type: Ddr::Files::CHECKSUM_TYPE_SHA1
      )
      batch_object.batch_object_datastreams << ds
    end

    def add_intermediate_file_ddr_file(batch_object, node)
      full_filepath = Filesystem.path_to_node(node)
      rel_filepath = Filesystem.path_to_node(node, 'relative')
      ds = Ddr::Batch::BatchObjectDatastream.create(
        name: :intermediate_file,
        operation: Ddr::Batch::BatchObjectDatastream::OPERATION_ADD,
        payload: full_filepath,
        payload_type: Ddr::Batch::BatchObjectDatastream::PAYLOAD_TYPE_FILENAME,
        checksum: checksum_provider.checksum(rel_filepath),
        checksum_type: Ddr::Files::CHECKSUM_TYPE_SHA1
      )
      batch_object.batch_object_datastreams << ds
    end

    def intermediate_file(node)
      return unless intermediate_files_name.present?

      @intermediates ||= filesystem.root[intermediate_files_name]
      return unless @intermediates

      @intermediates.children.select do |chld|
        ::File.basename(chld.name, '.*') == ::File.basename(node.name, '.*')
      end.first
    end
  end
end
