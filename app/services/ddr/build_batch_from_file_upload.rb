module Ddr
  class BuildBatchFromFileUpload
    attr_reader :batch_user, :checksum_provider, :collection_id, :filesystem, :batch_name, :batch_description,
                :ddr_file_name
    attr_accessor :batch

    def initialize(batch_user:, collection_id:, ddr_file_name:, filesystem:, batch_description: nil,
                   batch_name: 'File Uploads', checksum_file_path: nil)
      @batch_description = batch_description
      @batch_name = batch_name
      @batch_user = batch_user
      @checksum_provider = IngestChecksum.new(checksum_file_path) if checksum_file_path.present?
      @collection_id = collection_id
      @ddr_file_name = ddr_file_name
      @filesystem = filesystem
    end

    def call
      @batch = create_batch
      traverse_filesystem
      batch.update(status: Ddr::Batch::Batch::STATUS_READY)
      batch
    end

    private

    def create_batch
      Ddr::Batch::Batch.create(user: batch_user, name: batch_name, description: batch_description,
                               collection_id:, collection_title: collection.title.first)
    end

    def traverse_filesystem
      filesystem.tree.each_leaf do |leaf|
        file_path = Filesystem.path_to_node(leaf)
        builder_args = { batch:, ddr_file_name:, file_path:,
                         repo_id: find_matching_component(collection, file_path) }
        builder_args.merge!(checksum: checksum_provider.checksum(file_path)) if checksum_provider.present?
        BuildBatchObjectFromFileUpload.new(**builder_args).call
      end
    end

    def find_matching_component(collection, file_path)
      basename = ::File.basename(file_path, '.*')
      ids = find_matching_component_ids(collection, basename)
      raise Ddr::Error, "Unable to find matching component '#{basename}' for #{file_path}" if ids.count == 0

      raise Ddr::Error, "Multiple components found matching for '#{basename}' for #{file_path}" if ids.count > 1

      ids.first
    end

    def find_matching_component_ids(collection, basename)
      ids = matching_component_query_by_local_id(collection, basename).ids
      ids = matching_component_query_by_filename(collection, basename).ids if ids.count == 0
      ids
    end

    def matching_component_query_by_local_id(collection, local_id)
      Ddr::API::IndexRepository
        .find_members(collection.id, model: :component)
        .with(:value_filter, :local_id_ssim, local_id).tap do |query|
        Rails.logger.debug { query.inspect }
      end
    end

    def matching_component_query_by_filename(collection, filename)
      Ddr::API::IndexRepository
        .find_members(collection.id, model: :component)
        .with(:prefix_filter, 'original_filename_ssi', "#{filename}.").tap do |query|
        Rails.logger.debug { query.inspect }
      end
    end

    def collection
      Ddr.query_service.find_by(id: collection_id)
    end
  end
end
