module Ddr
  # @deprecated
  class DeletedDdrFile
    def self.call(*)
      event = ActiveSupport::Notifications::Event.new(*)
      payload = event.payload
      repo_id = payload[:resource_id]
      file_profile = payload[:file_profile]
      record(repo_id, file_profile)
    end

    def self.record(repo_id, deleted_file_profile)
      DeletedFile.create(repo_id: repo_id.id,
                         file_id: deleted_file_profile[:ddr_file_type],
                         path: deleted_file_profile[:file_path])
    end
  end
end
