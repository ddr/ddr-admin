module Ddr
  class DeleteStoredFile
    def self.delete_file_by_identifier(file_identifier)
      Ddr.storage_adapter.delete(id: file_identifier)
    end
  end
end
