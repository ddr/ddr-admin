module Ddr
  # @deprecated Use Ddr::V3::ImageDerivativesUpdateService.
  class UpdateDerivatives
    def self.call(*)
      event = ActiveSupport::Notifications::Event.new(*)
      payload = event.payload
      if event.name == 'delete.ddr_file' &&
         !ddr_file_types_relevant_to_derivative_generation.include?(payload[:file_profile][:ddr_file_type])
        return false
      end
      if event.name =~ /\.ddr_resource\z/ &&
         (ddr_file_types_relevant_to_derivative_generation & payload[:files_changed].map(&:to_sym)).empty?
        return false
      end

      Ddr::V3::ImageDerivativesUpdateService.call(resource: payload[:resource_id], async: Rails.env.production?)
    end

    def self.ddr_file_types_relevant_to_derivative_generation
      Ddr::Admin.update_derivatives_on_changed
    end
  end
end
