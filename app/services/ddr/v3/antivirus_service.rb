module Ddr::V3
  #
  # Runs malware scan on a file.
  #
  # If a virus is detected, the module provides for a quarantine
  # (if QUARANTINE_PATH is set) or immediate file removal.
  #
  class AntivirusService < AbstractService
    attribute :file, Types::FileLike

    # @return [Ddr::Antivirus::ScanResult] if scanned and no virus detected
    # @return [void] if Ddr::Antivirus::MaxFileSizeExceeded is raised.
    #   In this instance, the exception is logged.
    # @raise [Ddr::Antivirus::VirusFoundError] a virus was detected
    def run
      Ddr::Antivirus.scan(file.path)
    rescue Ddr::Antivirus::MaxFileSizeExceeded => e
      logger.info { e }
    rescue Ddr::Antivirus::VirusFoundError => e
      quarantine
      raise
    end

    # Quarantines an infected file.
    # @return [void]
    # @api private
    def quarantine
      if quarantine_path = ENV['QUARANTINE_PATH']
        FileUtils.mv(file.path, quarantine_path)
      else
        FileUtils.rm_f(file.path)
      end
    end
  end
end
