module Ddr::V3
  #
  # Deletes (or marks 'unavailable') an ARK, usually for a deleted resource.
  #
  class ArkDeletionService < AbstractService
    self.job_queue = :low_priority

    attribute :permanent_id, Types::PermanentID

    before_run :skip!, unless: :ark

    # @return [Ezid::Identifier] the ARK
    def run
      if ark.deletable?
        ark.delete
      else
        ark.unavailable!('deleted')
      end
    end

    private

    # @return [Ezid::Identifier, nil] the ARK, or nil, if not found.
    def ark
      @ark ||= Ezid::Identifier.find(permanent_id) if permanent_id
    rescue Ezid::IdentifierNotFoundError => e
      logger.error(e)

      nil
    end
  end
end
