module Ddr::V3
  #
  # Checks and records fixity for all files attached to a resource.
  #
  class FixityCheckService < ResourceService
    self.job_class = NoRetryServiceJob
    self.job_queue = :low_priority
    self.event_type = Ddr::Events::FixityCheckEvent

    after_run do
      outcome = result.values.all? ? 'success' : 'failure'

      capture_event(detail: result, outcome:)
    end

    # @return [Hash] the fixity check results
    def run
      resource.attached_files.each_with_object({}) do |(file_field, ddr_file), memo|
        memo[file_field] = ddr_file.stored_checksums_valid?
      end
    end
  end
end
