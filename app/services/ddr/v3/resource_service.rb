module Ddr::V3
  #
  # Abstract superclass for resource (Ddr::Resource) services.
  #
  # @abstract
  class ResourceService < AbstractService
    # The bulk service class for this service.
    # @return [Class] the bulk service class.
    def self.bulk_service
      raise 'Bulk service not defined' if @bulk_service.nil?

      @bulk_service.is_a?(String) ? @bulk_service.constantize : @bulk_service
    end

    # Sets the bulk service class or class name for this service.
    # @return [String, Class] the bulk service class or class name
    def self.bulk_service=(service)
      raise 'Bulk service is already defined' unless @bulk_service.nil?

      @bulk_service = (Types::Strict::String | Types.Instance(Class))[service]
    end

    class_attribute :batch_service, default: BatchResourceService

    # The change set class to use for this service.
    # N.B. Not all resource services use a change set.
    class_attribute :change_set_type

    # Sets the event type to use for this handler.
    class_attribute :event_type, default: Ddr::Events::UpdateEvent

    # @!attribute resource [rw] the resource which is the subject of the service call.
    # @return [Ddr::Resource]
    attribute :resource, Types::Resource

    # Ensure that we have a fresh resource before performing the service action!
    before_run :reload_resource

    # The database metadata adapter
    # @return [Valkyrie::Persistence::Postgres::MetadataAdapter]
    def self.database
      @database ||= Valkyrie::MetadataAdapter.find(:postgres)
    end

    # The base ActiveRecord::Relation for the database
    # @return [Class] {Valkyrie::Persistence::Postgres::ORM::Resource}
    def self.db_query
      database.resource_factory.orm_class
    end

    # The index metadata adapter
    # @return [Valkyrie::Persistence::Solr::MetadataAdapter]
    def self.index
      @index ||= Valkyrie::MetadataAdapter.find(:index_solr)
    end

    # @return [Ddr::API::IndexQuery]
    def self.index_query
      Ddr::API::IndexRepository.query
    end

    # Runs the bulk service.
    # @return [void]
    def self.bulk(...)
      bulk_service.call(...)
    end

    # Run the bulk service in batch mode.
    # @return [void]
    def self.batch(...)
      bulk_service.batch(...)
    end

    # Define a bulk service for this resource service.
    # @param superclass [Class] the superclass for the bulk service
    # @yield [bulk_service] the block to define the bulk service
    # @return [Class] the bulk service class
    def self.define_bulk_service(super_class = BulkResourceService, &)
      const_set :Bulk, Class.new(super_class)

      self.bulk_service = self::Bulk

      bulk_service.resource_service = self

      yield bulk_service if block_given?

      bulk_service
    end

    delegate :ar_relation, :index, :index_query, :database, :bulk, :batch, to: :class

    delegate :collection?, :item?, :component?, :target?, :attachment?,
             :published?, :permanent_id, :persisted?,
             to: :resource

    delegate :update_resource, to: :change_set

    # The change set instance for this service instance.
    # @return [ChangeSet]
    # @raise [Ddr::Error] if change set type is not defined.
    def change_set
      raise Ddr::Error, "#{self.class}.change_set_type is not defined" unless change_set_type?

      @change_set ||= change_set_type.new(resource)
    end

    # Reloads the resource from the database.
    # @return [Ddr::Resource]
    def reload_resource
      self.resource = Ddr.query_service.find_by(id: resource.id)
    end

    # @return [void] the result of the service
    def capture_event(**attrs)
      event_type.create(event_attributes.merge(attrs))
    end

    # Attributes to initialize the event instance.
    # @return [Hash]
    def event_attributes
      { resource_id: resource.id_s, permanent_id:, event_date_time: DateTime.now }
    end

    # Mints and assigns an ARK to the resource as its permanent_id.
    # @return [Ezid::Identifier] the new ARK
    # @see Ddr::V3::ArkAssignmentService
    def assign_ark
      return if permanent_id
      ArkAssignmentService.call(resource:)
    end

    # Updates the status of the ARK assigned to the resource according to the workflow state of the resource.
    # @return [Ezid::Identifier] the ARK
    # @see Ddr::V3::ArkUpdateService
    def update_ark
      return unless permanent_id
      ArkUpdateService.call(resource:)
    end

    # Deletes the ARK from EZID, or marks it 'unavailable - deleted',
    # presumably because the resource was deleted.
    # @return [Ezid::Identifier] the ARK
    # @see Ddr::V3::ArkDeletionService
    def delete_ark
      return unless permanent_id
      ArkDeletionService.call(permanent_id:)
    end

    # Does the resource support having a parent resource?
    # @return [Boolean]
    def parent?
      resource.respond_to?(:parent)
    end

    # Does the resource have a parent resource?
    # @return [Boolean]
    def has_parent?
      resource.try(:has_parent?) || false
    end

    # Does the resource have content?
    # @return [Boolean]
    def has_content?
      resource.try(:has_content?) || false
    end

    # Does the resource support having child resources?
    # @return [Boolean]
    def children?
      resource.respond_to?(:children)
    end

    # Does the resource support having targets (Ddr::Target)?
    # @return [Boolean]
    def targets?
      resource.respond_to?(:targets)
    end

    # Does the resource support having attachments (Ddr::Attachment)?
    # @return [Boolean]
    def attachments?
      resource.respond_to?(:attachments)
    end

    # Does the resource support having IIIF
    # @return [Boolean]
    def iiif?
      resource.respond_to?(:iiif_file)
    end

    # Does the resource need FITS file characterization?
    # @return [Boolean]
    def needs_fits?
      component? && resource.fits_file.nil?
    end

    # Is the resource an image?
    # @return [Boolean]
    def image?
      resource.try(:image?) || false
    end

    # Update the resource parent's structural metadata.
    # @return [void]
    def update_parent_structure
      return unless has_parent?
      FileUpdateService[:struct_metadata].call(resource: resource.parent)
    end

    # Update the resource parent's IIIF manifest, if it has a parent and the parent supports IIIF.
    # @return [void]
    def update_parent_iiif
      return unless has_parent?
      FileUpdateService[:iiif_file].call(resource: resource.parent)
    end

    # Update the image serivatives for the resource, if it's an image.
    # @return [void]
    def update_image_derivatives
      return unless image?
      ImageDerivativesUpdateService.call(resource:)
    end

    # Update the IIIF manifest for the resource, if it supports IIIF.
    # @return [void]
    def update_iiif_file
      return unless iiif?
      FileUpdateService[:iiif_file].call(resource:)
    end

    # Delete the IIIF manifest for the resource
    # @return [void]
    def delete_iiif_file
      return unless iiif?
      FileDeletionService[:iiif_file].call(resource:)
    end

    # Update the FITS file for the resource, if it is a Component with content.
    # @return [void]
    def update_fits_file
      return unless component? && has_content?
      FileUpdateService[:fits_file].call(resource:)
    end

    # Return the API representation of the resource.
    # @return [Ddr::API::ResourceEntity]
    def api_resource
      Ddr::API::ResourceEntity.represent(resource)
    end

    # Return the JSON representation of the resource, based on the API representation.
    # @return [String]
    def json_resource
      JSON.generate(api_resource)
    end
  end
end
