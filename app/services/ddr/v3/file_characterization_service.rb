module Ddr::V3
  #
  # Characterize a file using FITS.
  #
  # Use FileUpdateService[:fits_file] (which calls this service) to persist the characterization data.
  #
  # @see https://projects.iq.harvard.edu/fits
  #
  class FileCharacterizationService < AbstractService
    attribute :path, Types::FilePath

    # @return [String] FITS XML output
    def run
      # Send error stream to /dev/null b/c it pollutes XML output (DDR-2469)
      IO.popen(['fits.sh', '-i', path], err: '/dev/null', &:read).strip.tap do |fits_xml|
        raise Ddr::Error, "FITS failure on path #{path}" unless $?.success?
      end
    end
  end
end
