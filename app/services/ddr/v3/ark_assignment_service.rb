module Ddr::V3
  #
  # Mints and assigns an ARK as the permanent_id of a resource.
  #
  class ArkAssignmentService < ResourceService
    self.change_set_type = PermanentIdChangeSet

    before_run :skip!, if: :permanent_id

    after_run do
      capture_event(detail: ark.metadata.to_anvl, summary: 'Permanent ID assignment')
    end

    run do
      change_set.update_permanent_id(ark.id)
    end

    define_bulk_service

    # @return [Ezid::Identifier] a newly minted ARK.
    def ark
      @ark ||= ArkMintingService.call(resource:)
    end
  end
end
