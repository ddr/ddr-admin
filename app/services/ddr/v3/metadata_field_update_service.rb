module Ddr::V3
  module MetadataFieldUpdateService
    #
    # This service performs a single SQL UPDATE on the database
    # changing the value of the given property to the given value.
    #
    # N.B. This service is intended for INTERNAL USE of "pre-validated" data!
    # No validation of the data is performed (other than that the property name
    # must be valid).
    #
    # After a successful update, the index is updated in bulk.
    #
    # @api private
    #
    class Bulk < BulkResourceService
      attribute :value, Types::Array # coerce all values to arrays for best compatibility

      after_run { ResourceIndexService.batch(resource_ids:) }

      run do
        db_query.where(id: resource_ids).update_all(update_set_statement)
      end

      def update_set_statement
        "metadata = jsonb_set(metadata, '{%{field_name}}', '%{value}')" % {field_name: resource_service.field_name, value: JSON.dump(value)}
      end
    end

    class Base < ResourceService
      class_attribute :field_name
      attribute :value, Types::Any
      delegate :update_field, to: :change_set

      run do
        update_field(value)
      end
    end

    # Retrieves the resource service class for the field, generating a new class if necessary.
    # @param field_name [String, Symbol] metadata field name
    # @return [Class]
    def self.[](field_name)
      field = Ddr::Metadata[field_name]
      service_name = field.name.camelize

      return const_get(service_name) if const_defined?(service_name, false)

      const_set(service_name, generate_for_field(field_name.to_sym))

    rescue KeyError => e
      raise Ddr::Error, "Invalid metadata field name: #{field_name.inspect}"
    end

    def self.bulk(field_name:, **attrs)
      self[field_name].bulk(**attrs)
    end

    # Generates a new resource service class for the specified field
    # @param field_name [Symbol] the metadata field name
    # @return [Class]
    def self.generate_for_field(field_name)
      field = Ddr::Metadata[field_name]

      Class.new(self::Base) do
        self.field_name = field_name
        self.change_set_type = MetadataFieldChangeSet[field_name]

        define_bulk_service(MetadataFieldUpdateService::Bulk)
      end
    end

    private_class_method :generate_for_field
  end
end
