module Ddr::V3
  #
  # This module contains a service for deleting each file field from a resource.
  #
  # Note that these services do not delete files from backend storage.
  # Instead, "deleted" files are tracked in the deleted_files database table,
  # and permanently removed from storage by a separate process, according to
  # the retention policy.
  #
  # @see https://duldev.atlassian.net/wiki/spaces/DDR/pages/3859775489/File+Retention+Policy+for+Deaccessioned+Resources
  #
  module FileDeletionService
    extend FileFieldSpecializationMixin

    specialize_on_file_field(ResourceService) do
      self.change_set_type = FileChangeSet[file_field]

      def run
        update_resource(file_field => nil)
      end

      before_run do
        @deleted_file = resource.send(file_field)

        skip! unless @deleted_file
      end

      after_run do
        capture_event(summary: "File deleted: #{file_field}")

        Ddr::DeletedFile.create(file_id: file_field.to_s, repo_id: resource.id_s, path: @deleted_file.file_path)
      end
    end
  end
end
