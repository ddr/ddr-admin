module Ddr::V3
  #
  # This service prunes fixity check events for a resource from the database,
  # retaining the first and last (most recent) successful events.
  #
  class FixityCheckPruningService < ResourceService
    self.job_queue = :low_priority
    self.job_class = NoRetryServiceJob

    before_run :skip!, if: ->{ query.count < 2 }

    # @return [Integer] the number of records pruned from the database
    def run
      Ddr::Events::FixityCheckEvent.delete_by(id: query.pluck(:id)[1..-2])
    end

    private

    def query
      @query ||= Ddr::Events::FixityCheckEvent.for_resource(resource).success
    end
  end
end
