module Ddr::V3
  module FileUpdateService
    #
    # Updates the IIIF file for the resource
    #
    class IiifFile < GeneratedFileService
      attribute :resource, Types::IiifResource

      self.file_field = :iiif_file
      self.media_type = 'application/json'

      # IIIF is not valid when there are no 'items', and we only persist
      # the file for a published resource.
      before_run :skip!, unless: :published?
      before_run :skip!, unless: -> { published_children.any? }

      # Children must have ARKs to use in IIIF id URIs.
      before_run :ensure_children_have_arks

      # Components will not render properly without technical metadata,
      # specifically height and width data.
      before_run :ensure_components_have_techmd, if: :item?

      define_bulk_service

      # @deprecated Item attributes which, when changed, trigger a IIIF file update on the parent Collection.
      ITEM_ATTRIBUTES = %w[created_at ingestion_date local_id nested_path].freeze

      # @deprecated Component attributes which, when changed, trigger a IIIF file update on the parent Item.
      COMPONENT_ATTRIBUTES = %w[created_at display_label fits_file ingestion_date local_id original_filename
                              title].freeze

      # @deprecated For legacy ActiveSupport::Notifications::Event handling.
      def self.update_parent(resource:)
        call(resource: resource.parent) if resource.has_parent?
      end

      # @deprecated For legacy ActiveSupport::Notifications::Event handling.
      def self.handle_notification_event(*)
        warn 'Ddr::V3::FileUpdateService::IiifFile.handle_notification_event is deprecated.'

        event = ActiveSupport::Notifications::Event.new(*)

        resource_id, attributes_changed = event.payload.values_at(:resource_id, :attributes_changed)

        resource = Types::Resource[resource_id]

        return unless resource.published? && attributes_changed.present?

        case resource
        when Ddr::Collection
          call(resource:)

        when Ddr::Item
          call(resource:)
          update_parent(resource:) if attributes_changed.intersect?(ITEM_ATTRIBUTES)

        when Ddr::Component
          update_parent(resource:) if attributes_changed.intersect?(COMPONENT_ATTRIBUTES)
        end
      end

      def generate_file
        file.write IiifPresentationService.call(resource:, json: true)
      rescue IiifPresentationService::IiifPresentationError => e
        raise Ddr::Error, e
      end

      def published_children
        Ddr::API::IndexRepository.query
          .with_workflow_state_filter('published')
          .with_parent_filter(resource)
      end

      private

      def ensure_children_have_arks
        published_children.with_no_value_filter('permanent_id_ssi').each do |child|
          ArkAssignmentService.call(resource: child)
        end
      end

      def ensure_components_have_techmd
        published_children.with_no_value_filter('fits_file_tsim').each do |child|
          FileUpdateService[:fits_file].call(resource: child)
        end
      end
    end
  end
end
