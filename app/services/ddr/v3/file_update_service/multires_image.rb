module Ddr::V3
  module FileUpdateService
    class MultiresImage < GeneratedImageFileService
      self.source_media_types = %w[image/tiff image/jpeg image/gif image/bmp image/png].freeze

      TILE_HEIGHT = 256
      TILE_WIDTH  = 256

      Q_FACTOR = 90

      # Options for Vips::Image#tiffsave.
      OPTIONS = {
        pyramid:     true,
        Q:           Q_FACTOR,
        tile:        true,
        tile_height: TILE_HEIGHT,
        tile_width:  TILE_WIDTH,
        compression: :jpeg
      }.freeze

      self.file_field = :multires_image
      self.media_type = 'image/tiff'
      self.source_fields = %i[intermediate_file content].freeze

      define_bulk_service

      def generate_file
        source_image.tiffsave(file.path, **OPTIONS)
      end
    end
  end
end
