module Ddr::V3
  module FileUpdateService
    class DerivedImage < GeneratedImageFileService
      self.job_queue = :low_priority

      self.file_field = :derived_image
      self.source_fields = [:multires_image].freeze
      self.media_type = 'image/jpeg'

      define_bulk_service

      def generate_file
        source_image.jpegsave(file.path)
      end
    end
  end
end
