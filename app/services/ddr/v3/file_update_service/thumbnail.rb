module Ddr::V3
  module FileUpdateService
    class Thumbnail < GeneratedImageFileService
      self.job_queue = :low_priority

      WIDTH = 100

      self.file_field = :thumbnail
      self.media_type = 'image/png'
      self.source_fields = %i[intermediate_file content].freeze

      define_bulk_service

      def generate_file
        source_image
          .thumbnail_image(WIDTH, no_rotate: true, size: :down)
          .pngsave(file.path)
      end
    end
  end
end
