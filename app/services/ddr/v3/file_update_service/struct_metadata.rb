module Ddr::V3
  module FileUpdateService
    #
    # Updates the structural metadata of a structural resource.
    #
    class StructMetadata < GeneratedFileService
      attribute :resource, Types::StructuralResource

      self.file_field = :struct_metadata
      self.media_type = 'application/xml'

      before_run :ensure_children_have_arks

      define_bulk_service

      def generate_file
        file.write StructuralMetadataService.call(resource:)
      end

      # Ensures that all children of the resource have ARKs, so that the
      # structural metadata can reference them.
      # @api private
      # @return [void]
      def ensure_children_have_arks
        Ddr::API::IndexRepository.query
          .with_parent_filter(resource)
          .with_no_value_filter('permanent_id_ssi').each do |child|
          ArkAssignmentService.call(resource: child)
        end
      end
    end
  end
end
