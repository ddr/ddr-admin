module Ddr::V3
  module FileUpdateService
    class FitsFile < GeneratedFileService
      self.job_queue = :low_priority

      self.file_field = :fits_file
      self.media_type = 'application/xml'

      before_run :skip!, unless: :has_content?

      define_bulk_service

      def generate_file
        file.write FileCharacterizationService.call(path: resource.content.file_path)
      end
    end
  end
end
