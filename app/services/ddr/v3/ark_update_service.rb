module Ddr::V3
  #
  # Updates the status of an ARK according to the publication status of the resource.
  #
  class ArkUpdateService < ResourceService
    self.job_queue = :low_priority

    before_run :skip!, unless: :ark

    # @return [Ezid::Identifier] the ARK
    def run
      return ark.update(status: 'public') if !ark.public? && resource.published?

      return ark.update(status: 'unavailable | not published') if ark.public? && !resource.published?

      logger.debug { "#{self.class} -- no change made to #{ark} assigned to resource #{resource.id}" }

      ark
    end

    # @return [Ezid::Identifier] the ARK for the resource
    # @raise [Ezid::IdentifierNotFoundError] ARK not found in EZID
    def ark
      @ark ||= Ezid::Identifier.find(resource.permanent_id) if resource.permanent_id
    end
  end
end
