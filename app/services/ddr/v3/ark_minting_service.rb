# frozen_string_literal: true

module Ddr::V3
  #
  # Service for minting ARKs.
  #
  class ArkMintingService < ResourceService
    PROFILE = 'dc'
    EXPORT  = 'no'
    STATUS  = 'reserved'
    TARGET  = "https://repository.duke.edu/id/%{ark}"

    def run
      Ezid::Identifier.mint(profile: PROFILE, export: EXPORT, status: STATUS, 'ddr.resource.id': resource.id_s).tap do |ark|
        target = TARGET % {ark:}

        ark.update(target:)
      end
    end
  end
end
