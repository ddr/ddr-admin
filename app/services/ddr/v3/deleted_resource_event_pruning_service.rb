module Ddr::V3
  class DeletedResourceEventPruningService < AbstractService
    attribute? :dryrun, Types::Bool.default(false)

    def run
      {}.tap do |results|
        event_types.each do |event_type|
          events = event_type.for_deleted_resources

          results[event_type.name] = dryrun ? events.count : events.delete_all
        end
      end
    end

    def event_types
      Ddr::Events::Event.subclasses.reject(&:retain_for_deleted_resources)
    end
  end
end
