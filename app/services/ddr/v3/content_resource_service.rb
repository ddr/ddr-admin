module Ddr::V3
  #
  # Abstract superclass for content-bearing resource services.
  #
  # @abstract
  class ContentResourceService < ResourceService
    # @!attribute resoure [rw]
    #   @return [Ddr::HasContent]
    attribute :resource, Types::ContentResource
  end
end
