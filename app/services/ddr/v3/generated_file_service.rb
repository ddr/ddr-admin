module Ddr::V3
  # @abstract
  class GeneratedFileService < ResourceService
    class_attribute :file_field

    # The media type of the generated output file
    class_attribute :media_type
    self.media_type = 'application/octet-stream'

    # @!attribute file [r] the generated file
    #   @return [Tempfile, nil]
    attr_reader :file

    def self.file_extension
      @file_extension ||= MIME::Types[media_type].first&.preferred_extension || 'bin'
    end

    delegate :file_extension, to: :class

    before_run :skip!, unless: ->{ resource.respond_to?(file_field) }

    # Create a temporary file, assigns to the `file' attribute, yields (to #run), and then uploads it.
    # @return [void]
    # @api private
    def run
      Tempfile.create(%W[#{file_field}- .#{file_extension}]) do |tmp|
        @file = tmp
        generate_file
        upload_file
      end
    end

    def generate_file
      raise NotImplementedError, "Subclasses must implement #{self.class}#generate_file"
    end

    # Uploads the generated file to the repository.
    # @return [void]
    # @api private
    def upload_file
      file_upload_service.call(resource:, file:, media_type:, original_filename:)
    end

    # The file upload service for the file field.
    # @return [ResourceService]
    # @see FileUploadService
    # @api private
    def file_upload_service
      FileUploadService[file_field]
    end

    # The "original filename" to use for the generated file.
    # @return [String]
    # @api private
    def original_filename
      "#{resource.id}-#{file_field}.#{file_extension}"
    end
  end
end
