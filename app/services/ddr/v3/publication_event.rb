module Ddr::V3
  #
  # Abstract superclass of concrete publication services.
  #
  # Each subclass is responsible for implementing the following class attributes:
  # - workflow_event (an event from Ddr::V3::PublicationConstants::WORKFLOW_EVENTS)
  # - transitions_to (a state from Ddr::V3::PublicationConstants::WORKFLOW_STATES)
  # - allowed_from   (an array of states from Ddr::V3::PublicationService::WORKFLOW_STATES)
  #
  # @abstract
  # @see Ddr::V3::PublicationService
  #
  class PublicationEvent < ResourceService
    include PublicationConstants

    # Restricts resources to publishable types -- i.e., Collection, Item, Component.
    # @!attribute resource [rw]
    #   @return [Ddr::Publishable]
    attribute :resource, Types::PublishableResource

    # This is intended to be an email address for notifying a human
    # that the service has completed operation (successfully or not).
    # @!attribute on_behalf_of [rw]
    #   @return [String] expected to be an email address
    attribute? :on_behalf_of, Types::Agent.optional

    # The workflow event that triggers the service.
    class_attribute :workflow_event

    # The state to which the resource transitions.
    class_attribute :transitions_to

    # The states from which the resource is allowed to transition.
    class_attribute :allowed_from

    # A list of workflow states indicates that a child resource
    # must have a parent resource in one of the workflow states.
    class_attribute :parent_states

    # The state of the resource at the beginning of the service.
    attr_reader :initial_state

    # Set initial state for future reference
    after_initialize { @initial_state = current_state }

    before_run do
      # If no transition, skip the resource, but process children and notify, etc.
      finalize! if no_transition?

      # Don't raise an exception if the event/transition is not possible/allowed, just skip it.
      skip! unless can?
    end

    after_run do
      capture_event(user_key: on_behalf_of, summary: workflow_event)
      reload_resource
      update_ark
    end

    before_finalize :process_children, if: :children?

    after_finalize :notify, if: [:top_level?, :on_behalf_of]

    # Send e-mail notification that the service has completed (if on_behalf_of is present).
    # @return [void]
    def notify
      return unless on_behalf_of

      subject = "DDR Publication Service: #{resource.human_readable_type} #{resource.id}"
      body = <<-EOS
          The DDR Publication Service has completed successfully:

          Resource:       #{resource.human_readable_type} #{resource.id}
          Workflow state: #{current_state}
      EOS

      Mailer.with(to: on_behalf_of, subject:, body:).basic.deliver_now
    end

    # If the initial state of the resource is 'nonpublishable',
    # then we only process nonpublishable children; conversely,
    # if the initial state of the resource is publishable (i.e., not nonpublishable),
    # then we only process publishable children.
    # @return [void]
    def process_children
      if initial_state == NONPUBLISHABLE
        process_nonpublishable_children
      else
        process_publishable_children
      end
    end

    # Process publishable children.
    # @return [void]
    def process_publishable_children
      batch(resource_ids: publishable_children.ids, dependent: true)
    end

    # Publishable children are those that are not in the nonpublishable state.
    # @return [Ddr::API::IndexQuery]
    def publishable_children
      index_query.with_parent_filter(resource)
        .with_workflow_state_filter(allowed_from.excluding(NONPUBLISHABLE))
    end

    # Process nonpublishable children
    # @return [void]
    def process_nonpublishable_children
      bulk(resource_ids: nonpublishable_children.ids, dependent: true)
    end

    # Nonpublishable children are those that are in the nonpublishable state.
    # @return [Ddr::API::IndexQuery]
    def nonpublishable_children
      index_query.with_parent_filter(resource)
        .with_workflow_state_filter(NONPUBLISHABLE)
    end

    # Can the service be run?
    # @see #can?
    def self.can?(...)
      new(...).can?
    end

    # Persists the workflow state change to the resource.
    # @return [void]
    def run
      MetadataFieldUpdateService[:workflow_state].call(resource:, value: transitions_to)
    end

    # Is the service a no-op?
    # @return [Boolean]
    def no_transition?
      initial_state == transitions_to
    end

    # Can the service be run?
    # @return [Boolean]
    def can?
      persisted? && allowed_from? && parent_condition?
    end

    # An extra condition that must be met for the service to run.
    # @return [Boolean]
    def parent_condition?
      return true if collection?
      return false unless has_parent?
      return true if parent_states.blank?

      parent_states.include?(resource.parent.workflow_state)
    end

    # Is the workflow transition allowed from the initial state?
    # @return [Boolean]
    def allowed_from?
      allowed_from.include?(initial_state)
    end

    def descendants_allowed_from
      if initial_state == NONPUBLISHABLE
        [ NONPUBLISHABLE ]
      else
        allowed_from.excluding(NONPUBLISHABLE)
      end
    end

    # The current state of the resource.
    # @return [String]
    def current_state
      resource.workflow_state
    end
  end
end
