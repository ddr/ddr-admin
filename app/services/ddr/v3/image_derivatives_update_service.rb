module Ddr::V3
  #
  # Service for updating image derivatives.
  #
  class ImageDerivativesUpdateService < ContentResourceService
    self.job_queue = :low_priority

    before_run :skip!, unless: :image?

    def run
      FileUpdateService[:thumbnail].call(resource:, dependent: true)

      if component?
        FileUpdateService[:multires_image].call(resource:, dependent: true)
        FileUpdateService[:derived_image].call(resource:, dependent: true)
      end
    end
  end
end
