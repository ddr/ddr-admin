module Ddr::V3
  module PublicationService
    #
    # Makes a resource (and its decendants) previewable.
    #
    # @api private
    #
    class Preview < PublicationEvent
      define_bulk_service
      
      self.workflow_event = PREVIEW
      self.transitions_to = PREVIEWABLE
      self.allowed_from   = [UNPUBLISHED, PREVIEWABLE].freeze
      self.parent_states  = [PUBLISHED, PREVIEWABLE].freeze
    end
  end
end
