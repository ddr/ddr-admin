module Ddr::V3
  module PublicationService
    #
    # Publishes a resource and its descendants.
    #
    # @api private
    #
    class Publish < PublicationEvent
      self.job_queue = :high_priority

      self.workflow_event = PUBLISH
      self.transitions_to = PUBLISHED
      self.allowed_from   = [UNPUBLISHED, PREVIEWABLE, PUBLISHED].freeze
      self.parent_states  = [PUBLISHED].freeze

      define_bulk_service

      before_run :assign_ark, unless: :permanent_id
      before_run :update_fits_file, if: :needs_fits?

      finalize do
        update_iiif_file if iiif?
        update_parent_iiif if top_level? && parent?
      end

      # Update the FITS file for the resource
      # @return [void]
      def update_fits_file
        super
        reload_resource
      end

      # Assign an ARK to the resource
      # @return [void]
      def assign_ark
        super
        reload_resource
      end
    end
  end
end
