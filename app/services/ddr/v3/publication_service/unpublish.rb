module Ddr::V3
  module PublicationService
    #
    # Unpublishes a resource and its descendants.
    #
    # @api private
    #
    class Unpublish < PublicationEvent
      self.workflow_event = UNPUBLISH
      self.transitions_to = UNPUBLISHED
      self.allowed_from   = [UNPUBLISHED, PREVIEWABLE, PUBLISHED, NONPUBLISHABLE].freeze

      define_bulk_service
      
      finalize do
        delete_iiif_file if iiif?
        update_parent_iiif if top_level? && parent?
      end
    end
  end
end
