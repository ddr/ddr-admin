module Ddr::V3
  module PublicationService
    #
    # Makes a resource nonpublishable.
    #
    # @api private
    #
    class MakeNonpublishable < PublicationEvent
      define_bulk_service

      self.workflow_event = MAKE_NONPUBLISHABLE
      self.transitions_to = NONPUBLISHABLE
      self.allowed_from   = [UNPUBLISHED, PREVIEWABLE, NONPUBLISHABLE].freeze
    end
  end
end
