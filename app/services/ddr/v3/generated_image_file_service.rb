module Ddr::V3
  # @abstract
  class GeneratedImageFileService < GeneratedFileService
    class_attribute :source_fields
    self.source_fields = [].freeze

    # The usable media types of the source files
    # If nil, any media type is usable.
    # @return [Array<String>, nil] the media types
    class_attribute :source_media_types

    before_run :skip!, unless: :image?
    before_run :skip!, unless: :source

    # @return [Ddr::File, nil] the source file
    def source
      @source ||= catch(:file) do
        source_fields.each do |source_field|
          if file = resource.try(source_field)
            if !source_media_types || source_media_types.include?(file.media_type)
              throw :file, file
            end
          end
        end

        nil
      end
    end

    # @return [Vips::Image] the source image
    def source_image
      VipsImageLoader.call(path: source.file_path, media_type: source.media_type)
    end
  end
end
