module Ddr::V3
  #
  # A wrapper for running multiple resource services synchronously (in order).
  #
  class ResourceMultiService < ResourceService
    attribute :services, Types.Array(Types.Instance(Class))

    def run
      services.map { |service| service.call(service_attributes.excluding(:services)) }
    end

    define_bulk_service
  end
end
