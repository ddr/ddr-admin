module Ddr::V3
  #
  # Provides generated structural metadata for the resource.
  #
  class StructuralMetadataService < ResourceService
    # @!attribute resoure [rw]
    #   @return [Ddr::Types::StructuralResource]
    attribute :resource, Types::StructuralResource

    def run
      Ddr::DefaultStructureService.default_structure(resource)
    end
  end
end
