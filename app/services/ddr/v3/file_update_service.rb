module Ddr::V3
  module FileUpdateService
    extend FileFieldSpecializationMixin

    class Multi < ResourceService
      attribute :file_fields, Types.Array(Types::FileField).default([].freeze)

      before_run :skip!, if: :file_fields.empty?

      run do
        ResourceMultiService.call(services:, **service_attributes)
      end

      def services
        file_fields.map { |file_field| FileUpdateService[file_field] }
      end

      define_bulk_service
    end

    def self.multi(...)
      Multi.call(...)
    end

    def self.bulk(**attrs)
      file_fields = attrs[:file_fields]

      return if file_fields.blank?

      return Multi.bulk(**attrs) if file_fields.length > 1

      FileUpdateService[file_fields.first].bulk(**attrs.excluding(:file_fields))
    end
  end
end

Dir[File.join(__dir__, 'file_update_service', '*.rb')].each { |lib| require(lib) }
