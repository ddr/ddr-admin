require 'iiif/v3/presentation'

module Ddr::V3
  #
  # Generates IIIF v3 Presentation API manifests.
  # @see https://iiif.io/api/presentation/3.0/
  #
  # Based on the "O'Sullivan" Ruby library for IIIF.
  # @see https://github.com/hcayless/osullivan (H. Cayless's fork of the main project)
  #
  # IIIF manifests are not currently used directly in ddr-admin;
  # their primary purpose is to support public (and preview) renderings
  # of image resources.  Future development may expand on this
  # initial purpose.
  #
  # Note the this service does *not* persist the generated manifest;
  # use Ddr::V3::IiifFileUpdateService to create/update a
  # persisted manifest file for a resource.
  #
  class IiifPresentationService < ResourceService
    # @!attribute resource [rw]
    #   The resource for which to generate the IIIF manifest.
    #   @return [Ddr::Types::IiifResource]
    attribute :resource, Types::IiifResource

    # @!attribute start [rw]
    #   If a `start` Component is provided for the generation of an Item manifest,
    #   the output will include a 'start' element referencing that image in the manifest,
    #   which will instruct the viewer to begin rendering
    #   at that point instead of the first image.
    #   @see https://iiif.io/api/presentation/3.0/#start
    #   @return [Ddr::Component, nil]
    attribute :start, Types::Component.optional

    # @!attribute include_previewable [rw]
    #   Flag to include resources in the 'previewable' workflow state.
    #   The default is `false`, which means to include only published resources.
    #   @return [Boolean]
    attribute :include_previewable, Types::Bool.default(false)

    # @!attribute force [rw]
    #   Flag (from osullivan) to bypass validation (default: `false`)
    #   @return [Boolean]
    attribute :force, Types::Bool.default(false)

    # @!attribute json [rw]
    #   Flag to generate manifest as JSON (string).
    #   The default is `false` which generates a Hash.
    #   @return [Boolean]
    attribute :json, Types::Bool.default(false)

    # Error class for IIIF Presentation errors, which lack a common base class in osullivan.
    class IiifPresentationError < Ddr::Error; end

    # Public server URL that should be encoded in the manifest.
    SERVER_URL = ENV.fetch('IIIF_SERVER_URL', 'http://localhost:8080')

    # DDR resource media types (IANA) mapped to the IIIF 'Dataset' content resource type
    DATASET_MEDIA_TYPES = %w[ application/x-7z-compressed
                              application/x-bzip2
                              application/x-gzip
                              application/x-rar-compressed
                              application/x-tar
                              application/x-zip
                              application/zip].freeze

    # Class method to generate a IIIF 'id' for a resource.
    # @see https://iiif.io/api/presentation/3.0/#id
    # @return [String]
    def self.iiif_resource_id(resource, path = nil)
      "#{SERVER_URL}/iiif/#{CGI.escapeURIComponent(resource.permanent_id)}#{path}"
    end

    # @return [Array<IIIF::V3::Presentation::ImageResource>]
    def self.logo
      @logo ||= IIIF::V3::Presentation::ImageResource.new.tap do |image|
        image.id     = "#{SERVER_URL}/logos/devil_white_shadow.png"
        image.format = 'image/png'
        image.height = 92
        image.width  = 150
      end.freeze
    end

    # @return [Array<IIIF::V3::Presentation::Agent>]
    def self.provider
      @provider ||= IIIF::V3::Presentation::Agent.new.tap do |agent|
        agent.id = 'https://library.duke.edu'
        agent.label = { 'en' => ['Duke University Libraries'].freeze }.freeze
        agent.homepage = [
          IIIF::V3::Presentation::Resource.new('type' => 'Text').tap do |homepage|
            homepage.id = 'https://library.duke.edu'
            homepage.label = { 'en' => ['Duke University Libraries homepage'].freeze }.freeze
            homepage.format = 'text/html'
          end.freeze
        ].freeze
      end.freeze
    end

    # @return [Hash] the requiredStatement for the manifest
    def self.requiredStatement
      @requiredStatement ||= {
        'label' => { 'en' => ['Attribution'].freeze }.freeze,
        'value' => { 'en' => ['Provided by Duke University Libraries'].freeze }.freeze
      }.freeze
    end

    # Generates the manifest output
    # @api private
    # @return [String, Hash] JSON (if `json` == `true`) or Hash output
    def run
      iiif = iiif_type.new

      iiif.id       = self.class.iiif_resource_id(resource, '/manifest')
      iiif.logo     = [self.class.logo]
      iiif.label    = { 'en' => [resource.title_display] }
      iiif.homepage = [homepage]
      iiif.provider = [self.class.provider]
      iiif.requiredStatement = self.class.requiredStatement
      iiif.rights = resource.rights.first if resource.rights.present?
      iiif.viewingDirection = resource.viewing_direction if resource.viewing_direction
      iiif.metadata = metadata

      add_items_and_rendering(iiif)

      iiif['start'] = start_canvas if start

      return iiif.to_json(force:) if json

      iiif.to_ordered_hash(force:)
    rescue IIIF::V3::Presentation::MissingRequiredKeyError,
           IIIF::V3::Presentation::ProhibitedKeyError,
           IIIF::V3::Presentation::IllegalValueError => e
      raise IiifPresentationError, e
    end

    private

    # @return [String, Array<String>] the workflow state(s) to include
    def workflow_state
      include_previewable ? %w[previewable published] : 'published'
    end

    # @return [Class] the IIIF resource type (Collection or Manifest)
    def iiif_type
      collection? ? IIIF::V3::Presentation::Collection : IIIF::V3::Presentation::Manifest
    end

    # @return [Hash] the IIIF metadata fields
    def metadata
      fields = resource.descriptive_metadata.select { |_k, v| v.present? }

      fields.map do |name, value|
        {
          'label' => { 'en' => [Ddr::Metadata[name].title] },
          'value' => Array(value)
        }
      end
    end

    # @return [IIIF::V3::Presentation::Resource] the homepage resource
    def homepage
      IIIF::V3::Presentation::Resource.new('type' => 'Text').tap do |hp|
        hp.id = resource.permanent_url
        hp.label = { 'en' => ['Permanent URL'] }
        hp.format = 'text/html'
        hp.language = ['en']
      end
    end

    # Adds items and rendering to the IIIF object.
    # @api private
    # @param iiif [IIIF::V3::Presentation::Collection, IIIF::V3::Presentation::Manifest] the IIIF object
    # @return [void]
    def add_items_and_rendering(iiif)
      if collection?
        iiif.items = if items_query.any? && items_query.peek.nested_path
                       nested_collections
                     else
                       items_query.map { |item| manifest_reference(item) }
                     end

      else # Item
        components_query.each_with_index do |component, index|
          restype = iiif_resource_type(component)

          # Ddr::Item has 'folder' display_format OR Ddr::Component media_type implies 'Image' IIIF resource type
          if resource.display_format == 'folder' || restype == IIIF::V3::Presentation::ImageResource::TYPE
            label = component.display_label || "Image #{index + 1}" # 1-based
            iiif.items << image_canvas(component, label)

          # Ddr::Item has 'image' display_format (but NOT 'Image' IIIF resource type)
          elsif resource.display_format == 'image'
            iiif.rendering << rendering(component)

          else
            iiif.items << resource_canvas(component, restype)
          end
        end
      end
    end

    # @return [Array<IIIF::V3::Presentation::Collection>] the nested collections
    def nested_collections
      [].tap do |nested_collections|
        items_query.each do |item|
          # start/reset with top level list
          current = nested_collections

          # split the nested path into elements
          path = item.nested_path.split('/')

          # iterate through the path elements, finding or creating a nested collection for each element
          path.each_with_index do |path_element, index|
            # Cf. nested collection label below
            found = current.find { |i| i.label.dig('en', 0) == path_element && i.is_a?(Collection) }

            if found
              # move the current items pointer and keep going
              current = found.items
              next
            end

            # not found - create new nested collection
            nested_collection = IIIF::V3::Presentation::Collection.new.tap do |coll|
              # [DDK-343] use path elements up to this point in the URL fragment identifier
              nested_coll_path = '/manifest#' + CGI.escapeURIComponent(path[0..index].join('/'))
              coll.id = self.class.iiif_resource_id(resource, nested_coll_path)
              coll.label = { 'en' => [path_element] }
            end

            # append the nested collection to the current list of items
            current << nested_collection

            # point current at nested collection's items
            current = nested_collection.items
          end

          # at the end, append manifest reference to the current items (i.e., of the innermost nested collection)
          current << manifest_reference(item)
        end
      end
    end

    # @return [Ddr::API::IndexQuery] the items query
    def items_query
      Ddr::API::IndexRepository.query(rows: 1000)
                               .with_parent_filter(resource)
                               .with_workflow_state_filter(workflow_state)
                               .sort_by('nested_path_ssi asc,local_id_ssi asc,ingestion_date_dtsi asc,created_at_dtsi asc')
    end

    # @return [IIIF::V3::Presentation::Canvas] the image canvas for the component
    # @param component [Ddr::Component] the component
    # @param label [String] the label
    def image_canvas(component, label)
      height = component.techmd.image_height.first
      width = component.techmd.image_width.first
      base_id = self.class.iiif_resource_id(component)

      IIIF::V3::Presentation::Canvas.new.tap do |canvas|
        canvas.id = base_id + '/canvas'
        canvas.label = { 'en' => [label] }
        canvas.height = height.to_i if height
        canvas.width = width.to_i if width
        canvas.items << IIIF::V3::Presentation::AnnotationPage.new.tap do |anno_page|
          anno_page.id = base_id + '/annotation_page'
          anno_page.items << IIIF::V3::Presentation::Annotation.new.tap do |anno|
            anno.id = base_id + '/annotation'
            anno.motivation = 'painting'
            anno.target = base_id + '/canvas'
            anno.body = IIIF::V3::Presentation::ImageResource.new.tap do |body|
              body.id = base_id + '/full/full/0/default.jpg'
              body.format = 'image/jpeg'
              body.height = height.to_i if height
              body.width = width.to_i if width
              body.service << IIIF::V3::Presentation::Service.new(
                'type' => IIIF::V3::Presentation::Service::IIIF_IMAGE_V2_TYPE,
                'id' => base_id,
                'profile' => 'http://iiif.io/api/image/2/level2.json'
              )
            end
          end
        end
      end
    end

    # @return [Hash] the rendering hash for the component
    # @param component [Ddr::Component] the component
    def rendering(component)
      {
        'type' => iiif_resource_type(component),
        'id' => "#{SERVER_URL}/download/#{component.id}",
        'label' => { 'en' => [component.original_filename] },
        'format' => component.content_type
      }
    end

    # @return [IIIF::V3::Presentation::Canvas] the resource canvas for the component
    # @param component [Ddr::Component] the component
    # @param restype [String] the IIIF resource type
    def resource_canvas(component, restype)
      base_id = self.class.iiif_resource_id(component)

      stream_or_download = %w[Sound Video].include?(restype) ? 'stream' : 'download'

      IIIF::V3::Presentation::Canvas.new.tap do |canvas|
        canvas.id = base_id + '/canvas'
        canvas.label = { 'en' => [component.display_label || component.title_display] }
        canvas.items << IIIF::V3::Presentation::AnnotationPage.new.tap do |anno_page|
          anno_page.id = base_id + '/annotation_page'
          anno_page.items << IIIF::V3::Presentation::Annotation.new.tap do |anno|
            anno.id = base_id + '/annotation'
            anno.motivation = 'painting'
            anno.target = base_id + '/canvas'
            anno.body = IIIF::V3::Presentation::Resource.new(
              'type' => restype,
              'id' => "#{SERVER_URL}/#{stream_or_download}/#{component.id}"
              )
          end
        end
      end
    end

    # @return [String] the IIIF resource type for the component
    # @param component [Ddr::Component] the component
    def iiif_resource_type(component)
      case component.content_type
      when %r{^image/}
        IIIF::V3::Presentation::ImageResource::TYPE
      when %r{^text/}
        'Text'
      when %r{^audio/}
        'Sound'
      when %r{^video/}
        'Video'
      when Regexp.union(*DATASET_MEDIA_TYPES)
        'Dataset'
      else
        'Text'
      end
    end

    # @return [Ddr::API::IndexQuery] the components query
    def components_query
      Ddr::API::IndexRepository.query(rows: 1000)
                               .with_parent_filter(resource)
                               .with_workflow_state_filter(workflow_state)
                               .sort_by('local_id_ssi asc,title_ssi asc,original_filename_ssi asc,ingestion_date_dtsi asc,created_at_dtsi asc')
    end

    # @return [Hash] the manifest reference for the item
    # @param item [Ddr::Item] the item
    def manifest_reference(item)
      {
        'type' => IIIF::V3::Presentation::Manifest::TYPE,
        'id' => "#{SERVER_URL}/iiif/#{CGI.escapeURIComponent(item.permanent_id)}/manifest",
        'label' => { 'en' => [item.title_display] }
      }
    end

    # @return [IIIF::V3::Presentation::Canvas] the start canvas for the start component
    def start_canvas
      IIIF::V3::Presentation::Canvas.new('id' => self.class.iiif_resource_id(start, '/canvas'))
    end
  end
end
