# frozen_string_literal: true

module Ddr::V3
  #
  # Constants for publication workflow states and events.
  #
  module PublicationConstants
    # The published workflow state
    PUBLISHED = 'published'

    # The unpublished workflow state
    UNPUBLISHED = 'unpublished'

    # The previewable workflow state
    PREVIEWABLE = 'previewable'

    # The nonpublishable workflow state
    NONPUBLISHABLE = 'nonpublishable'

    # Valid workflow states
    WORKFLOW_STATES = [PUBLISHED, UNPUBLISHED, PREVIEWABLE, NONPUBLISHABLE].freeze

    # The default workflow state
    DEFAULT_WORKFLOW_STATE = UNPUBLISHED

    # The event that transitions a resource to the 'published' state.
    PUBLISH = :publish

    # The event that transitions a resource to the 'unpublished' state.
    UNPUBLISH = :unpublish

    # The event that transitions a resource to the 'previewable' state.
    PREVIEW = :preview

    # The event that transitions a resource to the 'nonpublishable' state.
    MAKE_NONPUBLISHABLE = :make_nonpublishable

    # Valid workflow events
    WORKFLOW_EVENTS = [PUBLISH, UNPUBLISH, PREVIEW, MAKE_NONPUBLISHABLE].freeze

    # A dry-types constant for coercion/validation of workflow_state values
    WorkflowState = Types::Coercible::String.default(DEFAULT_WORKFLOW_STATE).enum(*WORKFLOW_STATES)

    # A dry-types for coercion/validation of workflow event values
    WorkflowEvent = Types::Coercible::Symbol.enum(*WORKFLOW_EVENTS)
  end
end
