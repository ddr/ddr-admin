module Ddr::V3
  #
  # This module contains a storage service class for each file field (:content, :thumbnail, etc.)
  #
  # Generally, it should not be used directly; instead, use `FileUploadService[file_field]`.
  #
  # @see FileUploadService
  # @api private
  module FileStorageService
    extend FileFieldSpecializationMixin

    specialize_on_file_field(AbstractService) do
      # @!attribute file [rw] the file to upload
      #   This is a duck type for a "file-like" object, such as an instance of Tempfile.
      #   @return [#path, #size]
      attribute :file, Types::FileLike.constrained(min_size: 1)

      # @!attribute original_filename [rw] the original filename of the file.
      #   @return [String]
      attribute? :original_filename, Types::OriginalFilename

      # @!attribute media_type [rw] the media type of the file.
      #   Falls back to the content type of the file, if not provided.
      #   @return [String]
      # attribute? :media_type, Types::MediaType.optional

      # @!attribute digest [rw] the digest of the file.
      #   Used for validation.
      #   @return [Ddr::Digest]
      attribute? :digest, Ddr::Digest

      after_initialize do
        # Falls back to the basename of the file path, if original_filename not provided.
        self.original_filename ||= file
      end

      def run
        storage_adapter.upload(**service_attributes, file_field:)
      end

      def storage_adapter
        Ddr::Storage.adapter_for(file_field)
      end
    end
  end
end
