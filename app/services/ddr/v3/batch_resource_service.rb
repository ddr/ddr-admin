module Ddr::V3
  #
  # This service runs a bulk service in batches.
  #
  class BatchResourceService < AbstractService
    # @!attribute default_batch_size [rw] the default batch size in batch mode
    #   @return [Integer] default: 100
    class_attribute :default_batch_size, default: 100

    # @!attribute batch_size [rw] the batch size in batch mode
    #   @return [Integer] defaults to default_batch_size.
    attribute :batch_size, Types::Coercible::Integer.default { default_batch_size }

    # @!attribute bulk_service [rw] the bulk service to run in batches
    #   @return [Class]
    attribute :bulk_service, Types.Instance(Class)

    # @!attribute bulk_attributes [rw] the attributes for the bulk service
    #   @return [Hash]
    attribute :bulk_attributes, Types::Hash

    # Don't retry batch services by default.
    self.job_class = NoRetryServiceJob

    # Runs the bulk service in batches.
    # @return [Array] the results of the bulk service for each batch
    def run
      batches.map { |batch| bulk_service.call(batch_attributes(batch)) }
    end

    # Splits the resource IDs into batches.
    # @return [Enumerator<Array<String>>] the resource IDs split into batches
    def batches
      bulk_attributes.fetch(:resource_ids).each_slice(batch_size)
    end

    # @param batch [Array<String>] the resource IDs in the batch
    # @return [Hash] the attributes for the batch service
    def batch_attributes(batch)
      bulk_attributes.merge(resource_ids: batch, dependent: true)
    end
  end
end
