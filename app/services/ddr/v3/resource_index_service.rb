module Ddr::V3
  class ResourceIndexService < ResourceService
    delegate :persister, to: :index
    delegate :save, :save_all, to: :persister

    def run
      save(resource:)
    end

    define_bulk_service do |config|
      config.run do
        resource_service.index.persister.save_all(resources:)
      end
    end
  end
end
