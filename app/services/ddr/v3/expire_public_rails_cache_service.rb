module Ddr::V3
  #
  # Service to expire the public Rails cache.
  #
  class ExpirePublicRailsCacheService < AbstractService
    def run
      k8s_client.create_resource(k8s_resource)
    end

    # @return [Hash] the Kubernetes Job template
    def template
      {
        apiVersion: 'batch/v1',
        kind: 'Job',
        metadata: {
          name: "expire-rails-cache-#{id}",
          namespace: ENV.fetch('DDR_PUBLIC_K8S_NAMESPACE'),
          labels: {
            'app.kubernetes.io/managed-by': 'ddr-admin'
          }
        },
        spec: {
          completions: 1,
          template: {
            spec: {
              containers: [
                {
                  name: 'expire-rails-cache',
                  image: 'app:latest',
                  command: %w[memcflush --servers rails-cache]
                }
              ],
              restartPolicy: 'Never'
            }
          }
        }
      }
    end

    # @return [K8s::Resource] the Kubernetes Job resource
    def k8s_resource
      K8s::Resource.new(template)
    end

    private

    # @return [K8s::Client] the Kubernetes client
    def k8s_client
      K8s::Client.config(k8s_config)
    end

    # @return [K8s::Config] the Kubernetes configuration
    def k8s_config
      K8s::Config.load_file('~/.kube/config')
    end
  end
end
