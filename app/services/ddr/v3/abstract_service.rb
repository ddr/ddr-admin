module Ddr::V3
  #
  # == Ddr::V3::AbstractService -- Abstract superclass for DDR V3 services.
  #
  # A service ideally does "one thing", define by its #run instance method,
  # which each concrete service class must define. Some services call other services,
  # usually in callbacks (see below).
  #
  # Services are designed to be run in one of two ways:
  #
  # - Calling the class method `.call`
  # - Instantiating and calling `#run!` on the service instance.
  #
  # The `#run` method is considered private and is not intended to be called directly.
  #
  # The value returned by #run is stored in the `result` attribute of the service instance,
  # and is returned by `#run!`.
  #
  # Services are designed to be run just once.  If a service instance is run more than once,
  # it will skip the second and subsequent runs.  This is accomplished by setting
  # the `new_record` attribute to false after the first run (even if skipped).
  #
  # === Callbacks
  #
  # ActiveModel callbacks are defined on :initialize, :run, and :finalize.
  # See ActiveModel or Rails documentation for details on how these callbacks work.
  #
  # Actions that should be triggered by a successful run of the service
  # can be called with an `after_run` or `before_finalize` callback.
  #
  # === Skipping and Finalizing
  #
  # ==== #skip!
  #
  # The `#skip!` method is used, often in a `before_run` callback to make the
  # service a "no-op" for that instance.  It's a way of limiting how much other
  # code has to know about the service, and is alternative to raising an exception
  # when the service is not valid (but not fatal) or should otherwise not be run
  # in that instance.
  #
  # An example might be running an image derivative service on a resource that
  # is not an image. Instead of raising an exception, the service can "complete"
  # by skipping itself and performing no action.
  #
  # ==== #finalize!
  #
  # In some instances, the service should not be run on the resource itself, but
  # it still may need to trigger other actions, such as on the children of a
  # parent resource. The `#finalize!` method will jump over `#run` to `#finalize`.
  #
  # A service may also supply code to be executed
  # by `#finalize` using the `.finalize` class method and a block to be executed
  # in the context of the service instance.
  #
  # === Asynchronous Execution
  #
  # Services can be run asynchronously by setting the `async` attribute to true.
  # This will enqueue the service in a background job and skip the normal run.
  #
  # The job class and queue can be set using the `job_class` and `job_queue` class attributes.
  # The default job class is Ddr::V3::ServiceJob.
  # For services that should not be retried, set the job class to Ddr::V3::NoRetryServiceJob.
  #
  # === Subclasses
  #
  # Other classes in this module with names ending in "Service" are subclasses
  # of this abstract superclass.
  #
  class AbstractService < Valkyrie::Resource
    extend ::ActiveModel::Callbacks

    define_model_callbacks :initialize, :run, :finalize

    # @!attribute dependent [rw]
    #   @return [Boolean] whether this is a dependent service call.
    attribute :dependent, Types::Bool.default(false)

    # @!attribute async [rw]
    #   @return [Boolean] whether to run the service asynchronously.
    attribute? :async, Types::Bool.default(false)

    # A block to call in the context of the service instance, providing post-run finalization
    # @!attribute finalization_proc [rw]
    #   @return [#call]
    class_attribute :finalization_proc

    # The class of the job to use for asynchronous execution
    # @!attribute job_class [rw]
    #   @return [Class]
    class_attribute :job_class, default: ServiceJob

    # The queue to use for the job
    # @!attribute job_queue [rw]
    #   @return [Symbol]
    class_attribute :job_queue, default: :default

    # @!attribute result [r] The return result of the service's `run` method.
    #   @return [void]
    attr_reader :result

    # Use the Rails logger
    delegate :logger, to: :Rails

    # Saves block given to finalization_proc class attribute.
    # @return [Proc]
    def self.finalize(&block)
      self.finalization_proc = block
    end

    # Don't re-run the service, if it's already been run.
    before_run :skip!, unless: :new_record

    # Run the service asynchronously, if requested.
    before_run :perform_async, if: :async

    # Define the service action.
    # @yield the block to define the service action
    # @return [Symbol] :run
    def self.run(&)
      define_method(:run, &)
    end

    # The name to use for instrumation events.
    # @example
    #   Ddr::V3::PublicationService::Publish.instrumentation_event_name => "publish.publication_service.v3.ddr"
    def self.instrumentation_event_name
      @instrumentation_event_name ||= self.name.split('::').reverse.map(&:underscore).join('.')
    end

    # Service classes are callable.
    def self.call(...)
      new(...).run!
    end

    # Initialization is overridden to provided initialize_* callbacks
    def initialize(...)
      run_callbacks(:initialize) { super }
    end

    # Run service with callbacks.
    # Calls the api-private #run method unless @skip is truthy.
    # Calls #finalize if defined, after *_run callbacks.
    # Returns the result of #run, or nil, if skipped.
    # @return [void]
    def run!
      catch(:skip) do
        catch(:finalize) do
          run_callbacks(:run) do
            @result = run
          end
        end

        run_callbacks(:finalize) { finalize }
      end

      # Ensure that the service is marked as not new after running
      self.new_record = false

      # Return the result of the service, or nil if skipped
      result
    end

    def method_missing(name, *, &)
      raise NotImplementedError, 'Subclasses must implement #run.' if name == :run
      super
    end

    # Executes finalization proc, if defined
    # @return [void]
    def finalize
      return unless finalization_proc

      instance_exec(&finalization_proc)
    end

    # The explicitly defined attributes of the service
    # (i.e., excluding "reserved" attributes).
    # @return [Hash] the attributes of the service
    def service_attributes
      attributes.except(*self.class.reserved_attributes)
    end

    # Skips #run and #finalize and aborts the `run' callback chain.
    def skip!
      throw :skip
    end

    # Jumps directly to #finalize, skipping over #run.
    def finalize!
      throw :finalize
    end

    # @return [String]
    def inspect
      "#{self.class} #{service_attributes.inspect}"
    end

    # Is the service a dependent service?
    # @return [Boolean]
    def dependent?
      dependent
    end

    # Is the service a top-level service all -- i.e., not dependent
    # @return [Boolean]
    def top_level?
      !dependent?
    end

    # Enqueue the service in a background job and skip the normal run.
    # @return [void]
    # @api private
    def perform_async
      job.perform_later(service_name, job_attributes)

      skip!
    end

    # The job instance (wrapped by ActiveJob) to be enqueued.
    # @see https://api.rubyonrails.org/classes/ActiveJob/Core/ClassMethods.html#method-i-set
    def job
      job_class.set(queue: job_queue)
    end

    # The name of the service class.
    # @return [String]
    def service_name
      self.class.name
    end

    # @return [Hash] attributes to pass to the job instance.
    def job_attributes
      service_attributes.except(:async)
    end
  end
end
