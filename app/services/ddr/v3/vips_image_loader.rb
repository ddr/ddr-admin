module Ddr::V3
  #
  # Loads a source image file into Vips.
  #
  # @see Vips::Image
  #
  class VipsImageLoader
    # Loads a source image file into Vips.
    # @param path [String] the source path
    # @param media_type [String] the media type of the source file
    # @return [Vips::Image] the loaded image
    def self.call(path:, media_type:)
      load_method(media_type:).call(path)
    end

    # The Vips::Image load method, as determined by the file media type,
    # e.g., Vips::Image.tiffload, Vips::Image.jpegload, etc.
    # Falls back to Vips::Image.magickload
    # @param media_type [String] the media type
    # @return [Method]
    def self.load_method(media_type:)
      Vips::Image.method(:"#{media_type.split('/').last}load")
    rescue NameError
      Vips::Image.method(:magickload)
    end
  end
end
