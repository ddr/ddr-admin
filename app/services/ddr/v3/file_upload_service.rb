module Ddr::V3
  #
  # This module contains a class customized for each file field -- :content, :thumbnail, etc.
  #
  # The intended usage is:
  #   FileUploadService[file_field].call(**kwargs)
  #
  # @example
  #   FileUploadService[:content].call(resource:, file:, media_type: 'image/tiff', original_filename: 'foobar.tiff')
  #
  module FileUploadService
    extend FileFieldSpecializationMixin

    specialize_on_file_field(ResourceService) do
      self.change_set_type = FileChangeSet[file_field]

      # @!attribute file [rw] the file to upload
      #   This is a duck type for a "file-like" object, such as an instance of Tempfile.
      #   @return [#path, #size]
      attribute :file, Types::FileLike.constrained(min_size: 1)

      # @!attribute original_filename [rw] the original filename of the file.
      #   @return [String]
      attribute? :original_filename, Types::OriginalFilename

      # @!attribute media_type [rw] the media type of the file.
      #   @return [String]
      attribute? :media_type, Types::MediaType.optional

      # @!attribute digest [rw] the digest of the file.
      #   Used for validation.
      #   @return [Ddr::Digest]
      attribute? :digest, Ddr::Digest

      delegate :update_file, to: :change_set

      # @!attribute existing_file [r] the existing file, if any.
      #   @return [Ddr::File]
      attr_reader :existing_file

      after_initialize do
        # Scan for malware before any other action
        scan_for_malware!

        # Record existing file for tracking purposes
        @existing_file = resource.send(file_field)

        # original_filename falls back to the basename of the file path.
        self.original_filename ||= file

        # media_type falls back to the content type of the file or the result of file magic.
        self.media_type ||= file.try(:content_type) || Types::FileMagic[file]

        # Calculate a file digest up front and compare it to the provided one, if present.
        validate_file_digest! if digest

        # If the digest is not provided, calculate it.
        self.digest ||= calculate_file_digest
      end

      before_run :skip!, if: :matches_existing_file_digest?

      after_run do
        case file_field
        when :content
          capture_event(summary: "Content file uploaded")
          update_image_derivatives
          update_fits_file
        when :intermediate_file
          update_image_derivatives
        end
      end

      after_finalize :track_existing_file, if: :existing_file

      run do
        update_file(new_file)
      end

      # @raise [Ddr::ChecksumInvalid] if the file digest does not match the provided digest
      # @return [void]
      # @api private
      def validate_file_digest!
        calculated_digest = calculate_file_digest(type: digest.type)

        return if digest == calculated_digest

        raise Ddr::ChecksumInvalid, "Expected #{digest.inspect} / Calculated: #{calculated_digest.inspect}"
      end

      # Calculate a digest (SHA1 by default) for the file
      # @api private
      # @return [Ddr:Digest]
      # @param type [String] the digest type
      def calculate_file_digest(type: 'SHA1')
        Ddr::Digest.new(type:, value: Digest(type).file(file.path).to_s)
      end

      # Perform a malware scan of file.
      # N.B. Large files are *not* scanned due to limitations of ClamAV.
      # @api private
      # @raise [Ddr::Antivirus::VirusFoundError] if malware detected
      # @return [void]
      def scan_for_malware!
        AntivirusService.call(file:)
      end

      # Tracks existing (i.e., replaced) file, if present in the deleted files table.
      # @return [Boolean, nil]
      # @api private
      def track_existing_file
        return unless existing_file
        Ddr::DeletedFile.create(repo_id: resource.id_s, file_id: file_field.to_s, path: existing_file.file_path)
      end

      # Does the existing file (if present) digest match the submitted digest, if given?
      # @return [Boolean]
      def matches_existing_file_digest?
        return false unless existing_file
        return true if existing_file.digest.find { |d| d == digest }
        false
      end

      # @return [Ddr::File] the new file
      # @api private
      def new_file
        stored_file = file_storage_service.call(file:, original_filename:, digest:)

        created_at = updated_at = DateTime.now

        Ddr::File.new(file_identifier: stored_file.id,
                      media_type:,
                      original_filename:,
                      digest:,
                      created_at:,
                      updated_at:,
                      new_record: false)
      end

      # @return [Class] the file storage service
      # @api private
      def file_storage_service
        FileStorageService[file_field]
      end
    end
  end
end
