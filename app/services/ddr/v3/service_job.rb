module Ddr::V3
  #
  # This is generic job class for running services asynchronously.
  # It receives the service class name and arguments and runs the service,
  # ensuring that the service is run synchronously.
  #
  # The intended use is call the desired service with the `async: true` option,
  # and not to reference this class directly.
  #
  # @see AbstractService
  # @api private
  class ServiceJob < ::ApplicationJob
    queue_as :default

    def perform(service_name, attrs)
      service_name.constantize.call attrs.merge(async: false)
    end
  end
end
