module Ddr::V3
  #
  # Deletes a resource from the repository.
  #
  # Notes:
  # - Published resources cannot be deleted (must unpublish first).
  # - Resource 'descendants' (children, attachments, targets) are deleted before the resource itself.
  # - Files from the deleted resource are tracked by `Ddr::DeletedFile`.
  #
  class ResourceDeletionService < ResourceService
    self.change_set_type = DeletionChangeSet
    self.event_type = Ddr::Events::DeletionEvent
    self.job_class = NoRetryServiceJob
    self.job_queue = :low_priority

    run do
      change_set.delete
    end

    before_run do
      # Cannot delete a published resource!
      skip! if published?

      # Process descendants first
      delete_descendants
    end

    after_run do
      capture_event(detail: json_resource)
      track_deleted_files

      if top_level? && parent?
        update_parent_structure
        update_parent_iiif
      end

      delete_ark
    end

    def track_deleted_files
      Ddr::DeletedFile.create(deleted_files)
    end

    def delete_descendants
      %i[children targets attachments].each do |relation|
        next unless resource.respond_to?(relation)

        resource.send(relation).each do |descendant|
          self.class.call(resource: descendant, dependent: true)
        end
      end
    end

    def deleted_files
      resource.attached_files.map do |file_id, ddr_file|
        { repo_id: resource.id_s, file_id:, path: ddr_file.file_path }
      end
    end

    define_bulk_service do |config|
      config.track_parents = true
      config.track_exceptions = true

      config.after_run do
        FileUpdateService[:struct_metadata].bulk(resource_ids: parent_ids.to_a, dependent: true)

        FileUpdateService[:iiif_file].bulk(resource_ids: parents.select(&:published?).map(&:id_s), dependent: true)

        raise_exceptions!
      end
    end
  end
end
