# frozen_string_literal: true

module Ddr::V3
  #
  # This module provides objects related to managing the publication workflow of
  # resource in the DDR.
  #
  module PublicationService
    include PublicationConstants

    # A convenience method for accessing concrete service classes for publication workflow events
    # @example Ddr::V3::PublicationService[:publish] => Ddr::V3::PublicationService::Publish
    def self.[](workflow_event)
      const_get workflow_event.to_s.camelize
    end

    # A list of the concrete service classes for publication workflow events.
    def self.services
      @services ||= WORKFLOW_EVENTS.map { |workflow_event| self[workflow_event] }
    end

    # e.g., Ddr::V3::PublicationService.publish(resource)
    WORKFLOW_EVENTS.each do |event|
      define_singleton_method(event) do |resource|
        self[event].call(resource:)
      end
    end

    # A convenience method for instantiating a publication service for a workflow events
    # or target workflow state.
    # Either an :event or a :state keyword argument is required.
    # Other keyword arguments are passed to the concrete service initializer.
    # @return [Ddr::V3::PublicationEventService] the service
    # @raise [ArgumentError] Invalid or missing :event or :state keyword argument.
    def self.new(**args)
      if args.key?(:event)
        event = WorkflowEvent[args.delete(:event)]

        return self[event].new(**args)
      end

      if args.key?(:state)
        state = WorkflowState[args.delete(:state)]
        service = services.find { |svc| svc.transitions_to == state }

        return service.new(**args)
      end

      raise ArgumentError, "A valid 'event' or 'state' keyword argument is required."

    rescue Dry::Types::ConstraintError => e
      raise ArgumentError, e
    end

    # Is the publication event or state transition valid for the resource?
    # @return [Boolean]
    # @see .new
    # @see Ddr::V3::PublicationEventService#can?
    def self.can?(...)
      new(...).can?
    end

    # Call the publication service event or state transition.
    # @see .new
    def self.call(...)
      new(...).run!
    end

    # @deprecated Use Ddr::V3::PublicationService::WORKFLOW_STATES
    def self.workflow_state_names = WORKFLOW_STATES

    # @deprecated Use Ddr::V3::PublicationService::WORKFLOW_EVENTS
    def self.workflow_event_names = WORKFLOW_EVENTS

    # @deprecated Use Ddr::V3::PublicationService.call(**attrs)
    def self.transition_to(**attrs)
      call(**attrs)
    end
  end
end

Dir[File.join(__dir__, 'publication', '*.rb')].each { |lib| require(lib) }
