module Ddr::V3
  #
  # This abstract service is designed to support turning a service that operates on a single resource
  # into a bulk service that iterates over the resources and applies the 'dependent service' to each resource.
  #
  # === Batch mode
  #
  # A bulk service can be run in "batch mode" by using the `.batch` class method with the bulk service arguments.
  # In batch mode, the resource IDs are split into batches and the service is called (in normal mode)
  # on each batch of IDs.
  #
  # @abstract
  #
  class BulkResourceService < AbstractService
    # The resource service to be used by the bulk service.
    # @return [Class] the resource service class
    def self.resource_service
      raise 'Resource service not defined' if @resource_service.nil?

      @resource_service.is_a?(String) ? @resource_service.constantize : @resource_service
    end

    # Sets the resource service to be used by the bulk service.
    # A string can be used to avoid eager loading issues.
    # @param service [String, Class] the resource service class or its name
    # @return [String, Class] the resource service class or its name
    def self.resource_service=(service)
      raise 'Resource service is already defined' unless @resource_service.nil?

      @resource_service = (Types::Strict::String | Types.Instance(Class))[service]

      @resource_service
    end

    delegate :resource_service, to: :class

    delegate :db_query, :index_query, to: :resource_service

    # The batch service to be used by the bulk service.
    # @!attribute batch_service [rw] the batch service class
    #   @return [Class]
    class_attribute :batch_service, default: BatchResourceService

    # @!attribute batch_options [rw] the options to pass to the batch service
    #   @return [Hash]
    class_attribute :batch_options, default: {}.freeze

    # Don't retry bulk services by default.
    self.job_class = NoRetryServiceJob

    # @!attribute track_parents [rw] flag to track resource parent IDs for later processing
    #   @return [Boolean] default: false
    class_attribute :track_parents, default: false

    # @!attribute track_exceptions [rw] flag to catch per-resource exceptions to raise later
    #   @return [Booleab] default: false
    class_attribute :track_exceptions, default: false

    # @!attribute resource_ids [rw] the resource IDs to process in this bulk service
    #   @return [Array<String>]
    attribute :resource_ids, Types::Set.of(Types::ResourceID)

    # @!attribute resource_options [rw] the options to pass to the resource service
    #   @return [Hash]
    attribute :resource_options, Types::Hash.default({}.freeze)

    # Runs the bulk service in batch mode.
    # @param attrs [Hash] the bulk service attributes
    # @return [void] the result of the batch service call
    def self.batch(**attrs)
      async = attrs.delete(:async) || false # apply the async option to the batch service, and not each batch.

      batch_service.call(bulk_service: self, bulk_attributes: attrs, async:, **batch_options)
    end

    # Runs the bulk service in normal mode -- i.e., synchronously on each resource.
    # @return [Array] the results of the dependent service calls
    def run
      resources.map { |resource| each_resource(resource:) }
    end

    # Runs the resource service for the current resource (normal mode).
    # @param resource [Ddr::Resource] the resource to process
    # @return [void] the result of the dependent service call
    def each_resource(resource:)
      resource_service.call(resource_options.merge(resource:, dependent: true)).tap do |result|
        track_parent(resource:) if track_parents
      end

    rescue Exception => e
      raise unless track_exceptions

      track_exception(resource:, exception: e)
    end

    # @return [Array<Ddr::Resource>]
    # @api private
    def resources
      @resources ||= Ddr.query_service.find_many_by_ids(ids: resource_ids)
    end

    # Tracks the parent ID of the resource for later processing (normal mode).
    # @param resource [Ddr::Resource] the resource
    # @return [void]
    # @api private
    def track_parent(resource:)
      if parent_id = resource.try(:parent_id)
        parent_ids << parent_id.to_s
      end
    end

    # Tracks an exception for later raising (normal mode).
    # @param exception [Exception] the exception
    # @param resource [Ddr::Resource] the resource
    # @return [void]
    # @api private
    def track_exception(exception:, resource:)
      logger.error(exception)

      exceptions[resource.id_s] = exception.message
    end

    # @!attribute exceptions [r] the exceptions caught during processing
    #   @return [Hash<String, String>] a map of resource IDs to exception messages
    def exceptions
      @exceptions ||= {}
    end

    # @return [Boolean] true if exceptions were caught
    # @api private
    def exceptions?
      exceptions.any?
    end

    # Raises exceptions caught during processing (normal mode).
    # @raise [Ddr::Error] if exceptions were caught
    # @return [void]
    # @api private
    def raise_exceptions!
      return unless exceptions?

      raise Ddr::Error, "#{exceptions.length} exceptions were caught: #{exceptions.inspect}"
    end

    # @return [Set<String>] the parent IDs of the resources processed
    # @api private
    def parent_ids
      @parent_ids ||= Set.new
    end

    # @return [Array<Ddr::Resource>] the parent resources of the resources processed, if tracked.
    # @api private
    def parents
      @parents ||= Ddr.query_service.find_many_by_ids(ids: parent_ids)
    end

    # @return [Boolean] true if any parent resources were tracked
    # @api private
    def parents?
      parents.any?
    end
  end
end
