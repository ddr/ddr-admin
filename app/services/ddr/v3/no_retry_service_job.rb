module Ddr::V3
  #
  # A service job that does not retry on failure.
  # @see ServiceJob
  # @api private
  class NoRetryServiceJob < ServiceJob
    sidekiq_options retry: 0
  end
end
