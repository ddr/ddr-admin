module Ddr
  class FileMigrationService
    attr_reader :source_adapter, :destination_adapter

    delegate :logger, to: :Rails

    def initialize(source:, destination:)
      raise ArgumentError, 'source and destination adapters must be different' if source == destination

      @source_adapter = Valkyrie::StorageAdapter.find(source)
      @destination_adapter = Valkyrie::StorageAdapter.find(destination)
    end

    def migrate!(resource:)
      files = files_to_migrate(resource:)

      if files.empty?
        logger.info('FileMigrationService') { "No files to migrate for resource #{resource.id}" }
        return
      end

      logger.info('FileMigrationService') { "Starting migration of files for resource #{resource.id}." }

      change_set = Ddr::ResourceChangeSet.change_set_for(resource)

      files.each do |file_field, file|
        digest = file.digest.first.to_h.slice(:type, :value)

        file.io do |io|
          destination_adapter.upload(file: io, original_filename: file.original_filename, resource:,
                                     file_field:, digest:)
        end

        logger.info('FileMigrationService') do
          "Resource #{resource.id} file '#{file_field}' uploaded to destination: #{dest_file.id}"
        end

        new_file = file.dup
        new_file.file_identifier = dest_file.id
        new_file.new_record = false

        change_set.send(:"#{file_field}=", new_file)
      end

      #
      # We're bypassing Ddr::ResourceChangeSetPersister b/c of auto-actions
      #
      change_set.sync
      Ddr.persister.save(resource: change_set.resource)

      logger.info('FileMigrationService') { "Completed migration of files for resource #{resource.id}." }
    end

    # @return [Array<Ddr::File>]
    def files_to_migrate(resource:)
      resource.attached_files.select { |_, f| f.storage_adapter == source_adapter }
    end
  end
end
