module Ddr
  # @deprecated
  class DeletedResource
    def self.call(*)
      event = ActiveSupport::Notifications::Event.new(*)
      payload = event.payload
      repo_id = payload[:resource_id]
      payload[:file_profiles].each do |file_profile|
        DeletedDdrFile.record(repo_id, file_profile)
      end
    end
  end
end
