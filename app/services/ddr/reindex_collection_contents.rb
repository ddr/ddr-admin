module Ddr
  class ReindexCollectionContents
    TRIGGER_ON_ATTRIBUTES_CHANGED = %w[access_role admin_set title]

    def self.call(*args)
      return handle_notification(*args) if args.size > 1

      collection_or_id = args.first
      validate! collection_or_id

      collection_id = collection_or_id.respond_to?(:id) ? collection_or_id.id : collection_or_id
      resource_ids = query(collection_id).raw.map { |doc| doc['id'] }
      Ddr::IndexService.call(resource_ids)
    end

    def self.query(collection_id)
      Ddr::API::IndexRepository.query(
        q: "admin_policy_id_ssi:id-#{collection_id}",
        fq: '-common_model_name_ssi:Collection',
        fl: 'id',
        rows: 1_000_000
      )
    end

    def self.handle_notification(*)
      event = ActiveSupport::Notifications::Event.new(*)
      return unless (event.payload[:attributes_changed] & TRIGGER_ON_ATTRIBUTES_CHANGED).present?

      call event.payload[:resource_id]
    end

    def self.validate!(collection_or_id)
      case collection_or_id
      when Ddr::Collection
        if collection_or_id.new_record?
          raise ArgumentError,
                "Collection is not persisted: #{collection_or_id.inspect}."
        end
      when Valkyrie::ID
        Ddr.query_service.find_by(id: collection_or_id)
      when String
        Ddr.query_service.find_by(id: collection_or_id)
      else
        raise TypeError, 'Argument must be a Ddr::Collection, Valkyrie::ID, or Valkyrie::ID string.'
      end
    end
  end
end
