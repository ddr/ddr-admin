module Ddr
  class SetDefaultStructure
    attr_reader :job_class, :resource

    def self.call(*)
      return false unless Ddr::Admin.auto_update_structures

      event = ActiveSupport::Notifications::Event.new(*)
      payload = event.payload
      return false if payload[:skip_structure_updates]

      service = SetDefaultStructure.new(payload[:resource_id])
      service.enqueue_default_structure_job
    end

    def initialize(repo_id)
      @resource = Ddr.query_service.find_by(id: repo_id)
      @job_class = GenerateDefaultStructureJob
    end

    def enqueue_default_structure_job
      return unless default_structure_needed?

      job_class.perform_later(resource.id.id)
    end

    def default_structure_needed?
      if resource.can_have_struct_metadata?
        if resource.has_struct_metadata?
          if resource.structure.repository_maintained?
            true
          else
            false
          end
        else
          true
        end
      else
        false
      end
    end
  end
end
