module Ddr
  class RoleService
    # @private
    def self.find_resources(agent:)
      Ddr::API::ResourceRepository.find_by_role(agent:).tap do |resources|
        Rails.logger.warn "#{name} -- No resources found having roles assigned to #{agent}." if resources.size == 0
      end
    end

    # Revokes all roles assigned to agent
    # @param agent [String] agent whose roles will be revoked
    def self.revoke_all(agent:)
      find_resources(agent:).each { |resource| revoke(agent:, resource:) }
    end

    # Revokes roles assigned to agent on resource (if any)
    # @param agent [String] agent whose roles will be revoked
    # @param resource [Ddr::Resource] resource on which roles will be revoked
    # @return [Array<Ddr::Auth::Roles::Role>] roles that were revoked
    def self.revoke(agent:, resource:)
      resource.roles.select { |role| role.agent == agent }.tap do |revoked|
        change_set = Ddr::ResourceChangeSet.change_set_for(resource)
        change_set.roles = resource.roles - revoked
        change_set.summary = "Revoked role(s) for #{agent}"
        change_set.detail = "Revoked role(s): #{revoked}"
        # Workaround for a bug that triggers structural metadata re-generation incorrectly.
        change_set.skip_structure_updates = true
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        Rails.logger.info "#{name} -- Revoked roles assigned to #{agent} on resource #{resource.id}"
      end
    end

    # Reassigns all roles assigned to from_agent to to_agent
    # @param from_agent [String] agent from whom roles will be reassigned
    # @param to_agent [String] agent to whom role will be reassinged
    def self.reassign_all(from_agent:, to_agent:)
      if from_agent == to_agent
        Rails.logger.warn "#{name} -- Reassigning roles to an identical agent is a no-op (#{from_agent})."
      else
        find_resources(agent: from_agent).each do |resource|
          reassign(from_agent:, to_agent:, resource:)
        end
      end
    end

    # Reassigns roles assigned to from_agent on resource to to_agent
    # @param from_agent [String] agent from whom roles will be reassigned
    # @param to_agent [String] agent to whom role will be reassinged
    # @param resource [Ddr::Resource] resource on which roles will be reassigned
    # @return [Array<Ddr::Auth::Roles::Role>] roles that were reassigned
    def self.reassign(from_agent:, to_agent:, resource:)
      resource.roles.select { |role| role.agent == from_agent }.tap do |reassigned|
        change_set = Ddr::ResourceChangeSet.change_set_for(resource)
        updated = reassigned.dup.each { |role| role.agent = to_agent }
        # N.B. Since the access_role resource attribute is defined as a set,
        # roles are de-duped on persistence -- i.e., reassigning a role as done here
        # could duplicate an existing role, but it's not a concern.
        change_set.roles = resource.roles - reassigned + updated
        change_set.summary = "Reassigned role(s) from #{from_agent} to #{to_agent}"
        change_set.detail = "Reassigned role(s): #{reassigned}"
        # Workaround for a bug that triggers structural metadata re-generation incorrectly.
        change_set.skip_structure_updates = true
        Ddr::ResourceChangeSetPersister.new.save(change_set:)
        Rails.logger.info "#{name} -- Reassigned roles from #{from_agent} to #{to_agent} on resource #{resource.id}"
      end
    end
  end
end
