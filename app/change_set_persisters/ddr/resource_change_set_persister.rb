module Ddr
  class ResourceChangeSetPersister
    extend ActiveModel::Callbacks

    class WontSaveError < Ddr::Error; end

    # Lifecycle events - Ddr::Resource
    INGEST = 'ingest.ddr_resource'
    UPDATE = 'update.ddr_resource'
    DELETE = 'delete.ddr_resource'

    # Lifecycle events - Ddr::File
    DELETE_FILE = 'delete.ddr_file'

    attr_reader :metadata_adapter, :storage_adapter

    delegate :persister, :query_service, to: :metadata_adapter

    define_model_callbacks :create, :delete, :persist, :save

    around_save :notify_file_delete, if: :removed_files

    before_create :set_ingestion_date, unless: proc { change_set.ingestion_date }
    before_create :set_ingested_by, if: :performed_by, unless: proc { change_set.ingested_by }
    before_create :grant_default_roles

    around_persist :notify_update, unless: proc { change_set.resource.new_record? }

    around_create :assign_permanent_id!, if: :assign_permanent_id?

    after_create :set_admin_policy, if: proc { change_set.resource.internal_resource == 'Ddr::Collection' },
                                    unless: proc { change_set.admin_policy_id }
    after_create :notify_ingest

    after_delete :notify_delete

    def initialize(metadata_adapter: Ddr.metadata_adapter, storage_adapter: Ddr.storage_adapter)
      @metadata_adapter = metadata_adapter
      @storage_adapter = storage_adapter
    end

    # Raises Ddr::ResourceChangeSetPersister::WontSaveError (and doesn't perform the save) if change set is invalid.
    def save(change_set:)
      unless valid_change_set?(change_set)
        raise WontSaveError,
              I18n.t('ddr.change_set.wont_save',
                     id_or_new: change_set.resource.id&.id || 'NEW')
      end

      initialize_cache(change_set)
      run_callbacks(:save) do
        change_set.resource.new_record? ? create(change_set) : persist(change_set)
      end
    end

    def create(change_set)
      run_callbacks(:create) do
        persist(change_set)
      end
    end

    def persist(change_set)
      run_callbacks(:persist) do
        change_set.sync
        persisted_resource = persister.save(resource: change_set.resource).tap do |persisted_resource|
          cache.put(:modified_date, persisted_resource.updated_at)
          cache.put(:resource_id, persisted_resource.id)
        end
        create_virus_check_event(change_set, persisted_resource.id)
        persisted_resource
      end
    end

    def save_all(change_sets:)
      change_sets.each do |change_set|
        save(change_set:)
      end
    end

    # 'destroy' is essentially an alias for 'delete' but couldn't use 'alias_method' because of the paraemeter
    def destroy(change_set:)
      delete(change_set:)
    end

    def delete(change_set:)
      initialize_cache(change_set)
      run_callbacks(:delete) do
        remove(change_set:)
      end
    end

    def remove(change_set:)
      persister.delete(resource: change_set.resource)
    end

    def cache
      @__cache ||= Cache.new
    end

    def create_virus_check_event(change_set, resource_id)
      return unless result = change_set.virus_scan_result

      Ddr::Events::VirusCheckEvent.create(result.merge(resource_id:))
    end

    private

    def valid_change_set?(change_set)
      if change_set.valid?
        true
      else
        id_or_new = change_set.resource.id&.id || 'NEW'
        Rails.logger.error(I18n.t('ddr.change_set.wont_save', id_or_new:))
        Rails.logger.error(I18n.t('ddr.change_set.error', id_or_new:, error: change_set.errors.messages))
        false
      end
    end

    def initialize_cache(change_set)
      cache.clear
      cache.put(:change_set, change_set)
      cache.put(:existing_files, change_set.resource.attached_files_having_content)
      cache.put(:resource_id, change_set.resource.id)
    end

    def change_set
      cache[:change_set]
    end

    def performed_by
      return unless user = change_set.user

      user.to_s
    end

    def set_admin_policy
      resource = Ddr.query_service.find_by(id: cache[:resource_id])
      resource.admin_policy_id = resource.id
      Ddr.persister.save(resource:)
    end

    def set_ingested_by
      change_set.ingested_by = performed_by
    end

    def set_ingestion_date
      change_set.ingestion_date = Time.now.utc.iso8601
    end

    def grant_default_roles
      change_set.roles += change_set.default_roles
    end

    def assign_permanent_id?
      change_set.permanent_id.nil? && Ddr::Admin.auto_assign_permanent_id
    end

    def assign_permanent_id!
      yield
      PermanentId.assign!(cache[:resource_id], nil, change_set.skip_structure_updates)
    end

    def parent
      return unless change_set.respond_to?(:parent_id)

      change_set.parent_id
    end

    def default_notification_payload
      { resource_id: cache[:resource_id],
        model: change_set.internal_resource,
        user_key: change_set.user&.to_s,
        permanent_id: change_set.permanent_id,
        parent:,
        comment: change_set.comment,
        detail: change_set.detail,
        skip_structure_updates: change_set.skip_structure_updates,
        skip_iiif_updates: change_set.skip_iiif_updates,
        summary: change_set.summary }
    end

    def notify_ingest
      payload = default_notification_payload.merge(
        event_date_time: change_set.ingestion_date,
        files_changed: change_set.resource.attached_files_having_content
      )
      ActiveSupport::Notifications.instrument(INGEST, payload)
    end

    def notify_update
      Rails.logger.debug do
        "Notifying update for #{change_set.resource.id}, changed: #{metadata_changed}, files: #{files_changed}"
      end
      event_params = default_notification_payload.merge(
        attributes_changed: metadata_changed,
        files_changed:,
        new_files:
      )
      ActiveSupport::Notifications.instrument(UPDATE, event_params) do |payload|
        yield
        payload[:event_date_time] = cache[:modified_date]
      end
    end

    def delete_notification_payload
      default_notification_payload.merge(
        file_profiles: change_set.resource.attached_files_having_content.map do |f|
                         file_profile(f,
                                      change_set.resource.send(f))
                       end,
        create_date: change_set.created_at,
        modified_date: change_set.updated_at
      )
    end

    def notify_delete
      ActiveSupport::Notifications.instrument(DELETE, delete_notification_payload)
    end

    def default_file_notification_payload
      { resource_id: cache[:resource_id] }
    end

    def notify_file_delete
      existing_file_profiles = cache[:existing_files].map { |f| file_profile(f, change_set.resource.send(f)) }
      yield
      (removed_files + updated_files).each do |f|
        ActiveSupport::Notifications.instrument(DELETE_FILE,
                                                default_file_notification_payload
                                                    .merge({ file_profile: existing_file_profiles.select do |p|
                                                                             p[:ddr_file_type] == f.to_sym
                                                                           end.first }))
      end
    end

    def file_profile(ddr_file_type, ddr_file)
      { ddr_file_type:, file_identifier: ddr_file.file_identifier, file_path: ddr_file.file_path }
    end

    def metadata_changed
      change_set.changed.keys & change_set.resource.class.metadata_fields.map(&:to_s)
    end

    # All files changed, whether added, updated, or deleted
    def files_changed
      change_set.changed.keys & change_set.resource.class::FILE_FIELDS.map(&:to_s)
    end

    # The subset of `files_changed` that didn't previously point to a file but now do
    def new_files
      files_changed - cache[:existing_files].map(&:to_s)
    end

    # The subset of `files_changed` that previously pointed to a file but now don't
    def removed_files
      changed_existing_files = files_changed & cache[:existing_files].map(&:to_s)
      changed_existing_files.select { |file| change_set.send(file.to_sym).nil? }
    end

    # The subset of `files_changed` that represent updates to existing files
    def updated_files
      files_changed - new_files - removed_files
    end
  end
end
