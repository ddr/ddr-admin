module Ddr
  module API
    class SchemaValidator
      attr_reader :schema, :options

      def initialize(schema, skip_readonly: false)
        @schema = schema
        @options = {}
        @options[:before_property_validation] = skip_readonly_property if skip_readonly
      end

      def validator
        @validator = ::JSONSchemer.schema(schema, **options)
      end

      def skip_readonly_property
        proc do |data, property, property_schema, _parent|
          data.delete(property) if property_schema['readOnly']
        end
      end

      def validate(data)
        errors = validator.validate(data.dup)

        errors.map { |error| ::JSONSchemer::Errors.pretty(error) }
      end

      def valid?(data)
        validator.valid?(data.dup)
      end
    end
  end
end
