module Ddr
  module API
    #
    # Wraps "raw" response from Solr.
    #
    class IndexResponse < SimpleDelegator
      def empty?
        num_found == 0
      end

      def results
        dig('response', 'docs')
      end

      def num_found
        dig('response', 'numFound')
      end

      def next_cursor
        self['nextCursorMark']
      end
    end
  end
end
