module Ddr
  module API
    class PremisXMLDocument
      XMLNS = 'http://www.loc.gov/premis/v3'
      SCHEMA = 'https://www.loc.gov/standards/premis/v3/premis-v3-0.xsd'
      VERSION = '3.0'

      def self.call
        Nokogiri::XML::Builder.new(encoding: 'UTF-8') do |xml|
          xml[:premis].premis(
            'xmlns:premis' => XMLNS,
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:schemaLocation' => "#{XMLNS} #{SCHEMA}",
            'version' => VERSION
          ) do
            yield xml if block_given?
          end
        end
      end

      def self.root
        call.doc.root
      end
    end
  end
end
