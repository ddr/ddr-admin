module Ddr
  module API
    class RemoteUserAuthMiddleware < Grape::Middleware::Auth::Base
      HEADERS = %w[HTTP_REMOTE_USER REMOTE_USER]

      def initialize(app, *_, &authenticator)
        super(app, {})

        @authenticator = authenticator
      end

      def call(env)
        if @authenticator.call(remote_user(env))
          app.call(env)
        else
          throw(:error, status: 401, message: 'Authorization Failed.')
        end
      end

      def remote_user(env)
        env.slice(*HEADERS).values.first
      end
    end
  end
end
