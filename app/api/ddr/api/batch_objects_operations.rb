module Ddr
  module API
    module BatchObjectsOperations
      extend Grape::API::Helpers

      include Pagination
      include CSVHelper

      def get_batch_objects
        records = "Ddr::Batch::#{params[:type]}BatchObject".constantize.all

        filters = params.slice(:resource_id, :handled, :processed, :validated, :verified)
        filters[:model] = FullyQualifiedModel.fq_model(params[:model]) if params[:model]

        records = records.where(filters) if filters.present?

        if csv?
          filename = 'DDR-Batch-Objects-%s.csv' % csv_timestamp
          stream_csv(records:, entity_class: BatchObjectEntity, filename:)
        else
          present paginate(records), with: BatchObjectEntity
        end
      end

      def get_batch_object
        batch_object = Ddr::Batch::BatchObject.find(params[:id])
        present batch_object, with: BatchObjectEntity, batch_object_details: true
      end
    end
  end
end
