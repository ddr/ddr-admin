module Ddr
  module API
    class DeepFreeze
      def self.call(obj)
        case obj

        when Hash
          DeepFreeze.call(obj.values)
          obj.freeze

        when Enumerable
          obj.each { |v| DeepFreeze.call(v) }
          obj.freeze

        else
          obj.freeze
        end
      end
    end
  end
end
