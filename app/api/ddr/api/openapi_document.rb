module Ddr
  module API
    class OpenAPIDocument < ::Hash
      include Singleton

      def initialize
        super

        erb = ERB.new ::File.read(Rails.root.join('config/api/openapi.yaml'))
        merge! YAML.load(erb.result)

        DeepFreeze.call(self)
      end

      def valid?
        JSONSchemer.openapi(self).valid?
      end
    end
  end
end
