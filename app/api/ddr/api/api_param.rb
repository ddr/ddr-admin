module Ddr
  module API
    class APIParam
      attr_reader :name, :schema, :oapi_param, :skip_deprecated, :skip_read_only
      attr_accessor :type, :required

      # @param name [String] the param name
      # @param schema [Hash] schema object for defining the Grape param
      # @param oapi_param [Hash] an OpenAPI parameter object for defining the Grape param
      # @param skip_deprecated [Boolean] whether to skip deprecated nested fields
      # @param skip_read_only [Boolean] whether to skip readOnly nested fields
      # @raise [RuntimeError] if name or schema are missing
      def initialize(name: nil, schema: nil, oapi_param: nil, skip_deprecated: false, skip_read_only: false)
        @oapi_param = oapi_param.dup
        @oapi_param = RefResolver.call(ref: @oapi_param['$ref']) if @oapi_param&.key?('$ref')

        @name = name || @oapi_param&.fetch('name') # raises KeyError

        raise 'name is missing' if @name.nil?

        @schema = schema.dup || @oapi_param&.fetch('schema') # raises KeyError

        raise 'schema is missing' if @schema.nil?

        @schema = SchemaMerger.call(*@schema['allOf']) if @schema.key?('allOf')
        @schema = RefResolver.call(ref: @schema['$ref']) if @schema.key?('$ref')

        @skip_deprecated = skip_deprecated
        @skip_read_only = skip_read_only

        @type = APIDataType.call(schema: @schema)

        @required = (@schema['required'] || @schema.dig('items',
                                                        'required') || []).include?(name) || @oapi_param&.fetch(
                                                          'required', false
                                                        )
      end

      def required?
        required
      end

      def allow_blank
        false
      end

      def options
        { as:, type:, desc:, default:, values:, allow_blank:, regexp:, coerce_with: }.compact
      end

      def regexp
        return nil unless string_type?

        STRING_FORMAT_REGEXES[format]
      end

      def as
        schema['x-original-name']&.to_sym
      end

      def coerce_with
        return nil unless oapi_param.present? && type == [String]
        return CSVParamSplitter if oapi_param.values_at('in', 'style', 'explode') == ['query', 'form', false]

        nil
      end

      def format
        schema['format'] || schema.dig('items', 'format')
      end

      def values
        return schema['enum'] if schema.key?('enum')

        if type.is_a?(Array) && items_enum = schema.dig('items', 'enum')
          return items_enum
        end

        if integer_type?
          min = schema['minimum'] || schema['exclusiveMinimum']
          min += 1 if schema.key?('exclusiveMinimum')

          max = schema['maximum'] || schema['exclusiveMaximum']

          # Integer range
          return Range.new(min, max, schema.key?('exclusiveMaximum')) if min || max
        end

        nil
      end

      def string_type?
        [String, [String]].include?(type)
      end

      def integer_type?
        [Integer, [Integer]].include?(type)
      end

      def default
        schema['default']
      end

      def desc
        schema['description'] || oapi_param&.fetch('description', nil)
      end

      # @return [String]
      def inspect
        opts = options.map { |k, v| "#{k}: #{v.inspect}" }.join(', ')
        "#{param_method} #{name.to_sym.inspect}, #{opts}"
      end

      # The Grape param method name (:requires or :optional) to use for this param
      # @return [Symbol]
      def param_method
        required? ? :requires : :optional
      end

      def nested_properties
        @nested_properties ||= schema['properties'] || schema.dig('items', 'properties') || {}
      end

      def nested_params
        return @nested_params if @nested_params

        return @nested_params = [] unless object_type? && nested_properties.present?

        properties = nested_properties.reject do |_name, prop|
          (skip_read_only && prop.fetch('readOnly', false)) || (skip_deprecated && prop.fetch('deprecated', false))
        end

        return @nested_params = [] unless properties.present?

        required = schema['required'] || schema.dig('items', 'required') || []

        @nested_params ||= properties.map do |name, prop|
          APIParam.new(schema: prop, name:, skip_read_only:, skip_deprecated:).tap do |nested_param|
            nested_param.required = required.include?(name)
          end
        end
      end

      # Is the param for an object type -- i.e., Hash, JSON -- ?
      # @return [Boolean]
      def object_type?
        [APIDataType::OBJECT_TYPE, [APIDataType::OBJECT_TYPE]].include?(type) # N.B. false if key missing
      end
    end
  end
end
