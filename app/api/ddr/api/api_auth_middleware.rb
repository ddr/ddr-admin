# frozen_string_literal: true

module Ddr
  module API
    class APIAuthMiddleware < Rack::Auth::AbstractHandler
      def call(env)
        #
        # If we're bypassing auth (in development) or already authn'd, we're done here.
        #
        if Ddr::API.noauth?
          warn '[API] Authentication bypassed by configuration.'
          return @app.call(env)
        end

        if env['warden'].authenticated?(:user)
          Rails.logger.debug('API') { "Authenticated user found in session: #{env['warden'].user}" }
          return @app.call(env)
        end

        #
        # Check for a valid bearer token
        #
        auth = APIAuthMiddleware::Request.new(env)

        unless auth.provided?
          Rails.logger.error('API') { 'Authentication credentials not provided.' } unless Rails.env.test?

          return unauthorized
        end

        unless auth.bearer?
          Rails.logger.error('API') { 'Authorization header does not contain a bearer token.' } unless Rails.env.test?

          return unauthorized
        end

        #
        # auth.params is the token string extracted from the authorization header
        # not including the 'Bearer' prefix.
        #
        # See Rack::Auth::AbstractRequest#params
        #
        info = OAuthRequest.call(:introspection_endpoint, token: auth.params)
        Rails.logger.debug('API') { "OpenID Connect introspection: #{info}" }

        unless info['active'] # token is expired
          Rails.logger.error('API') { 'Authentication token is expired.' } unless Rails.env.test?

          return unauthorized
        end

        #
        # @authenticator is the block set in Ddr::API::AbstractAPI#authenticate_user:
        #
        # auth :api do |username|
        #   <block>
        # end
        #
        user = @authenticator.call(info['user_id'])
        if user.nil? # user not found, no auto-create for API
          Rails.logger.error('API') { "User '#{info['user_id']}' not found." }
          return unauthorized
        end

        env['warden'].set_user(user, scope: :user) # Manually "log in" the user
        Rails.logger.debug('API') { "Authenticated user logged in: #{user}" }

        @app.call(env)
      end

      private

      def challenge
        'Bearer realm="DDR API"'
      end

      class Request < Rack::Auth::AbstractRequest
        def bearer?
          scheme == 'bearer' && params.present?
        end
      end
    end
  end
end
