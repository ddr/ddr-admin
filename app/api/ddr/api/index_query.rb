module Ddr
  module API
    class IndexQuery
      delegate :logger, to: :Rails

      delegate(*IndexQueryParams.attribute_names, to: :@params)

      # Blocks that perform conversions of results
      CONVERTERS = {
        resource: ->(doc) { IndexRepository.to_resource(object: doc) },
        solr: ->(doc) { ::SolrDocument.new(doc) },
        none: nil # i.e., no conversion
      }.freeze

      # with_<filter_name> methods -- e.g., with_effective_read_filter
      IndexFilters.singleton_methods.each do |filter_name|
        define_method :"with_#{filter_name}" do |*args, **kw|
          with(filter_name, *args, **kw)
        end
      end

      def initialize(params)
        set_params(params)
      end

      # Executes a 'select' query
      def select(...)
        logger.debug('API') { "IndexQuery: #{params}" }
        repository.select(...)
      end

      # Executes a 'paginate' request
      def paginate(...)
        logger.debug('API') { "IndexQuery: #{params}" }
        repository.paginate(...)
      end

      # Applies a sort citerion to the query
      # @param crit [String] the sort criterion
      # @return [Ddr::API::IndexQuery] self
      def sort_by(crit)
        new_sort = Array(sort) << crit
        merge_params(sort: new_sort)
      end

      # Apply a filter to the query
      # @param filter_name [Symbol] the filter name
      # @param filter_args [Array] filter arguments
      # @return [Ddr::API::IndexQuery] self
      # @see Ddr::API::IndexFilters
      def with(filter_name, *filter_args, **filter_kw)
        filter = IndexFilters.method(filter_name).call(*filter_args, **filter_kw)
        return self unless filter

        merge_params(fq: fq + [filter])
      end

      # Return the query parameters, excluding nil values, with symbolized keys
      # @return [Hash] the query parameters
      def params
        @params.to_h.compact
      end

      # Get an enumerator of the query results.
      # @note Query results can be enumerated by calling :each directly.
      #   This method is specifically to create an enumerator.
      # @return [Enumerator] the results
      def results
        to_enum
      end

      # Sets the converter to :none so that no coversion is done on the
      # raw results returned from Solr.
      # @note In the future, this may become the default behavior.
      # @return [Ddr::API::IndexQuery] the query
      def raw
        @converter = :none
        self
      end

      # @return [String]
      def csv
        select({ method: :get, params: params.merge(wt: 'csv', rows: count) })
      end

      # Sets the converter to :solr
      # @return [Ddr::API::IndexQuery] the query
      def docs
        @converter = :solr
        self
      end

      def resources
        @converter = :resource
        self
      end

      # Total number of results for the query
      # @return [Integer] the number of results
      def count
        response = select({ method: :post, data: params.merge(rows: 0) })
        response.dig('response', 'numFound')
      end

      alias size count
      alias length count

      def empty?
        count == 0
      end

      alias none? empty?

      # Does the query have at least one result?
      # @return [Boolean]
      def any?
        count > 0
      end

      # Does the query have one result?
      # @return [Boolean]
      def one?
        count == 1
      end

      # @return [Array<String>] the ids for entire result set
      def ids
        raw.merge_params(fl: 'id').map { |doc| doc['id'] }
      end

      # Return a page of results
      # @param num [Integer] the page number
      # @param items [Integer] the number of items per page
      # @return [Array<Ddr::Resource>] the results
      def page(num, items)
        response = paginate(num, items, 'select', { method: :post, data: params })
        convert(response.dig('response', 'docs'))
      end

      # Return only the first result
      # @return [Ddr::Resource] the result
      def one
        page(1, 1).first
      end

      alias first one

      # Query results paginated with a cursor
      # @see Enumerable
      # @see IndexCursor#each
      # @yield [Ddr::Resource, SolrDocument, Hash] a query result
      def each(...)
        IndexCursor.new(self).each(...)
      end

      def method_missing(name, *, &)
        return results.send(name, *, &) if Enumerator.public_instance_methods.include?(name)

        super
      end

      def respond_to_missing?(name, include_all)
        Enumerator.public_instance_methods.include?(name) || super
      end

      def repository
        IndexRepository
      end

      def converter
        # Set the default to resource conversion for backward-compat behavior.
        # In the future, we may want to require this to be explicit (via #resources)
        @converter ||= :resource
        CONVERTERS.fetch(@converter)
      end

      # @param raw_results [Array<Hash>] the raw results
      # @return [Array] the converted results, unless
      def convert(raw_results)
        return raw_results unless converter

        raw_results.map { |doc| converter.call(doc) }
      end

      def merge_params(new_params)
        set_params params.merge(new_params)
        self
      end

      private

      def set_params(params)
        @params = IndexQueryParams.new(params)
      end
    end
  end
end
