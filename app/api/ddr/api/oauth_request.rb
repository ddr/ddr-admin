module Ddr
  module API
    class OAuthRequest
      def self.call(endpoint, **params)
        response = Net::HTTP.start(OAuthConfig.host, use_ssl: true) do |http|
          request = Net::HTTP::Post.new URI(OAuthConfig[endpoint])
          request.basic_auth OAuthConfig.client_id, OAuthConfig.client_secret
          http.request request, URI.encode_www_form(params)
        end

        JSON.parse(response.body)
      end
    end
  end
end
