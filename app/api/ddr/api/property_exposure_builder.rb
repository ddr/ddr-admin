module Ddr
  module API
    #
    # Generates an "exposure" on a Grape::Entity subclass for an individual property,
    # and nested properties, if any.
    #
    class PropertyExposureBuilder
      # @param entity [Class] the Grape::Entity subclass
      # @param schema [Hash] the JSON Schema for the property
      # @param property [String] the property name
      # @param options [Hash] extra options for the `expose' method
      def self.call(entity:, schema:, property:, options: {})
        # ::Rails.logger.debug('API') { "Building exposure for #{entity.inspect} -- #{property.inspect}" }

        if schema.key?('$ref')
          definition = RefResolver.call(ref: schema['$ref'], doc: entity.schema)

          if definition['type'] == 'object'
            klass_name = EntityClassName.call(definition)

            return PropertyExposure.call(entity:, schema:, property:, options: options.merge(using: klass_name))
          end

          return PropertyExposure.call(entity:, schema: definition, property:, options:)
        end

        type, is_array = SchemaType.call(schema)
        # ::Rails.logger.debug('API') { "Schema type for #{property.inspect}: #{[type, is_array].inspect} #{schema.inspect}" }

        if type == 'object'
          return ObjectArrayPropertyExposure.call(entity:, schema:, property:, options:) if is_array

          return ObjectPropertyExposure.call(entity:, schema:, property:, options:)
        end

        # if (type == 'string') && !is_array &&
        #   options = options.merge(format_with: :to_s)
        # end

        PropertyExposure.call(entity:, schema:, property:, options:)
      end
    end
  end
end
