module Ddr
  module API
    #
    # Callable class for generating "exposures" on a Grape::Entity subclass
    # based on a JSON Schema hash.
    #
    # N.B. This does NOT promise to be a general purpose algorithm
    # for mapping schemas to exposures!
    #
    class ExposureBuilder
      # @param entity [Class] the Grape::Entity subclass
      # @param schema [Hash] the JSON Schema
      # @param options [Hash] extra options for the `expose' method
      # @param parent [String] name of parent property for nested exposures
      def self.call(entity:, schema:, options: {}, parent: nil)
        # ::Rails.logger.debug('API') do
        #   if parent
        #     exp_name = schema.fetch('x-original-name', parent)
        #     "Building nested exposures for #{entity.inspect} -- #{exp_name.inspect}"
        #   else
        #     "Building exposures for #{entity.inspect}"
        #   end
        # end

        # Get the top-level properties of the schema
        props = schema.fetch('properties').dup

        # Process except/only options to `build_exposures`.
        props.except!(*options[:except]) if options[:except].present? && options[:only].blank?
        props.slice!(*options[:only]) if options[:only].present?

        # Remove deprecated and writeOnly properties
        props.delete_if do |_, schema|
          schema.fetch('deprecated', false) || schema.fetch('writeOnly', false)
        end

        # Expose each remaining property
        opts = options.except(:except, :only).freeze
        props.each do |property, schema|
          PropertyExposureBuilder.call(entity:, schema:, property:, options: opts)
        end
      end
    end
  end
end
