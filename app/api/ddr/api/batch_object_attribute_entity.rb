module Ddr
  module API
    class BatchObjectAttributeEntity < Entity
      self.schema = Schema[:batch_object].dig('$defs', 'batch_object_attribute')

      build_exposures
    end
  end
end
