module Ddr
  module API
    module IndexOperations
      extend Grape::API::Helpers

      def get_index_schema
        IndexRepository.schema
      end

      def get_index_doc
        doc = IndexRepository.get_doc(params[:id])
        doc.to_h
      end

      def delete_index_doc
        doc = IndexRepository.get_doc(params[:id])
        IndexRepository.delete_by_id(doc.id)
        doc.to_h
      end
    end
  end
end
