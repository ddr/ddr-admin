module Ddr
  module API
    module FullyQualifiedModel
      def fq_model(model)
        case model
        when String
          "Ddr::#{model.demodulize}"
        when Array
          model.map { |m| fq_model(m) }
        when Class
          model.name
        when Symbol
          fq_model(model.to_s.camelize)
        when nil
          nil
        end
      end

      extend self
    end
  end
end
