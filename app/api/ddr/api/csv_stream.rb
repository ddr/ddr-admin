require 'csv'

module Ddr
  module API
    class CSVStream
      attr_reader :record_set, :entity_class, :options

      def initialize(record_set, entity_class, options = {})
        @record_set = record_set
        @entity_class = entity_class
        @options = options
      end

      def each
        if options.fetch(:remove_empty_columns, false)
          return if rows.size == 0

          table = CSV::Table.new(rows.to_a)
          table.by_col!.delete_if { |_c, vals| vals.all?(&:blank?) }

          yield CSV.generate_line(table.headers, **csv_options)

          table.by_row!.each do |row|
            yield row.to_s(**csv_options)
          end

        else
          yield CSV.generate_line(rows.peek.headers, **csv_options)

          rows.each do |row|
            yield row.to_s(**csv_options)
          end
        end
      rescue StopIteration => _e # rows.peek
      end

      # @return [Enumerator<CSV::Row>]
      def rows
        resources.lazy.map do |resource|
          entity_class.represent(resource, options).to_csv
        end
      end

      def resources
        if record_set.respond_to?(:find_each) # e.g. ActiveRecord Relation
          record_set.find_each
        elsif record_set.respond_to?(:each)   # e.g., Ddr::API::IndexQuery
          record_set
        else
          Array(record_set)
        end
      end

      def csv_options
        { encoding: Encoding::UTF_8 }
      end
    end
  end
end
