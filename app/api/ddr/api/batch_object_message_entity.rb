module Ddr
  module API
    class BatchObjectMessageEntity < Entity
      self.schema = Schema[:batch_object].dig('$defs', 'batch_object_message')

      build_exposures
    end
  end
end
