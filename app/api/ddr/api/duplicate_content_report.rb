module Ddr
  module API
    class DuplicateContentReport < AbstractReport
      self.default_fields = %i[
        sha1
        id
        model
        original_filename
        parent_id
        collection_title
        workflow_state
        ingestion_date
      ].freeze

      def headers
        fields + %i[occurrences]
      end

      def results
        Enumerator.new do |yielder|
          duplicates.each do |sha1, count|
            query = IndexRepository.query(q: "content_sha1_ssi:#{sha1}")

            query.each do |resource|
              data = FlatResourceEntity.represent(resource, only: fields, serializable: true)
              yielder << data.merge(occurrences: count)
            end
          end
        end
      end

      def duplicates
        response = IndexRepository.select(
          params: {
            'q' => 'content_sha1_ssi:*',
            'fq' => 'common_model_name_ssi:Component',
            'facet' => 'true',
            'facet.field' => 'content_sha1_ssi',
            'facet.limit' => 1000,
            'facet.mincount' => 2,
            'rows' => 0
          }
        )

        facets = response.dig('facet_counts', 'facet_fields', 'content_sha1_ssi')

        Hash[*facets]
      end
    end
  end
end
