module Ddr
  module API
    #
    # Represents a Ddr::TechnicalMetadataManager instance
    #
    class TechnicalMetadataEntity < Entity
      # From the resource
      expose :id, :local_id

      # From FITS
      expose :color_space,
             :created,
             :creating_application,
             :extent,
             :format_label,
             :format_version,
             :icc_profile_name,
             :icc_profile_version,
             :image_height,
             :image_width,
             :media_type,
             :modified,
             :pronom_identifier,
             :valid,
             :well_formed

      def id
        object.object.id.to_s
      end

      def local_id
        object.object.local_id
      end
    end
  end
end
