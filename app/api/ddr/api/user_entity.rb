module Ddr
  module API
    class UserEntity < Entity
      self.schema = Schema[:user]

      build_exposures
    end
  end
end
