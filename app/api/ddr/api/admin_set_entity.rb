module Ddr
  module API
    class AdminSetEntity < Entity
      self.schema = Schema[:admin_set]

      build_exposures
    end
  end
end
