module Ddr
  module API
    class OAuthConfig
      STATE = SecureRandom.hex(4)

      class << self
        def config_uri
          URI::HTTPS.build(host:, path: '/oidc/.well-known/openid-configuration')
        end

        def config
          @config ||= load_config.symbolize_keys.freeze
        end

        def load_config
          JSON.parse Net::HTTP.get(config_uri)
        end

        def [](key)
          config.fetch(key)
        end

        def host
          ENV.fetch('OAUTH_HOST', 'oauth.oit.duke.edu')
        end

        def client_id
          ENV.fetch('OAUTH_CLIENT_ID')
        end

        def client_secret
          ENV.fetch('OAUTH_CLIENT_SECRET')
        end
      end
    end
  end
end
