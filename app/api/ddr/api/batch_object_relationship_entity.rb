module Ddr
  module API
    class BatchObjectRelationshipEntity < Entity
      self.schema = Schema[:batch_object].dig('$defs', 'batch_object_relationship')

      build_exposures
    end
  end
end
