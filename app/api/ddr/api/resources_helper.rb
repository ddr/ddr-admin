require 'base64'

module Ddr
  module API
    module ResourcesHelper
      # after_validation callback
      def load_and_authorize_resource
        return unless id = declared(params)[:id]

        @resource = ResourceRepository.find(id)

        # TODO: refactor
        if route.origin == '/resources/:id/iiif'
          authorize! :discover, @resource
        else
          authorize! :read, @resource
        end
      end

      def field_options
        if params[:fields]
          { except: [{ metadata: ResourceEntity.metadata_field_names - params[:fields] }] }
        elsif params[:exclude_fields]
          { except: [{ metadata: params[:exclude_fields] }] }
        else
          {}
        end
      end

      def csv_options
        params.slice(:remove_empty_columns, :file_field_value).tap do |opts|
          only = %w[id model] + params.fetch(:csv_fields, []) + params.fetch(:file_fields, [])

          only += if params[:fields]
                    params[:fields]
                  elsif params[:exclude_fields]
                    ResourceEntity.metadata_field_names - params[:exclude_fields]
                  else
                    ResourceEntity.metadata_field_names
                  end

          opts[:only] = only
        end
      end

      def download_file(file_field)
        error!('Invalid file field.', 400) unless ResourceEntity.file_field_names.include?(file_field)

        unless @resource.send(:"can_have_#{file_field}?")
          error!("File field '#{file_field}' is not supported for resource type '#{@resource.class}'.",
                 400)
        end

        unless file = @resource.send(file_field)
          error!("File field '#{file_field}' not found.", 404)
        end

        content_type file.media_type

        if sha1 = file.sha1
          header 'Content-Digest: sha=:%s:' % Base64.strict_encode64(sha1)
        end

        header 'Content-Disposition',
               'attachment; filename="%s"' % download_filename(file, file_field)

        if path = file.file_path
          return sendfile(path) if headers['X-Sendfile-Type'].present?

          return stream(path)
        end

        stream file.file.to_enum
      end

      def download_filename(file, file_field)
        if file_field == 'content'
          CGI.escape file.original_filename
        else
          format('%s.%s', file_field, file.default_file_extension)
        end
      end

      def resource_options
        field_options.merge(with: ResourceEntity)
      end

      def declared_params
        @declared_params ||= declared(params, include_missing: false).freeze
      end

      def present_or_stream(resources)
        if env['api.format'] == :csv
          stream_resources(resources)
        else
          present_resources(resources)
        end
      end

      def stream_resources(resources)
        filename = 'DDR-Resources-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
        header 'Content-Disposition', 'attachment; filename="%s"' % filename

        # Rack 2.x ETag middleware has some issue that affects streaming:
        # https://github.com/rack/rack/issues/1619#issuecomment-848460528
        # This is supposedly one workaround:
        header 'Last-Modified', Time.now.httpdate

        stream CSVStream.new(resources, FlatResourceEntity, csv_options)
      end

      def present_resources(resources)
        present paginate(resources), resource_options
      end
    end
  end
end
