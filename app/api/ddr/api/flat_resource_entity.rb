module Ddr
  module API
    #
    # Represents a resource in a "flattened" format suitable for use
    # in CSV output.
    #
    class FlatResourceEntity < BaseResourceEntity
      FILE_NOT_PRESENT = '-'

      FILE_FIELD_NOT_SUPPORTED = '[N/A]'

      # Method to call on Ddr::File for value to render
      DEFAULT_FILE_FIELD_VALUE = 'original_filename'

      metadata_field_names.each do |property|
        PropertyExposure.call(entity: self, schema: metadata.dig('properties', property), property:)
      end

      %w[collection_id parent_id].each do |property|
        options = { safe: true, format_with: :valkyrie_id }

        PropertyExposure.call(entity: self, schema: related.dig('properties', property), property:, options:)
      end

      # We want to expose these properties, even though not all resources support them,
      # in order to produce tabular output. See BaseResourceEntity for method definitions.
      expose :collection_title
      expose :original_filename, safe: true
      expose :parent_title
      expose :sha1

      # Files are rendered according to the value of the `file_field_value` param,
      # if present
      file_field_names.each do |file_field|
        expose file_field do |obj, opts|
          file = obj.send(file_field)
          if file.present?
            file_field_value = opts.fetch(:file_field_value, DEFAULT_FILE_FIELD_VALUE)
            file.send(file_field_value)
          else
            FILE_NOT_PRESENT
          end
        rescue NoMethodError, _e
          FILE_FIELD_NOT_SUPPORTED
        end
      end
    end
  end
end
