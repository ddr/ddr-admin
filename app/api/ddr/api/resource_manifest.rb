require 'csv'

module Ddr
  module API
    class ResourceManifest
      COLUMNS = (%i[model id path sha1] + ResourceEntity.metadata_field_names.map(&:to_sym)).freeze

      def initialize(resource)
        @resource = resource

        rows { |row| table << row }

        table.by_col!.delete_if { |_c, vals| vals.all?(&:blank?) }
      end

      def each
        yield CSV.generate_line(table.headers, **csv_options)

        table.by_row!.each do |row|
          yield row.to_s(**csv_options)
        end
      end

      def table
        @table ||= CSV::Table.new([], headers: COLUMNS.dup)
      end

      def rows(&)
        resource_rows(@resource, &)
      end

      def resource_rows(resource, &block)
        entity = ResourceEntity.represent(resource)

        yield _row(entity.as_csv)

        resource.attached_files_having_content.each do |file_field|
          file = resource.send(file_field)
          fields = { model: "File.#{file_field}", path: file.file_path, sha1: file.sha1 }

          yield _row(fields)
        end

        return unless resource.respond_to?(:children)

        resource.children.each do |child|
          resource_rows(child, &block)
        end
      end

      def _row(fields)
        CSV::Row.new(COLUMNS.dup, fields.values_at(*COLUMNS))
      end

      def csv_options
        { encoding: Encoding::UTF_8 }
      end
    end
  end
end
