module Ddr
  module API
    class EventEntity < Entity
      self.schema = Schema[:event]

      build_exposures(except: %w[exception])

      def self.outcomes
        warn "[DEPRECATION] [API] #{self}.outcomes is deprecated; use `Ddr::Events::Event::OUTCOMES' instead."

        Ddr::Events::Event::OUTCOMES
      end

      def self.types
        warn "[DEPRECATION] [API] #{self}.types is deprecated; use `Ddr::Events.event_types' instead."

        Ddr::Events.event_types
      end

      delegate :to_xml, to: :object
    end
  end
end
