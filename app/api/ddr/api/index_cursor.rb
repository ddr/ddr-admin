module Ddr
  module API
    #
    # Encapsulates a Solr query using a "cursor".
    #
    class IndexCursor
      include Enumerable

      # Regex for checking whether sort contains a sort by id clause
      ID_SORT_REGEX = Regexp.new('\bid (asc|ASC|desc|DESC)\b')

      DEFAULT_ID_SORT = 'id asc'

      attr_reader :query

      def initialize(query)
        @query = query
        @query.sort_by(DEFAULT_ID_SORT) unless ID_SORT_REGEX.match?(query.sort)
      end

      def each(...)
        cursor = '*'

        loop do
          data = query.params.merge(cursorMark: cursor)
          response = IndexResponse.new query.select({ method: :post, data: })
          break if response.empty?

          query.convert(response.results).each(...)

          break if response.num_found < query.rows
          break if cursor == response.next_cursor

          cursor = response.next_cursor
        end
      end
    end
  end
end
