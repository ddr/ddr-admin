module Ddr
  module API
    class ResourceEntity < BaseResourceEntity
      expose :metadata, using: 'Ddr::API::MetadataEntity'

      build_exposures(only: %w[files], safe: true)

      expose :related do
        expose(*related_field_names, safe: true, format_with: :valkyrie_id)
      end

      # Render entity as Hash for CSV output
      # @deprecated Use Ddr::API::FlatResourceEntity to render CSV.
      # @return [Hash]
      def as_csv
        super.slice(:id, :model, :metadata).tap do |h|
          metadata = h.delete(:metadata)
          metadata.transform_values! { |v| v.is_a?(Array) ? v.join(MV_SEPARATOR) : v }
          h.merge! metadata
        end
      end
    end
  end
end
