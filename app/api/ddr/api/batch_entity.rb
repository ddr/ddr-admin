module Ddr
  module API
    class BatchEntity < Entity
      self.schema = Schema[:batch]

      build_exposures(except: %w[batch_objects user])

      expose :user, using: 'Ddr::API::UserEntity'

      expose :batch_objects,
             if: { batch_objects: true },
             using: 'Ddr::API::BatchObjectEntity'
    end
  end
end
