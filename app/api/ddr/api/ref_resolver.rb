module Ddr
  module API
    class RefResolver
      MAX_DEPTH = 10 # max depth to resolve refs, to prevent infinite loops

      # @param ref [String]
      # @param doc [Hash] a JSON schema or OpenAPI document
      # @return [Hash] the resolved reference
      # @raise [TypeError] if doc is not a Hash
      # @raise [Ddr::API:::Error] if ref is not resolvable, retrievable, parseable, or max depth exceeded
      def self.call(ref:, doc: OpenAPIDocument.instance, depth: 0)
        raise TypeError, "doc must be a Hash (doc is a #{doc.class})" unless doc.is_a?(Hash)

        # no $ref - done
        return doc if ref.blank?

        depth += 1 # increment depth counter
        raise Error, "Max depth exceeded resolving ref: #{ref}" if depth > MAX_DEPTH

        # Local doc ref
        if m = %r{^\#/(.*)}.match(ref)
          # Unescape OpenAPI escape sequences for '/' and '~', if present
          parts = m[1].split('/').map { |part| part.gsub('~1', '/').gsub('~0', '~') }
          resolved = doc.dig(*parts)

          raise Error, "Unable to resolve ref: #{ref}" if resolved.nil?

          return RefResolver.call(doc: resolved, ref: resolved['$ref'], depth:)
        end

        # DDR-specific, by convention - pull schema directly, not over http
        if m = %r{^/api/schemas/(\w+)(\.json)?(\#/.*)?}.match(ref)
          schema = Schema.load_schema(m[1])

          return RefResolver.call(doc: schema, ref: m[3], depth:)
        end

        # External ref
        if /^https?:/.match?(ref)
          uri = URI(ref)
          response = Net::HTTP.get_response(uri, { 'accept' => 'application/json, application/yaml' })

          unless response.code == '200'
            raise Error,
                  "Error retrieving remote ref at #{uri}: #{response.code} #{response.message}"
          end

          begin
            remote_doc = response['content-type'] == 'application/json' ? JSON.parse(response.body) : YAML.safe_load(response.body)
          rescue JSON::ParserError, Psych::SyntaxError => e
            raise Error, "Unable to parse remote ref at #{uri}: #{e}"
          end

          # resolve URI fragment
          return RefResolver.call(ref: uri.fragment, doc: remote_doc, depth:)
        end

        raise Error, "Unable to resolve ref: #{ref} (document: #{doc.inspect})"
      end
    end
  end
end
