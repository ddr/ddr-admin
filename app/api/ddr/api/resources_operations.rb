module Ddr
  module API
    module ResourcesOperations
      extend Grape::API::Helpers

      include Pagination
      include ResourcesHelper

      # GET /resources
      def get_resources
        filter_params = params.slice(*IndexRepository::FILTER_PARAMS)
        resources = IndexRepository.find_resources(filter_params).with_effective_role_filter(current_ability)
        present_or_stream(resources)
      end

      # POST /resources
      def post_resources
        # authorization happens after schema validation and attribute setting
        resource = Ddr.const_get(params.fetch(:model)).new

        # set the resource attributes
        attrs = declared_params.fetch(:metadata, {}).merge(declared_params.fetch(:related, {}))
        attrs.each do |attr, value|
          resource.set_value(attr, value)
        end

        resource.roles = declared_params[:roles]

        # do authorization here b/c it may depend on attributes, like parent_id
        authorize! :create, resource

        @resource = Ddr.persister.save(resource:)

        present @resource, with: ResourceEntity
      end

      # GET /resources/by_role
      def get_resources_by_role
        #
        # Restricted to curators b/c we don't currently have a good way to filter
        # this query by permissions.
        #
        unauthorized! unless curator?
        error!('Missing required parameter: agent, role_type, or scope.', 400) unless params.slice(:agent, :role_type,
                                                                                                   :scope).compact.any?
        resources = ResourceRepository.find_by_role params.slice(:agent, :role_type, :scope, :model)
        present_or_stream(resources)
      end

      # POST /resources/collections
      def post_collections
        authorize! :create, Ddr::Collection
        resource = Ddr::Collection.new
        declared_params.fetch(:metadata, {}).each { |attr, value| resource.set_value(attr, value) }
        resource.roles = declared_params[:roles]
        @resource = Ddr.persister.save(resource:)
        present @resource, with: ResourceEntity
      end

      # POST /resources/export_files
      def post_resources_export_files
        @export = Ddr::ExportFiles::Package.new(
          **params.slice(:identifiers, :basename, :files).merge(ability: current_ability).symbolize_keys
        )

        error!("Invalid export package: #{@export.errors.full_messages}", 400) unless @export.valid?

        status 202
        ExportFilesJob.perform_later(
          @export.identifiers,
          @export.basename,
          current_user.id,
          @export.files
        )
      end

      # POST /resources/find_by_identifiers
      def post_resources_find_by_identifiers
        resources = IndexRepository.find_by_identifiers(params[:identifiers]).with_effective_role_filter(current_ability)
        status 200
        present_or_stream(resources)
      end

      # GET /resources/:id
      def get_resource
        present @resource, resource_options
      end

      # PATCH /resources/:id
      def patch_resource
        authorize! :update, @resource
        #
        # N.B. This method does not fully account for intentionally setting a single-valued
        # attribute to `nil'. Multi-valued attributes can be "cleared" by setting values to
        # empty arrays. We may have to add a parameter specifically for clearing attributes
        # -- for example: clear_attributes=subject,available -- in order to disambiguate
        # a missing or unintentionally `nil' parameter from the intention to clear a value.
        #
        change_set = Ddr::ResourceChangeSet.change_set_for(@resource)
        change_set.optimistic_lock_token = declared_params[:optimistic_lock_token]
        attrs = declared_params.fetch(:metadata, {}).merge(declared_params.fetch(:related, {}))
        attrs.each { |attr, value| change_set.set_values(attr, value) }
        change_set.roles = declared_params[:roles] if declared_params[:roles].present?

        Ddr::ResourceChangeSetPersister.new.save(change_set:)

        present @resource, with: ResourceEntity
      end

      # GET /resources/:id/attachments
      def get_resource_attachments
        resources = ResourceRepository.attachments @resource.id
        present_or_stream(resources)
      end

      # GET /resources/:id/batches
      def get_resource_batches
        batches = Ddr::Batch::Batch.joins(:batch_objects).where('batch_objects.resource_id' => @resource.id.to_s)
        if @resource.is_a?(Ddr::Collection)
          batches = batches.or(Ddr::Batch::Batch.where(collection_id: @resource.id.to_s))
        end

        present paginate(batches), with: BatchEntity, links: true, batch_objects: params[:batch_objects]
      end

      # GET /resources/:id/children
      def get_resource_children
        unless @resource.respond_to?(:children)
          error!("A #{@resource.human_readable_type} resource does not have child resources.", 400)
        end
        resources = ResourceRepository.children @resource.id
        present_or_stream(resources)
      end

      # POST /resources/:id/children
      def post_resource_children
        unless @resource.respond_to?(:children)
          error!("A #{@resource.human_readable_type} resource does not have child resources.", 400)
        end

        model = Ddr::Resource.subclasses.detect do |c|
          c.respond_to?(:parent_class) && c.parent_class == @resource.class
        end

        # Just in case
        error!("Unable to find child class for #{@resource.class}", 500) unless model

        child = model.new(parent_id: @resource.id)
        authorize! :create, child

        params.fetch(:metadata, {}).each { |attr, value| child.set_value(attr, value) }

        @child = Ddr.persister.save(resource: child)

        present @child, with: ResourceEntity
      end

      # GET /resources/:id/download
      def get_resource_download
        authorize! :download, @resource
        download_file('content')
      end

      # GET /resources/:id/files
      def get_resource_files
        ResourceEntity.represent(@resource).serializable_hash[:files]
      end

      # GET /resources/:id/files/:file_field
      def get_resource_file
        authorize! :download, @resource if params[:file_field] == 'content'
        download_file(params[:file_field])
      end

      # POST /resources/:id/files/:file_field
      def post_resource_file
        authorize! :update, @resource
        file_field, file_path = params.values_at(:file_field, :file_path)
        error!('Invalid file field.', 400) unless ResourceEntity.file_field_names.include?(file_field)
        unless @resource.send(:"can_have_#{file_field}?")
          error!("File field '#{file_field}' is not supported for resource type '#{@resource.class}'.",
                 400)
        end
        error!("File path '#{file_path}' not allowed.", 403) unless Ddr::API.safe_upload_paths.detect do |path|
                                                                      file_path.start_with?(path)
                                                                    end

        #
        # We shouldn't have to do this here b/c it should be
        # handled by the change set, which does calculate a
        # digest for an added file ...
        #
        sha1_digest = ::Digest::SHA1.file(file_path).hexdigest
        error!('File digest does not match SHA1 given.', 400) if params[:sha1] != sha1_digest

        # large files are accepted for background processing
        if ::File.size(file_path) > MAX_SYNC_UPLOAD
          status 204
          return Ddr::UploadFileJob.perform_later(resource_id: @resource.id.to_s, file_path:,
                                                  file_type: file_field)
        end

        is_file_update = @resource.send(:"has_#{file_field}?")

        change_set = Ddr::ResourceChangeSet.change_set_for(@resource)
        change_set.add_file(file_path, file_field)

        Ddr::ResourceChangeSetPersister.new.save(change_set:)

        if is_file_update
          status 200
        else
          status 201
          header 'content-location', env['REQUEST_PATH']
        end

        ResourceEntity.represent(@resource).serializable_hash[:files][file_field.to_sym]
      end

      # PUT /resources/:id/files/:file_field
      def put_resource_file
        authorize! :update, @resource
        file_field = params[:file_field]
        error!('Invalid file field.', 400) unless ResourceEntity.file_field_names.include?(file_field)
        unless @resource.send(:"can_have_#{file_field}?")
          error!("File field '#{file_field}' is not supported for resource type '#{@resource.class}'.",
                 400)
        end

        file = params[:file]

        # file headers are embedded in form-data element:
        # "content-disposition: form-data; name=\"file\"; filename=\"fits.xml\"\r\ncontent-type: text/xml\r\ncontent-length: 4520\r\n"

        content_length_header = file[:head].scan(/content-length: \d+/).first
        error!('content-length header is missing.', 411) if content_length_header.nil?

        content_length = content_length_header.split(': ').last.to_i
        if content_length > MAX_CONTENT_LENGTH
          error!(
            "Content too large (max: #{Ddr::API.max_content_length_human}); use POST at this endpoint to upload a file on the server.", 413
          )
        end

        #
        # We shouldn't have to do this here b/c it should be
        # handled by the change set, which does calculate a
        # digest for an added file ...
        #
        sha1_digest = ::Digest::SHA1.file(file[:tempfile].path).hexdigest
        error!('File digest does not match SHA1 given.', 400) if params[:sha1] != sha1_digest

        is_file_update = @resource.send(:"has_#{file_field}?")

        mime_type         = file[:type]
        original_filename = file[:filename]

        change_set = Ddr::ResourceChangeSet.change_set_for(@resource)
        change_set.add_file(file[:tempfile], file_field, mime_type:, original_filename:)

        Ddr::ResourceChangeSetPersister.new.save(change_set:)

        if is_file_update
          status 200
        else
          status 201
          header 'content-location', env['REQUEST_PATH']
        end

        ResourceEntity.represent(@resource).serializable_hash[:files][file_field.to_sym]
      end

      # GET /resources/:id/fixity_check
      def get_resource_fixity_check
        if @resource.has_content? && @resource.content_size > 1_000_000_000
          status 202
          Ddr::FixityCheckJob.perform_later(@resource.id.to_s)
        else
          Ddr::FixityCheck.call(@resource)
        end
      end

      def post_resource_fixity_check
        unauthorized! unless curator?
        event = Ddr::Events::FixityCheckEvent.create(
          resource_id: @resource.id.to_s,
          event_date_time: params[:checked_at],
          outcome: params[:outcome],
          detail: params[:details]
        )
        header 'content-location', "/api/events/#{event.id}"
        present event, with: EventEntity
      end

      # GET /resources/:id/iiif
      def get_resource_iiif
        if params[:dynamic] || params[:include_previewable] || !@resource.has_iiif_file?
          attrs = declared(params).compact.symbolize_keys.merge(resource: @resource)
          logger.debug('API') { "Generating IIIF manifest dynamically for #{@resource.id}" }
          return Ddr::V3::IiifPresentationService.call(**attrs)
        end

        unless params[:start]
          logger.debug('API') { "Returning static IIIF manifest for #{@resource.id}" }
          return @resource.iiif_file.content
        end

        # Ensures that `start' component exists
        start = Ddr::API::ResourceRepository.find_by_permanent_id(params[:start])

        IIIF::V3::AbstractResource.parse(@resource.iiif_file.content).tap do |iiif|
          logger.debug('API') { "Adding 'start' key to IIIF manifest for #{@resource.id}" }
          iiif['start'] = {
            id: Ddr::V3::IiifPresentationService.iiif_resource_id(start, '/canvas'),
            type: 'Canvas'
          }
        end
      end

      # GET /resources/:id/manifest
      def get_resource_manifest
        stream ResourceManifest.new(@resource)
      end

      # GET /resources/:id/members
      def get_resource_members
        resources = ResourceRepository.members(@resource.id, model: params[:model])
        present_or_stream(resources)
      end

      # GET /resources/:id/permanent_id
      def get_resource_permanent_id
        error!('The resource does not have a permanent ID assigned.', 404) unless @resource.permanent_id
        permanent_id = Ddr::PermanentId.new(@resource)
        permanent_id.identifier.metadata
      end

      # GET /resources/:id/permissions
      def get_resource_permissions
        @resource.effective_roles.each_with_object({}) do |role, memo|
          role.permissions.each do |perm|
            memo[perm] ||= []
            memo[perm] << role.agent
          end
        end
      end

      # GET /resources/:id/targets
      def get_resource_targets
        resources = ResourceRepository.targets @resource.id
        present_or_stream(resources)
      end

      # GET /resources/:id/technical_metadata
      def get_resource_technical_metadata
        resources = case @resource
                    when Ddr::Collection
                      ResourceRepository.components(@resource.id)
                    when Ddr::Item
                      ResourceRepository.children(@resource.id)
                    else
                      [@resource]
                    end

        techmd = resources.map(&:techmd)

        if env['api.format'] == :csv
          filename = 'DDR-Technical-Metadata-%s.csv' % @resource.id
          header 'Content-Disposition', 'attachment; filename="%s"' % filename
          # Rack 2.x ETag middleware has some issue that affects streaming:
          # https://github.com/rack/rack/issues/1619#issuecomment-848460528
          header 'Last-Modified', Time.now.httpdate

          stream CSVStream.new(techmd, TechnicalMetadataEntity)
        else
          present techmd.to_a, with: TechnicalMetadataEntity
        end
      end

      # POST /resources/:id/technical_metadata
      def post_resource_technical_metadata
        authorize! :update, @resource

        error! 'Bad Request - Not a content-bearing resource', 400 unless @resource.is_a?(Ddr::HasContent)
        error! 'Content not found for resource', 404 unless @resource.has_content?

        if file = params[:file]
          error! 'Bad Request - file media type must be "text/xml"' unless file[:type] == 'text/xml'

          Ddr::V3::FileUploadService[:fits_file].call(resource: @resource,
                                                      file: file[:tempfile],
                                                      media_type: 'application/xml',
                                                      original_filename: "#{@resource.id}-fits_file.xml")
          body false
          status 200
        else
          status 202
          present Ddr::V3::FileUpdateService[:fits_file].call(resource: @resource, async: Rails.env.production?)
        end
      end

      # POST /resources/:id/workflow
      def post_resource_workflow
        action = params[:action].to_sym
        authorize! action, @resource
        on_behalf_of = params.fetch(:email, current_user)
        status 202
        Ddr::V3::PublicationService.call(resource: @resource, event: action, async: true, on_behalf_of:)
      end
    end
  end
end
