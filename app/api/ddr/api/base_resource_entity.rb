module Ddr
  module API
    #
    # A Resource entity for use in Grape API responses.
    #
    # The instance methods/exposures should correspond to the properties defined in the schema.
    #
    class BaseResourceEntity < Entity
      self.schema = Schema[:resource]

      format_with(:valkyrie_id) { |value| value&.to_s }

      def self.models
        schema.dig('properties', 'model', 'enum')
      end

      def self.system_properties
        properties.select { |_, s| s['type'] == 'string' }
      end

      # @deprecated Use {Ddr::API::MetadataEntity.schema} instead.
      def self.metadata
        MetadataEntity.schema
      end

      # @deprecated
      def self.metadata_properties
        metadata.fetch('properties')
      end

      def self.role_properties
        properties.dig('roles', 'items', 'properties')
      end

      # @deprecated Use {Ddr::API::MetadataEntity.field_names} instead.
      def self.metadata_field_names
        MetadataEntity.field_names
      end

      def self.files
        properties.fetch('files')
      end

      def self.file_field_names
        files.fetch('properties').keys
      end

      def self.file_properties
        schema.dig('$defs', 'file', 'properties')
      end

      def self.related
        properties.fetch('related')
      end

      def self.related_field_names
        related.fetch('properties').keys
      end

      def self.admin_sets
        metadata_properties.dig('admin_set', 'enum')
      end

      def self.workflow_states
        metadata_properties.dig('workflow_state', 'enum')
      end

      build_exposures(except: %w[metadata files related])

      #
      # Instance methods
      #
      def id
        object['id'].to_s
      end

      def model
        object.human_readable_type
      end

      def optimistic_lock_token
        object.optimistic_lock_token&.first&.to_s
      end

      def collection_title
        object.admin_policy&.title_display
      end

      def parent_title
        object.parent&.title_display if object.respond_to?(:parent)
      end

      def sha1
        object.content&.sha1 if object.can_have_content?
      end

      def metadata
        object
      end

      def related
        object
      end
    end
  end
end
