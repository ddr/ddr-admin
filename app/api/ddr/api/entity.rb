require 'csv'

module Ddr
  module API
    #
    # @abstract superclass
    #
    class Entity < Grape::Entity
      MV_SEPARATOR = '|' # ASCII x7C

      class_attribute :schema

      def self.build_exposures(**options)
        ExposureBuilder.call(entity: self, schema:, options: options.dup.freeze)
      end

      def self.schema_validator(**)
        SchemaValidator.new(schema, **)
      end

      # @return [Hash] top-level properties
      def self.properties
        @properties ||= schema.fetch('properties').reject do |_name, prop|
          prop.fetch('deprecated', false) || prop.fetch('writeOnly', false)
        end
      end

      def to_csv
        row = as_csv.transform_values { |v| v.respond_to?(:to_ary) ? v.to_ary.join(MV_SEPARATOR) : v }

        CSV::Row.new(row.keys, row.values)
      end

      def as_csv
        serializable_hash
      end
    end
  end
end
