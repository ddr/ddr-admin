module Ddr
  module API
    class GroupMemberEntity < Entity
      # https://github.com/ruby-grape/grape-entity#basic-exposure
      self.hash_access = :to_s

      expose :id, :name, :type
    end
  end
end
