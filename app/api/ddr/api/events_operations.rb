module Ddr
  module API
    module EventsOperations
      extend Grape::API::Helpers

      include CSVHelper
      include Pagination

      def get_events
        klass = Ddr::Events::EventFactory.class_for_event_type(params[:type])
        conditions = params.slice(:resource_id, :outcome, :user_key)
        conditions[:user_key] = nil if conditions[:user_key] == Ddr::Events::Event::SYSTEM
        records = conditions.present? ? klass.where(conditions) : klass.all
        records = records.where('event_date_time >= ?', params[:since]) if params[:since]
        records = records.where('event_date_time < ?', params[:before]) if params[:before]

        if csv?
          filename = 'DDR-Events-%s.csv' % csv_timestamp
          stream_csv(records:, filename:, entity_class: EventEntity)
        elsif xml?
          error!('Resource ID required for XML output', 400) unless params[:resource_id]
          PremisXMLEvent.call(records)
        else
          present paginate(records), with: EventEntity
        end
      end

      def post_events
        attributes = declared(params).except(:event_type).compact
        attributes[:event_date_time] ||= DateTime.now
        event = Ddr::Events::EventFactory.call(declared(params)[:event_type], **attributes)
        event.save!
        present event, with: EventEntity
      end

      def get_event
        @event = Ddr::Events::Event.find(params[:id])

        if xml?
          @event
        else
          present @event, with: EventEntity
        end
      end
    end
  end
end
