module Ddr
  module API
    class GroupEntity < Entity
      self.schema = Schema[:group]

      expose :id
      expose :label
      expose :members, if: { members: true }, using: 'Ddr::API::GroupMemberEntity'

      def members
        GroupRepository.group_members(object.id)
      end
    end
  end
end
