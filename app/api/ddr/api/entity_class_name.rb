module Ddr
  module API
    class EntityClassName
      def self.call(schema)
        raise TypeError, 'Schema object must be Hash-like.' unless schema.respond_to?(:fetch)

        return nil unless schema['type'] == 'object'

        'Ddr::API::' + schema.fetch('title').gsub(/\W/, '') + 'Entity'
      end
    end
  end
end
