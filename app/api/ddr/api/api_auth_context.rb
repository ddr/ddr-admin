module Ddr
  module API
    #
    # Represents an API request context for authnz.
    #
    class APIAuthContext < Ddr::Auth::AuthContext
      def ismemberof
        if anonymous?
          super
        else
          GroupRepository.user_groups(user)
        end
      end

      def superuser?
        super || authorized_to_act_as_superuser?
      end
    end
  end
end
