module Ddr
  module API
    class BatchObjectEntity < Entity
      self.schema = Schema[:batch_object]

      build_exposures
    end
  end
end
