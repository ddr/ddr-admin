module Ddr
  module API
    class FileEntity < Entity
      self.schema = Schema[:resource].dig('$defs', 'file')

      build_exposures
    end
  end
end
