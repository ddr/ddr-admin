#
# THIS FILE WAS AUTOMATICALLY GENERATED. DO NOT EDIT!
#
# To update run: `bin/rails ddr:api:generate_api[schemas]`
#
# rubocop:disable Style/StringLiterals, Layout/TrailingWhitespace, Layout/LineLength, Style/WordArray, Lint/RedundantCopDisableDirective, Lint/RedundantCopEnableDirective, Layout/EmptyLinesAroundClassBody, Metrics/ClassLength
#
module Ddr
  module API
    class SchemasAPI < Grape::API

      content_type :json, 'application/schema+json'
      default_format :json
      helpers CommonHelpers, SchemasOperations

      # GET /schemas/:entity
      desc "Retrieve schema for an entity"
      params do
        requires :entity, type: String, desc: "Schema entity name", values: ["admin_set", "batch", "batch_object", "deleted_file", "event", "group", "metadata", "resource", "user"], allow_blank: false
      end
      get '/schemas/:entity' do
        get_schema
      end

    end
  end
end
# rubocop:enable Style/StringLiterals, Layout/TrailingWhitespace, Layout/LineLength, Style/WordArray, Lint/RedundantCopDisableDirective, Lint/RedundantCopEnableDirective, Layout/EmptyLinesAroundClassBody, Metrics/ClassLength
