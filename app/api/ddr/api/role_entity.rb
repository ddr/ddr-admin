module Ddr
  module API
    class RoleEntity < Entity
      self.schema = Schema[:resource].dig('properties', 'roles', 'items')

      build_exposures
    end
  end
end
