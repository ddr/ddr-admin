require 'csv'

module Ddr
  module API
    class AbstractReport
      class_attribute :default_fields, :field_values, :mv_separator

      # Default fields included in report if none are requested
      # by the "fields" param.
      self.default_fields = []

      # The available fields -- i.e., the "values" of the "fields" param.
      self.field_values = FlatResourceEntity.root_exposures.map(&:key).sort.freeze

      # String used to separate multiple data values in a column
      self.mv_separator = '|' # ASCII x7C

      attr_reader :fields

      def self.call(fields = nil)
        new(fields).run
      end

      def initialize(fields = nil)
        @fields = (fields || default_fields).map(&:to_sym)
      end

      def headers
        fields
      end

      def csv_opts
        { headers:,
          encoding: Encoding::UTF_8 }
      end

      def header_row
        CSV::Row.new(headers, headers, true)
      end

      def run
        Enumerator.new do |yielder|
          yielder << CSV.generate_line(header_row, **csv_opts)

          results.each do |result|
            values = result.values_at(*headers).map do |value|
              value.is_a?(::Array) ? value.join(mv_separator) : value
            end

            row = CSV::Row.new(headers, values)
            yielder << CSV.generate_line(row, **csv_opts)
          end
        end
      end

      def results
        raise NotImplementedError, "Subclasses of Ddr::API::AbstractReport must implement `#results'."
      end
    end
  end
end
