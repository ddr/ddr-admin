module Ddr
  module API
    #
    # Entity exposure for a property which is an array of type 'object'
    #
    class ObjectArrayPropertyExposure
      def self.call(entity:, schema:, property:, options: {})
        klass_name = EntityClassName.call(schema.fetch('items'))

        PropertyExposure.call(entity:, schema:, property:, options: options.merge(using: klass_name))
      end
    end
  end
end
