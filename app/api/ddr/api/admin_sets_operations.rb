module Ddr
  module API
    module AdminSetsOperations
      extend Grape::API::Helpers

      def get_admin_sets
        present Ddr::AdminSet.all, with: AdminSetEntity
      end

      def get_admin_set
        present Ddr::AdminSet.find_by_code(params[:code]), with: AdminSetEntity
      end
    end
  end
end
