module Ddr
  module API
    class BatchObjectRoleEntity < Entity
      self.schema = Schema[:batch_object].dig('$defs', 'batch_object_role')

      build_exposures
    end
  end
end
