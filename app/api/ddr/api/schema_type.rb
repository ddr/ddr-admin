module Ddr
  module API
    class SchemaType
      def self.call(schema)
        types = Array(schema.fetch('type'))

        return [schema.dig('items', 'type'), true] if types.include?('array')

        [types.detect { |t| t != 'null' }, false]
      end
    end
  end
end
