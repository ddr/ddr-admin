module Ddr
  module API
    class IndexQueryParams < Dry::Struct
      DEFAULT_ROWS = 1_000

      attribute  :q,    Ddr::Types::Strict::String.default('*:*'.freeze)
      attribute  :fq,   Ddr::Types::Set
      attribute? :fl,   Ddr::Types::UniqueCommaSeparatedValues
      attribute  :rows, Ddr::Types::Coercible::Integer.constrained(gteq: 0).default(DEFAULT_ROWS)
      attribute? :sort, Ddr::Types::UniqueCommaSeparatedValues
    end
  end
end
