module Ddr
  module API
    class IndexRepository
      extend FullyQualifiedModel

      FILTER_PARAMS = %i[admin_set model modified_before modified_after original_filename sha1 workflow_state]

      class << self
        delegate :connection, :resource_factory, to: :adapter
        delegate :commit, :get, :select, :paginate, to: :connection
        delegate :to_resource, to: :resource_factory
        delegate :logger, to: :Rails

        def schema
          get('schema').fetch('schema')
        end

        # Delete an index document by ID
        # @param id [String, Valkyrie::ID]
        # @raise [ConflictError] if a resource having the ID exists
        def delete_by_id(id)
          if ResourceRepository.exist?(id)
            raise ConflictError, "Cannot delete the index document for an existing resource: #{id}"
          end

          connection.delete_by_id(id.to_s)
          commit
        end

        # Create an index query
        # @return [IndexQuery]
        def query(**params)
          IndexQuery.new(params)
        end

        # Retrieve an index document by ID
        # @param id [String, Valkyrie::ID]
        # @return [::SolrDocument] the document
        # @raise [NotFoundError] document not found
        def get_doc(id)
          response = get('get', params: { id: })

          raise NotFoundError, "No index document having ID: #{id}" unless response['doc'].present?

          ::SolrDocument.new(response['doc'])
        end

        alias find get_doc

        # Find resources by query
        # @param params [Hash]
        # @return [Ddr::API::IndexQuery] the query
        def find_resources(params)
          query
            .with(:model_filter, *Array(params[:model]))
            .with(:date_filter, :updated_at_dtsi, before: params[:modified_before], after: params[:modified_after])
            .with(:workflow_state_filter, params[:workflow_state])
            .with(:admin_set_title_filter, params[:admin_set])
            .with(:any_value_filter, :content_sha1_ssi, *Array(params[:sha1]))
            .with(:original_filename_filter, params[:original_filename])
        end

        # Find child resources by parent ID
        # @param parent_id [String, Valkyrie::ID]
        # @return [Ddr::API::IndexQuery] the query
        def find_children(parent_id)
          query.with(:parent_id_filter, parent_id)
        end

        # Find Collection members
        # @param collection_id [String, Valkyrie::ID] the Collection ID
        # @param model [String, Class, Symbol] filter by member model
        # @return [Ddr::API::IndexQuery] the query
        def find_members(collection_id, model: nil)
          find_related_resources(:admin_policy_id_ssi, collection_id, model:)
        end

        # Find resources related to a resource by way of specified field
        # @param field [Symbol, String] the field name to query for the relation
        # @param resource_id [String, Valkyrie::ID] the ID of resource
        # @param model [String, Class, Symbol] filter by member model
        # @return [Ddr::API::IndexQuery] the query
        def find_related_resources(field, resource_id, model: nil)
          query
            .with(:id_filter, field, resource_id)
            .with(:model_filter, model)
        end

        # Find resources by identifier in bulk
        # @param identifiers [Array<String>] list of identifiers
        # @return [Ddr::API::IndexQuery] the query
        def find_by_identifiers(identifiers)
          raise ArgumentError, 'A list of identifiers is required' unless identifiers.present?

          query.with(:any_value_filter, :identifier_all_ssim, *identifiers)
        end

        protected

        def adapter
          Valkyrie::MetadataAdapter.find(:index_solr)
        end
      end
    end
  end
end
