module Ddr
  module API
    #
    # The root of the DDR API.
    #
    class Root < Grape::API
      content_type :json, 'application/json'
      default_format :json
      helpers CommonHelpers

      # See block below
      RESCUE_FROM_ERRORS = {
        400 => [
          Ddr::ChecksumInvalid,
          Ddr::API::BadRequestError,
          ActiveRecord::RecordInvalid
        ].freeze,
        403 => [
          CanCan::AccessDenied,
          Ddr::API::UnauthorizedError
        ].freeze,
        404 => [
          ActiveRecord::RecordNotFound,
          Ddr::API::NotFoundError,
          Ddr::PermanentId::IdentifierNotFound
        ].freeze,
        409 => [
          Ddr::API::ConflictError,
          Valkyrie::Persistence::StaleObjectError
        ].freeze
      }.freeze

      RESCUE_FROM_ERRORS.each do |status, exceptions|
        rescue_from(*exceptions) do |e|
          error!(e, status)
        end
      end

      desc 'OpenAPI 3.x document'
      get '/openapi' do
        # When redirecting to /api/openapi we want to ensure the content type is set to JSON
        content_type 'application/json'
        OpenAPIDocument.instance
      end

      desc '[DEPRECATED] Swagger 2.0 document route'
      get '/swagger_doc' do
        deprecated_route!
        redirect '/api/openapi.json', permanent: true
      end

      desc '[DEPRECATED] Schema route'
      params do
        requires :entity,
                 type: String,
                 desc: 'Schema entity name',
                 values: Schema.names,
                 allow_blank: false
      end
      get '/schema/:entity' do
        deprecated_route!
        redirect "/api/schemas/#{params[:entity]}", permanent: true
      end

      mount Ddr::API::AdminSetsAPI
      mount Ddr::API::BatchesAPI
      mount Ddr::API::BatchObjectsAPI
      mount Ddr::API::DeletedFilesAPI
      mount Ddr::API::EventsAPI
      mount Ddr::API::GroupsAPI
      mount Ddr::API::IndexAPI
      mount Ddr::API::QueuesAPI
      mount Ddr::API::ReportsAPI
      mount Ddr::API::ResourcesAPI
      mount Ddr::API::SchemasAPI
      mount Ddr::API::UsersAPI
    end
  end
end
