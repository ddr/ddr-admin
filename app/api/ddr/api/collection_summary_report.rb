module Ddr
  module API
    class CollectionSummaryReport < AbstractReport
      self.default_fields = %i[id title ingestion_date admin_set workflow_state].freeze

      self.field_values = AbstractReport.field_values -
                          %i[sha1 original_filename collection_title collection_id parent_title parent_id]

      SUMMARY_FIELDS = %i[item_count object_count total_size total_size_bytes].freeze

      def initialize(fields = nil)
        super

        # :id field is required
        @fields.unshift(:id).uniq!
      end

      # @override
      def headers
        fields + SUMMARY_FIELDS
      end

      def content_response
        IndexRepository.select(
          params: {
            'q' => '*:*',
            'facet' => 'true',
            'facet.limit' => collections.length,
            'facet.pivot' => '{!stats=piv1}' + facet_pivot_field,
            'stats' => 'true',
            'stats.field' => '{!tag=piv1 sum=true}' + stats_field,
            'rows' => 0
          }
        )
      end

      def pivot_results
        content_response.dig('facet_counts', 'facet_pivot', facet_pivot_field)
      end

      # @return [Array<Hash>]
      def collections
        @collections ||= ResourceRepository.find_all_of_model('Collection').map do |collection|
          FlatResourceEntity.represent(collection, only: fields, serializable: true)
        end.to_a
      end

      def stats_field
        'content_size_lsi'
      end

      def facet_pivot_field
        'admin_policy_id_ssi,common_model_name_ssi'
      end

      # Merges collection metadata with pivot results
      def results
        pivot_results.map do |pivot|
          collection_id = pivot['value'].sub(/^id-/, '')

          total_size = pivot.dig('stats', 'stats_fields', stats_field, 'sum').to_i
          human_size = ActiveSupport::NumberHelper.number_to_human_size(total_size)

          item_pivot = pivot.fetch('pivot', []).detect(-> {}) { |x| x['value'] == 'Item' }
          item_pivot ||= {}

          collection = collections.detect(-> {}) { |c| c[:id] == collection_id }

          collection.merge(
            object_count: pivot['count'],
            item_count: item_pivot.fetch('count', 0),
            total_size: human_size,
            total_size_bytes: total_size
          )
        end
      end
    end
  end
end
