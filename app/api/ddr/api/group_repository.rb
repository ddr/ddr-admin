module Ddr
  module API
    class GroupRepository
      class << self
        delegate :repository_groups,
                 :user_groups,
                 :group_users,
                 :group_members,
                 to: :group_service

        def group_service
          Ddr::PolicyGroupService
        end
      end
    end
  end
end
