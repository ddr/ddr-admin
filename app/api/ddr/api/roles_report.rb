module Ddr
  module API
    class RolesReport < AbstractReport
      ROLE_FIELDS = %i[role_type agent scope]

      self.default_fields = %i[id model title]

      attr_reader :params

      def initialize(**params)
        Rails.logger.debug('API') { "RolesReport initialized with #{params}" }
        super(params[:fields])
        @params = params
      end

      def headers
        fields + ROLE_FIELDS
      end

      def include_inherited?
        params[:include_inherited]
      end

      def collections
        if params[:collection_id]
          [ResourceRepository.find(params[:collection_id])]
        else
          ResourceRepository.find_all_of_model('Collection')
        end
      end

      def yield_results(resource, &)
        data = FlatResourceEntity.represent(resource, serializable: true)
        row = data.slice(*fields)

        row[:admin_set] ||= collection.admin_set if fields.include?(:admin_set)

        roles = include_inherited? ? resource.effective_roles : resource.roles
        roles.map! { |role| role.to_h.slice(*ROLE_FIELDS) }
        rows = roles.map { |role| row.merge(role) }
        rows.each(&)
      end

      def results
        Enumerator.new do |yielder|
          collections.each do |collection|
            yield_results(collection, &yielder)

            collection.children.each do |item|
              yield_results(item, &yielder) if include_inherited? || item.roles.any?
            end
          end
        end
      end
    end
  end
end
