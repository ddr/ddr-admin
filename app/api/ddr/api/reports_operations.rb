module Ddr
  module API
    module ReportsOperations
      extend Grape::API::Helpers

      def get_collection_summary_report
        filename = 'DDR-Collection-Summary-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
        header 'Content-Disposition', 'attachment; filename="%s"' % filename
        stream CollectionSummaryReport.call(params[:fields])
      end

      def get_duplicate_content_report
        filename = 'DDR-Duplicate-Content-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
        header 'Content-Disposition', 'attachment; filename="%s"' % filename
        stream DuplicateContentReport.call(params[:fields])
      end

      def get_roles_report
        if params[:collection_id]
          # Resource retrieval is repeated in report; this is a type guard and early not-found error.
          resource = ResourceRepository.find(params[:collection_id]) # not found -> 404
          error!('Resource must be a Collection', 400) unless resource.is_a?(Ddr::Collection)
        end

        filename = 'DDR-Roles-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
        header 'Content-Disposition', 'attachment; filename="%s"' % filename
        report = RolesReport.new(**declared(params))
        stream report.run
      end
    end
  end
end
