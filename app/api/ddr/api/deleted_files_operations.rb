module Ddr
  module API
    module DeletedFilesOperations
      extend Grape::API::Helpers

      include Pagination
      include CSVHelper

      def get_deleted_files
        filters = params.slice(:resource_id, :path, :file_field).compact
        filters[:repo_id] = filters.delete(:resource_id) if filters.key?(:resource_id)
        filters[:file_id] = filters.delete(:file_field) if filters.key?(:file_field)

        records = Ddr::DeletedFile.all
        records = records.where(**filters) if filters.present?
        records = records.where('created_at >= ?', params[:since]) if params[:since]
        records = records.where('created_at <= ?', params[:before]) if params[:before]

        if csv?
          filename = 'DDR-DeletedFiles-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
          stream_csv(filename:, records:, entity_class: DeletedFileEntity)
        else
          present paginate(records), with: DeletedFileEntity
        end
      end
    end
  end
end
