module Ddr
  module API
    #
    # Generates an "exposure" on a Grape::Entity subclass for an individual property,
    # and nested properties, if any.
    #
    class PropertyExposure
      # @param entity [Class] the Grape::Entity subclass
      # @param schema [Hash] the JSON Schema for the property
      # @param property [String] the property name
      # @param options [Hash] extra options for the `expose' method
      def self.call(entity:, schema:, property:, options: {}, &)
        opts = options.dup
        opts[:default] = schema['default'] if schema.key?('default')
        #
        # If the schema has an 'x-original-name' property, use that as the exposure name
        # and the property name as an alias:
        #
        # [schema]
        # new_name:
        #   type: string
        #   x-original-name: original_name
        #
        # [entity]
        # expose :original_name, as: :new_name
        #
        name = schema.fetch('x-original-name', property)
        opts[:as] = property if schema['x-original-name']

        # ::Rails.logger.debug('API') { "Exposure on #{entity} -- :#{name} #{opts.inspect}" }

        entity.expose(name, opts, &)
      end
    end
  end
end
