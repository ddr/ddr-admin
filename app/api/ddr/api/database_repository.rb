module Ddr
  module API
    #
    # @private This class should be considered a private implementation.
    #    Use Ddr::API::ResourceRepository instead for public methods.
    #
    class DatabaseRepository
      extend FullyQualifiedModel

      class << self
        delegate :run_query, to: :query_service
        delegate :orm_class, :to_resource, to: :resource_factory
        delegate :delete, to: :persister
        delegate :query_service, :resource_factory, :persister, to: :adapter

        def exist?(conditions)
          orm_class.exists?(conditions)
        end

        # Find a resource by ID
        # @param resource_id [String, Valkyrie::ID] resource ID
        # @return [Ddr::Resource] the resource
        # @raise [ResourceNotFoundError] the resource ID was not found
        def find(resource_id)
          query_service.find_by(id: resource_id)
        rescue Valkyrie::Persistence::ObjectNotFoundError => _e
          raise ResourceNotFoundError, resource_id
        end

        # Find resources by model
        # @param model [String]
        # @return [Enumerator<Ddr::Resource>]
        def find_all_of_model(model)
          query_service.find_all_of_model(model: fq_model(model))
        end

        # Find resource by permanent ID (ARK)
        # @param permanent_id [String]
        # @return [Ddr::Resource]
        # @raise [Ddr::API::ResourceNotFoundError] no match found
        # @raise [Dry::Types::ConstraintError] permanent_id is not a valid ARK
        def find_by_permanent_id(permanent_id)
          if found = find_by_metadata({ permanent_id: [Ddr::Types::PermanentID[permanent_id]] }).first
            return found
          end

          raise ResourceNotFoundError, "No results for permanent ID #{permanent_id.inspect}"
        end

        def find_collection_items_having_roles(collection_id)
          run_query(find_collection_items_having_roles_query, collection_id)
        end

        def find_by_metadata(metadata_hash, model: nil)
          metadata = JSON.dump(metadata_hash)

          case model
          when nil
            run_query(find_by_metadata_query, metadata)
          when String
            run_query(find_by_metadata_with_model_query, metadata, fq_model(model))
          when Array
            run_query(find_by_metadata_with_any_model_query, metadata, fq_model(model))
          end
        end

        # Find resource by role (using partial or complete role attributes)
        # @return [Enumerator<Ddr::Resource>]
        def find_by_role(params)
          role = params.slice(:role_type, :agent, :scope).compact

          find_by_metadata({ access_role: [role] }, model: params[:model])
        end

        def find_inverse_references_by(id:, property:, model: nil)
          find_by_metadata({ property => [{ id: }] }, model:)
        end

        def find_by_metadata_query
          'SELECT * FROM orm_resources WHERE metadata @> ? ORDER BY id ASC'
        end

        def find_by_metadata_with_model_query
          'SELECT * FROM orm_resources WHERE metadata @> ? and internal_resource = ? ORDER BY id ASC'
        end

        def find_by_metadata_with_any_model_query
          'SELECT * FROM orm_resources WHERE metadata @> ? AND internal_resource IN (?) ORDER BY id ASC'
        end

        def find_collection_items_having_roles_query
          <<-SQL.squish
            SELECT *#{' '}
            FROM orm_resources#{' '}
            WHERE internal_resource = 'Ddr::Item'#{' '}
              AND metadata->'parent_id'->0->>'id' = ?#{' '}
              AND metadata->'access_role'->0 IS NOT NULL#{' '}
            ORDER BY id ASC
          SQL
        end

        protected

        def adapter
          Valkyrie::MetadataAdapter.find(:postgres)
        end
      end
    end
  end
end
