module Ddr
  module API
    class DeletedFileEntity < Entity
      self.schema = Schema[:deleted_file]

      build_exposures
    end
  end
end
