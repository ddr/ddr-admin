module Ddr
  module API
    module CSVHelper
      def csv?
        env['api.format'] == :csv
      end

      def csv_timestamp
        DateTime.now.strftime('%Y%m%d%H%M%S')
      end

      def stream_csv(records:, entity_class:, filename:)
        header 'Content-Disposition', 'attachment; filename="%s"' % filename
        # Rack 2.x ETag middleware has some issue that affects streaming:
        # https://github.com/rack/rack/issues/1619#issuecomment-848460528
        header 'Last-Modified', Time.now.httpdate
        stream CSVStream.new(records, entity_class)
      end
    end
  end
end
