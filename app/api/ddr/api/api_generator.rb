module Ddr
  module API
    #
    # This class generates Grape API code for DDR API classes.
    #
    class APIGenerator
      HTTP_METHODS = %w[get put post delete patch].freeze

      DEFAULT_CONFIG = {
        helpers: %w[CommonHelpers].freeze,
        authenticate_user: true,
        curators_only: false,
        default_format: :json
      }.freeze

      def self.template_file
        ::File.expand_path('api_template.erb', __dir__)
      end

      def self.template
        @template ||= ERB.new(::File.read(template_file), trim_mode: '-')
      end

      # @return [Class] a generated API class
      def self.call(base_path, **config, &)
        Class.new(Grape::API) do |api_class|
          api_class.instance_exec(&) if block_given?

          APIGenerator.new(api_class, DEFAULT_CONFIG.merge(config.merge(base_path:)))
        end
      end

      Route = Struct.new(:path, :path_item, :http_method, :operation, keyword_init: true) do
        def desc
          operation['summary'] || operation['description'] || '[DESCRIPTION MISSING]'
        end

        def operation_id
          operation.fetch('operationId')
        end

        def params
          path_item_params = path_item.fetch('parameters', []).map { |p| APIParam.new(oapi_param: p) }
          operation_params = operation.fetch('parameters', []).map { |p| APIParam.new(oapi_param: p) }
          path_item_params + operation_params + request_body_params
        end

        def request_body_params
          return [] unless %w[post patch put].include?(http_method) && operation['requestBody']

          request_body = operation['requestBody']
          request_body_required = request_body.fetch('required', false)
          content = request_body.fetch('content') # content is required by OAPI spec
          content_type = content.keys.first       # has only one key

          raise NotImplementedError, "Unsupported request body content type: #{content_type.inspect}" unless [
            'application/json', 'multipart/form-data'
          ].include?(content_type)

          schema = content.dig(content_type, 'schema') || {}
          schema = SchemaMerger.call(*schema['allOf']) if schema.key?('allOf')
          schema = RefResolver.call(ref: schema['$ref']) if schema.key?('$ref')

          type = APIDataType.call(schema:)

          if content_type == 'application/json' && type != APIDataType::OBJECT_TYPE
            raise NotImplementedError,
                  "Unsupported JSON request body schema type: #{type.inspect}"
          end

          # reject readOnly and deprecated properties
          properties = schema.fetch('properties', {})
                             .reject { |_, prop| prop.fetch('readOnly', false) || prop.fetch('deprecated', false) }

          return [] if properties.empty?

          properties.map do |name, prop|
            APIParam.new(schema: prop, name:, skip_read_only: true, skip_deprecated: true).tap do |param|
              param.type = ::File if content_type == 'multipart/form-data' &&
                                     (content.dig(content_type, 'encoding', name,
                                                  'contentType') == 'application/octet-stream' ||
                                       (prop['type'] == 'string' && prop['format'] == 'binary')
                                     )
              param.required = request_body_required && schema.fetch('required', []).include?(name)
            end
          end
        end # request_body_params
      end # Route

      Config = Struct.new(:base_path, :helpers, :authenticate_user, :curators_only, :default_format,
                          keyword_init: true)

      delegate(*Config.members, to: :config)

      attr_reader :api_class, :config

      def initialize(api_class, config = {})
        @api_class = api_class
        @config = Config.new(**config)
        @api_class.class_eval(render)
      end

      # Evaluate the template with the generator as binding
      def render
        b = binding
        self.class.template.result(b)
      end

      def routes
        [].tap do |memo|
          paths.each do |path, path_item|
            path = path.gsub(/\{([^}]+)\}/, ':\1') # '/events/{id}' => '/events/:id'

            path_item.slice(*HTTP_METHODS).each do |http_method, operation|
              memo << Route.new(path:, path_item:, http_method:, operation:)
            end
          end
        end
      end

      def paths
        @paths ||= OpenAPIDocument.instance.fetch('paths').select { |key, _| key.start_with?(base_path) }
      end

      def path_items
        paths.values
      end

      def media_types
        types = Set.new

        path_items.each do |path_item|
          path_item.slice(*HTTP_METHODS).each_value do |operation|
            operation.fetch('responses', {}).each_value do |response|
              types.merge response.fetch('content', {}).keys
            end
          end
        end

        # text/plain triggers some kind of bug in Grape, so we exclude it.
        types.delete('text/plain')

        MEDIA_TYPES.slice(*types).invert
      end
    end
  end
end
