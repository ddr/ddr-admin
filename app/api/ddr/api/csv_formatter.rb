require 'csv'

module Ddr
  module API
    class CSVFormatter
      def self.call(data, _env)
        data = data.to_enum unless data.is_a?(Enumerator)

        headers = data.peek.to_csv.headers

        CSV.generate('', encoding: Encoding::UTF_8, headers:, write_headers: true) do |csv|
          loop do
            csv << data.next.to_csv
          rescue StopIteration => _e
            break
          end
        end
      end
    end
  end
end
