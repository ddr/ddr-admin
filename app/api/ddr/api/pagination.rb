module Ddr
  module API
    module Pagination
      include Pagy::Backend

      def add_page_headers
        return unless @pagy

        pagy_headers(@pagy).each do |key, value|
          header key, value
        end
      end

      def paginate(collection, add_headers = true)
        @pagy, page = pagy(collection)

        add_page_headers if add_headers

        page
      end

      def pagy_get_items(collection, pagy)
        return collection.page(pagy.page, pagy.items) if collection.is_a?(IndexQuery)

        # collection is an Enumerable (Array, Enumerator, Enumerator::Lazy, etc.)
        return collection.drop(pagy.offset).take(pagy.items).to_a unless collection.respond_to?(:offset)

        super
      end
    end
  end
end
