module Ddr
  module API
    #
    # ResourceRepository is the primary interface to repository data
    # -- i.e., the Postgres database and Solr index.
    #
    class ResourceRepository
      class << self
        delegate :exist?, :find, :find_all_of_model, :find_by_permanent_id, :find_by_role, :find_related_resources,
                 to: :database

        delegate :find_resources, :find_children, :find_members,
                 to: :index

        # Get children of a resource
        # @param resource_id [String, Valkyrie::ID] the resource ID
        # @return [Enumerator::Lazy] the child resources
        def children(resource_id)
          related_resources(id: resource_id, property: :parent_id)
        end

        # Get Component members of a Collection
        # @param collection_id [String, Valkyrie::ID] the Collection ID
        # @return [Enumerator::Lazy] the Components
        # @see #related_resources
        def components(collection_id)
          members(collection_id, model: 'Component')
        end

        # Get Collection members (Items and/or Components)
        # @param collection_id [String, Valkyrie::ID] the Collection ID
        # @param model [String, Symbol, Class] filter by member model
        # @return [Enumerator::Lazy] the member resoources
        # @see #related_resources
        def members(collection_id, model: nil)
          related_resources(id: collection_id, property: :admin_policy_id, model:)
        end

        # Get Targets associated with a Collection
        # @param collection_id [String, Valkyrie::ID] the Collection ID
        # @return [Enumerator::Lazy] the Targets
        # @see #related_resources
        def targets(collection_id)
          related_resources(id: collection_id, property: :for_collection_id)
        end

        # Get Attachments associated with a Collection
        # @param collection_id [String, Valkyrie::ID] the Collection ID
        # @return [Enumerator::Lazy] the Attachments
        # @see #related_resources
        def attachments(collection_id)
          related_resources(id: collection_id, property: :attached_to_id)
        end

        # Get the index document for a resource
        # @param resource_id [String, Valkyrie::ID] the resource ID
        # @return [SolrDocument] the index document
        def index_doc(resource_id)
          index.get_doc(resource_id)
        end

        # Get resources related to a resource by a certain property
        # @return [Enumerator::Lazy] the related resources
        # @param id [String, Valkyrie::ID] the resource ID
        # @param property [String, Symbol] the property name used for the relation
        # @param model [String, Symbol, Class] optional filter by related resource model
        def related_resources(id:, property:, model: nil)
          database.find_inverse_references_by(id:, property:, model:)
        end

        private

        def database
          DatabaseRepository
        end

        def index
          IndexRepository
        end
      end
    end
  end
end
