module Ddr
  module API
    module UsersOperations
      extend Grape::API::Helpers
      include Pagination

      def get_users
        users = ::User.all
        conditions = declared(params).except(:page, :per_page).compact

        if conditions.present?
          conditions[:username] = conditions.delete(:netid) + '@duke.edu' if conditions.key?(:netid)

          users = users.where(conditions)
        end

        if env['api.format'] == :csv
          filename = 'DDR-Users-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
          header 'Content-Disposition', 'attachment; filename="%s"' % filename

          # Rack 2.x ETag middleware has some issue that affects streaming:
          # https://github.com/rack/rack/issues/1619#issuecomment-848460528
          # This is supposedly one workaround:
          header 'Last-Modified', Time.now.httpdate

          stream CSVStream.new(users, UserEntity)
        else
          present paginate(users), with: UserEntity
        end
      end

      def post_users
        error! 'User creation via API is not allowed in the production environment', 405 if Rails.env.production?
        error! 'Username is taken: #{params[:username]}', 409 if ::User.find_by(username: params[:username])

        attributes = declared(params).compact.merge(password: Devise.friendly_token)
        user = ::User.create!(attributes)
        present user, with: UserEntity
      end

      def get_user
        user = ::User.find(params[:id])
        present user, with: UserEntity
      end
    end
  end
end
