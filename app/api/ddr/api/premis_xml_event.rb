module Ddr
  module API
    class PremisXMLEvent
      ARK_ID_TYPE = 'ARK'
      EVENT_ID_TYPE = 'DDR Event ID'
      DUKE_AGENT_ID_TYPE = 'Duke NetID'
      LOCAL_ID_TYPE = 'local'
      URN_ID_TYPE = 'URN'

      # We use 'failure' in the DDR Events model, but PREMIS uses 'fail'.
      FAIL_OUTCOME = 'fail'

      #
      # Serializes one or more Ddr::Events::Event objects, or an
      # ActiveRecord::Relation of Ddr::Events::Event objects, to PREMIS XML.
      #
      def self.call(events)
        Nokogiri::XML::Builder.with(PremisXMLDocument.root) do |xml|
          if events.respond_to?(:find_each)
            events.find_each do |event|
              append(xml, event)
            end
          elsif events.respond_to?(:each)
            events.each do |event|
              append(xml, event)
            end
          else
            append(xml, events)
          end
        end
      end

      def self.append(xml, event)
        # xml.event
        xml[:premis].event do
          xml[:premis].eventIdentifier do
            xml[:premis].eventIdentifierType(EVENT_ID_TYPE)
            xml[:premis].eventIdentifierValue(event.id)
          end

          xml[:premis].eventType(event.preservation_event_type)

          xml[:premis].eventDateTime(event.event_date_time_s)

          # N.B. What we call 'detail' in the Event class corresponds to
          # what PREMIS calls 'outcome detail'.
          # event.description is the class-level generic event description
          xml[:premis].eventDetailInformation do
            xml[:premis].eventDetail(event.description)
          end
          # Event summary is set to the same value as the description by default.
          if event.summary != event.description
            xml[:premis].eventDetailInformation do
              xml[:premis].eventDetail do
                xml.cdata(event.summary)
              end
            end
          end
          if event.comment.present?
            xml[:premis].eventDetailInformation do
              xml[:premis].eventDetail do
                xml.cdata(event.comment)
              end
            end
          end

          xml[:premis].eventOutcomeInformation do
            # We use 'failure' in the DDR Events model, but PREMIS uses 'fail'.
            if event.outcome == Ddr::Events::Event::FAILURE
              xml[:premis].eventOutcome(FAIL_OUTCOME)
            else
              xml[:premis].eventOutcome(event.outcome)
            end

            # N.B. What we call 'detail' in the Event class corresponds to
            # what PREMIS calls 'outcome detail'.
            if event.detail.present?
              xml[:premis].eventOutcomeDetail do
                xml[:premis].eventOutcomeDetailNote do
                  xml.cdata(event.detail)
                end
              end
            end
            # An exception is a tuple of exception type and exception message.
            # It may be that using eventOutcomeDetailExtension is more appropriate
            # for this information, but for now we're using eventOutcomeDetailNote.
            if event.exception.present?
              xml[:premis].eventOutcomeDetail do
                xml[:premis].eventOutcomeDetailNote(event.exception_type)
              end
              xml[:premis].eventOutcomeDetail do
                xml[:premis].eventOutcomeDetailNote do
                  xml.cdata(event.exception_message)
                end
              end
            end
          end

          xml[:premis].linkingAgentIdentifier do
            if agent = event.user_key
              xml[:premis].linkingAgentIdentifierType(DUKE_AGENT_ID_TYPE)
              xml[:premis].linkingAgentIdentifierValue(agent)
              xml[:premis].linkingAgentRole(event.user_agent_role)
            else
              xml[:premis].linkingAgentIdentifierType(LOCAL_ID_TYPE)
              xml[:premis].linkingAgentIdentifierValue(Ddr::Events::Event::SYSTEM)
              xml[:premis].linkingAgentRole(event.system_agent_role)
            end
          end

          xml[:premis].linkingObjectIdentifier do
            xml[:premis].linkingObjectIdentifierType(URN_ID_TYPE)
            xml[:premis].linkingObjectIdentifierValue(event.resource_urn)
            xml[:premis].linkingObjectRole(event.object_role)
          end

          if ark = event.resource_ark
            xml[:premis].linkingObjectIdentifier do
              xml[:premis].linkingObjectIdentifierType(ARK_ID_TYPE)
              xml[:premis].linkingObjectIdentifierValue(ark)
              xml[:premis].linkingObjectRole(event.object_role)
            end
          end
        end
      end
    end
  end
end
