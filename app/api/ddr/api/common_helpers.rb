module Ddr
  module API
    module CommonHelpers
      def logger
        ::Rails.logger
      end

      def current_user
        env['warden'].user(:user)
      end

      def current_ability
        @current_ability ||= APIAuthContext.new(current_user, env).ability
      end

      def deprecated_route!
        warn "[API] [DEPRECATION] Operation #{route.request_method} #{route.origin} is deprecated.",
             category: :deprecated
      end

      def unauthorized!
        raise UnauthorizedError, 'Forbidden'
      end

      # Mirrors the behavior of CanCan::ControllerAdditions#authorize!,
      # which is used in our Rails controllers.
      def authorize!(permission, object)
        @_authorized = true
        current_ability.authorize!(permission, object)
      end

      delegate :curator?, to: :current_ability

      def xml?
        env['api.format'] == :xml
      end
    end
  end
end
