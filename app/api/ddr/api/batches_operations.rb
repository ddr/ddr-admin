module Ddr
  module API
    module BatchesOperations
      extend Grape::API::Helpers

      include Pagination
      include CSVHelper

      def get_batches
        records = Ddr::Batch::Batch.where(params.slice(:outcome, :status, :user_id))
        records = records.accessible_by(current_ability) unless curator?

        if csv?
          filename = 'DDR-Batches-%s.csv' % csv_timestamp
          stream_csv(records:, entity_class: Ddr::API::BatchEntity, filename:)
        else
          present paginate(records), with: Ddr::API::BatchEntity
        end
      end

      def get_batch
        @batch = Ddr::Batch::Batch.find(params[:id])
        present @batch, with: Ddr::API::BatchEntity, batch_objects: params[:batch_objects]
      end

      def get_batch_batch_objects
        @batch = Ddr::Batch::Batch.find(params[:id])
        records = @batch.batch_objects

        filters = params.slice(:handled, :processed, :validated, :verified)

        if csv?
          filename = 'DDR-Batch-%s-BatchObjects.csv' % @batch.id

          records = records.where(filters) if filters.present?

          return stream_csv(records:, entity_class: Ddr::API::BatchObjectEntity, filename:)
        end

        if params[:batch_object_details]
          records = Ddr::Batch::BatchObject
                    .eager_load(:batch_object_messages,
                                :batch_object_attributes,
                                :batch_object_datastreams,
                                :batch_object_relationships,
                                :batch_object_roles)
                    .where(filters.merge(batch: @batch))
        elsif filters.present?
          records = records.where(filters)
        end

        present paginate(records), params.slice(:batch_object_details).merge(with: Ddr::API::BatchObjectEntity)
      end

      def get_batch_log
        redirect "/api/batches/#{params[:id]}/messages.csv", permanent: true
      end

      def get_batch_messages
        @batch = Ddr::Batch::Batch.find(params[:id])
        filters = params.slice(:handled, :processed, :validated, :verified)
        records = Ddr::Batch::BatchMessages.new(@batch, filters:).records

        if csv?
          filename = 'DDR-Batch-%s-Messages.csv' % @batch.id
          return stream_csv(records:, filename:, entity_class: Ddr::API::BatchObjectMessageEntity)
        end

        present paginate(records), with: Ddr::API::BatchObjectMessageEntity
      end
    end
  end
end
