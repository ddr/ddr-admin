module Ddr
  module API
    #
    # Static methods that return filter query strings.
    #
    module IndexFilters
      class << self
        def ability_filter(field, ability)
          return if ability.nil? || ability.superuser?

          any_value_filter(field, *ability.agents)
        end

        def admin_set_title_filter(title)
          value_filter(:admin_set_title_ssi, title, escape_spaces: true)
        end

        def any_value_filter(field, *values, **esc_opts)
          return unless values.present?

          value = escape(values, **esc_opts).join(' ')

          format('{!q.op=OR df=%<field>s}%<value>s', field:, value:)
        end

        def collection_filter(resource)
          collection_id_filter(resource.id)
        end

        def collection_id_filter(resource_id)
          id_filter(:admin_policy_id_ssi, resource_id)
        end

        def date_filter(field, after: nil, before: nil)
          format('%<field>s:[%<after>s TO %<before>s]', field:, after: solr_date(after) || '*',
                                                        before: solr_date(before) || '*')
        end

        def effective_download_filter(ability)
          ability_filter(:effective_download_ssim, ability)
        end

        def effective_read_filter(ability)
          ability_filter(:effective_read_ssim, ability)
        end

        def effective_role_filter(ability)
          ability_filter(:effective_role_ssim, ability)
        end

        def id_filter(field, resource_id)
          value_filter(field, "id-#{resource_id}")
        end

        def model_filter(*models)
          qual_models = FullyQualifiedModel.fq_model(models).compact

          return if qual_models.empty?

          value_filter(:internal_resource_ssim, qual_models)
        end

        def no_value_filter(field)
          format('-%<field>s:*', field:)
        end

        def not_value_filter(field, value)
          value_filter("-#{field}", value)
        end

        def original_filename_filter(filename)
          value_filter(:original_filename_ssi, filename, escape_spaces: true)
        end

        def parent_filter(resource)
          parent_id_filter(resource.id)
        end

        def parent_id_filter(parent_id)
          id_filter(:parent_id_ssi, parent_id)
        end

        def prefix_filter(field, prefix, **esc_opts)
          '%s*' % value_filter(field, prefix, **esc_opts)
        end

        def value_filter(field, value, **esc_opts)
          return if value.blank?

          if value.is_a?(::Array)
            return any_value_filter(field, *value, **esc_opts) if value.length > 1

            value = value.first
          end

          format('%<field>s:%<value>s', field:, value: escape(value, **esc_opts))
        end

        def workflow_state_filter(workflow_state)
          value_filter(:workflow_state_ssi, workflow_state)
        end

        private

        # Returns a string suitable to use as a Solr date
        # @param orig_date [Date, DateTime, Time] the original date/time
        # @return [String] the Solr date string
        def solr_date(orig_date)
          return if orig_date.blank?

          orig_date.to_time.utc.iso8601
        end

        def escape(value, **opts)
          return value.map { |v| escape(v, **opts) } if value.respond_to?(:map)

          RSolr.solr_escape(value.to_s).tap do |escaped|
            escaped.gsub!(/ /, '\\ ') if opts.fetch(:escape_spaces, false)
          end
        end
      end
    end
  end
end
