module Ddr
  module API
    #
    # Entity exposure for a property of type 'object' which is *not* an array.
    #
    # Essentially this will create an exposure with nested exposures for the
    # properties of the object, like so:
    #
    #   # Delegates subproperty access to the object
    #   alias_method :my_property, :object
    #
    #   expose :my_property do
    #     expose :subproperty1
    #     expose :subproperty2
    #     ...
    #   end
    #
    class ObjectPropertyExposure
      def self.call(entity:, schema:, property:, options: {})
        entity.alias_method property.to_sym, :object unless entity.public_method_defined?(property)

        nested = proc { ExposureBuilder.call(entity:, schema:, options:, parent: property) } # pass options thru?

        PropertyExposure.call(entity:, schema:, property:, options:, &nested)
      end
    end
  end
end
