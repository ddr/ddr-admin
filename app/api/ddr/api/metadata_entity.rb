module Ddr
  module API
    class MetadataEntity < Entity
      self.schema = Schema[:metadata]

      build_exposures

      def self.field_names
        schema.fetch('properties').keys
      end

      # Monkey patches https://duldev.atlassian.net/browse/DDK-131
      def available
        value = object.available
        value.is_a?(Array) ? value.first : value
      end
    end
  end
end
