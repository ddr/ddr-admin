module Ddr
  module API
    class Schema
      BASE_URI = 'https://api.repository.duke.edu/schemas/'

      def self.registry
        @registry ||= {}
      end

      private_class_method :registry

      def self.[](name)
        schema_name = name.to_s.delete_prefix(BASE_URI)

        registry[schema_name.to_sym] ||= DeepFreeze.call load_schema(schema_name)
      end

      def self.config_dir
        Rails.root.join('config/schemas')
      end

      # Load a schema from a YAML file.
      # @param schema_name [String, Symbol] the name of the schema
      # @raise [ArgumentError] if the schema is not found
      # @return [Hash] the schema as a hash
      def self.load_schema(schema_name)
        raise ArgumentError, "Schema '#{schema_name}' not found" unless names.include?(schema_name)

        schema_file = ::File.join(config_dir, "#{schema_name}.yaml")
        erb = ERB.new ::File.read(schema_file)
        YAML.load(erb.result)
      end

      # Get the names of all the schemas.
      # @return [Array<String>] the names of all the schemas
      def self.names
        @names ||= Dir.glob(::File.join(config_dir, '*.yaml')).map do |file|
          ::File.basename(file, '.yaml')
        end
      end
    end
  end
end
