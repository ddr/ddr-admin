module Ddr
  module API
    class BatchObjectFileEntity < Entity
      self.schema = Schema[:batch_object].dig('$defs', 'batch_object_file')

      build_exposures
    end
  end
end
