module Ddr
  module API
    # Base exception class
    Error = Class.new(StandardError)
    # 400 error
    BadRequestError = Class.new(::Ddr::API::Error)
    # 403 Forbidden
    UnauthorizedError = Class.new(::Ddr::API::Error)
    # 404 error
    NotFoundError = Class.new(::Ddr::API::Error)
    # 404 File not found exception
    FileNotFoundError = Class.new(::Ddr::API::NotFoundError)
    # 404 Resource not found exception
    ResourceNotFoundError = Class.new(::Ddr::API::NotFoundError)
    # 409 error
    ConflictError = Class.new(::Ddr::API::Error)

    VERSION = 'v1'
    TITLE = 'Duke Digital Repository API'

    # Advisory - see schema documents in {Rails.root}/config/api/schema
    JSON_SCHEMA_VERSION = 'https://json-schema.org/draft/2020-12/schema'

    UUID_RE  = /\A\h{8}-\h{4}-\h{4}-\h{4}-\h{12}\z/
    EMAIL_RE = Regexp.new('\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z')
    ARK_RE   = Regexp.new('\Aark:/\d+/\w+\z') # close enough

    STRING_FORMAT_REGEXES = {
      'uuid' => UUID_RE,
      'email' => EMAIL_RE
    }.freeze

    MAX_SYNC_UPLOAD = ENV.fetch('DDR_API_MAX_SYNC_UPLOAD', 100.megabytes).to_i

    def self.max_sync_upload_human
      @max_sync_upload_human ||= ActiveSupport::NumberHelper.number_to_human_size(MAX_SYNC_UPLOAD)
    end

    MAX_CONTENT_LENGTH = ENV.fetch('DDR_API_MAX_CONTENT_LENGTH', 1.gigabyte).to_i

    def self.max_content_length_human
      @max_content_length_human ||= ActiveSupport::NumberHelper.number_to_human_size(MAX_CONTENT_LENGTH)
    end

    MEDIA_TYPES = {
      'application/json' => :json,
      'application/schema+json' => :json,
      'text/csv' => :csv,
      # 'text/plain' => :txt, # This causes problems in Grape APIs for some reason
      'text/xml' => :xml
    }.freeze

    FORMATTERS = {
      'text/csv' => CSVFormatter
    }.freeze

    # Ensures references to Ruby's File class are resolved properly in this namespace,
    # and not to Ddr::File.
    File = ::File

    class CSVParamSplitter
      def self.call(value)
        value.split(',')
      end
    end

    def self.path
      ENV.fetch('DDR_API_MOUNT_PATH', '/api')
    end

    # TODO: This should probably be reconciled/merged with with the volume paths used in
    # batch ingest code, but we're adding it here for now to get the API working.
    def self.safe_upload_paths
      @safe_upload_paths ||= ENV.fetch('DDR_API_SAFE_UPLOAD_PATHS', '').split(':').freeze
    end

    def self.root_url
      @root_url ||= begin
        builder = default_url_options[:protocol] == 'https' ? URI::HTTPS : URI::HTTP
        options = default_url_options.slice(:host, :port).merge(path:)
        uri = builder.build(options)
        uri.to_s
      end
    end

    def self.url(path)
      root_url + path
    end

    def self.default_url_options
      @default_url_options ||= {}.tap do |opts|
        opts[:host] = Ddr::Admin.application_hostname || 'localhost'

        if opts[:host] == 'localhost'
          opts[:port] = ENV.fetch('RAILS_PORT', nil)
        else
          opts[:protocol] = 'https'
        end
      end
    end

    def self.noauth?
      return false if Rails.env.production?

      ENV['DDR_API_NOAUTH'].present?
    end

    def self.generate_api(...)
      APIGenerator.call(...)
    end

    AdminSetsAPI = generate_api('/admin_sets') do
      helpers AdminSetsOperations
    end

    BatchesAPI = generate_api('/batches', curators_only: true) do
      helpers BatchesOperations
    end

    BatchObjectsAPI = generate_api('/batch_objects', curators_only: true) do
      helpers BatchObjectsOperations
    end

    DeletedFilesAPI = generate_api('/deleted_files', curators_only: true) do
      helpers DeletedFilesOperations
    end

    EventsAPI = generate_api('/events', curators_only: true) do
      helpers EventsOperations
    end

    GroupsAPI = generate_api('/groups', curators_only: true) do
      helpers do
        def get_groups
          groups = GroupRepository.repository_groups
          present groups, with: GroupEntity, members: params[:include_members]
        end
      end
    end

    IndexAPI = generate_api('/index', curators_only: true) do
      helpers IndexOperations
    end

    QueuesAPI = generate_api('/queues', curators_only: true) do
      helpers do
        def delete_queues_locks
          { unlocked: ActiveJob::Uniqueness.unlock! }
        end

        def get_queues_stats
          redirect '/queues/stats', permanent: true
        end
      end
    end

    ReportsAPI = generate_api('/reports', curators_only: true, default_format: :csv) do
      helpers ReportsOperations
    end

    ResourcesAPI = generate_api('/resources') do
      before do
        logger.debug('API') do
          if route.request_method == 'GET'
            "#{route.request_method} #{route.origin} #{params}"
          else
            "#{route.request_method} #{route.origin}"
          end
        end
      end

      after_validation { load_and_authorize_resource }

      helpers ResourcesOperations
    end

    SchemasAPI = generate_api('/schemas', authenticate_user: false) do
      helpers do
        def get_schema
          Schema[params[:entity]]
        end
      end
    end

    UsersAPI = generate_api('/users', curators_only: true) do
      helpers UsersOperations
    end
  end
end

Grape::Middleware::Auth::Strategies.add(:api, Ddr::API::APIAuthMiddleware)

Grape::ContentTypes.register(:csv, 'text/csv')
Grape::ContentTypes.register(:json_schema, 'application/schema+json')
Grape::Formatter.register(:csv, Ddr::API::CSVFormatter)
Grape::Formatter.register(:json_schema, Grape::Formatter::Json)
