class ReportMailer < ApplicationMailer
  def basic(to:, content:, subject: 'Report', filename: 'report.csv', message: nil)
    body = message || 'The report you requested is attached.'
    attachments[filename] = content
    mail(to:, subject:, body:)
  end
end
