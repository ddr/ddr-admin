module Ddr
  class JobMailer < ApplicationMailer
    def basic(to:, subject: 'Job', message: nil)
      message || 'The job you enqueued has completed.'
      mail(from: from_address, to:, subject:, body: message)
    end

    private

    def from_address
      Rails.application.config.action_mailer.default_options[:from]
    rescue StandardError
      'job-queue@test.edu'
    end
  end
end
