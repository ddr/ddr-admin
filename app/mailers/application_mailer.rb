class ApplicationMailer < ActionMailer::Base
  default from: ENV.fetch('RAILS_MAILER_FROM_ADDRESS', 'from@example.com')
  layout 'mailer'
end
