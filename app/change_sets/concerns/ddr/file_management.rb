require 'openssl'

module Ddr
  module FileManagement
    extend ActiveSupport::Concern

    included do
      extend ActiveModel::Callbacks

      property :file_to_add, multiple: false, virtual: true
      property :virus_scan_result, multiple: false, virtual: true

      define_model_callbacks :add_file
      before_add_file :virus_scan
    end

    FileToAdd = Struct.new(:ddr_file_type, :source_path, :original_filename)

    # @param file [String, ::File, ActionDispatch::Http::UploadedFile]
    #   The file, or path to file, to add.
    # @param ddr_file_type [#to_sym] The file field to which file should be added.
    # @param mime_type [String] Explicit mime type to set
    #   (otherwise discerned from file path or name).
    # @param original_filename [String] original filename to override filename
    #   of file
    def add_file(file, ddr_file_type, mime_type: nil, original_filename: nil)
      # TODO: validate provided mime_type?
      mime_type         ||= MediaType.call(file)
      source_path         = Ddr::Utils.file_path(file)
      original_filename ||= Ddr::Utils.file_name(file)

      self.file_to_add = FileToAdd.new(ddr_file_type, source_path, original_filename)

      run_callbacks(:add_file) do
        add_ddr_file(file, ddr_file_type.to_sym, original_filename:, mime_type:)
      end
    end

    # @param file [String, ::File, ActionDispatch::Http::UploadedFile]
    #   The file, or path to file, to add.
    # @param ddr_file_type [#to_sym] The file field to which file should be added.
    # @param mime_type [String] Explicit mime type to set
    #   (otherwise discerned from file path or name).
    # @param original_filename [String] original filename to override filename
    #   of file
    def add_ddr_file(file, ddr_file_type, original_filename:, mime_type: nil)
      f = Ddr::Utils.file_path?(file) ? ::File.open(file) : file

      storage_adapter = ddr_file_storage_adapter(ddr_file_type)

      # upload the file to storage -> Valkyrie::StorageAdapter::File
      created_at = DateTime.now.utc
      stored_file = storage_adapter.upload(file: f, original_filename:, resource:, file_field: ddr_file_type)
      updated_at = DateTime.now.utc

      # get the SHA1 digest (Ddr::Digest instance)
      digest = ddr_file_digest(f)

      # instantiate the Ddr::File object
      # N.B. setting new_record to false + created_at + updated_at - NEW! (2024-02-20)
      ddr_file = File.new(digest:,
                          file_identifier: stored_file.id,
                          media_type: mime_type,
                          original_filename:,
                          created_at:,
                          updated_at:,
                          new_record: false)

      # update the change set
      send :"#{ddr_file_type.to_sym}=", ddr_file
    ensure
      f.close
    end

    # @param file [::File, ActionDispatch::Http::UploadedFile]
    # @param algorithm [String] digest algorithm
    # @return [Ddr::Digest] the digest
    # @see Valkyrie::StorageAdapter::File#checksum
    def ddr_file_digest(file, algorithm: Ddr::Files::CHECKSUM_TYPE_SHA1)
      # TODO: validate algorithm?
      file.rewind

      digest = OpenSSL::Digest.new(algorithm)
      created_at = DateTime.now.utc

      while chunk = file.read(65_536)
        digest << chunk
      end

      updated_at = DateTime.now.utc
      Ddr::Digest.new(type: algorithm, value: digest.to_s, created_at:, updated_at:, new_record: false)
    end

    def ddr_file_storage_adapter(ddr_file_type)
      Ddr::Storage.adapter_for(ddr_file_type)

      # ddr_file_type == :multires_image ? Valkyrie::StorageAdapter.find(:multires_image_disk) : Ddr.storage_adapter
    end

    protected

    def virus_scan
      result = Ddr::VirusCheck.call(file_to_add.source_path)
      self.virus_scan_result = result if file_to_add.ddr_file_type.to_sym == :content
    rescue ArgumentError => e # file is a blob (string)
      logger.error(e)
    end
  end
end
