module Ddr::V3
  #
  # FileUploadChangeSet[file_field].new(resource:)
  #
  # This module is not intended to be used directly;
  # instead, use FileUploadService[file_field] or FileDeletionService[file_field].
  #
  # @api private
  module FileChangeSet
    extend FileFieldSpecializationMixin

    specialize_on_file_field(ChangeSet) do
      property file_field, type: Ddr::File.optional, required: false, multiple: false

      define_method :update_file do |ddr_file|
        update_resource(file_field => ddr_file)
      end
    end
  end
end
