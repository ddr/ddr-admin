module Ddr::V3
  #
  # Change sets for metadata fields.
  #
  # This module is not intended to be used directly;
  # instead, use MetadataFieldChangeSet[field_name].
  #
  # @example
  #   change_set = MetadataFieldChangeSet[:title].new(resource)
  #   change_set.title = 'New title'
  #
  module MetadataFieldChangeSet
    # Retrieves the change set class for the field, generating a new class if necessary.
    # @param field_name [String, Symbol] metadata field name
    # @return [Class]
    def self.[](field_name)
      field = Ddr::Metadata[field_name]
      change_set_name = field.name.camelize

      return const_get(change_set_name) if const_defined?(change_set_name, false)

      const_set(change_set_name, generate_for_field(field_name.to_sym))

    rescue KeyError => e
      raise Ddr::Error, "Invalid metadata field name: #{field_name.inspect}"
    end

    # Generates a new change set class for the specified field
    # @param field_name [Symbol] the metadata field name
    # @return [Class]
    def self.generate_for_field(field_name)
      field = Ddr::Metadata[field_name]

      Class.new(ChangeSet) do
        property field_name, type: field.attribute_type, required: true, multiple: field.multiple?

        define_method :update_field do |value|
          update_resource(field_name => value)
        end
      end
    end

    private_class_method :generate_for_field
  end
end
