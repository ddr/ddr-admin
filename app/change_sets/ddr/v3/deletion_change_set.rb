module Ddr::V3
  #
  # Specialized change set for deleting a resource.
  #
  class DeletionChangeSet < ChangeSet
    def delete
      persister.delete(resource:)
    end
  end
end
