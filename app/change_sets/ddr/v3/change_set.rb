module Ddr::V3
  #
  # Base change set class.
  #
  class ChangeSet < Valkyrie::ChangeSet
    # @!attribute persister [rw]
    #   @return [void] a Valkyrie persister
    class_attribute :persister
    self.persister = Ddr.persister

    # @!attribute optimistic_lock_token [rw]
    #   @return [Valkyrie::Persistence::OptimisticLockToken]
    property :optimistic_lock_token, type: Ddr::Types::Set.of(Ddr::Types::OptimisticLockToken), multiple: true, required: true

    # Overrides Disposable::Twin::Save#save_model (via Reform::Form)
    # so that #save can be used in a more natural way.
    # @return [Ddr::Resource] the persisted resource
    def save_model
      persister.save(resource:)
    end

    # Validates attributes, syncs, and persists the resource.
    # @param attrs [Hash]
    # @return [Ddr::Resource] the persisted resource
    def update_resource(**attrs)
      unless validate(attrs)
        raise Ddr::Error, "Aborting resource changes due to validation errors: #{errors.full_messages}"
      end

      save
    end
  end
end
