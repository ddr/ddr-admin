module Ddr::V3
  #
  # Specialized change set for setting permanent IDs.
  #
  class PermanentIdChangeSet < ChangeSet
    PERMANENT_URL = "https://idn.duke.edu/"

    property :permanent_id, type: Types::PermanentID, required: true, multiple: false
    property :permanent_url, type: Types::URI, required: true, multiple: false

    def update_permanent_id(permanent_id)
      update_resource(permanent_id:, permanent_url: PERMANENT_URL + permanent_id)
    end
  end
end
