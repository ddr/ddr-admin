module Ddr
  class TargetChangeSet < Ddr::ResourceChangeSet
    %i[content
       fits_file
       for_collection_id].each do |attr|
      property attr, multiple: false
    end

    validate :valid_for_collection_id, if: proc { |a| a.for_collection_id.present? }

    private

    def valid_for_collection_id
      for_collection_class = Ddr.query_service.find_by(id: for_collection_id).class
      return if for_collection_class == Ddr::Collection

      errors.add(:for_collection_id, I18n.t('ddr.change_set.errors.for_collection_class'))
    end
  end
end
