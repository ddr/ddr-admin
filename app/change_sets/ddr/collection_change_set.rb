module Ddr
  class CollectionChangeSet < Ddr::ResourceChangeSet
    property :admin_set, multiple: false, required: true
    property :struct_metadata, multiple: false
    property :iiif_file, multiple: false

    # Redefining the title property from Ddr::ResourceChangeSet because it is required in this change set
    property :title, multiple: false, required: true, type: Ddr::Metadata[:title].attribute_type

    validates :admin_set, presence: true
    validates :title, presence: true

    def default_roles
      DefaultRoles[admin_set] + DefaultRoles['default']
    end

    def grant_roles_to_creator(creator)
      self.roles += [
        Ddr::Auth::Roles::Role.new(
          type: Ddr::Auth::Roles::CURATOR.title, agent: creator,
          scope: Ddr::Auth::Roles::POLICY_SCOPE
        )
      ]
    end
  end
end
