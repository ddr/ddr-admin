module Ddr
  class ItemChangeSet < Ddr::ResourceChangeSet
    %i[parent_id
       struct_metadata
       iiif_file].each do |attr|
      property attr, multiple: false
    end

    validate :valid_parent_id, if: proc { |a| a.parent_id.present? }

    private

    def valid_parent_id
      parent_class = Ddr.query_service.find_by(id: parent_id).class
      valid_parent_class = resource.parent_class
      return if parent_class == valid_parent_class

      errors.add(:parent_id, I18n.t('ddr.change_set.errors.parent_class', parent_class: valid_parent_class.name))
    end
  end
end
