module Ddr
  class ResourceChangeSet < Valkyrie::ChangeSet
    include FileManagement

    # ASCII control chars, except newline
    ILLEGAL_CHARS = Regexp.new('[\x00-\x09\x0B-\x1F]')

    Ddr::Metadata.fields.each do |name, field|
      property name, type: field.attribute_type, required: field.required?, multiple: field.multiple?
    end

    property :access_role, default: [], multiple: true # TODO: remove default?

    alias roles access_role
    alias roles= access_role=

    property :admin_policy_id, multiple: false # TODO: type: Valkyrie::Types::ID
    property :thumbnail, multiple: false       # TODO: type: Ddr::File

    property :optimistic_lock_token, multiple: true, required: true,
                                     type: Valkyrie::Types::Set.of(Valkyrie::Types::OptimisticLockToken)

    # Virtual properties
    property :comment, multiple: false, virtual: true
    property :detail, multiple: false, virtual: true
    property :skip_structure_updates, multiple: false, default: false, virtual: true
    property :skip_iiif_updates, multiple: false, default: false, virtual: true
    property :summary, multiple: false, virtual: true
    property :user, multiple: false, virtual: true

    validate :valid_admin_policy_id, if: proc { |a| a.admin_policy_id.present? }
    validate :valid_workflow_state_transition, if: proc { |a| a.workflow_state.present? }

    delegate :new_record?, to: :resource

    def self.field_names
      definitions.keys
    end

    def self.change_set_for(resource)
      change_set_class = "#{resource.class.name}ChangeSet".constantize
      change_set_class.new(resource)
    end

    def default_roles
      []
    end

    def admin_metadata_field_names(*args)
      return Ddr::HasAdminMetadata.term_names.map(&:to_s) if args.empty?

      arg = args.pop
      fields = case arg.to_sym
               when :present
                 admin_metadata_field_names.select { |f| send(f.to_sym).present? }
               when :required
                 admin_metadata_field_names.select { |f| required?(f) }
               end
      if args.empty?
        fields
      else
        fields | admin_metadata_field_names(*args)
      end
    end

    def desc_metadata_field_names(*args)
      return Ddr::Describable.term_names.map(&:to_s) if args.empty?

      arg = args.pop
      fields = case arg.to_sym
               when :present
                 desc_metadata_field_names.select { |f| send(f.to_sym).present? }
               when :required
                 desc_metadata_field_names.select { |f| required?(f) }
               end
      if args.empty?
        fields
      else
        fields | desc_metadata_field_names(*args)
      end
    end

    # @deprecated Use {#[]} instead.
    # @see Valkyrie::Resource
    def values(term)
      self[term]
    end

    def set_admin_metadata(admin_metadata_hash)
      admin_metadata_hash.each do |field_name, field_value|
        set_values(field_name, field_value)
      end
    end

    def set_desc_metadata(desc_metadata_hash)
      desc_metadata_hash.each do |field_name, field_value|
        set_values(field_name, field_value)
      end
    end

    # Update term with values
    # Note that empty values (nil or "") are rejected from values array
    def set_values(term, values)
      raise ArgumentError, "No such term: #{term}" unless self.class.field_names.include?(term.to_s)

      vals = Array(values).map { |v| sanitize_value(v) }.reject { |v| reject_value?(v) }
      value = if term == 'available'
                if vals.first.nil?
                  vals.first
                else
                  adjust_available_date(vals.first)
                end
              else
                multiple?(term) ? vals : vals.first
              end
      send(:"#{term}=", value)
    end

    # Add value to term
    # Note that empty value (nil or "") is not added
    def add_value(term, value)
      if !multiple?(term) && self[term].present?
        raise ArgumentError,
              "Cannot add another value to single-valued term: #{term}"
      end

      vals = Array(values(term)) + Array(value)
      set_values(term, vals)
    end

    def copy_admin_policy_or_roles_from(other)
      copy_resource_roles_from(other) unless copy_admin_policy_from(other)
    end

    def copy_admin_policy_from(other)
      self.admin_policy_id = if other.has_admin_policy?
                               other.admin_policy_id
                             elsif other.is_a?(Ddr::Collection)
                               other.id
                             end
    end

    def copy_resource_roles_from(other)
      self.roles += other.resource_roles
    end

    def grant_roles_to_creator(creator)
      role = Ddr::Auth::Roles::Role.new(type: Ddr::Auth::Roles::EDITOR.title, agent: creator,
                                        scope: Ddr::Auth::Roles::RESOURCE_SCOPE)
      self.roles += [role]
    end

    private

    # Available date values will typically come in as YYYY-MM-DD, and will default to
    # midnight UTC. This method pads the time by 5 hours (EST offset from UTC) so items don't
    # become available the evening before the date set.
    def adjust_available_date(date)
      if /(\+|-)\d\d:?\d\d$/.match?(date)
        ::DateTime.parse(date)
      else
        ::DateTime.parse(date) + Rational(5, 24)
      end
    rescue ArgumentError
      raise ArgumentError, I18n.t('ddr.change_set.errors.date_format')
    end

    def sanitize_value(value)
      value
        .to_s
        .strip
        .gsub(ILLEGAL_CHARS, '')
    end

    def reject_value?(value)
      value.blank?
    end

    def valid_admin_policy_id
      admin_policy_class = Ddr.query_service.find_by(id: admin_policy_id).class
      return if admin_policy_class == Ddr::Collection

      errors.add(:admin_policy_id, I18n.t('ddr.change_set.errors.admin_policy_class'))
      return unless errors.messages.to_h == {}

      raise 'valid_admin_policy_id messages empty after adding error'
    end

    # Workflow state cannot change directly from published to nonpublishable, because that would not
    # un-publish the resource first.
    def valid_workflow_state_transition
      return unless resource.workflow_state == 'published' && workflow_state == 'nonpublishable'

      errors.add(:workflow_state, I18n.t('ddr.change_set.errors.nonpub_workflow_state'))
    end
  end
end
