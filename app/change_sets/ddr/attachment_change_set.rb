module Ddr
  class AttachmentChangeSet < Ddr::ResourceChangeSet
    %i[attached_to_id
       content
       extracted_text
       fits_file].each do |attr|
      property attr, multiple: false
    end
  end
end
