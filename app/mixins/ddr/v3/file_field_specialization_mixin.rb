module Ddr::V3
  #
  # mixin designed to extend a class to provide sepcialized subclasses for each file field defined in DDR.
  #
  # The module provides a method, `specialize_on_file_field`, that creates the subclasses from
  # a base class and a block of code that is evaluated in the context of each subclass.
  #
  # The module also provides a class method, `[]`, that returns the subclass
  # for a given file field, which is the preferred way to access the subclass.
  #
  # @example
  #   module Ddr::V3
  #     class MyService < AbstractService
  #       extend FileFieldSpecializationMixin
  #
  #       specialize_on_file_field(ResourceService) do
  #         include ServiceJobConcern
  #       end
  #     end
  #  end
  #
  #   MyService[:content].call(resource: my_resource)
  #
  # @see Ddr::Files::FILE_FIELDS.
  #
  module FileFieldSpecializationMixin
    def [](file_field)
      const_get Types::FileField[file_field].to_s.camelize
    end

    def specialize_on_file_field(base_class, file_fields = Ddr::Files::FILE_FIELDS, &block)
      file_fields.each do |file_field|
        const_set(
          file_field.to_s.camelize,
          Class.new(base_class) do
            class_attribute :file_field, instance_writer: false
            self.file_field = file_field

            class_eval(&block) if block
          end
        )
      end
    end
  end
end
