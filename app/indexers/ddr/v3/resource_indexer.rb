module Ddr::V3
  class ResourceIndexer
    attr_reader :resource

    delegate_missing_to :resource

    def initialize(resource:)
      @resource = resource
    end

    def to_solr
      index_fields
    rescue Ddr::SolrDocumentBehavior::NotFound => e
      raise IndexError, "Indexing failed on resource #{resource.id.id} (original exception: #{e})"
    end

    # Valkryie's SolrPersister adds a number of fields to Solr on its own; in general, those needed to reinstantiate
    # the resource being persisted.  Included among these are all the resource attributes (I believe).  Each field is
    # added in a number of different ways; e.g., _tsim, _ssim, _tesim, _tsi, _ssi, _tesi, and perhaps others that are
    # not "stored".
    # The indexing accounted for below covers those cases where SolrPersister either does not or might not index a
    # field in a particular way.
    def index_fields
      fields = {
        access_role_ssi: roles.to_json,
        admin_set_title_ssi: admin_set_title,
        attached_files_having_content_ssim: attached_files_having_content.map(&:to_s),
        biblical_book_facet_sim: biblical_book,
        category_facet_sim: category, # use _ssim ?
        chapter_and_verse_facet_sim: chapter_and_verse, # use _ssim?
        collection_title_ssi: collection_title,
        common_model_name_ssi: common_model_name,
        company_facet_sim: company,
        contributor_facet_sim: contributor,
        creator_facet_sim: creator,
        date_facet_sim: date,
        date_sort_si: date_sort,
        effective_role_ssim: agents_having_effective_role,
        effective_read_ssim: agents_having_effective_permission(:read),
        effective_download_ssim: agents_having_effective_permission(:download),
        embargo_dtsi: resource.embargo,
        folder_facet_sim: folder,
        format_facet_sim: resource.format,
        genre_facet_sim: genre,
        identifier_all_ssim: all_identifiers,
        ingestion_date_dtsi: ingestion_date,
        language_facet_sim: language_name,
        language_name_tesim: language_name,
        medium_facet_sim: medium,
        placement_company_facet_sim: placement_company,
        policy_role_ssim: policy_roles.map(&:agent),
        product_facet_sim: product,
        publication_facet_sim: publication,
        publisher_facet_sim: publisher,
        resource_role_ssim: resource_roles.map(&:agent),
        series_facet_sim: series,
        setting_facet_sim: setting,
        spatial_facet_sim: spatial,
        subject_facet_sim: subject,
        temporal_facet_sim: temporal,
        tone_facet_sim: tone,
        type_facet_sim: type,
        volume_facet_sim: volume,
        year_facet_iim: year_facet
      }

      fields.merge!(title_ssi: title_display) unless title.present?

      # Last fixity check
      fields.merge!(last_fixity_check.to_solr) if last_fixity_check.present?

      # Last virus check
      fields.merge!(last_virus_check.to_solr) if last_virus_check.present?

      if has_content?
        fields[:content_create_date_dtsi] = Ddr::Utils.solr_date(content.timestamps.created)
        fields[:content_sha1_ssi] = content.sha1
        fields[:content_size_lsi] = content_size
        fields[:content_size_human_ssim] = content_human_size
        fields[:content_media_type_ssim] = content_type
        fields[:content_media_major_type_sim] = content_major_type
        fields[:content_media_sub_type_sim] = content_sub_type
        fields[:original_filename_ssi] = original_filename
        fields.merge! techmd_index_fields
      end

      fields[:multires_image_file_path_ssi] = multires_image_file_path if has_multires_image?

      fields[:derived_image_file_path_ssi] = derived_image_file_path if has_derived_image?

      if has_struct_metadata?
        fields[:structure_ss] = structure.dereferenced_structure.to_json

        # deprecated - all structural metadata is repository maintained
        fields[:structure_source_ssi] = if structure.repository_maintained?
                                          Ddr::Structure::REPOSITORY_MAINTAINED
                                        else
                                          Ddr::Structure::EXTERNALLY_PROVIDED
                                        end
      end

      fields[:extracted_text_tsm] = extracted_text.content if has_extracted_text?

      fields[:streamable_media_type_ssi] = streamable_media_type if streamable?

      if resource.is_a? Ddr::Component
        # Should use 'collection_id_ssi' b/c single-valued.
        fields[:collection_id_ssim] = collection_id&.id

        # deprecated
        fields[:is_part_of_ssim] = parent_id&.id # for backwards compatibility
      end

      if resource.is_a? Ddr::Collection
        fields[:admin_set_facet_sim] = admin_set_facet
        fields[:collection_facet_sim] = collection_facet
      end

      if resource.is_a? Ddr::Item
        # These two fields are duplicated above
        fields[:admin_set_facet_sim] = admin_set_facet
        fields[:collection_facet_sim] = collection_facet

        # deprecated
        fields[:is_member_of_collection_ssim] = parent_id&.id # for backwards compatibility

        # ???
        fields[:nested_path_text_teim] = nested_path

        fields[:all_text_timv] = all_text
      end

      fields
    end

    def associated_collection
      admin_policy
    end

    def admin_set_facet
      if admin_set.present?
        admin_set
      elsif associated_collection.present?
        associated_collection.admin_set
      end
    end

    def admin_set_title
      code = if admin_set.present?
               admin_set
             elsif associated_collection.present?
               associated_collection.admin_set
             end
      return unless code && (as = Ddr::AdminSet.find_by_code(code))

      as.title
    end

    def collection_facet
      associated_collection.id.id if associated_collection.present?
    end

    def collection_title
      if instance_of?(Ddr::Collection)
        title_display
      elsif associated_collection.present?
        associated_collection.title_display
      end
    end

    def date_sort
      date.first
    end

    def language_name
      language.map do |lang|
        Ddr::Language.find_by_code(lang).to_s
      rescue Ddr::NotFoundError
        lang
      end
    end

    def last_fixity_check
      Ddr::Events::FixityCheckEvent.for_resource_id(resource.id).last
    end

    def last_virus_check
      Ddr::Events::VirusCheckEvent.for_resource_id(resource.id).last
    end

    def year_facet
      Ddr::YearFacet.call(self)
    end

    def techmd_index_fields
      {
        techmd_color_space_ssim: techmd.color_space,
        techmd_creating_application_ssim: techmd.creating_application,
        techmd_creation_time_dtsim: Ddr::Utils.solr_dates(techmd.creation_time),
        techmd_file_size_lsi: techmd.file_size,

        # TODO: Temporary work-around until techmd.fits_datetime is fixed
        # techmd_fits_datetime_dtsi: Ddr::Utils.solr_date(techmd.fits_datetime),

        techmd_fits_version_ssi: techmd.fits_version,
        techmd_format_label_ssim: techmd.format_label,
        techmd_format_version_ssim: techmd.format_version,
        techmd_icc_profile_name_ssim: techmd.icc_profile_name,
        techmd_icc_profile_version_ssim: techmd.icc_profile_version,
        techmd_image_height_isim: techmd.image_height,
        techmd_image_width_isim: techmd.image_width,

        # deprecated - We are no longer calculating MD5 digests with FITS.
        techmd_md5_ssi: techmd.md5,

        techmd_media_type_ssim: techmd.media_type,
        techmd_modification_time_dtsim: Ddr::Utils.solr_dates(techmd.modification_time),
        techmd_pronom_identifier_ssim: techmd.pronom_identifier,
        techmd_valid_ssim: techmd.valid,
        techmd_well_formed_ssim: techmd.well_formed
      }
    end

    def agents_having_effective_role
      effective_roles.map(&:agent).uniq
    end

    def agents_having_effective_permission(perm)
      effective_roles.select { |r| r.permissions.include?(perm) }.map(&:agent).uniq
    end

    def effective_roles
      @effective_roles ||= resource.effective_roles
    end
  end
end
