module Ddr
  # @deprecated Use Ddr::V3::ResourceIndexer.
  class ResourceIndexer
    include Ddr::Index::Fields

    attr_reader :resource

    delegate_missing_to :resource

    def initialize(resource:)
      @resource = resource
    end

    def to_solr
      index_fields
    rescue Ddr::SolrDocumentBehavior::NotFound => e
      raise IndexError, "Indexing failed on resource #{resource.id.id} (original exception: #{e})"
    end

    # Valkryie's SolrPersister adds a number of fields to Solr on its own; in general, those needed to reinstantiate
    # the resource being persisted.  Included among these are all the resource attributes (I believe).  Each field is
    # added in a number of different ways; e.g., _tsim, _ssim, _tesim, _tsi, _ssi, _tesi, and perhaps others that are
    # not "stored".
    # The indexing accounted for below covers those cases where SolrPersister either does not or might not index a
    # field in a particular way.
    def index_fields
      fields = {
        ACCESS_ROLE => roles.to_json,
        ADMIN_SET_TITLE => admin_set_title,
        ASPACE_ID => aspace_id,
        ATTACHED_FILES_HAVING_CONTENT => attached_files_having_content.map(&:to_s),
        BIBLICAL_BOOK_FACET => biblical_book,
        BIBSYS_ID => bibsys_id,
        CATEGORY_FACET => category,
        CHAPTER_AND_VERSE_FACET => chapter_and_verse,
        COLLECTION_TITLE => collection_title,
        COMMON_MODEL_NAME => common_model_name,
        COMPANY_FACET => company,
        CONTENTDM_ID => contentdm_id,
        CONTRIBUTOR_FACET => contributor,
        CREATOR_FACET => creator,
        DATE_FACET => date,
        DATE_SORT => date_sort,
        DC_IS_PART_OF => isPartOf,
        DESCRIPTION => description,
        DISPLAY_FORMAT => display_format,
        EAD_ID => ead_id,
        EFFECTIVE_ROLE => agents_having_effective_role,
        'effective_read_ssim' => agents_having_effective_permission(:read),
        'effective_download_ssim' => agents_having_effective_permission(:download),
        EMBARGO_DATE => resource.embargo,
        FCREPO3_PID => fcrepo3_pid,
        FOLDER_FACET => folder,
        FORMAT_FACET => resource.format,
        GENRE_FACET => genre,
        IDENTIFIER_ALL => all_identifiers,
        INGESTED_BY => ingested_by,
        INGESTION_DATE => ingestion_date,
        LANGUAGE_FACET => language_name,
        LANGUAGE_NAME => language_name,
        MEDIUM_FACET => medium,
        PLACEMENT_COMPANY_FACET => placement_company,
        POLICY_ROLE => policy_roles.map(&:agent),
        PRODUCT_FACET => product,
        PUBLICATION_FACET => publication,
        PUBLISHER_FACET => publisher,
        RESOURCE_ROLE => resource_roles.map(&:agent),
        SERIES_FACET => series,
        SETTING_FACET => setting,
        SPATIAL_FACET => spatial,
        SUBJECT_FACET => subject,
        TEMPORAL_FACET => temporal,
        TITLE => title_display,
        TONE_FACET => tone,
        TYPE_FACET => type,
        VOLUME_FACET => volume,
        YEAR_FACET => year_facet
      }
      # Last fixity check
      fields.merge!(last_fixity_check.to_solr) if last_fixity_check.present?
      # Last virus check
      fields.merge!(last_virus_check.to_solr) if last_virus_check.present?
      if has_content?
        fields[CONTENT_CREATE_DATE] = Ddr::Utils.solr_date(content.timestamps.created)
        fields[CONTENT_SHA1] = content.sha1
        fields[CONTENT_SIZE] = content_size
        fields[CONTENT_SIZE_HUMAN] = content_human_size
        fields[MEDIA_TYPE] = content_type
        fields[MEDIA_MAJOR_TYPE] = content_major_type
        fields[MEDIA_SUB_TYPE] = content_sub_type
        fields[ORIGINAL_FILENAME] = original_filename
        fields.merge! techmd_index_fields
      end
      fields[MULTIRES_IMAGE_FILE_PATH] = multires_image_file_path if has_multires_image?
      fields[DERIVED_IMAGE_FILE_PATH] = derived_image_file_path if has_derived_image?
      if has_struct_metadata?
        fields[STRUCTURE] = structure.dereferenced_structure.to_json
        fields[STRUCTURE_SOURCE] = if structure.repository_maintained?
                                     Ddr::Structure::REPOSITORY_MAINTAINED
                                   else
                                     Ddr::Structure::EXTERNALLY_PROVIDED
                                   end
      end
      fields[EXTRACTED_TEXT] = extracted_text.content if has_extracted_text?
      fields[STREAMABLE_MEDIA_TYPE] = streamable_media_type if streamable?
      if resource.is_a? Ddr::Component
        fields[COLLECTION_ID] = collection_id&.id
        fields[IS_PART_OF] = parent_id&.id # for backwards compatibility
      end
      if resource.is_a? Ddr::Collection
        fields[ADMIN_SET_FACET] = admin_set_facet
        fields[COLLECTION_FACET] = collection_facet
      end
      if resource.is_a? Ddr::Item
        fields[ADMIN_SET_FACET] = admin_set_facet
        fields[COLLECTION_FACET] = collection_facet
        fields[IS_MEMBER_OF_COLLECTION] = parent_id&.id # for backwards compatibility
        fields[NESTED_PATH_TEXT] = nested_path
        fields[ALL_TEXT] = all_text
      end
      fields
    end

    def associated_collection
      admin_policy
    end

    def admin_set_facet
      if admin_set.present?
        admin_set
      elsif associated_collection.present?
        associated_collection.admin_set
      end
    end

    def admin_set_title
      code = if admin_set.present?
               admin_set
             elsif associated_collection.present?
               associated_collection.admin_set
             end
      return unless code && (as = Ddr::AdminSet.find_by_code(code))

      as.title
    end

    def collection_facet
      associated_collection.id.id if associated_collection.present?
    end

    def collection_title
      if instance_of?(Ddr::Collection)
        title_display
      elsif associated_collection.present?
        associated_collection.title_display
      end
    end

    def date_sort
      date.first
    end

    def language_name
      language.map do |lang|
        Language.find_by_code(lang).to_s
      rescue StandardError
        lang
      end
    end

    def last_fixity_check
      Ddr::Events::FixityCheckEvent.for_resource_id(resource.id).last
    end

    def last_virus_check
      Ddr::Events::VirusCheckEvent.for_resource_id(resource.id).last
    end

    def year_facet
      YearFacet.call(self)
    end

    def techmd_index_fields
      { TECHMD_FITS_VERSION => techmd.fits_version,
        # TODO: Temporary work-around until techmd.fits_datetime is fixed
        # TECHMD_FITS_DATETIME     => Ddr::Utils.solr_date(techmd.fits_datetime),
        TECHMD_CREATION_TIME => Ddr::Utils.solr_dates(techmd.creation_time),
        TECHMD_MODIFICATION_TIME => Ddr::Utils.solr_dates(techmd.modification_time),
        TECHMD_COLOR_SPACE => techmd.color_space,
        TECHMD_CREATING_APPLICATION => techmd.creating_application,
        TECHMD_FILE_SIZE => techmd.file_size,
        TECHMD_FORMAT_LABEL => techmd.format_label,
        TECHMD_FORMAT_VERSION => techmd.format_version,
        TECHMD_ICC_PROFILE_NAME => techmd.icc_profile_name,
        TECHMD_ICC_PROFILE_VERSION => techmd.icc_profile_version,
        TECHMD_IMAGE_HEIGHT => techmd.image_height,
        TECHMD_IMAGE_WIDTH => techmd.image_width,
        TECHMD_MD5 => techmd.md5,
        TECHMD_MEDIA_TYPE => techmd.media_type,
        TECHMD_PRONOM_IDENTIFIER => techmd.pronom_identifier,
        TECHMD_VALID => techmd.valid,
        TECHMD_WELL_FORMED => techmd.well_formed }
    end

    def agents_having_effective_role
      effective_roles.map(&:agent).uniq
    end

    def agents_having_effective_permission(perm)
      effective_roles.select { |r| r.permissions.include?(perm) }.map(&:agent).uniq
    end

    def effective_roles
      @effective_roles ||= resource.effective_roles
    end
  end
end
