class ApplicationController < ActionController::Base
  # Adds a few additional behaviors into the application controller
  include Blacklight::Controller

  layout 'blacklight'

  include Ddr::Auth::RoleBasedAccessControlsEnforcement

  helper_method :acting_as_superuser?

  rescue_from CanCan::AccessDenied, with: :forbidden

  # Uncomment to glabally require user authentication:
  before_action :authenticate_user!

  before_action :configure_devise_params, if: :devise_controller?

  delegate :curator?, to: :current_ability

  helper_method :curator?

  protected

  def acting_as_superuser?
    signed_in?(:superuser)
  end

  def configure_devise_params
    devise_parameter_sanitizer.permit(:sign_in, keys: %i[email username])
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[email username])
  end

  def render_404
    respond_to do |format|
      format.html do
        render file: ::File.expand_path(Rails.public_path.join('404.html')), layout: false, status: :not_found
      end
      format.any { head :not_found }
    end
  end

  def forbidden
    render file: ::File.expand_path(Rails.public_path.join('403.html')),
           formats: [:html],
           status: :forbidden,
           layout: false
  end
end
