class AdminController < ApplicationController
  before_action :forbidden, unless: :curator?

  def api; end

  def expire_public_rails_cache
    Ddr::V3::ExpirePublicRailsCacheService.call

    flash[:success] = 'Public cache expiration job created.'

    redirect_to root_url
  end

  def queues; end
end
