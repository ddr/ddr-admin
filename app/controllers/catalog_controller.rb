# frozen_string_literal: true

class CatalogController < ApplicationController
  include Blacklight::Catalog

  # This is needed for the Blacklight 6 -> 7 migration, since
  # we can no longer access the request context by default.
  # See:
  # - https://bibwild.wordpress.com/2019/04/30/blacklight-7-current_user-or-other-request-context-in-searchbuilder-solr-query-builder/
  # - https://github.com/projectblacklight/blacklight/blob/v8.0.0/app/controllers/concerns/blacklight/searchable.rb#L27-L33
  def search_service_context
    { current_ability: }
  end

  configure_blacklight do |config|
    ## Default parameters to send to solr for all search-like requests. See also SearchBuilder#processed_parameters
    config.default_solr_params = {
      rows: 10
    }

    config.index.title_field = Ddr::Index::Fields::TITLE

    config.index.thumbnail_method = :thumbnail_image_tag

    config.add_facet_field Ddr::Index::Fields::COMMON_MODEL_NAME.to_s, label: 'Type'
    config.add_facet_field Ddr::Index::Fields::ADMIN_SET_TITLE.to_s, label: 'Admin Set'
    config.add_facet_field Ddr::Index::Fields::WORKFLOW_STATE.to_s, label: 'Publication Status', query: {
      published: { label: 'Published', fq: "#{Ddr::Index::Fields::WORKFLOW_STATE}:published" },
      previewable: { label: 'Previewable', fq: "#{Ddr::Index::Fields::WORKFLOW_STATE}:previewable" },
      not_published: { label: 'Not Published', fq: "-#{Ddr::Index::Fields::WORKFLOW_STATE}:published" },
      nonpublishable: { label: 'Not Publishable', fq: "#{Ddr::Index::Fields::WORKFLOW_STATE}:nonpublishable" }
    }

    # Have BL send all facet field names to Solr, which has been the default
    # previously. Simply remove these lines if you'd rather use Solr request
    # handler defaults, or have no facets.
    config.add_facet_fields_to_solr_request!

    #  solr fields to be displayed in the index (search results) view
    #  The ordering of the field names is the order of the display
    Ddr::Admin.index_list_entry_fields.each do |field|
      config.add_index_field(Ddr::Index::Fields.get(field).to_s, label: I18n.t("ddr.index_list_entry.#{field}"))
    end

    config.add_search_field('all_fields', label: 'All Fields') do |field|
      field.solr_parameters = {
        qf: ['id',
             'abstract_tesim',
             'alternative_tesim',
             'artist_tesim',
             'biblical_book_tesim',
             'bibliographicCitation_tesim',
             'category_tesim',
             'chapter_and_verse_tesim',
             'company_tesim',
             'creator_tesim',
             'contributor_tesim',
             'description_tesim',
             'extent_tesim',
             'folder_tesim',
             'format_tesim',
             'genre_tesim',
             'headline_tesim',
             'identifier_tesim',
             'isPartOf_tesim',
             'isReferencedBy_tesim',
             'issue_number_tesim',
             'language_name_tesim',
             'medium_tesim',
             'nested_path_text_teim',
             'placement_company_tesim',
             'product_tesim',
             'provenance_tesim',
             'publication_tesim',
             'publisher_tesim',
             'rights_tesim',
             'series_tesim',
             'setting_tesim',
             'spatial_tesim',
             'sponsor_tesim',
             'subject_tesim',
             'temporal_tesim',
             'title_tesim',
             'tone_tesim',
             'type_tesim',
             'volume_tesim',
             Ddr::Index::Fields::ALL_TEXT,
             Ddr::Index::Fields::IDENTIFIER_ALL,
             Ddr::Index::Fields::YEAR_FACET].join(' ')
      }
    end

    # Now we see how to over-ride Solr request handler defaults, in this
    # case for a BL "search field", which is really a dismax aggregate
    # of Solr search fields.

    config.add_search_field('title') do |field|
      # solr_parameters hash are sent to Solr as ordinary url query params.
      field.solr_parameters = {
        'spellcheck.dictionary': 'title',
        qf: '${title_qf}',
        pf: '${title_pf}'
      }
    end

    config.add_search_field('identifier') do |field|
      field.solr_parameters = {
        qf: Ddr::Index::Fields::IDENTIFIER_ALL
      }
    end

    # "sort results by" select (pulldown)
    # label in pulldown is followed by the name of the SOLR field to sort by and
    # whether the sort is ascending or descending (it must be asc or desc
    # except in the relevancy case).
    config.add_sort_field 'score desc', label: 'Relevance', default_for_user_query: true
    config.add_sort_field "#{Ddr::Index::Fields::TITLE} asc", label: 'Title', default: true
    config.add_sort_field "#{Ddr::Index::Fields::LOCAL_ID} asc", label: 'Local ID'
    config.add_sort_field "#{Ddr::Index::Fields::INGESTION_DATE} desc", label: 'Ingest Date'

    # If there are more than this many search results, no spelling ("did you
    # mean") suggestion is offered.
    config.spell_max = 5

    # Blacklight 8 will filter out non-search parameters, extra ones we want to include
    # need to be specified here:
    config.filter_search_state_fields = true
    config.search_state_fields += [:id]
  end
end
