module Ddr
  class PermanentIdsController < ApplicationController
    def show
      redirect_to resolve_id
    end

    protected

    def resolve_id
      permanent_id = params.require(:permanent_id)

      SolrDocument.find_by_permanent_id(permanent_id)
    end
  end
end
