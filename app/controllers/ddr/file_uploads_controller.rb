module Ddr
  class FileUploadsController < ApplicationController
    before_action :new_file_upload, only: %i[new create]
    load_resource
    before_action :authorize_create, only: [:create]

    def new_file_upload
      @file_upload = FileUpload.new(file_upload_params)
    end

    def create
      authorize! :create, @file_upload
      @file_upload.user = current_user
      if @file_upload.valid?
        FileUploadJob.perform_later(
          'basepath' => @file_upload.basepath,
          'batch_user' => current_user.user_key,
          'checksum_file' => @file_upload.checksum_file,
          'collection_id' => @file_upload.collection_id,
          'ddr_file_name' => @file_upload.ddr_file_name,
          'subpath' => @file_upload.subpath
        )
        render 'queued'
      else
        render 'new'
      end
    end

    private

    def file_upload_params
      params.require(:ddr_file_upload).permit(:basepath, :checksum_file, :collection_id,
                                              :ddr_file_name, :subpath)
    end

    def authorize_create
      return unless (collection_id = @file_upload.collection_id).present?

      if can?(:upload, collection_id)
        current_ability.can(:create, FileUpload, collection_id:)
      else
        current_ability.cannot :create, FileUpload, collection_id:
      end
    end
  end
end
