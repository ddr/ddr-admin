module Ddr
  class ManifestBasedIngestsController < ApplicationController
    load_resource
    before_action :authorize_create, only: [:create]

    def create
      authorize! :create, @manifest_based_ingest
      @manifest_based_ingest.user = current_user
      if @manifest_based_ingest.valid?
        job_args = {
          'admin_set' => @manifest_based_ingest.admin_set,
          'batch_user' => @manifest_based_ingest.user.user_key,
          'collection_id' => @manifest_based_ingest.collection_id,
          'collection_title' => @manifest_based_ingest.collection_title,
          'config_file' => @manifest_based_ingest.config_file
        }
        unless @manifest_based_ingest.manifest_file.empty?
          job_args.merge!({ 'manifest_file' => @manifest_based_ingest.manifest_file })
        end
        if create_params.has_key?(:manifest)
          path = ::File.join(Ddr::Admin.data_dir, 'ddr_manifests')
          Dir.mkdir path unless ::File.exist? path
          manifest = ::File.open(::File.join(path, "#{SecureRandom.uuid}.txt"), 'wb') do |f|
            f.write(create_params[:manifest].read)
            f.path
          end
          job_args.merge!({ 'manifest_file' => manifest })
        end
        ManifestBasedIngestJob.perform_later(job_args)
        render 'queued'
      else
        render 'new'
      end
    end

    private

    def create_params
      params.require(:ddr_manifest_based_ingest).permit(:manifest_file, :manifest, :admin_set,
                                                        :collection_id, :collection_title, :config_file)
    end

    def authorize_create
      return unless (collection_id = @manifest_based_ingest.collection_id).present?

      if can?(:add_children, collection_id)
        current_ability.can(:create, ManifestBasedIngest, collection_id:)
      else
        current_ability.cannot :create, ManifestBasedIngest, collection_id:
      end
    end
  end
end
