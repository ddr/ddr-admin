module Ddr
  class TargetsController < ResourcesController
    include Controller::HasContentBehavior

    delegate :for_collection, to: :current_object

    helper_method :for_collection

    def create
      # This should happen in the change set or persister
      # https://duldev.atlassian.net/browse/DDR-2629
      change_set.admin_policy_id = for_collection.admin_policy_id
      super
    end

    private

    def after_load_before_authorize
      current_object.for_collection_id = params.require(:for_collection_id)
      super
    end
  end
end
