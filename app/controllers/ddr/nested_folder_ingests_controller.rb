module Ddr
  class NestedFolderIngestsController < ApplicationController
    load_resource
    before_action :authorize_create, only: [:create]

    def create
      authorize! :create, @nested_folder_ingest
      @nested_folder_ingest.user = current_user
      if @nested_folder_ingest.valid?
        job_args = { 'admin_set' => @nested_folder_ingest.admin_set,
                     'basepath' => @nested_folder_ingest.basepath,
                     'batch_user' => @nested_folder_ingest.user.user_key,
                     'collection_id' => @nested_folder_ingest.collection_id,
                     'collection_title' => @nested_folder_ingest.collection_title,
                     'config_file' => @nested_folder_ingest.config_file,
                     'subpath' => @nested_folder_ingest.subpath }
        unless @nested_folder_ingest.checksum_file.empty?
          job_args.merge!({ 'checksum_file' => @nested_folder_ingest.checksum_file })
        end
        if create_params.has_key?(:checksums)
          path = ::File.join(Ddr::Admin.data_dir, 'ddr_checksums')
          Dir.mkdir path unless ::File.exist? path
          checksum_file = ::File.open(::File.join(path, "#{SecureRandom.uuid}.txt"), 'wb') do |f|
            f.write(create_params[:checksums].read)
            f.path
          end
          job_args.merge!({ 'checksum_file' => checksum_file })
        end
        unless @nested_folder_ingest.metadata_file.empty?
          job_args.merge!({ 'metadata_file' => @nested_folder_ingest.metadata_file })
        end
        if create_params.has_key?(:metadata)
          path = ::File.join(Ddr::Admin.data_dir, 'ddr_metadata')
          Dir.mkdir path unless ::File.exist? path
          metadata_file = ::File.open(::File.join(path, "#{SecureRandom.uuid}.txt"), 'wb') do |f|
            f.write(create_params[:metadata].read)
            f.path
          end
          job_args.merge!({ 'metadata_file' => metadata_file })
        end
        Rails.logger.info(job_args.to_s)
        NestedFolderIngestJob.perform_later(job_args)
        render 'queued'
      else
        render 'new'
      end
    end

    private

    def create_params
      params.require(:ddr_nested_folder_ingest).permit(:admin_set, :basepath, :checksum_file, :checksums, :collection_id,
                                                       :collection_title, :config_file, :metadata, :metadata_file, :subpath)
    end

    def authorize_create
      return unless (collection_id = @nested_folder_ingest.collection_id).present?

      if can?(:add_children, collection_id)
        current_ability.can(:create, NestedFolderIngest, collection_id:)
      else
        current_ability.cannot :create, NestedFolderIngest, collection_id:
      end
    end
  end
end
