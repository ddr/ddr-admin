module Ddr
  class ResourcesController < ::ApplicationController
    extend ActiveModel::Callbacks

    layout :ddr_admin_layout

    load_resource instance_name: :current_object
    before_action :after_load_before_authorize, only: %i[new create]
    authorize_resource instance_name: :current_object

    before_action :xhr_tab

    attr_reader :current_object

    helper_method :change_set
    helper_method :current_object
    helper_method :current_document
    helper_method :admin_metadata_fields
    helper_method :editable_admin_metadata_fields
    helper_method :readonly_admin_metadata_fields
    helper_method :current_tabs

    def show; end

    def edit; end

    def create
      change_set.grant_roles_to_creator(current_user)

      if change_set.resource.has_attribute?(:parent_id)
        change_set.copy_admin_policy_or_roles_from(change_set.resource.parent)
      end

      if change_set.valid?
        saved_resource = save_change_set(change_set)
        flash[:success] = after_create_success_message
        after_create_success(saved_resource)
        redirect_to after_create_redirect(saved_resource)
      else
        render :new
      end
    end

    def update
      change_set.optimistic_lock_token = params[:optimistic_lock_token]
      change_set.set_desc_metadata(desc_metadata_params)
      if change_set.valid?
        save_change_set(change_set, summary: 'Descriptive metadata updated')
        flash[:success] = 'Descriptive metadata updated.'
        redirect_to action: 'show', tab: 'descriptive_metadata'
      else
        render :edit
      end
    rescue Valkyrie::Persistence::StaleObjectError
      flash[:error] = I18n.t('ddr.change_set.stale_object')
      redirect_to action: 'show', tab: 'descriptive_metadata'
    rescue ArgumentError => e
      flash[:error] = "#{e.message}"
      render :edit
    end

    def events
      @events = Ddr::Events::Event.for_object(current_object).reorder('event_date_time DESC')
    end

    def event
      @event = Ddr::Events::Event.find(params[:event_id])
    end

    def admin_metadata
      if request.patch?
        change_set.optimistic_lock_token = params[:optimistic_lock_token]
        change_set.set_admin_metadata(admin_metadata_params)
        if change_set.valid?
          save_change_set(change_set, summary: 'Administrative metadata updated')
          flash[:success] = 'Administrative metadata updated.'
          redirect_to action: 'show', tab: 'admin_metadata'
        else
          render :admin_metadata
        end
      end
    rescue Valkyrie::Persistence::StaleObjectError
      flash[:error] = I18n.t('ddr.change_set.stale_object')
      redirect_to action: 'show', tab: 'admin_metadata'
    end

    def roles
      if request.patch?
        change_set.optimistic_lock_token = params[:optimistic_lock_token]
        change_set.roles = submitted_roles
        if change_set.valid?
          save_change_set(change_set, summary: 'Roles updated')
          flash[:success] = 'Roles successfully updated'
          redirect_to(action: 'show', tab: 'roles')
        end
      end
    rescue Valkyrie::Persistence::StaleObjectError
      flash[:error] = I18n.t('ddr.change_set.stale_object')
      redirect_to action: 'show', tab: 'roles'
    end

    def files
      redirect_to action: 'show', tab: 'files'
    end

    def upload
      @ddr_file = params[:ddr_file] || request.params['content']['file']

      if request.patch?
        begin
          change_set.optimistic_lock_token = params[:optimistic_lock_token]
          upload_file(content_params[:ddr_file_type])
          change_set.add_value(:comment, params[:comment]) if params[:comment]
          change_set.add_value(:summary, 'Object was updated with file')
          persisted_resource = Ddr::ResourceChangeSetPersister.new.save(change_set:)
          validate_checksum persisted_resource[content_params[:ddr_file_type]]
          flash[:success] = I18n.t('ddr.upload.alerts.success')
          redirect_to(action: 'show') and return
        rescue ActionController::ParameterMissing => e
          flash.now[:error] = (e.param == :file ? 'File is required.' : e.to_s)
        rescue Valkyrie::Persistence::StaleObjectError
          flash[:error] = I18n.t('ddr.change_set.stale_object')
          redirect_to(action: 'show', tab: 'files') and return
        end
      end

      return unless current_object.has_file?(@ddr_file)

      flash.now[:alert] = I18n.t('ddr.upload.alerts.has_content', ddr_file: @ddr_file)
    end

    def current_document
      @document ||= SolrDocument.find(params[:id])
    end

    def change_set
      @change_set ||= Ddr::ResourceChangeSet.change_set_for(current_object)
    end

    protected

    def after_load_before_authorize
      # no-op -- override to add behavior after load and before authorization of resource
    end

    def after_create_success(saved_resource)
      # no-op -- override to add behavior after save and before redirect
    end

    def after_create_success_message
      "New #{change_set.internal_resource} created."
    end

    def after_create_redirect(resource)
      { action: :edit, id: resource }
    end

    def admin_metadata_fields
      flds = Ddr::HasAdminMetadata.term_names - [:access_role] + [:original_filename]
      flds.select { |t| current_object.respond_to?(t) }
    end

    def editable_admin_metadata_fields
      Ddr::Admin.user_editable_admin_metadata_fields
    end

    def readonly_admin_metadata_fields
      admin_metadata_fields - editable_admin_metadata_fields
    end

    def comment_param
      params.permit(:comment).to_h
    end

    def ddr_admin_layout
      case params[:action].to_sym
      when :new, :create then 'new'
      else 'objects'
      end
    end

    def admin_metadata_params
      @admin_metadata_params ||= params.require(:adminMetadata).permit(permitted_admin_metadata_params).to_h
    end

    def desc_metadata_params
      @desc_metadata_params ||= params.require(:descMetadata).permit(permitted_desc_metadata_params).to_h
    end

    def permitted_admin_metadata_params
      change_set.admin_metadata_field_names.map do |field_name|
        change_set.multiple?(field_name) ? { field_name => [] } : field_name
      end
    end

    def permitted_desc_metadata_params
      change_set.desc_metadata_field_names.index_with { |_term| [] }
    end

    def save_change_set(change_set, options = {})
      save_options(options).each do |k, v|
        change_set.set_values(k, v)
      end
      Ddr::ResourceChangeSetPersister.new.save(change_set:)
    end

    def save_options(extra = {})
      default_save_options.merge(extra)
    end

    def default_save_options
      { user: current_user }.merge(comment_param)
    end

    def submitted_roles
      JSON.parse(roles_param).map { |r| Ddr::Auth::Roles::Role.new(r.symbolize_keys) }
    end

    def roles_param
      params.require :roles
    end

    def show_ddr_file_download_link?(ddr_file_type)
      current_object.send(ddr_file_type)&.file_identifier.present? &&
        can?(:"download_#{ddr_file_type}", current_object)
    end

    def current_tabs
      @current_tabs ||= Ddr::V3::Tabs.new(resource: current_object, ability: current_ability, active: params[:tab])
    end

    def xhr_tab
      return unless request.xhr? && current_tabs.tab?(params[:action])

      @tab = current_tabs[params[:action]]
      render(:tab, layout: false) and return
    end
  end
end
