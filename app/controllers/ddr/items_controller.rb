module Ddr
  class ItemsController < ResourcesController
    include Controller::HasChildrenBehavior
    include Controller::HasParentBehavior
    include Controller::HasStructuralMetadataBehavior
    include Controller::HasIiifBehavior
    include V3::PublishableResourceController

    define_model_callbacks :components

    around_components :configure_blacklight_for_children

    def components
      run_callbacks :components do
        sort = params[:sort]
        objects ||= get_children(current_object, sort)
        per_page = params[:per_page] || 10
        @paginatable_objects = Kaminari.paginate_array(objects.to_a).page(params[:page]).per(per_page)
        render 'components' # called explicitly here to ensure occurs within callbacks block
      end
    end

    def create
      if change_set.resource.has_attribute?(:parent_id)
        change_set.copy_admin_policy_or_roles_from(change_set.resource.parent)
      end
      change_set.set_desc_metadata(desc_metadata_params)
      super
    end

    private

    def after_create_success(created_resource)
      return unless params[:content].present?

      child_params = params.require(:content).permit(:file, :checksum, :checksum_type)
      if child_params[:file].present?
        # instantiate the component
        child = Ddr::Component.new(parent_id: created_resource.id)
        # instantiate the component change set
        child_cs = Ddr::ComponentChangeSet.new(child)
        # set permissions on the component
        child_cs.grant_roles_to_creator(current_user)
        child_cs.copy_admin_policy_or_roles_from(created_resource)
        # upload the file
        child_cs.add_file(child_params[:file], 'content')
        if child_cs.valid?
          saved_child = save_change_set(child_cs)
          if child_params[:checksum].present?
            begin
              checksum, checksum_type = child_params.values_at :checksum, :checksum_type
              flash[:info] = saved_child.content.validate_checksum!(checksum, checksum_type)
            rescue Ddr::ChecksumInvalid => e
              flash[:error] = e.message
            end
          end
        else # component is invalid
          error_messages = child_cs.errors.full_messages.join('<br />')
          flash[:error] = "The component was not created: #{error_messages}".html_safe
        end
      elsif child_params[:checksum].present?
        flash[:error] = 'The component was not created: File missing.'
      end
      # Checksum provided for component without file
    end

    def after_load_before_authorize
      current_object.parent_id = params.require(:parent_id)
      super
    end

    def editable_admin_metadata_fields
      super + Ddr::Admin.user_editable_item_admin_metadata_fields
    end
  end
end
