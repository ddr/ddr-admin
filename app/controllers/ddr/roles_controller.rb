module Ddr
  class RolesController < ::ApplicationController
    class MaxResourcesExceeded < ::StandardError; end

    BATCH_ROLES_MAX_RESOURCES = ENV.fetch('BATCH_ROLES_MAX_RESOURCES', '1000').to_i
    BATCH_ROLES_MAX_SYNC_JOBS = ENV.fetch('BATCH_ROLES_MAX_SYNC_JOBS', '10').to_i

    def batch
      if request.method == 'GET'
        render :batch
      else
        set_batch_params
        @unauthorized = @ids.select { |id| cannot?(:grant, id) }
        if @unauthorized.present?
          flash.now[:error] =
            format('You are not authorized to modify roles on the following resources: %s', @unauthorized)
          render :batch, status: :unauthorized
        elsif @ids.length > BATCH_ROLES_MAX_SYNC_JOBS
          @ids.each { |id| RolesJob.perform_later(id, @roles) }
          flash.now[:success] = 'Background jobs have been created to apply ' \
                                'roles changes to the submitted resource IDs.'
          render :batch, status: :accepted
        else
          @ids.each { |id| RolesJob.perform_now(id, @roles) }
          flash.now[:success] = 'Roles successfully applied.'
          render :batch
        end
      end
    end

    protected

    def set_batch_params
      @ids = params.require(:ids).split
      if @ids.length > BATCH_ROLES_MAX_RESOURCES
        raise MaxResourcesExceeded,
              format('Unable to accept more than %s IDs in a single request.', BATCH_ROLES_MAX_RESOURCES)
      end
      @roles = params.require(:roles)
    end
  end
end
