module Ddr
  module Controller
    module HasParentBehavior
      extend ActiveSupport::Concern

      included do
        delegate :parent, to: :current_object
        helper_method :parent
      end

      protected

      def after_load_before_authorize
        current_object.parent_id = params.require(:parent_id)
        super
      end
    end
  end
end
