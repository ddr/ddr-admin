module Ddr
  module Controller
    module HasChildrenBehavior
      extend ActiveSupport::Concern

      included do
        helper_method :get_children_query
      end

      protected

      # Used in ActiveModel 'around' callback
      def configure_blacklight_for_children
        save_sort_fields = blacklight_config.sort_fields.dup

        blacklight_config.configure do |config|
          config.sort_fields.clear
          config.add_sort_field "#{Ddr::Index::Fields::LOCAL_ID} asc", label: 'Local ID'
          config.add_sort_field "#{Ddr::Index::Fields::TITLE} asc", label: 'Title', default: true
        end

        yield

        blacklight_config.configure do |config|
          config.sort_fields.clear
          save_sort_fields.each do |_k, v|
            config.add_sort_field v.sort, label: v.label, default: v.default_for_user_query
          end
        end
      end

      # @return [Array<::SolrDocument>]
      def get_children(parent_object, sort_order)
        get_children_query(parent_object, sort_order).docs
      end

      # @return [Ddr::API::IndexQuery]
      def get_children_query(parent_object, sort_order = nil)
        fields = Ddr::Admin.index_list_entry_fields.map { |f| Ddr::Index::Fields.get(f) }
        fields << Ddr::Index::Fields::TITLE
        fields.uniq!

        sort = sort_order || blacklight_config.default_sort_field.sort

        Ddr::API::IndexRepository.query(
          q: "parent_id_tsim:id-#{parent_object.id}",
          fl: fields,
          sort:,
          rows: 1_000_000
        ).with_effective_role_filter(current_ability)
      end
    end
  end
end
