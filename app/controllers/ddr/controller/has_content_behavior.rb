module Ddr
  module Controller
    module HasContentBehavior
      extend ActiveSupport::Concern

      included do
        before_action :upload_content, only: :create
        helper_method :tech_metadata_fields
      end

      protected

      def tech_metadata_fields
        Ddr::Admin.techmd_show_fields.index_with do |field|
          Array(current_object.techmd.send(field))
        end
      end

      def upload_content
        upload_file('content')
      end

      def upload_file(ddr_file_type)
        change_set.add_file content_params[:file], ddr_file_type
      end

      def after_create_success(created_resource)
        return unless created_resource.has_content?

        validate_checksum(created_resource.content)
      end

      def validate_checksum(ddr_file)
        return if content_params[:checksum].blank?

        flash[:info] = ddr_file.validate_checksum!(*checksum_params)
      rescue Ddr::ChecksumInvalid => e
        flash[:error] = e.message
      end

      def content_params
        @content_params ||= params.require(:content).tap do |p|
          p.require(:ddr_file_type)
          p.require(:file)
          p.permit(:checksum, :checksum_type)
        end
      end

      def checksum_params
        content_params.values_at :checksum, :checksum_type
      end
    end
  end
end
