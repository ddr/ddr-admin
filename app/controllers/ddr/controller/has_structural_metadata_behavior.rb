module Ddr
  module Controller
    module HasStructuralMetadataBehavior
      extend ActiveSupport::Concern

      def generate_structure
        Ddr::GenerateDefaultStructureJob.perform_later(current_object.id.id)
        flash[:success] = 'Default Structure Generation Job queued.'
        redirect_to(action: 'show', tab: 'structural_metadata')
      end

      def structural_metadata
        render layout: false
      end
    end
  end
end
