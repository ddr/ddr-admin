module Ddr
  module Controller
    module HasIiifBehavior
      extend ActiveSupport::Concern

      def iiif
        render layout: false
      end

      def generate_iiif
        created_updated = current_object.has_iiif_file? ? 'updated' : 'created'

        Ddr::V3::IiifFileUpdateService.call(resource: current_object)

        flash[:success] = "IIIF manifest #{created_updated}."

        redirect_to(action: 'show', tab: 'iiif')
      end
    end
  end
end
