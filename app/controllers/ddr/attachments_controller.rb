module Ddr
  class AttachmentsController < ResourcesController
    include Controller::HasContentBehavior

    delegate :attached_to, to: :current_object

    helper_method :attached_to

    def create
      if change_set.resource.has_attribute?(:attached_to_id)
        change_set.copy_admin_policy_or_roles_from(change_set.resource.attached_to)
      end
      change_set.set_desc_metadata(desc_metadata_params)
      super
    end

    private

    def after_load_before_authorize
      current_object.attached_to_id = params.require(:attached_to_id)
      super
    end
  end
end
