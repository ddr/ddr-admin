module Ddr
  class ComponentsController < ResourcesController
    include Controller::HasContentBehavior
    include Controller::HasParentBehavior
    include V3::PublishableResourceController

    def create
      if change_set.resource.has_attribute?(:parent_id)
        change_set.copy_admin_policy_or_roles_from(change_set.resource.parent)
      end
      super
    end

    def stream
      if current_object.streamable?

        send_file current_object.streamable_media_path,
                  type: current_object.streamable_media_type,
                  stream: true,
                  filename: [current_object.id.id, current_object.streamable_media_extension].join('.'),
                  disposition: 'inline'
      else
        render nothing: true, status: :not_found
      end
    end

    def captions
      if current_object.captioned?

        send_file current_object.caption_path,
                  type: current_object.caption_type,
                  filename: [current_object.id.id, current_object.caption_extension].join('.')
      else
        render nothing: true, status: :not_found
      end
    end

    private

    def editable_admin_metadata_fields
      super + Ddr::Admin.user_editable_component_admin_metadata_fields
    end
  end
end
