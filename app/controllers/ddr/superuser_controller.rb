module Ddr
  class SuperuserController < ::ApplicationController
    before_action :authorize_to_act_as_superuser!, only: :create
    after_action :cleanup_session

    def create
      sign_in(:superuser, current_user)
      flash[:alert] = 'Caution! You are now acting as Superuser.'
      redirect_back(fallback_location: root_path)
    end

    def destroy
      sign_out(:superuser)
      flash[:success] = 'You are no longer acting as Superuser.'
      redirect_to root_path
    end

    protected

    def authorize_to_act_as_superuser!
      return if authorized_to_act_as_superuser?

      raise CanCan::AccessDenied
    end

    def cleanup_session
      session.delete(:create_menu_models)
    end
  end
end
