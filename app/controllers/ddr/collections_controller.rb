require 'csv'

module Ddr
  class CollectionsController < ResourcesController
    include Controller::HasChildrenBehavior
    include Controller::HasStructuralMetadataBehavior
    include Controller::HasIiifBehavior
    include V3::PublishableResourceController

    define_model_callbacks :attachments, :items, :targets

    around_attachments :configure_blacklight_for_attachments
    # defined in Ddr::Controller::HasChildrenBehavior
    around_items :configure_blacklight_for_children
    # defined in Ddr::Controller::HasTargetsBehavior
    around_targets :configure_blacklight_for_targets

    respond_to :csv
    skip_authorize_resource only: :aspace

    helper_method :collection_report

    def attachments
      run_callbacks :attachments do
        objects = get_attachments.to_a
        per_page = params[:per_page] || 10
        @paginatable_objects = Kaminari.paginate_array(objects).page(params[:page]).per(per_page)
        render 'attachments' # called explicitly here to ensure occurs within callbacks block
      end
    end

    # HTML format intended for tab content loaded via ajax
    def collection_info
      respond_to do |format|
        format.html { render layout: false }
        format.csv do
          filename = "#{current_object.title_display.gsub(/[^\w]/, '_')}.csv"
          send_data collection_csv_report, type: 'text/csv', filename:
        end
      end
    end

    def create
      change_set.admin_set = params.require(:admin_set)
      change_set.set_desc_metadata(desc_metadata_params)
      super
    end

    def export
      respond_to do |format|
        format.html do
          render :export
        end

        format.csv do
          case params[:type]
          when 'descmd'
            export_metadata
          when 'aspace'
            export_aspace
          else
            head :not_found
          end
        end
      end
    end

    def aspace
      @aspace_authorized = ArchivesSpace::CreateDigitalObjects.authorized?(current_user.aspace_username)
      unless @aspace_authorized
        flash.now[:error] =
          'You are not authorized to execute this operation in ArchivesSpace. Contact the ArchivesSpace administrator.'
      end
      @submitted = request.post?
      if @submitted
        if @aspace_authorized
          options = {
            publish: !!params[:publish],
            filename: params.require(:filename) + '.csv',
            notify: params.require(:notify),
            user: current_user.aspace_username,
            debug: !!params[:debug]
          }
          ArchivesSpace::CreateDigitalObjectsJob.perform_later(current_object.id.id, options)
        end
      else
        # Replacing safe_id (Fedora-related):
        # @filename = "aspace_dos_created_from_#{current_object.safe_id}"
        @filename = "aspace_dos_created_from_#{current_object.id.id}"
      end
    end

    def items
      run_callbacks :items do
        sort = params[:sort]
        objects ||= get_children(current_object, sort).to_a
        per_page = params[:per_page] || 10
        @paginatable_objects = Kaminari.paginate_array(objects).page(params[:page]).per(per_page)
        render 'items' # called explicitly here to ensure occurs within callbacks block
      end
    end

    def targets
      run_callbacks :targets do
        objects = get_targets.to_a
        per_page = params[:per_page] || 10
        @paginatable_objects = Kaminari.paginate_array(objects).page(params[:page]).per(per_page)
        render 'targets' # called explicitly here to ensure occurs within callbacks block
      end
    end

    protected

    def configure_blacklight_for_attachments
      save_sort_fields = blacklight_config.sort_fields.dup

      blacklight_config.configure do |config|
        config.sort_fields.clear
        config.add_sort_field "#{Ddr::Index::Fields::TITLE} asc", label: 'Title', default: true
      end

      yield

      blacklight_config.configure do |config|
        config.sort_fields.clear
        save_sort_fields.each do |_k, v|
          config.add_sort_field v.sort, label: v.label, default: v.default_for_user_query
        end
      end
    end

    def get_attachments
      Ddr::API::IndexRepository.query(
        q: "attached_to_id_tsim:id-#{current_object.id}",
        sort: params[:sort] || blacklight_config.default_sort_field.sort
      ).with_effective_role_filter(current_ability).docs
    end

    def configure_blacklight_for_targets
      save_sort_fields = blacklight_config.sort_fields.dup

      blacklight_config.configure do |config|
        config.sort_fields.clear
        config.add_sort_field "#{Ddr::Index::Fields::LOCAL_ID} asc", label: 'Local ID'
        config.add_sort_field "#{Ddr::Index::Fields::TITLE} asc", label: 'Title', default: true
      end

      yield

      blacklight_config.configure do |config|
        config.sort_fields.clear
        save_sort_fields.each do |_k, v|
          config.add_sort_field v.sort, label: v.label, default: v.default_for_user_query
        end
      end
    end

    def get_targets
      Ddr::API::IndexRepository.query(
        q: "for_collection_id_tsim:id-#{current_object.id}",
        sort: params[:sort] || blacklight_config.default_sort_field.sort
      ).with_effective_role_filter(current_ability).docs
    end

    private

    def collection_report
      return @collection_report if @collection_report

      components = current_object.components
      total_file_size = components.map(&:content_size).reduce(0, :+)
      @collection_report = {
        components: components.size,
        items: current_object.children.size,
        total_file_size:
      }
    end

    def collection_csv_report
      CSV.generate do |csv|
        csv << Ddr::Admin.collection_report_fields.collect { |f| f.to_s.upcase }
        current_object.components.each do |obj|
          csv << Ddr::Admin.collection_report_fields.collect { |f| obj.send(f) }
        end
      end
    end

    def editable_admin_metadata_fields
      super + %i[admin_set research_help_contact]
    end

    def export_aspace
      url = url_for(current_tabs['actions']['export_aspace'].url_options)

      redirect_to url
    end

    def export_metadata
      query = { csv_fields: 'parent_id' }
      fields = []

      query[:remove_empty_columns] = 'true' if params[:remove_empty_columns]
      fields += params[:dmd_fields] if params[:dmd_fields].present?
      fields += params[:amd_fields] if params[:amd_fields].present?
      query[:fields] = fields.join(',') if fields.present?
      query[:model] = params[:models].join(',') if params[:models].present?
      query[:csv_fields] += ',original_filename' if params[:include_original_filename]
      query[:admin_set] = AdminSet.find_by_code(current_object.admin_set) if params[:scope] == 'admin_set'

      query_string = URI.encode_www_form(query)

      case params.require(:scope)
      when 'collection'
        redirect_to(format('/api/resources/%s/members.csv?%s', current_object.id, query_string))
      when 'admin_set'
        redirect_to('/api/resources.csv?%s' % query_string)
      else
        head :bad_request
      end
    end

    def tab_actions
      Tab.new('actions')
    end

    def tab_collection_info
      Tab.new('collection_info',
              href: url_for(action: 'collection_info'))
    end
  end
end
