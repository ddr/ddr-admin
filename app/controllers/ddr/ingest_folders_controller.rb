module Ddr
  class IngestFoldersController < ApplicationController
    before_action :new_ingest_folder, only: %i[new create]

    load_and_authorize_resource

    def new_ingest_folder
      @ingest_folder = IngestFolder.new(ingest_folder_params)
    end

    def show
      @scan_results = @ingest_folder.scan
    end

    def create
      @ingest_folder.user = current_user
      @ingest_folder.checksum_type ||= IngestFolder.default_checksum_type
      @ingest_folder.checksum_file ||= @ingest_folder.default_checksum_file

      if @ingest_folder.save
        redirect_to @ingest_folder
      else
        render :new
      end
    end

    def procezz
      IngestFolderJob.perform_later('id' => @ingest_folder.id)
      render 'queued'
    end

    private

    def ingest_folder_params
      params.require(:ddr_ingest_folder).permit(:collection_id, :model, :base_path, :sub_path, :checksum_file,
                                                :checksum_type, :parent_id_length)
    end
  end
end
