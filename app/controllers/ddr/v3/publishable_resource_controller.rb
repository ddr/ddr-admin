module Ddr::V3
  module PublishableResourceController
    PublicationService.workflow_event_names.each do |event|
      define_method event do
        unless params[:confirmed]
          @action_title = params[:action].titleize
          render :publication and return
        end

        PublicationService.call(event:, resource: current_object, async: true, on_behalf_of: current_user.email)
        flash[:success] = 'Background publication job enqueued.'
        redirect_to action: :show
      end
    end
  end
end
