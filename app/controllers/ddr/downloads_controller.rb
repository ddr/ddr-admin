module Ddr
  class DownloadsController < ::ApplicationController
    # This module is included in the dul-hydra DownloadsController but is not available in the ddr-admin dependencies.
    # We'll have to see if what things from there are needed and, if so, how to account for them in the present
    # context.
    # include Hydra::Controller::DownloadBehavior

    include Ddr::Auth::RoleBasedAccessControlsEnforcement

    before_action :load_asset
    before_action :load_ddr_file
    before_action :authorize_download!

    attr_reader :asset, :ddr_file

    # DDR-C renders 404 if the requested datastream responds `true` to `#new?` (inherited from
    # `Hydra::Controller::DownloadBehavior#show`).  In DDRevo, `Ddr::File` objects always return `true` to `new_record`,
    # even if they have been persisted as part of a resource.  I think this is because those objects are persisted as
    # nested resources.  `ddr_file.file_identifer.nil?` seems perhaps the closest analogue to `datastream.new?`.  In
    # any case, without a `file_identifier`, we cannot download a `Ddr::File` file.
    def show
      if ddr_file.file_identifier.nil?
        render_404
      else
        send_content
      end
    end

    protected

    def send_content
      if request.head?
        head :ok,
             content_length: ddr_file.file_size,
             content_type: ddr_file.media_type
      else
        disposition = params[:inline] ? 'inline' : 'attachment'

        send_file ddr_file.file.disk_path,
                  type: ddr_file.media_type,
                  filename: ddr_file_name,
                  disposition:
      end
    end

    def ddr_file_type
      params.has_key?(:ddr_file_type) ? params[:ddr_file_type] : 'content'
    end

    def load_asset
      @asset = Ddr.query_service.find_by(id: params[:id])
    end

    def load_ddr_file
      @ddr_file = ddr_file_to_show
    end

    def authorize_download!
      authorize! :"download_#{ddr_file_type}", asset
    end

    def ddr_file_to_show
      ddrf = asset.send(ddr_file_type.to_sym)
      raise "Unable to find a file for #{asset}" if ddrf.nil?

      ddrf
    end

    def ddr_file_name
      return ddr_file.original_filename if ddr_file.original_filename.present?
      # In DDR-C, the first 'identifier' is used if present.  However, I think that's probably a holdover from
      # the time before there was a 'local_id' field and the DPC identifier, if any, was the first entry in the
      # 'identifier' field.
      # local_id may be file name minus extension
      return "#{asset.local_id}.#{ddr_file.default_file_extension}" if asset.local_id.present?

      # Mimics DDR-C Ddr::Datastreams::DatastreamBehavior#default_file_name
      "#{asset.id.id}_#{ddr_file_type}.#{ddr_file.default_file_extension}"
    end
  end
end
