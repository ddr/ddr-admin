module Ddr
  class BatchesController < ::ApplicationController
    load_and_authorize_resource class: Ddr::Batch::Batch

    def index
      @batches = @batches.includes(:batch_objects,
                                   :user).order('id DESC').page(params[:page]).per(Ddr::Admin.batches_per_page)
      if params[:filter] == 'current_user'
        @batches = @batches.where(user: current_user)
        render 'my_batches'
      else
        render 'index'
      end
    end

    def show
      @batch_objects = @batch.batch_objects.page params[:page]
    end

    def destroy
      if @batch.deletable?
        Ddr::Batch::BatchDeletionJob.perform_later(@batch.id)
        flash[:notice] = I18n.t('ddr.batch.web.batch_deleting', id: @batch.id)
      else
        flash[:notice] = I18n.t('ddr.batch.web.batch_not_deletable', id: @batch.id, status: @batch.status)
      end
      redirect_to action: :index
    end

    def retry
      if @batch.outcome == 'FAILURE'
        unverified_created_components = Ddr.query_service.run_query(unverified_created_components_query, @batch.id)

        if unverified_created_components.size > 0
          msg = "One or more unverified Components in batch #{@batch.id} have been found: %s"
          flash[:danger] = msg % unverified_created_components.map { |c| c.id.to_s }.to_a.join(', ')

          redirect_to ddr_batch_url(@batch) and return
        else
          Ddr::Batch::BatchReprocessorJob.perform_later(@batch.id, current_user.id)
          flash[:notice] = I18n.t('ddr.batch.web.batch_requeueing', id: @batch.id)
        end
      end

      redirect_to action: :index
    end

    def procezz
      Ddr::Batch::BatchProcessorJob.perform_later(@batch.id, current_user.id)
      @batch.status = Ddr::Batch::Batch::STATUS_QUEUED
      @batch.save
      flash[:notice] = I18n.t('ddr.batch.web.batch_queued', id: @batch.id)
      redirect_to ddr_batch_url
    end

    def validate
      referrer = request.env['HTTP_REFERER']
      @errors = @batch.validate
      valid = @errors.empty?
      if valid
        @batch.status = Ddr::Batch::Batch::STATUS_VALIDATED
        @batch.save
      end
      flash[:notice] = "Batch is #{valid ? '' : 'not '}valid"
      if valid && referrer == url_for(action: 'index', only_path: false)
        redirect_to batches_url
      else
        # render :show
        redirect_to batch_url(@batch.id)
      end
    end

    def unverified_created_components_query
      <<-SQL
        SELECT o.* FROM orm_resources o
        INNER JOIN batch_object_relationships bor ON o.metadata->'parent_id'->0->>'id' = bor.object
        INNER JOIN batch_object_datastreams bod ON bod.batch_object_id = bor.batch_object_id
        INNER JOIN batch_objects bo ON bo.id = bod.batch_object_id
        WHERE bo.verified = false
          AND bo.model = 'Ddr::Component'
          AND bo.resource_id IS NULL
          AND bo.batch_id = ?
          AND bod.name = 'content'
          AND bod.checksum IS NOT NULL
          AND bor.name = 'parent'
          AND bor.object_type = 'ID'
          AND o.internal_resource = 'Ddr::Component'
          AND o.metadata->'content'->0->'digest'->0->>'value' = bod.checksum
      SQL
    end
  end
end
