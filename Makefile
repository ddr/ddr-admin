SHELL = /bin/bash

repository ?= ddr-admin

build_tag ?= $(repository):latest

bundle_command = docker run --rm -v $(shell pwd):/opt/app-root $(build_tag) bundle

.PHONY: build
build:
	repository=$(repository) build_tag=$(build_tag) ./build.sh

.PHONY: clean
clean:
	rm -rf ./tmp/*
	rm -f ./log/*.log

.PHONY: test
test: clean
	./.docker/test.sh ci

.PHONY: lock
lock:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) /bin/bash -c '\
		git config --global --add safe.directory "*" && bundle lock'

.PHONY: update
update:
	$(bundle_command) update $(args)

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) ./audit.sh

.PHONY: rubocop
rubocop:
	$(bundle_command) exec rubocop $(args)

.PHONY: autocorrect
autocorrect:
	$(bundle_command) exec rubocop -a

.PHONY: rails
rails:
	docker run --rm -v "$(shell pwd):/opt/app-root" -e RAILS_ENV=development $(build_tag) \
		bin/rails $(args)

.PHONY: workflow
workflow:
	docker run --rm -v "$(shell pwd):/opt/app-root" -e RAILS_ENV=development $(build_tag) \
		bin/rails runner 'Ddr::V3::PublicationService.workflow_diagram(path: "./app/assets/images/ddr_admin", name: "workflow")'

.PHONY: doc
doc:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) yardoc
