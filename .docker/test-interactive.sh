#!/bin/bash

echo "test-interactive.sh is deprecated - use 'test.sh shell' instead."

cd "$(dirname ${BASH_SOURCE[0]})"

./test.sh shell
