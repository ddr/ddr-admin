#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

rm -f ../tmp/pids/server.pid

compose_opts="-f docker-compose.yml -f docker-compose.dev.yml -p ddr-admin-dev"

case $1 in
    shell)
	set -- exec app bash
	;;
    console)
	set -- exec app /bin/bash -c 'bin/rails c'
	;;
    up)
	trap "docker compose ${compose_opts} down" SIGINT
	;;
esac

docker compose ${compose_opts} "$@"
