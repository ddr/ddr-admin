#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

compose_opts="-f docker-compose.yml -f docker-compose.test.yml -p ddr-admin-test"

run_app() {
    docker compose ${compose_opts} run \
		   -v "$(git rev-parse --show-toplevel):/opt/app-root" \
		   app "$@"
}

shut_down() {
    docker compose ${compose_opts} down --remove-orphans
}

case ${1:-shell} in
    ci)
	docker compose ${compose_opts} up --exit-code-from app
	code=$?
	shut_down
	exit $code
	;;

    test)
	run_app /bin/bash -c 'bundle exec rake'
	shut_down
	;;

    shell)
	run_app /bin/bash -c 'bin/rails db:prepare; bash'
	shut_down
	;;

    console)
	run_app /bin/bash -c 'bin/rails db:prepare; bin/rails c'
	shut_down
	;;

    *)
	docker compose ${compose_opts} "$@"
	;;
esac
