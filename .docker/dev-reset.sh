#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

echo "Stopping services ..."
./dev.sh down

echo "Removing existing volumes ..."
vols=$(docker volume ls -q -f 'name=ddr-admin-dev')
docker volume rm ${vols[@]}
