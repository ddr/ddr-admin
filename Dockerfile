ARG ruby_version="3.1.4"
ARG builder="builder"
ARG bundle="bundle"

FROM ruby:${ruby_version} AS builder

ARG fits_version="1.5.5"
ARG fits_sha256="48be7ad9f27d9cc0b52c63f1aea1a3814e1b6996ca4e8467e77772c187ac955c"

ENV APP_ROOT="/opt/app-root" \
    APP_USER="app-user" \
    APP_UID="1001" \
    FITS_HOME="/opt/fits" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    RAILS_ENV="production" \
    TZ="US/Eastern"

SHELL ["/bin/bash", "-c"]

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install \
      curl jq libjemalloc2 less locales libpq-dev libvips42 nodejs npm \
      clamdscan ffmpeg file mediainfo openjdk-17-jre-headless unzip zip; \
    rm -rf /var/lib/apt/lists/*; \
    echo "$LANG UTF-8" >> /etc/locale.gen; \
    locale-gen $LANG; \
    npm install -g yarn

WORKDIR $FITS_HOME

# Download and configure FITS
RUN set -eux; \
    curl -fL -o fits.zip \
      https://github.com/harvard-lts/fits/releases/download/${fits_version}/fits-${fits_version}.zip; \
    echo "${fits_sha256}  fits.zip" | sha256sum -c - ; \
    unzip -q fits.zip; \
    rm fits.zip; \
    [[ "$(uname -m)" == "x86_64" ]] && \
    cp /usr/lib/x86_64-linux-gnu/libmediainfo.so.0 /usr/lib/x86_64-linux-gnu/libzen.so.0 ./tools/mediainfo/linux/; \
    # Disable logging to file
    sed -E -i 's/^(log4j\.appender\.FILE\.Threshold=).*$/\1OFF/' ./log4j.properties; \
    sed -E -i 's/^(JAVA_OPTS=).*$/JAVA_OPTS="\${FITS_JAVA_OPTS}"/' ./fits-env.sh; \
    sed -E -i 's/^(.*<enable-checksum>)true(<\/enable-checksum>.*)$/\1false\2/' ./xml/fits.xml; \
    ln -s $FITS_HOME/fits.sh /usr/local/bin/fits.sh; \
    # Smoke test
    /usr/local/bin/fits.sh -v

WORKDIR $APP_ROOT

#--------------------------------+

FROM ${builder} AS bundle

COPY .ruby-version Gemfile Gemfile.lock ./

RUN gem install bundler -v "$(tail -1 Gemfile.lock | awk '{print $1}')" && \
    bundle install && \
    chmod -R g=u $GEM_HOME

#--------------------------------+

FROM ${bundle} AS app

ARG APP_VERSION="0.0.0"
ARG APP_VCS_REF="0"
ARG BUILD_DATE="1970-01-01T00:00:00Z"

LABEL org.opencontainers.artifact.description="Duke Digital Repository administrative application (UI + API)"
LABEL org.opencontainers.image.created="{BUILD_DATE}"
LABEL org.opencontainers.image.license="BSD-3-Clause"
LABEL org.opencontainers.image.revision="${APP_VCS_REF}"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/ddr/ddr-admin"
LABEL org.opencontainers.image.url="https://repository.duke.edu"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.version="${APP_VERSION}"

ENV APP_VCS_REF="${APP_VCS_REF}" \
    APP_VERSION="${APP_VERSION}" \
    APP_LOG="/var/log/app" \
    DATA_DIR="/data" \
    RAILS_PORT="3000"

COPY . .

COPY ./etc/ /etc/

RUN set -eux; \
    useradd -u $APP_UID -g 0 -d $APP_ROOT -s /sbin/nologin $APP_USER; \
    mkdir -p $DATA_DIR $APP_LOG ./public/system; \
    chown -R ${APP_UID}:0 $DATA_DIR $APP_LOG .

USER ${APP_UID}:0

RUN yardoc && SECRET_KEY_BASE=1 ./bin/rails assets:precompile

VOLUME $DATA_DIR $APP_LOG $APP_ROOT/public/system

EXPOSE $RAILS_PORT

CMD ["./bin/rails", "server"]
