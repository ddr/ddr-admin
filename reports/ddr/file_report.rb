#
# CSV report of files by ID (e.g., 'content', 'thumbnail', etc.)
# including file path, SHA1, and other information.
#
# For more information:
#
#   $ bundle exec rails r reports/ddr/file_report.rb --help
#
require 'csv'
require 'json'
require 'optparse'

options = {
  limit: 10_000_000,
  file_fields: Ddr::Resource::FILE_FIELDS,
  include_size: false
}

parser = OptionParser.new do |opts|
  opts.banner = 'Usage: bundle exec rails r reports/ddr/file_report.rb [options]'

  ff_help = <<~EOS
    Limit to specific file field(s) (e.g., 'content')
    Separate multiple file fields with comma ('content,thumbnail')

    Valid file fields:
      #{Ddr::Resource::FILE_FIELDS.join("\n  ")}

    All file fields are included by default.
  EOS

  opts.on('-s', '--include-size', 'Include size (resource intensive)') do |v|
    options[:include_size] = v
  end

  opts.on('-lLIMIT', '--limit=LIMIT', 'Limit on Solr query (per file type)') do |v|
    options[:limit] = v.to_i
  end

  opts.on('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end

  opts.on('-fFILE_FIELD', '--file-field=FILE_FIELD', ff_help) do |v|
    options[:file_fields] = v.split(',')
  end
end

parser.parse!

headers = %w[RESOURCE_ID MODEL FILE_FIELD MEDIA_TYPE SHA1 PATH]
headers << 'SIZE' if options[:include_size]

CSV.instance(headers:, write_headers: true) do |csv|
  options[:file_fields].each do |file_field|
    solr_field = "#{file_id}_tsim"

    query = Ddr::Index::Query.new do
      present solr_field
      fields :id, :common_model_name, solr_field
      rows options[:limit]
    end

    query.docs.each do |doc|
      file_data = JSON.parse(doc[solr_field].first.sub(/^serialized-/, ''))

      path = file_data['file_identifier']['id'].sub(%r{^disk://}, '')
      sha1 = begin
        file_data['digest'].first['value']
      rescue StandardError
        nil
      end
      media_type = file_data['media_type']

      row = [doc.id, doc.common_model_name, file_field, media_type, sha1, path]

      if options[:include_size]
        begin
          row << File.size(path)
        rescue StandardError
          nil
        end
      end

      csv << row
    end
  end
end
