source 'https://rubygems.org'

ruby File.read(File.expand_path('.ruby-version', __dir__))

# Adding sidekiq patch automatically removes locks when jobs are deleted
# See https://github.com/veeqo/activejob-uniqueness#sidekiq-api-support
# N.B. Dead jobs are automatically unlocked.
gem 'activejob-uniqueness'
gem 'aws-sdk-s3', require: false
gem 'bagit', '~> 0.5.0'
gem 'blacklight', '~> 7.32'
gem 'bootstrap', '~> 4.0'
gem 'cancancan', '~> 3.3'
gem 'coffee-rails', '~> 5.0'
gem 'connection_pool'
gem 'csv'
gem 'dalli', '~> 3.0'
gem 'ddr-antivirus', '3.0.0'
gem 'devise', '~> 4.8'
gem 'dry-logic', '>= 1.4.0' # https://github.com/dry-rb/dry-logic/issues/104
gem 'edtf', '~> 3.0.8'      # https://github.com/inukshuk/edtf-ruby/issues/38
gem 'ezid-client', '~> 1.10'
gem 'font-awesome-sass', '< 6'
gem 'grape', '~> 2.1.3'
gem 'grape-entity'
gem 'hashie'
# gem 'iiif-presentation', '~> 1.3'
gem 'iiif-presentation', git: 'https://github.com/hcayless/osullivan.git',
                         ref: '2e8d7b11f32bca733375e1539e777e42e4a7eb8f'
gem 'jquery-rails'
gem 'json_schemer'
gem 'k8s-ruby'
gem 'kt-paperclip'
gem 'mime-types', '~> 3.4'
gem 'nokogiri'
gem 'omniauth', '< 2'
gem 'omniauth-shibboleth', '~> 1.3'
gem 'pagy', '~> 8.0'
gem 'pg'
gem 'puma', '< 7'
gem 'rails', '~> 7.2.0'
gem 'rsolr', '>= 1.0', '< 3'
gem 'rubytree'
gem 'ruby-vips'
gem 'sass-rails', '~> 6.0'
gem 'sidekiq'
gem 'sidekiq-failures'
gem 'terser'
gem 'valkyrie', '~> 3.5.0'
gem 'yard'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara', '~> 3.36'
  gem 'equivalent-xml'
  gem 'factory_bot_rails'
  gem 'rails-controller-testing'
  gem 'rspec-its'
  gem 'rspec-rails', '~> 6.0'
  gem 'rubocop'
  gem 'rubocop-capybara', require: false
  gem 'rubocop-factory_bot', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
  gem 'rubocop-rspec_rails', require: false
end

group :development do
  gem 'i18n-tasks', '~> 1.0.14'
  gem 'listen', '>= 3.0.5', '< 3.8'
  gem 'pry'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
end
