# ddr-admin: Duke Digital Repository (DDR) administrative application

## Overview / History

`ddr-admin` incorporates all of the core functionality of the DDR as a "digital repository":

- A digital object model for representing collections, items, and their component files;
- Batch ingest and update processing;
- File and metadata persistence;
- Image derivative generation;
- Fixity checking;
- Malware scanning;
- File characterization and technical metadata extraction;
- Background job processing;
- [IIIF presentation](https://iiif.io/api/presentation/3.0/);
- Structural metadata generation;
- Permanent identifiers;
- Etc.

In its current form, the application is built around these technologies:

- The Ruby programming language (see `.ruby-version` file for specific release)
- The Rails application framework
- Apache HTTPD web server
- [Valkyrie](https://github.com/samvera/valkyrie) data mapper library
- PostgreSQL (major version 15) for metadata persistence
- Apache Solr (major version 9) for metadata indexing
- Blacklight for search and discovery UI
- Redis for background job queue management (currently using compatible Valkey library)
- Memcached cache store (for Rails)
- [Sidekiq](https://github.com/sidekiq/sidekiq) for background job processing
- [Vips](https://www.libvips.org) for image processing
- [EZID](https://ezid.cdlib.org) for permanent identifier minting
- [FITS](https://harvard-lts.github.io/fits/) for file characterization
- ClamAV for malware scanning

## Development Requirements

- Docker
- [GNU Make](https://www.gnu.org/software/make/) - `brew install make` on Mac OS

## Building the app

    $ make

A re-build is usually only required for development when dependencies --
i.e., the `Gemfile` and `Gemfile.lock` -- have changed.

## Using the development stack (docker compose)

Start the development stack (with the `-d` option to run in the background):

    $ .docker/dev.sh up -d

The Apache web server runs on port 8080, or you can access the Rails server directly
at port 3000.

To run a Bash shell on the development app server:

    $ .docker/dev.sh exec app bash

Stop the background development stack:

    $ .docker/dev.sh down

## Database setup and migrations

The development stack does not automatically setup or migrate the database.

To setup (or re-create) the database, start the development stack as described above
and then run the `db:setup` task in the `app` container:

    $ .docker/dev.sh exec app bin/rails db:setup

Migrations and rollback can be run similarly:

    $ .docker/dev.sh exec app bin/rails db:migrate

    $ .docker/dev.sh exec app bin/rails db:rollback

## Sample Corpus

To load (or reload), the sample corpus in the development environment, run:

    $ .docker/dev.sh exec app bin/rails runner lib/ddr/scripts/sample_corpus.rb

or if you are already in a `bash` shell on the dev `app` container:

    app-user@/opt/app-root# bin/rails runner lib/ddr/scripts/sample_corpus.rb

Reloading the sample corpus deletes the sample corpus resources and related files and
database records and then recreates them.

## Ingesting Resources Locally

There is a folder, `ingest_samples/`, that is mounted within the app container as
`/ingest`. It has two subfolders, `nested/` and `standard/`, into which you can
place packages to be ingested using Nested Folder Ingest or Standard Ingest
respectively.

For documentation on how to set up ingests, see
[Repository Curation Workflows and Policies](https://duldev.atlassian.net/wiki/spaces/DDRKB/pages/34045953/Repository+Curation+Workflows+and+Policies).

## Running tests

To run an interactive shell with the test stack:

    $ .docker/test.sh

This command will start the app container with the local code directory
bind-mounted, reset the database, and start an interactive bash session.

After exiting the bash session, the script will
cleanup the test environment, removing the containers and network.

To run the full test suite *against the latest build* (mainly for CI
pipelines), run:

    $ make clean test

If you have made changes since the last build and want to run the test suite
against your local code without doing another build, then use the interactive
mode below and run the command `bundle exec rake` as usual in the container.

## CI/CD and Deployment

- Deploying to *production server*: latest code changes in main branch are deployed manually during business hours,
  and automatically on a [daily schedule](https://gitlab.oit.duke.edu/ddr/ddr-admin/-/pipeline_schedules).

- Deploying to *dev and staging servers*: latest code changes are deployed automatically,
  if the `DEPLOY_REF_NAME` variable for the CI environment matches the CI pipeline branch.

## Environment

Several environment variables control behaviors of ddr-admin or its dependencies.
Custom values for these variables per host are stored in our Ansible repository.

N.B. This list is not intended to be exhaustive.

`APPLICATION_HOSTNAME`

  The canonical (external) host name for the application, usually a CNAME. Example: `ddr-admin.lib.duke.edu`.

`ASPACE_BACKEND_URL`

  ArchivesSpace API URL.

`ASPACE_USER`
`ASPACE_PASSWORD`

  ArchivesSpace API credentials.

`AUTO_ASSIGN_PERMANENT_ID`
`AUTO_UPDATE_PERMANENT_ID`

  Used in test/development to disable automatic permanent ID assignment and updating by setting variables to 'false'.

`BATCH_LOG_LOCATION`

`DATABASE_URL`

  Rails database URL (sensitive: contains credentials).

`DDR_PUBLIC_K8S_NAMESPACE`

  OKD/K8s namespace for the ddr-public application for this instance.
  Used to trigger remote actions such as cache expiration.

`DDR_STORAGE_VOLUME_IDS`

`DERIVATIVES`

  **Deprecated** Names of derivative files to generate.

`EXPORT_FILES_BASE_URL`

`EXPORT_FILES_MAX_PAYLOAD_SIZE`

`EXPORT_FILES_STORE`

`EXPORT_FILES_VOLUME`

`EZID_DEFAULT_SHOULDER`

  Default shoulder for minting ARKs via EZID API.

`EZID_USER`
`EZID_PASSWORD`

  EZID API credentials.

`FITS_JAVA_OPTS`

  Java options passed to FITS.

`HIGH_PRIORITY_JOBS`

  **Deprecated** Comma-separated list of job class names (de-modulized) to queue as `high_priority`.

`IIIF_SERVER_URL`

  Base URL for IIIF presentation manifest ids.

`INGEST_BASE_PATHS`

`INGEST_FOLDER_CHECKSUM_FILE_DIR`

`LEGACY_MRI_STORAGE_BASE_PATH`

`LEGACY_STORAGE_BASE_PATH`

`LOW_PRIORITY_JOBS`

  **Deprecated** Comma-separated list of job class names (de-modulized) to queue as `low_priority`.

`MEMCACHE_SERVERS`

  List of Memcached hosts used as Rails cache stores. See Rails documentation for more information.

`MULTIRES_IMAGE_STORAGE_BASE_PATH`

`OAUTH_CLIENT_ID`
`OAUTH_CLIENT_SECRET`

  OAuth client credentials.

`POLICY_GROUP_SERVICE_USER`
`POLICY_GROUP_SERVICE_PASSWORD`

  Campus group API credentials.

`RAILS_LOG_LEVEL`

  Sets Rails log level.

`RAILS_MAILER_FROM_ADDRESS`
`RAILS_MAILER_REPLY_TO_ADDRESS`
`RAILS_MAILER_SMTP_ADDRESS`

  Rails mailer config. See Rails config documentation for more information.

`RAILS_MAX_THREADS`

  Used in production to set thread count of [Puma](https://puma.io) server. See `config/puma/production.rb`.
  See Rails and Puma documentation for more information.

`REDIS_URL`

  URL for Redis (Valkey) instance used for background job processing.

`SECRET_KEY_BASE`

  Rails secret key required in production. See Rails documentation for more information.

`SOLR_ADMIN_URL`

  The URL to the Solr admin UI for privileged staff.

`SOLR_URL`

  The base URL of the Solr core for the repository index.

`STORAGE_BASE_PATH`

`SUPERUSER_GROUP`

  Name of campus group whose members can act as 'superuser'.

`WEB_CONCURRENCY`

  Number of processes started by the Rails server.  See Rails and Puma documentation for more information.
