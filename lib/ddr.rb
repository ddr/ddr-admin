module Ddr
  # Base class for custom exceptions
  class Error < ::StandardError; end

  # Invalid checksum
  class ChecksumInvalid < ::Ddr::Error; end

  # A model instance was not found
  class NotFoundError < ::Ddr::Error; end

  SOLR_DOCUMENT_ID = 'id'

  def self.find_resource(resource)
    query_service.find_by(id: Ddr::Types::ResourceID[resource])
  end

  # Default metadata adapter configured for Valkyrie
  def self.metadata_adapter
    @metadata_adapter ||= Valkyrie.config.metadata_adapter
  end

  # Persister for default metadata_adapter
  def self.persister
    @persister ||= metadata_adapter.persister
  end

  # Query Service for default metadata_adapter
  def self.query_service
    @query_service ||= metadata_adapter.query_service
  end

  # Default storage adapter
  def self.storage_adapter
    @storage_adapter ||= Ddr::Storage.default_adapter
  end

  # Convenient to be able to set the default storage adapter in certain cases
  def self.storage_adapter=(storage_adapter)
    @storage_adapter = storage_adapter
  end

  # This static vocab list is to replace the ones generated from RDF.
  # @deprecated
  def self.vocab
    @vocab ||= JSON.load_file(::File.expand_path('../config/vocab.json', __dir__), symbolize_names: true)
                   .transform_values { |v| v.map(&:to_sym).freeze }.freeze
  end

  mattr_accessor :default_mime_type do
    'application/octet-stream'
  end

  # Maps media types to preferred file extensions
  mattr_accessor :preferred_file_extensions do
    {
      'application/zip' => 'zip',
      'audio/mp4' => 'm4a',
      'audio/mpeg' => 'mp3',
      'audio/ogg' => 'ogg',
      'audio/wav' => 'wav',
      'text/vtt' => 'vtt',
      'video/x-flv' => 'flv',
      'video/mp4' => 'mp4',
      'video/quicktime' => 'mov',
      'video/webm' => 'webm'
    }
  end

  # Maps file extensions to preferred media types
  mattr_accessor :preferred_media_types do
    {
      '.aac' => 'audio/mp4',
      '.f4a' => 'audio/mp4',
      '.flv' => 'video/x-flv',
      '.m2t' => 'video/vnd.dlna.mpeg-tts',
      '.m2ts' => 'video/m2ts',
      '.m4a' => 'audio/mp4',
      '.mov' => 'video/quicktime',
      '.mp3' => 'audio/mpeg',
      '.mp4' => 'video/mp4',
      '.mts' => 'video/vnd.dlna.mpeg-tts',
      '.oga' => 'audio/ogg',
      '.ogg' => 'audio/ogg',
      '.srt' => 'text/plain',
      '.vtt' => 'text/vtt',
      '.wav' => 'audio/wav',
      '.webm' => 'video/webm',
      '.zip' => 'application/zip'
    }
  end

  mattr_accessor :finding_aid_base_url do
    ENV['FINDING_AID_BASE_URL'] || 'https://archives.lib.duke.edu'
  end
end
