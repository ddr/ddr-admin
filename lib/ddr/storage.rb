module Ddr
  module Storage
    class << self
      # The storage configuration
      # @return [Hash]
      def config
        @config ||= load_config
      end

      # Path to the default storage config file
      # @return [String]
      def default_config_file
        Rails.root.join('config/ddr_storage.yml')
      end

      # Path to the config file for the current Rails environment (which may not exist)
      # @return [String]
      def env_config_file
        Rails.root.join('config', 'ddr_storage', "#{Rails.env}.yml")
      end

      # Loads a config file
      # @param config_file [String] path to config file (which may not exist)
      # @return [Hash]
      def load_config_file(config_file)
        return {} unless ::File.file?(config_file)

        YAML.load ERB.new(::File.read(config_file), trim_mode: '-').result
      end

      # Loads the comprehensive storage config
      # @return [Hash]
      def load_config
        default_conf = load_config_file(default_config_file)
        env_conf     = load_config_file(env_config_file)

        default_conf.deep_merge(env_conf)
      end

      # A hash of adapter short names and adapter instances, just like
      # Valkyrie::StorageAdapter.storage_adapters registry.
      # @return [Hash<Symbol, Object>]
      def adapters
        @adapters ||= {}.tap do |memo|
          config.fetch('adapters').each do |adapter_name, adapter_conf|
            adapter_type = adapter_conf.fetch('type').constantize
            options = adapter_conf.fetch('options', {}).symbolize_keys
            memo[adapter_name.to_sym] = adapter_type.new(**options)
          end
        end.freeze
      end

      # Registers DDR storage adapters with Valkyrie::StorageAdapter for compatibility.
      # @return [Boolean] the adapters are registered
      def register_adapters!
        return if @registered

        adapters.each do |key, adapter|
          Valkyrie::StorageAdapter.register(adapter, key)
        end

        @registered = true
      end

      # Retrieves the configured storage adapter for the file field,
      # or the default adapter if none is configured.
      # @param file_field [Symbol, String] the file field name
      # @return [Object] the storage adapter
      def adapter_for(file_field)
        if key = config.dig('files', file_field.to_s)
          return adapters.fetch(key.to_sym)
        end

        default_adapter
      end

      # Retrieves the default storage adapter.
      # @return [Object] the adapter
      def default_adapter
        key = config.fetch('default').to_sym

        adapters.fetch(key)
      end

      # @param file [IO] expected to be open for reading
      # @param digest [String] exepcted hexdigest
      # @param algorithm [String, Symbol] Digest class
      # @raise [Ddr::ChecksumInvalid]
      # @return [IO]
      def verify_checksum(file:, digest:, algorithm: 'SHA1')
        file.rewind

        calc = Digest(algorithm).new

        while chunk = file.read(64.megabytes)
          calc << chunk
        end
        actual = calc.hexdigest

        if digest == actual
          Rails.logger.debug { "Checksum verification success: #{algorithm}[#{digest}]" }
        else
          raise Ddr::ChecksumInvalid,
                "Checksum verification failure: expected #{algorithm}[#{digest}]; actual #{algorithm}[#{actual}]"
        end

        file.rewind
      end
    end
  end
end
