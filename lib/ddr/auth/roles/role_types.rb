module Ddr
  module Auth
    module Roles
      module RoleTypes
        CURATOR = RoleType.new(
          'Curator',
          'The Curator role conveys responsibility for curating a resource ' \
          'and delegating responsibilities to other agents.',
          Permissions::ALL
        )

        EDITOR = RoleType.new(
          'Editor',
          'The Editor role conveys reponsibility for managing the content, ' \
          'description and structural arrangement of a resource.',
          [Permissions::DISCOVER, Permissions::READ, Permissions::DOWNLOAD,
           Permissions::ADD_CHILDREN, Permissions::UPDATE, Permissions::REPLACE]
        )

        METADATA_EDITOR = RoleType.new(
          'MetadataEditor',
          'The Metadata Editor role conveys responsibility for ' \
          'managing the description of a resource.',
          [Permissions::DISCOVER, Permissions::READ, Permissions::DOWNLOAD,
           Permissions::UPDATE]
        )

        DOWNLOADER = RoleType.new(
          'Downloader',
          'The Downloader role conveys access to the original file associated with a resource.',
          [Permissions::DISCOVER, Permissions::READ, Permissions::DOWNLOAD]
        )

        VIEWER = RoleType.new(
          'Viewer',
          'The Viewer role conveys access to the description and "service" ' \
          'files (e.g., derivative bitstreams) associated with a resource.',
          [Permissions::DISCOVER, Permissions::READ]
        )

        METADATA_VIEWER = RoleType.new(
          'MetadataViewer',
          'The MetadataViewer role conveys access to the description of a resource.',
          [Permissions::DISCOVER]
        )
      end
    end
  end
end
