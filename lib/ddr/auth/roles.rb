module Ddr::Auth
  module Roles
    include RoleTypes

    RESOURCE_SCOPE = 'resource'.freeze
    POLICY_SCOPE = 'policy'.freeze
    SCOPES = [RESOURCE_SCOPE, POLICY_SCOPE].freeze

    ORDERED_ROLE_TYPES = [
      CURATOR,
      EDITOR,
      METADATA_EDITOR,
      DOWNLOADER,
      VIEWER,
      METADATA_VIEWER
    ]

    class << self
      def type_map
        @type_map ||= role_types.index_by { |role_type| role_type.to_s }
      end

      def role_types
        ORDERED_ROLE_TYPES
      end

      def titles
        @titles ||= role_types.map(&:title)
      end
    end
  end
end
