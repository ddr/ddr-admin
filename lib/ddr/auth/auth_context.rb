module Ddr::Auth
  # @abstract
  class AuthContext
    attr_reader :user, :env

    DUKE_AGENT = Regexp.new('@duke\.edu\z')

    def initialize(user = nil, env = nil)
      @user = user
      @env = env
    end

    def ability
      if anonymous?
        AnonymousAbility.new(self)
      elsif superuser?
        SuperuserAbility.new(self)
      else
        default_ability_class.new(self)
      end
    end

    def default_ability_class
      Ddr::Auth.default_ability.constantize
    end

    # Return whether a user is absent from the auth context.
    # @note We assume that `user' is explicitly passed to the constructor.
    #   Since we use Warden, however, we could check env['warden'].authenticated?(:user)
    #   in the case where `env' is available.
    # @return [Boolean]
    def anonymous?
      user.nil?
    end

    # Return whether a user is present in the auth context.
    # @return [Boolean]
    def authenticated?
      !anonymous?
    end

    # Return whether context is authenticated in superuser scope.
    # @return [Boolean]
    def superuser?
      return false if env.nil?

      env['warden']&.authenticated?(:superuser) || false
    end

    # Return the user agent for this context.
    # @return [String] or nil, if auth context is anonymous/
    def agent
      user&.agent
    end

    # Is the authenticated agent a Duke identity?
    # @return [Boolean]
    def duke_agent?
      authenticated? && DUKE_AGENT.match?(agent)
    end

    # Return the list of groups for this context.
    # @return [Array<Group>]
    def groups
      @groups ||= Groups.call(self)
    end

    # Is the user associated with the auth context a member of the group?
    # @param group [Ddr::Auth::Group, String] group object or group id
    # @return [Boolean]
    def member_of?(group)
      case group
      when Group
        groups.include?(group)
      when String
        member_of? Group.new(group)
      when nil
        false
      else
        raise TypeError, 'group must be a Ddr::Auth::Group or a String'
      end
    end

    # Is the auth context authorized to act as superuser?
    #   This is separate from whether the context is authenticated in superuser scope.
    # @return [Boolean]
    def authorized_to_act_as_superuser?
      member_of?(Ddr::Auth.superuser_group)
    end

    # Return the combined user and group agents for this context.
    # @return [Array<String>]
    def agents
      groups.map(&:agent).push(agent).compact
    end

    # @return [String] the IP address, or nil
    # @see ActionDispatch::RemoteIp
    def ip_address
      return nil if env.nil?

      env['action_dispatch.remote_ip']&.calculate_ip
    end

    # The affiliation values associated with the context.
    # @return [Array<String>]
    def affiliation
      []
    end

    # The remote group values associated with the context.
    # @return [Array<String>]
    def ismemberof
      []
    end

    # Is the user a DDR curator?
    # @return [Boolean] user, if present, is a member of the curators group
    def curator?
      superuser? || member_of?(Ddr::Admin.curators_group)
    end
  end
end
