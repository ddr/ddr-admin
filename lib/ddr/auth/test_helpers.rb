module Ddr
  module Auth
    module TestHelpers
      class MockGrouperGateway
        def self.repository_groups(*) = new.repository_groups(*)
        def repository_groups(_raw = false) = []
        def self.user_groups(*) = new.user_groups(*)
        def user_groups(_user, _raw = false) = []
      end
    end
  end
end
