module Ddr::Auth
  #
  # This class represents a Rails web context.
  #
  class WebAuthContext < AuthContext
    AFFILIATION_ENV_KEY = 'affiliation'
    IS_MEMBER_OF_ENV_KEY = 'isMemberOf'
    ENV_VALUE_DELIM = ';'

    # @return [Array<String>]
    def affiliation
      if anonymous?
        super
      else
        split_env(AFFILIATION_ENV_KEY).map { |a| a.sub(/@duke\.edu\z/, '') }
      end
    end

    # @return [Array<String>]
    def ismemberof
      if anonymous?
        super
      else
        split_env(IS_MEMBER_OF_ENV_KEY)
      end
    end

    private

    def split_env(attr, delim = ENV_VALUE_DELIM)
      val = env[attr] || env["HTTP_#{attr.upcase}"]
      val ? val.split(delim) : []
    end
  end
end
