module Ddr::Auth
  #
  # This class represents a "detached" context such as a Rake task.
  #
  class DetachedAuthContext < AuthContext
    def ismemberof
      if anonymous?
        super
      else
        Ddr::API::GroupRepository.user_groups(user)
      end
    end
  end
end
