module Ddr
  module Auth
    class TargetAbilityDefinitions < AbilityDefinitions
      def call
        can :create, Ddr::Target do |obj|
          obj.for_collection_id.present? && can?(:add_target, obj.for_collection)
        end
      end
    end
  end
end
