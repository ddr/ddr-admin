module Ddr
  module Auth
    class Ability < AbstractAbility
      self.ability_definitions = [
        AliasAbilityDefinitions,
        ItemAbilityDefinitions,
        ComponentAbilityDefinitions,
        AttachmentAbilityDefinitions,
        TargetAbilityDefinitions,
        RoleBasedAbilityDefinitions,
        EmbargoAbilityDefinitions
      ]
    end
  end
end
