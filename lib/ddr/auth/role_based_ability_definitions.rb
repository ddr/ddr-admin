module Ddr
  module Auth
    class RoleBasedAbilityDefinitions < AbilityDefinitions
      def call
        Permissions::ALL.each do |permission|
          can permission, [Ddr::Resource, ::SolrDocument, String] do |obj|
            has_permission? permission, obj
          end
        end
      end

      private

      def has_permission?(permission, object)
        permissions(object).include? permission
      end

      def permissions(object)
        case object
        when Ddr::Resource, ::SolrDocument
          cached_permissions(object.id) do
            object.effective_permissions(agents)
          end
        when String
          cached_permissions(object) do
            doc = ::SolrDocument.find(object) # raises SolrDocument::NotFound
            doc.effective_permissions(agents)
          end
        end
      end

      def cached_permissions(id)
        cache[id] ||= yield
      end
    end
  end
end
