module Ddr::Auth
  class Permissions
    DISCOVER            = :discover
    READ                = :read
    DOWNLOAD            = :download
    ADD_CHILDREN        = :add_children
    UPDATE              = :update
    REPLACE             = :replace
    PUBLISH             = :publish
    UNPUBLISH           = :unpublish
    MAKE_NONPUBLISHABLE = :make_nonpublishable
    GRANT               = :grant

    ALL = [DISCOVER, READ, DOWNLOAD, ADD_CHILDREN, UPDATE,
           REPLACE, PUBLISH, UNPUBLISH, MAKE_NONPUBLISHABLE, GRANT]
  end
end
