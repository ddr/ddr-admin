module Ddr::Auth
  class EffectiveRoles
    def self.call(obj, agents = nil)
      (obj.roles | obj.inherited_roles).tap do |roles|
        roles.select! { |r| agents.include?(r.agent) } if agents
      end
    end
  end
end
