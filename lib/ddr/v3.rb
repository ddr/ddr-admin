module Ddr
  #
  # Ddr::V3
  #
  # This module exists mainly to allow for gradual replacement of various "legacy" features,
  # while also providing some new features that fit into newer development paradigms.
  #
  # See app/services/ddr/v3 and similar paths for module classes.
  #
  module V3
  end
end
