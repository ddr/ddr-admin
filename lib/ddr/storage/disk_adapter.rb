module Ddr
  module Storage
    #
    # DDR disk storage adapter
    #
    # Wraps the Valkyrie disk storage adapter in order to add features
    # and for future-proofing.
    #
    class DiskAdapter < Valkyrie::Storage::Disk
      FEATURES = %i[file_path file_size timestamps].freeze

      def initialize(base_path:, path_generator: FilenameGenerator, file_mover: FileMover)
        super
      end

      def upload(**args)
        super.tap do |file|
          if (digest = args[:digest]) && file.checksum(digests: Array(Digest(digest.type).new)).first != (digest.value)
            delete(id: file.id) # roll back!

            raise Ddr::ChecksumInvalid, "File upload checksum did not match expected digest: #{digest.inspect}"
          end
        end
      end

      # @param feature [Symbol] a feature
      # @return [Boolean] whether the feature is supported by the adapter
      def supports?(feature)
        FEATURES.include?(feature)
      end

      # @param id [Valkyrie::ID] file identifier
      # @return [Integer] the size of the file in bytes
      def file_size(id)
        return unless path = file_path(id)

        ::File.size(path)
      end

      # @param id [Valkyrie::ID] file identifier
      # @return [DateTime] the creation time of the file
      def created_at(id)
        return unless path = file_path(id)

        Ddr::Types::DateTime[::File.ctime(path).utc.to_datetime]
      end

      # @param id [Valkyrie::ID] file identifier
      # @return [DateTime] the modification time of the file
      def updated_at(id)
        return unless path = file_path(id)

        Ddr::Types::DateTime[::File.mtime(path).utc.to_datetime]
      end
    end
  end
end
