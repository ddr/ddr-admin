module Ddr::Storage
  # @abstract
  class PersistedFile < Valkyrie::StorageAdapter::File
    def created_at
      raise NotImplementedError
    end

    def updated_at
      raise NotImplementedError
    end
  end
end
