module Ddr
  module Storage
    class FilenameGenerator
      attr_reader :base_path

      def initialize(base_path:)
        @base_path = base_path
      end

      def generate(resource:, file:, original_filename:)
        uuid = SecureRandom.uuid
        Pathname.new(base_path).join(*bucketed_path(uuid)).join(uuid)
      end

      def bucketed_path(id)
        cleaned_id = id.to_s.delete('-')
        cleaned_id[0..5].chars.each_slice(2).map(&:join)
      end
    end
  end
end
