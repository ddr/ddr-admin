module Ddr
  module Storage
    class FileMover
      def self.call(source_path, target_path)
        FileUtils.cp(source_path, target_path)
        ::File.chmod(Ddr::Admin.stored_file_permissions, target_path)
      end
    end
  end
end
