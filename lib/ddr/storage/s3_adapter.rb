require 'aws-sdk-s3'

module Ddr
  module Storage
    #
    # A storage adapter for AWS S3
    #
    # @note Aws::S3::Errors::ServiceError is a superclass of all S3 errors
    #
    class S3Adapter
      # @param options [Hash] adapter options
      # @option options [String] :bucket_name the S3 bucket name
      # @option options [String] :region
      # @option options [String] :access_key_id
      # @option options [String] :secret_access_key
      # @note AWS options may also be set in env vars:
      #   - AWS_REGION
      #   - AWS_ACCESS_KEY_ID
      #   - AWS_SECRET_ACCESS_KEY
      def initialize(**options)
        @options = options.freeze
      end

      def wipe!
        if Rails.env.production? && ENV['ALLOW_PRODUCTION_STORAGE_WIPE'].blank?
          raise 'Aborting wipe operation in production; set ALLOW_PRODUCTION_STORAGE_WIPE=1 to permit.'
        end

        bucket.clear!
      end

      # @return [Aws::S3::Resource] the S3 resource
      def s3_resource
        @s3_resource ||= Aws::S3::Resource.new @options.slice(:region, :access_key_id, :secret_access_key)
      end

      # @return [Aws::S3::Bucket] the S3 bucket
      def bucket
        @bucket ||= s3_resource.bucket @options.fetch(:bucket_name)
      end

      # Placeholder for feature support check
      def supports?(_feature)
        false
      end

      # @return [String] the base URI for S3 objects
      def base_uri
        @base_uri ||= "s3://#{bucket.name}/".freeze
      end

      def handles?(id:)
        id.to_s.start_with?(base_uri)
      end

      # @param file [String, IO] the file to upload
      # @param original_filename [String] the original filename
      # @param resource [Valkyrie::Resource] the resource to which the file belongs
      # @param options [Hash] extra options
      # @option options [Symbol, String] :file_field (nil) the file field on the resource
      # @option options [Hash] :digest ({}) the digest of the file (e.g., {type: "alg", value: "value"})
      # @option options [Hash] :metadata ({}) custom metadata to store with the file in S3
      # @option options [String] :checksum_algorithm ('SHA1') the checksum algorithm to use in S3
      # @raise [ArgumentError] file_field value is invalid, or file is closed
      # @raise [Aws::S3::Errors::ServiceError] S3 service error
      # @return [Ddr::Storage::S3Adapter::S3File] the file object
      def upload(file:, original_filename:, resource: nil, **options)
        file_field = options.fetch(:file_field, nil)

        raise ArgumentError, 'Cannot upload a closed file.' if file.respond_to?(:closed?) && file.closed?

        unless file_field.nil? || Ddr::Files::FILE_FIELDS.include?(file_field.to_sym)
          raise ArgumentError,
                "Invalid file field: #{file_field}"
        end

        warn '[DEPRECATION] Uploading file without associated resource is deprecated.' if resource.nil?
        if resource.present? && resource.id.blank?
          warn '[DEPRECATION] Uploading file for unpersisted resource is deprecated.'
        end
        warn '[DEPRECATION] Uploading file without specifying file_field is deprecated.' if file_field.nil?

        # TODO: something with digest, if present ...
        # digest = options.fetch(:digest, {}) # expects {type: "alg", value: "value"}

        metadata = options.fetch(:metadata, {}).merge(original_filename:)
        upload_options = { metadata:, checksum_algorithm: options.fetch(:checksum_algorithm, 'SHA1') }

        resource_id = resource&.id.present? ? resource.id.to_s : SecureRandom.uuid

        # Generate the Valkyrie ID for the file
        id = id_for(resource_id:, file_field: file_field || generic_file_field)

        # AWS: "When using an open Tempfile, rewind it before uploading or else the object will be empty."
        file.rewind if file.respond_to?(:rewind)

        s3_object = s3_object_for(id)

        warn "Overwriting existing S3 object: #{s3_object.inspect}" if s3_object.exists?

        s3_object.upload_file(file, upload_options)

        # AWS: "If you pass an open File or Tempfile object, then you are
        # responsible for closing it after the upload completes."
        # DDR: We may expect an open IO object to remain open after upload.
        # file.close if file.respond_to?(:close)

        find_by(id:)
      end

      # @param id [Valkyrie::ID] the ID of the file
      # @return [Valkyrie::StorageAdapter::StreamFile] the file object
      # @raise [Valkyrie::StorageAdapter::FileNotFound] if the file is not found
      def find_by(id:)
        io = IOProxy.new s3_object_for(id)

        S3StreamFile.new(id:, io:)
      rescue Aws::S3::Errors::NoSuchKey => e
        raise Valkyrie::StorageAdapter::FileNotFound => e.message
      end

      # Delete the S3 object associated with the ID
      # @param id [Valkyrie::ID] the file ID
      # @raise [Valkyrie::StorageAdapter::FileNotFound] file not found
      def delete(id:)
        s3_object_for(id).delete
      rescue Aws::S3::Errors::NoSuchKey => e
        raise Valkyrie::StorageAdapter::FileNotFound => e.message
      end

      # @param resource_id [Valkyrie::ID] the resource ID
      # @param file_field [Symbol, String] the file field
      # @return [String] the URI for the given S3 key using this adapter
      def uri_for(resource_id:, file_field:)
        base_uri + key_for(resource_id:, file_field:)
      end

      # Extract the S3 key from a Valkyrie ID
      # @param id [Valkyrie::ID]
      # @return [String] the S3 key for the given ID
      def key_from(id)
        id.to_s.sub(base_uri, '')
      end

      # @param id [Valkyrie::ID] the ID of the S3 object
      # @return [Aws::S3::Object] the S3 object for the given key
      # @raise [Aws::S3::Errors::NoSuchKey] key not found
      def s3_object_for(id)
        bucket.object key_from(id)
      end

      # Generates an S3 key for the resource + file_field
      # @param resource_id [String, Valkyrie::ID] the resource ID
      # @param file_field [String, Symbol] the file field
      def key_for(resource_id:, file_field:)
        "#{resource_id}/#{file_field}"
      end

      # @param resource_id [Valkyrie::ID]
      # @param file_field [Symbol, String] the file field
      # @return [Valkyrie::ID] the ID for the given S3 key using this adapter
      def id_for(resource_id:, file_field:)
        Valkyrie::ID.new uri_for(resource_id:, file_field:)
      end

      def generic_file_field
        "generic-#{SecureRandom.hex(4)}"
      end

      # Force to UTF-8, replacing invalid and undefined characters,
      # replace non-printable characters (except space) with double-underscores,
      # and string surrounding whitespace.
      def sanitize_filename(filename)
        filename.encode('UTF-8', invalid: :replace, undef: :replace).gsub(/[^[:print:]]/, '__').strip
      end

      class IOProxy
        def initialize(s3_object)
          @s3_object = s3_object
        end

        delegate :read, :rewind, :close, to: :io

        def size
          @s3_object.size
        end

        def each
          @s3_object.get do |chunk, _headers|
            yield chunk
          end
        end

        private

        def io
          @io ||= @s3_object.get.body
        end
      end

      class S3StreamFile < Valkyrie::StorageAdapter::StreamFile
        def checksum(digests:)
          io.each do |chunk|
            digests.each { |digest| digest.update(chunk) }
          end

          digests.map(&:to_s)
        end
      end
    end
  end
end
