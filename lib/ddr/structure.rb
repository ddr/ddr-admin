module Ddr
  #
  # Wraps a Nokogiri (XML) 'mets' Document
  #
  class Structure < SimpleDelegator
    # Indicates whether the structure is externally provided or maintained by the repository itself (i.e., is the
    # default structure for the object).
    EXTERNALLY_PROVIDED = 'provided'.freeze
    REPOSITORY_MAINTAINED = 'repository'.freeze

    TYPE_DEFAULT = 'default'.freeze

    def structmaps
      @structmaps ||= structMap_nodes.map { |sm| Ddr::Structures::StructMap.new(sm) }
    end

    def metshdr
      @metshdr ||= Ddr::Structures::MetsHdr.new(metsHdr_node)
    end

    def creator
      metshdr.agents.first&.name
    end

    def repository_maintained?
      creator == Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT
    end

    def dereferenced_structure
      structmaps.map { |sm| [sm.type, sm.dereferenced_hash] }.to_h
    end

    def as_xml_document
      __getobj__
    end

    def add_metshdr(id: nil, createdate: nil, lastmoddate: nil, recordstatus: nil)
      metshdr = Ddr::Structures::MetsHdr.build(id:, createdate:, lastmoddate:,
                                               recordstatus:, document: as_xml_document)
      root.add_child(metshdr)
      metshdr
    end

    def add_agent(parent:, role:, id: nil, otherrole: nil, type: nil, othertype: nil, name: nil)
      agent = Ddr::Structures::Agent.build(id:, role:, otherrole:, type:,
                                           othertype:, name:, document: as_xml_document)
      parent.add_child(agent)
      agent
    end

    def add_structmap(type:, id: nil, label: nil)
      structmap = Ddr::Structures::StructMap.build(id:, label:, type:, document: as_xml_document)
      root.add_child(structmap)
      structmap
    end

    def add_div(parent:, id: nil, label: nil, order: nil, orderlabel: nil, type: nil)
      div = Ddr::Structures::Div.build(id:, label:, order:, orderlabel:, type:,
                                       document: as_xml_document)
      parent.add_child(div)
      div
    end

    def add_mptr(parent:, href:, id: nil, loctype: 'ARK', otherloctype: nil)
      mptr = Ddr::Structures::Mptr.build(id:, loctype:, otherloctype:, href:,
                                         document: as_xml_document)
      parent.add_child(mptr)
      mptr
    end

    private

    def structMap_nodes
      xpath('//xmlns:structMap')
    end

    def structMap_node(type)
      xpath("//xmlns:structMap[@TYPE='#{type}']").first
    end

    def metsHdr_node
      xpath('//xmlns:metsHdr')
    end

    def self.xml_template
      Nokogiri::XML(
        '<mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink" />'
      ) do |config|
        config.noblanks
      end
    end
  end
end
