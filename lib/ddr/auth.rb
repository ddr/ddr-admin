module Ddr
  module Auth
    # Name of group whose members are authorized to act as superuser
    mattr_accessor :superuser_group do
      ENV.fetch('SUPERUSER_GROUP', nil)
    end

    mattr_accessor :default_ability do
      '::Ability'
    end
  end
end
