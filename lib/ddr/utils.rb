require 'openssl'
require 'cgi'

module Ddr::Utils
  extend Deprecation

  def self.digest(content, algorithm)
    warn '[DEPRECATION] This operation may be unsafe and is therefore deprecated.'

    raise TypeError, "Algorithm must be a string: #{algorithm.inspect}" unless algorithm.is_a?(String)

    digest_class = OpenSSL::Digest.const_get(algorithm.sub('-', '').to_sym)
    digest_class.new(content).to_s
  rescue NameError
    raise ArgumentError, "Invalid algorithm: #{algorithm}"
  end

  def self.file_or_path?(file)
    file_path(file)
  rescue ArgumentError
    false
  end

  def self.file_path?(file)
    # length is a sanity check
    file.is_a?(String) && (file.length < 1024) && ::File.exist?(file)
  end

  def self.file_path(file)
    if file.respond_to?(:path)
      ::File.absolute_path(file.path)
    elsif file_path?(file)
      file
    else
      raise ArgumentError, 'Argument is neither a File nor a path to an existing file.'
    end
  end

  def self.file_name_for(file)
    if file.respond_to?(:original_filename) && file.original_filename.present?
      file.original_filename
    else
      ::File.basename file_path(file)
    end
  end

  def self.file_uri?(uri)
    return false unless uri

    URI.parse(uri).scheme == 'file'
  end

  def self.sanitize_filename(file_name)
    return unless file_name
    raise ArgumentError, 'file_name argument must be a string' unless file_name.is_a?(String)
    raise ArgumentError, 'file_name argument must not include path' if file_name.include?(::File::SEPARATOR)

    file_name.gsub(/[^\w.-]/, '_')
  end

  # Return file path for URI string
  # Should reverse .path_to_uri
  # "file:/path/to/file" => "/path/to/file"
  # @param uri [String] The URI string to pathify
  # @return [String] the file path
  def self.path_from_uri(uri_string)
    uri = URI.parse(uri_string)
    raise ArgumentError, 'URI does not have the file: scheme.' unless uri.scheme == 'file'

    CGI.unescape(uri.path)
  end

  # Return URI string for file path
  # Should reverse .path_from_uri
  # "/path/to/file" => "file:/path/to/file"
  # @param path [String] the file path
  # @return [String] the file: URI string
  def self.path_to_uri(path)
    uri = URI.parse CGI.escape(path)
    uri.scheme = 'file'
    uri.to_s
  end

  def self.ds_as_of_date_time(ds)
    ds.create_date_string
  end

  # Returns a string suitable to index as a Solr date
  # @param dt [Date, DateTime, Time] the date/time
  # @return [String]
  def self.solr_date(dt)
    return if dt.nil?

    dt.to_time.utc.iso8601
  end

  def self.solr_dates(dts)
    dts.map { |dt| solr_date(dt) }
  end

  class << self
    alias file_name file_name_for
  end
end
