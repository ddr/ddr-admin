require 'ddr'

module Ddr
  IndexError = Class.new(Error)

  module Admin
    VERSION = format('%s+%s', ENV.fetch('APP_VERSION'), ENV.fetch('APP_VCS_REF'))

    mattr_accessor :alert_message do
      ENV.fetch('ALERT_MESSAGE', nil)
    end

    # Host name for use in non-web-request situations
    mattr_accessor :application_hostname do
      ENV.fetch('APPLICATION_HOSTNAME', nil)
    end

    mattr_accessor :auto_assign_permanent_id do
      ActiveModel::Type::Boolean.new.cast(ENV.fetch('AUTO_ASSIGN_PERMANENT_ID', nil)) || false
    end

    mattr_accessor :auto_update_permanent_id do
      ActiveModel::Type::Boolean.new.cast(ENV.fetch('AUTO_UPDATE_PERMANENT_ID', nil)) || false
    end

    # Whether or not structural metadata is updated automatically
    mattr_accessor :auto_update_structures do
      true
    end

    # Batch log location
    mattr_accessor :batch_log_location do
      ENV['BATCH_LOG_LOCATION'] || Rails.root.join('log').to_s
    end

    # Entries per page on batches index display
    mattr_accessor :batches_per_page do
      ENV.fetch('BATCHES_PER_PAGE', 10)
    end

    # Columns in the CSV report generated for a collection
    # Each column represents a *method* of a SolrDocument
    # See ::SolrDocument
    mattr_accessor :collection_report_fields do
      %i[id local_id content_size]
    end

    # List of models that may appear on a "create" menu (if user has ability)
    mattr_accessor :create_menu_models do
      ['Ddr::Collection',
       'Ddr::MetadataFile',
       'Ddr::ManifestBasedIngest',
       'Ddr::NestedFolderIngest',
       'Ddr::StandardIngest']
    end

    # Value used in metadata export/import to separate
    # multiple values in a single CSV cell.
    mattr_accessor :csv_mv_separator do
      ENV.fetch('CSV_MV_SEPARATOR', '|')
    end

    mattr_accessor :data_dir do
      ENV.fetch('DATA_DIR', '/data')
    end

    mattr_accessor :curators_group do
      ENV.fetch('DDR_CURATORS_GROUP', 'duke:policies:duke-digital-repository:curators')
    end

    mattr_accessor :dc_program_group do
      ENV.fetch('DC_PROGRAM_GROUP', 'duke:policies:duke-digital-repository:dc_program')
    end

    # Directory to store export files
    mattr_accessor :export_files_store do
      ENV['EXPORT_FILES_STORE'] || Rails.public_path.join('export_files').to_s
    end

    # Base URL for export files
    mattr_accessor :export_files_base_url do
      ENV.fetch('EXPORT_FILES_BASE_URL', '/export_files')
    end

    mattr_accessor :export_files_max_payload_size do
      ENV.fetch('EXPORT_FILES_MAX_PAYLOAD_SIZE', 1_000_000_000).to_i
    end

    mattr_accessor :export_files_volume do
      ENV.fetch('EXPORT_FILES_VOLUME', nil)
    end

    # Maximum number of resources on which to perform fixity checks
    mattr_accessor :fixity_check_limit do
      ENV.fetch('FIXITY_CHECK_LIMIT', 10_000).to_i
    end

    # Minimum amount of time between successive fixity checks on a resource
    mattr_accessor :fixity_check_period_in_days do
      ENV.fetch('FIXITY_CHECK_PERIOD', '60').to_i
    end

    # Jobs to handle as high priority
    #
    # Set env var value to comma-separated list of demodulized job names.
    mattr_accessor :high_priority_jobs do
      ENV['HIGH_PRIORITY_JOBS']&.split(',') || %w[PublicationJob RolesJob FileUpdateJob]
    end

    # Permitted ingest base paths
    mattr_accessor :ingest_base_paths do
      ENV.fetch('INGEST_BASE_PATHS', '').split(':').freeze
    end

    # File patterns to exclude from ingest
    # Use fnmatch-style patterns separated by spaces
    mattr_accessor :ingest_exclude_files do
      ENV.fetch('INGEST_EXCLUDE_FILES', '**/.DS_Store **/Thumbs.db **/*-sha1.txt').split(/\s+/).freeze
    end

    # Jobs to handle as low priority
    #
    # Set env var value to comma-separated list of demodulized job names.
    mattr_accessor :low_priority_jobs do
      ENV['LOW_PRIORITY_JOBS']&.split(',') || %w[FixityCheckJob]
    end

    # Base storage for multires image storage adapter
    mattr_accessor :multires_image_storage_base_path do
      ENV.fetch('MULTIRES_IMAGE_STORAGE_BASE_PATH', Rails.root.join('tmp/mrifs'))
    end

    # Message displayed in banner indicating a preview
    mattr_accessor :preview_banner_msg do
      ENV.fetch('PREVIEW_BANNER_MSG', nil)
    end

    mattr_accessor :publication_preview_enabled do
      ActiveModel::Type::Boolean.new.cast(ENV.fetch('PUBLICATION_PREVIEW_ENABLED', 'true'))
    end

    mattr_accessor :rubenstein_staff_group do
      ENV.fetch('RL_STAFF_GROUP', 'duke:policies:duke-digital-repository:rubenstein_staff')
    end

    # Number of documents to include in a Solr update batch
    mattr_accessor :solr_update_batch_size do
      ENV.fetch('SOLR_UPDATE_BATCH_SIZE', 20).to_i
    end

    # Base path for disk storage adapter
    mattr_accessor :storage_base_path do
      ENV.fetch('STORAGE_BASE_PATH', Rails.root.join('tmp/files'))
    end

    # Technical metadata fields to display on the show page
    # of a content object.
    mattr_accessor :techmd_show_fields do
      %i[ format_label
          format_version
          media_type
          pronom_identifier
          creating_application
          valid
          well_formed
          file_human_size
          image_width
          image_height
          color_space
          icc_profile_name
          icc_profile_version
          creation_time
          modification_time
          checksum_digest
          checksum_value]
    end

    # Ddr File resource attributes that are relevant for derivatives and thus should trigger
    # derivatives to be updated when they are changed
    mattr_accessor :update_derivatives_on_changed do
      %i[content intermediate_file]
    end

    # Admin metadata fields that can be edited by the user
    mattr_accessor :user_editable_admin_metadata_fields do
      %i[ aspace_id
          bibsys_id
          contentdm_id
          display_format
          ead_id
          local_id
          rights_note]
    end

    # Additional admin metadata fields that can be edited by the user for Ddr::Items
    mattr_accessor :user_editable_item_admin_metadata_fields do
      %i[nested_path viewing_direction]
    end

    mattr_accessor :user_editable_component_admin_metadata_fields do
      [:display_label]
    end

    # Fields displayed in CatalogController index view and
    # "object_list_entry" partial (e.g., for object "children" views).
    # Order is respected and each field must have label text
    # under i18n key ddr.index_list_entry.[field].
    mattr_accessor :index_list_entry_fields do
      %w[common_model_name permanent_id id identifier_all]
    end

    # Permissions to use for stored files
    mattr_accessor :stored_file_permissions do
      0o644
    end
  end
end
