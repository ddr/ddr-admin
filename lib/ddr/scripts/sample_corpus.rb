# Guard against running this script in production
if Rails.env.production? && ENV['ALLOW_SAMPLE_CORPUS'].blank?
  puts 'If you want to run this script in the PRODUCTION environment, set the env var ALLOW_SAMPLE_CORPUS=1 '
  exit
end

# Resource ID's
coll_id = Valkyrie::ID.new('a12e9f2e-01be-47b2-87cd-658a73fe48f8')
item_id = Valkyrie::ID.new('e113ce18-04ad-4a82-be7a-77e2bef328e8')
comp_id = Valkyrie::ID.new('eba3d5dc-7f1c-41e4-8c0b-60c6ae9cb8aa')
att_id = Valkyrie::ID.new('7c44bf8a-bc08-473f-b6b0-7b73ad895c02')
tgt_id = Valkyrie::ID.new('c33bb72f-c069-4562-b48a-b5dd8f18022e')

# Resource Change Set Persister
csp = Ddr::ResourceChangeSetPersister.new

# Remove existing sample corpus
# Resources
begin
  csp.delete(change_set: Ddr::CollectionChangeSet.new(Ddr.query_service.find_by(id: coll_id)))
rescue Valkyrie::Persistence::ObjectNotFoundError
  nil
end
begin
  csp.delete(change_set: Ddr::ItemChangeSet.new(Ddr.query_service.find_by(id: item_id)))
rescue Valkyrie::Persistence::ObjectNotFoundError
  nil
end
begin
  csp.delete(change_set: Ddr::ComponentChangeSet.new(Ddr.query_service.find_by(id: comp_id)))
rescue Valkyrie::Persistence::ObjectNotFoundError
  nil
end
begin
  csp.delete(change_set: Ddr::AttachmentChangeSet.new(Ddr.query_service.find_by(id: att_id)))
rescue Valkyrie::Persistence::ObjectNotFoundError
  nil
end
begin
  csp.delete(change_set: Ddr::TargetChangeSet.new(Ddr.query_service.find_by(id: tgt_id)))
rescue Valkyrie::Persistence::ObjectNotFoundError
  nil
end
# Events
Ddr::Events::Event.where(resource_id: coll_id.id).destroy_all
Ddr::Events::Event.where(resource_id: item_id.id).destroy_all
Ddr::Events::Event.where(resource_id: comp_id.id).destroy_all
Ddr::Events::Event.where(resource_id: att_id.id).destroy_all
Ddr::Events::Event.where(resource_id: tgt_id.id).destroy_all
# Users
User.where(username: 'editor').destroy_all

# Users
User.create(email: 'ed.i.tor@nowhere.com', password: 'editor', username: 'editor')

# Access Roles
editor_policy_role = Ddr::Auth::Roles::Role.new(agent: 'editor', type: 'Editor', scope: 'policy')
public_viewer_policy_role = Ddr::Auth::Roles::Role.new(agent: 'public', type: 'Viewer', scope: 'policy')

# Collection
coll = Ddr::Collection.new(id: coll_id, admin_set: 'dc', title: 'Collection Alpha')
coll_cs = Ddr::CollectionChangeSet.new(coll)
coll_cs.access_role = [editor_policy_role, public_viewer_policy_role]
csp.save(change_set: coll_cs)

# Item
item = Ddr::Item.new(id: item_id, admin_policy_id: coll_id, parent_id: coll_id, title: 'Item Beta')
item_cs = Ddr::ItemChangeSet.new(item)
csp.save(change_set: item_cs)

# Component
comp = Ddr::Component.new(id: comp_id, admin_policy_id: coll_id, parent_id: item_id, title: 'Component Gamma')
comp_cs = Ddr::ComponentChangeSet.new(comp)
comp_cs.add_file(Rails.root.join('spec/fixtures/imageA.tif').to_s, :content)
csp.save(change_set: comp_cs)

# Attachment
att = Ddr::Attachment.new(id: att_id, admin_policy_id: coll_id, attached_to_id: coll_id, title: 'Attachment Delta')
att_cs = Ddr::AttachmentChangeSet.new(att)
att_cs.add_file(Rails.root.join('spec/fixtures/sample.pdf').to_s, :content)
csp.save(change_set: att_cs)

# Target
tgt = Ddr::Target.new(id: tgt_id, admin_policy_id: coll_id, for_collection_id: coll_id, title: 'Target Epsilon')
tgt_cs = Ddr::TargetChangeSet.new(tgt)
tgt_cs.add_file(Rails.root.join('spec/fixtures/target.png').to_s, :content)
csp.save(change_set: tgt_cs)
