namespace :ddr do
  desc 'Print the version string of the app'
  task version: :environment do
    puts Ddr::Admin::VERSION
  end

  desc 'Run FITS file characterization process on content files'
  task :characterize_files, [:limit] => :environment do |_t, args|
    queued = Ddr::BatchFileCharacterization.call(args[:limit])
    puts "#{queued} FITS file characterization job(s) submitted for processing."
  end

  desc 'Destroy object listed in file, one ID per line, and their descendants/dependents'
  task :destroy_objects, [:infile] => :environment do |_t, args|
    ids = File.readlines(args[:infile], chomp: true).map(&:strip).reject { |id| id.blank? }

    print "#{ids.length} objects and their descendants/dependents will be destroyed! Proceed? [y/N] "

    if STDIN.gets.strip == 'y'
      puts 'Destroying objects!'
      Ddr::V3::ResourceDeletionService.bulk(resource_ids: ids)
    else
      puts 'Task aborted.'
    end
  end

  desc 'Sets missing thumbnails in specified collection'
  task :thumbnails, [:collection_id] => :environment do |_t, args|
    Ddr::ThumbnailsJob.perform_later(args[:collection_id])
  end

  namespace :deleted_files do
    desc 'Clean up deleted files according to policy'
    task cleanup: :environment do
      Ddr::DeletedFile.removable.destroy_all
    end
  end

  namespace :file do
    desc 'Upload file to existing resource'
    task upload: :environment do
      resource_id = nil
      while resource_id.blank?
        print "\nResource ID: "
        resource_id = STDIN.gets.strip
      end
      resource = Ddr.query_service.find_by(id: resource_id)

      file_path = nil
      while file_path.blank?
        print "\nPath to file for upload: "
        file_path = STDIN.gets.strip
      end
      unless File.file?(file_path)
        warn 'Path does not exist or is not a file.'
        exit(false)
      end

      print "\nFile type (default: 'content'): "
      file_type = STDIN.gets.strip
      file_type = 'content' if file_type.blank?

      puts <<~EOS

        RESOURCE:  #{resource.common_model_name}
        ID:        #{resource.id}
        FILE PATH: #{file_path}
        FILE TYPE: #{file_type}

      EOS
      print 'Upload file to resource (y/N)? '
      if STDIN.gets.strip == 'y'
        Ddr::UploadFileJob.perform_later(resource_id:, file_path:, file_type:)

        puts "\nUpload file job enqueued.\n"
      else
        puts "\nTask aborted.\n"
      end
    end
  end

  namespace :iiif do
    desc 'Generate IIIF manifests for a collection'
    task :generate_manifests, [:collection_id] => :environment do |_t, args|
      Ddr::IiifCollectionJob.perform_later(args[:collection_id])
    end

    desc 'Generate IIIF manifest for all collections'
    task generate_all_manifests: :environment do
      Ddr::IiifAllCollectionsJob.perform_later
    end
  end

  namespace :index do
    desc 'Deletes everything from the Solr index'
    task clean: :environment do
      indexing_persister = Valkyrie::MetadataAdapter.find(:index_solr).persister
      indexing_persister.wipe!
    end

    desc 'Index single object'
    task :update, [:resource_id] => :environment do |_t, args|
      Ddr::IndexService.call(args[:resource_id])
    end

    desc 'Index all objects in the database'
    task all: :environment do
      Ddr::IndexAllJob.perform_later
    end

    desc 'Re-index all currently indexed objects (DEPRECATED)'
    task reindex_all: :environment do
      warn '[DEPRECATION] ddr:index:reindex_all task is deprecated; use ddr:index:all instead.'
      Ddr::IndexService.call(Ddr::Index.ids)
      puts 'All indexed objects queued for re-indexing.'
    end

    desc 'Index all objects in the repository (DEPRECATED)'
    task update_all: :environment do
      warn '[DEPRECATION] ddr:index:update_all task is deprecated; use ddr:index:all instead.'
      # The indexing of collections and items is dependent on their child resources already being indexed.
      # If there are multiple workers assigned to the :index queue, there could still be race conditions that will
      # cause some index jobs to fail.
      [Ddr::Component, Ddr::Item, Ddr::Collection, Ddr::Attachment, Ddr::Target].each do |model|
        ids = Ddr.query_service.find_all_of_model(model:).map(&:id)
        Ddr::IndexService.call(ids)
      end
      puts 'All repository objects queued for indexing.'
    end

    desc '(Re-)index the contents of a Collection'
    task :collection_contents, [:collection_id] => :environment do |_t, args|
      Ddr::ReindexCollectionContents.call(args[:collection_id])
      puts 'Collection contents have been queued for re-indexing.'
    end

    desc '(Re-)index the children of an Item'
    task :item_children, [:item_id] => :environment do |_t, args|
      item = Ddr.query_service.find_by(id: args[:item_id])
      child_ids = item.children.map(&:id).map(&:to_s).to_a
      Ddr::IndexBatchJob.perform_later(child_ids)
      puts 'Item children have been queued for indexing.'
    end

    desc 'Delete index document by ID'
    task :delete_by_id, [:doc_id] => :environment do |_t, args|
      doc_id = args.fetch(:doc_id)

      Ddr::API::IndexRepository.delete_by_id(doc_id)
    end
  end

  namespace :jobs do
    desc <<-EOS
      Submit resource IDs to ddr-jobs host for fixity checking

      Env vars:
      - FIXITY_CHECK_MIN_DAYS - min number of days since last check (default: 90)
      - FIXITY_CHECK_MAX_RESOURCES - max number of resources to check (default: 100)
    EOS
    task fixity_check: :environment do
      days  = ENV.fetch('FIXITY_CHECK_MIN_DAYS', '90').to_i
      limit = ENV.fetch('FIXITY_CHECK_MAX_RESOURCES', '100').to_i

      query = Ddr::API::IndexRepository.query(
        q: '*:*',
        fq: "last_fixity_check_on_dtsi:[* TO NOW-#{days}DAYS]",
        fl: 'id',
        sort: 'last_fixity_check_on_dtsi ASC'
      )

      resources = query.raw.take(limit)

      if resources.empty?
        warn "No resources found matching criteria: #{query}"
        exit(false)
      end

      jobs_host = ENV['JOBS_HOST'] || ENV.fetch('APPLICATION_HOSTNAME').sub('admin', 'jobs')
      jobs_uri = URI("https://#{jobs_host}/api/fixity_check")
      data = JSON.dump({ resources: })
      Rails.logger.debug { "POSTing fixity check job: #{data}" }

      response = Net::HTTP.post(jobs_uri, data, { 'content-type' => 'application/json' })
      puts response.body
      exit(response.code.to_i < 400)
    end
  end

  namespace :permanent_id do
    desc 'Assign permanent IDs to objects which do not have one'
    task :assign, [:limit] => :environment do |_t, args|
      rows = args[:limit] || 100
      query = Ddr::API::IndexRepository.query(q: '-permanent_id_ssi:*', fl: 'id', rows: rows.to_i)
      resource_ids = query.raw.map { |result| result['id'] }
      puts "Index query limit: #{rows}"
      puts "Number of resources that are missing permanent ids: #{resource_ids.count}"
      Ddr::AssignPermanentIdJob.perform_later(resource_ids) unless resource_ids.empty?
    end

    desc 'Assign permanent ID to single object'
    task :assign_one, [:resource_id] => :environment do |_t, args|
      res_id = args.fetch(:resource_id)
      puts "Resource that will be assigned permanent id: #{res_id}"
      Ddr::AssignPermanentIdJob.perform_now(Array(res_id))
    rescue KeyError
      puts 'Resource id is required.'
      puts 'Example: rake ddr:permanent_id:assign_one[a5b03d9e-6add-4c87-9124-12f2a589ab0c]'
    end
  end

  namespace :roles do
    desc 'Reassign roles from one agent to another'
    task :reassign, %i[from_agent to_agent] => :environment do |_t, args|
      from_agent = args.fetch(:from_agent)
      to_agent   = args.fetch(:to_agent)

      print "This action will permanently reassign roles from '#{from_agent}' to '#{to_agent}' on all resources! Continue (y/N)? "
      unless STDIN.gets.strip == 'y'
        puts 'Task aborted.'
        exit(0)
      end

      Ddr::RoleService.reassign_all(from_agent:, to_agent:)
    end # ddr:roles:reassign

    desc 'Revoke roles assigned to an agent (on all resources)'
    task :revoke, [:agent] => :environment do |_t, args|
      agent = args.fetch(:agent)

      print "This action will permanently revoke all roles assigned to '#{agent}' on all resources! Continue (y/N)? "
      unless STDIN.gets.strip == 'y'
        puts 'Task aborted.'
        exit(0)
      end

      Ddr::RoleService.revoke_all(agent:)
    end
  end
end
